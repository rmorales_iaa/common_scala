/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  27/Sep/2022
 * Time:  13h:29m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming
//=============================================================================
import com.common.fits.standard.block.Block
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.structure.hdu.extension.Extension
//=============================================================================
//=============================================================================
case class A3dtable(recordMap:RecordMap, dataBlockSeq: Array[Block]) extends Extension {
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file A3dtable.scala
//=============================================================================
