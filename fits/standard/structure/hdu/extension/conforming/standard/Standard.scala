/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Sep/2022
 * Time:  16h:52m
 * Description: FITS standard: sectionName 3.1 Overall file structure
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard
//=============================================================================
import com.common.fits.standard.structure.hdu.extension.Extension
import com.common.fits.standard.structure.hdu.extension.Extension.STANDARD_EXTENSION_NAME_SEQ
//=============================================================================
object Standard {
  //---------------------------------------------------------------------------
  def isValidExtensionName(extensionName: String) =
    STANDARD_EXTENSION_NAME_SEQ.contains(extensionName)
  //---------------------------------------------------------------------------
}
//=============================================================================
trait Standard extends Extension
//=============================================================================
//End of file Standard.scala
//=============================================================================
