/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Nov/2022
 * Time:  20h:34m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tdim.parser
//=============================================================================
import com.common.logger.MyLogger
import fastparse.NoWhitespace._
import fastparse._
//=============================================================================
object ParserTdim extends MyLogger {
  //---------------------------------------------------------------------------
  private def digit[_:P] = P(CharIn("0-9")).!.map( s => s )
  //---------------------------------------------------------------------------
  private def integer[_: P] = digit.rep(1).!.map(s=> s.toInt )
  //---------------------------------------------------------------------------
  private def integerSeq[_: P]= (integer | "," ~ integer).rep.map { case s=> s}
  //---------------------------------------------------------------------------
  private def parseInput[_: P] = ("(" ~ integer ~ integerSeq  ~")").map { case (i,seq) =>i +: seq}
  //---------------------------------------------------------------------------
  def run(s: String): (Option[Seq[Int]],String) =
    parse(s, parseInput(_)) match {
      case Parsed.Success(r, index) =>
        if (index == s.size) (Some(r),"")
        else {
          val parsedInput = s.take(index)
          val remain = s.drop(index)
          (None,s"Error parsing input '$s' at position (first position 0): $index. Parsed input: '$parsedInput' Remain input: '$remain'")
        }
      case Parsed.Failure(expected, index, failure) =>
        (None,s"Error parsing the input at index: $index. Expected: '$expected' . Error: '${failure.trace().longAggregateMsg}'")
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ParserTdim.scala
//=============================================================================