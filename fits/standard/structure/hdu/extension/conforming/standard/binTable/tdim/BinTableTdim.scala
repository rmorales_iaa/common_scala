/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  26/Jul/2023
  * Time:  15h:25m
  * Description: None
  */
package com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tdim
//=============================================================================
import com.common.fits.standard.Keyword.KEYWORD_TDIM
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tdim.parser.ParserTdim
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.CommonTable.generateDisconfFatal
import com.common.util.util.Util
//=============================================================================
//=============================================================================
object BinTableTdim {
  //---------------------------------------------------------------------------
  def build(recordMap: RecordMap
            , colIndex: Long
            , ctxt: FitsCtxt): Option[BinTableTdim] = {
    val dimAsString = recordMap.getOptTableFieldValue[String](KEYWORD_TDIM + colIndex).get
    val (dimSeq, errorMessage) = ParserTdim.run(dimAsString)
    if (dimSeq.isEmpty) {
      generateDisconfFatal(
        ctxt
        , s"the value: '$dimAsString' of keyword: '$KEYWORD_TDIM$colIndex' is not valid"
        , additionalInfo = errorMessage
        , sectionName = SectionName.SECTION_7_3_1)
      return None
    }
    Some(BinTableTdim(dimSeq.get.toArray))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class BinTableTdim(dimSeq:Array[Int]) {
  //---------------------------------------------------------------------------
  def format(seq: Array[String]) =
    Util.groupByDimSeq(dimSeq,seq.toList)
  //---------------------------------------------------------------------------
  override def toString() = dimSeq.mkString("(",",",")")
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file BinTableTdim.scala
//=============================================================================