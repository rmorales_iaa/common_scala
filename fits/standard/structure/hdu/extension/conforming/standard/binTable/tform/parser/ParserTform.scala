/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Nov/2022
 * Time:  20h:34m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tform.parser
//=============================================================================
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.BinTableDataType.BIN_TABLE_DATA_TYPE_UNKNOWN
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tform.{BinTableArrayDescriptor, BinTableTform}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.{BinTableDataType}
import com.common.logger.MyLogger
import fastparse.NoWhitespace._
import fastparse._
//=============================================================================
object ParserTform extends MyLogger {
  //---------------------------------------------------------------------------
  private def digit[_:P] = P(CharIn("0-9")).!.map( s => s )
  //---------------------------------------------------------------------------
  private def integer[_: P] = digit.rep(1).!.map(s=> s.toInt )
  //---------------------------------------------------------------------------
  private def dataTypeArrayDescriptor[_: P] = (P("P") | P("Q")).!
  //---------------------------------------------------------------------------
  private def dataTypeBasic[_: P] = (P("L") | P("X") | P("B") | P("I") | P("J") | P("K") |
                                     P("A") | P("E") | P("D") | P("C") | P("M")).!
  //---------------------------------------------------------------------------
  private def dataType[_: P] = dataTypeBasic | dataTypeArrayDescriptor
  //---------------------------------------------------------------------------
  private def arrayDescriptorString[_:P]: P[(Option[String],Option[BinTableArrayDescriptor])] =
    P(CharPred { s => (s >= ' ') && (s <= '~') }).rep.!.map ( s=> (Some(s),None) )
  //---------------------------------------------------------------------------
  private def arrayDescriptor[_: P]: P[(Option[String],Option[BinTableArrayDescriptor])] =
    (dataTypeBasic ~ "(" ~ integer ~ ")").map { case(t,l) =>
      (None,Some(BinTableArrayDescriptor(BinTableDataType(t),l))) }
  //---------------------------------------------------------------------------
  private def additionalString[_: P]: P[(Option[String],Option[BinTableArrayDescriptor])] =
    arrayDescriptor | arrayDescriptorString
  //---------------------------------------------------------------------------
  private def parseInput[_: P]: P[BinTableTform]  = (integer.? ~ dataType ~ additionalString.?).map {
    case(r,t,a) =>
      if (a.isEmpty) BinTableTform(r,t,None,None)
      else BinTableTform(r,t,a.get._1,a.get._2)
  }
  //---------------------------------------------------------------------------
  def run(s: String): (Option[BinTableTform],String) =
    parse(s, parseInput(_)) match {
      case Parsed.Success(r, index) =>
        if (index == s.size)
          if (r.binDataType == BIN_TABLE_DATA_TYPE_UNKNOWN) (None,"Unknown data type")
          else {
            if (r.isArrrayDescriptor && r.repeatCount > 1)  { //P,Q data type
              (None,s"The repeat count: $r can not be applied on column data type: '${r.binDataType.id}'")
            }
            else (Some(r),"")
          }
        else {
          val parsedInput = s.take(index)
          val remain = s.drop(index)
          (None,s"Error parsing input '$s' at position (first position 0): $index. Parsed input: '$parsedInput' Remain input: '$remain'")
        }
      case Parsed.Failure(expected, index, failure) =>
        (None,s"Error parsing the input at index: $index. Expected: '$expected' . Error: '${failure.trace().longAggregateMsg}'")
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ParserTform.scala
//=============================================================================