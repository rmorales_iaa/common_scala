/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  01/Dec/2022
 * Time:  20h:16m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tform
//=============================================================================
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.BinTableDataType
//=============================================================================
//=============================================================================
case class BinTableArrayDescriptor(binTableDataType: BinTableDataType, maxColCount: Long) {
  //---------------------------------------------------------------------------
  def getString(): String = s"${binTableDataType.id}($maxColCount)"
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file BinTableArrayDescriptor.scala
//=============================================================================
