/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  03/Nov/2022
  * Time:  11h:54m
  * Description: None
  */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable
//=============================================================================
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.disconformity.DisconfFatal
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tform.{BinTableTform, BinTableTformArrayDescriptorInt_32, BinTableTformArrayDescriptorInt_64}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.CommonTable.{ByteArr, generateDisconfFatal}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.tdisp.Tdisp
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.{TableColDef, TableRow}
import com.common.util.util.Util
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object BinTableRow {
  //---------------------------------------------------------------------------
  private def parseComplexNumber(colDef: BinTableColDef
                                 , colIndex: Long
                                 , colByteSeq: Array[Byte]
                                 , ctxt: FitsCtxt): Option[Array[String]] = {
    val valueSeq = colDef.applyLinearTransformationComplexNumber(colByteSeq)
    Some(
      valueSeq.grouped(2).map { pairSeq =>
      if (!colDef.isValueInRange(pairSeq(0), ctxt)) return None //check table range, only real part. No null allowed for complex
      "(" +
        colDef.formatDouble(pairSeq(0), colIndex, ctxt) +
        "," +
        colDef.formatDouble(pairSeq(1), colIndex, ctxt) +
        ")"
    }.toArray)
  }
  //---------------------------------------------------------------------------
  private def parseCol(colDef: BinTableColDef
                       , colIndex: Long
                       , colByteSeq: Array[Byte]
                       , applyLinearTransformation: Boolean
                       , ctxt: FitsCtxt):Option[String] = {

    //apply transformation if it exists
    val isDefinedLinearTransformation = colDef.scale.isDefined || colDef.zero.isDefined
    val appliedTransformation = isDefinedLinearTransformation && applyLinearTransformation &&
      BinTableTform.isValidTypeForLinearTransformation(colDef.form)
    val valueSeq =
      if (appliedTransformation) {
        if (colDef.form.isComplexNumber)
          parseComplexNumber(colDef, colIndex, colByteSeq, ctxt).getOrElse(return None)
        else {
          val valueSeq = colDef.applyLinearTransformation(colByteSeq)
          valueSeq.map { value =>
            if (!colDef.isValueInRange(value, ctxt)) return None //check table range
            val isNullValue = BinTableTform.isTnull(value, colDef.nullValue)
            if (isNullValue) colDef.nullValue.get
            else colDef.formatDouble(value, colIndex, ctxt)
          }
        }
      }
      else { //no transformation applied
        if (!colDef.isValueInRange(colByteSeq, ctxt)) return None //check table range
        val (isNullValue, nullValue) = BinTableTform.isTnull(colDef.form, colByteSeq, colDef.nullValue)
        if (isNullValue) Array(nullValue)
        else colDef.format(colByteSeq, colIndex, ctxt)
      }

    //format result  with tdim
    val colAsString =
      if (colDef.tDim.isDefined) colDef.formatArrayWithTdim(valueSeq)
      else {
        if (colDef.form.isArrrayDescriptor || valueSeq.size > 1) valueSeq.mkString(Tdisp.VALUE_SEQ_START_CHAR, Tdisp.VALUE_SEQ_DIVIDER, Tdisp.VALUE_SEQ_END_CHAR)
        else valueSeq.mkString("")
    }

    Some(colAsString)
  }
  //---------------------------------------------------------------------------
  private def parseArrayDescriptor(form: BinTableTform
                                   , _valueSeq: Array[String]
                                   , extendedTable: Array[Byte]
                                   , extendTableLastValidPos: Long
                                   , startPos: Long
                                   , ctxt: FitsCtxt): (Boolean,ByteArr) = {
    val valueSeq =_valueSeq map { _.toLong }
    val colCount = valueSeq(0)
    val startPosition = valueSeq(1)
    val itemByteSize = colCount * form.dataType.byteSize
    if ((startPosition + itemByteSize) > extendTableLastValidPos) {
      generateDisconfFatal(ctxt
        , s"invalid array descriptor with form:'$form'. Starting position:'$startPosition' plus array byte size:'$itemByteSize' is:'${startPosition + itemByteSize}' that is beyond of the extend table last valid position:'$extendTableLastValidPos'"
        , sectionName = SectionName.SECTION_7_3_5)
      return (false,Array())
    }

    val rowByteSeq = extendedTable.drop(startPosition.toInt).take(itemByteSize.toInt)
    val r = form.isValidDataTypeValue(rowByteSeq)
    if (!r._1) {
      generateDisconfFatal(ctxt
        , s"invalid data type value :$startPos. Form:'$form' Byte seq:'${Util.toHexString(extendedTable)}'"
        , sectionName = SectionName.SECTION_7_2_1)
      return (false,Array())
    }

    //check column count using the declared max column count
    val maxAllowedColCount = form match {
      case t:BinTableTformArrayDescriptorInt_32 => t.arrayDescriptor.get.maxColCount
      case t:BinTableTformArrayDescriptorInt_64 => t.arrayDescriptor.get.maxColCount
    }
    if (colCount > maxAllowedColCount) {
      generateDisconfFatal(ctxt
        , s"invalid column count:$colCount regarding the max allowed column defined:$maxAllowedColCount. Form:'$form'}'"
        , sectionName = SectionName.SECTION_7_3_5)
      return (false, Array())
    }
    (true,rowByteSeq)
  }
  //---------------------------------------------------------------------------
  private def parseColSeq(colDefSeq: Array[BinTableColDef]
                          , rowByteSeq: ByteArr
                          , processedRowCount: Long
                          , applyLinearTransformation: Boolean
                          , extendedTable: ByteArr
                          , extendTableLastValidPos: Long
                          , callbackOnEachRow: Option[Array[String] => Boolean]
                          , ctxt: FitsCtxt): Option[Array[String]] = {

    val maxAllowedPos = rowByteSeq.length - 1
    var dropByteSeq = 0

    val colValueSeq = colDefSeq.map { colDef => //slice in columns
      val colIndex = colDef.colIndex
      val startPos = colDef.position
      val endPos = startPos + colDef.byteSize - 1

      if ((startPos < 0) || (endPos > maxAllowedPos)) {
        generateDisconfFatal(ctxt
          , s"invalid row position :$startPos .Valid positions {1,${rowByteSeq.length}}, row:'${Util.toHexString(rowByteSeq)}'"
          , sectionName = SectionName.SECTION_7_2_1)
        return None
      }

      //calculate position of next col
      val byteSize = colDef.byteSize

      //get col byte seq
      var colByteSeq = rowByteSeq.drop(dropByteSeq).take(byteSize)
      dropByteSeq += byteSize

      //update context information to report errors
      ctxt.position = processedRowCount + 1

      //verify the content of the field
      val form = colDef.form

      val ignoreCheckOnDataType = !colDef.form.isInstanceOf[BinTableDataTypeUnsignedByte] //all bytes are valid bytes
      val (isValid,valueSeq) = form.isValidDataTypeValue(colByteSeq, ignoreCheckOnDataType)

      if (!isValid) {
        generateDisconfFatal(ctxt
          , s"invalid data type value :$startPos. Form:'$form' Byte seq:'${Util.toHexString(rowByteSeq)}'"
          , sectionName = SectionName.SECTION_7_2_1)
        return None
      }

      //in case of array descriptor, get the pointed data
      if (form.isArrrayDescriptor) {
        val r = parseArrayDescriptor(
          form
          , valueSeq
          , extendedTable
          , extendTableLastValidPos
          , startPos
          , ctxt)
        if (!r._1) return None
        colByteSeq = r._2 //point to extended area column data
      }

      //parse cols
      parseCol(
        colDef
        , colIndex
        , colByteSeq
        , applyLinearTransformation
        , ctxt).getOrElse(return None)
    }

    //call back with the validated value
    if (callbackOnEachRow.isDefined && !callbackOnEachRow.get(colValueSeq)) return None
    Some(colValueSeq)
  }
  //---------------------------------------------------------------------------
  //(success,rowSeq)
  def buildRowSeq(recordMap: RecordMap
                  , byteSeq:Array[Byte]
                  , applyLinearTransformation: Boolean
                  , storeResult: Boolean
                  , callbackOnEachRow: Option[Array[String] => Boolean] = None
                  , ctxt: FitsCtxt = FitsCtxt()): (Boolean,Array[BinTableColDef],Array[TableRow]) = {
    //--------------------------------------------------------------------------
    val colDefSeq = TableColDef.toBinTableColDef(
      TableColDef.buildColDefSeq(
        recordMap
        , isAciiTable = false
        , SectionName.SECTION_7_3_2
        , ctxt).getOrElse(return (false, Array(), Array()))
    )
    val axisSeq  = recordMap.getAxisSeq()
    val rowByteSize = axisSeq.head  //NAXIS1
    val rowCount    = axisSeq.last  //NAXIS2

    //check size
    if (byteSeq.length < (rowCount * rowByteSize)) {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_7_2_1
        , s"Structure: ${ctxt.structureID} the record the expected binary table size (rowCount,rowByteSize): ($rowCount x $rowByteSize) does not match with the input data size: ${byteSeq.length}"
      )
      return (false,Array(),Array())
    }

    val extendTableFirstPos = axisSeq.product
    val extendTableLastPos = extendTableFirstPos + recordMap.getPcount()  //main table plus extended table
    val extendedTable = byteSeq.drop(extendTableFirstPos.toInt).take(extendTableLastPos.toInt)
    val rowSeq = ArrayBuffer[TableRow]()
    var processedRowCount = 0L
    val mainTableByteSeq = byteSeq.take(extendTableFirstPos.toInt)

    //process byte seq
    mainTableByteSeq
      .grouped(rowByteSize.toInt).foreach { byteSeq =>  //slice in rows

      val rowValueSeq = parseColSeq(
        colDefSeq
        , byteSeq
        , processedRowCount
        , applyLinearTransformation
        , extendedTable
        , extendTableLastPos
        , callbackOnEachRow
        , ctxt).getOrElse(return (false, Array(),Array()))

      processedRowCount += 1
      if (storeResult) rowSeq += BinTableRow(rowValueSeq)
      if (processedRowCount == rowCount) return (true,colDefSeq,if(storeResult) rowSeq.toArray else Array())
    }

    (true,colDefSeq,rowSeq.toArray)
  }
  //---------------------------------------------------------------------------
  def getRowValueSeq(byteSeq: Array[Byte]
                     , colDefSeq: Array[BinTableColDef]
                     , processedRowCount: Long
                     , callbackOnEachRow: Option[Array[String] => Boolean] = None
                     , applyLinearTransformation: Boolean
                     , extendedTable: ByteArr  = Array()
                     , extendTableLastValidPos: Long  = 0
                     , ctxt: FitsCtxt = FitsCtxt()
                    ): (Boolean, Array[String]) =
    (true,
      parseColSeq(
        colDefSeq
        , byteSeq
        , processedRowCount
        , applyLinearTransformation
        , extendedTable
        , extendTableLastValidPos
        , callbackOnEachRow
        , ctxt).getOrElse(return (false, Array())))
  //---------------------------------------------------------------------------
}
//=============================================================================
case class BinTableRow(itemSeq : Array[String]) extends TableRow
//=============================================================================
//End of file BinTableRow.scala
//=============================================================================
