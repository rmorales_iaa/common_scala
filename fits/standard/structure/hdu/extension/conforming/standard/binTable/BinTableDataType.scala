/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  30/Nov/2022
 * Time:  09h:48m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable
//=============================================================================
import com.common.fits.standard.dataType.DataType
import com.common.fits.standard.dataType.DataType._
//=============================================================================
object BinTableDataType {
  //---------------------------------------------------------------------------
  final val BIN_TABLE_DATA_TYPE_LOGICAL_TRUE = 'T'.toByte
  final val BIN_TABLE_DATA_TYPE_LOGICAL_FALSE = 'F'.toByte
  //---------------------------------------------------------------------------
  //null values
  final val BIN_TABLE_DATA_TYPE_LOGICAL_NULL_VALUE    = 0.toByte
  final val BIN_TABLE_DATA_TYPE_CHARACTER_NULL_VALUE  = 0.toByte
  final val BIN_TABLE_DATA_TYPE_FLOAT_32_NULL_VALUE   = Float.NaN
  final val BIN_TABLE_DATA_TYPE_FLOAT_64_NULL_VALUE   = Double.NaN
  //---------------------------------------------------------------------------
  final val BIN_TABLE_DATA_TYPE_L = "L" //Logical
  final val BIN_TABLE_DATA_TYPE_X = "X" //Bit
  final val BIN_TABLE_DATA_TYPE_B = "B" //Unsigned byte
  final val BIN_TABLE_DATA_TYPE_I = "I"  //16-bit integer
  final val BIN_TABLE_DATA_TYPE_J = "J" //32-bit integer
  final val BIN_TABLE_DATA_TYPE_K = "K"  //64-bit integer
  final val BIN_TABLE_DATA_TYPE_A = "A"  //Character
  final val BIN_TABLE_DATA_TYPE_E = "E" //Single-precision floating point
  final val BIN_TABLE_DATA_TYPE_D = "D" //Double-precision floating point
  final val BIN_TABLE_DATA_TYPE_C = "C" //Single-precision complex
  final val BIN_TABLE_DATA_TYPE_M = "M"  //Double-precision complex
  final val BIN_TABLE_DATA_TYPE_P = "P"  //Array descriptor of 32-bit integer
  final val BIN_TABLE_DATA_TYPE_Q = "Q"  //Array descriptor of 64-bit integer
  //---------------------------------------------------------------------------
  //Singleton Objects
  val BIN_TABLE_DATA_TYPE_LOGICAL        = BinTableDataTypeLogical()
  val BIN_TABLE_DATA_TYPE_BIT            = BinTableDataTypeBit()
  val BIN_TABLE_DATA_TYPE_U_BYTE         = BinTableDataTypeUnsignedByte()
  val BIN_TABLE_DATA_TYPE_INT_16         = BinTableDataTypeInt_16()
  val BIN_TABLE_DATA_TYPE_INT_32         = BinTableDataTypeInt_32()
  val BIN_TABLE_DATA_TYPE_INT_64         = BinTableDataTypeInt_64()
  val BIN_TABLE_DATA_TYPE_CHARACTER      = BinTableDataTypeCharacter()
  val BIN_TABLE_DATA_TYPE_FLOAT          = BinTableDataTypeFloat_32()
  val BIN_TABLE_DATA_TYPE_DOUBLE         = BinTableDataTypeFloat_64()
  val BIN_TABLE_DATA_TYPE_FLOAT_COMPLEX  = BinTableDataTypeFloat_32_Complex()
  val BIN_TABLE_DATA_TYPE_DOUBLE_COMPLEX = BinTableDataTypeFloat_64_Complex()
  val BIN_TABLE_DATA_TYPE_ARRAY_INT_32   = BinTableDataTypeArrayDescriptorInt_32()
  val BIN_TABLE_DATA_TYPE_ARRRAY_INT64   = BinTableDataTypeArrayDescriptorInt_64()

  val BIN_TABLE_DATA_TYPE_UNKNOWN        = BinTableDataTypeUnknown()
  //---------------------------------------------------------------------------
  def apply(t: String): BinTableDataType = {
    t match {
      case BIN_TABLE_DATA_TYPE_L => BIN_TABLE_DATA_TYPE_LOGICAL
      case BIN_TABLE_DATA_TYPE_X => BIN_TABLE_DATA_TYPE_BIT
      case BIN_TABLE_DATA_TYPE_B => BIN_TABLE_DATA_TYPE_U_BYTE
      case BIN_TABLE_DATA_TYPE_I => BIN_TABLE_DATA_TYPE_INT_16
      case BIN_TABLE_DATA_TYPE_J => BIN_TABLE_DATA_TYPE_INT_32
      case BIN_TABLE_DATA_TYPE_K => BIN_TABLE_DATA_TYPE_INT_64
      case BIN_TABLE_DATA_TYPE_A => BIN_TABLE_DATA_TYPE_CHARACTER
      case BIN_TABLE_DATA_TYPE_E => BIN_TABLE_DATA_TYPE_FLOAT
      case BIN_TABLE_DATA_TYPE_D => BIN_TABLE_DATA_TYPE_DOUBLE
      case BIN_TABLE_DATA_TYPE_C => BIN_TABLE_DATA_TYPE_FLOAT_COMPLEX
      case BIN_TABLE_DATA_TYPE_M => BIN_TABLE_DATA_TYPE_DOUBLE_COMPLEX
      case BIN_TABLE_DATA_TYPE_P => BIN_TABLE_DATA_TYPE_ARRAY_INT_32
      case BIN_TABLE_DATA_TYPE_Q => BIN_TABLE_DATA_TYPE_ARRRAY_INT64
      case _                     => BIN_TABLE_DATA_TYPE_UNKNOWN
    }
  }
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
trait BinTableDataType {
  //---------------------------------------------------------------------------
  val id: String
  val dataType: DataType[_]
  val isArrayDescriptor: Boolean
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
import BinTableDataType._
sealed case class BinTableDataTypeLogical()                 extends BinTableDataType {val  id = BIN_TABLE_DATA_TYPE_L; val dataType = DATA_TYPE_U_INT_8; val isArrayDescriptor = false}
sealed case class BinTableDataTypeBit()                     extends BinTableDataType {val  id = BIN_TABLE_DATA_TYPE_X; val dataType = DATA_TYPE_U_INT_8; val isArrayDescriptor = false}
sealed case class BinTableDataTypeUnsignedByte()            extends BinTableDataType {val  id = BIN_TABLE_DATA_TYPE_B; val dataType = DATA_TYPE_U_INT_8; val isArrayDescriptor = false}
sealed case class BinTableDataTypeInt_16()                  extends BinTableDataType {val  id = BIN_TABLE_DATA_TYPE_I; val dataType = DATA_TYPE_INT_16; val isArrayDescriptor = false}
sealed case class BinTableDataTypeInt_32()                  extends BinTableDataType {val  id = BIN_TABLE_DATA_TYPE_J; val dataType = DATA_TYPE_INT_32; val isArrayDescriptor = false}
sealed case class BinTableDataTypeInt_64()                  extends BinTableDataType {val  id = BIN_TABLE_DATA_TYPE_K; val dataType = DATA_TYPE_INT_64; val isArrayDescriptor = false}
sealed case class BinTableDataTypeCharacter()               extends BinTableDataType {val  id = BIN_TABLE_DATA_TYPE_A; val dataType = DATA_TYPE_U_INT_8; val isArrayDescriptor = false}
sealed case class BinTableDataTypeFloat_32()                extends BinTableDataType {val  id = BIN_TABLE_DATA_TYPE_E; val dataType = DATA_TYPE_FLOAT_32; val isArrayDescriptor = false}
sealed case class BinTableDataTypeFloat_64()                extends BinTableDataType {val  id = BIN_TABLE_DATA_TYPE_D; val dataType = DATA_TYPE_FLOAT_64; val isArrayDescriptor = false}
sealed case class BinTableDataTypeFloat_32_Complex()        extends BinTableDataType {val  id = BIN_TABLE_DATA_TYPE_C; val dataType = DATA_TYPE_FLOAT_32; val isArrayDescriptor = false}
sealed case class BinTableDataTypeFloat_64_Complex()        extends BinTableDataType {val  id = BIN_TABLE_DATA_TYPE_M; val dataType = DATA_TYPE_FLOAT_64; val isArrayDescriptor = false}
sealed case class BinTableDataTypeArrayDescriptorInt_32()   extends BinTableDataType {val  id = BIN_TABLE_DATA_TYPE_P; val dataType = DATA_TYPE_INT_32; val isArrayDescriptor = true}
sealed case class BinTableDataTypeArrayDescriptorInt_64()   extends BinTableDataType {val  id = BIN_TABLE_DATA_TYPE_Q; val dataType = DATA_TYPE_INT_64; val isArrayDescriptor = true}

sealed case class BinTableDataTypeUnknown()                 extends BinTableDataType {val  id = ""; val dataType = DATA_TYPE_UNKNOWN; val isArrayDescriptor = false}
//=============================================================================
//End of file BinTableDataType.scala
//=============================================================================
