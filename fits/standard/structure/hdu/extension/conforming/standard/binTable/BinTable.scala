/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  27/Sep/2022
 * Time:  13h:29m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable
//=============================================================================
import com.common.fits.standard.block.Block
import com.common.fits.standard.block.record.RecordNumber.RecordInteger
import com.common.fits.standard.Keyword._
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.sectionName.SectionName.SECTION_7_3_2
import com.common.fits.standard.structure.hdu.HDU
import com.common.fits.standard.structure.hdu.HDU._
import com.common.fits.standard.structure.hdu.extension.Extension
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tform._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tform.parser.ParserTform
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.CommonTable._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.tdisp._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.tdisp.parser.ParserTdisp
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable._
import com.common.stream.local.LocalOutputStream
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tdim.parser.ParserTdim
//=============================================================================
//=============================================================================
object BinTable {
  //---------------------------------------------------------------------------
  val DUMP_CSV_NAME  = "bin_table.csv"
  //---------------------------------------------------------------------------
  def isValidNaxis_1(recordMap: RecordMap, ctxt: FitsCtxt = FitsCtxt()): Boolean = {
    val expectedRowByteSize = recordMap.getAxisSeq().head  //bytes per row
    val calculatedRowByteSize = recordMap.getAllRecordWithPrefix(KEYWORD_TFORM).map { r =>
      val result = ParserTform.run(r.value.toString)
      if (result._1.isEmpty) {
        ctxt.extraInfo = result._2
        generateDisconfFatal(
          ctxt
          , s"the key '$KEYWORD_TFORM' with value '${r.value.toString}' is not valid"
          , sectionName = SectionName.SECTION_7_3_1)
      }
      result._1.get.getByteSize
    }.sum
    if (expectedRowByteSize != calculatedRowByteSize) {
      generateDisconfFatal(ctxt
        , s"the value of $KEYWORD_NAXIS:$expectedRowByteSize is less than the sum of all column size: $calculatedRowByteSize"
        , sectionName = SectionName.SECTION_7_3_1)
      true
    }
    else true
  }
  //---------------------------------------------------------------------------
  //the value of  'HEAP' in the gap byte size between main table and the
  // extended storage for variable arrays
  def isValidTheap(recordMap: RecordMap, ctxt: FitsCtxt): Boolean = {
    if (!recordMap.contains(KEYWORD_THEAP)) return true
    if (recordMap.getPcount() == 0) {
      generateDisconfFatal(ctxt
        , s"the value of $KEYWORD_THEAP can not be defined with $KEYWORD_PCOUNT = 0"
        , sectionName = SectionName.SECTION_7_3_1)
    }

    val heapValue = recordMap.getFirstRecord(KEYWORD_THEAP).get
    if (!isValidIntegerRecord(heapValue, SectionName.SECTION_7_3_1, ctxt)) return false
    val r = heapValue.asInstanceOf[RecordInteger]
    val expectedTheap = recordMap.getAxisSeq().take(2)
    if (r.value < expectedTheap.product) {
      generateDisconfFatal(ctxt
        , s"the value of $KEYWORD_THEAP :${r.value} is less than NAXIS1xNAXIS2: ${expectedTheap.head}x${expectedTheap.last}"
        , sectionName = SectionName.SECTION_7_3_1)
    }

    true
  }
  //---------------------------------------------------------------------------
  def isValidTdim(recordMap: RecordMap, ctxt: FitsCtxt): Boolean = {
    val n = recordMap.getTfields()
    for(index <-1L to n) {

      //check is a string
      val keyword = KEYWORD_TDIM + index
      if (!recordMap.contains(keyword)) return true

      //check is valid string
      if (!HDU.isValidStringRecord(
            recordMap.getFirstRecord(keyword).get
          , SectionName.SECTION_7_3_1
          , ctxt)) return false

      //check format string
      val dimAsString = recordMap.getFirstValue[String](keyword).get
      val (r, errorMessage) = ParserTdim.run(dimAsString)
      if (r.isEmpty) {
        generateDisconfFatal(ctxt
          , s"the value: '$dimAsString' of keyword: '$KEYWORD_TDIM$index' is not valid"
          , additionalInfo = errorMessage
          , sectionName = SectionName.SECTION_7_3_1)
      }

      //check size
      val itemCount = r.get.product
      val form =  ParserTform.run(KEYWORD_TFORM + index)._1.get
      if (form.isArrrayDescriptor) {  //array descriptor
        val minExpected = form.arrayDescriptor.get.maxColCount

        if (minExpected == 0) {
          generateDisconfFatal(ctxt
            , s"the value: '$dimAsString' of keyword: '$KEYWORD_TDIM$index' can not be defined when the array length iz zero in keyword: '$KEYWORD_TFORM$index' with value: '${form.getValue()}'"
            , additionalInfo = errorMessage
            , sectionName = SectionName.SECTION_7_3_1)
        }

        if (itemCount > minExpected) {
          generateDisconfFatal(ctxt
            , s"the value: '$dimAsString' of keyword: '$KEYWORD_TDIM$index' is not valid. The item count: $itemCount must be greater than the array length: $minExpected of keyword: '$KEYWORD_TFORM$index' with value: '${form.getValue()}'"
            , additionalInfo = errorMessage
            , sectionName = SectionName.SECTION_7_3_1)
        }
      }
      else { //not array descriptor
        if (itemCount > form.repeatCount) {
          generateDisconfFatal(ctxt
            , s"the value: '$dimAsString' of keyword: '$KEYWORD_TDIM$index' is not valid. The item count: $itemCount must be greater than the repeat count: ${form.repeatCount} of keyword: '$KEYWORD_TFORM$index' with value: '${form.getValue()}'"
            , additionalInfo = errorMessage
            , sectionName = SectionName.SECTION_7_3_1)
        }
      }
    }
    true
  }
  //---------------------------------------------------------------------------
  private def isValidTformCharacterWithTdisp(tDisp: Tdisp
                                             , i: Long
                                             , formValue: String
                                             , dispValue: String
                                             , ctxt: FitsCtxt): Boolean =
    tDisp match {
      case _: TdispCharacter => true
      case _ =>
        generateDisconfFatal(ctxt
          , s"The keyword: '${KEYWORD_TFORM + i}' with value: '$formValue' must have associated with keyword: '${KEYWORD_TDISP + i}' with value 'Ax' but is has value:'$dispValue'"
          , sectionName = SectionName.SECTION_7_3_4)
        true
    }
  //---------------------------------------------------------------------------
  private def isValidTformLogicalWithTdisp(tDisp: Tdisp
                                           , i: Long
                                           , formValue: String
                                           , dispValue: String
                                           , ctxt: FitsCtxt): Boolean =
    tDisp match {
      case _: TdispLogical => true
      case _ =>
        generateDisconfFatal(ctxt
          , s"The keyword: '${KEYWORD_TFORM + i}' with value: '$formValue' must have associated with keyword: '${KEYWORD_TDISP + i}' with value 'Lw' but is has value:'$dispValue'"
          , sectionName = SectionName.SECTION_7_3_4)
        true
    }
  //---------------------------------------------------------------------------
  private def isValidTformIntegerWithTdisp(tDisp: Tdisp
                                           , i: Long
                                           , formValue: String
                                           , dispValue: String
                                           , ctxt: FitsCtxt): Boolean =
    tDisp match {
      case _: TdispInteger     => true
      case _: TdispBinary      => true
      case _: TdispOctal       => true
      case _: TdispHexadecimal => true
      case _: TdispExponential => true
      case _ =>
        generateDisconfFatal(ctxt
          , s"The keyword: '${KEYWORD_TFORM + i}' with value: '$formValue' must have associated with keyword: '${KEYWORD_TDISP + i}' with value 'Iw.m', 'Bw.m', 'Ow.m', or 'Zw.m' but is has value:'$dispValue'"
          , sectionName = SectionName.SECTION_7_3_4)
        true
    }
  //---------------------------------------------------------------------------
  private def isValidTformFloatWithTdisp(tDisp: Tdisp
                                          , i: Long
                                          , formValue: String
                                          , dispValue: String
                                          , ctxt: FitsCtxt): Boolean =
    tDisp match {
      case _: TdispCommonInteger => true
      case _: TdispExponential   => true
      case _ =>
        generateDisconfFatal(ctxt
          , s"The keyword: '${KEYWORD_TFORM + i}' with value: '$formValue' must have associated with keyword: '${KEYWORD_TDISP + i}' with value 'Fw.d', 'Ew.dEe', 'Dw.dEe', 'ENw.d', or 'ESw.d' but is has value:'$dispValue'"
          , sectionName = SectionName.SECTION_7_3_4)
        true
    }
  //---------------------------------------------------------------------------
  private def isValidTdisp(recordMap: RecordMap, ctxt: FitsCtxt): Boolean = {
    val colCount = recordMap.getTfields()

    for (i <- 1L to colCount) {
      val formValue = recordMap.getFirstValue[String](KEYWORD_TFORM + i).getOrElse("")
      val tForm = ParserTform.run(formValue)._1.get
      val dispValue = recordMap.getFirstValue[String](KEYWORD_TDISP + i).getOrElse("")
      val tDisp = ParserTdisp.run(dispValue, isAciiTable = false)

      if (!dispValue.isEmpty) {
        if (BinTableTform.isValidIntegerTdispDataType(tForm)) {
          if (dispValue.isEmpty && !isValidTformIntegerWithTdisp(tDisp, i, formValue, dispValue: String, ctxt)) return false
        }
        else {
          if (BinTableTform.isFloatDataType(tForm)) {
            if (!isValidTformFloatWithTdisp(tDisp, i, formValue, dispValue: String, ctxt)) return false
          }
          else {
            tForm match {
              //character
              case _: BinTableDataTypeCharacter =>
                if (!isValidTformCharacterWithTdisp(tDisp, i, formValue, dispValue: String, ctxt)) return false

              //logical
              case _: BinTableDataTypeLogical =>
                if (!isValidTformLogicalWithTdisp(tDisp, i, formValue, dispValue: String, ctxt)) return false

              case _ => return true
            }
          }
        }
      }
    }
    true
  }
  //---------------------------------------------------------------------------
  private def isValidTypeForLinearTransformation(recordMap: RecordMap
                                                 , ctxt: FitsCtxt
                                                 , colCount: Long): Boolean = {
    for (i <- 1L to colCount) {
      val tScaleDefined = recordMap.contains(KEYWORD_TSCAL + i)
      val tZeroDefined = recordMap.contains(KEYWORD_TZERO + i)

      if (tScaleDefined || tZeroDefined) {
        val key = KEYWORD_TFORM + i
        val formValue = recordMap.getFirstValue[String](key).getOrElse("")
        val tForm = ParserTform.run(formValue)._1.get
        if (!BinTableTform.isValidTypeForLinearTransformation(tForm)) {

          val tScaleMessage = if (tScaleDefined)
            s"keyword: '${KEYWORD_TSCAL + i}' with value:'${recordMap.getFirstRecord(KEYWORD_TSCAL + i).toString}'"
          else ""

          val tZeroMessage = if (tZeroDefined)
            s"keyword: '${KEYWORD_TZERO + i}' with value:'${recordMap.getFirstRecord(KEYWORD_TZERO + i).toString}'"
          else ""

          val message =
            if (tScaleMessage.isEmpty) tZeroMessage
            else
              if (tZeroMessage.isEmpty) tScaleMessage
              else tZeroMessage + " " + tScaleMessage

          generateDisconfFatal(
            ctxt
            , s"the keyword: '$key' can not be used with TFORM of type: '${tForm.getValue()}' in a linear transformation:$message"
            , sectionName = SectionName.SECTION_7_3_2)
        }
      }
    }
    if (!isValidTscal(recordMap, colCount, isAsciiTable = false, ctxt)) return false
    true
  }
  //---------------------------------------------------------------------------
  private def isValidTnull(recordMap: RecordMap, ctxt: FitsCtxt): Boolean = {
    val colCount = recordMap.getTfields()
    for (i <- 1L to colCount) {
      val key = KEYWORD_TNULL + i
      if (recordMap.contains(key)) {
        val record = recordMap.getFirstRecord(key).get
        if (!record.isInstanceOf[RecordInteger]) {
          generateDisconfFatal(
            ctxt
            , s"the keyword: '$key' the value of the record must be an integer. Current value:'${record.value.toString}''"
            , sectionName = SectionName.SECTION_7_3_2)
        }

        val formValue = recordMap.getFirstValue[String](KEYWORD_TFORM + i).getOrElse("")
        val tForm = ParserTform.run(formValue)._1.get
        if (!BinTableTform.isValidTypeForTnull(tForm)) {
          generateDisconfFatal(
            ctxt
            , s"the keyword: '$key' can not be used in TFORM of type: '${tForm.getValue()}'"
            , sectionName = SectionName.SECTION_7_3_2)
        }

        Extension.isValidBlankCommon(
          recordMap
          , ctxt
          , key = key
          , scaleKey = KEYWORD_TSCAL + i
          , zeroKey = KEYWORD_TZERO + i
          , sectionID = SECTION_7_3_2)
      }
    }
    true
  }
  //---------------------------------------------------------------------------
  def isValidHeader(recordMap: RecordMap, ctxt: FitsCtxt = FitsCtxt()): Boolean = {

    //mandatory keywords
    if (!CommonTable.isValidTfields(recordMap, ctxt)) return false
    if (!CommonTable.isValidRecordTform(recordMap, SectionName.SECTION_7_3_1, ctxt)) return false
    if (!isValidNaxis_1(recordMap, ctxt)) return false
    val colCount = recordMap.getTfields()

    //other reserved keywords (check only the concordance of keywordN with TFIELDS=N)
    if (!HDU.isValidRecordStringSeq(recordMap, SectionName.SECTION_7_3_2, colCount, KEYWORD_TTYPE, ctxt, keywordMustExist = true)) return false
    checkTtypeCharacterRange(recordMap, SectionName.SECTION_7_3_2, ctxt)

    if (!HDU.isValidRecordStringSeq(recordMap, SectionName.SECTION_7_3_2, colCount, KEYWORD_TUNIT, ctxt)) return false
    if (!HDU.isValidRecordFloatSeq(recordMap, SectionName.SECTION_7_3_2, colCount, KEYWORD_TSCAL, ctxt)) return false
    if (!HDU.isValidRecordFloatSeq(recordMap, SectionName.SECTION_7_3_2, colCount, KEYWORD_TZERO, ctxt)) return false
    if (!HDU.isValidRecordStringSeq(recordMap, SectionName.SECTION_7_3_2, colCount, KEYWORD_TNULL, ctxt)) return false
    if (!HDU.isValidRecordStringSeq(recordMap, SectionName.SECTION_7_3_2, colCount, KEYWORD_TDISP, ctxt)) return false
    if (!HDU.isValidRecordFloatSeq(recordMap, SectionName.SECTION_7_3_2, colCount, KEYWORD_TDMIN, ctxt)) return false
    if (!HDU.isValidRecordFloatSeq(recordMap, SectionName.SECTION_7_3_2, colCount, KEYWORD_TDMAX, ctxt)) return false
    if (!HDU.isValidRecordFloatSeq(recordMap, SectionName.SECTION_7_3_2, colCount, KEYWORD_TLMIN, ctxt)) return false
    if (!HDU.isValidRecordFloatSeq(recordMap, SectionName.SECTION_7_3_2, colCount, KEYWORD_TLMAX, ctxt)) return false

    if (!HDU.isValidRecordFloatSeq(recordMap, SectionName.SECTION_7_3_2, colCount, KEYWORD_TLMAX, ctxt)) return false
    if (!HDU.isValidRecordFloatSeq(recordMap, SectionName.SECTION_7_3_2, colCount, KEYWORD_TDIM, ctxt)) return false

    if (!isValidTypeForLinearTransformation(recordMap, ctxt, colCount)) return false
    if (!isValidTzero(recordMap, colCount, isAsciiTable = false, ctxt)) return false
    if (!isValidTscal(recordMap, colCount, isAsciiTable = false, ctxt)) return false
    if (!isValidTnull(recordMap, ctxt)) return false
    if (!isValidTdisp(recordMap, ctxt)) return false
    if (!isValidTheap(recordMap, ctxt)) return false
    if (!isValidMinMax(recordMap, colCount, KEYWORD_TDMIN, KEYWORD_TDMAX, SectionName.SECTION_7_3_2, ctxt)) return false
    if (!isValidMinMax(recordMap, colCount, KEYWORD_TLMIN, KEYWORD_TLMAX, SectionName.SECTION_7_3_2, ctxt)) return false
    if (!isValidTdim(recordMap, ctxt)) return false

    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import BinTable._
case class BinTable(recordMap:RecordMap
                    , dataBlockSeq: Array[Block]) extends Extension {
  //---------------------------------------------------------------------------
  val rowCount     = recordMap.getAxisSeq()(1)
  val columnCount  = recordMap.getTfields()
  private var colDefSeq: Array[BinTableColDef] = null
  //---------------------------------------------------------------------------
  def isValid(ctxt: FitsCtxt): Boolean =
    processRowSeq(ctxt, applyLinearTransformation = true)._1
  //---------------------------------------------------------------------------
  //(success,rowSeq)
  private def processRowSeq(ctxt: FitsCtxt
                            , applyLinearTransformation: Boolean
                            , storeResult: Boolean = false
                            , callbackOnEachRow: Option[Array[String] => Boolean] = None):
  (Boolean,Array[BinTableColDef],Array[TableRow]) = {

    val r  = BinTableRow.buildRowSeq(recordMap
                                     , Block.collectAllByteSeq(dataBlockSeq)
                                     , applyLinearTransformation
                                     , storeResult
                                     , callbackOnEachRow
                                     , ctxt)
    if (r._1 && colDefSeq == null) colDefSeq = r._2
    r
  }
  //---------------------------------------------------------------------------
  def getColDefSeq() = colDefSeq
  //---------------------------------------------------------------------------
  def dumpTable(oDir: String, applyLinearTransformation: Boolean): Unit = {

    //process the csv header with the column names
    val columnCount = recordMap.getTfields()
    val csvHeader = (for (i <- 1L to columnCount) yield {
      recordMap.getFirstValue[String](KEYWORD_TTYPE + i).getOrElse("")
    }).mkString(DUMP_CSV_COLUMN_DIVIDER)

    val stream = LocalOutputStream(oDir + DUMP_CSV_NAME)
    stream.writeLine(csvHeader)
    //-------------------------------------------------------------------------
    def saveRow(row: Array[String]): Boolean = {
      stream.writeLine(row.mkString(DUMP_CSV_COLUMN_DIVIDER))
      true
    }
    //-------------------------------------------------------------------------
    processRowSeq(FitsCtxt(), applyLinearTransformation, callbackOnEachRow = Some(saveRow))
    stream.close
  }
  //---------------------------------------------------------------------------
  override def getSummary(structurePosition: String, streamPos: Long = 0): String = {
    val colDefAsString = colDefSeq.map { col => col.getDescription().mkString(" ") }
    super.getSummary(structurePosition, streamPos) +
      "\n\t" + "table column definition" +
      "\n\t\t" + s" rows:$rowCount columns:$columnCount" +
      "\n\t" + "-----------------------" +
      "\n\t" + TableColDef.TABLE_BIN_COL_HEADER +
      "\n\t" + colDefAsString.mkString("\n\t") +
      "\n\t" + "-----------------------"
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file BinTable.scala
//=============================================================================
