/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  03/Nov/2022
  * Time:  11h:45m
  * Description: None
  */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable
//=============================================================================
import com.common.fits.standard.block.record.RecordMap.{BSCALE_DEFAULT_VALUE, BZERO_DEFAULT_VALUE}
import com.common.fits.standard.dataType.DataType._
import com.common.fits.standard.dataType.complex.ComplexFloat_64
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tform.{BinTableTform, BinTableTformCharacter}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.CommonTable.ByteArr
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.tdisp.Tdisp
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.{TableColDef, TableRangeValue}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tdim.BinTableTdim
import com.common.fits.standard.dataType.Conversion._
//=============================================================================
case class BinTableColDef(colIndex: Long //index of the column, starting with index 1
                          , position: Long //starting position
                          , name: Option[String] = None
                          , form: BinTableTform
                          , unit: Option[String] = None
                          , scale: Option[Double] = None
                          , zero: Option[Double] = None
                          , nullValue: Option[String] = None
                          , disp: Option[Tdisp] = None
                          , tableRangeValue: Option[TableRangeValue] = None
                          , tDim: Option[BinTableTdim] = None) extends TableColDef {
  //---------------------------------------------------------------------------
  val dataType = form.dataType
  //---------------------------------------------------------------------------
  val byteSize = form.getByteSize.toByte
  //---------------------------------------------------------------------------
  def applyLinearTransformationComplexNumber(_byteSeq: ByteArr): Array[Double] = {

    val _byteSize = form.dataType.byteSize
    val valueSeq = _byteSize match {
      case DATA_TYPE_FLOAT_32.byteSize =>
        val floatSeq = (_byteSeq.grouped(_byteSize) flatMap (byteSeqToFloatSeq(_))).toArray
        ComplexFloat_64.build(floatSeq map (_.toDouble))

      case DATA_TYPE_FLOAT_64.byteSize =>
        val doubleSeq = (_byteSeq.grouped(_byteSize) flatMap (byteSeqToDoubleSeq(_))).toArray
        ComplexFloat_64.build(doubleSeq)
    }

    val _scale = ComplexFloat_64(scale.getOrElse(BSCALE_DEFAULT_VALUE),0) //imaginary part set to 0, see "7.3.2. Other reserved keywords"
    val _zero = ComplexFloat_64(scale.getOrElse(BZERO_DEFAULT_VALUE),0)  //imaginary part set to 0, see "7.3.2. Other reserved keywords"

    val r = if (_zero.real == BSCALE_DEFAULT_VALUE && _scale.real == BZERO_DEFAULT_VALUE) valueSeq
    else valueSeq map (value => _zero + value * _scale)

    r.flatMap( c=> Seq(c.real,c.imaginary))
  }
  //---------------------------------------------------------------------------
  def applyLinearTransformation(_byteSeq: ByteArr): Array[Double] =
    _byteSeq.grouped(form.dataType.byteSize).toArray.flatMap { byteSeq =>
      val _scale = scale.getOrElse(BSCALE_DEFAULT_VALUE)
      val _zero = zero.getOrElse(BZERO_DEFAULT_VALUE)
      val value = form.getAsDoubleSeq(byteSeq)
      if (_zero == BSCALE_DEFAULT_VALUE && _scale == BZERO_DEFAULT_VALUE) value
      else value map (value => _zero + value * _scale)
    }
  //---------------------------------------------------------------------------
  def formatArrayWithTdim(seq:Array[String]) = {
    if (seq.size == 1) seq.head
    else {
      if (tDim.isDefined)
        tDim.get
          .format(seq).mkString(Tdisp.VALUE_SEQ_START_CHAR, Tdisp.VALUE_SEQ_DIVIDER, Tdisp.VALUE_SEQ_END_CHAR)
          .replaceAll("List\\(", "{")
          .replaceAll("\\)", "}")
      else seq.mkString(Tdisp.VALUE_SEQ_START_CHAR, Tdisp.VALUE_SEQ_DIVIDER, Tdisp.VALUE_SEQ_END_CHAR)
    }
  }
  //---------------------------------------------------------------------------
  private def format(byteSeq: ByteArr
                     , ctxt: FitsCtxt): Array[String] = {
    if (!disp.isDefined) Array(form.format(byteSeq, ctxt))
    else {
      if (form.isInstanceOf[BinTableTformCharacter] || BinTableTform.isComplexNumber(form))
        Array(disp.get.format(byteSeq, form, colIndex, ctxt))
      else {
        byteSeq.grouped(form.dataType.byteSize).map { itemSeq =>
          disp.get.format(itemSeq, form, colIndex, ctxt)
        }.toArray
      }
    }
  }
  //---------------------------------------------------------------------------
  def format(byteSeq: ByteArr
             , colIndex: Long = 1
             , ctxt: FitsCtxt = FitsCtxt()): Array[String] = {
    //---------------------------------------------------------------------------
    if (form.isArrrayDescriptor) {
      if (!disp.isDefined) Array(form.getValue(byteSeq, colIndex, ctxt).get.toString)
      else
        byteSeq.grouped(form.getByteSize.toInt).flatMap { bSeq=>
          format(bSeq, ctxt)
        }.toArray
    }
    else format(byteSeq, ctxt)
  }
  //---------------------------------------------------------------------------
  def formatDouble(value: Double
                   , colIndex: Long = 1
                   , ctxt: FitsCtxt = FitsCtxt()): String =
    if (disp.isDefined) disp.get.format(value, form, colIndex, ctxt)
    else value.toString
  //---------------------------------------------------------------------------
  def formatDoubleSeq(valueSeq: Array[Double]
                      , colIndex: Long = 1
                      , ctxt: FitsCtxt = FitsCtxt()): String =
    valueSeq.map (formatDouble(_, colIndex, ctxt)).mkString(Tdisp.VALUE_SEQ_START_CHAR, Tdisp.VALUE_SEQ_DIVIDER, Tdisp.VALUE_SEQ_END_CHAR)

  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file BinTableColDef.scala
//=============================================================================
