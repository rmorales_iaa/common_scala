/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  03/Nov/2022 
 * Time:  10h:30m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tform
//=============================================================================
import com.common.fits.standard.Keyword.KEYWORD_TFORM
import com.common.fits.standard.dataType.DataType
import com.common.fits.standard.dataType.complex.{ComplexFloat_32, ComplexFloat_64}
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.BinTableDataType._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tform.parser.ParserTform
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.CommonTable.{ByteArr, generateDisconfFatal}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.{TableColForm}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.tdisp.Tdisp
import com.common.util.util.Util
import com.common.util.string.MyString.rightPadding
import com.common.fits.standard.dataType.Conversion._
//=============================================================================
//=============================================================================
object BinTableTform {
  //---------------------------------------------------------------------------
  def apply(repeatCount: Option[Int]
            , dataType: String
            , additionalString: Option[String]
            , arrayDesciptor: Option[BinTableArrayDescriptor]): BinTableTform  = {

    val r = repeatCount.getOrElse(1)
    dataType match {
      case BIN_TABLE_DATA_TYPE_L => BinTableTformLogical(r,additionalString,arrayDesciptor)
      case BIN_TABLE_DATA_TYPE_X => BinTableTformBit(r,additionalString,arrayDesciptor)
      case BIN_TABLE_DATA_TYPE_B => BinTableTformUnsignedByte(r,additionalString,arrayDesciptor)
      case BIN_TABLE_DATA_TYPE_I => BinTableTformInt_16(r,additionalString,arrayDesciptor)
      case BIN_TABLE_DATA_TYPE_J => BinTableTformInt_32(r,additionalString,arrayDesciptor)
      case BIN_TABLE_DATA_TYPE_K => BinTableTformInt_64(r,additionalString,arrayDesciptor)
      case BIN_TABLE_DATA_TYPE_A => BinTableTformCharacter(r,additionalString,arrayDesciptor)
      case BIN_TABLE_DATA_TYPE_E => BinTableTformFloat_32(r,additionalString,arrayDesciptor)
      case BIN_TABLE_DATA_TYPE_D => BinTableTformFloat_64(r,additionalString,arrayDesciptor)
      case BIN_TABLE_DATA_TYPE_C => BinTableTformFloat_32_Complex(r,additionalString,arrayDesciptor)
      case BIN_TABLE_DATA_TYPE_M => BinTableTformFloat_64_Complex(r,additionalString,arrayDesciptor)
      case BIN_TABLE_DATA_TYPE_P => BinTableTformArrayDescriptorInt_32(r,additionalString,arrayDesciptor)
      case BIN_TABLE_DATA_TYPE_Q => BinTableTformArrayDescriptorInt_64(r,additionalString,arrayDesciptor)
      case _                     => BinTableTformUnknown()
    }
  }
  //---------------------------------------------------------------------------
  def build(form: String
            , index: Long
            , ctxt: FitsCtxt): Option[BinTableTform] = {
    val (r,errorMessagge) = ParserTform.run(form)
    if (r.isEmpty) {
      generateDisconfFatal(ctxt
        , s"the value: '$form' of keyword: '$KEYWORD_TFORM$index' is not valid"
        , additionalInfo = errorMessagge)
      None
    }
    else r
  }
  //---------------------------------------------------------------------------
  def isIntegerDataType(f: BinTableTform) =
    f.binDataType match {
      case _: BinTableDataTypeUnsignedByte           => true    //B
      case _: BinTableDataTypeInt_16                 => true    //I
      case _: BinTableDataTypeInt_32                 => true    //J
      case _: BinTableDataTypeInt_64                 => true    //K
      case _: BinTableDataTypeArrayDescriptorInt_32  => true    //P
      case _: BinTableDataTypeArrayDescriptorInt_64  => true    //Q
      case _                                         => false
    }
  //---------------------------------------------------------------------------
  def isValidIntegerTdispDataType(f: BinTableTform) =
    isIntegerDataType(f) || f.isInstanceOf[BinTableTformBit]
  //---------------------------------------------------------------------------
  def isFloatDataType(f: BinTableTform) =
    f.binDataType match {
      case _: BinTableDataTypeFloat_32 => true  //E
      case _: BinTableDataTypeFloat_64 => true  //D
      case _                           => false
    }
  //---------------------------------------------------------------------------
  def isValidTypeForLinearTransformation(f: BinTableTform) =
    f.binDataType match {
      case _: BinTableDataTypeCharacter => false   //A
      case _: BinTableDataTypeLogical   => false   //L
      case _: BinTableDataTypeBit       => false   //X
      case _                            => true
    }
  //---------------------------------------------------------------------------
  //only integers, see definition of TNULL
  def isValidTypeForTnull(f: BinTableTform): Boolean =
    isValidTypeForTnull(f.binDataType.id)

  //---------------------------------------------------------------------------
  //only integers, see definition of TNULL
  def isValidTypeForTnull(dataTypeID: String): Boolean =
    dataTypeID match {
      case BIN_TABLE_DATA_TYPE_B => true //B
      case BIN_TABLE_DATA_TYPE_I => true //I
      case BIN_TABLE_DATA_TYPE_J => true //J
      case BIN_TABLE_DATA_TYPE_K => true //K
      case BIN_TABLE_DATA_TYPE_P => true //P
      case BIN_TABLE_DATA_TYPE_Q => true //Q
      case _ => false
    }

  //---------------------------------------------------------------------------
  def isComplexNumber(f: BinTableTform) =
    f.binDataType match {
      case _: BinTableDataTypeFloat_32_Complex => true //C
      case _: BinTableDataTypeFloat_64_Complex => true //M
      case _ => false
    }
  //---------------------------------------------------------------------------
  def isTnull(form: BinTableTform, v: Array[Byte], tNull: Option[String]): (Boolean,String) = {

    if (tNull.isEmpty) return (false,"")
    if (isIntegerDataType(form)) { //integers
      return (form.binDataType.dataType.getFirstValue(v).toString == tNull.get,tNull.get)
    }

    //remain data types
    form.binDataType match {
      case _: BinTableDataTypeLogical => //L
        (v.head == BIN_TABLE_DATA_TYPE_LOGICAL_NULL_VALUE,v.toString)

      case _: BinTableDataTypeCharacter => //A
        (v.head == BIN_TABLE_DATA_TYPE_CHARACTER_NULL_VALUE,v.toString)

      case _: BinTableDataTypeFloat_32 => //E
        (form.binDataType.dataType.getFirstValue(v).asInstanceOf[Float] == BIN_TABLE_DATA_TYPE_FLOAT_32_NULL_VALUE,BIN_TABLE_DATA_TYPE_FLOAT_32_NULL_VALUE.toString)

      case _: BinTableDataTypeFloat_64 => //D
        (form.binDataType.dataType.getFirstValue(v).asInstanceOf[Double] == BIN_TABLE_DATA_TYPE_FLOAT_64_NULL_VALUE, BIN_TABLE_DATA_TYPE_FLOAT_64_NULL_VALUE.toString)

      case _: BinTableDataTypeFloat_32_Complex => //C
        form.binDataType.dataType.getValueSeq(v) foreach { t =>
          if (t.asInstanceOf[Float] == BIN_TABLE_DATA_TYPE_FLOAT_32_NULL_VALUE) return (true,BIN_TABLE_DATA_TYPE_FLOAT_32_NULL_VALUE.toString)
        }
        (false,"")

      case _: BinTableDataTypeFloat_64_Complex => //M
        form.binDataType.dataType.getValueSeq(v) foreach { t =>
          if (t.asInstanceOf[Double] == BIN_TABLE_DATA_TYPE_FLOAT_64_NULL_VALUE) return (true,BIN_TABLE_DATA_TYPE_FLOAT_64_NULL_VALUE.toString)
        }
        (false,"")

      case _ => (false,"")
    }
  }

  //---------------------------------------------------------------------------
  def isTnull(v: Double, tNull: Option[String]): Boolean = {
    if (tNull.isEmpty) return false
    Math.round(v) == tNull.get.toLong
  }
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
trait BinTableTform extends TableColForm {
  val repeatCount: Int
  val additionalString: Option[String]
  val arrayDescriptor: Option[BinTableArrayDescriptor]
  val binDataType: BinTableDataType
  //---------------------------------------------------------------------------
  def getByteSize = {
    val itemCount =
      if (arrayDescriptor.isDefined || isComplexNumber) 2 //two number describing an array descriptor or a complex number
      else 1
     if (this.isInstanceOf[BinTableTformBit]) Util.getByteCount(repeatCount)
     else repeatCount.toLong * binDataType.dataType.byteSize * itemCount
  }
  //---------------------------------------------------------------------------
  def isArrrayDescriptor = binDataType.isArrayDescriptor
  //---------------------------------------------------------------------------
  def getValue(): String = {
    val a =
      if (additionalString.isDefined) additionalString.get
      else
        if (arrayDescriptor.isDefined) arrayDescriptor.get.getString()
        else ""
    s"$repeatCount${binDataType.id}$a"
  }
  //---------------------------------------------------------------------------
  //used in ASCII tables
  def getValue(s: String
               , colIndex: Long
               , ctxt: FitsCtxt): Option[String] = None
  //---------------------------------------------------------------------------
  //used in ASCII tables
  def format(s: String, ctxt: FitsCtxt): String = ""
  //---------------------------------------------------------------------------
  def format(s: String): Array[Byte]
  //---------------------------------------------------------------------------
  def getValue(byteSeq: ByteArr
               , colIndex: Long
               , ctxt: FitsCtxt): Option[Array[_]] =
    Some(binDataType.dataType.getValueSeq(byteSeq))
  //---------------------------------------------------------------------------
  def getFirstValueAsDouble(byteSeq: ByteArr): Double =
    binDataType.dataType.getFirstValueAsDouble(byteSeq)
  //---------------------------------------------------------------------------
  def getAsDoubleSeq(byteSeq: ByteArr): Array[Double] =
    byteSeq.grouped(dataType.byteSize).map (getFirstValueAsDouble(_)).toArray
  //---------------------------------------------------------------------------
  def isValidDataTypeValue(byteSeq: ByteArr, ignoreCheck: Boolean = false): (Boolean,Array[String]) =
    binDataType.dataType.isValid(byteSeq, ignoreCheck)
  //---------------------------------------------------------------------------
  def isComplexNumber =
    this match {
      case _ : BinTableTformFloat_32_Complex  => true
      case _ : BinTableTformFloat_64_Complex => true
      case _ => false
    }
  //-------------------------------------------------------------------------
  private def getString(_s: Array[Byte]) = {
    val itemByteSize = getDataTypeByteSize
    if (DataType.isInteger(dataType)) DataType.getFirstValueAsLong(_s, itemByteSize)
    else DataType.getStringFromFloat(_s, itemByteSize)
  }
  //---------------------------------------------------------------------------
  def format(byteSeq: Array[Byte], ctxt: FitsCtxt): String = {
    val d = binDataType.dataType
    val r = byteSeq.grouped(d.byteSize).map { seq => getString(seq) }.toArray
    if (r.size == 1) r.mkString("")
    else r.mkString(Tdisp.VALUE_SEQ_START_CHAR, Tdisp.VALUE_SEQ_DIVIDER, Tdisp.VALUE_SEQ_END_CHAR)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
trait BinTableTformInteger extends BinTableTform
//=============================================================================
trait BinTableTformFloat extends BinTableTform
//=============================================================================
sealed case class BinTableTformLogical(repeatCount: Int = 1
                                       , additionalString: Option[String] = None
                                       , arrayDescriptor: Option[BinTableArrayDescriptor] = None) extends BinTableTform {
  //---------------------------------------------------------------------------
  val binDataType =  BinTableDataType.BIN_TABLE_DATA_TYPE_LOGICAL
  val dataType = binDataType.dataType
  //---------------------------------------------------------------------------
  override def getValue(byteSeq: ByteArr
                        , colIndex: Long
                        , ctxt: FitsCtxt): Option[Array[_]] = {
    val valueSeq = byteSeq.map { b =>
      if (b != BIN_TABLE_DATA_TYPE_LOGICAL_TRUE &&
          b != BIN_TABLE_DATA_TYPE_LOGICAL_FALSE &&
          b != BIN_TABLE_DATA_TYPE_LOGICAL_NULL_VALUE) {
        generateDisconfFatal(ctxt
          , s"binary table keyword '${getValue()}' .The input '${Util.toHexString(byteSeq)}' is not a valid logical value. Row ${ctxt.position}: '${ctxt.info}'"
          , SectionName.SECTION_7_3_3_1)
        return None
      }
      else b
    }
    Some(valueSeq)
  }
  //---------------------------------------------------------------------------
  override def format(byteSeq: Array[Byte], ctxt: FitsCtxt): String =
    new String(byteSeq)
  //---------------------------------------------------------------------------
  def format(s: String): Array[Byte] = Array(s.toByte)
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class BinTableTformBit(repeatCount: Int = 1
                                   , additionalString: Option[String] = None
                                   , arrayDescriptor: Option[BinTableArrayDescriptor] = None) extends BinTableTformInteger {
  //---------------------------------------------------------------------------
  val binDataType = BinTableDataType.BIN_TABLE_DATA_TYPE_BIT
  val dataType = binDataType.dataType
  //---------------------------------------------------------------------------
  override def getValue(byteSeq: ByteArr
                        , colIndex: Long
                        , ctxt: FitsCtxt): Option[Array[_]] = {

    val valueSeq = byteSeq.map { b=>
      rightPadding(b.toBinaryString,8,"0")
    }
    Some(valueSeq.asInstanceOf[Array[AnyVal]])
  }
  //---------------------------------------------------------------------------
  override def format(byteSeq: Array[Byte], ctxt: FitsCtxt): String = {
    //avoid negative bytes and pad result to 8 bits
    val r = byteSeq.map(b=> rightPadding(b.toBinaryString.takeRight(8),8,"0"))
    if (r.size == 1) r.mkString("")
    else r.mkString(Tdisp.VALUE_SEQ_START_CHAR, Tdisp.VALUE_SEQ_DIVIDER, Tdisp.VALUE_SEQ_END_CHAR)
  }
  //---------------------------------------------------------------------------
  def format(s: String): Array[Byte] = Array(s.toByte)
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class BinTableTformCharacter(repeatCount: Int = 1
                                         , additionalString: Option[String] = None
                                         , arrayDescriptor: Option[BinTableArrayDescriptor] = None) extends BinTableTform {
  //---------------------------------------------------------------------------
  val binDataType = BinTableDataType.BIN_TABLE_DATA_TYPE_CHARACTER
  val dataType = binDataType.dataType
  //---------------------------------------------------------------------------
  override def format(byteSeq: Array[Byte], ctxt: FitsCtxt): String =
    new String(byteSeq)
  //---------------------------------------------------------------------------
  def format(s: String): Array[Byte] = s.getBytes()
  //---------------------------------------------------------------------------
  def getFirstValue(byteSeq: Array[Byte]) = new String(byteSeq)
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class BinTableTformUnsignedByte(repeatCount: Int = 1
                                            , additionalString: Option[String] = None
                                            , arrayDescriptor: Option[BinTableArrayDescriptor] = None) extends BinTableTformInteger {
  //---------------------------------------------------------------------------
  val binDataType =  BinTableDataType.BIN_TABLE_DATA_TYPE_U_BYTE
  val dataType = binDataType.dataType
  //---------------------------------------------------------------------------
  def format(s: String): Array[Byte] = Array(s.toByte)
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class BinTableTformInt_16(repeatCount: Int = 1
                                      , additionalString: Option[String] = None
                                      , arrayDescriptor: Option[BinTableArrayDescriptor] = None) extends BinTableTformInteger {
  //---------------------------------------------------------------------------
  val binDataType =  BinTableDataType.BIN_TABLE_DATA_TYPE_INT_16
  val dataType = binDataType.dataType
  //---------------------------------------------------------------------------
  def format(s: String): Array[Byte] = shortToByteSeq(s.toShort)
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class BinTableTformInt_32(repeatCount: Int = 1
                                      , additionalString: Option[String] = None
                                      , arrayDescriptor: Option[BinTableArrayDescriptor] = None) extends BinTableTformInteger {
  //---------------------------------------------------------------------------
  val binDataType = BinTableDataType.BIN_TABLE_DATA_TYPE_INT_32
  val dataType = binDataType.dataType
  //---------------------------------------------------------------------------
  def format(s: String): Array[Byte] = intToByteSeq(s.toInt)
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class BinTableTformInt_64(repeatCount: Int = 1
                                      , additionalString: Option[String] = None
                                      , arrayDescriptor: Option[BinTableArrayDescriptor] = None) extends BinTableTformInteger {
  //---------------------------------------------------------------------------
  val binDataType = BinTableDataType.BIN_TABLE_DATA_TYPE_INT_64
  val dataType = binDataType.dataType
  //---------------------------------------------------------------------------
  def format(s: String): Array[Byte] = longToByteSeq(s.toLong)
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class BinTableTformFloat_32(repeatCount: Int = 1
                                        , additionalString: Option[String] = None
                                        , arrayDescriptor: Option[BinTableArrayDescriptor] = None) extends BinTableTformFloat {
  //---------------------------------------------------------------------------
  val binDataType = BinTableDataType.BIN_TABLE_DATA_TYPE_FLOAT
  val dataType = binDataType.dataType
  //---------------------------------------------------------------------------
  def format(s: String): Array[Byte] = floatToByteSeq(s.toFloat)
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class BinTableTformFloat_64(repeatCount: Int = 1
                                        , additionalString: Option[String] = None
                                        , arrayDescriptor: Option[BinTableArrayDescriptor] = None) extends BinTableTformFloat {
  //---------------------------------------------------------------------------
  val binDataType = BinTableDataType.BIN_TABLE_DATA_TYPE_DOUBLE
  val dataType = binDataType.dataType
  //---------------------------------------------------------------------------
  def format(s: String): Array[Byte] = doubleToByteSeq(s.toDouble)
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class BinTableTformFloat_32_Complex(repeatCount: Int = 1
                                                , additionalString: Option[String] = None
                                                , arrayDescriptor: Option[BinTableArrayDescriptor] = None) extends BinTableTform {
  //---------------------------------------------------------------------------
  val binDataType =  BinTableDataType.BIN_TABLE_DATA_TYPE_FLOAT_COMPLEX
  val dataType = binDataType.dataType
  //---------------------------------------------------------------------------
  override def format(byteSeq: Array[Byte], ctxt: FitsCtxt): String = {
    binDataType.dataType.getValueSeq(byteSeq).grouped(2).map { t=>
      ComplexFloat_32(t.head,t.last).format
    }.mkString(Tdisp.VALUE_SEQ_START_CHAR, Tdisp.VALUE_SEQ_DIVIDER, Tdisp.VALUE_SEQ_END_CHAR)
  }
  //---------------------------------------------------------------------------
  def format(s: String): Array[Byte] = {
    val seq = s.trim.drop(1).dropRight(1).split(",")
    floatToByteSeq(seq(0).toFloat) ++ floatToByteSeq(seq(0).toFloat)
  }
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class BinTableTformFloat_64_Complex(repeatCount: Int = 1
                                                , additionalString: Option[String] = None
                                                , arrayDescriptor: Option[BinTableArrayDescriptor] = None) extends BinTableTform {
  //---------------------------------------------------------------------------
  val binDataType =  BinTableDataType.BIN_TABLE_DATA_TYPE_DOUBLE_COMPLEX
  val dataType = binDataType.dataType
  //---------------------------------------------------------------------------
  override def format(byteSeq: Array[Byte], ctxt: FitsCtxt): String = {
    binDataType.dataType.getValueSeq(byteSeq).grouped(2).map { t =>
      ComplexFloat_64(t.head, t.last).format
    }.mkString(Tdisp.VALUE_SEQ_START_CHAR, Tdisp.VALUE_SEQ_DIVIDER, Tdisp.VALUE_SEQ_END_CHAR)
  }
  //---------------------------------------------------------------------------
  def format(s: String): Array[Byte] = {
    val seq = s.trim.drop(1).dropRight(1).split(",")
    doubleToByteSeq(seq(0).toDouble) ++ doubleToByteSeq(seq(0).toDouble)
  }
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class BinTableTformArrayDescriptorInt_32(repeatCount: Int = 1
                                                     , additionalString: Option[String] = None
                                                     , arrayDescriptor: Option[BinTableArrayDescriptor] = None) extends BinTableTform {
  //---------------------------------------------------------------------------
  val binDataType =  BinTableDataType.BIN_TABLE_DATA_TYPE_ARRAY_INT_32
  val dataType = binDataType.dataType
  //---------------------------------------------------------------------------
  override def format(byteSeq: Array[Byte], ctxt: FitsCtxt): String = {
    binDataType.dataType.getValueSeq(byteSeq).grouped(2).map { t =>
      s"(${t.head},${t.last})"
    }.mkString(Tdisp.VALUE_SEQ_START_CHAR, Tdisp.VALUE_SEQ_DIVIDER, Tdisp.VALUE_SEQ_END_CHAR)
  }
  //---------------------------------------------------------------------------
  def format(s: String): Array[Byte] = {
    val seq = s.trim.drop(1).dropRight(1).split(",")
    floatToByteSeq(seq(0).toFloat) ++ floatToByteSeq(seq(0).toFloat)
  }
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class BinTableTformArrayDescriptorInt_64(repeatCount: Int = 1
                                                     , additionalString: Option[String] = None
                                                     , arrayDescriptor: Option[BinTableArrayDescriptor] = None) extends BinTableTform {
  //---------------------------------------------------------------------------
  val binDataType =  BinTableDataType.BIN_TABLE_DATA_TYPE_ARRRAY_INT64
  val dataType = binDataType.dataType
  //---------------------------------------------------------------------------
  override def format(byteSeq: Array[Byte], ctxt: FitsCtxt): String = {
    binDataType.dataType.getValueSeq(byteSeq).grouped(2).map { t =>
      s"(${t.head},${t.last})"
    }.mkString(Tdisp.VALUE_SEQ_START_CHAR, Tdisp.VALUE_SEQ_DIVIDER, Tdisp.VALUE_SEQ_END_CHAR)
  }
  //---------------------------------------------------------------------------
  def format(s: String): Array[Byte] = {
    val seq = s.trim.drop(1).dropRight(1).split(",")
    doubleToByteSeq(seq(0).toDouble) ++ doubleToByteSeq(seq(0).toDouble)
  }
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class BinTableTformUnknown() extends BinTableTform {
  //---------------------------------------------------------------------------
  val repeatCount = 0
  val additionalString =  None
  val arrayDescriptor = None
  val binDataType =  BinTableDataType.BIN_TABLE_DATA_TYPE_UNKNOWN
  val dataType = binDataType.dataType
  //---------------------------------------------------------------------------
  override def getValue(byteSeq: ByteArr
                        , colIndex: Long
                        , ctxt: FitsCtxt): Option[Array[_]] = None
  //---------------------------------------------------------------------------
  override def format(byteSeq: Array[Byte], ctxt: FitsCtxt): String = ""
  //---------------------------------------------------------------------------
  def format(s: String): Array[Byte] = Array()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file BinTableTform.scala
//=============================================================================
