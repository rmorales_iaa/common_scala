/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Nov/2022
 * Time:  13h:48m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.tdisp
//=============================================================================
//=============================================================================
import com.common.fits.standard.block.record.parser.RecordValue.{COMPLEX_NUMBER_DIVIDER_STRING, COMPLEX_NUMBER_END_STRING, COMPLEX_NUMBER_START_STRING}
import com.common.fits.standard.dataType.DataType
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tform._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.CommonTable._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.TableColForm
import com.common.util.string.MyString.{leftPadding}
import com.common.util.util.Util
import com.common.fits.standard.dataType.Conversion._
//=============================================================================
import java.text.DecimalFormat
//=============================================================================
object Tdisp {
  //---------------------------------------------------------------------------
  final val VALUE_SEQ_START_CHAR  = "{"
  final val VALUE_SEQ_DIVIDER = ","
  final val VALUE_SEQ_END_CHAR = "}"
  //---------------------------------------------------------------------------
  final val TDISP_POINT_DIVIDER = "."
  //---------------------------------------------------------------------------
  final val TDISP_A = "A"
  final val TDISP_L = "L"
  final val TDISP_I = "I"
  final val TDISP_B = "B"
  final val TDISP_O = "O"
  final val TDISP_Z = "Z"
  final val TDISP_F = "F"
  final val TDISP_E = "E"
  final val TDISP_EN = "EN"
  final val TDISP_ES = "ES"
  final val TDISP_G = "G"
  final val TDISP_D = "D"
  //---------------------------------------------------------------------------
  final val FLOAT_NUMBER_FRACTION_STEP     =    1
  final val FLOAT_NUMBER_FRACTION_MAX_STEP = 1024
  //---------------------------------------------------------------------------
}
//=============================================================================
import Tdisp._
trait Tdisp {
  //---------------------------------------------------------------------------
  val charSize: Long //w in the standard
  //---------------------------------------------------------------------------
  def getString(colIndex:Long): String
  //---------------------------------------------------------------------------
  def format(s: String, form:TableColForm, colIndex: Long, ctxt: FitsCtxt): String
  //---------------------------------------------------------------------------
  def format(byteSeq: ByteArr, form:TableColForm, colIndex: Long, ctxt: FitsCtxt): String
  //---------------------------------------------------------------------------
  def format(value: Double, form: TableColForm, colIndex: Long, ctxt: FitsCtxt): String =
    format(value.toString, form, colIndex, ctxt)
  //---------------------------------------------------------------------------
}
//=============================================================================
import Tdisp._
trait TdispCommonInteger extends Tdisp {
  //---------------------------------------------------------------------------
  val zeroPaddingMinCount: Int  //m in the standard
  val id: String
  //---------------------------------------------------------------------------
  def getString(colIndex: Long): String = {
    val s = s"$id$colIndex$TDISP_POINT_DIVIDER"
    if (zeroPaddingMinCount == 0) s
    else s".$zeroPaddingMinCount"
  }
  //---------------------------------------------------------------------------
  def format(value: Long, form:TableColForm): String
  //---------------------------------------------------------------------------
  def format(s: String, form: TableColForm, colIndex: Long, ctxt: FitsCtxt): String = {
    val v = if (!Util.isLong(s.trim)) s.toDouble.toLong else s.trim.toLong
    format(v, form)
  }
  //---------------------------------------------------------------------------
  //only for binary display
  def format(byteSeq: ByteArr, form:TableColForm, colIndex: Long, ctxt: FitsCtxt): String = {
    val itemByteSize = form.getDataTypeByteSize
    val r = byteSeq.grouped(itemByteSize).map { seq =>
      val v = DataType.getFirstValueAsLong(seq, itemByteSize)
      format(v, form)
    }.toArray
    if (r.size == 1) r.mkString("")
    else r.mkString(Tdisp.VALUE_SEQ_START_CHAR, Tdisp.VALUE_SEQ_DIVIDER, Tdisp.VALUE_SEQ_END_CHAR)
  }
  //---------------------------------------------------------------------------
  def format(s: String): String = {
    val size = s.size
    if (size > charSize) return "*" * charSize.toInt
    val zeroPadding = if (size < zeroPaddingMinCount) "0" * (zeroPaddingMinCount - size) else ""
    val paddedString = zeroPadding + s
    leftPadding(paddedString.take(charSize.toInt), charSize.toInt)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
trait TdispExponential extends Tdisp {
  //---------------------------------------------------------------------------
  val fractionDigitCount: Int
  val exponentFractionDigitCount: Int
  //---------------------------------------------------------------------------
  val id: String
  val isFloat32B: Boolean
  //---------------------------------------------------------------------------
  val fractionMinValue: Double
  val fractionMinValueCompareWithEqual:Boolean
  //---------------------------------------------------------------------------
  val fractionMaxValue: Double
  val fractionMaxValueCompareWithEqual:Boolean
  //---------------------------------------------------------------------------
  val exponentialFormatting: DecimalFormat = buildExponentialFormat(fractionDigitCount, exponentFractionDigitCount)
  val tDispFixedPoint: TdispFloatFixed
  //---------------------------------------------------------------------------
  def getString(colIndex:Long): String = {
    val s = s"$id$colIndex$TDISP_POINT_DIVIDER$fractionDigitCount"
    if (exponentFractionDigitCount == 0) s
    else s"${s}E$exponentFractionDigitCount"
  }
  //---------------------------------------------------------------------------
  //(fraction,exponent)
  private def findFractionAndExponent(v: BigDecimal): Option[(BigDecimal,Int)]  = {
    //-------------------------------------------------------------------------
    def find(exponentStep: Int): Option[(BigDecimal,Int)] = {
      var fraction: BigDecimal = 0d
      var exponent = 0

      while (Math.abs(exponent) < Tdisp.FLOAT_NUMBER_FRACTION_MAX_STEP) {

        //calculate fraction
        fraction = v.abs / Math.pow(10, exponent)

        //check min
        val isValidMin =
          if (fractionMinValueCompareWithEqual) fraction >= fractionMinValue
          else fraction > fractionMinValue

        //check max
        val isValidMax =
          if (fractionMaxValueCompareWithEqual) fraction <= fractionMaxValue
          else fraction < fractionMaxValue

        if (isValidMin && isValidMax) return Some((fraction,exponent))
        exponent += exponentStep
      }
      None
    }
    //-------------------------------------------------------------------------
    val r = find(FLOAT_NUMBER_FRACTION_STEP)
    if (r.isDefined) r
    else find(-FLOAT_NUMBER_FRACTION_STEP)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private def getExponentString(exponent: Int)  = {
    val sign =  if (exponent >= 0) "+" else "-"
    val exponentString = if (exponent < 0) exponent.toString.drop(1) else exponent.toString  //make it unsigned
    val expectedResultSize = exponentFractionDigitCount + 1
    if((exponentString.size + 1) <= expectedResultSize) //count the sign
      sign + leftPadding(exponentString.take(exponentFractionDigitCount), exponentFractionDigitCount, paddingChar = "0")
    else {
      //can not fit the exponent in e+1 characters (as it said in section 7.3.4).
      //Not implemented the fit in e+2 characters, due it removes de exponent letter,
      //so if it not fit in e+1, then 'entire w-character output field is filled with asterisks (*)'
      ""
    }
  }
  //---------------------------------------------------------------------------
  def format(s: String, form:TableColForm, colIndex: Long, ctxt: FitsCtxt): String = {
    val v = BigDecimal(s.trim)
    val r = findFractionAndExponent(v)
    if (r.isEmpty) {  //can not find an appropriate fraction and exponent
      generateDisconfWarn(ctxt
        , s"Structure: ${ctxt.structureID} .Binary table keyword '${form.getValue()}' .Can not find a valid exponent and fraction for value '$s' . Using fixed point diplay"
        , SectionName.SECTION_7_3_4)

      TdispFloatFixed(charSize,fractionDigitCount).format(s, form, colIndex, ctxt)
    }
    else {
      val sign = if (v < 0) "-" else ""
      val fraction = r.get._1
      val exponentString = getExponentString(r.get._2)
      if (exponentString.isEmpty) return "*" * charSize.toInt
      val fixedPointString = tDispFixedPoint.format(sign + fraction.toString(), form, colIndex, ctxt)
      val exponentialLetter = if (this.isInstanceOf[TdispDoubleExponential]) TDISP_D else TDISP_E
      leftPadding(exponentString.trim.take(exponentFractionDigitCount+1), exponentFractionDigitCount+1, paddingChar = "0")
      val sF = fixedPointString + exponentialLetter + exponentString
      leftPadding(sF.trim.take(charSize.toInt), charSize.toInt)
    }
  }
  //---------------------------------------------------------------------------
  def format(byteSeq: ByteArr, form:TableColForm, colIndex: Long, ctxt: FitsCtxt): String = {
    val itemByteSize = form.getDataTypeByteSize
    form match {
      case _: BinTableTformCharacter => format(new String(byteSeq).toDouble.toString, form, colIndex, ctxt)

      case _: BinTableTformFloat_32_Complex =>
        val valueSeq = (byteSeq.grouped(itemByteSize) map { seq => format(getString(seq, form), form, colIndex, ctxt)}).toSeq
        formatComplexNumber(valueSeq)

      case _: BinTableTformFloat_64_Complex =>
        val valueSeq = (byteSeq.grouped(itemByteSize) map { seq => format(getString(seq, form), form, colIndex, ctxt)}).toSeq
        formatComplexNumber(valueSeq)

      case _ =>
        if (byteSeq.size == itemByteSize) format(getString(byteSeq, form), form, colIndex, ctxt)
        else {
          (byteSeq.grouped(itemByteSize) map { seq => format(getString(seq, form), form, colIndex, ctxt) })
            .mkString(VALUE_SEQ_START_CHAR, VALUE_SEQ_DIVIDER, VALUE_SEQ_END_CHAR)
        }
    }
  }
  //---------------------------------------------------------------------------
  private def formatComplexNumber(valueSeq: Seq[String]) = {
    if (valueSeq.size == 2)
      s"$COMPLEX_NUMBER_START_STRING${valueSeq.head}$COMPLEX_NUMBER_DIVIDER_STRING${valueSeq.last}$COMPLEX_NUMBER_END_STRING"
    else
      valueSeq.grouped(2).map { t =>
        s"$COMPLEX_NUMBER_START_STRING${t.head}$COMPLEX_NUMBER_DIVIDER_STRING${t.last}$COMPLEX_NUMBER_END_STRING"
      }.mkString(VALUE_SEQ_START_CHAR, VALUE_SEQ_DIVIDER, VALUE_SEQ_END_CHAR)
  }
  //-------------------------------------------------------------------------
  private def getString(_s: Array[Byte], form: TableColForm) = {
    val itemByteSize = form.getDataTypeByteSize
    if (DataType.isInteger(form.dataType)) DataType.getFirstValueAsLong(_s, itemByteSize).toString
    else DataType.getStringFromFloat(_s, itemByteSize)
  }
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class TdispCharacter(charSize: Long) extends Tdisp {
  //---------------------------------------------------------------------------
  def getString(colIndex:Long): String = s"$TDISP_A$colIndex"
  //---------------------------------------------------------------------------
  def format(s: String, form:TableColForm, colIndex: Long, ctxt: FitsCtxt): String =
    leftPadding(s.take(charSize.toInt), charSize.toInt)
  //---------------------------------------------------------------------------
  def format(s: ByteArr, form:TableColForm, colIndex: Long, ctxt: FitsCtxt): String =
    format(new String(s),form, colIndex, ctxt)
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class TdispLogical(charSize: Long) extends Tdisp {  //only used in binary tables
  //---------------------------------------------------------------------------
  def getString(colIndex:Long): String = s"$TDISP_L$colIndex"
  //---------------------------------------------------------------------------
  def format(s: String, form:TableColForm, colIndex: Long, ctxt: FitsCtxt): String =
    leftPadding(s.take(charSize.toInt),charSize.toInt)
  //---------------------------------------------------------------------------
  def format(s: ByteArr, form:TableColForm, colIndex: Long, ctxt: FitsCtxt): String =
    format(new String(s),form, colIndex, ctxt)
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class TdispInteger(charSize: Long, zeroPaddingMinCount: Int) extends TdispCommonInteger {
  //---------------------------------------------------------------------------
  val id = TDISP_I
  //---------------------------------------------------------------------------
  def format(value: Long, form: TableColForm): String =
    format(value.toString)
  //---------------------------------------------------------------------------
  override def format(value: Double, form: TableColForm, colIndex: Long, ctxt: FitsCtxt): String =
    format(Math.round(value).toInt.toString, form, colIndex, ctxt)
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class TdispBinary(charSize: Long, zeroPaddingMinCount: Int) extends TdispCommonInteger {
  //---------------------------------------------------------------------------
  val id = TDISP_B
  //---------------------------------------------------------------------------
  def format(value: Long, form: TableColForm): String = {
    var s = value.toBinaryString
    val paddingCharCount = s.size % 8
    if (paddingCharCount > 0) s = s + ("0" * (8 - paddingCharCount))
    format(s.take(charSize.toInt))
  }
  //---------------------------------------------------------------------------
  override def format(value: Double, form: TableColForm, colIndex: Long, ctxt: FitsCtxt): String = {
    val hexSequence = Util.toHexString(doubleToByteSeq(value)).replaceAll(" ","")
    format(hexSequence, form, colIndex, ctxt)
  }
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class TdispOctal(charSize: Long, zeroPaddingMinCount: Int) extends TdispCommonInteger {
  //---------------------------------------------------------------------------
  val id = TDISP_O
  //---------------------------------------------------------------------------
  def format(value: Long, form:TableColForm): String =
    format(value.toOctalString.takeRight(form.getDataTypeOctalSize))
  //---------------------------------------------------------------------------
  override def format(value: Double, form: TableColForm, colIndex: Long, ctxt: FitsCtxt): String =
    format(Math.round(value), form)
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class TdispHexadecimal(charSize: Long, zeroPaddingMinCount: Int) extends TdispCommonInteger {
  //---------------------------------------------------------------------------
  val id = TDISP_Z
  //---------------------------------------------------------------------------
  def format(value: Long, form: TableColForm): String = {
    val s = value.toHexString
    format(s.toUpperCase.takeRight(charSize.toInt))
  }
  //---------------------------------------------------------------------------
  override def format(value: Double, form: TableColForm, colIndex: Long, ctxt: FitsCtxt): String = {
    val r = doubleToByteSeq(value) map { byte =>
      format(Util.unsignedByteToInt(byte),form)
    }
    if (r.size == 1) r.head
    else  r.mkString(Tdisp.VALUE_SEQ_START_CHAR, Tdisp.VALUE_SEQ_DIVIDER, Tdisp.VALUE_SEQ_END_CHAR)
  }
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class TdispFloatFixed(charSize: Long, fractionDigitCount: Int) extends TdispExponential {
  //---------------------------------------------------------------------------
  val id = TDISP_F
  //---------------------------------------------------------------------------
  val exponentFractionDigitCount = 0
  override val exponentialFormatting = buildFloatFixedPoint(fractionDigitCount)
  //---------------------------------------------------------------------------
  val isFloat32B = true
  override val tDispFixedPoint = null
  //---------------------------------------------------------------------------
  val fractionMinValue = Double.NaN //not used
  val fractionMinValueCompareWithEqual = false
  //---------------------------------------------------------------------------
  val fractionMaxValue = Double.NaN //not used
  val fractionMaxValueCompareWithEqual = false
  //---------------------------------------------------------------------------
  override def getString(colIndex:Long): String = s"$id$colIndex$TDISP_POINT_DIVIDER$fractionDigitCount"
  //---------------------------------------------------------------------------
  override def format(s: String, form: TableColForm, colIndex: Long, ctxt: FitsCtxt): String = {
    val v = BigDecimal(s.trim)
    val sF = exponentialFormatting.format(v)
    leftPadding(sF.trim.take(charSize.toInt), charSize.toInt)
  }
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class TdispFloatExponential(charSize: Long
                                        , fractionDigitCount: Int
                                        , exponentFractionDigitCount: Int  = 2) extends TdispExponential {
  //---------------------------------------------------------------------------
  val id = TDISP_E
  val isFloat32B = false
  //---------------------------------------------------------------------------
  override val tDispFixedPoint = TdispFloatFixed(charSize - exponentFractionDigitCount - 2, fractionDigitCount)
  //---------------------------------------------------------------------------
  val fractionMinValue = 0.1
  val fractionMinValueCompareWithEqual = true
  //---------------------------------------------------------------------------
  val fractionMaxValue = 1.0
  val fractionMaxValueCompareWithEqual = false
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class TdispEngineeringExponential(charSize: Long
                                              , fractionDigitCount: Int
                                              , exponentFractionDigitCount: Int = 2) extends TdispExponential {
  //---------------------------------------------------------------------------
  val id = TDISP_EN
  val isFloat32B = false
  override val tDispFixedPoint = TdispFloatFixed(charSize - exponentFractionDigitCount - 2, fractionDigitCount)
  //---------------------------------------------------------------------------
  val fractionMinValue = 1
  val fractionMinValueCompareWithEqual = true
  //---------------------------------------------------------------------------
  val fractionMaxValue = 1000
  val fractionMaxValueCompareWithEqual = false
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class TdispScientificExponential(charSize: Long
                                             , fractionDigitCount: Int
                                             , exponentFractionDigitCount: Int  = 2) extends TdispExponential {
  //---------------------------------------------------------------------------
  val id = TDISP_ES
  val isFloat32B = false
  override val tDispFixedPoint = TdispFloatFixed(charSize - exponentFractionDigitCount - 2, fractionDigitCount)
  //---------------------------------------------------------------------------
  val fractionMinValue = 1.0
  val fractionMinValueCompareWithEqual = true
  //---------------------------------------------------------------------------
  val fractionMaxValue = 10
  val fractionMaxValueCompareWithEqual = false
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class TdispGeneralExponential(charSize: Long
                                          , fractionDigitCount: Int
                                          , exponentFractionDigitCount: Int) extends TdispExponential {
  //---------------------------------------------------------------------------
  val id = TDISP_G
  val isFloat32B = false
  override val tDispFixedPoint = TdispFloatFixed(charSize, fractionDigitCount)
  //---------------------------------------------------------------------------
  val fractionMinValue = Double.NaN //not used
  val fractionMinValueCompareWithEqual = false
  //---------------------------------------------------------------------------
  val fractionMaxValue = Double.NaN //not used
  val fractionMaxValueCompareWithEqual = false
  //---------------------------------------------------------------------------
  val tDispExponential = TdispFloatExponential(charSize, fractionDigitCount)
  val minValueForExponentialTdisp = 0.1 - 0.5 * Math.pow(10,-fractionDigitCount-1)
  val maxValueForExponentialTdisp = Math.pow(10,fractionDigitCount) - 0.5
  //---------------------------------------------------------------------------
  private def getNewFractionDigitCount(valueAbs: BigDecimal): Int = {
    val a = 1 - 0.5 * Math.pow(10,-fractionDigitCount)
    for(k <-0 to fractionDigitCount) {
      if (valueAbs >= (Math.pow(10,k-1) * a) &&
         (valueAbs <= Math.pow(10,k) * a))
        return fractionDigitCount - k
    }
    -1
  }
  //---------------------------------------------------------------------------
  override def format(s: String, form: TableColForm, colIndex: Long, ctxt: FitsCtxt): String = {
    val size = s.size
    if (size > charSize) return "*" * charSize.toInt

    val valueAbs = BigDecimal(s.trim).abs
    if (valueAbs < minValueForExponentialTdisp) tDispFixedPoint.format(s,form,colIndex,ctxt)
    else {
      if (valueAbs >= minValueForExponentialTdisp &&
          valueAbs < maxValueForExponentialTdisp)
        tDispExponential.format(s, form, colIndex, ctxt)
      else {
        val newFractionDigitCount = getNewFractionDigitCount(valueAbs)
        if (newFractionDigitCount == -1) tDispFixedPoint.format(s, form, colIndex, ctxt)
        else {
          val r = TdispFloatFixed(charSize - exponentFractionDigitCount - 2
            , newFractionDigitCount).format(s, form, colIndex, ctxt)
          r + (" " * (exponentFractionDigitCount + 2))
        }
      }
    }
  }
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class TdispDoubleExponential(charSize: Long
                                         , fractionDigitCount: Int
                                         , exponentFractionDigitCount: Int  = 2) extends TdispExponential {
  //---------------------------------------------------------------------------
  val id = TDISP_D
  val isFloat32B = true
  override val tDispFixedPoint = TdispFloatFixed(charSize - exponentFractionDigitCount - 2, fractionDigitCount)
  //---------------------------------------------------------------------------
  val fractionMinValue = 0.1
  val fractionMinValueCompareWithEqual = true
  //---------------------------------------------------------------------------
  val fractionMaxValue = 1.0
  val fractionMaxValueCompareWithEqual = false
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class TdispError(message: String) extends Tdisp {
  //---------------------------------------------------------------------------
  val colIndex = -1
  val charSize = -1
  //---------------------------------------------------------------------------
  val fractionMinValue = Double.NaN //not used
  val fractionMinValueCompareWithEqual = false
  //---------------------------------------------------------------------------
  val fractionMaxValue = Double.NaN //not used
  val fractionMaxValueCompareWithEqual = false
  //---------------------------------------------------------------------------
  def getString(colIndex:Long)  = message
  //---------------------------------------------------------------------------
  def format(s: String, form:TableColForm, colIndex: Long, ctxt: FitsCtxt): String = ""
  //---------------------------------------------------------------------------
  def format(s: ByteArr, form:TableColForm, colIndex: Long, ctxt: FitsCtxt) = ""
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
//=============================================================================
//End of file Tdisp.scala
//=============================================================================
