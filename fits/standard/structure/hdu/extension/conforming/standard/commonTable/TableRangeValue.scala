/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  04/Nov/2022
 * Time:  08h:57m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable
//=============================================================================
import com.common.fits.standard.Keyword._
import com.common.fits.standard.disconformity.{DisconfFatal, DisconfWarn}
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.tform.AsciiTableTformCharacter
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.CommonTable.{generateDisconfFatal}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.TableColDef._
import com.common.util.string.MyString.{leftPadding}
//=============================================================================
import scala.language.postfixOps
//=============================================================================
object TableRangeValue {
  //---------------------------------------------------------------------------
  def getRangeAsString(prefix:String, min: Option[Double],max: Option[Double]) =
    s"$prefix{${min.getOrElse("")},${max.getOrElse("")}}"
  //---------------------------------------------------------------------------
  private def getTableRange(recordMap: RecordMap
                          , colIndex: Long
                          , ctxt: FitsCtxt = FitsCtxt()): (Boolean, Option[TableRangeValue]) = {

    //TDMIN
    val _tdMin = recordMap.getFirstValue[Double](KEYWORD_TDMIN + colIndex)
    val tdMin = if (_tdMin isDefined) Some(_tdMin.get) else None

    //TDMAX
    val _tdMax = recordMap.getFirstValue[Double](KEYWORD_TDMAX + colIndex)
    val tdMax = if (_tdMax isDefined) Some(_tdMax.get) else None

    //TLMIN
    val _tlMin = recordMap.getFirstValue[Double](KEYWORD_TLMIN + colIndex)
    val tlMin = if (_tlMin isDefined) Some(_tlMin.get) else None

    //TLMAX
    val _tlMax = recordMap.getFirstValue[Double](KEYWORD_TLMAX + colIndex)
    val tlMax = if (_tlMax isDefined) Some(_tlMax.get) else None

    //final result
    if (tdMin.isEmpty && tdMax.isEmpty && tlMin.isEmpty && tlMax.isEmpty) (true,None)
    else (true, Some(TableRangeValue(tdMin, tdMax, tlMin, tlMax)))
  }
  //---------------------------------------------------------------------------
  def build(recordMap: RecordMap
            , colIndex: Long
            , form: TableColForm
            , formString: String
            , isAsciiTable: Boolean
            , ctxt: FitsCtxt = FitsCtxt()): (Boolean, Option[TableRangeValue]) = {
    val tableRangeValue = getTableRange(recordMap, colIndex, ctxt)
    if (!tableRangeValue._1) return (false, None)

    if (isAsciiTable) {
      val isCharacter = form.isInstanceOf[AsciiTableTformCharacter]
      if (isCharacter && recordMap.contains(KEYWORD_TSCAL + colIndex)) {
        generateDisconfFatal(ctxt, s"the value: '$formString' of keyword: '$KEYWORD_TFORM$colIndex' and keyword: '$KEYWORD_TSCAL$colIndex' can not be used toghether describing a field")
        return (false, None)
      }

      if (isCharacter && recordMap.contains(KEYWORD_TZERO + colIndex)) {
        generateDisconfFatal(ctxt, s"the value: '$formString' of keyword: '$KEYWORD_TFORM$colIndex' and keyword: '$KEYWORD_TZERO$colIndex' can not be used toghether describing a field")
        return (false, None)
      }
    }
    (true, tableRangeValue._2)
  }
  //---------------------------------------------------------------------------
  def generateDisconfFatalTableRange(v: Double
                                     , ctxt: FitsCtxt
                                     , sectionName: String) =
    DisconfFatal(
      ctxt
      , sectionName
      , s"Structure: ${ctxt.structureID} the value: '$v' of keyword: '${ctxt.info}' at position: ${ctxt.position} is not in the valid range:${ctxt.extraInfo}'"
    )

  //---------------------------------------------------------------------------
  def generateDisconfWarnTableRange(v: Double
                                    , ctxt: FitsCtxt
                                    , sectionName: String) =
    DisconfWarn(
      ctxt
      , sectionName
      , s"Structure: ${ctxt.structureID} the value: '$v' of keyword: '${ctxt.info}' at position: ${ctxt.position} is not in the valid range:${ctxt.extraInfo}'"
    )
  //---------------------------------------------------------------------------
}
//=============================================================================
import TableRangeValue._
case class TableRangeValue(tdMin:Option[Double]
                           , tdMax:Option[Double]
                           , tlMin:Option[Double]
                           , tlMax:Option[Double]) {
  //---------------------------------------------------------------------------
  override def toString() : String =
    TableRangeValue.getRangeAsString("TD",tdMin,tdMax) +
    TableRangeValue.getRangeAsString("TL",tlMin,tlMax)
  //---------------------------------------------------------------------------
  def isValid(v: Double, cxt: FitsCtxt): Boolean = {
    val newCtxt = FitsCtxt(cxt)

    //TD
    newCtxt.extraInfo =  TableRangeValue.getRangeAsString("TD",tdMin,tdMax)
    if (!isValid(v,tdMin,tdMax,isTlRange = false,cxt)) return false

    //TL
    newCtxt.extraInfo =  TableRangeValue.getRangeAsString("TL",tdMin,tdMax)
    isValid(v,tlMin,tlMax,isTlRange = true,cxt)
  }
  //---------------------------------------------------------------------------
  private def isValid(v: Double
                     , minIncluded: Option[Double]
                     , maxIncluded: Option[Double]
                     , isTlRange: Boolean
                     , cxt: FitsCtxt): Boolean = {

    if (minIncluded.isDefined) {
      val min = minIncluded.get
      if (v < min) {
        if (isTlRange) generateDisconfWarnTableRange(v, cxt, SectionName.SECTION_7_3_2)
        else {
          generateDisconfFatalTableRange(v, cxt, SectionName.SECTION_7_3_2)
        }
      }
    }

    if (maxIncluded.isDefined) {
      val max = maxIncluded.get
      if (v > max) {
        if (isTlRange) generateDisconfWarnTableRange(v, cxt, SectionName.SECTION_7_3_2)
        else {
          generateDisconfFatalTableRange(v, cxt, SectionName.SECTION_7_3_2)
        }
      }
    }
    true
  }
  //---------------------------------------------------------------------------
  def getDescription() = {
    List(
        leftPadding(tdMin.getOrElse("-").toString, TABLE_COL_INFO_TDMIN_MAX_CHAR_SIZE)
      , leftPadding(tdMax.getOrElse("-").toString, TABLE_COL_INFO_TDMAX_MAX_CHAR_SIZE)
      , leftPadding(tlMin.getOrElse("-").toString, TABLE_COL_INFO_TLMIN_MAX_CHAR_SIZE)
      , leftPadding(tlMax.getOrElse("-").toString, TABLE_COL_INFO_TLMAX_MAX_CHAR_SIZE)
    )
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file TableRangeValue.scala
//=============================================================================
