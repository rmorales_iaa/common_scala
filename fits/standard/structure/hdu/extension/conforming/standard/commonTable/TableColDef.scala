/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Nov/2022
 * Time:  11h:45m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable
//=============================================================================
import com.common.fits.standard.block.record.RecordCharacter.RecordString
import com.common.fits.standard.block.record.RecordNumber.{RecordFloat, RecordInteger}
import com.common.fits.standard.Keyword._
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.dataType.DataType
import com.common.fits.standard.dataType.DataType.{DATA_TYPE_FLOAT_64, DATA_TYPE_INT_64}
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.AsciiTableColDef
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.tform._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.BinTableColDef
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.BinTableDataType.BIN_TABLE_DATA_TYPE_A
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tdim.BinTableTdim
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tform.BinTableTform
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.CommonTable.{ByteArr, generateDisconfFatal, getNumericValue}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.tdisp.Tdisp
import com.common.util.string.MyString.{leftPadding}
//=============================================================================
object TableColDef {
  //---------------------------------------------------------------------------
  //table col info max char size
  final val TABLE_COL_INFO_COL_INDEX_MAX_CHAR_SIZE        = 6
  final val TABLE_COL_INFO_TNAME_MAX_CHAR_SIZE            = 24
  final val TABLE_COL_INFO_TBCOL_MAX_CHAR_SIZE            = 8
  final val TABLE_COL_INFO_TFORM_MAX_CHAR_SIZE            = 8
  final val TABLE_COL_INFO_TUNIT_MAX_CHAR_SIZE            = 8
  final val TABLE_COL_INFO_TSCALE_MAX_CHAR_SIZE           = 8
  final val TABLE_COL_INFO_TZERO_MAX_CHAR_SIZE            = 8
  final val TABLE_COL_INFO_TNULL_MAX_CHAR_SIZE            = 8
  final val TABLE_COL_INFO_TDISP_MAX_CHAR_SIZE            = 8
  final val TABLE_COL_INFO_TDMIN_MAX_CHAR_SIZE            = 8
  final val TABLE_COL_INFO_TDMAX_MAX_CHAR_SIZE            = 8
  final val TABLE_COL_INFO_TLMIN_MAX_CHAR_SIZE            = 8
  final val TABLE_COL_INFO_TLMAX_MAX_CHAR_SIZE            = 8

  final val TABLE_COL_INFO_TDIM_MAX_CHAR_SIZE             = 8

  final val TABLE_ASCII_COL_HEADER                        =
    leftPadding("index",TABLE_COL_INFO_COL_INDEX_MAX_CHAR_SIZE) +
    leftPadding("name",TABLE_COL_INFO_TNAME_MAX_CHAR_SIZE+1) +
    leftPadding("pos",TABLE_COL_INFO_TBCOL_MAX_CHAR_SIZE+1) +
    leftPadding("form",TABLE_COL_INFO_TFORM_MAX_CHAR_SIZE+1) +
    leftPadding("unit",TABLE_COL_INFO_TUNIT_MAX_CHAR_SIZE+1) +
    leftPadding("scale",TABLE_COL_INFO_TSCALE_MAX_CHAR_SIZE+1) +
    leftPadding("zero",TABLE_COL_INFO_TZERO_MAX_CHAR_SIZE+1) +
    leftPadding("null",TABLE_COL_INFO_TNULL_MAX_CHAR_SIZE+1) +
    leftPadding("disp",TABLE_COL_INFO_TDISP_MAX_CHAR_SIZE+1) +
    leftPadding("tdMin",TABLE_COL_INFO_TDMIN_MAX_CHAR_SIZE+1) +
    leftPadding("tdMax",TABLE_COL_INFO_TDMAX_MAX_CHAR_SIZE+1) +
    leftPadding("tlMin",TABLE_COL_INFO_TLMIN_MAX_CHAR_SIZE+1) +
    leftPadding("tlMax",TABLE_COL_INFO_TLMAX_MAX_CHAR_SIZE+1)

  final val TABLE_BIN_COL_HEADER = TABLE_ASCII_COL_HEADER +
    leftPadding("tDim",TABLE_COL_INFO_TDIM_MAX_CHAR_SIZE)
  //---------------------------------------------------------------------------
  def toAsciiTableColDef(a: Array[TableColDef]) =
    a map (_.asInstanceOf[AsciiTableColDef])
  //---------------------------------------------------------------------------
  def toBinTableColDef(a: Array[TableColDef]) =
    a map (_.asInstanceOf[BinTableColDef])
  //---------------------------------------------------------------------------
  //(RecordMap,CharSize)
  def filldMap(recordMap: RecordMap
               , colIndex: Long //index of the column, starting with index 1
               , startingPos: Long //offset of the column in characters
               , form: String
               , name: Option[String] = None  //ttype
               , unit: Option[String] = None
               , scale: Option[Double] = None
               , zero: Option[Double] = None
               , nullValue: Option[String] = None //it will be converted to the proper data type defined in 'form'
               , disp: Option[String] = None
               , tdMin: Option[Double] = None //it will be converted to the proper data type defined in 'form'
               , tdMax: Option[Double] = None //it will be converted to the proper data type defined in 'form'
               , tlMin: Option[Double] = None //it will be converted to the proper data type defined in 'form'
               , tlMax: Option[Double] = None //it will be converted to the proper data type defined in 'form'
               , tDim: Option[String] = None //only applied on binary tables
               , isAsciiTable: Boolean
              ): Long = {

    val tForm =
      if (isAsciiTable) AsciiTableTform.build(form,colIndex,FitsCtxt()).getOrElse(return 0)
      else BinTableTform.build(form,colIndex,FitsCtxt()).getOrElse(return 0)

    var recordID = -1L
    //-------------------------------------------------------------------------
    def getNextID(): Long = {
      recordID += 1
      recordID
    }
    //-------------------------------------------------------------------------
    //TBCOL (mandatory,column start position)
    if (isAsciiTable) recordMap.append(RecordInteger(KEYWORD_TBCOL + colIndex, startingPos, id = getNextID()))

    //TFORM (mandatory)
    recordMap.append(RecordString(KEYWORD_TFORM + colIndex, form, id = getNextID()))

    //TTYPE (name)
    if (name.isDefined) recordMap.append(RecordString(KEYWORD_TTYPE + colIndex, name.get, id = getNextID()))

    //TUNIT
    if (unit.isDefined) recordMap.append(RecordString(KEYWORD_TUNIT + colIndex, unit.get, id = getNextID()))

    //TSCAL
    if (scale.isDefined) recordMap.append(RecordFloat(KEYWORD_TSCAL + colIndex, scale.get, id = getNextID()))

    //TZERO
    if (zero.isDefined) recordMap.append(RecordFloat(KEYWORD_TZERO + colIndex, zero.get, id = getNextID()))

    //TNULL
    if (nullValue.isDefined) recordMap.append(RecordString(KEYWORD_TNULL + colIndex, nullValue.get, id = getNextID()))

    //TDISP
    if (disp.isDefined) recordMap.append(RecordString(KEYWORD_TDISP + colIndex, disp.get, id = getNextID()))

    //TDIM
    if (tDim.isDefined) recordMap.append(RecordString(KEYWORD_TDIM + colIndex, tDim.get, id = getNextID()))

    //TDMIN
    if (tdMin.isDefined) recordMap.append(RecordFloat(KEYWORD_TDMIN + colIndex, tdMin.get, id = getNextID()))

    //TDMAX
    if (tdMax.isDefined) recordMap.append(RecordFloat(KEYWORD_TDMAX + colIndex, tdMax.get, id = getNextID()))

    //TLMIN
    if (tlMin.isDefined) recordMap.append(RecordFloat(KEYWORD_TLMIN + colIndex, tlMin.get, id = getNextID()))

    //TLMAX
    if (tlMax.isDefined) recordMap.append(RecordFloat(KEYWORD_TLMAX + colIndex, tlMax.get, id = getNextID()))

    tForm match {
      case t: AsciiTableTform => t.charSize
      case t: BinTableTform   =>  t.getByteSize
    }
  }
  //---------------------------------------------------------------------------
  private def getLinearTransformationValue(recordMap: RecordMap
                                           , isAciiTable: Boolean
                                           , colIndex: Long
                                           , form: TableColForm
                                           , keyword: String
                                           , sectionName: String
                                           , ctxt: FitsCtxt): (Boolean,Option[Double]) = {
    val optScale = getNumericValue(recordMap
      , keyword + colIndex
      , DATA_TYPE_FLOAT_64
      , sectionName
      , ctxt)
    if (!optScale._1) return (false,None)
    if (!optScale._2.isDefined) return (true,None)

    if (isAciiTable) {
      if (form.isInstanceOf[AsciiTableTformCharacter]) {
        generateDisconfFatal(
          ctxt
          , s"Structure: ${ctxt.structureID} the keyword: '$keyword$colIndex' can not be used in TFORM of type 'character'"
          , sectionName = sectionName)
      }
    }
    else { //binary table
      val t = form.asInstanceOf[BinTableTform]
      if (!BinTableTform.isValidTypeForLinearTransformation(t)) {
        generateDisconfFatal(
          ctxt
          , s"Structure: ${ctxt.structureID} the keyword: '$keyword$colIndex' can not be used in TFORM of type: '${t.getValue()}'"
          , sectionName = sectionName)
       }
    }

    (true,Some(optScale._2.get.toDouble))
  }
  //---------------------------------------------------------------------------
  def getTNull(recordMap: RecordMap
               , isAciiTable: Boolean
               , colIndex: Long
               , dataType: DataType[_]
               , sectionName: String
               , ctxt: FitsCtxt): (Boolean,Option[String]) = {

    if (!recordMap.contains(KEYWORD_TNULL + colIndex)) return (true,None)
    if (!isAciiTable) {
      if (!BinTableTform.isValidTypeForTnull(dataType.id)) {
        generateDisconfFatal(
          ctxt
          , s"Structure: ${ctxt.structureID} the keyword: '$KEYWORD_TNULL$colIndex' can not be used with data type:'${dataType.id}'"
          , sectionName = sectionName)
      }
    }

    if (isAciiTable) {
      val nullValue = getNumericValue(
        recordMap
        , KEYWORD_TNULL + colIndex
        , dataType
        , sectionName
        , ctxt)
      if (!nullValue._1) (false, None)
      else (true, nullValue._2)
    }
    else {
      val nullValue = getNumericValue(
        recordMap
        , KEYWORD_TNULL + colIndex
        , DATA_TYPE_INT_64
        , sectionName
        , ctxt)
      if (!nullValue._1) (false, None)
      else (true, nullValue._2)
    }
  }
  //---------------------------------------------------------------------------
  def buildColDefSeq(recordMap: RecordMap
                     , isAciiTable: Boolean
                     , sectionName: String
                     , ctxt: FitsCtxt = FitsCtxt()): Option[Array[TableColDef]] = {

    val colNameSet  = scala.collection.mutable.Set[String]()
    val colNameCaseInsensitive = scala.collection.mutable.Set[String]()

    //TFIELDS (mandatory,column count)
    val colCount = recordMap.getTfields()

    var binaryColStartPos = 0L  //first position of the column

    Some((for (colIndex <- 1L to colCount) yield {

      //TFORM (mandatory)
      val formString = recordMap.getOptTableFieldValue[String](KEYWORD_TFORM + colIndex).get
      val form =
        if (isAciiTable)
          AsciiTableTform.build(formString,colIndex,ctxt).getOrElse(return None)
        else
          BinTableTform.build(formString,colIndex,ctxt).getOrElse(return None)

      //TTYPE
      val name = recordMap.getOptTableFieldValue[String](KEYWORD_TTYPE + colIndex)
      if (!CommonTable.isValidTtype(name
                                   , sectionName
                                   , colIndex
                                   , colNameSet
                                   , ctxt)) return None

      if (name.isDefined) {
         if (colNameCaseInsensitive.contains(name.get.toLowerCase)) {
           generateDisconfFatal(ctxt, message = s"the value: '${name.get}' of keyword: '$KEYWORD_TTYPE$colIndex' is duplicated", sectionName = sectionName)
         }
         else colNameCaseInsensitive += name.get
      }

      val dataType = {
        if (isAciiTable) AsciiTableTform.getDataType(form.asInstanceOf[AsciiTableTform])
        else form.asInstanceOf[BinTableTform].binDataType.dataType
      }

      //TUNIT
      val unit = recordMap.getOptTableFieldValue[String](KEYWORD_TUNIT + colIndex)
      if(!CommonTable.isValidTunit(unit,colIndex,ctxt)) return None

      //TSCAL
      val (scaleResult,optScale) =
        getLinearTransformationValue(
            recordMap
          , isAciiTable
          , colIndex
          , form
          , KEYWORD_TSCAL
          , sectionName
          , ctxt)
       if (!scaleResult) return scala.None

      //TZERO
      val (zeroResult, optZero) =
        getLinearTransformationValue(
          recordMap
          , isAciiTable
          , colIndex
          , form
          , KEYWORD_TZERO
          , sectionName
          , ctxt)
      if (!zeroResult) return scala.None

      //TNULL
      val (nullValueResult, optNullValue) = getTNull(
        recordMap
        , isAciiTable
        , colIndex
        , dataType
        , sectionName
        , ctxt)
      if (!nullValueResult) return scala.None

      //TDISP
      val dispString = recordMap.getOptTableFieldValue[String](KEYWORD_TDISP + colIndex)
      val formIsInteger =
        if (isAciiTable) form.isInstanceOf[AsciiTableTformInteger]
        else BinTableTform.isValidIntegerTdispDataType(form.asInstanceOf[BinTableTform])
      val disp = CommonTable.isValidTdisp(
          dispString
        , sectionName
        , colIndex
        , formIsInteger
        , isAciiTable
        , ctxt)
      if (!disp._1) return None

      //table range
      val tableRangeValue =
        if (form.isInstanceOf[AsciiTableTformCharacter]) (true,None)
        else {
          TableRangeValue.build(
            recordMap
            , colIndex
            , form
            , formString
            , isAciiTable
            , ctxt)
        }
      if (!tableRangeValue._1) return None

      //TDIM
      val dim =
        if (!isAciiTable && recordMap.contains(KEYWORD_TDIM + colIndex)) {
         BinTableTdim.build(recordMap: RecordMap
                  , colIndex: Long
                  , ctxt: FitsCtxt)
        }
        else None

      //finally, process the column definition
      if (isAciiTable)
        AsciiTableColDef(
            colIndex
          , recordMap.getFirstValue[Long](KEYWORD_TBCOL + colIndex).get //TBCOL (mandatory)
          , name
          , form.asInstanceOf[AsciiTableTform]
          , unit
          , optScale
          , optZero
          , optNullValue
          , disp._2
          , tableRangeValue._2)
      else {
        val r = BinTableColDef(
          colIndex
          , binaryColStartPos
          , name
          , form.asInstanceOf[BinTableTform]
          , unit
          , optScale
          , optZero
          , optNullValue
          , disp._2
          , tableRangeValue._2
          , dim)

        //update the start position of the next field
        binaryColStartPos += r.form.getByteSize
        r
      }
    }).toArray)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import TableColDef._
trait TableColDef  {
  //---------------------------------------------------------------------------
  val colIndex: Long //index of the column, starting with index 1
  val position: Long //starting character position
  val name: Option[String]
  val form: TableColForm
  val unit: Option[String]
  val scale: Option[Double]
  val zero: Option[Double]
  val nullValue: Option[String]
  val disp: Option[Tdisp]
  val tableRangeValue: Option[TableRangeValue]
  val dataType: DataType[_]
  //---------------------------------------------------------------------------
  def isTformTypeCharacter = form.isInstanceOf[AsciiTableTformCharacter]
  //---------------------------------------------------------------------------
  def isValueInRange(s: ByteArr, ctxt: FitsCtxt): Boolean = {
    if (tableRangeValue.isEmpty) return true

    this match {
      case x: BinTableTform => x.binDataType.id == BIN_TABLE_DATA_TYPE_A
      case _ =>
        form match {
          case _: AsciiTableTformCharacter => true
          case _ =>
            val newCtxt = FitsCtxt(ctxt)
            newCtxt.info = KEYWORD_TFORM + colIndex
            newCtxt.position = position
            tableRangeValue.get.isValid(form.dataType.getFirstValueAsDouble(s), newCtxt)
        }
    }
  }
  //---------------------------------------------------------------------------
  def isValueInRange(value: Double, ctxt: FitsCtxt): Boolean = {
    if (tableRangeValue.isEmpty) return true
    val newCtxt = FitsCtxt(ctxt)
    newCtxt.info = KEYWORD_TFORM + colIndex
    newCtxt.position = position
    tableRangeValue.get.isValid(value, newCtxt)
  }
  //---------------------------------------------------------------------------
  def getDescription(): List[String] = {
    List(
       leftPadding(colIndex.toString,TABLE_COL_INFO_COL_INDEX_MAX_CHAR_SIZE)
     , leftPadding(name.getOrElse("-"),TABLE_COL_INFO_TNAME_MAX_CHAR_SIZE)
     , leftPadding(position.toString,TABLE_COL_INFO_TBCOL_MAX_CHAR_SIZE)
     , leftPadding(form.getValue(),TABLE_COL_INFO_TFORM_MAX_CHAR_SIZE)
     , leftPadding(unit.getOrElse("-"),TABLE_COL_INFO_TUNIT_MAX_CHAR_SIZE)
     , leftPadding(scale.getOrElse("-").toString,TABLE_COL_INFO_TSCALE_MAX_CHAR_SIZE)
     , leftPadding(zero.getOrElse("-").toString,TABLE_COL_INFO_TZERO_MAX_CHAR_SIZE)
     , leftPadding(nullValue.getOrElse("-"),TABLE_COL_INFO_TNULL_MAX_CHAR_SIZE)
     , leftPadding(if (disp.isDefined) disp.get.getString(colIndex) else "-",TABLE_COL_INFO_TDISP_MAX_CHAR_SIZE)) ++
      (if (tableRangeValue.isDefined) tableRangeValue.get.getDescription()
       else Array(leftPadding("",TABLE_COL_INFO_TDMIN_MAX_CHAR_SIZE +
                                  TABLE_COL_INFO_TDMAX_MAX_CHAR_SIZE +
                                  TABLE_COL_INFO_TLMIN_MAX_CHAR_SIZE +
                                  TABLE_COL_INFO_TLMAX_MAX_CHAR_SIZE)))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file TableColDef.scala
//=============================================================================
