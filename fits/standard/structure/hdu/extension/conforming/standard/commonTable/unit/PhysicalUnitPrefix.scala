/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Nov/2022
 * Time:  18h:49m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.unit
//=============================================================================
//=============================================================================
//Submultipliers
sealed case class Deci()    extends Submultiplier{val n="deci";val i="d";val m=10e-1}
sealed case class Centi()   extends Submultiplier{val n="centi";val i="c";val m=10e-2}
sealed case class Milli()   extends Submultiplier{val n="milli";val i="m";val m=10e-3}
sealed case class Micro()   extends Submultiplier{val n="micro";val i="i";val m=10e-6}
sealed case class Nano()    extends Submultiplier{val n="nano";val i="n";val m=10e-9}
sealed case class Pico()    extends Submultiplier{val n="pico";val i="p";val m=10e-12}
sealed case class Femto()   extends Submultiplier{val n="femto";val i="f";val m=10e-15}
sealed case class Atto()    extends Submultiplier{val n="atto";val i="u";val m=10e-18}
sealed case class Zepto()   extends Submultiplier{val n="zepto";val i="z";val m=10e-21}
sealed case class Yocto()   extends Submultiplier{val n="yocto";val i="y";val m=10e-24}
//=============================================================================
//Multipliers
sealed case class Deca()    extends Multiplier{val n="deca";val i="da";val m=10e1}
sealed case class Hecto()   extends Multiplier{val n="hecto";val i="h";val m=10e2}
sealed case class Kilo()    extends Multiplier{val n="kilo";val i="k";val m=10e3}
sealed case class Mega()    extends Multiplier{val n="mega";val i="M";val m=10e6}
sealed case class Giga()    extends Multiplier{val n="giga";val i="G";val m=10e9}
sealed case class Tera()    extends Multiplier{val n="tera";val i="T";val m=10e12}
sealed case class Peta()    extends Multiplier{val n="peta";val i="P";val m=10e15}
sealed case class Exa()     extends Multiplier{val n="exa";val i="E";val m=10e18}
sealed case class Zetta()   extends Multiplier{val n="zetta";val i="Z";val m=10e21}
sealed case class Yotta()   extends Multiplier{val n="yotta";val i="Y";val m=10e24}
//=============================================================================
sealed case class NoneUnit()   extends PhysicalUnitPrefix{val n="NONE";val i="NONE";val m=0}
//=============================================================================
trait PhysicalUnitPrefix {
  val n: String  //name
  val i: String  //m2_id
  val m: Double  //multiplier
}
//=============================================================================
trait Submultiplier extends PhysicalUnitPrefix
//=============================================================================
trait Multiplier extends PhysicalUnitPrefix
//=============================================================================
object PhysicalUnitPrefix {
  //---------------------------------------------------------------------------
  private val noneUnitSingleton  = NoneUnit()
  //---------------------------------------------------------------------------
  def apply(s:String): PhysicalUnitPrefix = {
    submultiplerSeq.foreach (prefix=> if (prefix.i == s) return prefix )
    multiplerSeq.foreach (prefix=> if (prefix.i == s) return prefix )
    noneUnitSingleton
  }
  //---------------------------------------------------------------------------
  final val submultiplerSeq: Array[Submultiplier] = Array(
      Deci()
    , Centi()
    , Milli()
    , Micro()
    , Nano()
    , Pico()
    , Femto()
    , Atto()
    , Zepto()
    , Yocto()
  )
  //---------------------------------------------------------------------------
  final val multiplerSeq: Array[Multiplier] = Array(
      Deca()
    , Hecto()
    , Kilo()
    , Mega()
    , Giga()
    , Tera()
    , Peta()
    , Exa()
    , Zetta()
    , Yotta()
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file PhysicalUnitPrefix.scala
//=============================================================================