/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Nov/2022
 * Time:  20h:34m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.tdisp.parser
//=============================================================================
import fastparse._
import NoWhitespace._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.tform.parser.TformParser.{dotD, integer, w}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.tdisp._
//=============================================================================

import com.common.logger.MyLogger
//=============================================================================
//=============================================================================
object ParserTdisp extends MyLogger {
  //---------------------------------------------------------------------------
  private def m[_: P]: P[Int] = integer
  //---------------------------------------------------------------------------
  private def dotM[_: P]: P[Int] = ("." ~ m).map { s => s }
  //---------------------------------------------------------------------------
  private def e[_: P]: P[Int] = integer
  //---------------------------------------------------------------------------
  private def Ee[_: P]: P[Int] = ("E" ~ e).map { s=> s }
  //---------------------------------------------------------------------------
  private def tDispCharacter[_: P]: P[Tdisp]        =  "A" ~ w.map( s=> TdispCharacter(s) )
  //---------------------------------------------------------------------------
  private def tDispLogical[_: P]: P[Tdisp]          =  "L" ~ w.map(s => TdispLogical(s))
  //---------------------------------------------------------------------------
  private def tDispInteger[_: P]: P[Tdisp]          =  "I" ~ (w ~ dotM.?).map { case (w,m) => TdispInteger(w,m.getOrElse(0)) }
  //---------------------------------------------------------------------------
  private def tDispBinary[_: P]: P[Tdisp]           =  "B" ~ (w ~ dotM.?).map { case (w,m) => TdispBinary(w,m.getOrElse(0)) }
  //---------------------------------------------------------------------------
  private def tDispOctal[_: P]: P[Tdisp]            =  "O"~ (w ~ dotM.?).map { case (w,m) => TdispOctal(w,m.getOrElse(0)) }
  //---------------------------------------------------------------------------
  private def tDispHexadecimal[_: P]: P[Tdisp]      =  "Z"~ (w ~ dotM.?).map { case (w,m) => TdispHexadecimal(w,m.getOrElse(0)) }
  //---------------------------------------------------------------------------
  private def tDispFloatFixed[_: P]: P[Tdisp]       =  "F"~ (w ~ dotD).map { case (w,d) => TdispFloatFixed(w,d) }
  //---------------------------------------------------------------------------
  private def tDispFloatExponential[_: P]: P[Tdisp] =  "E"~ (w ~ dotD ~ Ee.?).map { case (w,d,eE) => TdispFloatExponential(w,d,eE.getOrElse(0)) }
  //---------------------------------------------------------------------------
  private def tDispEngineering[_: P]: P[Tdisp]      =  "EN"~ (w ~ dotD ~ Ee.?).map { case (w,d,eE) => TdispEngineeringExponential(w,d,eE.getOrElse(0)) }
  //---------------------------------------------------------------------------
  private def tDispScientific[_: P]: P[Tdisp]       =  "ES"~ (w ~ dotD ~ Ee.?).map { case (w,d,eE) => TdispScientificExponential(w,d,eE.getOrElse(0)) }
  //---------------------------------------------------------------------------
  private def tDispGeneral[_: P]: P[Tdisp]          =  "G"~ (w ~ dotD ~ Ee.?).map { case (w,d,eE) => TdispGeneralExponential(w,d,eE.getOrElse(0)) }
  //---------------------------------------------------------------------------
  private def tDispDouble[_: P]: P[Tdisp]           =  "D"~ (w ~ dotD ~ Ee.?).map { case (w,d,eE) => TdispDoubleExponential(w,d,eE.getOrElse(0)) }
  //---------------------------------------------------------------------------
  private def parseInput[_: P] : P[Tdisp]  =
    tDispCharacter |
    tDispLogical |
    tDispInteger |
    tDispBinary |
    tDispOctal |
    tDispHexadecimal |
    tDispFloatFixed |
    tDispFloatExponential |
    tDispEngineering  |
    tDispScientific |
    tDispGeneral  |
    tDispDouble
  //---------------------------------------------------------------------------
  private def asciiTableChecking(r: Tdisp): Tdisp = {
    r match {
      case _: TdispLogical => TdispError(s"Logical display is not allowed in ASCII tables")
      case x: TdispEngineeringExponential =>
        if (x.exponentFractionDigitCount > 0 && (x.exponentFractionDigitCount % 3) != 0)
          TdispError(s"Engineering display must have an exponent multiple of theree")
        else r
        case _ => r
      }
  }
  //---------------------------------------------------------------------------
  private def binaryTableChecking(r: Tdisp): Tdisp = {
    r match {
      case x: TdispEngineeringExponential =>
        if (x.exponentFractionDigitCount > 0 && (x.exponentFractionDigitCount % 3) != 0)
          TdispError(s"Engineering display must have an exponent multiple of theree")
        else r
      case _ => r
    }
  }
  //---------------------------------------------------------------------------
  //ASCII table and binary table share the same format string, except that 'TdispLogical' is available only in the binary table
  def run(s: String,isAciiTable: Boolean): Tdisp =
    parse(s, parseInput(_)) match {
      case Parsed.Success(r, index) =>
        if (index == s.size) {

          //check zero padding size
          r match {
            case x: TdispCommonInteger =>
              if (x.zeroPaddingMinCount > x.charSize) return  TdispError(s"The number of zero padding count: ${x.zeroPaddingMinCount} must be less than the char size: ${x.charSize}")
            case _ =>
          }
          //additional checks
          isAciiTable match {
            case true =>   asciiTableChecking(r)
            case false =>  binaryTableChecking(r)
          }
        }
        else {
          val parsedInput = s.take(index)
          val remain = s.drop(index)
          TdispError(s"Error parsing input '$s' at position (first position 0): $index. Parsed input: '$parsedInput' Remain input: '$remain'")
        }
      case Parsed.Failure(expected, index, failure) =>
        TdispError(s"Error parsing the input at index: $index. Expected: '$expected' . Error: '${failure.trace().longAggregateMsg}'")
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ParserTdisp.scala
//=============================================================================