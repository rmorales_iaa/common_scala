/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  30/Nov/2022
 * Time:  13h:24m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable
//=============================================================================
import com.common.fits.standard.dataType.DataType
import com.common.fits.standard.fits.FitsCtxt
//=============================================================================
//=============================================================================
trait TableColForm {
  //---------------------------------------------------------------------------
  val dataType: DataType[_]
  //---------------------------------------------------------------------------
  def getDataTypeByteSize = dataType.byteSize
  //---------------------------------------------------------------------------
  def getDataTypeOctalSize = dataType.octalSize
  //---------------------------------------------------------------------------
  def getValue(): String
  //---------------------------------------------------------------------------
  //used in ASCII tables
  def getValue(s: String
               , colIndex: Long
               , ctxt: FitsCtxt): Option[String]
  //---------------------------------------------------------------------------
  //used in binary tables
  def getValue(byteSeq: Array[Byte]
               , colIndex: Long
               , ctxt: FitsCtxt): Option[Array[_]]
  //---------------------------------------------------------------------------
  //used in ASCII tables
  def format(s:String, ctxt: FitsCtxt): String
  //---------------------------------------------------------------------------
  //used in binary tables
  def format(byteSeq: Array[Byte], ctxt: FitsCtxt): String
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file TableColForm.scala
//=============================================================================
