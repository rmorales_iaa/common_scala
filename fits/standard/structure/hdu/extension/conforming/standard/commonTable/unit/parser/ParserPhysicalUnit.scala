/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Nov/2022
 * Time:  20h:34m
 * Description:
 * https://nxg.me.uk/dist/unity/c-docs/html/index.html
 * https://nxg.me.uk/dist/unity/
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.unit.parser
//=============================================================================
import fastparse._
import NoWhitespace._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.tform.parser.TformParser.digit
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.tform.parser.TformParser.integer
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.unit._
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
//=============================================================================
object ParserPhysicalUnit extends MyLogger {
  //
  //---------------------------------------------------------------------------
  //basic tokens
  def dot[_: P]: P[String] = P(".").!

  private def division[_: P]: P[String] = P("/").!

  def sign[_: P] = P("+" | "-").!

  private def signedInteger[_: P] = sign ~ integer
  def optSignedInteger[_: P] = sign.? ~ integer
  private def enclosedOptSignedInteger[_: P] = "(" ~ optSignedInteger ~ ")"

  private def decimalNumber[_: P] = digit.? ~ dot ~ integer
  private def optSignedDecimalNumber[_: P] = sign.? ~ decimalNumber
  private def enclosedOptSignedDecimalNumber[_: P] = "(" ~ optSignedDecimalNumber ~ ")"

  private def rationalNumber[_: P] = integer ~ division ~ integer
  private def optSignedRationalNumber[_: P] = sign.? ~ rationalNumber
  private def enclosedOptSignedRationalNumber[_: P] = "(" ~ optSignedRationalNumber ~ ")"

  private def arithmeticUnitOperator[_: P]: P[String] =  (" " | "*" | "." | "/").!
  private def powerOperator[_: P]: P[String] = ("**" | "^" | "").!

  private def appliedFunctionUnitOperator[_: P]: P[String] = ("log" | "ln" | "exp" | "sqrt").!
  private def numericMultiplierBase[_: P]: P[String] = "10".!
  //---------------------------------------------------------------------------
  //submultiples and multiples
  private def scalePrefix[_: P]: P[String] =
    ("da" | "h" | "k" | "M" | "G" | "T" | "P" | "E" | "Z" | "Y" |   //prefixes for multiples
      "d" | "c" | "m" | "i" | "n" | "p" | "f" | "u" | "z" | "y" ).! //prefixes for submultiples
  //---------------------------------------------------------------------------
  //physical unit name, divided in groups to avoid ambiguities ( i.e: mas (milli arc sec) can be also a (milli year second))
  //units sort alphabetically. Some units that are contained in other (i.e: H, Hz), then the bigger one appear first
  private def physicalUnitScalePrefix[_: P]: P[String] =
    ("A" | "C" | "F" | "G" | "Hz" | "H" | "Jy" | "J" | "K" | "N" | "Ohm" | "Pa" | "R" | "S" | "T" | "V" |
     "Wb" | "W" | "a" | "barn" | "bit" | "byte" | "cd" | "eV" | "g" | "lm" | "lx" | "mag" | "mol" | "m" |
     "pc" | "rad" | "sr" | "s" | "yr").!

  private def physicalUnitNoneScalePrefix[_: P]: P[String] =
    ("AU" | "Angstrom" | "D" | "Ry" | "Sun" | "adu" | "arcmin" | "arcsec" | "beam" | "bin" |
     "chan" | "count" | "ct" | "deg" | "d" | "erg" | "h" | "i" | "lyr" | "mas" | "min" | "photon" | "ph" |
     "pixel" | "pix" | "solLum" | "solMass" | "solRad" | "voxel").!

  //identify all the possibles unique characters that can exist at the end of a valid physical unit
  private def physicalUnitFollowers[_: P] = P(
    End | //end of the input
      arithmeticUnitOperator |
      "^" | sign | digit | //powerUnitOperator
      "(" | //log,ln,exp,sqrt
      ")"   //EnclosedPhysicalUnitExpression
  )
  //---------------------------------------------------------------------------
  //unit power
  private def powerExponent[_: P]: P[String] = (
    optSignedInteger |
    enclosedOptSignedInteger |
    optSignedDecimalNumber |
    enclosedOptSignedDecimalNumber |
    enclosedOptSignedRationalNumber).!

  //---------------------------------------------------------------------------
  //physical unit numeric multiplier
  private def numericMultiplierOperator[_: P]: P[String] = ("**" | "^" ).!

  //unicode 0xB1 (177 dec)
  //extendend ASCII table code 0xF1  (241 dec)
  //https://stackoverflow.com/questions/22273046/how-to-print-the-extended-ascii-code-in-java-from-integer-value
  //Dec Hex Unicode     Char    Description
  //--- --- -------     ----    -----------------------------------
  //241 F1  U+00B1      ±   plus-minus sign
  //This character is not allowed character (e.g: 0x20 - 0x7E) as it is indicated in FITS standard 4.0, see section '3.3.1 Primary header'
  //But it is included in section '4.3.1 Construction of units strigs'. Probably because this section is a is shorten
  //copy paste of the section '4. Specification of units' of paper :
  //"Representation of world coordinates in FITS" E.W. Greisen, M.R. Calabretta. A&A 395,1061-1075 (2002)

  private def numericMultiplierOperatorPlusMinus[_: P]: P[String] = "\u00B1".!

  private def numericMultiplier[_: P]: P[String] =
    (numericMultiplierBase ~ numericMultiplierOperator ~ (optSignedInteger | enclosedOptSignedInteger)).! |
    (numericMultiplierBase ~ numericMultiplierOperatorPlusMinus ~ (signedInteger | enclosedOptSignedInteger)).!
  //---------------------------------------------------------------------------
  //physical unit
  //in case of the same unit name and prefix name, unit has preference. Then the parser
  //uses "Positive Lookahead" see https://com-lihaoyi.github.io/fastparse/#PositiveLookahead
  //to not consume any input
  private def basicPhysicalUnit[_: P]: P[PhysicalUnitExpression] =
    ((physicalUnitNoneScalePrefix | physicalUnitScalePrefix) ~ &(physicalUnitFollowers)).map {u => BasicPUE(None, PhysicalUnit(u))} |
    (scalePrefix ~ physicalUnitScalePrefix ~ &(physicalUnitFollowers)).map { case (p, u) => BasicPUE(Some(PhysicalUnitPrefix(p)), PhysicalUnit(u)) }
  //---------------------------------------------------------------------------
  //physical unit expression (compound units)
  private def appliedFunctionExpression[_: P]: P[PhysicalUnitExpression] =
    (appliedFunctionUnitOperator ~ "(" ~ optEnclosedExpression ~ ")").map {
      case (functionOperator, rightUnitExpression) =>
        AppliedFunctionPUE(functionOperator, rightUnitExpression)
    }
  private def powerExpression[_: P]: P[PhysicalUnitExpression] =
    (basicPhysicalUnit ~ powerOperator ~ powerExponent).map { case (u,o,e)=> PowerPUE(Some(u), o,e)}
  private def basicExpression[_: P]: P[PhysicalUnitExpression] =
    powerExpression |
    appliedFunctionExpression  |
    basicPhysicalUnit

  private def expression[_: P]: P[PhysicalUnitExpression] = (basicExpression ~ optArithmeticExpression).map { case (le,re)=>
    if (re.isDefined) ArithmeticPUE(Some(le), re.get._1, re.get._2)
    else le
  }
  private def optArithmeticExpression[_: P]: P[Option[(String,PhysicalUnitExpression)]] =
    (arithmeticUnitOperator ~ optEnclosedExpression).?. map { s=> s }

  private def optEnclosedExpression[_: P]: P[PhysicalUnitExpression] =
    ("(" ~ optEnclosedExpression  ~ ")" ~ optArithmeticExpression).map{  case (le,re) =>
      if (re.isDefined) EnclosedPUE(Some(ArithmeticPUE(Some(le), re.get._1, re.get._2)))
      else EnclosedPUE(Some(le))
    } |
    expression
  private def parseInput[_: P]: P[PhysicalUnitExpression] = (numericMultiplier.?  ~ optEnclosedExpression).map { case (nm,e) =>
    if(nm.isDefined) e.setNumericMultiplier(nm.get)
    e
  }
  //---------------------------------------------------------------------------
  def parseInput(s: String): (Option[PhysicalUnitExpression], String) = {
    parse(s, parseInput(_)) match {
      case Parsed.Success(r, index) =>
        if (index == s.size)
          (Some(r), "")
        else {
          val parsedInput = s.take(index)
          val remain = s.drop(index)
          (None, s"Error parsing input '$s' at position (first position 0): $index. Parsed input: '$parsedInput' Remain input: '$remain'")
        }
      case Parsed.Failure(expected, index, failure) =>
        (None, s"Error parsing the input at index: $index. Expected: '$expected' . Error: '${failure.trace().longAggregateMsg}'")
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ParserPhysicalUnit.scala
//=============================================================================