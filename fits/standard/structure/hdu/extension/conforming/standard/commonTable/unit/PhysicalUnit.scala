/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Nov/2022
 * Time:  17h:28m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.unit
//=============================================================================
//=============================================================================
//Section 4.2.4 Real floating-point number
//Table 3: IAU-recommended basic unit
sealed case class Meter()                extends PhysicalUnit {val i="meter";val m="length";val u="m";val canHavePrefix=true}
sealed case class Gram()                 extends PhysicalUnit {val i="gram";val m="mass";val u="g";val canHavePrefix=true}
sealed case class Second()               extends PhysicalUnit {val i="second";val m="time";val u="s";val canHavePrefix=true}
sealed case class Radian()               extends PhysicalUnit {val i="radian";val m="plane angle";val u="rad";val canHavePrefix=true}
sealed case class Steradian()            extends PhysicalUnit {val i="steradian";val m="solid angle";val u="sr";val canHavePrefix=true}
sealed case class Kelvin()               extends PhysicalUnit {val i="kelvin";val m="temperature";val u="K";val canHavePrefix=true}
sealed case class Ampere()               extends PhysicalUnit {val i="ampere";val m="electric current";val u="A";val canHavePrefix=true}
sealed case class Mole()                 extends PhysicalUnit {val i="mole";val m="amount of substance";val u="mol";val canHavePrefix=true}
sealed case class Candela()              extends PhysicalUnit {val i="candela";val m="luminous intensity";val u="cd";val canHavePrefix=true}
sealed case class Hertz()                extends PhysicalUnit {val i="hertz";val m="frequency";val u="Hz";override val ai="s^−1";val canHavePrefix=true}
sealed case class Joule()                extends PhysicalUnit {val i="joule";val m="energy";val u="J";override val ai="N m";val canHavePrefix=true}
sealed case class Watt()                 extends PhysicalUnit {val i="watt";val m="power";val u="W";override val ai="J s^−1";val canHavePrefix=true}
sealed case class Volt()                 extends PhysicalUnit {val i="volt";val m="electric potential";val u="V";override val ai="J C^−1";val canHavePrefix=true}
sealed case class Newton()               extends PhysicalUnit {val i="newton";val m="force";val u="N";override val ai="kg m s^−2";val canHavePrefix=true}
sealed case class Pascal()               extends PhysicalUnit {val i="pascal";val m="pressure";val u="Pa";override val ai="N m^−2";override val am="stress";val canHavePrefix=true}
sealed case class Coulomb()              extends PhysicalUnit {val i="coulomb";val m="electric charge";val u="C";override val ai="A s";val canHavePrefix=true}
sealed case class Ohm()                  extends PhysicalUnit {val i="ohm";val m="electric resistance";val u="Ohm";override val ai="V A^−1";val canHavePrefix=true}
sealed case class Siemens()              extends PhysicalUnit {val i="siemens";val m="electric conductance";val u="S";override val ai="A V−1";val canHavePrefix=true}
sealed case class Farad()                extends PhysicalUnit {val i="farad";val m="electric capacitance";val u="F";override val ai="C V^−1";val canHavePrefix=true}
sealed case class Weber()                extends PhysicalUnit {val i="weber";val m="magnetic flux";val u="Wb";override val ai="V s";val canHavePrefix=true}
sealed case class Tesla()                extends PhysicalUnit {val i="tesla";val m="magnetic flux density";val u="T";override val ai="Wb m^−2";val canHavePrefix=true}
sealed case class Henry()                extends PhysicalUnit {val i="henry";val m="inductance";val u="H";override val ai="Wb A^−1";val canHavePrefix=true}
sealed case class Lumen()                extends PhysicalUnit {val i="lumen";val m="luminous";val u="lm";override val ai="cd sr";val canHavePrefix=true}
sealed case class Lux()                  extends PhysicalUnit {val i="lux";val m="illuminance";val u="lx";override val ai="lm m^−2";val canHavePrefix=true}
//=============================================================================
//Section 4.2.4 Real floating-point number
//Table 5: Additional allowed unit
sealed case class DegreeOfArc()          extends PhysicalUnit {val i="degree of arc";val m="plane angle";val u="deg";override val ai="π/180 rad";val canHavePrefix=false}
sealed case class MinuteOfArc()          extends PhysicalUnit {val i="minute of arc";val m="plane angle";val u="arcmin";override val ai="1/60 deg";val canHavePrefix=false}
sealed case class SecondOfArc()          extends PhysicalUnit {val i="second of arc";val m="plane angle";val u="arcsec";override val ai="1/3600 deg";val canHavePrefix=false}
sealed case class MilliSecondOfArc()     extends PhysicalUnit {val i="milli-second of arc";val m="time";val u="mas";override val ai="1/3 600 000 deg";val canHavePrefix=false}
sealed case class Minute()               extends PhysicalUnit {val i="minute";val m="time";val u="min";val canHavePrefix=false}
sealed case class Hour()                 extends PhysicalUnit {val i="hour";val m="time";val u="h";val canHavePrefix=false}
sealed case class Day()                  extends PhysicalUnit {val i="day";val m="time";val u="d";override val ai="86 400 s";val canHavePrefix=false}
sealed case class YearA()               extends PhysicalUnit {val i="year(Julian)";val m="time";val u="a";override val ai="31 557 600 s (365.25 d), peta a (Pa) forbidden";val canHavePrefix=true}
sealed case class YearIAU()              extends PhysicalUnit {val i="year(Julian)";val m="time";val u="yr";val canHavePrefix=true}
sealed case class Electronvolt()         extends PhysicalUnit {val i="electron volt";val m="energy";val u="eV";override val ai="1.6021765 × 10^19 J";val canHavePrefix=true}
sealed case class Erg()                  extends PhysicalUnit {val i="erg";val m="energy";val u="erg";override val ai="10^7 J";val canHavePrefix=false}
sealed case class Rydberg()              extends PhysicalUnit {val i="rydberg";val m="energy";val u="Ry";override val ai=" 1/2 × (2πe^2)^2 × me × c^2 = 13.605692 eV";val canHavePrefix=false}
sealed case class SolarMass()            extends PhysicalUnit {val i="solar mass";val m="mass";val u="solMass";override val ai=" 1.9891 × 10^30 kg";val canHavePrefix=false}
sealed case class UnifiedAtomicMassUnit()extends PhysicalUnit {val i="unified atomic mass unit";val m="mass";val u="i";override val ai="1.6605387 × 10^−27 kg";val canHavePrefix=false}
sealed case class SolarLuminosity()      extends PhysicalUnit {val i="Solar luminosity";val m="luminosity";val u="solLum";override val ai="3.8268 × 10^26 W";val canHavePrefix=false}
sealed case class Angstrom()             extends PhysicalUnit {val i="angstrom";val m="length";val u="Angstrom";override val ai="10^−10 m";val canHavePrefix=false}
sealed case class SolarRadius()          extends PhysicalUnit {val i="Solar radius";val m="length";val u="solRad";override val ai="6.9599 × 10^8 m";val canHavePrefix=false}
sealed case class AstronomicalUnit()     extends PhysicalUnit {val i="astronomical unit";val m="length";val u="AU";override val ai=" 1.49598 × 10^11 m";val canHavePrefix=false}
sealed case class LightYear()            extends PhysicalUnit {val i="light year";val m="length";val u="lyr";override val ai="9.460730 × 10^15 m";val canHavePrefix=false}
sealed case class Parsec()               extends PhysicalUnit {val i="parsec";val m="length";val u="pc";override val ai="3.0857 × 10^16 m";val canHavePrefix=true}
sealed case class Count()                extends PhysicalUnit {val i="count";val m="events";val u="count";val canHavePrefix=false}
sealed case class Ct()                   extends PhysicalUnit {val i="count";val m="events";val u="ct";val canHavePrefix=false}
sealed case class Photon()               extends PhysicalUnit {val i="photon";val m="events";val u="photon";val canHavePrefix=false}
sealed case class Ph()                   extends PhysicalUnit {val i="photon";val m="events";val u="ph";val canHavePrefix=false}
sealed case class Jansky()               extends PhysicalUnit {val i="jansky";val m="flux density";val u="Jy";override val ai="10^−26 W m^−2 Hz^−1";val canHavePrefix=true}
sealed case class Magnitude()            extends PhysicalUnit {val i="(stellar)magnitude";val m="flux density";val u="mag";val canHavePrefix=true}
sealed case class Rayleigh()             extends PhysicalUnit {val i="rayleigh";val m="flux density";val u="R";override val ai="10^10 /(4π) photons m^−2 s^−1 sr^-1 ";val canHavePrefix=true}
sealed case class Gauss()                extends PhysicalUnit {val i="gauss";val m="magnetic field";val u="G";override val ai="10^−4 T";val canHavePrefix=true}
sealed case class Pixel()                extends PhysicalUnit {val i="(image/detector) pixel";val m="area";val u="pixel";val canHavePrefix=false}
sealed case class Pix()                  extends PhysicalUnit {val i="(image/detector) pixel";val m="area";val u="pix";val canHavePrefix=false}
sealed case class Barn()                 extends PhysicalUnit {val i="barn";val m="area";val u="barn";override val ai="10^−28 m^2";val canHavePrefix=true}
sealed case class Debye()                extends PhysicalUnit {val i="debye";val m="";val u="D";override val ai=" 1/3 × 10^−29 C.m";val canHavePrefix=false}
sealed case class Sun()                  extends PhysicalUnit {val i="sun";val m="relative to Sun";val u="Sun";override val ai= "e.g. abundances";val canHavePrefix=false}
sealed case class Channel()              extends PhysicalUnit {val i="channel";val m="detector";val u="chan";val canHavePrefix=false}
sealed case class Bin()                  extends PhysicalUnit {val i="bin";val m="information unit";val u="bin";override val ai="(including the 1-d analogue of pixel)";val canHavePrefix=false}
sealed case class Voxel()                extends PhysicalUnit {val i="voxel";val m="area";val u="voxel";val canHavePrefix=false}
sealed case class Bit()                  extends PhysicalUnit {val i="bit";val m="information unit";val u="bit";val canHavePrefix=true}
sealed case class Byte()                 extends PhysicalUnit {val i="byte";val m="information unit";val u="byte";override val ai="8 bit";val canHavePrefix=true}
sealed case class Adu()                  extends PhysicalUnit {val i="adu";val m="digital conversion unit";val u="adu";val canHavePrefix=false}
sealed case class Beam()                 extends PhysicalUnit {val i="beam";val m="area of observation";val u="beam";override val ai="as in Jy/beam";val canHavePrefix=false}
//=============================================================================
sealed case class NonePhysicalUnit()     extends PhysicalUnit {val i="None";val m="None";val u="None";val canHavePrefix=true}
//=============================================================================
trait PhysicalUnit {
  val u: String       //unit name (abbreviature)
  val m: String       //magnitude measured
  val i: String       //info
  val am: String = "" //alternative magnitude
  val ai: String = "" //additional info
  val canHavePrefix: Boolean
}
//=============================================================================
object PhysicalUnit {
  //---------------------------------------------------------------------------
  private val nonePhysicalUnitSingleton = NonePhysicalUnit()
  //---------------------------------------------------------------------------
  final val physicalUnitSeq: Array[PhysicalUnit] = Array(
      Meter()
    , Gram()
    , Second()
    , Radian()
    , Steradian()
    , Kelvin()
    , Ampere()
    , Mole()
    , Candela()
    , Hertz()
    , Joule()
    , Watt()
    , Volt()
    , Newton()
    , Pascal()
    , Coulomb()
    , Ohm()
    , Siemens()
    , Farad()
    , Weber()
    , Tesla()
    , Henry()
    , Lumen()
    , Lux()
    , DegreeOfArc()
    , MinuteOfArc()
    , SecondOfArc()
    , MilliSecondOfArc()
    , Minute()
    , Hour()
    , Day()
    , YearA()
    , YearIAU()
    , Electronvolt()
    , Erg()
    , Rydberg()
    , SolarMass()
    , UnifiedAtomicMassUnit()
    , SolarLuminosity()
    , Angstrom()
    , SolarRadius()
    , AstronomicalUnit()
    , LightYear()
    , Parsec()
    , Count()
    , Ct()
    , Photon()
    , Ph()
    , Jansky()
    , Magnitude()
    , Rayleigh()
    , Gauss()
    , Pixel()
    , Pix()
    , Barn()
    , Debye()
    , Sun()
    , Channel()
    , Bin()
    , Voxel()
    , Bit()
    , Byte()
    , Adu()
    , Beam()
  )
  //---------------------------------------------------------------------------
  def apply(s: String): PhysicalUnit = {
    physicalUnitSeq.foreach(unit => if (unit.u == s) return unit)
    nonePhysicalUnitSingleton
  }
  //---------------------------------------------------------------------------
  def getSortedUnitName() = (physicalUnitSeq map ( _.u)).sorted
  def getSortedUnitNameWithPrefix() = (physicalUnitSeq.filter( _.canHavePrefix ) map ( _.u)).sorted
  def getSortedUnitNameNonePrefix() = (physicalUnitSeq.filter( !_.canHavePrefix ) map ( _.u)).sorted
  //---------------------------------------------------------------------------
  def getCommonUnitNameAndPrefixId() = {
    val unitSeq = PhysicalUnitPrefix.submultiplerSeq ++ PhysicalUnitPrefix.multiplerSeq
    (PhysicalUnit.physicalUnitSeq.filter(_.canHavePrefix) flatMap { case u =>
      unitSeq map { m =>
        if (u.u == m.i) Some((u,m)) else None
      }
    }).flatten
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file PhysicalUnit.scala
//=============================================================================
