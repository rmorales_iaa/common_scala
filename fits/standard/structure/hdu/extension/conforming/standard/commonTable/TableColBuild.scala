/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  25/Nov/2022
 * Time:  14h:15m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable
//=============================================================================
import com.common.fits.standard.block.record.RecordNumber.RecordInteger
import com.common.fits.standard.Keyword.KEYWORD_TFIELDS
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.BinTableColDef
//=============================================================================
//=============================================================================
object TableColBuild {
  //---------------------------------------------------------------------------
  def build(colDef:TableColDef) = {

    val tDim =
      if (colDef.tableRangeValue.isEmpty) None
      else {
        colDef match {
          case t: BinTableColDef=> if(t.tDim.isEmpty) None else Some(t.tDim.get.toString())
          case _ => None
        }
    }
    TableColBuild(colDef.form
                  , colDef.name
                  , colDef.unit
                  , colDef.scale
                  , colDef.zero
                  , colDef.nullValue
                  , if (colDef.disp.isEmpty) None else Some(colDef.disp.get.toString)
                  , if (colDef.tableRangeValue.isEmpty || colDef.tableRangeValue.get.tdMin.isEmpty) None else Some(colDef.tableRangeValue.get.tdMin.get)
                  , if (colDef.tableRangeValue.isEmpty || colDef.tableRangeValue.get.tdMax.isEmpty) None else Some(colDef.tableRangeValue.get.tdMax.get)
                  , if (colDef.tableRangeValue.isEmpty || colDef.tableRangeValue.get.tlMin.isEmpty) None else Some(colDef.tableRangeValue.get.tlMin.get)
                  , if (colDef.tableRangeValue.isEmpty || colDef.tableRangeValue.get.tlMax.isEmpty) None else Some(colDef.tableRangeValue.get.tlMax.get)
                  , tDim = tDim)
  }
  //---------------------------------------------------------------------------
  //(recordMap,rowCharSize)
  def buildRecordMap(colDefSeq: Array[TableColBuild]
                     , isAsciiTable: Boolean) = {

    val recordMap = RecordMap()
    var colIndex   = 1L //index of the column, starting with index 1
    var charOffset = 1L //offset of the column in characters

    //-------------------------------------------------------------------------
    //TFIELDS (mandatory,column count)
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, 1))
    //-------------------------------------------------------------------------
    colDefSeq.map { colDef =>
      val fieldSize = TableColDef.filldMap(
        recordMap
        , colIndex
        , charOffset
        , form = colDef.form.getValue()
        , name = colDef.name
        , unit = colDef.unit
        , scale = colDef.scale
        , zero = colDef.zero
        , nullValue = colDef.nullValue
        , disp = colDef.disp
        , tdMin = colDef.tdMin
        , tdMax = colDef.tdMax
        , tlMin = colDef.tlMin
        , tlMax = colDef.tlMax
        , tDim  = colDef.tDim
        , isAsciiTable
      )
      charOffset += fieldSize + 1 //one more for the column divider
      colIndex += 1
    }
    //-------------------------------------------------------------------------
    //TFIELDS (mandatory,column count)
    recordMap.updateValue[Long](KEYWORD_TFIELDS, colIndex - 1)
    //-------------------------------------------------------------------------
    (recordMap,charOffset-2)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class TableColBuild(form: TableColForm
                         , name: Option[String] = None
                         , unit: Option[String] = None
                         , scale: Option[Double] = None
                         , zero: Option[Double] = None
                         , nullValue: Option[String] = None
                         , disp: Option[String] = None
                         , tdMin: Option[Double] = None
                         , tdMax: Option[Double] = None
                         , tlMin: Option[Double] = None
                         , tlMax: Option[Double] = None
                         , tDim: Option[String] = None)
//=============================================================================
//End of file TableColBuild.scala
//=============================================================================
