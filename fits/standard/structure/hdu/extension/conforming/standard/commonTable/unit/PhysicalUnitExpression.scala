/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  11/Nov/2022
 * Time:  10h:54m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.unit
//=============================================================================
//=============================================================================
trait PhysicalUnitExpression {
  //---------------------------------------------------------------------------
  private var numericMultiplier:Option[String] = None
  //---------------------------------------------------------------------------
  val leftUnit:Option[PhysicalUnitExpression]
  val operator: String
  //---------------------------------------------------------------------------
  def setNumericMultiplier(nm:String) = numericMultiplier = Some(nm)
  //---------------------------------------------------------------------------
  private def synthetizeString(useMagnitude: Boolean = false) : String =
    this match {
      case x: BasicPUE =>
        useMagnitude match {
          case true =>
            (if (x.unitPrefix.isDefined) x.unitPrefix.get.i else "") ++ x.physicalUnit.i
          case false =>
            (if (x.unitPrefix.isDefined) x.unitPrefix.get.n else "") ++ x.physicalUnit.u
        }
       case x: ArithmeticPUE       => x.leftUnit.get.synthetizeString(useMagnitude) + x.operator + x.rightUnit.synthetizeString(useMagnitude)
       case x: PowerPUE            => leftUnit.get.synthetizeString(useMagnitude) + operator + x.numericPower
       case x: AppliedFunctionPUE  => operator + "(" + x.rightUnit.synthetizeString(useMagnitude) + ")"
       case x: EnclosedPUE         => "(" + x.leftUnit.get.synthetizeString(useMagnitude) + ")"
  }
  //---------------------------------------------------------------------------
 def synthesizeString(): String = {
    val s = synthetizeString(useMagnitude = false)
    if (numericMultiplier.isDefined) numericMultiplier.get + s
    else s
  }
  //---------------------------------------------------------------------------
  def synthesizeMagnitude() : String = {
    val s = synthetizeString(useMagnitude = true)
    if (numericMultiplier.isDefined) numericMultiplier.get + s
    else s
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//PUE == PhysicalUnitExpression
sealed case class BasicPUE(unitPrefix: Option[PhysicalUnitPrefix], physicalUnit: PhysicalUnit) extends PhysicalUnitExpression {val operator = "";val leftUnit = None}
sealed case class ArithmeticPUE(leftUnit: Option[PhysicalUnitExpression], operator: String, rightUnit: PhysicalUnitExpression) extends PhysicalUnitExpression
sealed case class PowerPUE(leftUnit: Option[PhysicalUnitExpression], operator: String, numericPower: String) extends PhysicalUnitExpression
sealed case class AppliedFunctionPUE(operator: String, rightUnit: PhysicalUnitExpression) extends PhysicalUnitExpression {val leftUnit: Option[PhysicalUnitExpression] = None}
sealed case class EnclosedPUE(leftUnit: Option[PhysicalUnitExpression]) extends PhysicalUnitExpression {val operator: String = ""}
//=============================================================================
//=============================================================================
//End of file PhysicalUnitExpression.scala
//=============================================================================
