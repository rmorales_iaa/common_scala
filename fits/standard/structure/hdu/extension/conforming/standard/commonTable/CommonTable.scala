/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  04/Nov/2022
 * Time:  08h:44m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable
//=============================================================================
import com.common.fits.standard.block.record.RecordNumber.{RecordFloat, RecordInteger}
import com.common.fits.standard.disconformity.DisconfWarn
import com.common.fits.standard.Keyword._
import com.common.fits.standard.dataType.DataType
import com.common.fits.standard.disconformity.DisconfFatal
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.hdu.HDU
import com.common.fits.standard.structure.hdu.HDU.isValidIntegerRecord
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.tdisp._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.tdisp.parser.ParserTdisp
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.unit.parser.ParserPhysicalUnit
//=============================================================================
import java.math.RoundingMode
import java.text.DecimalFormat
//=============================================================================
object CommonTable {
  //---------------------------------------------------------------------------
  type ByteArr = Array[Byte]
  //---------------------------------------------------------------------------
  private final val MIN_ALLOWED_TFIELDS =   0 //included
  private final val MAX_ALLOWED_TFIELDS = 999 //included
  //---------------------------------------------------------------------------
  def generateDisconfFatal(ctxt: FitsCtxt
                           , message: String
                           , additionalInfo: String = ""
                           , sectionName: String = SectionName.SECTION_7_2_2) =
    DisconfFatal(
        ctxt
      , sectionName
      , s"Structure: ${ctxt.structureID} $message"
      , additionalInfo = additionalInfo)
  //---------------------------------------------------------------------------
  def generateDisconfWarn(ctxt: FitsCtxt
                           , message: String
                           , additionalInfo: String = ""
                           , sectionName: String = SectionName.SECTION_7_2_2) =
    DisconfWarn(
      ctxt
      , sectionName
      , s"Structure: ${ctxt.structureID} $message"
      , additionalInfo = additionalInfo
    )
  //---------------------------------------------------------------------------
  def buildExponentialFormat(fractionfractionDigitCount: Long, exponentfractionDigitCount: Int ) = {
    val digitString = if (fractionfractionDigitCount == -1) "" else "0" * fractionfractionDigitCount.toInt
    val exponentString = if (exponentfractionDigitCount == 0) "" else "E" + ("0" * exponentfractionDigitCount)
    val df = new DecimalFormat(digitString + "." + exponentString)
    df.setRoundingMode(RoundingMode.HALF_UP)
    df.setGroupingUsed(false)
    df
  }
  //---------------------------------------------------------------------------
  def buildFloatFixedPoint(fractionDigitCount: Long) = {
    val df = new DecimalFormat()
    df.setMinimumFractionDigits(fractionDigitCount.toInt)
    df.setMaximumFractionDigits(fractionDigitCount.toInt)
    df.setRoundingMode(RoundingMode.HALF_UP)
    df.setGroupingUsed(false)
    df
  }
  //---------------------------------------------------------------------------
  def isValidTfields(recordMap: RecordMap
                     , ctxt: FitsCtxt = FitsCtxt()): Boolean = {
    val record = recordMap.getFirstRecord(KEYWORD_TFIELDS).getOrElse {
      generateDisconfFatal(ctxt, message = s"Can not find the keyword: '$KEYWORD_TFIELDS'", sectionName=SectionName.SECTION_7_2_1)
      return false
    }
    if (!isValidIntegerRecord(record, SectionName.SECTION_7_2_1, ctxt)) return false
    val r = record.asInstanceOf[RecordInteger]
    val value = r.value
    if (value < MIN_ALLOWED_TFIELDS || value > MAX_ALLOWED_TFIELDS) {
      generateDisconfFatal(ctxt,
        message = s"keyworf '$KEYWORD_TFIELDS' is expecting a value between: {$MIN_ALLOWED_TFIELDS,$MAX_ALLOWED_TFIELDS} both limits included"
        , additionalInfo = s"Current value: ${value.toString}"
        , sectionName=SectionName.SECTION_4_4_1_1)
      true
    }
    else true
  }
  //---------------------------------------------------------------------------
  def isValidRecordTform(recordMap: RecordMap
                         , sectionName: String
                         , ctxt: FitsCtxt): Boolean = {

    //check if it possible to have TBCOL
    val fieldCount = recordMap.getTfields()
    if (fieldCount == 0) return true
    val formSeqName = recordMap.getTformSeqName()
    if (fieldCount == 0 && formSeqName.length > 0) {
      generateDisconfFatal(ctxt, message = s"found keywords with 'TFORMn' but the are not allowed because '$KEYWORD_TFORM' is zero", sectionName = SectionName.SECTION_7_2_1)
    }
    HDU.recordSequenceCommonCheck(recordMap
      , sectionName
      , KEYWORD_TFORM
      , fieldCount
      , formSeqName
      , ctxt
      , checkRecordString = true
      , checkAreConsecutive = false)
  }
  //---------------------------------------------------------------------------
  def isValidRecordTbcol(recordMap: RecordMap
                         , ctxt: FitsCtxt): Boolean = {

    //check if it possible to have TBCOL
    val fieldCount = recordMap.getTfields()
    if (fieldCount == 0) return true
    val colSeqName = recordMap.getTbcolSeqName()
    if (fieldCount == 0 && colSeqName.length > 0) {
      generateDisconfFatal(ctxt, message = s"found keywords with 'TBCOLn' but the are not allowed because '$KEYWORD_TFIELDS' is zero", sectionName=SectionName.SECTION_7_2_1)
    }

    HDU.recordSequenceCommonCheck(recordMap
      , SectionName.SECTION_7_2_1
      , KEYWORD_TBCOL
      , fieldCount
      , colSeqName
      , ctxt
      , checkAreConsecutive = false)
  }
  //---------------------------------------------------------------------------
  def isValidTtype(name: Option[String]
                   , sectionName: String
                   , colIndex: Long
                   , nameSet: scala.collection.mutable.Set[String]
                   , ctxt: FitsCtxt): Boolean = {
    if (name.isEmpty) return true
    if (nameSet.contains(name.get.toLowerCase()))
      generateDisconfWarn(ctxt, message = s"the value: '${name.get}' of keyword: '$KEYWORD_TTYPE$colIndex' is not unique", sectionName=sectionName)

    nameSet += name.get.toLowerCase()

    val isValidCharSeq = name.get.forall { c =>
      (c >= 0x30 && c <= 0x39) || //digit
      (c >= 0x41 && c <= 0x5A) || //upper case
      (c >= 0x61 && c <= 0x7A) || //lower case
      c == 0x5f // underscore
    }

    if (!isValidCharSeq)
      generateDisconfWarn(ctxt, message = s"the value: '${name.get}' of keyword: '$KEYWORD_TTYPE$colIndex' is not a valid char sequence", sectionName=sectionName)
    true
  }
  //---------------------------------------------------------------------------
  def isValidTunit(_unit: Option[String]
                   , colIndex: Long
                   , ctxt: FitsCtxt): Boolean = {

    val unit = _unit.getOrElse(return true)
    val (exprUnit,errorMessage) = ParserPhysicalUnit.parseInput(unit)
    if (exprUnit.isDefined) return true

    generateDisconfFatal(ctxt
      , message = s"the value: '$unit' of keyword: '$KEYWORD_TUNIT$colIndex' is not allowed"
      , additionalInfo = errorMessage
      , sectionName = SectionName.SECTION_4_2_TABLE_4)
    true
  }
  //---------------------------------------------------------------------------
  def isValidTdisp(disp: Option[String]
                   , sectionName: String
                   , colIndex: Long
                   , formIsInteger: Boolean
                   , isAciiTable: Boolean
                   , ctxt: FitsCtxt): (Boolean,Option[Tdisp]) = {
    if (disp.isEmpty) return (true,None)
    val r = ParserTdisp.run(disp.get, isAciiTable)

    r match {
      case _: TdispError =>
        generateDisconfFatal(ctxt
          , message = s"the value: '${disp.get}' of keyword: '$KEYWORD_TDISP$colIndex' is not valid"
          , additionalInfo = r.asInstanceOf[TdispError].message
          , sectionName=sectionName)
        return (false,None)

      case _: TdispBinary | _:TdispOctal  | _:TdispHexadecimal =>
        if(!formIsInteger) {
          generateDisconfFatal(ctxt
            , message = s"keyword: '$KEYWORD_TDISP$colIndex' with value: '${disp.get}'can only be used with a TFORM of type integer"
            , sectionName = sectionName)
          return (false,None)
        }
      case _ =>
    }
    (true,Some(r))
  }

  //---------------------------------------------------------------------------
  private def isValidTscalTzero(recordMap: RecordMap
                                , isAsciiTable: Boolean
                                , key: String
                                , ctxt: FitsCtxt): Boolean = {
    if (!recordMap.contains(key)) return true
    val record = recordMap.getFirstRecord(key).get
    if (!record.isInstanceOf[RecordFloat]) {
      generateDisconfFatal(
        ctxt
        , message = s"the value: '${record.value.toString}' of keyword: '$key' is not valid. It must be a floating point"
        , sectionName = if (isAsciiTable) SectionName.SECTION_7_2_2 else SectionName.SECTION_7_3_2
      )
      return true
    }
    false
  }
  //---------------------------------------------------------------------------
  def isValidTscal(recordMap: RecordMap
                   , colCount: Long
                   , isAsciiTable: Boolean
                   , ctxt: FitsCtxt): Boolean = {
    for (i <- 1L to colCount)
      if (!isValidTscalTzero(recordMap, isAsciiTable, KEYWORD_TSCAL + i, ctxt)) return false
    true
  }
  //---------------------------------------------------------------------------
  def isValidTzero(recordMap: RecordMap
                   , colCount: Long
                   , isAsciiTable: Boolean
                   , ctxt: FitsCtxt): Boolean = {
    for (i <- 1L to colCount)
      if (!isValidTscalTzero(recordMap, isAsciiTable, KEYWORD_TZERO + i, ctxt)) return false
    true
  }
  //---------------------------------------------------------------------------
  def isValidMinMax(recordMap: RecordMap
                    , colCount: Long
                    , keyMin: String
                    , keyMax: String
                    , sectionName: String
                    , ctxt: FitsCtxt): Boolean = {

    for (i <- 1L to colCount) {
      if (recordMap.contains(keyMin) && recordMap.contains(keyMax)) {
        val min = recordMap.getFirstValue[String](keyMin + i).get.toDouble
        val max = recordMap.getFirstValue[String](keyMax + i).get.toDouble
        if (min > max) {
          generateDisconfFatal(ctxt
            , s"the value: '$min' of keyword: '$keyMin' must be great than the value: '$max' of keyword: '$keyMax'"
            , sectionName = sectionName)
        }
      }
    }
    true
  }
  //---------------------------------------------------------------------------
  def isValidTlminTlMax(recordMap: RecordMap
                        , colIndex: Long
                        , ctxt: FitsCtxt): Boolean = {

    val keyMin = KEYWORD_TLMIN + colIndex
    val keyMax = KEYWORD_TLMAX + colIndex
    if (recordMap.contains(keyMin) && recordMap.contains(keyMax)) {
      val min = recordMap.getFirstValue[String](keyMin).get.toDouble
      val max = recordMap.getFirstValue[String](keyMax).get.toDouble
      if (min > max) {
      }
    }
    true
  }
  //---------------------------------------------------------------------------
  def getNumericValue(recordMap: RecordMap
                      , keyword: String
                      , dataType: DataType[_]
                      , sectionName: String
                      , ctxt: FitsCtxt): (Boolean, Option[String]) = {
    if (!recordMap.contains(keyword)) return (true, None)
    val keywordValue = DataType.getString(recordMap,keyword, dataType)
    if (keywordValue.isEmpty || !dataType.isValid(keywordValue.get)) {
      generateDisconfFatal(ctxt
        , s"the value: '$keywordValue' of keyword: '$keyword' is not a valid: ${dataType.name}"
        , sectionName = sectionName)
      (false, None)
    }
    else (true, keywordValue)
  }

  //---------------------------------------------------------------------------
  def checkTtypeCharacterRange(recordMap: RecordMap
                               , sectionName: String
                               , ctxt: FitsCtxt = FitsCtxt()) = {
    val colCount = recordMap.getTfields()
    //check if each column can fit in the row definition
    for (i <- 1L to colCount) {
      val r = recordMap.getFirstValue[String](KEYWORD_TTYPE + i)
      if (r.isDefined) {
        r.get.foreach { c =>
          val isValid =
              (c >= 'A' && c <= 'Z') ||
              (c >= 'a' && c <= 'z') ||
              (c >= '0' && c <= '9') ||
              c == '_'
          if (!isValid)
            generateDisconfWarn(ctxt
              , s" keyword '$KEYWORD_TTYPE$i' with value: '${r.get}' has the character '$c' that is not in range {a,z},{A,Z},{0,9},{_}"
              , sectionName)
        }
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CommonTable.scala
//=============================================================================
