/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Nov/2022
 * Time:  11h:45m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable
//=============================================================================
import com.common.fits.standard.block.record.RecordMap.{BSCALE_DEFAULT_VALUE, BZERO_DEFAULT_VALUE}
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.tform._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.tdisp.Tdisp
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.{TableColDef, TableRangeValue}
//=============================================================================
//=============================================================================
case class AsciiTableColDef(colIndex: Long  //index of the column, starting with index 1
                            , position: Long //starting character position
                            , name: Option[String] = None
                            , form: AsciiTableTform
                            , unit: Option[String] = None
                            , scale: Option[Double] = None
                            , zero: Option[Double] = None
                            , nullValue: Option[String] = None
                            , disp: Option[Tdisp] = None
                            , tableRangeValue: Option[TableRangeValue] = None) extends TableColDef {
  //---------------------------------------------------------------------------
  val dataType = AsciiTableTform.getDataType(form)
  //---------------------------------------------------------------------------
  def getCharSize = form.charSize
  //---------------------------------------------------------------------------
  def applyLinearTransformation(s: String): Double = {
    val _scale = scale.getOrElse(BSCALE_DEFAULT_VALUE)
    val _zero = zero.getOrElse(BZERO_DEFAULT_VALUE)
    val value = s.toDouble
    if (_zero == 0D && _scale == 1D) value
    else _zero + value * _scale
  }
  //---------------------------------------------------------------------------
  def format(s: String, colIndex: Long  = 1, ctxt: FitsCtxt = FitsCtxt()): String = {
    //--------------------------------------------------------------------------
    if (!disp.isDefined) form.format(s, ctxt)
    else disp.get.format(s,form, colIndex, ctxt)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file AsciiTableColDef.scala
//=============================================================================
