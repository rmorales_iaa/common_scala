/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Nov/2022
 * Time:  11h:54m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable
//=============================================================================
import com.common.fits.standard.disconformity.DisconfFatal
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.tform.AsciiTableTformCharacter
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.CommonTable.generateDisconfFatal
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.{TableColDef, TableRow}
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object AsciiTableRow {
  //---------------------------------------------------------------------------
  private def isSevenBitString(s: String) = s.forall( c=> c >= 0 && c <= 0x7F)
  //---------------------------------------------------------------------------
  def parseColSeq(colDefSeq: Array[AsciiTableColDef]
                  , rowAsString: String
                  , processedRowCount: Long
                  , applyLinearTransformation: Boolean
                  , callbackOnEachRow: Option[Array[String] => Boolean] = None
                  , _ctxt: FitsCtxt): Option[Array[String]] = {

    val ctxt =  _ctxt
    val maxAllowedPos = rowAsString.length - 1
    val rowValueSeq = colDefSeq.map { colDef => //slice in columns
      val colIndex = colDef.colIndex
      val starPos = colDef.position.toInt - 1 //first string index is 0
      val endPos = starPos + colDef.getCharSize - 1 //first string index is 0

      if ((starPos < 0) || (endPos > maxAllowedPos)) {
        generateDisconfFatal(ctxt
          , s"invalid row position :$starPos .Valid positions {1,${rowAsString.length}}, row: '$rowAsString'"
          , sectionName = SectionName.SECTION_7_2_1)
        return None
      }

      val charSize = colDef.getCharSize.toInt
      val colValueRawAsString = rowAsString.drop(starPos).take(charSize)

      //update context information to report errors
      ctxt.info = rowAsString
      ctxt.position = processedRowCount + 1

      //verify the content
      var colValueAsString = colDef.form.getValue(colValueRawAsString
                                                 , colIndex
                                                 , ctxt).getOrElse(return None)

      val isDefinedLinearTransformation = colDef.scale.isDefined || colDef.zero.isDefined
      val appliedTransformation = isDefinedLinearTransformation && applyLinearTransformation
      if (appliedTransformation)
        colValueAsString = colDef.applyLinearTransformation(colValueAsString).toString

      if (!colDef.form.isInstanceOf[AsciiTableTformCharacter]) {
        if(!colDef.isValueInRange(colValueAsString.toDouble, ctxt)) return None
      }

      //check NULL value
      colValueAsString =
        if (colDef.nullValue.isDefined && colDef.nullValue.get == colValueAsString)  //check NULL value
          " " * colDef.form.charSize.toInt
        else {
          val r = colDef.format(colValueAsString, colIndex, ctxt)
          if (r.isEmpty) return None
          else r
        }

      colValueAsString
    }

    //call back with the validated value
    if (callbackOnEachRow.isDefined && !callbackOnEachRow.get(rowValueSeq)) None
    else Some(rowValueSeq)
  }
  //---------------------------------------------------------------------------
  def getRowValueSeq(byteSeq: Array[Byte]
                     , colDefSeq: Array[AsciiTableColDef]
                     , processedRowCount: Long
                     , callbackOnEachRow: Option[Array[String] => Boolean] = None
                     , applyLinearTransformation: Boolean
                     , ctxt: FitsCtxt = FitsCtxt()
                    ) : (Boolean,Array[String]) = {
    val rowAsString = new String(byteSeq)
    if (!isSevenBitString(rowAsString)) {
      generateDisconfFatal(ctxt
        , message =  s"invalid character sequence. Allowed chars: [0x00,0x7F]. String '$rowAsString'"
        , sectionName = SectionName.SECTION_7_2_2
      )
      return (false, Array())
    }

    (true,
     parseColSeq(colDefSeq
                 , rowAsString
                 , processedRowCount
                 , applyLinearTransformation
                 , callbackOnEachRow
                 , ctxt).getOrElse(return (false, Array()))
    )
  }
  //---------------------------------------------------------------------------
  //(success,rowSeq)
  def buildRowSeq(recordMap: RecordMap
                  , byteSeq:Array[Byte]
                  , applyLinearTransformation: Boolean
                  , storeResult: Boolean
                  , callbackOnEachRow: Option[Array[String] => Boolean]
                  , ctxt: FitsCtxt = FitsCtxt()): (Boolean,Array[AsciiTableRow], Array[AsciiTableColDef]) = {
    //--------------------------------------------------------------------------
    val colDefSeq = TableColDef.toAsciiTableColDef(
      TableColDef.buildColDefSeq(
        recordMap
        , isAciiTable = true
        , SectionName.SECTION_7_2_2
        , ctxt)
        .getOrElse(return (false, Array(), Array()))
    )
    val axisSeq  = recordMap.getAxisSeq()
    val rowByteSize = axisSeq.head  //NAXIS1
    val rowCount    = axisSeq.last  //NAXIS2

    //check size
    if (byteSeq.length < (rowCount * rowByteSize)) {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_7_2_1
        , s"Structure: ${ctxt.structureID} the record the expected ASCII table size (rowCount,rowByteSize): ($rowCount x $rowByteSize) does not match with the input data size: ${byteSeq.length}"
      )
      return (false,Array(),Array())
    }

    val rowSeq = ArrayBuffer[AsciiTableRow]()
    var processedRowCount = 0L
    byteSeq.grouped(rowByteSize.toInt).foreach { byteSeq =>  //slice in rows

      val rowValueSeq = getRowValueSeq(
          byteSeq
        , colDefSeq
        , processedRowCount
        , callbackOnEachRow
        , applyLinearTransformation
        , ctxt)

      if (!rowValueSeq._1) return (false,Array(),Array())

      processedRowCount += 1
      if (storeResult) rowSeq += AsciiTableRow(rowValueSeq._2)
      if (processedRowCount == rowCount) return (true,if(storeResult) rowSeq.toArray else Array(), colDefSeq)
    }
    (true,rowSeq.toArray,colDefSeq)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class AsciiTableRow(itemSeq : Array[String]) extends TableRow
//=============================================================================
//End of file AsciiTableRow.scala
//=============================================================================
