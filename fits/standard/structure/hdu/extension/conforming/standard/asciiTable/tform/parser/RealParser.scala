/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Nov/2022
 * Time:  20h:34m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.tform.parser
//=============================================================================
//=============================================================================
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.tform.parser.TformParser.{longInteger, longIntegerAsString}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.unit.parser.ParserPhysicalUnit.sign
import com.common.logger.MyLogger
import fastparse.NoWhitespace._
import fastparse._
//=============================================================================
case class Real(integerPart: Long, decimalPointAtIntegerPart: Boolean, decimalPart: Option[String])
//=============================================================================
object RealParser extends MyLogger {
  //---------------------------------------------------------------------------
  private def optSpaceSeq[_:P] = P(" ").rep
  private def dot[_:P]    = P(".")
  //---------------------------------------------------------------------------
  private def endOfIntegerPart[_: P]  = End | P("." | "+" | "-" | "E" | "D")
  //---------------------------------------------------------------------------
  private def optSignedLong[_: P]: P[Long] =
    (sign.? ~ longInteger) map { case (s, i) =>
      if (s.isDefined && s.get == "-") -i
      else i
    }
  //---------------------------------------------------------------------------
  private def exponent[_: P] : P[String]  =
    (P("E" | "D").! ~ optSignedLong) .map { case (exp,i)=> exp + i.toString }
  //---------------------------------------------------------------------------
  private def integerPart[_: P]: P[Real] = {
    (longInteger ~ End).map { integer =>
      Real(integer
           , decimalPointAtIntegerPart = false
           , decimalPart = None)
    }
  }
  //---------------------------------------------------------------------------
  private def fractionPart[_: P]: P[Real] = {
    (dot ~ longIntegerAsString.? ~ End).map { fractionPart =>
      Real(0
           , decimalPointAtIntegerPart = true
           , if (fractionPart.isEmpty) None else Some(fractionPart.get))
    }
  }
  //---------------------------------------------------------------------------
  private def composed[_: P]: P[Real] = {
    (longInteger.? ~ (dot ~ longIntegerAsString.?).? ~ exponent.? ~ End).map { case (integerPart, fractionalPart, exponent) =>
      val decimalPointAtIntegerPart = fractionalPart.isDefined || exponent.isDefined
      Real(integerPart.getOrElse(0)
        , decimalPointAtIntegerPart = decimalPointAtIntegerPart
        , if (decimalPointAtIntegerPart) Some(fractionalPart.getOrElse("0") + exponent.getOrElse("")) else None)
    }
  }
  //---------------------------------------------------------------------------
  def parseInput[_: P] : P[Real]  =
    (optSpaceSeq ~ sign.? ~ (integerPart | fractionPart | composed)).map { case (sign,real) =>
      if (sign.isDefined && sign.get == "-") //update sign
        Real(-real.integerPart,real.decimalPointAtIntegerPart, real.decimalPart)
      else real
    }
  //---------------------------------------------------------------------------
  def run(s: String): (Option[Real],String) = {
    parse(s, parseInput(_)) match {
      case Parsed.Success(r, index) =>
        if (index == s.size) (Some(r),"")
        else {
          val parsedInput = s.take(index)
          val remain = s.drop(index)
          (None,s"Error parsing input '$s' at position (first position 0): $index. Parsed input: '$parsedInput' Remain input: '$remain'")
        }
      case Parsed.Failure(expected, index, failure) =>
        (None,s"Error parsing the input at index: $index. Expected: '$expected' . Error: '${failure.trace().longAggregateMsg}'")
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ParserTform.scala
//=============================================================================