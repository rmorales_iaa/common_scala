/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Nov/2022
 * Time:  20h:34m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.tform.parser
//=============================================================================
import fastparse._
import NoWhitespace._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.tform._
import com.common.logger.MyLogger
//=============================================================================
object TformParser extends MyLogger {
  //---------------------------------------------------------------------------
  def digit[_:P] = P(CharIn("0-9")).!.map( s => s )
  //---------------------------------------------------------------------------
  def integer[_: P]: P[Int] =  digit.rep(1).!.map(s=> s.toInt )
  //---------------------------------------------------------------------------
  def longInteger[_: P]: P[Long] =  digit.rep(1).!.map(s=> s.toLong )
  def longIntegerAsString[_: P]: P[String] =  digit.rep(1).!
  //---------------------------------------------------------------------------
  def w[_: P]: P[Int] = integer
  //---------------------------------------------------------------------------
  def d[_: P]: P[Int] = integer
  //---------------------------------------------------------------------------
  def dotD[_: P]: P[Int] = ("." ~ d).map { s => s }
  //---------------------------------------------------------------------------
  private def tFormCharacter[_: P]: P[AsciiTableTform]        =  "A" ~ w.map(s=> AsciiTableTformCharacter(s) )
  //---------------------------------------------------------------------------
  private def tFormInteger[_: P]: P[AsciiTableTform]          =  "I" ~ w.map { s => AsciiTableTformInteger(s) }
  //---------------------------------------------------------------------------
  private def tFormFloatFixed[_: P]: P[AsciiTableTform]       =  "F"~ (w ~ dotD).map { case (w,d) => AsciiTableTformFloatFixed(w,d) }
  //---------------------------------------------------------------------------
  private def tFormFloatExponential[_: P]: P[AsciiTableTform] =  "E"~ (w ~ dotD).map { case (w,d) => AsciiTableTformFloat_32(w,d) }
  //---------------------------------------------------------------------------
  private def tFormDouble[_: P]: P[AsciiTableTform]           =  "D"~ (w ~ dotD).map { case (w,d) => AsciiTableTformFloat_64(w,d) }
  //---------------------------------------------------------------------------
  private def parseInput[_: P] : P[AsciiTableTform]  =
    tFormCharacter |
    tFormInteger |
    tFormFloatFixed |
    tFormFloatExponential |
    tFormDouble
  //---------------------------------------------------------------------------
  def run(s: String): AsciiTableTform =
    parse(s, parseInput(_)) match {
      case Parsed.Success(r, index) =>
        if (index == s.size) r
        else {
          val parsedInput = s.take(index)
          val remain = s.drop(index)
          AsciiTableTformError(s"Error parsing input '$s' at position (first position 0): $index. Parsed input: '$parsedInput' Remain input: '$remain'")
        }
      case Parsed.Failure(expected, index, failure) =>
        AsciiTableTformError(s"Error parsing the input at index: $index. Expected: '$expected' . Error: '${failure.trace().longAggregateMsg}'")
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ParserTform.scala
//=============================================================================