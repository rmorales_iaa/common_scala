/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  27/Sep/2022
 * Time:  13h:29m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable
//=============================================================================
import com.common.fits.standard.block.Block
import com.common.fits.standard.Keyword._
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.hdu.HDU
import com.common.fits.standard.structure.hdu.HDU._
import com.common.fits.standard.structure.hdu.extension.Extension
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.tform.parser.TformParser
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.{CommonTable, TableColDef}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.CommonTable._
import com.common.stream.local.LocalOutputStream
//=============================================================================
//=============================================================================
object AsciiTable {
  //---------------------------------------------------------------------------
  val DUMP_CSV_NAME = "ascii_table.csv"
  //---------------------------------------------------------------------------
  def isValidNaxis_1(recordMap: RecordMap, ctxt: FitsCtxt = FitsCtxt()) : Boolean = {
    val expectedRowByteSize = recordMap.getAxisSeq().head
    val calculatedRowByteSize = recordMap.getAllRecordWithPrefix(KEYWORD_TFORM).map { r=>
      TformParser.run(r.value.toString) .charSize}.sum
    if(expectedRowByteSize < calculatedRowByteSize) {
      generateDisconfFatal(ctxt
        , s"the value of ${KEYWORD_NAXIS}1:$expectedRowByteSize is less than the sum of all column size: $calculatedRowByteSize"
        , sectionName = SectionName.SECTION_7_2_1)
      true
    }
    else true
  }
  //---------------------------------------------------------------------------
  def isValidTbcol(recordMap: RecordMap, ctxt: FitsCtxt = FitsCtxt()): Boolean = {
    if (!HDU.isValidRecordIntegerSeq(recordMap, SectionName.SECTION_7_2_1, recordMap.getTfields(), KEYWORD_TBCOL, ctxt)) return false
    if (!CommonTable.isValidRecordTbcol(recordMap, ctxt)) return false

    val expectedRowCharSize = recordMap.getAxisSeq().head //expected row size in chars
    val colCount = recordMap.getTfields()

    //check if each column can fit in the row definition
    val colCharSizeSeq = (for (i <- 1L to colCount) yield {
      val startPos = recordMap.getFirstValue[Long](KEYWORD_TBCOL + i).get
      val columnCharSize = TformParser.run(recordMap.getFirstValue[String](KEYWORD_TFORM + i).get).charSize
      val endPos = startPos + columnCharSize - 1
      if (endPos > expectedRowCharSize) {
        generateDisconfFatal(ctxt
          , s"the value of '$KEYWORD_TBCOL$i' is $startPos plus its character size: '$KEYWORD_TFORM$i' that is $columnCharSize can not fit on expected row size: $expectedRowCharSize"
          , sectionName = SectionName.SECTION_7_2_1)
      }
      columnCharSize
    }).toArray

    //check the the sum of sizes od all columns can fit in the row definition
    val totalCharSizeInColumn = colCharSizeSeq.sum
    if (expectedRowCharSize < totalCharSizeInColumn) {
      generateDisconfFatal(ctxt
        , s"the value of ${KEYWORD_NAXIS}1:$expectedRowCharSize is less than the sum of all column size: $totalCharSizeInColumn"
        , sectionName = SectionName.SECTION_7_2_1)
      true
    } else true
  }
  //---------------------------------------------------------------------------
  def isValidHeader(recordMap: RecordMap, ctxt: FitsCtxt = FitsCtxt()) : Boolean = {

    //mandatory keywords
    if (!CommonTable.isValidTfields(recordMap, ctxt)) return false
    if (!CommonTable.isValidRecordTform(recordMap, SectionName.SECTION_7_2_1, ctxt)) return false
    if (!isValidTbcol(recordMap, ctxt)) return false
    if (!isValidNaxis_1(recordMap, ctxt)) return false

    val colCount = recordMap.getTfields()

    //other reserved keywords (check only the concordance of keyword N with TFIELDS=N)
    if (!HDU.isValidRecordStringSeq(recordMap, SectionName.SECTION_7_2_2, colCount, KEYWORD_TTYPE, ctxt, keywordMustExist = true)) return false
    checkTtypeCharacterRange(recordMap,SectionName.SECTION_7_2_2, ctxt)

    if (!HDU.isValidRecordStringSeq(recordMap, SectionName.SECTION_7_2_2, colCount, KEYWORD_TUNIT, ctxt)) return false
    if (!HDU.isValidRecordFloatSeq(recordMap, SectionName.SECTION_7_2_2, colCount, KEYWORD_TSCAL, ctxt)) return false
    if (!HDU.isValidRecordFloatSeq(recordMap, SectionName.SECTION_7_2_2,  colCount, KEYWORD_TZERO, ctxt)) return false
    if (!HDU.isValidRecordStringSeq(recordMap, SectionName.SECTION_7_2_2, colCount, KEYWORD_TNULL, ctxt)) return false
    if (!HDU.isValidRecordStringSeq(recordMap, SectionName.SECTION_7_2_2, colCount, KEYWORD_TDISP, ctxt)) return false
    if (!HDU.isValidRecordFloatSeq(recordMap, SectionName.SECTION_7_2_2,  colCount, KEYWORD_TDMIN, ctxt)) return false
    if (!HDU.isValidRecordFloatSeq(recordMap, SectionName.SECTION_7_2_2, colCount, KEYWORD_TDMAX, ctxt)) return false
    if (!HDU.isValidRecordFloatSeq(recordMap, SectionName.SECTION_7_2_2, colCount, KEYWORD_TLMIN, ctxt)) return false
    if (!HDU.isValidRecordFloatSeq(recordMap, SectionName.SECTION_7_2_2, colCount, KEYWORD_TLMAX, ctxt)) return false

    if (!isValidTscal(recordMap, colCount, isAsciiTable = true, ctxt)) return false
    if (!isValidTzero(recordMap, colCount, isAsciiTable = true, ctxt)) return false
    if (!isValidMinMax(recordMap, colCount, KEYWORD_TDMIN,  KEYWORD_TDMAX, SectionName.SECTION_7_2_2, ctxt)) return false
    if (!isValidMinMax(recordMap, colCount, KEYWORD_TLMIN,  KEYWORD_TLMAX, SectionName.SECTION_7_2_2, ctxt)) return false

    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import AsciiTable.DUMP_CSV_NAME
case class AsciiTable(recordMap:RecordMap
                      , dataBlockSeq: Array[Block]) extends Extension {
  //---------------------------------------------------------------------------
  val rowCount = recordMap.getAxisSeq()(1)
  val columnCount = recordMap.getTfields()
  private var colDefSeq: Array[AsciiTableColDef] = null
  //---------------------------------------------------------------------------
  def isValid(ctxt: FitsCtxt) = processRowSeq(ctxt, applyLinearTransformation = true)._1
  //---------------------------------------------------------------------------
  //(success,rowSeq)
  private def processRowSeq(ctxt: FitsCtxt
                            , applyLinearTransformation: Boolean
                            , storeResult: Boolean = false
                            , callbackOnEachRow: Option[Array[String] => Boolean] = None)
                             : (Boolean,Array[AsciiTableRow]) = {
    val r = AsciiTableRow.buildRowSeq(recordMap
      , Block.collectAllByteSeq(dataBlockSeq)
      , applyLinearTransformation
      , storeResult
      , callbackOnEachRow
      , ctxt)

    if (r._1 && colDefSeq == null) colDefSeq = r._3
    (r._1,r._2)
  }
  //---------------------------------------------------------------------------
  def dumpTable(oDir: String, applyLinearTransformation: Boolean): Unit = {

    //process the csv header with the column names

    val csvHeader = (for (i <- 1L to columnCount) yield{
      recordMap.getFirstValue[String](KEYWORD_TTYPE + i).getOrElse("")
    }).mkString(DUMP_CSV_COLUMN_DIVIDER)

    val stream = LocalOutputStream(oDir + DUMP_CSV_NAME)
    stream.writeLine(csvHeader)
    //-------------------------------------------------------------------------
    def saveRow(row:Array[String]): Boolean = {
      stream.writeLine(row.mkString(DUMP_CSV_COLUMN_DIVIDER))
      true
    }
    //-------------------------------------------------------------------------
    processRowSeq(FitsCtxt()
                  , applyLinearTransformation
                  , callbackOnEachRow = Some(saveRow))
    stream.close
  }
  //---------------------------------------------------------------------------
  override def getSummary(structurePosition: String, streamPos: Long = 0): String = {
    val colDefAsString = colDefSeq.map { col => col.getDescription().mkString(" ") }
    super.getSummary(structurePosition, streamPos) +
    "\n\t" + "table column definition" +
    "\n\t\t" + s" rows:$rowCount columns:$columnCount" +
    "\n\t" + "-----------------------" +
    "\n\t" + TableColDef.TABLE_ASCII_COL_HEADER +
    "\n\t" + colDefAsString.mkString("\n\t") +
    "\n\t" + "-----------------------"
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file AsciiTable.scala
//=============================================================================
