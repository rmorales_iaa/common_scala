/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Nov/2022
 * Time:  13h:42m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.tform
//=============================================================================
import com.common.fits.standard.Keyword.KEYWORD_TFORM
import com.common.fits.standard.dataType.DataType
import com.common.fits.standard.dataType.DataType.{DATA_TYPE_FLOAT_32, DATA_TYPE_FLOAT_64, DATA_TYPE_INT_64, DATA_TYPE_U_INT_8}
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.tform.AsciiTableTform.{TFORM_POINT_DIVIDER, getRealValue}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.tform.parser.{RealParser, TformParser}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.CommonTable._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.TableColForm
import com.common.util.util.Util
import com.common.util.string.MyString.leftPadding
//=============================================================================
import java.text.DecimalFormat
//=============================================================================
//=============================================================================
object AsciiTableTform {
  //---------------------------------------------------------------------------
  final val TFORM_POINT_DIVIDER = "."
  //---------------------------------------------------------------------------
  final val TFORM_A = "A"
  final val TFORM_I = "I"
  final val TFORM_F = "F"
  final val TFORM_E = "E"
  final val TFORM_D = "D"
  //---------------------------------------------------------------------------
  private final val TFORM_EXPONENTIAL_LETTER_D = "D"
  private final val TFORM_EXPONENTIAL_LETTER_E = "E"
  //---------------------------------------------------------------------------
  def getDataType(form: AsciiTableTform): DataType[_] =
    form match {
      case _: AsciiTableTformCharacter  => DATA_TYPE_U_INT_8
      case _: AsciiTableTformInteger    => DATA_TYPE_INT_64
      case _: AsciiTableTformFloatFixed => DATA_TYPE_FLOAT_32
      case _: AsciiTableTformFloat_32   => DATA_TYPE_FLOAT_32
      case _: AsciiTableTformFloat_64   => DATA_TYPE_FLOAT_64
    }
  //---------------------------------------------------------------------------
  def build(form: String
            , index: Long
            , ctxt: FitsCtxt): Option[AsciiTableTform] = {
    val r = TformParser.run(form)
    if (r.isInstanceOf[AsciiTableTformError]) {
      generateDisconfFatal(ctxt
        , s"the value: '${form}' of keyword: '$KEYWORD_TFORM$index' is not valid"
        , additionalInfo = r.asInstanceOf[AsciiTableTformError].message)
      None
    }
    else Some(r)
  }
  //---------------------------------------------------------------------------
  def getRealValue(s: String
                   , colIndex: Long
                   , charSize: Long
                   , digitCount: Int
                   , isFloat32B: Boolean
                   , ctxt: FitsCtxt): Option[String] = {
    if (s.length != charSize) return None
    var simpleS = s.replace(TFORM_EXPONENTIAL_LETTER_D,TFORM_EXPONENTIAL_LETTER_E).trim

    //check if it is empty
    if (simpleS.isEmpty) {
      generateDisconfFatal(ctxt
        , s"ASCII table keyword '$KEYWORD_TFORM$colIndex' .The input '$s' is not a valid real number because it is empty. Row ${ctxt.position}: '${ctxt.info}'"
        , SectionName.SECTION_7_2_5)
    }

    //check the syntax
    val (realValue,errorParsingRealValue) = RealParser.run(simpleS.toUpperCase())
    if (realValue.isEmpty) {
      generateDisconfFatal(ctxt
        , s"ASCII table keyword '$KEYWORD_TFORM$colIndex' .The input '$s' is not a valid real number. Row ${ctxt.position}: '${ctxt.info}'"
        , additionalInfo = errorParsingRealValue
        , SectionName.SECTION_7_2_5)
    }

    if (!realValue.get.decimalPointAtIntegerPart && realValue.get.decimalPart.isEmpty) {
      val leftPart = simpleS.dropRight(digitCount).trim
      val rightPart = leftPadding(simpleS.takeRight(digitCount).replaceAll(" ", "0"), digitCount, paddingChar = "0")
      //append the implicit decimal point
      simpleS = (if (leftPart.isEmpty) "0" else leftPart) + TFORM_POINT_DIVIDER + rightPart
      generateDisconfWarn(ctxt
        , s"ASCII table keyword '$KEYWORD_TFORM$colIndex' .The input '$s' has not a decimal point divider at the integer part. Assuming it at position: '$digitCount' resulting: '$simpleS'.Row ${ctxt.position}: '${ctxt.info}'"
        , SectionName.SECTION_7_2_5)
    }

    isFloat32B match {
      case true => if (Util.isFloat(simpleS)) Some(simpleS) else None
      case false => if (Util.isDouble(simpleS)) Some(simpleS) else None
    }
  }
  //---------------------------------------------------------------------------
  def getStringValue(s: String): Option[String] = {
    s.foreach { c => if (c < 0x20 || c > 0x7E) return None }
    Some(s)
  }
  //---------------------------------------------------------------------------
  def getLongValue(s: String
                   , charSize: Long): Option[String] = {
    if (s.length != charSize) return None
    val simpleS = s.trim
    if (simpleS.isEmpty) return Some("0")
    if (!Util.isLong(simpleS)) None
    else Some(simpleS)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================

import AsciiTableTform._
trait AsciiTableTform extends TableColForm {
  //---------------------------------------------------------------------------
  val charSize: Long
  //---------------------------------------------------------------------------
  //used in binary tables
  def getValue(byteSeq: ByteArr
               , colIndex: Long
               , ctxt: FitsCtxt): Option[Array[_]] = None
  //---------------------------------------------------------------------------
  //used in binary tables
  def format(s: Array[Byte], ctxt: FitsCtxt): String = ""
  //---------------------------------------------------------------------------
}
//=============================================================================
import AsciiTableTform._
trait AsciiTableTformRealNumber extends AsciiTableTform {
  //---------------------------------------------------------------------------
  val fractionDigitCount: Int
  val id: String
  val isFloat32 : Boolean
  val formatting: DecimalFormat
  //---------------------------------------------------------------------------
  def getValue(): String = s"$id$charSize$TFORM_POINT_DIVIDER$fractionDigitCount"
  //---------------------------------------------------------------------------
  def getValue(s: String
               , colIndex: Long
               , ctxt: FitsCtxt): Option[String] =
    getRealValue(s
      , colIndex
      , charSize
      , fractionDigitCount
      , isFloat32
      , ctxt)
  //---------------------------------------------------------------------------
  def format(s: String, ctxt: FitsCtxt): String = {
    val validS =
      getRealValue(
        leftPadding(s.trim,charSize.toInt)
        , colIndex = -1
        , charSize
        , fractionDigitCount
        , isFloat32
        , ctxt).getOrElse(return "")
    leftPadding(formatting.format(BigDecimal(validS)),charSize.toInt)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class AsciiTableTformCharacter(charSize: Long) extends AsciiTableTform {
  //---------------------------------------------------------------------------
  val dataType = DataType.DATA_TYPE_U_INT_8
  //---------------------------------------------------------------------------
  def getValue(): String = s"$TFORM_A$charSize"
  //---------------------------------------------------------------------------
  def getValue(s:String
               , colIndex: Long
               , ctxt: FitsCtxt): Option[String] = {
    s.foreach { c =>
      if (c < 0x20 || c > 0x7E) {
        generateDisconfFatal(ctxt
          , s"ASCII table keyword '${getValue()}' .The input '$s' is not a valid string. Row ${ctxt.position}: '${ctxt.info}'"
          , SectionName.SECTION_7_2_5)
        return None
      }
    }
    Some(s)
  }
  //---------------------------------------------------------------------------
  def format(s: String, ctxt: FitsCtxt): String =
    leftPadding(s.take(charSize.toInt),charSize.toInt)
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class AsciiTableTformInteger(charSize: Long) extends AsciiTableTform {
  //---------------------------------------------------------------------------
  val dataType = DataType.DATA_TYPE_FLOAT_64
  //---------------------------------------------------------------------------
  def getValue(): String = s"$TFORM_I$charSize"
  //---------------------------------------------------------------------------
  def getValue(s: String
               , colIndex: Long
               , ctxt: FitsCtxt): Option[String] = {

    val r = AsciiTableTform.getLongValue(leftPadding(s,charSize.toInt), charSize)
    if (r.isEmpty){
      generateDisconfFatal(ctxt
        , s"ASCII table keyword '${getValue()}' .The input '$s' is not a valid integer. Row ${ctxt.position}: '${ctxt.info}'"
        , SectionName.SECTION_7_2_5)
      None
    }
    else r
  }

  //---------------------------------------------------------------------------
  def format(s: String, ctxt: FitsCtxt): String =
    leftPadding(s.take(charSize.toInt),charSize.toInt)
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class AsciiTableTformFloatFixed(charSize: Long, fractionDigitCount: Int) extends AsciiTableTformRealNumber {
  //---------------------------------------------------------------------------
  val dataType = DataType.DATA_TYPE_FLOAT_64
  //---------------------------------------------------------------------------
  val id = TFORM_F
  val isFloat32 = false
  val formatting: DecimalFormat = buildFloatFixedPoint(fractionDigitCount)
}
//-----------------------------------------------------------------------------
sealed case class AsciiTableTformFloat_32(charSize: Long, fractionDigitCount: Int) extends AsciiTableTformRealNumber {
  //---------------------------------------------------------------------------
  val dataType = DataType.DATA_TYPE_FLOAT_32
  //---------------------------------------------------------------------------
  val id = TFORM_E
  val isFloat32 = true
  val formatting: DecimalFormat =
    buildExponentialFormat(charSize - fractionDigitCount - 3  //take into account the sign, dot and letter "E"
                           , exponentfractionDigitCount = fractionDigitCount)
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------

sealed case class AsciiTableTformFloat_64(charSize: Long, fractionDigitCount: Int) extends AsciiTableTformRealNumber {
  //---------------------------------------------------------------------------
  val dataType = DataType.DATA_TYPE_FLOAT_64
  //---------------------------------------------------------------------------
  val id = TFORM_D
  val isFloat32 = false
  val formatting: DecimalFormat =
    buildExponentialFormat(charSize - fractionDigitCount - 3 //take into account the sign, dot and letter "E"
                          , exponentfractionDigitCount = fractionDigitCount)
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class AsciiTableTformError(message: String) extends AsciiTableTform {
  //---------------------------------------------------------------------------
  val dataType = DataType.DATA_TYPE_UNKNOWN
  //---------------------------------------------------------------------------
  val charSize = Long.MinValue
  //---------------------------------------------------------------------------
  def getValue(): String = message
  //---------------------------------------------------------------------------
  def getValue(s: String
               , colIndex: Long
               , ctxt: FitsCtxt) = None
  //---------------------------------------------------------------------------
  def format(s: String, ctxt: FitsCtxt):String = ""
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file AsciiTableTform.scala
//=============================================================================
