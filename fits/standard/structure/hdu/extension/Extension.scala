/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Sep/2022
 * Time:  16h:52m
 * Description: FITS standard: sectionName 3.1 Overall file structure
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.extension
//=============================================================================
import com.common.fits.standard.block.Block
import com.common.fits.standard.block.record.Record
import com.common.fits.standard.block.record.Record.RecordSeq
import com.common.fits.standard.block.record.RecordNumber.RecordInteger
import com.common.fits.standard.block.record.RecordCharacter.RecordString
import com.common.fits.standard.Keyword._
import com.common.fits.standard.block.dataIntegrity.DataIntegrity.PartialDataSum
import com.common.fits.standard.dataType.{DataType, Tzero}
import com.common.fits.standard.disconformity.{DisconfFatal, DisconfWarn}
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.block.record.RecordMap.{BSCALE_DEFAULT_VALUE, BZERO_DEFAULT_VALUE}
import com.common.fits.standard.block.record.verifier.RecordVerifier
import com.common.fits.standard.block.record.verifier.section_10.Section_10
import com.common.fits.standard.block.record.verifier.section_8.Section_8
import com.common.fits.standard.block.record.verifier.section_9.Section_9
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.sectionName.SectionName.SECTION_4_4_2_5
import com.common.fits.standard.structure.hdu.HDU
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.AsciiTable
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.BinTable
import com.common.fits.standard.structure.hdu.extension.conforming.standard.image.Image
import com.common.fits.standard.structure.hdu.extension.conforming.{A3dtable, Dump, Foreign, Iueimage}
import com.common.stream.MyInputStream
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object Extension {
  //standard
  final val EXTENSION_STANDARD_IMAGE_NAME       = "IMAGE   "
  final val EXTENSION_STANDARD_ASCII_TABLE_NAME = "TABLE   "
  final val EXTENSION_STANDARD_BINTABLE_NAME    = "BINTABLE"

  //conforming
  final val EXTENSION_CONFORMING_IUEIMAGE_NAME  = "IUEIMAGE"
  final val EXTENSION_CONFORMING_A3DTABLE_NAME  = "A3DTABLE"
  final val EXTENSION_CONFORMING_FOREIGN_NAME   = "FOREIGN "
  final val EXTENSION_CONFORMING_DUMP_NAME      = "DUMP    "
  //----------------------------------------------------------------------------
  final val CONFORMING_EXTENSION_NAME_SEQ = Seq(
      EXTENSION_CONFORMING_IUEIMAGE_NAME
    , EXTENSION_CONFORMING_A3DTABLE_NAME
    , EXTENSION_CONFORMING_FOREIGN_NAME
    , EXTENSION_CONFORMING_DUMP_NAME
  )
  //---------------------------------------------------------------------------
  final val STANDARD_EXTENSION_NAME_SEQ = Seq(
      EXTENSION_STANDARD_IMAGE_NAME
    , EXTENSION_STANDARD_ASCII_TABLE_NAME
    , EXTENSION_STANDARD_BINTABLE_NAME
  )
  //---------------------------------------------------------------------------
  final val ALL_EXTENSION_NAME_SEQ = STANDARD_EXTENSION_NAME_SEQ ++ CONFORMING_EXTENSION_NAME_SEQ
  //---------------------------------------------------------------------------
  def isValidExtensionName(extensionName: String) = ALL_EXTENSION_NAME_SEQ.contains(extensionName)
  //---------------------------------------------------------------------------
  private final val MANDATORY_RECORD_SEQ_1 = Array(
      KEYWORD_XTENSION
    , KEYWORD_BITPIX
    , KEYWORD_NAXIS
  )
  //----------------------------------------------------------------------------
  private final val MANDATORY_RECORD_SEQ_2 = Array(
      KEYWORD_PCOUNT
    , KEYWORD_GCOUNT
  )
  //----------------------------------------------------------------------------
  final val EXTENSION_NOT_ALLOWED_KEYWORD = Array(
      (KEYWORD_SIMPLE,  SectionName.SECTION_4_4_1_1)
    , (KEYWORD_EXTEND,  SectionName.SECTION_4_4_2_1)
    , (KEYWORD_BLOCKED, SectionName.SECTION_7_1_2)
  )
  //---------------------------------------------------------------------------
  final val KEYWORD_EXTVER_DEFAULT_VALUE   = 1L
  final val KEYWORD_EXTLEVEL_DEFAULT_VALUE = 1L
  //---------------------------------------------------------------------------
  def getDataBlockByteSize(recordMap: RecordMap) =
    Math.abs(recordMap.getBytePerPix()) *
      recordMap.getGcount() *
      (recordMap.getPcount() + recordMap.getAxisTotalItemCount())
  //---------------------------------------------------------------------------
  private def isValidBitPix(recordMap: RecordMap
                            , ctxt: FitsCtxt): Boolean = {
    if (!HDU.isValidRecordBitPix(recordMap, ctxt)) return false

    recordMap.getExtensionName match {
      case EXTENSION_STANDARD_ASCII_TABLE_NAME |
           EXTENSION_STANDARD_BINTABLE_NAME =>
        if (recordMap.getBitPix() != DataType.DATA_TYPE_U_INT_8.bitSize) {
          DisconfFatal(
            ctxt
            , s"ASCII table structure: ${ctxt.structureID} the keyword:'$KEYWORD_BITPIX' must be equal to ${DataType.DATA_TYPE_ID_UINT_8}"
            , SectionName.SECTION_7_2_1)
          true
        }
        else true
      case _ => true
    }
  }
  //---------------------------------------------------------------------------
  private def isValidNaxis(recordMap: RecordMap
                           , ctxt: FitsCtxt): Boolean = {
    if (!HDU.isValidRecordNaxis(recordMap, ctxt)) return false

    recordMap.getExtensionName match {
      case EXTENSION_STANDARD_ASCII_TABLE_NAME |
           EXTENSION_STANDARD_BINTABLE_NAME =>
        if (recordMap.getNaxis() != 2L) {
          DisconfFatal(
            ctxt
            , s"ASCII table structure: ${ctxt.structureID} the keyword:'$KEYWORD_NAXIS' must be equal to 2"
            , SectionName.SECTION_7_2_1)
          false
        }
        else true
      case _ => true
    }
  }
  //----------------------------------------------------------------------------
  def isValidExtensionRecord(recordMap: RecordMap
                             , ctxt: FitsCtxt): Boolean = {
    val record = recordMap.getFirstRecord(KEYWORD_XTENSION).getOrElse {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_4_4_1_2
        , s"Structure: ${ctxt.structureID} can not find the keyword: '$KEYWORD_XTENSION'")
      return false
    }
    val extensionName = record.value.toString
    if (!isValidExtensionName(record.value.toString)) {
      DisconfFatal(
        ctxt
        , s"Structure: ${ctxt.structureID} has an invalid extension name :'$extensionName' valid extension names: ${ALL_EXTENSION_NAME_SEQ.mkString("{",",","}")}"
        , SectionName.SECTION_4_4_1_2)
      return false
    }
    true
  }
  //---------------------------------------------------------------------------
  def isValidPcountRecord(recordMap: RecordMap
                          , ctxt: FitsCtxt
                          , extensionName: String): Boolean = {
    val record = recordMap.getFirstRecord(KEYWORD_PCOUNT).getOrElse {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_4_4_1_2
        , s"Can not find the keyword: '$KEYWORD_PCOUNT'")
      return false
    }
    if (!HDU.isNonNegativeIntegerRecord(record, SectionName.SECTION_4_4_1_2, ctxt)) return false
    val value = recordMap.getPcount
    extensionName match {
      case EXTENSION_STANDARD_IMAGE_NAME |
           EXTENSION_STANDARD_ASCII_TABLE_NAME  =>
        if (value != 0) {
          DisconfFatal(
            ctxt
            , SectionName.SECTION_4_4_1_2
            , s"Structure: ${ctxt.structureID} the value of keyword: '$KEYWORD_PCOUNT' must be 0 but it has the value: $value")
          return false
        }
        true
      case _ => true
    }
  }
  //---------------------------------------------------------------------------
  def isValidGcountRecord(recordMap: RecordMap
                          , ctxt: FitsCtxt
                          , extensionName: String): Boolean = {
    val record = recordMap.getFirstRecord(KEYWORD_GCOUNT).getOrElse {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_4_4_1_2
        , s"Structure: ${ctxt.structureID} can not find the keyword: '$KEYWORD_GCOUNT'")
      return false
    }
    if (!HDU.isNonNegativeIntegerRecord(record, SectionName.SECTION_4_4_1_2, ctxt)) return false
    val value = recordMap.getGcount
    extensionName match {
      case EXTENSION_STANDARD_IMAGE_NAME |
           EXTENSION_STANDARD_ASCII_TABLE_NAME |
           EXTENSION_STANDARD_BINTABLE_NAME =>
        if (value != 1) {
          DisconfFatal(
            ctxt
            , SectionName.SECTION_4_4_1_2
            , s"Structure: ${ctxt.structureID} the value of keyword: '$KEYWORD_GCOUNT' must be 1 but it has the value: $value")
          return false
        }
        true
      case _ => true
    }
  }

  //---------------------------------------------------------------------------
  def isValidBlankCommon(recordMap: RecordMap
                         , ctxt: FitsCtxt
                         , key:String
                         , scaleKey: String
                         , zeroKey: String
                         , sectionID: String): Boolean = {

    val bitPix = recordMap.getBitPix().toInt
    val record = recordMap.getFirstRecord(key).getOrElse(return true)

    if (!Tzero.isValid(bitPix)) {
      DisconfFatal(
        ctxt
        , sectionID
        , s"Structure: ${ctxt.structureID} the value of keyword: '$key' can not be define for bit pix:'$bitPix'")
      return false
    }

    //check TSCAL and TZERO
    val tScale = recordMap.getBscale()
    val tZero = recordMap.getBzero()

    if ((tScale != BSCALE_DEFAULT_VALUE) ||
        (tZero != BZERO_DEFAULT_VALUE)) {

      val recordValue = record.value.toString.toLong
      val tZero = Tzero.getTzero(bitPix)

      if (recordValue != tZero.undefinedValue) {
        DisconfFatal(
          ctxt
          , sectionID
          , s"Structure: ${ctxt.structureID} the keyword: '$key' with value:'${record.value.toString}' must be equal to an undefined element value:'$tZero'"
        )
        return false
      }
    }
    true
  }
  //---------------------------------------------------------------------------
  def isValidBlank(recordMap: RecordMap
                   , ctxt: FitsCtxt): Boolean =
    isValidBlankCommon(recordMap
                       , ctxt
                       , key        = KEYWORD_BLANK
                       , scaleKey   = KEYWORD_TSCAL
                       , zeroKey    = KEYWORD_TZERO
                       , sectionID  = SECTION_4_4_2_5)
  //---------------------------------------------------------------------------
  private def isValidInheritRecord(recordMap: RecordMap
                                   , ctxt: FitsCtxt): Boolean = {
    if (recordMap.contains(KEYWORD_INHERIT)) {
      val posGcount = recordMap.getFirstRecord(KEYWORD_GCOUNT).get.id
      val posInherit = recordMap.getFirstRecord(KEYWORD_INHERIT).get.id
      if ((posGcount + 1) != posInherit)
        DisconfWarn(
          ctxt
          , s"Structure: ${ctxt.structureID} the keyword:'$KEYWORD_INHERIT' does not follow the keyword: '$KEYWORD_GCOUNT'"
          , SectionName.APPENDIX_K)
    }
    true
  }
  //---------------------------------------------------------------------------
  private def isValidRecordOrder(recordMap: RecordMap
                                 , ctxt: FitsCtxt) : Boolean = {
    if (!HDU.isValidRecordOrder(recordMap
      , MANDATORY_RECORD_SEQ_1
      , MANDATORY_RECORD_SEQ_2
      , SectionName.SECTION_4_4_1_2
      , ctxt))
      return false
    val extensionName = recordMap.getExtensionName()
    extensionName match {
      case EXTENSION_STANDARD_ASCII_TABLE_NAME |  EXTENSION_STANDARD_BINTABLE_NAME =>
        val gcCountPos = recordMap.getFirstPosition(KEYWORD_GCOUNT)
        val tFieldsPos = recordMap.getFirstPosition(KEYWORD_TFIELDS)
        if ((gcCountPos + 1) != tFieldsPos) {
          DisconfWarn(
            ctxt
            , s"ASCII table structure: ${ctxt.structureID} the keyword:'$KEYWORD_TFIELDS' does not follow the keyword: '$KEYWORD_GCOUNT'"
            , (if (extensionName == EXTENSION_STANDARD_ASCII_TABLE_NAME) SectionName.SECTION_7_2_1
              else SectionName.SECTION_7_3_1)
          )
          false
        }
        else true
      case _ => true
    }
  }
  //---------------------------------------------------------------------------
  private def additionalCheck(recordMap: RecordMap
                              , ctxt: FitsCtxt) : Boolean = {
    //check value of 'XTENSION'
    recordMap.getExtensionName match {
      case EXTENSION_STANDARD_ASCII_TABLE_NAME =>
        AsciiTable.isValidHeader(recordMap, ctxt)
      case EXTENSION_STANDARD_BINTABLE_NAME =>
        BinTable.isValidHeader(recordMap, ctxt)
      case _ => true
    }
  }
  //---------------------------------------------------------------------------
  private def isValidHeader(recordMap: RecordMap
                           , ctxt: FitsCtxt): Boolean = {
    if (!isValidExtensionRecord(recordMap,ctxt)) return false
    if (!isValidRecordOrder(recordMap,ctxt)) return false

    //check the presence of not allowed keywords
    EXTENSION_NOT_ALLOWED_KEYWORD.foreach { case (keyword, section) =>
      if (recordMap.contains(keyword)) {
        DisconfFatal(
          ctxt
          , section
          , s"Structure: ${ctxt.structureID} contains the keyword:'$keyword' but it is not allowed in the extension structure")
        return false
      }
    }
    val extensionName = recordMap.getExtensionName()
    if (!isValidBitPix(recordMap, ctxt)) return false
    if (!isValidNaxis(recordMap, ctxt)) return false
    if (!HDU.isValidRecordAxisN(recordMap, ctxt)) return false
    if (!isValidPcountRecord(recordMap, ctxt, extensionName)) return false
    if (!isValidGcountRecord(recordMap, ctxt, extensionName)) return false
    if (!isValidBlank(recordMap, ctxt)) return false
    if (!HDU.isValidRecordEnd(recordMap, ctxt)) return false
    if (!additionalCheck(recordMap, ctxt)) return false

    isValidInheritRecord(recordMap, ctxt)
   }
  //---------------------------------------------------------------------------
  def build(recordMap:RecordMap
            , dataBlockSeq: Array[Block]
            , ctxt: FitsCtxt): Option[Extension] = {
    val extensionName = recordMap.getFirstValue[String](KEYWORD_XTENSION).get
    extensionName match {
      case EXTENSION_CONFORMING_IUEIMAGE_NAME   => Some(Iueimage(recordMap, dataBlockSeq))
      case EXTENSION_CONFORMING_A3DTABLE_NAME   => Some(A3dtable(recordMap, dataBlockSeq))
      case EXTENSION_CONFORMING_FOREIGN_NAME    => Some(Foreign(recordMap, dataBlockSeq))
      case EXTENSION_CONFORMING_DUMP_NAME       => Some(Dump(recordMap, dataBlockSeq))
      case EXTENSION_STANDARD_IMAGE_NAME        =>
        val img = Image(recordMap, dataBlockSeq)
        RecordVerifier.checkDeprecatedKeywordSeq(recordMap, ctxt)
        if (!Section_8.verify(recordMap,ctxt)) return None
        if (!Section_9.verify(recordMap,ctxt)) return None
        Some(img)
      case EXTENSION_STANDARD_ASCII_TABLE_NAME  =>
        val ext = AsciiTable(recordMap, dataBlockSeq)
        if (!ext.isValid(ctxt)) return None
        Some(ext)
      case EXTENSION_STANDARD_BINTABLE_NAME     =>
        val ext = BinTable(recordMap, dataBlockSeq)
        if (!Section_10.verify(recordMap,ctxt)) return None
        if (!ext.isValid(ctxt)) return None
        Some(ext)
    }
  }
  //---------------------------------------------------------------------------
  def build(stream: MyInputStream
            , headerBlockStorage: ArrayBuffer[Block]
            , hduHeaderDataSum: PartialDataSum
            , ctxt: FitsCtxt
            , readDataBlockFlag: Boolean): Option[Extension] = {
    val recordMap = Block.readHeader(stream
                                    , hduHeaderDataSum
                                    , ctxt
                                    , headerBlockStorage).getOrElse(return None)
    if (!isValidHeader(recordMap, ctxt))  return None
    val dataBlockSeq = Block.readAllDataBlock(
        stream
      , getDataBlockByteSize(recordMap)
      , ctxt
      , readDataBlockFlag
      , isExtensionTableAscii = recordMap.getExtensionName() == EXTENSION_STANDARD_ASCII_TABLE_NAME).getOrElse(return None)
    build(recordMap, dataBlockSeq, ctxt)
  }
  //---------------------------------------------------------------------------
  def buildHeader(bitPix: Int
                  , axisSeq: Array[Long]
                  , recordSeq: RecordSeq
                  , extName: String
                  , pcount: Int
                  , gcount: Int) = {
    assert(isValidExtensionName(extName), s"'$extName' is not a valid extension name")
    RecordMap(Array(
      RecordString(keyword = KEYWORD_XTENSION, value = extName)
      , RecordInteger(KEYWORD_BITPIX, bitPix, "number of bits per data item")
      , RecordInteger(KEYWORD_NAXIS, axisSeq.length, "number of data axes")
      ) ++
      Record.getNaxisRecordSeq(axisSeq) ++
      Array(
        RecordInteger(keyword = KEYWORD_PCOUNT, pcount)
        , RecordInteger(keyword = KEYWORD_GCOUNT, gcount)
      ) ++
      recordSeq ++
      Array(RecordString(keyword = KEYWORD_END, value = ""))
    )
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
trait Extension extends HDU {
  //---------------------------------------------------------------------------
  override def getSummary(structurePosition: String, streamPos: Long = 0) = {
    super.getSummary(structurePosition,streamPos) +
    (if (recordMap.contains(KEYWORD_PCOUNT))   s"\n      PCOUNT                              : ${recordMap.getPcount()}" else "") +
    (if (recordMap.contains(KEYWORD_GCOUNT))   s"\n      GCOUNT                              : ${recordMap.getGcount()}" else "") +
    s"\n    ----------------------------------------------"
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file extension.scala
//=============================================================================
