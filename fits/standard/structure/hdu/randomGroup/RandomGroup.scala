/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Oct/2022
 * Time:  19h:52m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.randomGroup
//=============================================================================
import com.common.fits.standard.block.Block
import com.common.fits.standard.block.record.Record.RecordSeq
import com.common.fits.standard.block.record.RecordNumber.{RecordInteger, RecordLogical}
import com.common.fits.standard.block.record.RecordCharacter.RecordString
import com.common.fits.standard.Keyword._
import com.common.fits.standard.disconformity.DisconfFatal
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.hdu.HDU
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.tdisp.Tdisp
import com.common.fits.standard.structure.hdu.primary.Primary
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object RandomGroup {
  //----------------------------------------------------------------------------
  final val RecordLogicalGroups = RecordLogical(
      KEYWORD_GROUPS
    , true
    , "random group is present")
  //----------------------------------------------------------------------------
  def canBeRandomGroup(recordMap: RecordMap): Boolean =
    recordMap.contains(KEYWORD_GROUPS)
  //----------------------------------------------------------------------------
  private def getDataAxisItemCount(recordMap: RecordMap) = {
    val nAxisSeq = recordMap.getAxisSeq().drop(1) //NAXIS_1 is zero
    if (nAxisSeq.isEmpty) 0L else nAxisSeq.product
  }
  //---------------------------------------------------------------------------
  def getDataBlockByteSize(recordMap: RecordMap) = {
    val axisItemCount = getDataAxisItemCount(recordMap)
    Math.abs(recordMap.getBytePerPix()) *
      recordMap.getGcount() *
      (recordMap.getPcount() + axisItemCount)
  }
  //---------------------------------------------------------------------------
  private def getGroupDataByteSize(recordMap: RecordMap) = {
    val parameterCount = recordMap.getPcount()
    val dataType = recordMap.getDataType()
    val dataTypeByteSize = dataType.byteSize
    val parameterByteSize = parameterCount * dataTypeByteSize
    val dataByteSize = getDataAxisItemCount(recordMap) * dataTypeByteSize
    parameterByteSize + dataByteSize
  }
  //----------------------------------------------------------------------------
  private def generateDisconformity(ctxt: FitsCtxt) = {
    DisconfFatal(
      ctxt
      , SectionName.SECTION_6_1_1
      , s"Structure: ${ctxt.structureID} have not the expected value for keyword:'$KEYWORD_GROUPS'. Expecting: 'T' but found 'F'")
    false
  }
  //----------------------------------------------------------------------------
  private def isValidPtype(recordMap: RecordMap, ctxt: FitsCtxt) = {
    val recordSeq = recordMap.getAllRecordWithPrefix(KEYWORD_PTYPE)
    recordSeq.forall{ record =>
        HDU.isValidStringRecord(
            record
          , SectionName.SECTION_6_1_2
          , ctxt)
     }
  }
  //----------------------------------------------------------------------------
  private def isValidPscal(recordMap: RecordMap, ctxt: FitsCtxt) : Boolean = {
    val groupCount = recordMap.getGcount()
    val recordSeq = recordMap.getAllRecordWithPrefix(KEYWORD_PSCAL)
    if (recordSeq.isEmpty) return true
    if (groupCount != recordSeq.length) {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_6_1_2
        , s"Structure: ${ctxt.structureID} is a 'Random group' but the group count: $groupCount does not match with the number of keywords '$KEYWORD_PSCAL': ${recordSeq.length}")
      return false
    }
    recordSeq.forall { record =>
      HDU.isValidFloatRecord(
        record
        , SectionName.SECTION_6_1_2
        , ctxt)
    }
  }
  //----------------------------------------------------------------------------
  private def isValidPzero(recordMap: RecordMap, ctxt: FitsCtxt): Boolean = {
    val groupCount = recordMap.getGcount()
    val recordSeq = recordMap.getAllRecordWithPrefix(KEYWORD_PZERO)
    if (recordSeq.isEmpty) return true
    if (groupCount != recordSeq.length) {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_6_1_2
        , s"Structure: ${ctxt.structureID} is a 'Random group' but the group count: $groupCount does not match with the number of keywords '$KEYWORD_PZERO': ${recordSeq.length}")
      return false
    }
    recordSeq.forall { record =>
      HDU.isValidFloatRecord(
        record
        , SectionName.SECTION_6_1_2
        , ctxt)
    }
  }
  //----------------------------------------------------------------------------
  private def isValidDataSize(recordMap: RecordMap
                              , dataBlockSeq: Array[Block]
                              , ctxt: FitsCtxt) = {
    val groupCount = recordMap.getGcount()
    val groupByteSize = getGroupDataByteSize(recordMap)
    val expectedDataByteSize = groupCount * groupByteSize
    val storedDataByteSize = Block.getBlockByteSize(dataBlockSeq.length)

    if(storedDataByteSize < expectedDataByteSize) {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_6_2
        , s"Structure: ${ctxt.structureID} is a 'Random group' and have not the expected parameter data length. Expected: $storedDataByteSize but found: $storedDataByteSize")
      false
    }
    else true
  }
  //----------------------------------------------------------------------------
  //it must be call after 'isValidHeader' of primary HDU
  def isValidHeader(recordMap: RecordMap
                    , dataBlockSeq: Array[Block]
                    , ctxt: FitsCtxt): Boolean = {
    val groupsPos = recordMap.getFirstPosition(KEYWORD_GROUPS)
    if (groupsPos == 0) return generateDisconformity(ctxt)

    val pcCountPos = recordMap.getFirstPosition(KEYWORD_PCOUNT)
    if (pcCountPos == 0) return generateDisconformity(ctxt)

    val gcCountPos = recordMap.getFirstPosition(KEYWORD_GCOUNT)
    if (gcCountPos == 0) return generateDisconformity(ctxt)

    //check if (KEYWORD_GROUPS,KEYWORD_PCOUNT,KEYWORD_GCOUNT) are one after other
    if ((groupsPos + 1) != pcCountPos)  return generateDisconformity(ctxt)
    if ((pcCountPos + 1) != gcCountPos)  return generateDisconformity(ctxt)

    //check NAXIS
    if (recordMap.getNaxis() < 2) return generateDisconformity(ctxt)
    if (recordMap.getFirstValue[Long](KEYWORD_NAXIS+1).get != 0) return generateDisconformity(ctxt)

    //check group
    if (!HDU.isValidTrueLogicalRecord(
        recordMap.getFirstRecord(KEYWORD_GROUPS).get
      , SectionName.SECTION_6_1_1
      , ctxt)) return generateDisconformity(ctxt)

    if (!isValidPtype(recordMap, ctxt)) return false
    if (!isValidPscal(recordMap, ctxt)) return false
    if (!isValidPzero(recordMap, ctxt)) return false
    if (!isValidDataSize(recordMap, dataBlockSeq, ctxt)) return false
    true
  }
  //----------------------------------------------------------------------------
  private def getParameterNameSeq(recordMap: RecordMap) = {
    recordMap.getAllRecordWithPrefix(KEYWORD_PTYPE).sortWith(_.id < _.id) map { record =>
      record.asInstanceOf[RecordString].value
    }
  }
  //---------------------------------------------------------------------------
  private def buildGroupSeq(recordMap:RecordMap, byteSeq: Array[Byte]): Array[Group] = {
    val groupCount = recordMap.getGcount()
    val paremeterCount = recordMap.getPcount()
    val dataType = recordMap.getDataType()
    val dataTypeByteSize = dataType.byteSize
    val parameterNameSeq = getParameterNameSeq(recordMap)
    val nAxisSeq = recordMap.getAxisSeq().drop(1)  //NAXIS_1 is zero
    val parameterByteSize = paremeterCount * dataTypeByteSize
    val dataByteSize = getDataAxisItemCount(recordMap) * dataTypeByteSize
    val groupByteSize = parameterByteSize + dataByteSize
    var remainByteSeqCount = groupCount * groupByteSize
    val groupSeq = ArrayBuffer[Group]()

    byteSeq.grouped(groupByteSize.toInt).zipWithIndex.foreach { case (seq,i) =>
      val scale = recordMap.getFirstValueOrDefault[Double](KEYWORD_PSCAL + i, 1).get
      val zero = recordMap.getFirstValueOrDefault[Double](KEYWORD_PZERO + i, 0).get

      val group = Group(
          parameterNameSeq
        , seq.take(parameterByteSize.toInt)
        , zero
        , scale
        , nAxisSeq
        , seq.drop(parameterByteSize.toInt)
        , dataType)

      groupSeq += group
      remainByteSeqCount -= groupByteSize
      if (remainByteSeqCount == 0) return groupSeq.toArray
    }
    groupSeq.toArray
  }
  //---------------------------------------------------------------------------
  def buildHeader(bitPix: Int
                  , axisSeq: Array[Long]
                  , recordSeq: RecordSeq = Array()
                  , randoGroupParameterNameSeq: Array[String]
                  , randomGroupCount: Long) = {
    val finalRecordSeq =
      Array(
        RandomGroup.RecordLogicalGroups
        , RecordInteger(KEYWORD_PCOUNT, randoGroupParameterNameSeq.length, "number of parameters on each group")
        , RecordInteger(KEYWORD_GCOUNT, randomGroupCount, "number of groups")
      ) ++ randoGroupParameterNameSeq.zipWithIndex.map { case (name, i) => RecordString(KEYWORD_PTYPE + (i + 1), name, s"parameter ${i+1}")}

    Primary.buildHeader(bitPix
      , axisSeq
      , finalRecordSeq ++ recordSeq)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import RandomGroup._
case class RandomGroup(recordMap:RecordMap, dataBlockSeq: Array[Block]) extends HDU {
  //---------------------------------------------------------------------------
  val groupSeq = buildGroupSeq(recordMap,Block.collectAllByteSeq(dataBlockSeq))
  //---------------------------------------------------------------------------
  def getParameterNameSeq() = groupSeq.head.parameterNameSeq
  //---------------------------------------------------------------------------
  def groupCount = groupSeq.length
  //---------------------------------------------------------------------------
  def getString() = {
    s"------------------------------------------------------------------------\n" +
    "parameter name: " + getParameterNameSeq().mkString(Tdisp.VALUE_SEQ_START_CHAR, Tdisp.VALUE_SEQ_DIVIDER, Tdisp.VALUE_SEQ_END_CHAR) + "\n" +
    s"${ groupSeq.zipWithIndex.map  { case (group,i)  => group.getString(i+1) }.mkString("\n") }" +
    s"------------------------------------------------------------------------\n"
  }
  //---------------------------------------------------------------------------
  def getGroupDataByteSize() = RandomGroup.getGroupDataByteSize(recordMap)
  //---------------------------------------------------------------------------
  def getDataByteSize() =
    getGroupDataByteSize() * recordMap.getPcount()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RandomGroup.scala
//=============================================================================
