/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  31/Oct/2022
 * Time:  11h:57m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.randomGroup
//=============================================================================
import com.common.fits.standard.dataType.DataType
import com.common.util.util.Util
//=============================================================================
//=============================================================================
object DataAxis {
  //---------------------------------------------------------------------------
  def apply(axisSeq: List[Long]
            , byteSeq: Array[Byte]
            , dataType: DataType[_]): DataAxis = {
    //-------------------------------------------------------------------------
    def slice(axisSeq: List[Long]
              , dataAxis: DataAxis
              , dataType: DataType[_]): DataAxis = {
      axisSeq match {
        case Nil => dataAxis
        case axis :: (remainAxisSeq: Seq[_]) =>
          val newSlice =
            axis match {
              case 1 => DataAxis(Array(dataAxis))
              case _ =>
                DataAxis((dataAxis.data.grouped(axis.toInt) map (DataAxis(_))).toArray)
            }
          slice(remainAxisSeq, newSlice, dataType)
      }
    }
    //-------------------------------------------------------------------------
    val r = slice(
      axisSeq
      , DataAxis(byteSeq.grouped(dataType.byteSize).toArray)
      , dataType
    )
    r.data.head.asInstanceOf[DataAxis]
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class DataAxis(data: Array[_]) {
  //---------------------------------------------------------------------------
  def size = data.size
  //---------------------------------------------------------------------------
  def getAxisItemSizeSeq(): List[Long] = {
    data match {
      case x: Array[Array[Byte]] => List(x.length.toLong)
      case x: Array[DataAxis] =>
        x.length.toLong +: x.head.getAxisItemSizeSeq()
    }
  }
  //---------------------------------------------------------------------------
  def getHexString(prefix: String = "", index: Int = 1): String = {
    val sStart = s"--------- start axis: $index size: ${data.size} ---------\n"
    val sEnd =   s"--------- end axis: $index size: ${data.size} -----------\n"
    data match {
      case x: Array[Array[Byte]] =>
        prefix + sStart +
        (x map (prefix + "\t" +  Util.toHexString( _ , startingDivider = "[", endDivider = "]"))).mkString(",\n") + "\n" +
        prefix + sEnd
      case x: Array[DataAxis] =>
        prefix + sStart +
        (x map (_.getHexString(prefix + "\t", index + 1))).mkString("") +
        prefix + sEnd
    }
  }
  //---------------------------------------------------------------------------
  def getValueString(dataType: DataType[_], prefix: String = "", index: Int = 1): String = {
    val sStart = s"--------- start axis: $index size: ${data.size} ---------\n"
    val sEnd = s"--------- end axis: $index size: ${data.size} -----------\n"
    data match {
      case x: Array[Array[Byte]] =>
        prefix + sStart +
          (x map (prefix + "\t" + dataType.getFirstValue( _ ))).mkString(",\n") + "\n" +
          prefix + sEnd
      case x: Array[DataAxis] =>
        prefix + sStart +
          (x map (_.getValueString(dataType, prefix + "\t", index + 1))).mkString("") +
          prefix + sEnd
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file DataAxis.scala
//=============================================================================
