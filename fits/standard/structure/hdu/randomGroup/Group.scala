/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Oct/2022
 * Time:  12h:40m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.randomGroup
//=============================================================================
import com.common.fits.standard.dataType.DataType
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.tdisp.Tdisp
//=============================================================================
case class Group(parameterNameSeq: Array[String]
                 , parameterByteSeq: Array[Byte]
                 , linearTransformationZero: Double
                 , linearTransformationScale: Double
                 , dataAxisSeq: Array[Long]
                 , dataByteSeq: Array[Byte]
                 , dataType: DataType[_]) {
  //---------------------------------------------------------------------------
  def findParameterPosition(parameterName: String) =
    parameterNameSeq.zipWithIndex.flatMap { case (pName, i) =>
      if (pName == parameterName) Some(i)
      else None
    }
  //---------------------------------------------------------------------------
  def getParameterValue(parameterName: String): Double = {
    val value = findParameterPosition(parameterName).map { position =>
      val skipByteCount = position * dataType.byteSize
      val valueByteSeq = parameterByteSeq.drop(skipByteCount).take(dataType.byteSize)
      dataType.getFirstValueAsDouble(valueByteSeq)
    }.sum
    linearTransformationZero + linearTransformationScale *  value
  }
  //---------------------------------------------------------------------------
  def getParameterValueSeq(): Array[Double] = parameterNameSeq map (getParameterValue(_))
  //---------------------------------------------------------------------------
  def getDataSeq() =
    DataAxis(dataByteSeq.grouped(dataType.byteSize).toArray)
  //---------------------------------------------------------------------------
  def getString(groupID: Int) = {
    val dataAxis = DataAxis(dataAxisSeq.toList
                            , dataByteSeq
                            , dataType)
    s"############## group: $groupID start ##############\n" +
    "  parameter value: " + getParameterValueSeq.mkString(Tdisp.VALUE_SEQ_START_CHAR, Tdisp.VALUE_SEQ_DIVIDER, Tdisp.VALUE_SEQ_END_CHAR) + "\n" +
    s"  group data hex:\n ${dataAxis.getHexString()} \n" +
    s"  group data value:\n ${dataAxis.getValueString(dataType)}" +
    s"############## group: $groupID end ################\n"
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Group.scala
//=============================================================================
