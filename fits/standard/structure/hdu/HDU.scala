/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  17/Oct/2022
 * Time:  11h:56m
 * Description: HDU: header and data unit
 */
//=============================================================================
package com.common.fits.standard.structure.hdu
//=============================================================================
import com.common.fits.standard.block.Block
import com.common.fits.standard.block.record.Record
import com.common.fits.standard.block.record.RecordNumber.{RecordFloat, RecordInteger, RecordLogical}
import com.common.fits.standard.block.record.RecordCharacter.RecordString
import com.common.fits.standard.ItemSize.BLOCK_BYTE_SIZE
import com.common.fits.standard.Keyword._
import com.common.fits.standard.dataType.{DataType, DataTypeUnknown}
import com.common.fits.standard.disconformity.DisconfFatal
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.Structure
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.AsciiTable
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.BinTable
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.tdisp.Tdisp
import com.common.fits.standard.structure.hdu.primary.Primary
import com.common.logger.MyLogger
import com.common.stream.MyOutputStream
import com.common.stream.local.LocalOutputStream
import com.common.util.util.Util
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object HDU extends MyLogger {
  //---------------------------------------------------------------------------
  final val MIN_ALLOWED_NAXIS =   0  //included
  final val MAX_ALLOWED_NAXIS = 999  //included
  //---------------------------------------------------------------------------
  //Dump CSV
  val DUMP_CSV_HEADER_NAME  = "header.csv"
  val DUMP_CSV_COLUMN_DIVIDER  = "\t"
  val DUMP_CSV_HEADER = Array("keyword","value","comment","type").mkString(DUMP_CSV_COLUMN_DIVIDER)

  //Dump data block FITS extension
  private val DUMP_DATA_BLOCK_EXTENSION  = ".fits"
  //---------------------------------------------------------------------------
  def saveRecordMapAsHeader(stream: MyOutputStream
                            , _recordMap: RecordMap
                            , addPadding: Boolean = false
                            , skipEmptyRecord: Boolean = false): Boolean = {
    _recordMap.getPlainRecordSeq.foreach { record=>
      if (skipEmptyRecord && record.isEmpty) None
      else record.save(stream)
    }
    if (addPadding) {
      val headerPadding = Record.RECORD_HEADER_PADDING_STRING * Block.getLastBlockPaddingByteSize(stream.getWritePosition).toInt
      stream.write(headerPadding.getBytes(), closeIt = false)
    }
    true
  }
  //---------------------------------------------------------------------------
  private def isValidRecordOrder(expectedRecordOrderSeq: Seq[String]
                               , recordMap: RecordMap
                               , sectionName: String
                               , ctxt: FitsCtxt
                               , checkAreConsecutive: Boolean = true): Boolean = {
    //get the position of the expected keyword sequence
    val missKeywordSeq = ArrayBuffer[String]()
    val actualKeywordPosSeq = expectedRecordOrderSeq.flatMap { keyword =>
      if (recordMap.contains(keyword)) Some(recordMap(keyword).head.id)
      else return false
    }
    //check missed keywods
    if (missKeywordSeq.length > 0) {
      DisconfFatal(
            ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} miss the keywords: ${missKeywordSeq.mkString("'", ",", "'")}")
      return false
    }
    if (!checkAreConsecutive) return true

    //check if they are consecutive
    if (!Util.isSorted(actualKeywordPosSeq.toList) ||
      !Util.isConsecutiveSeqLong(actualKeywordPosSeq)) {
      DisconfFatal(
            ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} has not the proper order in the required records"
        , additionalInfo = s"Expected ordered sequence: ${expectedRecordOrderSeq.mkString("'", ",", "'")}")
      false
    }
    else true
  }
  //----------------------------------------------------------------------------
  def isValidRecordOrder(recordMap: RecordMap
                         , mandatoryRecordSeq_1: Array[String]
                         , mandatoryRecordSeq_2: Array[String]
                         , sectionName: String
                         , ctxt: FitsCtxt) : Boolean= {
    val expectedHeaderRecordOrderSeq =
      mandatoryRecordSeq_1 ++ recordMap.getAxisSeqName() ++ mandatoryRecordSeq_2
    isValidRecordOrder(expectedHeaderRecordOrderSeq
      , recordMap
      , sectionName
      , ctxt)
  }
  //----------------------------------------------------------------------------
  def isValidTrueLogicalRecord(record: Record[_]
                               , sectionName: String
                               , ctxt: FitsCtxt): Boolean = {
    record match {
      case r: RecordLogical =>
        if (!r.value) {
           DisconfFatal(
               ctxt
             , sectionName
             , s"Structure: ${ctxt.structureID} have not the expected value for keyword:'${r.keyword}'. Expecting: 'T' but found 'F'")
           return false
        }
      case r =>
          DisconfFatal(
              ctxt
            , sectionName
            , s"Structure: ${ctxt.structureID} the record with keyword:'${r.keyword}' is not a logical record"
            , s"First record: '${record.toString()}'")
        return false
    }
    true
  }

  //----------------------------------------------------------------------------
  def isValidStringRecord(record: Record[_]
                          , sectionName: String
                          , ctxt: FitsCtxt): Boolean = {
    record match {
      case _: RecordString => true
      case _ =>
        DisconfFatal(
            ctxt
          , sectionName
          , s"Structure: ${ctxt.structureID} the record with keyword:'${record.keyword}' is not a string record"
          , s"First record: '${record.toString()}'")
        false
    }
  }

  //----------------------------------------------------------------------------
  def isValidRecordStringSeq(recordMap: RecordMap
                             , sectionName: String
                             , recordCount: Long
                             , keywordTemplate: String
                             , ctxt: FitsCtxt
                             , keywordMustExist: Boolean = false): Boolean = {
    for (i <- 1L to recordCount) {
      val keyword = keywordTemplate + i
      if (recordMap.contains(keyword))
        return isValidStringRecord(recordMap.getFirstRecord(keyword).get, sectionName, ctxt)
      else {
        if (keywordMustExist) {
          DisconfFatal(ctxt
            , sectionName
            , s"Structure: ${ctxt.structureID} the record with keyword:'$keyword' does not exist")
          return false
        }
      }
    }
    true
  }

  //----------------------------------------------------------------------------
  def isValidIntegerRecord(record: Record[_]
                           , sectionName: String
                           , ctxt: FitsCtxt): Boolean = {
    record match {
      case _ : RecordInteger => true
      case _ =>
        DisconfFatal(
          ctxt
          , sectionName
          , s"Structure: ${ctxt.structureID} the record with keyword:'${record.keyword}' is not a integer record"
          , s"First record: '${record.toString()}'")
        false
    }
  }
  //----------------------------------------------------------------------------
  def isNonNegativeIntegerRecord(record: Record[_]
                                 , sectionName: String
                                 , ctxt: FitsCtxt): Boolean = {
    if (!isValidIntegerRecord(record, sectionName, ctxt)) return false
    val r = record.asInstanceOf[RecordInteger]
    if (r.value < 0) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} the keyword '${r.keyword}' must be non negative integer record")
      return false
    }
    true
  }
  //----------------------------------------------------------------------------
  def isValidRecordIntegerSeq(recordMap: RecordMap
                              , sectionName: String
                              , recordCount: Long
                              , keywordTemplate: String
                              , ctxt: FitsCtxt
                              , keywordMustExist: Boolean = false): Boolean = {
    for (i <- 1L to recordCount) {
      val keyword = keywordTemplate + i
      if (recordMap.contains(keyword))
        return isValidIntegerRecord(recordMap.getFirstRecord(keyword).get, sectionName, ctxt)
      else {
        if (!keywordMustExist) {
          DisconfFatal(ctxt
            , sectionName
            , s"Structure: ${ctxt.structureID} the record with keyword:'$keyword' does not exist")
          return false
        }
      }
    }
    true
  }
  //----------------------------------------------------------------------------
  def isValidFloatRecord(record: Record[_]
                         , sectionName: String
                         , ctxt: FitsCtxt): Boolean = {
    record match {
      case _: RecordFloat => true
      case _ =>
        DisconfFatal(
          ctxt
          , sectionName
          , s"Structure: ${ctxt.structureID} the record with keyword:'${record.keyword}' is not a float record"
          , s"First record: '${record.toString()}'")
        false
    }
  }
  //----------------------------------------------------------------------------
  def isValidRecordFloatSeq(recordMap: RecordMap
                            , sectionName: String
                            , recordCount: Long
                            , keywordTemplate: String
                            , ctxt: FitsCtxt
                            , keywordMustExist: Boolean = false): Boolean = {
    for (i <- 1L to recordCount) {
      val keyword = keywordTemplate + i
      if (recordMap.contains(keyword))
        return isValidFloatRecord(recordMap.getFirstRecord(keyword).get, sectionName, ctxt)
      else {
        if (keywordMustExist) {
          DisconfFatal(ctxt
            , sectionName
            , s"Structure: ${ctxt.structureID} the record with keyword:'$keyword' does not exist")
          return false
        }
      }
    }
    true
  }
  //---------------------------------------------------------------------------
  def recordSequenceCommonCheck(recordMap: RecordMap
                                , sectionName: String
                                , templateKeyword: String
                                , maxAllowedItem: Long
                                , keywordSeq: Array[String]
                                , ctxt: FitsCtxt
                                , checkRecordString: Boolean = false
                                , checkAreConsecutive: Boolean = true): Boolean = {

    //check that there are not extra 'templateKeyword'
    val allKeywordSet = recordMap.getAllKeywordWithPrefix(templateKeyword)
      .toSet
      .filter(_ != templateKeyword)

    allKeywordSet.diff(keywordSeq.toSet).foreach { _ =>
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} found an extra $templateKeyword keyword but it not allowed. Last valid $templateKeyword allowed: $templateKeyword$maxAllowedItem")
      return false
    }

    //check if number of records match
    if (maxAllowedItem != keywordSeq.length) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} the expected records of $templateKeyword keyword ($maxAllowedItem) does not match with the found records ${templateKeyword}N (${keywordSeq.length})")
      return false
    }

    //check order
    if (!isValidRecordOrder(keywordSeq
      , recordMap
      , sectionName
      , ctxt
      , checkAreConsecutive))
      return false

    //check items are non negative integer
    keywordSeq.map { name =>
      val record = recordMap.getFirstRecord(name).getOrElse {
        DisconfFatal(
            ctxt
          , sectionName
          , s"Structure: ${ctxt.structureID} can not find the keyword: '$name'")
        return false
      }
      if (checkRecordString) {
        if (!isValidStringRecord(record, sectionName, ctxt)) return false
      }
      else if (!isNonNegativeIntegerRecord(record, sectionName, ctxt)) return false
    }
    true
  }
  //----------------------------------------------------------------------------
  def isValidSimpleRecord(recordMap: RecordMap
                          , ctxt: FitsCtxt): Boolean = {
    if (!recordMap.contains(KEYWORD_SIMPLE)) {
      DisconfFatal(
            ctxt
        , SectionName.SECTION_4_4_1_1
        , s"Structure: ${ctxt.structureID} keyword:'$KEYWORD_SIMPLE' was not found")
      return false
    }

    val record = recordMap.getFirstRecord().getOrElse {
      DisconfFatal(
            ctxt
        , SectionName.SECTION_NONE
        , s"Structure: ${ctxt.structureID} can not find the first record")
      return false
    }

    if (!isValidTrueLogicalRecord(record,SectionName.SECTION_4_4_1_1,ctxt)) return false
    if (record.keyword != KEYWORD_SIMPLE) {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_4_4_1_1
        , s"Structure: ${ctxt.structureID} have not the expected keyword:'$KEYWORD_SIMPLE' as first record")
      return false
    }
    true
  }
  //----------------------------------------------------------------------------
  def isValidRecordBitPix(recordMap: RecordMap
                          , ctxt: FitsCtxt): Boolean = {
    val record = recordMap.getFirstRecord(KEYWORD_BITPIX).getOrElse {
      DisconfFatal(
            ctxt
        , SectionName.SECTION_4_4_1_1
        , s"Structure: ${ctxt.structureID} can not find the keyword: '$KEYWORD_BITPIX'")
      return false
    }
    if (!isValidIntegerRecord(record,SectionName.SECTION_4_4_1_1,ctxt)) return false
    val r = record.asInstanceOf[RecordInteger]
    if (DataType.build(r.value).isInstanceOf[DataTypeUnknown]) {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_4_4_1_1
        , s"Structure: ${ctxt.structureID} invalid value for keyword: '$KEYWORD_BITPIX' expecting one of: ${DataType.DATA_TYPE_SEQ.mkString(Tdisp.VALUE_SEQ_START_CHAR, Tdisp.VALUE_SEQ_DIVIDER, Tdisp.VALUE_SEQ_END_CHAR)}"
        , s"Current value: ${record.value.toString}")
      return false
    }
    true
  }
  //---------------------------------------------------------------------------
  def isValidRecordNaxis(recordMap: RecordMap
                         , ctxt: FitsCtxt): Boolean = {
    val record = recordMap.getFirstRecord(KEYWORD_NAXIS).getOrElse {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_4_4_1_1
        , s"Can not find the keyword: '$KEYWORD_NAXIS'")
      return false
    }
    if (!isValidIntegerRecord(record, SectionName.SECTION_4_4_1_1, ctxt)) return false

    val r = record.asInstanceOf[RecordInteger]
    val value = r.value
    if (value < MIN_ALLOWED_NAXIS || value > MAX_ALLOWED_NAXIS) {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_4_4_1_1
        , s"Structure: ${ctxt.structureID} invalid '$KEYWORD_NAXIS' expecting a value between: {$MIN_ALLOWED_NAXIS,$MAX_ALLOWED_NAXIS} both limits included "
        , s"Current value: ${value.toString}")
      false
    }
    else true
  }

  //---------------------------------------------------------------------------
  def isValidRecordAxisN(recordMap: RecordMap
                         , ctxt: FitsCtxt): Boolean = {

    //check if it possible to have axis
    val naxis = recordMap.getNaxis()
    if (naxis == 0) return true
    val axisSeqName = recordMap.getAxisSeqName()
    if (naxis == 0 && axisSeqName.length > 0) {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_4_4_1_1
        , s"Structure: ${ctxt.structureID} found keywords with 'NAXISn' but the are not allowed because '$KEYWORD_NAXIS' is zero")
      return false
    }

    recordSequenceCommonCheck(recordMap
      , SectionName.SECTION_4_4_1_1
      , KEYWORD_NAXIS
      , naxis
      , axisSeqName
      , ctxt)
  }
  //---------------------------------------------------------------------------
  def isValidRecordEnd(recordMap: RecordMap
                       , ctxt: FitsCtxt): Boolean =
    recordMap.getRecordSeq(KEYWORD_END).length match {
      case 0 =>
        DisconfFatal(
            ctxt
          , SectionName.SECTION_4_4_1_1
          , s"Structure: ${ctxt.structureID} can not find the keyword: '$KEYWORD_END'")
        false
      case 1 => true
      case x =>
        DisconfFatal(
            ctxt
          , SectionName.SECTION_4_4_1_1
          , s"Structure: ${ctxt.structureID} there are: $x keywords '$KEYWORD_END' but just one is allowed")
        false
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
import HDU._
trait HDU extends Structure {
  //---------------------------------------------------------------------------
  val recordMap: RecordMap //fast access to records
  val dataType = recordMap.getDataType()
  val headerByteSize = (recordMap.getPlainRecordSeq map (r => r.toString().size.toLong)).sum
  val headerBlockSize = Block.getBlockCount(headerByteSize)
  //---------------------------------------------------------------------------
  def getBlockCount() = headerBlockSize + getDataBlockByteSize
  //---------------------------------------------------------------------------
  def getLastByte() = {
    if (hasDataBlocks()) dataBlockSeq.last.endPos
    else headerByteSize
  }
  //---------------------------------------------------------------------------
  def save(stream: MyOutputStream) = {
    saveHeader(stream)
    saveDataBlock(stream)
  }

  //---------------------------------------------------------------------------
  def saveHeader(stream: MyOutputStream, addPadding: Boolean = false): Boolean =
    saveRecordMapAsHeader(stream, recordMap, addPadding)
  //---------------------------------------------------------------------------
  def getStoredID() = {
    Array(s"'${recordMap.getExtName}'"
      , s"'${recordMap.getExtVer}'"
      , s"'${recordMap.getExtLevel}'")
  }
  //---------------------------------------------------------------------------
  def getNormalizedName() = {
    val extName = recordMap.getExtName
    val extVer = recordMap.getExtVer
    val extLevel = recordMap.getExtLevel
    ((if (extName.isEmpty) "0" else extName) + "_" +
      extVer + "_" +
      extLevel.toString).replaceAll(" ","_")
  }
  //---------------------------------------------------------------------------
  def dump(oDir: String, primaryHeaderRecordMap: RecordMap, applyLinearConversion: Boolean): Unit = {

    //header as csv
    var stream = LocalOutputStream(oDir + DUMP_CSV_HEADER_NAME)
    stream.writeLine(DUMP_CSV_HEADER)
    recordMap.getPlainRecordSeq.foreach { record => stream.writeLine(record.getAsCsv(DUMP_CSV_COLUMN_DIVIDER))}
    stream.close

    //write primary header
    stream = LocalOutputStream(oDir + className + "_" + getNormalizedName + DUMP_DATA_BLOCK_EXTENSION)
    if (!this.isInstanceOf[Primary]) {

      //set to zero the data blocks in primary header
      val naxis = primaryHeaderRecordMap.getNaxis()
      for (i <- 1L to naxis)
        primaryHeaderRecordMap.remove(KEYWORD_NAXIS + i)
      primaryHeaderRecordMap.updateValue[Long](KEYWORD_NAXIS, 0L)

      //save primary header
      primaryHeaderRecordMap.getPlainRecordSeq.foreach { record => record.save(stream) }
      val headerPadding = Record.RECORD_HEADER_PADDING_STRING * Block.getLastBlockPaddingByteSize(stream.getWritePosition).toInt
      stream.write(headerPadding.getBytes(), closeIt = false)
    }

    //structure header and data block
    save(stream)
    stream.close

    //in case of tables, save a NEW csv
    this match {
      case x: AsciiTable => x.dumpTable(oDir, applyLinearConversion)
      case x: BinTable => x.dumpTable(oDir, applyLinearConversion)
      case _ =>
    }
  }
  //---------------------------------------------------------------------------
  def getSummary(structurePosition: String, streamPos: Long = 0) =
    s"    Structure $structurePosition: $className '$getNormalizedName'" +
    s"\n    ----------------------------------------------" +
    s"\n      Header total records                : ${recordMap.getRecordCount}" +
    s"\n      Header records with unique key      : ${recordMap.getRecordCountWithUniqueKeyword}" +
    s"\n      Header start stream position        : $streamPos" +
    s"\n    ----------------------------------------------" +
    s"\n      Data block count                    : ${dataBlockSeq.size.toLong}" +
    s"\n      Data block byte size                : ${dataBlockSeq.size.toLong * BLOCK_BYTE_SIZE}" +
    (if (!dataBlockSeq.isEmpty)s"\n      First data block stream position    : ${dataBlockSeq.head.starPos}" else "") +
    (if (!dataBlockSeq.isEmpty)s"\n      Last data block stream position     : ${dataBlockSeq.last.endPos}" else "") +
    s"\n    ----------------------------------------------" +
    (if (recordMap.contains(KEYWORD_BITPIX))   s"\n      Data type                           : ${dataType.name}" else "") +
    (if (recordMap.contains(KEYWORD_BITPIX))   s"\n      Bits per pixel                      : ${dataType.bitSize}" else "") +
    (if (recordMap.contains(KEYWORD_BITPIX))   s"\n      Bytes per pixel                     : ${dataType.byteSize}" else "") +
    (if (recordMap.contains(KEYWORD_NAXIS))    s"\n      Axis count                          : ${recordMap.getNaxis()}" else "") +
    (if (recordMap.contains(KEYWORD_NAXIS))    s"\n      Axis                                : ${recordMap.getAxisSeq().mkString(Tdisp.VALUE_SEQ_START_CHAR, Tdisp.VALUE_SEQ_DIVIDER, Tdisp.VALUE_SEQ_END_CHAR)}" else "") +
    s"\n    ----------------------------------------------" +
    (if (recordMap.contains(KEYWORD_EXTNAME))  s"\n      EXTNAME                             : ${recordMap.getExtName()}" else "") +
    (if (recordMap.contains(KEYWORD_EXTVER))   s"\n      EXTVER                              : ${recordMap.getExtVer()}" else "") +
    (if (recordMap.contains(KEYWORD_EXTLEVEL)) s"\n      EXTLEVEL                            : ${recordMap.getExtLevel()}" else "") +
  s"\n    ----------------------------------------------"
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file HDU.scala
//=============================================================================
