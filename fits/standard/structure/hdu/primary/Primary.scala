/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Sep/2022
 * Time:  18h:56m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.hdu.primary
//=============================================================================
import com.common.fits.standard.Keyword._
import com.common.fits.standard.disconformity.DisconfFatal
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.block.Block
import com.common.fits.standard.block.dataIntegrity.DataIntegrity.PartialDataSum
import com.common.fits.standard.block.record.Record
import com.common.fits.standard.block.record.Record.RecordSeq
import com.common.fits.standard.block.record.RecordNumber.{RecordInteger, RecordLogical}
import com.common.fits.standard.block.record.RecordCharacter.RecordString
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.block.record.verifier.RecordVerifier
import com.common.fits.standard.block.record.verifier.section_8.Section_8
import com.common.fits.standard.block.record.verifier.section_9.Section_9
import com.common.fits.standard.structure.hdu.HDU
import com.common.stream.MyInputStream
import com.common.fits.standard.structure.hdu.randomGroup.RandomGroup
//=============================================================================
//=============================================================================
object Primary {
  //----------------------------------------------------------------------------
  final val RecordSimple = RecordLogical(
      KEYWORD_SIMPLE
    , true
    , "conforms FITS format version 4.0 2016 July 22")
  //----------------------------------------------------------------------------
  final val MANDATORY_RECORD_SEQ_1 = Array (
      KEYWORD_SIMPLE
    , KEYWORD_BITPIX
    , KEYWORD_NAXIS
  )
  //----------------------------------------------------------------------------
  final val NOT_ALLOWED_KEYWORD = Seq(
      (KEYWORD_XTENSION,SectionName.SECTION_4_4_1_2)
    , (KEYWORD_INHERIT,SectionName.APPENDIX_K)
  )
  //----------------------------------------------------------------------------
  private def isValidInheritRecord(recordMap: RecordMap
                                   , ctxt: FitsCtxt
                                  ): Boolean = {
    if (!recordMap.contains(KEYWORD_INHERIT)) true
    else {
      val naxis = recordMap.getNaxis()
      if (naxis != 0) {
        DisconfFatal(
          ctxt
          , SectionName.APPENDIX_K
          , s"Structure: ${ctxt.structureID} contains the keyword:'$KEYWORD_INHERIT' but it has not a null primary array: NAXIs=$naxis")
        true
      }
      else true
    }
  }
  //----------------------------------------------------------------------------
  private def isValidHeader(recordMap: RecordMap
                            , ctxt: FitsCtxt): Boolean = {
    //check first record
    if (!HDU.isValidSimpleRecord(recordMap,ctxt)) return false
    if (!HDU.isValidRecordOrder(recordMap
       , MANDATORY_RECORD_SEQ_1
       , mandatoryRecordSeq_2 = Array()
       , SectionName.SECTION_4_4_1_1
       , ctxt))
      return false

    if (!HDU.isValidRecordBitPix(recordMap,ctxt)) return false
    if (!HDU.isValidRecordNaxis(recordMap,ctxt)) return false
    if (!HDU.isValidRecordAxisN(recordMap,ctxt)) return false
    if (!HDU.isValidRecordEnd(recordMap,ctxt)) return false

    //check the presence of not allowed keywords
    NOT_ALLOWED_KEYWORD.foreach { case (keyword, section) =>
      if (recordMap.contains(keyword)) {
        DisconfFatal(
          ctxt
          , section
          , s"Structure: ${ctxt.structureID} contains the keyword:'$keyword' but it is not allowed in the primary header")
        return true
      }
    }

    RecordVerifier.checkDeprecatedKeywordSeq(recordMap,ctxt)
    if (!Section_8.verify(recordMap,ctxt)) return false
    if (!Section_9.verify(recordMap, ctxt)) return false
    isValidInheritRecord(recordMap, ctxt)
  }
  //---------------------------------------------------------------------------
  def getDataBlockByteSize(recordMap: RecordMap): Long =
    recordMap.getAxisTotalByteSize()
  //---------------------------------------------------------------------------
  def build(stream: MyInputStream
            , readDataBlockFlag: Boolean
            , hduHeaderDataSum: PartialDataSum
            , ctxt: FitsCtxt): Option[HDU] = {
    val recordMap = Block.readHeader(stream,hduHeaderDataSum,ctxt).getOrElse(return None)
    if (!isValidHeader(recordMap, ctxt)) None
    else {
      val dataBlockSeq = Block.readAllDataBlock(stream
                                                , getDataBlockByteSize(recordMap)
                                                , ctxt
                                                , readDataBlockFlag).getOrElse(return None)
      //check if it is a random group
      if (RandomGroup.canBeRandomGroup(recordMap)) {
        val dataBlockSeq = Block.readAllDataBlock(stream
          , RandomGroup.getDataBlockByteSize(recordMap)
          , ctxt
          , readDataBlockFlag).getOrElse(return None)
        if (!RandomGroup.isValidHeader(recordMap, dataBlockSeq, ctxt)) None
        else Some(RandomGroup(recordMap,dataBlockSeq))
      }
      else Some(Primary(recordMap, dataBlockSeq))
    }
  }
  //---------------------------------------------------------------------------
  def buildHeader(bitPix: Int
                  , axisSeq: Array[Long]
                  , recordSeq: RecordSeq) =
    RecordMap(
      Array(
          RecordSimple
        , RecordInteger(KEYWORD_BITPIX, bitPix, "number of bits per data item")
        , RecordInteger(KEYWORD_NAXIS, axisSeq.length, "number of data axes")
      ) ++
      Record.getNaxisRecordSeq(axisSeq) ++
      recordSeq ++
      Array(RecordString(keyword = KEYWORD_END, value = ""))
    )
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Primary(recordMap:RecordMap
                   , dataBlockSeq: Array[Block]) extends HDU {
  //---------------------------------------------------------------------------
  def getDataStartPos = Block.getBlockByteSize(headerBlockSize)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file primary.scala
//=============================================================================
