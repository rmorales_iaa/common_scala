/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  27/Sep/2022
 * Time:  12h:54m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.structure.special
//=============================================================================
import com.common.fits.standard.Keyword.{KEYWORD_SIMPLE, KEYWORD_XTENSION}
import com.common.fits.standard.disconformity.{DisconfFatal, DisconfWarn}
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.Structure
import com.common.fits.standard.block.Block
import com.common.fits.standard.block.record.Record
import com.common.fits.standard.ItemSize.{BLOCK_BYTE_SIZE, KEYWORD_CHAR_SIZE}
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.block.record.RecordMap
import com.common.logger.MyLogger
import com.common.stream.local.LocalOutputStream
import com.common.stream.{MyInputStream, MyOutputStream}
//=============================================================================
//=============================================================================
object Special extends MyLogger {
  //---------------------------------------------------------------------------
  //Dump data block FITS extension
  private val DUMP_DATA_BLOCK_EXTENSION = ".fits"
  //---------------------------------------------------------------------------
  private def isValid(block: Block
                      , ctxt: FitsCtxt): Boolean = {
    val firstRecordKey = new String(block.byteSeq.take(KEYWORD_CHAR_SIZE))
    if (firstRecordKey == KEYWORD_XTENSION) {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_3_5
        , s"Structure: ${ctxt.structureID} can not start with keyword: '$KEYWORD_XTENSION'"
        , s"First record key: '$firstRecordKey'")
      true
    }
    else {
      if (firstRecordKey.contains(KEYWORD_SIMPLE))
        DisconfWarn(
          ctxt
          , SectionName.SECTION_3_5
          , s"Structure: ${ctxt.structureID} contains the keyword:'$KEYWORD_SIMPLE' but it is not recommended to be in a 'Special record'")
      true
    }
  }
  //---------------------------------------------------------------------------
  def build(stream: MyInputStream
            , remaindBlockCount: Long
            , headerBlockStorage: Array[Block]
            , ctxt: FitsCtxt
            , readDataBlockFlag: Boolean): Option[Special] = {
    if (!isValid(headerBlockStorage.head, ctxt)) None
    else {
      //read remain blocks
      val remainBlockSeq = Block.readAllDataBlock(
        stream
        , (remaindBlockCount - headerBlockStorage.size) * BLOCK_BYTE_SIZE
        , ctxt
        , readDataBlockFlag).getOrElse(return None)
      Some(Special(headerBlockStorage ++ remainBlockSeq))
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import Special._
case class Special(dataBlockSeq: Array[Block]) extends Structure {
  //---------------------------------------------------------------------------
  def getBlockCount() = getDataBlockByteSize
  //---------------------------------------------------------------------------
  def save(stream: MyOutputStream) = saveDataBlock(stream, writeHeader = false)
  //---------------------------------------------------------------------------
  def getLastByte() = {
    if (hasDataBlocks()) dataBlockSeq.last.endPos
    else 0L
  }
  //---------------------------------------------------------------------------
  def getStoredID() = Array("","","")
  //---------------------------------------------------------------------------
  def getNormalizedName() = ""
  //---------------------------------------------------------------------------
  def getSummary(structurePosition: String,streamPos: Long = 0) = {
    s"    structure name $structurePosition: '$className'" +
      s"\n    ----------------------------------------------" +
      s"\n      Total block count                   : ${dataBlockSeq.size}" +
      s"\n      Total block byte size               : ${dataBlockSeq.size * BLOCK_BYTE_SIZE}" +
      s"\n    ----------------------------------------------" +
      s"\n    ########################################################"
  }
  //---------------------------------------------------------------------------
  def dump(oDir: String, primaryHeaderRecordMap: RecordMap, applyLinearConversion: Boolean): Unit = {

    val stream = LocalOutputStream(oDir + className + DUMP_DATA_BLOCK_EXTENSION)
    primaryHeaderRecordMap.getPlainRecordSeq.foreach { record => record.save(stream) }
    val headerPadding = Record.RECORD_HEADER_PADDING_STRING * Block.getLastBlockPaddingByteSize(stream.getWritePosition).toInt
    stream.write(headerPadding.getBytes(), closeIt = false)

    //structure header and data block
    save(stream)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Special.scala
//=============================================================================
