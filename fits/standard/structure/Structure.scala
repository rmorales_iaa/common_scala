/*
 * Rafael Morales (rmorales@iaa.es)
 * Instituto de Astrofísisca de Andalucía - CSIC
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  05/abr/2018
  * Time:  10h:28m
  * Description: FITS structure definition of FITS standard version 4.0 (2018 August 13, sectionName 3.1)
  */
//=============================================================================
package com.common.fits.standard.structure
//=============================================================================
import com.common.fits.standard.Keyword.{KEYWORD_CHECKSUM, KEYWORD_DATASUM}
import com.common.fits.standard.disconformity.{DisconfFatal, Disconformity}
import com.common.fits.standard.block.Block
import com.common.fits.standard.block.dataIntegrity.DataIntegrity
import com.common.fits.standard.block.record.Record
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.hdu.HDU
import com.common.fits.standard.structure.hdu.extension.Extension
import com.common.fits.standard.structure.hdu.primary.Primary
import com.common.fits.standard.structure.special.Special
import com.common.logger.MyLogger
import com.common.stream.{MyInputStream, MyOutputStream}
import com.common.fits.standard.block.dataIntegrity.DataIntegrity.PartialDataSum
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
object Structure extends MyLogger {
  //---------------------------------------------------------------------------
  private def isValidOrder(structSeq: ArrayBuffer[Structure], ctxt: FitsCtxt): Boolean = {
    structSeq.zipWithIndex.foreach { case (structure,i) =>
      if (structure.isInstanceOf[Primary] && (i != 0)) {
        DisconfFatal(
          ctxt
          , SectionName.SECTION_3_1
          , s"Structure: ${ctxt.structureID} the primary header and data unit must be the very first one but it is in position: ${i-1}")
        return true
      }
      if (structure.isInstanceOf[Special] && (i != (structSeq.length-1))) {
        DisconfFatal(
          ctxt
          , SectionName.SECTION_3_1
          , s"Structure: ${ctxt.structureID} the 'Special Record' structure must be the last one, but it is in position: ${i-1}")
        return true
      }
    }
    true
  }
  //---------------------------------------------------------------------------
  private def checkDataIntegrity(structure: Structure
                                 , hduHeaderDataSum: PartialDataSum
                                 , ctxt: FitsCtxt): Boolean = {
    structure match {
      case hdu: HDU =>
        val recordMap = hdu.recordMap
        if (recordMap.contains(KEYWORD_DATASUM) ||
            recordMap.contains(KEYWORD_CHECKSUM))
          DataIntegrity.check(
              hdu.recordMap
            , structure.dataBlockSeq
            , hduHeaderDataSum.getDataSum()
            , ctxt)
        else true
      case _        => true
    }
  }
  //---------------------------------------------------------------------------
  def build(stream: MyInputStream
            , totalBlockCount: Long
            , readDataBlockFlag: Boolean
            , debug: Boolean = false): Array[Structure] = {

    val structSeq = ArrayBuffer[Structure]()

    //build primary HDU
    val primaryHduHeaderDataSum = PartialDataSum()
    val hduPrimary = Primary.build(stream
                                   , readDataBlockFlag
                                   , primaryHduHeaderDataSum
                                   , FitsCtxt(1L,stream.name)).getOrElse(return Array()) //process the primary: it must be always the first one
    //check data integrity
    if (!checkDataIntegrity(hduPrimary
                            , primaryHduHeaderDataSum
                            , FitsCtxt(structureID = 0, stream.name))) return structSeq.toArray
    structSeq += hduPrimary
    var remainBlockCount = totalBlockCount - Block.getBlockCount(stream.getReadPosition)
    val headerBlockStorage = ArrayBuffer[Block]()
    var structureID = 1L
    if (debug) println(s"Read position after parsing 'Primary HDU' = ${stream.getReadPosition} remain block count: $remainBlockCount")

    //build remain HDUs (extension HDU)
    while (remainBlockCount > 0) {
      val ctxt = FitsCtxt(structureID,stream.name)
      val extHduHeaderDataSum = PartialDataSum()
      val hduExtension = Extension.build(stream
                                         , headerBlockStorage
                                         , extHduHeaderDataSum
                                         , ctxt
                                         , readDataBlockFlag) //try to process an extension
      val structure =
        if (hduExtension.isDefined) {
          if (debug) println(s"Read position after parsing extension: '${hduExtension.get.getNormalizedName()}' = ${stream.getReadPosition} remain block count: $remainBlockCount")
          hduExtension.get
        }
        else { //try to process a special record processing again the first byte used to process the extension structure
          warning("Trying to process a 'Special Record' section")
          Disconformity.clearAllDisconformity(stream.name)
          Special.build(
              stream
            , remainBlockCount
            , headerBlockStorage.toArray
            , FitsCtxt(structureID,stream.name)
            , readDataBlockFlag).getOrElse(return structSeq.toArray)
        }

      headerBlockStorage.clear()
      remainBlockCount = totalBlockCount - Block.getBlockCount(stream.getReadPosition)

      //check data integrity
      if (!checkDataIntegrity(structure
                              , extHduHeaderDataSum
                              , ctxt)) return structSeq.toArray

      structSeq += structure
      structureID += 1
    }
    //check order
    if (Structure.isValidOrder(structSeq,FitsCtxt(1L,stream.name))) structSeq.toArray
    else Array()
  }
  //---------------------------------------------------------------------------
  def groupByStructureType(structureSeq: Array[Structure]) = {
    val map = scala.collection.mutable.Map[String, List[Structure]]()
    structureSeq map { structure =>
      val className = structure.className.toUpperCase
      val structureList =
        if (map.contains(className)) map(className) ++ List(structure)
        else List(structure)
      map(className) = structureList
    }
    map
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
trait Structure {
  //---------------------------------------------------------------------------
  val dataBlockSeq: Array[Block]
  //---------------------------------------------------------------------------
  val className = getClass.getName.split("\\.").last
  //---------------------------------------------------------------------------
  def getBlockCount(): Long
  //---------------------------------------------------------------------------
  def save(stream: MyOutputStream): Unit
  //---------------------------------------------------------------------------
  def getLastByte(): Long
  //---------------------------------------------------------------------------
  def getStoredID(): Array[String]
  //---------------------------------------------------------------------------
  def getNormalizedName(): String
  //---------------------------------------------------------------------------
  def getSummary(structurePosition: String,streamPos:Long = 0): String
  //---------------------------------------------------------------------------
  def dump(oDir: String, primaryHeaderRecordMap: RecordMap, applyLinearConversion: Boolean): Unit
  //---------------------------------------------------------------------------
  def getDataBlockByteSize = {
    if (dataBlockSeq.isEmpty) 0
    else Block.getBlockCount(dataBlockSeq.last.endPos - dataBlockSeq.head.starPos + 1)
  }
  //---------------------------------------------------------------------------
  def getDataBlockCount() = Block.getBlockCount(getDataBlockByteSize)
  //---------------------------------------------------------------------------
  def getLastByteAsBlock() = Block.getBlockByteSize(Block.getBlockCount(getLastByte))
  //---------------------------------------------------------------------------
  def hasDataBlocks() = !dataBlockSeq.isEmpty
  //---------------------------------------------------------------------------
  def saveDataBlock(stream: MyOutputStream, writeHeader: Boolean = true): Unit = {

    if (writeHeader) {
      //header padding bytes
      val headerPadding = Record.RECORD_HEADER_PADDING_STRING * Block.getLastBlockPaddingByteSize(stream.getWritePosition).toInt
      stream.write(headerPadding.getBytes(), closeIt = false)
    }

    //data blocks
    dataBlockSeq.foreach { block => stream.write(block.byteSeq, closeIt = false) }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Structure.scala
//=============================================================================