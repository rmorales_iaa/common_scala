/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Sep/2022
 * Time:  17h:30m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.sectionName
//=============================================================================
//=============================================================================
object SectionName {
  //--------------------------------------------------------------------------
  final val SECTION_NONE: String = "None section"

  final val SECTION_3_1     : String = "3.1. Overall file structure"

  final val SECTION_3_2     : String = "3.2. Individual FITS structures"
  final val SECTION_3_3_2   : String = "3.3.2. Primary data array"

  final val SECTION_3_4_1_1 : String = "3.4.1.1. Identity"
  final val SECTION_3_5     : String = "3.5. Special records (restricted use)"

  final val SECTION_4_4_1_1 : String = "4.4.1.1. Primary header"
  final val SECTION_4_4_1_2 : String = "4.4.1.2. Conforming extensions"
  final val SECTION_4_4_2_1 : String = "4.4.2.1. General descriptive keywords"
  final val SECTION_4_4_2_2 : String = "4.4.2.2. Keywords describing observations"
  final val SECTION_4_4_2_3 : String = "4.4.2.3. Bibliographic keywords"
  final val SECTION_4_4_2_5 : String = "4.4.2.5. Keywords that describe arrays"
  final val SECTION_4_4_2_6 : String = "4.4.2.6. Extension keywords"
  final val SECTION_4_4_2_7 : String = "4.4.2.7. Data-integrity keywords"

  //6. Random-groups structure
  final val SECTION_6_1_1   : String = "6.1.1. Mandatory keywords"
  final val SECTION_6_1_2   : String = "6.1.2. Reserved keywords"
  final val SECTION_6_2     : String = "6.2. Data Sequence"

  //7. Standard extensions
  final val SECTION_7_1_2   : String = "7.1.2. Other reserved keywords"

  //7.2 The ASCII-table extension
  final val SECTION_7_2     : String = "7.2. The ASCII-table extension"
  final val SECTION_7_2_1   : String = "7.2.1. Mandatory keywords"
  final val SECTION_7_2_2   : String = "7.2.2. Other reserved keywords"
  final val SECTION_7_2_3   : String = "7.2.3. Data Sequence"
  final val SECTION_7_2_5   : String = "7.2.5. Entries"

  //7.3 Binary-table extension
  final val SECTION_7_3     : String = "7.3. Binary-table extension"
  final val SECTION_7_3_1   : String = "7.3.1. Mandatory keywords"
  final val SECTION_7_3_2   : String = "7.3.2. Other reserved keywords"
  final val SECTION_7_3_3_1 : String = "7.3.3.1. Main data table"
  final val SECTION_7_3_4   : String = "7.3.4. Data display"
  final val SECTION_7_3_5   : String = "7.3.5. Variable-length arrays"

  //8. World-coordinate systems
  final val SECTION_8_1     : String = "8.1. Basic concepts"

  final val SECTION_8_2     : String = "8.2. World-coordinate system representations"
  final val SECTION_8_2_1   : String = "8.2.1. Alternative WCS axis descriptions"

  final val SECTION_8_3     : String = "8.3. Celestial-coordinate-system representation"
  final val SECTION_8_4     : String = "8.4. Spectral-coordinate-system representation"
  final val SECTION_8_5     : String = "8.5. Conventional-coordinate types"

  //9. Representation of time coordinates
  final val SECTION_9_1     : String = "9.1. Time values"
  final val SECTION_9_2     : String = "9.1. Time coordinate frame"

  final val SECTION_9_2_1   : String = "9.2.1. Time scale"
  final val SECTION_9_2_2   : String = "9.2.2. Time reference value"
  final val SECTION_9_2_3   : String = "9.2.3. Time reference position"
  final val SECTION_9_2_4   : String = "9.2.4. Time reference direction"
  final val SECTION_9_2_5   : String = "9.2.5. Solar System ephemeris"

  final val SECTION_9_3     : String = "9.3. Time unit"

  final val SECTION_9_4_1   : String = "9.4.1. Time offset"
  final val SECTION_9_4_2   : String = "9.4.2. Time resolution and binning"
  final val SECTION_9_4_3   : String = "9.4.3. Time errors"

  final val SECTION_9_5     : String = "9.5. Global time keywords"

  final val SECTION_9_6     : String = "9.6. Other time-coordinates axes"

  final val SECTION_9_7     : String = "9.7. Durations"

  final val SECTION_9_8     : String = "9.8. Recommended best practices"

  final val SECTION_9_8_2   : String = "9.8.2 Restrictions on alternate descriptions"

  //10. Representation of compressed data
  final val SECTION_10_1_1  : String = "10.1.1. Required keywords"
  final val SECTION_10_1_2  : String = "10.1.2. Other reserved keywords"
  final val SECTION_10_1_3  : String = "10.1.3. Table columns"

  final val SECTION_10_2_1  : String = "10.2.1. Dithering algorithms"

  final val SECTION_10_3_1  : String = "10.3.1. Required keywords"
  final val SECTION_10_3_3  : String = "10.3.3. Compression directive keywords"
  final val SECTION_10_3_4  : String = "10.3.4. Other reserved keywords"
  final val SECTION_10_3_6  : String = "10.3.6. Compressing variable-lenght array columns"

  final val SECTION_10_4    : String = "10.4. Compression algorithms"
  final val SECTION_10_4_1  : String = "10.4.1. Rice compression"
  final val SECTION_10_4_2  : String = "10.4.2. Gzip compression"
  final val SECTION_10_4_3  : String = "10.4.3. IRAF/PLIO compression"
  final val SECTION_10_4_4  : String = "10.4.4. H-Compress algorithm"


  //Appendix
  final val APPENDIX_F      : String = "Appendix F: Reserved extension type names"
  final val APPENDIX_K      : String = "Appendix K: Header inheritance convention"

  //tables
  final val SECTION_4_2_TABLE_4   : String = "4.2. Table 4"
  //--------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file SectionName.scala
//=============================================================================
