/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  24/Oct/2022
 * Time:  13h:11m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.fits
//=============================================================================
import com.common.configuration.MyConf
import com.common.hardware.cpu.CPU
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
//=============================================================================
import scala.util.{Failure, Success, Try}
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object FitsVerify extends MyLogger {
  //---------------------------------------------------------------------------
  //(Fits,DisconWarnCount, DisconFatalCount)
  def verifyFile(fitsName:String): Option[Fits] = {
    var fits : Option[Fits] = None
    Try {
      fits = FitsLoad.load(
          fitsName
        , readDataBlockFlag = true
        , printDisconfFlag = true)
    }
    match {
      case Success(_) =>
        if (fits.isDefined) fits.get.printDetailedInfo()
        fits
      case Failure(e) =>
        error(s"Error processing FITS file: '$fitsName'")
        error(e.toString)
        error(e.getStackTrace.mkString("\n"))
        None
    }
  }

  //---------------------------------------------------------------------------
  class ProcessFitsSeq(fileNameSeq: Array[String]
                      , bwValid: BufferedWriter
                      , bwNotValid: BufferedWriter) extends ParallelTask[String](
    fileNameSeq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(fitsName: String) = {

      verifyFile(fitsName) match {
        case Some(_) => synchronized{bwValid.write(fitsName + "\n")}
        case None    => synchronized{bwNotValid.write(fitsName + "\n")}
      }
    }
    //-----------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //(Fits,DisconWarnCount, DisconFatalCount)
  def verifyDirectory(path: String
                      , validFile: String
                      , notValidFile: String): Unit = {

    info(s"Getting the recursive list of files to process in '$path'")
    val fileSeq = Path.getSortedFileListRecursive(
      path
      , MyConf.c.getStringSeq("Common.fitsFileExtension")).map(_.getAbsolutePath)
    info(s"Processing: '${fileSeq.size}' FITS files")

    //open the files
    val bwValid = new BufferedWriter(new FileWriter(new File(validFile)))
    val bwNotValid = new BufferedWriter(new FileWriter(new File(notValidFile)))

    //process the files
    new ProcessFitsSeq(fileSeq.toArray,bwValid,bwNotValid)

    bwValid.close()
    bwNotValid.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FitsVerify.scala
//=============================================================================
