/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  04/Oct/2022
 * Time:  09h:56m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.fits
//=============================================================================
//=============================================================================
import com.common.fits.standard.block.Block
import com.common.fits.standard.disconformity.{DisconfFatal, Disconformity}
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.Structure
import com.common.hardware.cpu.CPU
import com.common.logger.MyLogger
import com.common.stream.MyInputStream
import com.common.stream.local.LocalInputStream
import com.common.util.file.MyFile
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
//=============================================================================
import java.util.concurrent.ConcurrentLinkedQueue
import scala.collection.JavaConverters.collectionAsScalaIterableConverter
import scala.util.{Failure, Success, Try}
//=============================================================================
object FitsLoad extends MyLogger {
  //---------------------------------------------------------------------------
  private class MyParallelImageSeq(seq: Array[String]
                                  , fitsQueue: ConcurrentLinkedQueue[Fits]
                                  , readDataBlockFlag: Boolean) extends ParallelTask[String](
    seq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(fitsName: String) = {
      val fits = load(fitsName, readDataBlockFlag = readDataBlockFlag, printDisconfFlag = false)
      if (fits.isDefined) fitsQueue.add(fits.get)
    }
    //-----------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def build(stream: MyInputStream
           , readDataBlockFlag: Boolean = true): Option[Fits] = {

    val structureID = 0

    //read stream
    val ctxt = FitsCtxt(structureID,stream.name)
    if (stream.isClosed) {
      DisconfFatal(ctxt, s"Structure: $structureID the stream is closed", SectionName.SECTION_NONE)
      return None
    }

    //get dataBlock count
    val (totalBlockCount, disconf) = Block.getBlockCount(stream,ctxt)
    if (disconf.isDefined) {
      stream.close
      return None
    }

    //load all structures
    val structureSeq = Structure.build(stream, totalBlockCount, readDataBlockFlag)
    stream.close
    if (structureSeq.isEmpty) None
    else Some(Fits(stream.name, structureSeq))
  }
  //---------------------------------------------------------------------------
  private def loadStream(fitsName: String
                         , readDataBlockFlag: Boolean = true): Option[MyInputStream] = {
    if (!MyFile.fileExist(fitsName)) {
      error(s"The file: '$fitsName' does not exist. Can not process it")
      return None
    }
    var stream: MyInputStream = null
    Try {
      stream = LocalInputStream(fitsName)
    }
    match {
      case Success(_) => Some(stream)
      case Failure(e) =>
        error(e.getMessage + s" Error loading the FITS file '$fitsName' . Continuing with the next FITS file")
        error(e.getStackTrace.mkString("\n"))
        None
    }
  }
  //---------------------------------------------------------------------------
  private def loadRaw(fitsName: String
                      , loadStream: (String,Boolean) => Option[MyInputStream]
                      , readDataBlockFlag: Boolean = true
                      , printDisconfFlag: Boolean = false
                      , ignoreDisconf: Boolean = false): Option[Fits] = {

    var optFits : Option[Fits] = None
    Try {
      val stream = loadStream(fitsName,readDataBlockFlag).getOrElse(return None)
      optFits = build(stream, readDataBlockFlag)
    }
    match {
      case Success(_) =>
      case Failure(e) =>
        error(e.getMessage + s" Error loading the FITS file '$fitsName' . Continuing with the next FITS file")
        error(e.getStackTrace.mkString("\n"))
    }
    if (printDisconfFlag) printDisconf(fitsName)

    //clear disconformities for this file
    if (!ignoreDisconf && Disconformity.hasFatalDisconformity(fitsName)) return None
    Disconformity.clearAllDisconformity(fitsName)
    optFits
  }
  //---------------------------------------------------------------------------
  def loadDir(inputDir: String
              , notReadDataBlockFlag: Boolean = true
              , reportStats: Boolean = false): Unit = {

    if (!Path.directoryExist(inputDir)) {
      error(s"The directory: '$inputDir' does not exist. Can not process it")
      return
    }
    val fitsQueue = new ConcurrentLinkedQueue[Fits]()

    info(s"Loading all FITS files recursively from directory: '$inputDir'")
    //calculate al files in the input directory
    val fileSeq = Path.getSortedFileList(inputDir, Fits.fitsExtensionSeq).map(_.getAbsolutePath)
    if (fileSeq.length > 0) new MyParallelImageSeq(fileSeq.toArray, fitsQueue, notReadDataBlockFlag)

    //calculate all subdirs
    Path.getSubDirectoryList(inputDir).map(_.getAbsolutePath).foreach { subDirName =>
      info(s"Parsing directory: '$subDirName'")
      val fileSeq = Path.getSortedFileListRecursive(subDirName, Fits.fitsExtensionSeq).map(_.getAbsolutePath)
      new MyParallelImageSeq(fileSeq.toArray, fitsQueue, notReadDataBlockFlag)
    }

    if (reportStats) {
      fitsQueue.forEach {fits=> printDisconf(fits.name)}
      info(Disconformity.getTotalStats(inputDir, fitsQueue.size()))
    }
    fitsQueue.asScala.toArray
  }
  //---------------------------------------------------------------------------
  private def printDisconf(fitsName: String) = {
    if (!Disconformity.existAnyDisconformity) info(s"FITS: '$fitsName': None disconformity detected")
    else Disconformity.print(fitsName)
  }
  //---------------------------------------------------------------------------
  def load(fitsName: String
            , readDataBlockFlag: Boolean = true
            , printDisconfFlag: Boolean = false
            , ignoreDisconf: Boolean = false
            , loadStream: (String,Boolean) => Option[MyInputStream] = loadStream): Option[Fits] = {

    var fits: Option[Fits] = None
    Try {
      fits = loadRaw(fitsName
                     , loadStream
                     , readDataBlockFlag
                     , printDisconfFlag
                     , ignoreDisconf)
    }
    match {
      case Success(_) =>
        if (fits.isEmpty) warning(s"Errors found parsing FITS, but parser finished successfully. File: '$fitsName'")
        fits
      case Failure(e) =>
        error(s"Error loading FITS file: '$fitsName'")
        error(e.toString)
        error(e.getStackTrace.mkString("\n"))
        None
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FitsLoad.scala
//=============================================================================
