/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  24/Oct/2022
 * Time:  13h:11m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.fits
//=============================================================================
import com.common.configuration.MyConf
import com.common.fits.standard.block.Block
import com.common.fits.standard.block.record.RecordCharacter.RecordEnd
import com.common.fits.standard.block.record.RecordNumber.RecordInteger
import com.common.fits.standard.Keyword._
import com.common.fits.standard.structure.hdu.HDU
import com.common.fits.standard.structure.hdu.extension.conforming.standard.image.Image
import com.common.fits.standard.structure.hdu.primary.Primary
import com.common.logger.MyLogger
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.structure.Structure
import com.common.fits.standard.structure.hdu.primary.Primary.RecordSimple
import com.common.hardware.cpu.CPU
import com.common.stream.local.LocalOutputStream
import com.common.util.file.MyFile
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
//=============================================================================
import scala.util.{Failure, Success, Try}
import scala.io.Source
//=============================================================================
//=============================================================================
object FitsSliceCube extends MyLogger {
  //---------------------------------------------------------------------------
  private class MyParallelSlice(pathSeq: Array[String]
                                , outputDir:String
                                , uniqueHdu: Boolean
                                , hduNameSeq: Option[List[String]]
                                , scliceIndexSeq: Option[List[Int]]
                                , ignoreDisconf: Boolean = false) extends ParallelTask[String](
    pathSeq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(path: String) =
      scliceImage(path
                  , s"$outputDir/"
                  , uniqueHdu
                  , hduNameSeq
                  , scliceIndexSeq
                  , ignoreDisconf)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private def saveHeader(hdu: HDU
                         , _primaryHeaderRecordMap: RecordMap
                         , uniqueHdu: Boolean
                         , stream: LocalOutputStream): Boolean = {

    if (hdu.isInstanceOf[Primary])  //affine_transformation case, no extension
      return hdu.saveHeader(stream,addPadding = true)

    val finalRecordMap = _primaryHeaderRecordMap.myClone()
    val hduRecordClonedMap = hdu.recordMap.myClone()
    val hduAxisNameSeq = hduRecordClonedMap.getAxisSeqName() //get the name of the axis

    if (!uniqueHdu) { //hdu is an extension, so transform it in a primary y HDU

      //save primary header
      hduRecordClonedMap.remove(Seq(KEYWORD_XTENSION, KEYWORD_PCOUNT, KEYWORD_GCOUNT))
      hduRecordClonedMap.insertRecordAfterPos(RecordSimple,0)
      return HDU.saveRecordMapAsHeader(stream,hduRecordClonedMap,addPadding = true,skipEmptyRecord = true)
    }

    //merge primary header and extension header
    val keywordSeqBeRemovedFromHdu = Seq(
      KEYWORD_XTENSION
      , KEYWORD_BITPIX
      , KEYWORD_NAXIS
      , KEYWORD_BSCALE
      , KEYWORD_BZERO
      , KEYWORD_PCOUNT
      , KEYWORD_GCOUNT) ++ hduAxisNameSeq.drop(2) //get the name of the axis to be removed
    val axisRecordSeq = (hduAxisNameSeq.take(2) map (hduRecordClonedMap.getRecordSeq(_))).flatten //axis of the image 2D

    finalRecordMap.removeNaxisDefinition()       //remove NAXIS definition
    finalRecordMap.remove(Seq(KEYWORD_END))      //remove keyword 'END' from primary header
    finalRecordMap.remove(Seq(KEYWORD_EXTEND))   //remove keyword 'END' from primary header
    finalRecordMap.remove(Seq(KEYWORD_INHERIT))  //inherith can not be in the primary HDU
    finalRecordMap.appendOrUpdate(KEYWORD_NAXIS, hduRecordClonedMap.getFirstRecord(KEYWORD_NAXIS).get) //fix the number of axis in the NEW image
    finalRecordMap.appendOrUpdate(KEYWORD_BITPIX, hduRecordClonedMap.getFirstRecord(KEYWORD_BITPIX).get) //fix the BITPIX in the NEW image

    if (hduRecordClonedMap.contains(KEYWORD_BSCALE))
      finalRecordMap.appendOrUpdate(KEYWORD_BSCALE, hduRecordClonedMap.getFirstRecord(KEYWORD_BSCALE).get) //fix the BSCALE

    if (hduRecordClonedMap.contains(KEYWORD_BZERO))
      finalRecordMap.appendOrUpdate(KEYWORD_BZERO, hduRecordClonedMap.getFirstRecord(KEYWORD_BZERO).get) //fix the BZERO

    finalRecordMap.insertRecordSeqAfterPos(axisRecordSeq, 3) //append the image 2D axis

    //remove keywords from hdu
    hduRecordClonedMap.removeNaxisDefinition() //remove NAXIS definition
    hduRecordClonedMap.remove(keywordSeqBeRemovedFromHdu)

    //add the hdu header
    finalRecordMap.appendMap(hduRecordClonedMap)

    //save header
    HDU.saveRecordMapAsHeader(stream,finalRecordMap,addPadding = true, skipEmptyRecord = true)
  }
  //---------------------------------------------------------------------------
  private def scliceCube(hdu: HDU
                         , baseName: String
                         , recordMap: RecordMap
                         , uniqueHdu: Boolean): Boolean = {
    //-------------------------------------------------------------------------
    var sliceID  = 1L
    //-------------------------------------------------------------------------
    def getName =
      baseName +
      s"${hdu.className}_slice_" +
      s"${f"$sliceID%03d"}-" +
      hdu.getNormalizedName() + ".fits"

    //-------------------------------------------------------------------------
    //check in the cube can be sliced
    val hduRecordMap = hdu.recordMap
    val extensionNaxis = hduRecordMap.getNaxis()
    val name = getName

    if (extensionNaxis < 3) {
      info(s"Saving image: $name")
      val stream = LocalOutputStream(name)
      saveHeader(hdu, recordMap, uniqueHdu, stream)
      hdu.saveDataBlock(stream, writeHeader = false)
      stream.close
      return true
    }

    val naxisSeq = hduRecordMap.getAxisSeq()
    val dataBlockSeq = hdu.dataBlockSeq
    val dataByteSeq = Block.collectAllByteSeq(dataBlockSeq)
    val imageByteSize = (hduRecordMap.getBytePerPix() * naxisSeq.dropRight(1).product).toInt //reduce the number of axis
    var remainImageTotalByteSize = hdu.recordMap.getAxisTotalByteSize()

    //reduce the number of axis
    recordMap.remove(KEYWORD_NAXIS + extensionNaxis)
    recordMap.updateValue[Long](KEYWORD_NAXIS,naxisSeq.length-1)

    //slice
    dataByteSeq.grouped(imageByteSize).foreach { case byteSeq =>

      //add the keyword EXTLEVEL
      val hduRecordMap = hdu.recordMap
      hduRecordMap.remove(KEYWORD_END)
      hduRecordMap.appendOrUpdate(KEYWORD_EXTLEVEL,RecordInteger(KEYWORD_EXTLEVEL,sliceID,id = hduRecordMap.getNewID))
      hduRecordMap.append(RecordEnd())

      //create the stream
      val name = getName
      info(s"Saving image: $name")
      val stream = LocalOutputStream(name)

      //save header
      saveHeader(
          hdu
        , recordMap
        , uniqueHdu
        , stream)

      //save data block
      Block.splitInBlockSeq(byteSeq).foreach { block =>
        stream.write(block.byteSeq, closeIt = false) }
      stream.close

      //update stats
      remainImageTotalByteSize -= imageByteSize
      sliceID += 1
      if (remainImageTotalByteSize == 0) return true
    }
    true
  }
  //---------------------------------------------------------------------------
  private def scliceRaw(path:String
                        , outputDir:String
                        , uniqueHdu: Boolean
                        , hduNameSeq: Option[List[String]]
                        , scliceIndexSeq: Option[List[Int]]
                        , ignoreDisconf: Boolean = false
                        , verbose: Boolean = false): Unit = {
    //--------------------------------------------------------------------------
    def canProcessHDU(hdu: HDU,hduIndex: Int): Boolean = {
      if (hduNameSeq.isEmpty && scliceIndexSeq.isEmpty) return true
      else {
        if (hduNameSeq.isDefined)
          if (hduNameSeq.get.contains(hdu.getNormalizedName())) return true
      }
      if (scliceIndexSeq.isDefined)
          return scliceIndexSeq.get.contains(hduIndex)
      false
    }
    //-------------------------------------------------------------------------
    def getBaseName(structureID: Int, structure: Structure) =
       s"$outputDir/${Path.getOnlyFilenameNoExtension(path)}_structure_${f"$structureID%03d"}_" + structure.className
    //-------------------------------------------------------------------------
    val fits = FitsLoad.load(
        path
      , readDataBlockFlag = true
      , printDisconfFlag = true
      , ignoreDisconf)
    if (fits.isEmpty) error(s"Error loading file: '$path' can not slice cubes")
    else {
      //get structure sequence
      val structureSeq = fits.get.structureSeq
      val primaryHeaderRecordMap = structureSeq.head.asInstanceOf[Primary].recordMap
      info(s"Slicing:'${fits.get.shortName}'")

      //slice all cubes
      structureSeq.zipWithIndex.foreach { case(structure,structureID) =>

        structure match {
          case hdu: Primary =>
            if(canProcessHDU(hdu,structureID))
              scliceCube(hdu, getBaseName(structureID,structure), primaryHeaderRecordMap, uniqueHdu)
            else
              if (verbose) warning(s"Image:'$path' ignoring hdu:'${hdu.getNormalizedName()}' and index:$structureID because does not match with supplied filters")

          case hdu: Image   =>
            if(canProcessHDU(hdu,structureID))
              scliceCube(hdu, getBaseName(structureID,structure), hdu.recordMap, uniqueHdu)
            else
              if (verbose) warning(s"Image:'$path' ignoring hdu:'${hdu.getNormalizedName()}' and index:$structureID because does not match with supplied filters")          case _ =>
        }
      }
    }
  }
  //---------------------------------------------------------------------------
  private def scliceImage(path: String
                          , outputDir: String
                          , uniqueHdu: Boolean
                          , hduNameSeq: Option[List[String]]
                          , scliceIndexSeq: Option[List[Int]]
                          , ignoreDisconf: Boolean = true): Unit = {
    Try { scliceRaw(path
                    , outputDir
                    , uniqueHdu
                    , hduNameSeq
                    , scliceIndexSeq
                    , ignoreDisconf) }
    match {
      case Success(_) =>
      case Failure(e) =>
        error(s"Error slicing FITS file: '$path'")
        error(e.toString)
        error(e.getStackTrace.mkString("\n"))
    }
  }
  //---------------------------------------------------------------------------
  private def sliceDirectory(path: String
                             , outputDir: String
                             , uniqueHdu: Boolean
                             , hduNameSeq: Option[List[String]]
                             , scliceIndexSeq: Option[List[Int]]) = {
    val pathSeq = Path.getSortedFileListRecursive(path, MyConf.c.getStringSeq("Common.fitsFileExtension")) map (_.getAbsolutePath)
    new MyParallelSlice(pathSeq.toArray
                        , outputDir
                        , uniqueHdu
                        , hduNameSeq
                        , scliceIndexSeq
                        , ignoreDisconf = true)
  }

  //---------------------------------------------------------------------------
  private def processFileSet(path: String
                            , outputDir: String
                            , uniqueHdu: Boolean
                            , hduNameSeq: Option[List[String]]
                            , scliceIndexSeq: Option[List[Int]]) = {
    val br = Source.fromFile(path)
    val lineSeq = (for (line <- br.getLines) yield {
      val s = line.trim
      if (!s.isEmpty) Some(s)
      else  None
    }).flatten.toArray
    br.close

    new MyParallelImageSeq(lineSeq)
    //-------------------------------------------------------------------------
    class MyParallelImageSeq (seq: Array[String])
    extends ParallelTask[String](
      seq
      , threadCount = CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 1000) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(imageName: String) =
        Try {
          scliceImage(
            imageName
            , outputDir
            , uniqueHdu
            , hduNameSeq
            , scliceIndexSeq
            , ignoreDisconf = true)
        }
        match {
          case Success(_) =>
          case Failure(e) =>
            error(e.getMessage + s" Error fitting the WCS on image '$imageName' continuing with the next image")
            error(e.getStackTrace.mkString("\n"))
        }
      //-----------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def slice(path:String
            , outputDir: String
            , uniqueHdu: Boolean
            , hduNameSeq: Option[List[String]]
            , scliceIndexSeq: Option[List[Int]]
            , _processFileSet: Boolean) = {

    Path.resetDirectory(outputDir)

    if (_processFileSet)
      processFileSet(path
                     , outputDir
                     , uniqueHdu
                     , hduNameSeq
                     , scliceIndexSeq)
    else {
      if (MyFile.fileExist(path))
        scliceImage(
            path
          , outputDir
          , uniqueHdu
          , hduNameSeq
          , scliceIndexSeq)

      if (Path.directoryExist(path))
        sliceDirectory(
            path
          , outputDir
          , uniqueHdu
          , hduNameSeq
          , scliceIndexSeq)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FitsSliceCube.scala
//=============================================================================
