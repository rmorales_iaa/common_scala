/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  24/Oct/2022
 * Time:  13h:11m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.fits
//=============================================================================
import com.common.fits.standard.structure.hdu.primary.Primary
import com.common.logger.MyLogger
import com.common.util.path.Path
//=============================================================================
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
object FitsDump extends MyLogger {
  //---------------------------------------------------------------------------
  private def dumpRaw(fitsName:String, oDir:String, applyLinearConversion: Boolean): Unit = {
    val fits = FitsLoad.load(fitsName
      , readDataBlockFlag = true
      , printDisconfFlag = false)
    if (fits.isDefined) {
      //get primary header
      val structureSeq = fits.get.structureSeq
      val primaryHeaderRecordMap = structureSeq.head.asInstanceOf[Primary].recordMap

      //dump all structures
      structureSeq.zipWithIndex.foreach { case (structure,i) =>
        structure.dump(Path.resetDirectory(s"${oDir}structure_${f"${i+1}%03d"}"), primaryHeaderRecordMap, applyLinearConversion) }
    }
  }

  //---------------------------------------------------------------------------
  def dump(fitsName: String, oDir: String, applyLinearConversion: Boolean): Unit = {
    Try { dumpRaw(fitsName, oDir, applyLinearConversion)}
    match {
      case Success(_) =>
      case Failure(e) =>
        error(s"Error dumping FITS file: '$fitsName'")
        error(e.toString)
        error(e.getStackTrace.mkString("\n"))
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FitsDump.scala
//=============================================================================
