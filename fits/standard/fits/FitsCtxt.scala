/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Sep/2022
 * Time:  16h:54m
 * Description: FITS standard: sectionName 3.1 Overall file structure
 */
//=============================================================================
package com.common.fits.standard.fits
//=============================================================================
//=============================================================================
//-----------------------------------------------------------------------------
object FitsCtxt {
  //---------------------------------------------------------------------------
  def apply(ctxt:FitsCtxt): FitsCtxt = {
    val newCtxt = FitsCtxt(ctxt.structureID, ctxt.streamName)
    newCtxt.info = ctxt.info
    newCtxt.position = ctxt.position
    newCtxt.extraInfo = ctxt.extraInfo
    newCtxt
  }
  //---------------------------------------------------------------------------
  def apply(): FitsCtxt = FitsCtxt(-1,"None")
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
case class FitsCtxt(structureID: Long, streamName: String) {
  //---------------------------------------------------------------------------
  var info : String = ""
  var position = 0L
  var extraInfo : String = ""
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FitsCtxt.scala
//=============================================================================
