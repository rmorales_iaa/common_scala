/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  06/Aug/2023
  * Time:  09h:50m
  * Description: None
  */
package com.common.fits.standard.fits.factory.table
//=============================================================================
import com.common.csv.CsvDataType._
import com.common.csv.{CsvDataType, CsvRead}
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.fits.factory.FitsFactory
import com.common.logger.MyLogger
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.AsciiTableColDef
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.tform._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.BinTableColDef
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tform._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.{TableColBuild, TableRangeValue}
import com.common.util.string.MyString
import com.common.util.util.Util
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object FitsBuildTable extends MyLogger {
  //---------------------------------------------------------------------------
  private def buildAsciiColDefInteger[T](colIndex: Int
                                         , colNameSeq: Array[String]
                                         , colMinValue: Array[AnyVal]
                                         , colMaxValue: Array[AnyVal]) = {
    val minValue = colMinValue(colIndex).asInstanceOf[T].toString
    val maxValue = colMaxValue(colIndex).asInstanceOf[T].toString
    val tableRangeValue = Some(TableRangeValue(Some(minValue.toDouble), Some(maxValue.toDouble), None, None))
    AsciiTableColDef(
      colIndex
      , position = 1 //it will be updated latter
      , name = Some(colNameSeq(colIndex))
      , form = AsciiTableTformInteger(Math.max(minValue.size, maxValue.size))
      , tableRangeValue = tableRangeValue)
  }
  //---------------------------------------------------------------------------
  private def buildAsciiColDefFloat(colIndex: Int
                                    , colNameSeq: Array[String]
                                    , colMinValue: Array[AnyVal]
                                    , colMaxValue: Array[AnyVal]
                                    , isFloat32: Boolean) = {

    val minValue = colMinValue(colIndex).asInstanceOf[Double]
    val maxValue = colMaxValue(colIndex).asInstanceOf[Double]
    val minValueString = minValue.toString
    val maxValueString = maxValue.toString
    val tableRangeValue = Some(TableRangeValue(Some(minValue), Some(maxValue), None, None))
    val maxDecimalCount = Math.max(Util.getDecimalCount(minValueString)
                                 , Util.getDecimalCount(maxValueString))
    val maxCharSize = Math.max(minValueString.size, maxValueString.size)

    val minValueIsFixedPointFloat = minValueString.split("[.eE]").size <= 2
    val maxValueIsFixedPointFloat = maxValueString.split("[.eE]").size <= 2

    val form =
      if (minValueIsFixedPointFloat && maxValueIsFixedPointFloat)
        AsciiTableTformFloatFixed(maxCharSize, maxDecimalCount)
      else {
        if (isFloat32) AsciiTableTformFloat_32(maxCharSize, maxDecimalCount)
        else AsciiTableTformFloat_64(maxCharSize, maxDecimalCount)
      }

    AsciiTableColDef(
      colIndex
      , position = 1 //it will be updated latter
      , name = Some(colNameSeq(colIndex))
      , form = form
      , tableRangeValue = tableRangeValue)
  }
  //---------------------------------------------------------------------------
  private def getFitsAsciiTableColDefSeq(colDataTypeSeq: Array[CsvDataType[_]]
                                         , colNameSeq: Array[String]
                                         , colMinValue: Array[AnyVal]
                                         , colMaxValue: Array[AnyVal]): Array[AsciiTableColDef] = {

    (for (colIndex <- 0 until colDataTypeSeq.size) yield {
      colDataTypeSeq(colIndex) match {

        case _: CsvDataTypeByte  => buildAsciiColDefInteger[Byte](colIndex, colNameSeq, colMinValue, colMaxValue)
        case _: CsvDataTypeShort => buildAsciiColDefInteger[Short](colIndex, colNameSeq, colMinValue, colMaxValue)
        case _: CsvDataTypeChar  => buildAsciiColDefInteger[Short](colIndex, colNameSeq, colMinValue, colMaxValue)
        case _: CsvDataTypeInt   => buildAsciiColDefInteger[Int](colIndex, colNameSeq, colMinValue, colMaxValue)
        case _: CsvDataTypeLong  => buildAsciiColDefInteger[Long](colIndex, colNameSeq, colMinValue, colMaxValue)

        case _: CsvDataTypeFloat | _: CsvDataTypeDouble  =>
          buildAsciiColDefFloat(colIndex
                                , colNameSeq
                                , colMinValue
                                , colMaxValue
                                , isFloat32 = false)

        case _: CsvDataTypeString =>
          val minValue = colMinValue(colIndex).asInstanceOf[Long]
          val maxValue = colMaxValue(colIndex).asInstanceOf[Long]
          val tableRangeValue = Some(TableRangeValue(Some(minValue), Some(maxValue), None, None))
          AsciiTableColDef(
            colIndex + 1
            , position = 1 //it will be updated latter
            , name = Some(colNameSeq(colIndex))
            , form = AsciiTableTformCharacter(maxValue)
            , tableRangeValue = tableRangeValue)
      }
    }).toArray
  }
  //---------------------------------------------------------------------------
  private def getFitsBinTableColDefSeq(colDataTypeSeq: Array[CsvDataType[_]]
                                         , colNameSeq: Array[String]
                                         , colMinValue: Array[AnyVal]
                                         , colMaxValue: Array[AnyVal]): Array[BinTableColDef] = {
    (for(colIndex<-0 until colDataTypeSeq.size) yield {

     colDataTypeSeq(colIndex) match {

        case _: CsvDataTypeByte |
             _: CsvDataTypeShort |
             _: CsvDataTypeChar =>
          val minValue = colMinValue(colIndex).asInstanceOf[Short]
          val maxValue = colMaxValue(colIndex).asInstanceOf[Short]
          val tableRangeValue = Some(TableRangeValue(Some(minValue), Some(maxValue), None, None))
          BinTableColDef(
            colIndex + 1
            , position = 1 //it will be updated latter
            , name = Some(colNameSeq(colIndex))
            , form = BinTableTformInt_16()
            , tableRangeValue = tableRangeValue)

        case _: CsvDataTypeInt =>
          val minValue = colMinValue(colIndex).asInstanceOf[Int]
          val maxValue = colMaxValue(colIndex).asInstanceOf[Int]
          val tableRangeValue = Some(TableRangeValue(Some(minValue), Some(maxValue), None, None))
          BinTableColDef(
            colIndex + 1
            , position = 1 //it will be updated latter
            , name = Some(colNameSeq(colIndex))
            , form = BinTableTformInt_32()
            , tableRangeValue = tableRangeValue)

        case _: CsvDataTypeLong =>
          val minValue = colMinValue(colIndex).asInstanceOf[Long]
          val maxValue = colMaxValue(colIndex).asInstanceOf[Long]
          val tableRangeValue = Some(TableRangeValue(Some(minValue), Some(maxValue), None, None))
          BinTableColDef(
            colIndex + 1
            , position = 1 //it will be updated latter
            , name = Some(colNameSeq(colIndex))
            , form = BinTableTformInt_64()
            , tableRangeValue = tableRangeValue)

        //all doubles to avoid rounding problems (i.e. "2460146.40727176")
        case _: CsvDataTypeFloat | _: CsvDataTypeDouble  =>
          val minValue = colMinValue(colIndex).asInstanceOf[Double]
          val maxValue = colMaxValue(colIndex).asInstanceOf[Double]
          val tableRangeValue = Some(TableRangeValue(Some(minValue), Some(maxValue), None, None))
          BinTableColDef(
            colIndex + 1
            , position = 1 //it will be updated latter
            , name = Some(colNameSeq(colIndex))
            , form = BinTableTformFloat_64()
            , tableRangeValue = tableRangeValue)

        case _: CsvDataTypeString =>
          val maxValue = colMaxValue(colIndex).asInstanceOf[Long]
          BinTableColDef(
            colIndex + 1
            , position = 1 //it will be updated latter
            , name = Some(colNameSeq(colIndex))
            , form = BinTableTformCharacter(maxValue.toInt))
      }
    }).toArray
  }
  //---------------------------------------------------------------------------
  private def getFitsTableColDefSeq(colDataTypeSeq: Array[CsvDataType[_]]
                                    , colNameSeq: Array[String]
                                    , colMinValue: Array[AnyVal]
                                    , colMaxValue: Array[AnyVal]
                                    , isAsciiTable: Boolean): Array[TableColBuild] =
    if (isAsciiTable)
      getFitsAsciiTableColDefSeq(colDataTypeSeq, colNameSeq, colMinValue, colMaxValue).map { TableColBuild.build(_) }
    else
      getFitsBinTableColDefSeq(colDataTypeSeq, colNameSeq, colMinValue, colMaxValue).map { TableColBuild.build(_) }
  //---------------------------------------------------------------------------
  private def getTableColMinMax(csv:CsvRead
                                , colDataTypeSeq: ArrayBuffer[CsvDataType[_]]
                                , isAsciiTable: Boolean): Array[TableColBuild] = {
    val colNameSeq = csv.getColNameSeq
    val colMinValue = ArrayBuffer[AnyVal]()
    val colMaxValue = ArrayBuffer[AnyVal]()
    var isFirstRow = true
    //-------------------------------------------------------------------------
    csv.getRowSeq.foreach { row =>
      colNameSeq.zipWithIndex.foreach { case (colName, i) =>
        val colValue = row(colName)
        colDataTypeSeq(i) match {
          case  _: CsvDataTypeByte   =>
            val v = colValue.toByte
            if (isFirstRow) {
              colMinValue += v
              colMaxValue += v
            }
            else {
              val actualMinValue = colMinValue(i).asInstanceOf[Byte]
              val actualMaxValue = colMaxValue(i).asInstanceOf[Byte]
              if (v < actualMinValue) colMinValue(i) = v
              if (v > actualMaxValue) colMaxValue(i) = v
            }

          case  _: CsvDataTypeShort | _:CsvDataTypeChar =>
            val v = colValue.toShort
            if (isFirstRow) {
              colMinValue += v
              colMaxValue += v
            }
            else {
              val actualMinValue = colMinValue(i).asInstanceOf[Short]
              val actualMaxValue = colMaxValue(i).asInstanceOf[Short]
              if (v < actualMinValue) colMinValue(i) = v
              if (v > actualMaxValue) colMaxValue(i) = v
            }

          case  _: CsvDataTypeInt    =>
            val v = colValue.toInt
            if (isFirstRow) {
              colMinValue += v
              colMaxValue += v
            }
            else {
              val actualMinValue = colMinValue(i).asInstanceOf[Int]
              val actualMaxValue = colMaxValue(i).asInstanceOf[Int]
              if (v < actualMinValue) colMinValue(i) = v
              if (v > actualMaxValue) colMaxValue(i) = v
            }

          case  _: CsvDataTypeLong   =>
            val v = colValue.toLong
            if (isFirstRow) {
              colMinValue += v
              colMaxValue += v
            }
            else {
              val actualMinValue = colMinValue(i).asInstanceOf[Long]
              val actualMaxValue = colMaxValue(i).asInstanceOf[Long]
              if (v < actualMinValue) colMinValue(i) = v
              if (v > actualMaxValue) colMaxValue(i) = v
            }

          //all doubles to avoid rounding problems (i.e. "2460146.40727176")
          case  _: CsvDataTypeFloat | _: CsvDataTypeDouble =>
            val v = colValue.toDouble
            if (isFirstRow) {
              colMinValue += v
              colMaxValue += v
            }
            else {
              val actualMinValue = colMinValue(i).asInstanceOf[Double]
              val actualMaxValue = colMaxValue(i).asInstanceOf[Double]
              if (v < actualMinValue) colMinValue(i) = v
              if (v > actualMaxValue) colMaxValue(i) = v
            }

          case _: CsvDataTypeString =>
            val v: Long = colValue.size
            if (isFirstRow) {
              colMinValue += v
              colMaxValue += v
            }
            else {
              val actualMinValue = colMinValue(i).asInstanceOf[Long]
              val actualMaxValue = colMaxValue(i).asInstanceOf[Long]
              if (v < actualMinValue) colMinValue(i) = v
              if (v > actualMaxValue) colMaxValue(i) = v
            }
        }
      }
      isFirstRow = false
    }
    getFitsTableColDefSeq(colDataTypeSeq.toArray
                          , colNameSeq.toArray
                          , colMinValue.toArray
                          , colMaxValue.toArray
                          , isAsciiTable)
  }
  //---------------------------------------------------------------------------
  private def buildTableColDefSeq(csv:CsvRead, isAsciiTable: Boolean): Array[TableColBuild] = {
    val colNameSeq = csv.getColNameSeq
    val colDataTypeSeq = ArrayBuffer[CsvDataType[_]]()
    var isFirstRow = true

    csv.getRowSeq.foreach { row=>
      colNameSeq.zipWithIndex.foreach { case (colName,i) =>

        val colValue = row(colName)
        var csvDataType = CsvDataType.guessDataType(colValue)

        //unify types in bin tables
        if (!isAsciiTable) {
          csvDataType match {
            case _: CsvDataTypeByte |
                 _: CsvDataTypeShort |
                 _: CsvDataTypeChar =>  csvDataType = CsvDataTypeShort()
            case _ =>
          }
        }

        if (isFirstRow)  colDataTypeSeq += csvDataType
        else {
          if (csvDataType.order > colDataTypeSeq(i).order) //update data type if it is necessary
            colDataTypeSeq(i) = csvDataType
        }
      }
      isFirstRow = false
    }
    //calculating min and max values
    info(s"Calculating min and max values for each column")
    getTableColMinMax(csv, colDataTypeSeq, isAsciiTable)
  }
  //---------------------------------------------------------------------------
  private def getAsciiRowSeq(csv: CsvRead, tableColDefSeq: Array[TableColBuild] ) = {
    csv.getRowSeq map { row =>
      val valueSeq = (row.getItemSeq zip tableColDefSeq).map { case (col, colDef) =>
        val columnExpectedSize = colDef.form.asInstanceOf[AsciiTableTform].charSize.toInt
        val formattedValue = colDef.form.format(col.value.take(columnExpectedSize), FitsCtxt())
        formattedValue.take(columnExpectedSize)
      }.toArray
      if (csv.itemDivider == "\\t") valueSeq.mkString("\t")
      else valueSeq.mkString(csv.itemDivider)
    }
  }
  //---------------------------------------------------------------------------
  private def getBinRowSeq(csv: CsvRead, tableColDefSeq: Array[TableColBuild]): Array[Array[Byte]] = {
    csv.getRowSeq map { row =>
      (row.getItemSeq zip tableColDefSeq).flatMap { case (col, colDef) =>
        colDef.form.asInstanceOf[BinTableTform] match {
          case t: BinTableTformCharacter =>
            val charSize = t.repeatCount
             t.format(MyString.rightPadding(col.value.take(charSize), charSize))
          case t =>
             t.format(col.value)
        }
      }.toArray
    }
  }
  //---------------------------------------------------------------------------
  def build(csvFilename: String
            , csvHasHeader: Boolean
            , csvItemDivider: String
            , fitsIsAsciiTable: Boolean
            , fitsFileName: String): Boolean = {

    info(s"Loading csv file: '$csvFilename'")
    val csv = CsvRead(csvFilename
                      , hasHeader= csvHasHeader
                      , itemDivider = csvItemDivider)
    if (!csv.read())
      return error(s"Error parsing csv file:'$csvFilename'")

    //build col definition
    info(s"Guessing data types:'$csvFilename'")
    val tableColDefSeq = buildTableColDefSeq(csv, fitsIsAsciiTable)

    //building FITS file from column definition
    if (tableColDefSeq.isEmpty) return error("Can not find a valid table column definition")
    else {
      info(s"Creating a FITS file with a table with:${tableColDefSeq.size} columns")
      if (fitsIsAsciiTable)
        FitsFactory.asciiTable(
          fitsFileName
          , mainHduBitPix = 8
          , mainHduAxisSeq = Array()
          , colDefSeq = tableColDefSeq
          , rowSeq = getAsciiRowSeq(csv,tableColDefSeq))
      else
        FitsFactory.binTable(
          fitsFileName
          , mainHduBitPix = 8
          , mainHduAxisSeq = Array()
          , colDefSeq = tableColDefSeq
          , rowSeq = getBinRowSeq(csv, tableColDefSeq))
    }

    info(s"Created FITS file:'$fitsFileName'")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file FitsBuildTable.scala
//=============================================================================