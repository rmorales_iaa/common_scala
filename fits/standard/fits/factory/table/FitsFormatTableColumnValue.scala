/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  24/Oct/2022
 * Time:  13h:11m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.fits.factory.table
//=============================================================================
import com.common.fits.standard.block.record.RecordNumber.RecordInteger
import com.common.fits.standard.Keyword.KEYWORD_TFIELDS
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.disconformity.Disconformity
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.TableColDef
import com.common.logger.MyLogger
import com.common.util.string.MyString.leftPadding
//=============================================================================
//=============================================================================
object FitsFormatTableColumnValue extends MyLogger {
  //---------------------------------------------------------------------------
  private def printResult(inputString: String
                         , form: String
                         , tFormString: String
                         , display:String
                         , tDispString: String) = {
    println(s"........................................................")
    println(s"value         : '$inputString' size(${inputString.size})")
    println(s"format  format: '$form'    => '$tFormString' size(${tFormString.size})")
    if (!display.isEmpty)
      println(s"display format: '$display' => '$tDispString' size(${tDispString.size})")
    println(s"........................................................")
  }
  //---------------------------------------------------------------------------
  def format(form: String
             , value: String
             , display: String
             , isAsciiTable: Boolean
             , valueByteSeq: Array[Byte] = Array[Byte]()): Unit = {

    val recordMap = RecordMap()
    TableColDef.filldMap(
      recordMap
      , colIndex = 1
      , startingPos = 0L
      , form = form
      , disp = if (display.isEmpty) None else Some(display)
      , isAsciiTable = isAsciiTable)

    if (recordMap.isEmpty) {
      Disconformity.print("None")
      println("Error parsing the column definition")
      return
    }
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, 1))

    isAsciiTable match {
      case true =>  //ASCII table

        val col= TableColDef.toAsciiTableColDef(
          TableColDef.buildColDefSeq(recordMap, isAciiTable = true, SectionName.SECTION_NONE).get).head

        val tForm = col.form
        //ensure the size of the input. The input comes from a sclice of a table row, so
        // the slice has that has always the proper size
        val input = leftPadding(value.take(tForm.charSize.toInt), tForm.charSize.toInt)
        printResult(input
          , form
          , tFormString = tForm.format(input, FitsCtxt())
          , display
          , tDispString  = col.format(input, 1))

      case false =>  //binary table

         println("Not defined")
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FitsFormatTableColumnValue.scala
//=============================================================================
