/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  04/Oct/2022
 * Time:  09h:56m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.fits.factory
//=============================================================================
//=============================================================================
import com.common.fits.standard.block.Block
import com.common.fits.standard.block.Block.{BLOCK_ASCII_DATA_PADDING_BYTE, BLOCK_BYTE_DATA_PADDING_BYTE}
import com.common.fits.standard.block.record.Record
import com.common.fits.standard.block.record.Record.{RecordSeq, VALUE_INDICATOR}
import com.common.fits.standard.block.record.RecordCharacter.RecordUnknownKeywordValue
import com.common.fits.standard.block.record.parser.RecordParser
import com.common.fits.standard.ItemSize.{KEYWORD_CHAR_SIZE, RECORD_CHAR_SIZE}
import com.common.fits.standard.dataType.DataType.DATA_TYPE_U_INT_8
import com.common.fits.standard.disconformity.Disconformity
import com.common.fits.standard.fits.{Fits, FitsCtxt}
import com.common.fits.standard.structure.Structure
import com.common.fits.standard.structure.hdu.extension.Extension
import com.common.fits.standard.structure.hdu.extension.Extension._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.TableColBuild
import com.common.fits.standard.structure.hdu.primary.Primary
import com.common.fits.standard.structure.hdu.randomGroup.RandomGroup
import com.common.fits.standard.structure.special.Special
import com.common.logger.MyLogger
import com.common.util.string.MyString.rightPadding
//=============================================================================
object FitsFactory extends MyLogger {
  //---------------------------------------------------------------------------
  def buildRecord(keyword:String,value: String): Option[Record[_]] = {
    val recordLine = rightPadding(rightPadding(keyword, KEYWORD_CHAR_SIZE) + VALUE_INDICATOR + value
                                  , RECORD_CHAR_SIZE)
    RecordParser.run(recordLine) match {
      case _: RecordUnknownKeywordValue => None
      case r: Record[_] => Some(r)
    }
  }
  //---------------------------------------------------------------------------
  private def hduPrimary(bitPix: Int
                          , axisSeq: Array[Long]
                          , recordSeq: RecordSeq = Array()
                          , dataByteSeq: Array[Byte]  = Array()) = {
    val recordMap = Primary.buildHeader(bitPix
                                        , axisSeq
                                        , recordSeq)
    val dataBlockByteSize = Primary.getDataBlockByteSize(recordMap)
    val dataBlockSeq = Block.build(dataBlockByteSize,dataByteSeq)
    Primary(recordMap, dataBlockSeq)
  }
  //---------------------------------------------------------------------------
  private def hduExtension(bitPix: Int
                            , axisSeq: Array[Long]
                            , dataByteSeq: Array[Byte]  = Array()
                            , recordSeq: RecordSeq = Array()
                            , extName: String
                            , extPcount: Int
                            , extGcount: Int
                            , fitsName: String) = {
    val recordMap = Extension.buildHeader(bitPix
                                          , axisSeq
                                          , recordSeq
                                          , extName
                                          , extPcount
                                          , extGcount)

    val paddingByte = extName match {
      case  EXTENSION_STANDARD_ASCII_TABLE_NAME => BLOCK_ASCII_DATA_PADDING_BYTE
      case _                                    => BLOCK_BYTE_DATA_PADDING_BYTE
    }

    val dataBlockByteSize = Extension.getDataBlockByteSize(recordMap)
    val dataBlockSeq = Block.build(dataBlockByteSize,dataByteSeq,paddingByte = paddingByte)
    Extension.build(recordMap, dataBlockSeq, FitsCtxt(1,fitsName))
  }
  //---------------------------------------------------------------------------
  private def hduRandomGroup(mainHduBitPix: Int
                             , mainHduAxisSeq: Array[Long]
                             , mainHduRecordSeq: RecordSeq = Array()
                             , randomGroupParameterNameSeq: Array[String]
                             , randomGroupParameterValueSeq: Array[Array[Byte]]
                             , randomGroupDataSeq: Array[Array[Byte]]) = {
    val randomGroupCount =  randomGroupDataSeq.length
    val recordMap = RandomGroup.buildHeader(mainHduBitPix
                                            , mainHduAxisSeq
                                            , mainHduRecordSeq
                                            , randomGroupParameterNameSeq
                                            , randomGroupCount)
    val parameterValueByteSize = (randomGroupParameterValueSeq map ( _.length)).sum
    val groupDataByteSize = randomGroupCount * {
      if (randomGroupDataSeq.isEmpty) 0
      else randomGroupDataSeq.head.length //all groups have same size
    }
    val dataBlockByteSize = Block.getBlockCount((parameterValueByteSize + groupDataByteSize) * randomGroupCount)
    val randomGroupParameterValuePackedSeq = randomGroupParameterValueSeq.flatten
    val dataSeq = randomGroupDataSeq.map { randomGroupParameterValuePackedSeq ++ _ }.flatten
    val dataBlockSeq = Block.build(dataBlockByteSize,dataSeq)
    RandomGroup(recordMap, dataBlockSeq)
  }
  //---------------------------------------------------------------------------
  def primary(fitsName: String
              , bitPix: Int
              , axisSeq: Array[Long]
              , dataByteSeq: Array[Byte]  = Array()
              , recordSeq: RecordSeq = Array()
              , additionalStructureSeq: Array[Structure] = Array()
              , saveFile: Boolean = true): Fits = {
    val hdu = hduPrimary(
        bitPix
      , axisSeq
      , recordSeq
      , dataByteSeq)
    val fits = Fits(fitsName, Array(hdu) ++ additionalStructureSeq)
    if (saveFile) fits.saveLocal(fitsName)
    fits
  }
  //---------------------------------------------------------------------------
  def extension(fitsName: String
                , mainHduBitPix: Int
                , mainHduAxisSeq: Array[Long]
                , mainHduDataByteSeq: Array[Byte]  = Array()
                , mainHduRecordSeq: RecordSeq = Array()
                , extName: String
                , extBitPix: Int
                , extensionAxisSeq: Array[Long]
                , extensionData: Array[Byte] = Array()
                , extRecordSeq: RecordSeq = Array()
                , pcount: Int
                , gcount: Int
                , additionalStructureSeq: Array[Structure] = Array()
                , saveFile: Boolean = true): Option[Fits] = {

    val hdu = hduPrimary(
        mainHduBitPix
      , mainHduAxisSeq
      , mainHduRecordSeq
      , mainHduDataByteSeq)

    val extensionHeader = hduExtension(extBitPix
                                      , extensionAxisSeq
                                      , extensionData
                                      , extRecordSeq
                                      , extName
                                      , pcount
                                      , gcount
                                      , fitsName).getOrElse {
      Disconformity.print(fitsName)
      return None
    }

    val fits = Fits(fitsName, Array(hdu, extensionHeader) ++ additionalStructureSeq)
    if (saveFile) fits.saveLocal(fitsName)
    Some(fits)
  }
  //---------------------------------------------------------------------------
  def specialRecord(fitsName: String
                    , bitPix: Int
                    , axisSeq: Array[Long]
                    , recordSeq: RecordSeq = Array()
                    , dataByteSeq: Array[Byte]  = Array()
                    , specialByteSeq: Array[Byte]
                    , saveFile: Boolean = true) = {
    val hdu = hduPrimary(bitPix
      , axisSeq
      , recordSeq
      , dataByteSeq)

    val fits = new Fits(fitsName, Array(hdu
                                        , Special(Block.splitInBlockSeq(specialByteSeq))))
    if (saveFile) fits.saveLocal(fitsName)
    fits
  }
  //---------------------------------------------------------------------------
  def randomGroup(fitsName: String
                  , mainHduBitPix: Int
                  , mainHduAxisSeq: Array[Long]
                  , mainHduRecordSeq: RecordSeq = Array()
                  , randoGroupParameterNameSeq: Array[String]
                  , randomGroupParameterValueSeq: Array[Array[Byte]]
                  , randomGroupDataSeq: Array[Array[Byte]]
                  , additionalStructureSeq: Array[Structure] = Array()
                  , saveFile: Boolean = true) = {

    val hduPrimary = hduRandomGroup(mainHduBitPix
                                    , mainHduAxisSeq
                                    , mainHduRecordSeq
                                    , randoGroupParameterNameSeq
                                    , randomGroupParameterValueSeq
                                    , randomGroupDataSeq)
    val fits = new Fits(fitsName, Array(hduPrimary) ++ additionalStructureSeq)
    if (saveFile) fits.saveLocal(fitsName)
    fits
  }
  //---------------------------------------------------------------------------
  def asciiTable(fitsName: String
                 , mainHduBitPix: Int
                 , mainHduAxisSeq: Array[Long]
                 , mainHduRecordSeq: RecordSeq = Array()
                 , mainHduDataByteSeq: Array[Byte]  = Array()
                 , colDefSeq: Array[TableColBuild]
                 , rowSeq: Array[String]
                 , additionalStructureSeq: Array[Structure] = Array()
                 , saveFile: Boolean = true) = {

    val (extRecordMap,rowCharSize) =
      TableColBuild.buildRecordMap(colDefSeq, isAsciiTable = true)

    extension(fitsName
              , mainHduBitPix
              , mainHduAxisSeq
              , mainHduDataByteSeq
              , mainHduRecordSeq
              , extName = EXTENSION_STANDARD_ASCII_TABLE_NAME
              , extBitPix = DATA_TYPE_U_INT_8.bitSize //mandatory
              , extensionAxisSeq = Array(rowCharSize,rowSeq.size)
              , extensionData = (rowSeq map (_.getBytes())).flatten
              , extRecordMap.values.toArray.flatten
              , pcount = 0
              , gcount = 1 //mandatory
              , additionalStructureSeq
              , saveFile)
  }
  //---------------------------------------------------------------------------
  def binTable(fitsName: String
               , mainHduBitPix: Int
               , mainHduAxisSeq: Array[Long]
               , mainHduRecordSeq: RecordSeq = Array()
               , mainHduDataByteSeq: Array[Byte] = Array()
               , colDefSeq: Array[TableColBuild]
               , rowSeq: Array[Array[Byte]]
               , heap: Array[Byte] = Array[Byte]()
               , additionalStructureSeq: Array[Structure] = Array()
               , saveFile: Boolean = true) = {

    val (extRecordMap, _) =
      TableColBuild.buildRecordMap(colDefSeq, isAsciiTable = false)

    extension(fitsName
              , mainHduBitPix
              , mainHduAxisSeq
              , mainHduDataByteSeq
              , mainHduRecordSeq
              , extName = EXTENSION_STANDARD_BINTABLE_NAME
              , extBitPix = DATA_TYPE_U_INT_8.bitSize //mandatory
              , extensionAxisSeq = Array(if (rowSeq.isEmpty) 0 else rowSeq.head.size, rowSeq.size)
              , extensionData = rowSeq.flatten
              , extRecordMap.values.toArray.flatten
              , pcount = heap.size
              , gcount = 1 //mandatory
              , additionalStructureSeq
              , saveFile)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FitsFactory.scala
//=============================================================================
