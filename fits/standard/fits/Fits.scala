/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Sep/2022
 * Time:  16h:54m
 * Description: FITS standard: sectionName 3.1 Overall file structure
 */
//=============================================================================
package com.common.fits.standard.fits
//=============================================================================
import com.common.configuration.MyConf
import com.common.fits.standard.Keyword.{KEYWORD_BITPIX, KEYWORD_END, KEYWORD_NAXIS, KEYWORD_SUBIMAGE}
import com.common.fits.standard.block.Block
import com.common.fits.standard.block.record.RecordCharacter.{RecordEnd, RecordString}
import com.common.fits.standard.structure.Structure
import com.common.fits.standard.structure.hdu.HDU
import com.common.fits.standard.structure.hdu.extension.Extension
import com.common.fits.standard.structure.hdu.primary.Primary
import com.common.logger.MyLogger
import com.common.stream.local.LocalOutputStream
import com.common.util.file.MyFile
import com.common.util.path.Path
import com.common.stream.local.LocalInputStream
import com.common.util.string.MyString.rightPadding
//=============================================================================
import scala.util.{Try,Failure,Success}
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object Fits extends MyLogger {
  //---------------------------------------------------------------------------
  final val fitsExtensionSeq = {
    if (MyConf.c == null) List(".fit", ".fts", ".fits")
    else MyConf.c.getStringSeq("Common.fitsFileExtension")
  }
  //---------------------------------------------------------------------------
  def apply(name: String): Fits = Fits(name,Array[Structure]())
  //---------------------------------------------------------------------------
  def apply(name: String, structure: Structure): Fits = Fits(name,Array(structure))
  //---------------------------------------------------------------------------
}
//=============================================================================
import Fits._
case class Fits(name: String,
                structureSeq: Array[Structure])  {
  //---------------------------------------------------------------------------
  val shortName = Path.getOnlyFilenameNoExtension(name)
  //---------------------------------------------------------------------------
  def isSIF() = structureSeq.length == 1   //single image FITS (SECTION_3_1)
  //---------------------------------------------------------------------------
  def isMEF() = structureSeq.length > 1  //multi-extension FITS(MEF) (SECTION_3_1)
  //---------------------------------------------------------------------------
  def saveLocal(fitsName: String) = {
    val fileExtension = MyFile.getFileExtension(fitsName)
    val fExt = if (!fitsExtensionSeq.endsWith(fileExtension)) ".fits" else ""
    val stream = LocalOutputStream(Path.getParentPath(fitsName) + Path.getOnlyFilenameNoExtension(fitsName) + fExt)
    structureSeq.foreach( structure => structure.save(stream) )
    stream.close
  }
  //---------------------------------------------------------------------------
  def getRecordSeq() = {
    (structureSeq flatMap { structure =>
      structure match {
        case hdu: HDU => Some(hdu.recordMap.getPlainRecordSeq)
        case _        => None
      }
    }).flatten
  }
  //---------------------------------------------------------------------------
  def getSubImageSeq(xm: Int
                     , ym: Int
                     , xM: Int
                     , yM: Int
                     , copyMainHDU: Boolean = true): Array[Fits] = {
    //---------------------------------------------------------------------------
    val subImageSeq = ArrayBuffer[Fits]()
    //---------------------------------------------------------------------------
    structureSeq.foreach { structure =>
      structure match {
        case primaryHDU: Primary =>
          getSubImage(
              primaryHDU
            , subImageSeq
            , xm
            , ym
            , xM
            , yM
            , copyMainHDU)

        case extension: Extension =>
          //fixme: not implemented
          /*
          val extensionName = extension.recordMap.getFirstValue[String](KEYWORD_XTENSION).get
          extensionName match {
            case EXTENSION_STANDARD_IMAGE_NAME =>
              getSubImage(
                   extension
                , subImageSeq
                , xm
                , ym
                , xM
                , yM)
          }*/
      }
    }
    subImageSeq.toArray
  }
  //---------------------------------------------------------------------------
  private def getSubImage(hdu: HDU
                          , subImageSeq: ArrayBuffer[Fits]
                          , xm: Int
                          , ym: Int
                          , xM: Int
                          , yM: Int
                          , copyMainHDU: Boolean = true): Array[Fits] = {

    val hduRecordMap = hdu.recordMap
    val hduBytePerPixel = Math.abs(hduRecordMap.getBitPix() / 8)
    val hduAxesSeq = hduRecordMap.getAxisSeq()
    val hduDataOffset = hdu.dataBlockSeq.head.starPos
    val xAxisRange = xM - xm + 1
    val yAxisRange = yM - ym + 1
    val imageRowPixSize = hduAxesSeq.head

    if (hduAxesSeq.length == 1) {
      error(s"Error building sub-image. The data of the HDU has only one axe")
      return Array()
    }

    val subImageCount =
      if (hduAxesSeq.length > 2) hduAxesSeq.drop(2).product
      else 1

    Try {
      var currentOffset = hduDataOffset.toInt
      val imageByteSize = (hduAxesSeq.take(2).product * hduBytePerPixel).toInt
      val subImageRowByteSize = (xAxisRange * hduBytePerPixel).toInt
      val stream = LocalInputStream(name)
      for (hdu <- 1L to subImageCount) {
        //read sub image from stream
        val data = (for {y <- ym to yM} yield {

          val readPos = ((((y * imageRowPixSize) + xm) * hduBytePerPixel) + currentOffset).toInt
          val bytesToSkip = (readPos - stream.streamPointer).toInt
          if (!stream.setReadPos(bytesToSkip)) {
            info(s"Error building sub-image '$hdu/$subImageCount'. Error setting read pointer to position: '$readPos' ")
            stream.close()
            return Array()
          }
          val (okRead, subImageData) = stream.read(subImageRowByteSize)
          if (!okRead) {
            info(s"Error building sub-image '$hdu/$subImageCount'. Error reading from position: '$readPos' a total of '$subImageRowByteSize' bytes")
            stream.close()
            return Array()
          }
          subImageData
        }).flatten.toArray

        //fix head
        hduRecordMap.updateValue[Long](KEYWORD_NAXIS, 2)
        hduRecordMap.updateValue[Long](KEYWORD_BITPIX, hduRecordMap.getBitPix())
        hduRecordMap.updateValue[Long](KEYWORD_NAXIS + 1, xAxisRange)
        hduRecordMap.updateValue[Long](KEYWORD_NAXIS + 2, yAxisRange)
        hduRecordMap.remove(KEYWORD_END)
        hduRecordMap.append(RecordString(KEYWORD_SUBIMAGE, s"hdu:$hdu xPix:$xm yPix:$yM"))
        hduRecordMap.append(RecordEnd())

        //build new name
        val newName = Path.getParentPath(name) +
          Path.getOnlyFilenameNoExtension(name) +
          s"_hdu_" + f"$hdu%02d" + "_sub_" + f"${subImageSeq.length}%04d" +
          MyFile.getFileExtension(name)

        //build FITS
        subImageSeq +=
          Fits(newName
               , Primary(hduRecordMap
                         , Block.splitInBlockSeq(data)))

        //update current offset
        currentOffset += imageByteSize
      }

      //close stream
      stream.close()
    }
    match {
      case Success(_) =>

      case Failure(e) =>
        error(e.getMessage + s" Error loading the FITS file '$name'")
        error(e.getStackTrace.mkString("\n"))
        return Array()
    }
    subImageSeq.toArray
  }
  //---------------------------------------------------------------------------
  def getPrimaryHdu() = structureSeq.head.asInstanceOf[Primary]
  //---------------------------------------------------------------------------
  def printHeader() =
   println( getRecordSeq().mkString("\n") )
  //---------------------------------------------------------------------------
  def printDetailedInfo() = {
    val totalStructure = structureSeq.size
    var streamPos = 0L
    val summary =
      s"............................... Detailed structure info ............................." +
      s"\nName: '$name'" +
      s"\n....................................................................." +
      s"\nTotal structures: ${structureSeq.size}" +
      s"\n    ##############################################\n" +
      structureSeq.zipWithIndex.map { case (structure, i) =>
        val summary = structure.getSummary(s"${i+1}/$totalStructure", streamPos)
        streamPos = structure.getLastByteAsBlock
        summary
      }.mkString(   s"\n    ##############################################\n") +
      s"\n    ##############################################"+
      s"\n....................................................................." +
      s"\nStructure summary:" +
      s"\n    Pos Type       Name" +
      s"\n    ----------------------------------" +
      s"\n    ${structureSeq.zipWithIndex.map { case (s, i) =>
        s"${f"${i+1}%03d"} ${rightPadding(s.className,10)} ${s.getNormalizedName()}" }.mkString("\n    ")}" +
      s"\n....................................................................."
    println(summary)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Fits.scala
//=============================================================================
