/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Sep/2022
 * Time:  18h:25m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.block
//=============================================================================
import com.common.fits.standard.block.record.Record
import com.common.fits.standard.block.record.Record.RecordSeq
import com.common.fits.standard.block.record.RecordCharacter.{RecordBlank, RecordEnd, RecordError, RecordUnknownKeywordValue}
import com.common.fits.standard.block.record.parser.RecordParser
import com.common.fits.standard.ItemSize.{BLOCK_BYTE_SIZE, RECORD_CHAR_SIZE}
import com.common.fits.standard.Keyword.{KEYWORD_CHECKSUM, KEYWORD_COMMENT, KEYWORD_END, KEYWORD_HIERARCHY, KEYWORD_HISTORY}
import com.common.fits.standard.block.dataIntegrity.DataIntegrity.PartialDataSum
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.block.record.verifier.section_4_4.Section_4_4
import com.common.fits.standard.disconformity.{DisconfFatal, DisconfWarn, Disconformity}
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.sectionName.SectionName
import com.common.logger.MyLogger
import com.common.stream.MyInputStream
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object Block extends MyLogger {
  //----------------------------------------------------------------------------
  private final val MIN_VALID_BYTE = 0x20
  private final val MAX_VALID_BYTE = 0x7E
  //---------------------------------------------------------------------------
  final val BLOCK_BYTE_DATA_PADDING_BYTE   = 0x0.toByte
  final val BLOCK_ASCII_DATA_PADDING_BYTE  = 0x20.toByte   //space
  //---------------------------------------------------------------------------
  private final val ALLOWED_DUPLICATED_KEYWORD_SEQ = Seq(KEYWORD_COMMENT
                                                     , KEYWORD_HISTORY
                                                     , KEYWORD_HIERARCHY)
  //---------------------------------------------------------------------------
  def apply(pos: Long): Block = Block(pos, Array[Byte]()) //empty block
  //---------------------------------------------------------------------------
  def isValidByteSize(size: Long) = (size % BLOCK_BYTE_SIZE) == 0
  //---------------------------------------------------------------------------
  def getMaxValidByteSize(size: Long) = (size / BLOCK_BYTE_SIZE) * BLOCK_BYTE_SIZE
  //---------------------------------------------------------------------------
  def getBlockByteSize(blockCount: Long) = blockCount * BLOCK_BYTE_SIZE
  //---------------------------------------------------------------------------
  def getTrunkBlockCount(size: Long) = size / BLOCK_BYTE_SIZE
  //---------------------------------------------------------------------------
  def getLastBlockUsedByteSize(size: Long): Long = size % BLOCK_BYTE_SIZE
  //---------------------------------------------------------------------------
  def getLastBlockPaddingByteSize(size: Long): Long = {
    val remainder = getLastBlockUsedByteSize(size)
    if (remainder == 0) 0 else BLOCK_BYTE_SIZE - getLastBlockUsedByteSize(size)
  }
  //---------------------------------------------------------------------------
  def getBlockCount(size: Long) =  {
    val blockCount = size / BLOCK_BYTE_SIZE
    val remainByteCount = size % BLOCK_BYTE_SIZE
    if (remainByteCount > 0) blockCount + 1
    else blockCount
  }
  //---------------------------------------------------------------------------
  def getBlockCount(stream: MyInputStream, ctxt: FitsCtxt): (Long,Option[Disconformity]) = {
    val streamByteSize = stream.getByteSize
    val maxAllowedByteSize = getMaxValidByteSize(streamByteSize)
    val remainByteCount = streamByteSize - maxAllowedByteSize

    streamByteSize match {
      case 0 =>
        (0
          , Some(DisconfFatal(ctxt
                              , SectionName.SECTION_3_1
                              , s"Structure: ${ctxt.structureID} the stream has zero byte size. It must be an integral multiplier of $BLOCK_BYTE_SIZE")))

      case _ if streamByteSize < BLOCK_BYTE_SIZE =>
        (0
          , Some( DisconfFatal(ctxt
                                , SectionName.SECTION_3_1
                                , s"Structure: ${ctxt.structureID} the stream with bytes size: $streamByteSize has less than one dataBlock size: $BLOCK_BYTE_SIZE bytes. It must be an integral multiplier of $BLOCK_BYTE_SIZE")))

      case _ if !isValidByteSize(streamByteSize)=>
        DisconfWarn(ctxt
                    , SectionName.SECTION_3_1
                    , s"Structure: ${ctxt.structureID} has an invalid byte size. It must be an integral multiplier of $BLOCK_BYTE_SIZE"
                    , additionalInfo = s"Stream byte size: $streamByteSize. Assuming max allowed size: $maxAllowedByteSize and ignoring last: $remainByteCount bytes")
        (getBlockCount(maxAllowedByteSize), None)

      case _ =>  (getBlockCount(maxAllowedByteSize),None)
    }
  }
  //---------------------------------------------------------------------------
  //verfication by inspection:  Section_3_1_spec
  def isAllASCII(block: Block, streamName: String
                 , streamReadPosition: Long
                 , ctxt: FitsCtxt): Boolean = {
    var index = 0
    block.byteSeq.foreach { b =>
      index += 1
      if ((b < MIN_VALID_BYTE) || (b > MAX_VALID_BYTE)) {
        DisconfFatal(
          ctxt
          , SectionName.SECTION_3_2
          , s"Structure: ${ctxt.structureID} no all characters in the block are allowed ASCII characters. Character pos: $index. Stream read position: $streamReadPosition")
      }
    }
    true
  }
  //---------------------------------------------------------------------------
  def skip(stream: MyInputStream, blockCount: Long = 1, ctxt: FitsCtxt): Boolean = {
    val currentPos = stream.getReadPosition
    if (stream.skip((blockCount * BLOCK_BYTE_SIZE).toInt)) true
    else {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_NONE
        , s"Structure: ${ctxt.structureID} error skipping from stream: '${stream.name}' $blockCount blocks with: " + s"${BLOCK_BYTE_SIZE * blockCount} bytes. " +
          s"Starting read position: $currentPos current stream read position: ${stream.getReadPosition}")
      true
    }
  }
  //---------------------------------------------------------------------------
  def readFromStream(stream: MyInputStream, ctxt: FitsCtxt): Option[Block] = {
    val startPos = stream.getReadPosition
    if (!stream.canRead(BLOCK_BYTE_SIZE)) { //check if ther   e are enough bytes
      DisconfFatal(
        ctxt
        , SectionName.SECTION_3_1
        , s"Structure: ${ctxt.structureID} error reading from the stream a block with : $BLOCK_BYTE_SIZE bytes")
      return None
    }

    val (sucess, byteSeq) = stream.read(BLOCK_BYTE_SIZE) //slice the stream is blocks, this guarantees the min expected size per record: BLOCK_BYTE_SIZE
    if (!sucess) {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_3_1
        , s"Structure: ${ctxt.structureID} error reading from stream a block with : $BLOCK_BYTE_SIZE bytes")
      None
    }
    else Some(Block(startPos, byteSeq.toArray))
  }
  //---------------------------------------------------------------------------
  private def parseRecordSeq(block: Block
                             , hduHeaderDataSum: PartialDataSum
                             , ctxt: FitsCtxt): Option[RecordSeq] = {
    //------------------------------------------------------------------------
    def getDisconfFatal(r: Record[_],prefix: String = "") =
      DisconfFatal(ctxt.streamName
        , SectionName.SECTION_NONE
        , r.value.toString
        , Array(prefix + r.keyword,r.comment)
       )
    //------------------------------------------------------------------------
    val recordSeq = ArrayBuffer[Record[_]]()
    //slice the dataBlock is records, this guarantees the min expected size per record: RECORD_CHAR_SIZE
    block.byteSeq.grouped(RECORD_CHAR_SIZE).foreach { byteSeq =>
      val record = RecordParser.run(new String(byteSeq))
      record match {
        case r: RecordError               => getDisconfFatal(r)
        case r: RecordUnknownKeywordValue => getDisconfFatal(r,"Unknown keyword value")
        case _                            =>
          if (record.keyword != KEYWORD_CHECKSUM) hduHeaderDataSum.update(byteSeq) //update data sum of the header avoiding the record 'CHECKSUM' itself
          else hduHeaderDataSum.updateRecordCheckSum(byteSeq) //special case gor record checksum
          recordSeq += record
      }
    }
    Some(recordSeq.toArray)
  }
  //---------------------------------------------------------------------------
  private def findPositionRecordEnd(recordSeq: Array[Record[_]]): Long = {
    recordSeq.zipWithIndex.foreach { case (r, i) =>
      r match {
        case _: RecordEnd => return i
        case _ =>
      }
    }
    -1L
  }
  //---------------------------------------------------------------------------
  private def ignoreRecordAfterEndRecord(recordSeq: Array[Record[_]]
                                        , ctxt: FitsCtxt): Array[Record[_]] = {
    //-------------------------------------------------------------------------
    val indexRecordEnd = findPositionRecordEnd(recordSeq)
    recordSeq.takeRight(recordSeq.length - indexRecordEnd.toInt - 1) foreach { record =>
      record match {
        case r: RecordBlank =>
          if (!r.allCharAreBlank()) {
            DisconfWarn(
              ctxt
              , s"Structure: ${ctxt.structureID} found valid records after the '$KEYWORD_END' record, ignoring them"
              , SectionName.SECTION_NONE)
            return recordSeq.take(indexRecordEnd.toInt + 1)
          }
      }
    }
    recordSeq.take(indexRecordEnd.toInt + 1)
  }
  //---------------------------------------------------------------------------
  //verification by inspection:  Section_3_1_spec
  def readHeader(stream: MyInputStream
                 , hduHeaderDataSum: PartialDataSum
                 , ctxt: FitsCtxt
                 , headerBlockStorage: ArrayBuffer[Block] = null):
  Option[RecordMap] = {
    val recordMap = RecordMap()
    var continue = true
    while (continue) {
      val block = readFromStream(stream, ctxt).getOrElse(return None)
      if (headerBlockStorage != null) headerBlockStorage += block
      //used in 'special records' structure process
      if (!isAllASCII(block, stream.name, stream.getReadPosition,
        ctxt)) return None
      val recordSeq = parseRecordSeq(block
        , hduHeaderDataSum
        , ctxt).getOrElse(return None)
      if (findPositionRecordEnd(recordSeq) != -1) {
        recordMap.appendSeq(ignoreRecordAfterEndRecord(recordSeq, ctxt))
        if (!Section_4_4.verify(recordMap, ctxt)) return None
        else continue = false
      }
      else recordMap.appendSeq(recordSeq)
    }

    //check the record map
    if (recordMap.size == 0) {
      DisconfFatal(
        ctxt
        , s"Structure: ${ctxt.structureID} no valid records found"
        , SectionName.SECTION_NONE)
      return None
    }

    //check duplicated keywords
    val duplicatedKeyword = recordMap.getDuplicatedKeyword(ALLOWED_DUPLICATED_KEYWORD_SEQ)
    if (duplicatedKeyword.size > 0)
      DisconfWarn(
        ctxt
        , s"Structure: ${ctxt.structureID} has duplicated keywords:${duplicatedKeyword.mkString("'", ",", "'")}"
        , SectionName.SECTION_NONE)

    Some(recordMap)
  }
  //---------------------------------------------------------------------------
  def readAllDataBlock(stream: MyInputStream
                       , dataBlockByteSize: Long
                       , ctxt: FitsCtxt
                       , readDataBlockFlag: Boolean
                       , isExtensionTableAscii: Boolean = false): Option[Array[Block]] = {
    //--------------------------------------------------------------------------
    def checkRemainByteSeq(block: Block, usedDataLastBlockByteSize: Int, expetedByte:Byte) = {
      if (usedDataLastBlockByteSize == 0) true
      else block.byteSeq.drop(usedDataLastBlockByteSize).forall( b=> b == expetedByte )
    }
    //--------------------------------------------------------------------------
    val blockCount = getBlockCount(dataBlockByteSize)
    var pos = getBlockByteSize(getBlockCount(stream.getReadPosition))
    val blockSeq = {
      (for (_ <- 0L until blockCount) yield {
        if(!readDataBlockFlag) {
          val block = Block(pos) //empty block
          pos += BLOCK_BYTE_SIZE
          if (!stream.skip(BLOCK_BYTE_SIZE)) {
            DisconfFatal(
              ctxt
              , s"Structure: ${ctxt.structureID} error skipping $BLOCK_BYTE_SIZE . Current read position: ${stream.getReadPosition}"
              , SectionName.SECTION_NONE)
            return None
          }
          block
        }
        else Block.readFromStream(stream,ctxt).getOrElse(return None)
      }).toArray
    }

    if (blockCount == 0) return Some(Array())

    //check last block remain byteSeq is all 0
    val usedDataLastBlockByteSize = getLastBlockUsedByteSize(dataBlockByteSize).toInt

    isExtensionTableAscii match  {
      case false  =>
        if (!checkRemainByteSeq(blockSeq.last, usedDataLastBlockByteSize, BLOCK_BYTE_DATA_PADDING_BYTE)) {
          DisconfWarn(
            ctxt
            , s"Structure: ${ctxt.structureID} the stream has non zero bytes after last valid byte in the last block"
            , SectionName.SECTION_3_3_2)
        }
      case true  =>
        if (!checkRemainByteSeq(blockSeq.last, usedDataLastBlockByteSize, BLOCK_ASCII_DATA_PADDING_BYTE)) {
          DisconfFatal(
            ctxt
            , s"Structure: ${ctxt.structureID} the stream has non spaces after last valid byte in the last block"
            , SectionName.SECTION_7_2_3)
            return None
        }
    }
    Some(blockSeq)
  }
  //---------------------------------------------------------------------------
  def splitInBlockSeq(byteSeq: Array[Byte]
                      , startPos: Long = 0L
                      , paddingByte: Byte = BLOCK_BYTE_DATA_PADDING_BYTE) = {
    val lastBlockPaddingByteSize = getLastBlockPaddingByteSize(byteSeq.length)
    val lastBlockPaddingByteSeq =
      if (lastBlockPaddingByteSize != 0)
        Array.fill(lastBlockPaddingByteSize.toInt)(paddingByte)
      else Array[Byte]()
    (byteSeq ++ lastBlockPaddingByteSeq).grouped(BLOCK_BYTE_SIZE).zipWithIndex.map { case (bSeq, i) =>
      Block(startPos + (i * BLOCK_BYTE_SIZE), bSeq)
    }.toArray
  }
  //---------------------------------------------------------------------------
  def collectAllByteSeq(dataBlockSeq: Array[Block]) = (dataBlockSeq map (_.byteSeq)).flatten
  //---------------------------------------------------------------------------
  def build(byteCount: Long //incuded the 'byteSeq' parameter size
            , dataByteSeq:Array[Byte] = Array()
            , paddingByte: Byte = BLOCK_BYTE_DATA_PADDING_BYTE): Array[Block] = {
    if (byteCount == 0) return Array(Block(0))
    val mainBlockSeq = splitInBlockSeq(dataByteSeq, paddingByte = paddingByte)
    val remainBlockCount = Block.getBlockCount(byteCount) - mainBlockSeq.size
    var pos = mainBlockSeq.size * BLOCK_BYTE_SIZE
    val remainBlock = (for (_ <- 0L until remainBlockCount) yield {
      val block = Block(pos, Array.fill(BLOCK_BYTE_SIZE)(0))
      pos += BLOCK_BYTE_SIZE
      block
    }).toArray
    mainBlockSeq ++ remainBlock
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Block(starPos: Long   //included
                 , byteSeq:Array[Byte]) {
  //---------------------------------------------------------------------------
  val endPos = starPos + BLOCK_BYTE_SIZE - 1  //included
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Block.scala
//=============================================================================
