/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  11/Sep/2023
  * Time:  19h:22m
  * Description: None
  */
package com.common.fits.standard.block.dataIntegrity
//=============================================================================
import com.common.fits.standard.Keyword.{KEYWORD_CHECKSUM, KEYWORD_DATASUM}
import com.common.fits.standard.block.Block
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.dataType.Conversion
import com.common.fits.standard.disconformity.{DisconfFatal, DisconfWarn}
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.sectionName.SectionName
//=============================================================================
import java.nio.{ByteBuffer, ByteOrder}
import java.nio.charset.StandardCharsets
import java.util
//=============================================================================
//=============================================================================
object DataIntegrity {
  //---------------------------------------------------------------------------
  // Base source code "http://arxiv.org/abs/1201.1345"
  // implemented in 'nom.tam.fits.utilities.FitsChecksum'
  // at repository: https://github.com/nom-tam-fits/nom-tam-fits
  //---------------------------------------------------------------------------
  val DATASUM_DEFAULT_VALUE = 0L
  val CHECKSUM_DEFAULT_VALUE = "-0"
  //---------------------------------------------------------------------------
  private val CHECKSUM_BLOCK_SIZE = 4
  private val CHECKSUM_BLOCK_MASK = CHECKSUM_BLOCK_SIZE - 1
  private val CHECKSUM_STRING_SIZE = 16
  private val SHIFT_2_BYTES = 16
  private val MASK_2_BYTES = 0xffff
  private val MASK_4_BYTES = 0xffffffff
  private val MASK_BYTE = 0xff
  private val ASCII_ZERO = '0'
  private val SELECT_BYTE = Array(24, 16, 8, 0)
  private val EXCLUDE = ":;<=>?@[\\]^_`"
  private val INTEGER_MASK = 0x00000000ffffffffL
  //---------------------------------------------------------------------------
  private val CHAR_ZERO = '0'.toByte
  //---------------------------------------------------------------------------
  case class PartialDataSum() {
    //-------------------------------------------------------------------------
    private var dataSum:Long = 0
    //-------------------------------------------------------------------------
    def getDataSum() = dataSum
    //-------------------------------------------------------------------------
    def update(byteSeq: Array[Byte]) : Unit =
      dataSum = calculateDataSum(byteSeq, dataSum)
    //-------------------------------------------------------------------------
    def updateRecordCheckSum(byteSeq: Array[Byte]) = {
      //same record but assuming 0 in the value of the checksum, keeping the possible comment with te timestamp.
      // See FITS standard appendix J1
      val updatedByteSeq = byteSeq.zipWithIndex.map { case (byte, i) =>
        if ((i >= 11) && (i <= 26)) CHAR_ZERO
        else byte
      }
      update(updatedByteSeq)
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private def addTwoPartialDataSums(h: Long, l: Long): Long = {
    var hi = h & MASK_4_BYTES
    var lo = l & MASK_4_BYTES

    while (true) {
      val hiCarry = hi >>> SHIFT_2_BYTES
      val loCarry = lo >>> SHIFT_2_BYTES
      if ((hiCarry | loCarry) == 0) return (hi << SHIFT_2_BYTES) | lo
      hi = (hi & MASK_2_BYTES) + loCarry
      lo = (lo & MASK_2_BYTES) + hiCarry
    }
    (hi << SHIFT_2_BYTES) | lo
  }
  //---------------------------------------------------------------------------
  private def encode(checksum: Long
                     , applyOneComplement: Boolean = true): String = {
    var modifiedChecksum = checksum

    if (applyOneComplement) modifiedChecksum = ~checksum & INTEGER_MASK

    val asc = new Array[Byte](CHECKSUM_STRING_SIZE)
    val ch = new Array[Byte](CHECKSUM_BLOCK_SIZE)
    val sum = modifiedChecksum.toInt

    for (i <- 0 until CHECKSUM_BLOCK_SIZE) {
      // each byte becomes four
      val byt = (sum >>> SELECT_BYTE(i)) & MASK_BYTE

      util.Arrays.fill(ch, ((byt >>> 2) + ASCII_ZERO).toByte) // quotient
      ch(0) = (ch(0) + (byt & CHECKSUM_BLOCK_MASK)).toByte // remainder

      for (j <- 0 until CHECKSUM_BLOCK_SIZE by 2) {
        while (EXCLUDE.indexOf(ch(j)) >= 0 || EXCLUDE.indexOf(ch(j + 1)) >= 0) {
          ch(j) = (ch(j) + 1).toByte
          ch(j + 1) = (ch(j + 1) - 1).toByte
        }
      }

      for (j <- 0 until CHECKSUM_BLOCK_SIZE) {
        var k = CHECKSUM_BLOCK_SIZE * j + i
        k = if (k == CHECKSUM_STRING_SIZE - 1) 0 else k + 1 // rotate right
        asc(k) = ch(j)
      }
    }
    new String(asc, StandardCharsets.US_ASCII)
  }
  //---------------------------------------------------------------------------
  private def decode(encoded: String
                     , ctxt: FitsCtxt
                     , applyOneComplement: Boolean = true): Option[Long] = {
    val bytes = encoded.getBytes(StandardCharsets.US_ASCII)
    if (bytes.length != CHECKSUM_STRING_SIZE) {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_4_4_2_7
        , s"Structure: ${ctxt.structureID} keyword '$KEYWORD_CHECKSUM' with value '$encoded' must have $CHECKSUM_STRING_SIZE char size")
      return None
    }

    // Rotate the bytes one to the left
    val tmp = bytes(0)
    System.arraycopy(bytes, 1, bytes, 0, CHECKSUM_STRING_SIZE - 1)
    bytes(CHECKSUM_STRING_SIZE - 1) = tmp

    for (i <- 0 until CHECKSUM_STRING_SIZE) {
      if (bytes(i) < ASCII_ZERO) {
        DisconfFatal(
          ctxt
          , SectionName.SECTION_4_4_2_7
          , s"Structure: ${ctxt.structureID} keyword '$KEYWORD_CHECKSUM' with value '$encoded' has an illegal character at position: $i (ASCII below 0x30)")
        return None
      }
      bytes(i) = (bytes(i) - ASCII_ZERO).toByte
    }

    val bb = ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN)
    val sum = bb.getInt() + bb.getInt() + bb.getInt() + bb.getInt()
    val result = if (applyOneComplement) ~sum else sum
    Some(result & INTEGER_MASK)
  }
  //---------------------------------------------------------------------------
  def calculateDataSum(valueSeq: Array[Long]
                       , previousDataSum: Long): Long = {
    //-------------------------------------------------------------------------
    var h: Long = (previousDataSum >>> SHIFT_2_BYTES) & MASK_2_BYTES
    var l: Long = previousDataSum & MASK_2_BYTES
    //-------------------------------------------------------------------------
    valueSeq.foreach { value =>
      h += value >>> SHIFT_2_BYTES
      l += value & MASK_2_BYTES
    }
    addTwoPartialDataSums(h,l)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def calculateDataSum(byteSeq: Array[Byte]
                        , previousDataSum: Long): Long =
    calculateDataSum(Conversion.byteSeqToLongSeq(byteSeq, byteOrder = ByteOrder.BIG_ENDIAN)
                    , previousDataSum)
  //---------------------------------------------------------------------------
  private def calculateDataSum(blockSeq: Array[Block]
                               , _previousDataSum: Long): Long = {
    var partialDataSum = _previousDataSum
    blockSeq.foreach { block => partialDataSum = calculateDataSum(block.byteSeq, partialDataSum) }
    partialDataSum
  }
  //---------------------------------------------------------------------------
  private def checkHduDataDataSum(storedHduDataDataSum: Long
                                 , hduDataBlockDataSum: Long
                                 , ctxt: FitsCtxt): Boolean = {
    if (storedHduDataDataSum == DATASUM_DEFAULT_VALUE) {
      DisconfWarn(
        ctxt
        , SectionName.SECTION_4_4_2_7
        , s"Structure: ${ctxt.structureID} has the default '$KEYWORD_DATASUM' value")
      return true
    }

    if (hduDataBlockDataSum != storedHduDataDataSum) {
      DisconfWarn(
        ctxt
        , SectionName.SECTION_4_4_2_7
        , s"Structure: ${ctxt.structureID} keyword '$KEYWORD_DATASUM' mistmatch. Stored '$storedHduDataDataSum' but calculated: '$hduDataBlockDataSum'")
    }
    true
  }

  //---------------------------------------------------------------------------
  //checksum for a HDU is composed by the data sum of header
  // (setting the value of CHECKSUM to ’0000000000000000’,as is exposed in Appendix J.5)
  // plus the data sum of the data blocks
  //The data sum of the header is calculated when thr header is being processed
  private def checkHduCheckSum(storedHduCheckSum: String
                              , hduHeaderDataSum: Long
                              , hduDataBlockDataSum: Long
                              , ctxt: FitsCtxt): Boolean = {
    if (storedHduCheckSum == CHECKSUM_DEFAULT_VALUE) {
      DisconfWarn(
        ctxt
        , SectionName.SECTION_4_4_2_7
        , s"Structure: ${ctxt.structureID} has the default '$KEYWORD_CHECKSUM' value")
      return true
    }

    //add the data sum of header and the data sum of the data block
    val calculatedHduDataSum = calculateDataSum(Array(hduHeaderDataSum,hduDataBlockDataSum), previousDataSum = 0)
    val calculatedHduCheckSum = encode(calculatedHduDataSum)
    if (calculatedHduCheckSum != storedHduCheckSum) {
      DisconfWarn(
        ctxt
        , SectionName.SECTION_4_4_2_7
        , s"Structure: ${ctxt.structureID} keyword '$KEYWORD_CHECKSUM' mistmatch. Stored '$storedHduCheckSum' but calculated: '$calculatedHduCheckSum'")
      return false
    }
    true
  }
  //---------------------------------------------------------------------------
  def check(recordMap: RecordMap
            , blockSeq: Array[Block]
            , hduHeaderDataSum: Long
            , ctxt: FitsCtxt): Boolean = {

    val hduDataBlockDataSum = calculateDataSum(blockSeq,0)

    //not generated an error in case of mismatch with data sum, just a warning
    if (recordMap.contains(KEYWORD_DATASUM)) {
       checkHduDataDataSum(storedHduDataDataSum = recordMap.getFirstValue[String](KEYWORD_DATASUM).get.trim.toLong
                           , hduDataBlockDataSum
                          , ctxt)
    }

    if (recordMap.contains(KEYWORD_CHECKSUM)) {
       checkHduCheckSum(storedHduCheckSum = recordMap.getFirstValue[String](KEYWORD_CHECKSUM).get
        , hduHeaderDataSum
        , hduDataBlockDataSum
        , ctxt)
    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file DataIntegrity.scala
//=============================================================================