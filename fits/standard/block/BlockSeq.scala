/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Nov/2022
 * Time:  12h:18m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.block
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
//'Block' data type does not store the data continusly but sometimes is required
// to manage large byte data array grather than  2,147,483,647 bytes (Int data type limit)
//In that case, use this data type
//In other case, just use "Block.collectAllByteSeq"
case class BlockSeq(seq:Array[Block]) {
  //---------------------------------------------------------------------------
  private var readPos = 0L
  private val maxReadPos = (seq map ( _.byteSeq.length.toLong )).sum - 1
  //---------------------------------------------------------------------------
  private def getBlock(byteSize: Long) = {
    val blockIndex = Block.getBlockCount(byteSize).toInt
    seq(blockIndex)
  }
  //---------------------------------------------------------------------------
  private def readFromBlock(block: Block, byteSize: Long, result: ArrayBuffer[Byte]) = {
    val slice = block.byteSeq
      .drop((readPos - block.starPos).toInt)
      .take(byteSize.toInt)
    result.append(slice:_*)
    readPos += byteSize
  }
  //---------------------------------------------------------------------------
  def getByteSeq(byteSize: Long): Option[Array[Byte]]  = {
    if ((readPos + byteSize) > maxReadPos) return None
    var remainByteSeq = byteSize
    val result = ArrayBuffer[Byte]()

    while(remainByteSeq > 0) {
      val startBlock = getBlock(readPos)
      val startBlockRemainByteSize = startBlock.endPos - readPos
      if (startBlockRemainByteSize > remainByteSeq) { //Simple case all bytes in the same block
        readFromBlock(startBlock, remainByteSeq, result)
        remainByteSeq = 0
      }
      else {
        readFromBlock(startBlock, startBlockRemainByteSize, result)  //read as much as possible from first block
        remainByteSeq -= startBlockRemainByteSize
      }
    }
    None
  }
  //---------------------------------------------------------------------------
  def resetReadPos() = readPos = 0
  //---------------------------------------------------------------------------
  def collectAllByteSeq() = (seq map (_.byteSeq)).flatten
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file BlockSeq.scala
//=============================================================================
