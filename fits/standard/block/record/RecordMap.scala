//=============================================================================
package com.common.fits.standard.block.record
//=============================================================================
import com.common.fits.standard.ItemSize.KEYWORD_CHAR_SIZE
import com.common.fits.standard.block.record.Record.RecordSeq
import com.common.fits.standard.block.record.RecordNumber.{RecordFloat, RecordInteger}
import com.common.fits.standard.Keyword._
import com.common.fits.standard.block.record.verifier.section_8.section_8_2.Section_8_3
import com.common.fits.standard.dataType.DataType
import com.common.fits.standard.structure.hdu.extension.Extension.{KEYWORD_EXTLEVEL_DEFAULT_VALUE, KEYWORD_EXTVER_DEFAULT_VALUE}
//=============================================================================
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.language.existentials
import scala.reflect.{ClassTag}
//=============================================================================
object RecordMap {
  //---------------------------------------------------------------------------
  //default values
  final val BSCALE_DEFAULT_VALUE   = 1d
  final val BZERO_DEFAULT_VALUE    = 0d
  private final val PC_DEFAULT_VALUE_ONE   = 1d
  private final val PC_DEFAULT_VALUE_ZERO  = 0d
  private final val CDELT_DEFAULT_VALUE    = 1d
  private final val CD_DEFAULT_VALUE       = 0d
  private final val CROTA_DEFAULT_VALUE    = 0d
  private final val CTYPE_DEFAULT_VALUE    = ""
  private final val CUNIT_DEFAULT_VALUE    = ""
  private final val CRPIX_DEFAULT_VALUE    = 0d
  private final val CRVAL_DEFAULT_VALUE    = 0d
  private final val CRDER_DEFAULT_VALUE    = 0D
  private final val CSYER_DEFAULT_VALUE    = 0D
  private final val CNAME_DEFAULT_VALUE    = ""
  private final val SSYSOBS_DEFAULT_VALUE  = "TOPOCENT"
  private final val TIMESYS_DEFAULT_VALUE  = "UTC"
  private final val MJDREF_DEFAULT_VALUE   = 0D
  private final val MJDREFI_DEFAULT_VALUE  = 0L
  private final val TREFPOS_DEFAULT_VALUE  = "TOPOCENTER"
  private final val PLEPHEM_DEFAULT_VALUE  = "DE405"
  private final val TIMEUNIT_DEFAULT_VALUE = "s"
  private final val TIMEOFFS_DEFAULT_VALUE = "0d"
  private final val TIMSYER_DEFAULT_VALUE  = 0d
  private final val TIMRDER_DEFAULT_VALUE  = 0d
  private final val ZTILE_DEFAULT_VALUE    = 0d
  private final val ZQUANTIZ_DEFAULT_VALUE  = "NO DITHER"
  //---------------------------------------------------------------------------
  def apply(recordSeq: RecordSeq): RecordMap = {
    val recordMap = RecordMap()
    recordMap.appendSeq(recordSeq)
    recordMap
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import RecordMap._
case class RecordMap()  {
  //---------------------------------------------------------------------------
  private val map = mutable.LinkedHashMap[String,ArrayBuffer[Record[_]]] ()  //(keyword,record)
  private var lastID = -1L
  //---------------------------------------------------------------------------
  def getNewID = {
    lastID += 1
    lastID
  }
  //---------------------------------------------------------------------------
  def setLastID(li: Long) = lastID = li
  //---------------------------------------------------------------------------
  def apply(keyword: String) = map(keyword)
  //---------------------------------------------------------------------------
  def contains(keyword: String) = map.contains(keyword)
  //---------------------------------------------------------------------------
  def get(keyword: String) = map.get(keyword)
  //---------------------------------------------------------------------------
  def clear() = map.clear()
  //---------------------------------------------------------------------------
  def values = map.values
  //---------------------------------------------------------------------------
  def size = map.size
  //---------------------------------------------------------------------------
  def isEmpty = map.isEmpty
  //---------------------------------------------------------------------------
  def myClone(): RecordMap = {
    val newRecordMap = RecordMap()
    newRecordMap.appendSeq(values.toArray.flatten)
    newRecordMap.setLastID(lastID)
    newRecordMap
  }
  //---------------------------------------------------------------------------
  def getRecordCountWithUniqueKeyword = map.size
  //---------------------------------------------------------------------------
  def getRecordCount = map.map{case (_,arr)=> arr.size }.sum
  //---------------------------------------------------------------------------
  def append(recordSeq: Record[_], offfsetID:Long = 0) : Unit =
    appendSeq(Array(recordSeq),offfsetID)
  //---------------------------------------------------------------------------
  def appendMap(recordMap: RecordMap, offfsetID:Long = 0): Unit =
    appendSeq(recordMap.values.toArray.flatten, offfsetID)
  //---------------------------------------------------------------------------
  def appendSeq(seq: RecordSeq, offfsetID:Long = 0)  : Unit = {
    seq.foreach { r =>
      val keyword = r.keyword
      if (!map.contains(keyword)) map(keyword) = ArrayBuffer[Record[_]]()
      map(keyword) += Record.cloneWithNewID(r, lastID + 1 + offfsetID)
      getNewID
    }
  }
  //---------------------------------------------------------------------------
  def updateValue[T](keyword: String, value: T): Unit = {
    if (map.contains(keyword)) {
      if (value.isInstanceOf[Double]) { //fore a float register
        val r =  map(keyword).head
        map(keyword) = ArrayBuffer(RecordFloat(keyword, value.asInstanceOf[Double], r.comment, r.id))
      }
      else map(keyword) = map(keyword).map{ Record.cloneWithNewValue( _ , value) }
    }
  }

  //---------------------------------------------------------------------------
  def appendOrUpdate(keyword: String, record: Record[_]): Unit =
    if (!map.contains(keyword)) map(keyword) = ArrayBuffer[Record[_]](record)
    else map(keyword) = ArrayBuffer(record)
  //---------------------------------------------------------------------------
  def insertRecordAfterPos(record: Record[_], pos: Int): Unit =
    insertRecordSeqAfterPos(Array(record),pos)
  //---------------------------------------------------------------------------
  //'recordSeq' will be placed at 'pos' starting at index 0
  def insertRecordSeqAfterPos(recordSeq: RecordSeq, pos: Int): Unit = {
    val seq = getPlainRecordSeq
    map.clear()
    lastID = -1
    seq.take(pos).foreach {  r => append(r); lastID = r.id + 1 }
    recordSeq.foreach { r => append(r) }
    val offset = lastID
    seq.drop(pos).foreach { r => append(r, offset); lastID = r.id + 1 }
  }
  //---------------------------------------------------------------------------
  def remove(keyword: String): Unit =
    if (map.contains(keyword)) map.remove(keyword)
  //---------------------------------------------------------------------------
  def remove(keywordSeq: Seq[String]): Unit =
    keywordSeq.foreach(map.remove(_))
  //---------------------------------------------------------------------------
  def getPlainRecordSeq = values.flatten.toArray.sortWith(_.id < _.id)
  //---------------------------------------------------------------------------
  def getFirstRecord(): Option[Record[_]] = {
    val recordSimple_1 = getFirstRecord(KEYWORD_SIMPLE)  //affine_transformation case
    if (recordSimple_1.isDefined && recordSimple_1.get.id == 0) return Some(recordSimple_1.get)

    val recordSimple_2 = getFirstRecord(KEYWORD_XTENSION) //affine_transformation case 2
    if (recordSimple_2.isDefined && recordSimple_2.get.id == 0) return Some(recordSimple_2.get)

    map.valuesIterator.foreach { record =>
      record.foreach { r => if (r.id == 0) return Some(r) }}
    None
  }
  //---------------------------------------------------------------------------
  def getRecordSeq(keyword: String): Array[Record[_]] = {
    if (map.contains(keyword)) map(keyword).toArray
    else Array()
  }
  //---------------------------------------------------------------------------
  def getFirstRecord(keyword: String) = {
    if (map.contains(keyword)) Some(map(keyword).head)
    else None
  }
  //---------------------------------------------------------------------------
  def getFirstValue[T](keyword: String) = {
    if (map.contains(keyword)) Some(map(keyword).head.value.asInstanceOf[T])
    else None
  }
  //---------------------------------------------------------------------------
  def getFirstValueOrDefault[T](keyword: String, default: T) = {
    if (map.contains(keyword)) Some(map(keyword).head.value.asInstanceOf[T])
    else Some(default)
  }
  //---------------------------------------------------------------------------
  def getFirstValueOrDefaultFloat(keyword: String, default: Double) = {
    if (map.contains(keyword)) Some(map(keyword).head.value.asInstanceOf[RecordFloat].value)
    else Some(default)
  }
  //---------------------------------------------------------------------------
  def getAllKeywordWithPrefix(prefix:String) =
    map.keys.filter( keyword => keyword.startsWith(prefix) ).toArray
  //---------------------------------------------------------------------------
  def getAllRecordSeq(keyword:String): Seq[Record[_]] = {
    if (!map.contains(keyword)) Seq()
    else map(keyword).toSeq
  }
  //---------------------------------------------------------------------------
  def getAllStringValueSeq(keyword: String): Seq[String] =
    getAllRecordSeq(keyword) map (_.value.toString())
  //---------------------------------------------------------------------------
  def getAllKeywordWithPrefixAndSuffix(prefix: String, suffix: String) = {
    if (suffix.isEmpty) getAllKeywordWithPrefix(prefix)
    else {
      map.keys.filter{keyword =>
        keyword.startsWith(prefix) &&
        keyword.endsWith(suffix)
      }.toArray.sortWith(_ < _)
    }
  }
  //---------------------------------------------------------------------------
  def getAllRecordWithPrefix(prefix: String) =
    (getAllKeywordWithPrefix(prefix) map (map( _ ))).flatten
  //---------------------------------------------------------------------------
  def getAllRecordWithPrefixAndSuffix(prefix: String, suffix:String) =
    (getAllKeywordWithPrefixAndSuffix(prefix, suffix) map (map( _ ))).flatten
  //---------------------------------------------------------------------------
  def getFirstPosition(keyword:String) = {
    if (!map.contains(keyword)) 0L
    else map(keyword).head.id
  }
  //---------------------------------------------------------------------------
   def getDuplicatedKeyword(allowedKeywordSeq: Seq[String]) = map.flatMap {
    case (key, arr) =>
      if (arr.size > 1 &&
         ! allowedKeywordSeq.contains(key)) Some(key) else None
  }
  //---------------------------------------------------------------------------
  def getDataType() = DataType.build(getBitPix().toString)
  //---------------------------------------------------------------------------
  def getNaxis() = getFirstValue[Long](KEYWORD_NAXIS).get

  //---------------------------------------------------------------------------
  def getZNaxis() = getFirstValue[Long](KEYWORD_ZNAXIS).get
  //---------------------------------------------------------------------------
  def getAxisSeq(firstAxis: Long = 1L) = {
    val naxisCount = getNaxis()
    (for (i <- firstAxis to naxisCount) yield
      getFirstValue[Long](KEYWORD_NAXIS + i).get).toArray
  }
  //---------------------------------------------------------------------------
  def getAxisSeqName() = {
    val naxisCount = getNaxis()
    (for (i <- 1L to naxisCount) yield KEYWORD_NAXIS + i).toArray
  }
  //---------------------------------------------------------------------------
  def getBitPix() = getFirstValue[Long](KEYWORD_BITPIX).get
  //---------------------------------------------------------------------------
  def getBytePerPix() = Math.abs(getBitPix()) / 8
  //---------------------------------------------------------------------------
  def getAxisTotalItemCount() = {
    val naxisSeq = getAxisSeq()
    if (naxisSeq.isEmpty) 0L
    else naxisSeq.product
  }
  //---------------------------------------------------------------------------
  def getAxisTotalByteSize() =
    getAxisTotalItemCount * getBytePerPix()
  //---------------------------------------------------------------------------
   def removeNaxisDefinition () =
     getAxisSeqName().foreach( remove( _ ) )
  //---------------------------------------------------------------------------
  def getExtensionName() =
    getFirstValueOrDefault[String](KEYWORD_XTENSION, "").get
  //---------------------------------------------------------------------------
  def getPcount() =
    getFirstValue[Long](KEYWORD_PCOUNT).get
  //---------------------------------------------------------------------------
  def getGcount() =
    getFirstValue[Long](KEYWORD_GCOUNT).get
  //---------------------------------------------------------------------------
  def getExtName() =
    getFirstValueOrDefault[String](KEYWORD_EXTNAME,"").get
  //---------------------------------------------------------------------------
  def getExtVer() = {
    getFirstValueOrDefault[Long](KEYWORD_EXTVER,KEYWORD_EXTVER_DEFAULT_VALUE).get
  }
  //---------------------------------------------------------------------------
  def getExtLevel() =
    getFirstValueOrDefault[Long](KEYWORD_EXTLEVEL,KEYWORD_EXTLEVEL_DEFAULT_VALUE).get
  //---------------------------------------------------------------------------
  def getTfields() =
    getFirstValue[Long](KEYWORD_TFIELDS).get
  //---------------------------------------------------------------------------
  def getTableFieldName(keywordTemplate: String) =  {
    val fieldCount = getTfields()
    (for (i <- 1L to fieldCount) yield keywordTemplate + i).toArray
  }
  //---------------------------------------------------------------------------
  def getOptTableFieldValue[T: ClassTag](keyword: String) : Option[T] =
    if (contains(keyword)) Some(getFirstValue[T](keyword).get)
    else None
  //---------------------------------------------------------------------------
  def getTbcolSeqName() = getTableFieldName(KEYWORD_TBCOL)
  //---------------------------------------------------------------------------
  def getTformSeqName() = getTableFieldName(KEYWORD_TFORM)
  //---------------------------------------------------------------------------
  //default values
  //---------------------------------------------------------------------------
  def getBscale() = {
    if (!map.contains(KEYWORD_BSCALE)) BSCALE_DEFAULT_VALUE
    else
      map(KEYWORD_BSCALE).head match {
        case x: RecordInteger => x.value.toDouble
        case x: RecordFloat => x.value
      }
  }

  //---------------------------------------------------------------------------
  def getBzero() = {
    if (!map.contains(KEYWORD_BZERO)) BZERO_DEFAULT_VALUE
    else
      map(KEYWORD_BZERO).head match {
        case x: RecordInteger => x.value.toDouble
        case x: RecordFloat => x.value
      }
  }
  //---------------------------------------------------------------------------
  def getWcsAxes(wcsSuffix: String = "") =
    getFirstValueOrDefault[Long](KEYWORD_WCSAXES+wcsSuffix, getNaxis()).get

  //---------------------------------------------------------------------------
  def getPC(i:Long,j:Long,wcsSuffix: String = "") = {
    val default = if (i == j) PC_DEFAULT_VALUE_ONE else PC_DEFAULT_VALUE_ZERO
    getFirstValueOrDefaultFloat(s"$KEYWORD_PC${i}_$j$wcsSuffix", default)
  }
  //---------------------------------------------------------------------------
  def getCDELT(i: Long,wcsSuffix: String = "") =
    getFirstValueOrDefaultFloat(s"$KEYWORD_PC$i$wcsSuffix", CDELT_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getCD(i: Long, j: Long,wcsSuffix: String = "") =
    getFirstValueOrDefaultFloat(s"$KEYWORD_PC${i}_$j$wcsSuffix", CD_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getROTA(i: Long,wcsSuffix: String = "") =
    getFirstValueOrDefaultFloat(s"$KEYWORD_PC$i$wcsSuffix", CROTA_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getCTYPE(i: Long,wcsSuffix: String = "") =
    getFirstValueOrDefault(s"$KEYWORD_CTYPE$i$wcsSuffix", CTYPE_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getCUNIT(i: Long,wcsSuffix: String = "") =
    getFirstValueOrDefault(s"$KEYWORD_CUNIT$i$wcsSuffix", CUNIT_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getCRPIX(i: Long,wcsSuffix: String = "") =
    getFirstValueOrDefault(s"$KEYWORD_CRPIX$i$wcsSuffix", CRPIX_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getCRVAL(i: Long,wcsSuffix: String = "") =
    getFirstValueOrDefault(s"$KEYWORD_CRVAL$i$wcsSuffix", CRVAL_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getCRDER(i: Long,wcsSuffix: String = "") =
    getFirstValueOrDefault(s"$KEYWORD_CRDER$i$wcsSuffix", CRDER_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getCSYER(i: Long,wcsSuffix: String = "") =
    getFirstValueOrDefault(s"$KEYWORD_CSYER$i$wcsSuffix", CSYER_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getRADESYS(wcsSuffix: String = ""): String =
    Section_8_3.getDefaultRADESYS(this,wcsSuffix)
  //---------------------------------------------------------------------------
  def getLONPOLE(wcsSuffix: String = ""): Option[Double] =
    Section_8_3.getDefaultLONPOLE(this, wcsSuffix)
  //---------------------------------------------------------------------------
  def getLATPOLE(wcsSuffix: String = ""): Option[Double] =
    Section_8_3.getDefaultLATPOLE(this, wcsSuffix)
  //---------------------------------------------------------------------------
  def getCNAME(i: Long, wcsSuffix: String = "") =
    getFirstValueOrDefault(s"$KEYWORD_CNAME$i$wcsSuffix", CNAME_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getSSYSOBS(wcsSuffix: String = "") =
    getFirstValueOrDefault(s"$KEYWORD_SSYSOBS$wcsSuffix", SSYSOBS_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getTIMESYS() =
    getFirstValueOrDefault(s"$KEYWORD_TIMESYS", TIMESYS_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getMJDREF() =
    getFirstValueOrDefault(s"$KEYWORD_MJDREF", MJDREF_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getMJDREFI() =
    getFirstValueOrDefault(s"$KEYWORD_MJDREFI", MJDREFI_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getTREFPOS() =
    getFirstValueOrDefault(s"$KEYWORD_TREFPOS", TREFPOS_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getPLEPHEM() =
    getFirstValueOrDefault(s"$KEYWORD_PLEPHEM", PLEPHEM_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getTIMEUNIT() =
    getFirstValueOrDefault(s"$KEYWORD_TIMEUNIT", TIMEUNIT_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getTIMEOFFS() =
    getFirstValueOrDefault(s"$KEYWORD_TIMEOFFS", TIMEOFFS_DEFAULT_VALUE)

  //---------------------------------------------------------------------------
  def getTIMSYER() =
    getFirstValueOrDefault(s"$KEYWORD_TIMSYER", TIMSYER_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getTIMRDER() =
    getFirstValueOrDefault(s"$KEYWORD_TIMRDER", TIMRDER_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def getZTILE(i: Long): Long = {
    val keyword = s"$KEYWORD_ZTILE$i"
    if (contains(keyword)) getFirstValue[Long](keyword).get
    else {
      if (i != 1) 1L
      else getFirstValueOrDefault(s"$KEYWORD_ZTILE$i", getFirstValue[Long](s"$KEYWORD_ZNAXIS$i").get).get
    }
  }
  //---------------------------------------------------------------------------
  def getZQUANTIZ() =
    getFirstValueOrDefault(s"$KEYWORD_ZQUANTIZ", ZQUANTIZ_DEFAULT_VALUE)
  //---------------------------------------------------------------------------
  def toPlainMap() = {
    map.map { case (key, valueSeq) =>
      val s = valueSeq.head.toString()
      val key = s.take(KEYWORD_CHAR_SIZE)
      val value = s.drop(KEYWORD_CHAR_SIZE)
      (key,value)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RecordMap.scala
//=============================================================================
