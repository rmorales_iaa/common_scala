/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  29/Sep/2022
 * Time:  11h:21m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.block.record
//=============================================================================
import com.common.fits.standard.ItemSize.KEYWORD_CHAR_SIZE
import com.common.fits.standard.Keyword.{KEYWORD_COMMENT, KEYWORD_END, KEYWORD_HISTORY}
//=============================================================================
//=============================================================================
object RecordCharacter {
  //---------------------------------------------------------------------------
  final val RECORD_STRING_FLAG_LONG_COMMENT_START    = 1
  final val RECORD_STRING_FLAG_LONG_COMMENT_CONTINUE = 2
  final val RECORD_STRING_FLAG_LONG_COMMENT_END      = 3
  //---------------------------------------------------------------------------
  case class RecordString(keyword: String
                        , value: String
                        , comment: String = ""
                        , id: Long = 0
                        , flag: Byte = 0) extends Record[String]
  //--------------------------------------------------------------------------
  //section '4.4.2.4 Commentary words'
  //used for aesthetic purposes to provide a break between groups of related keywords in the header
  case class RecordBlank(value: String, id: Long = 0) extends Record[String] {
    //------------------------------------------------------------------------
    val keyword = " " * KEYWORD_CHAR_SIZE
    val comment = ""
    //------------------------------------------------------------------------
    override def getFormatted() = keyword + value
    //------------------------------------------------------------------------
    def allCharAreBlank() = value.forall(_ == ' ')
    //------------------------------------------------------------------------
  }
  //--------------------------------------------------------------------------
  case class RecordComment(value: String, id: Long = 0) extends Record[String] {
    //------------------------------------------------------------------------
    val keyword = KEYWORD_COMMENT
    val comment = ""
    //------------------------------------------------------------------------
  }
  //--------------------------------------------------------------------------
  case class RecordGeneralComment(keyword:String, value: String, id: Long = 0) extends Record[String] {
    //------------------------------------------------------------------------
    val comment = ""
    //------------------------------------------------------------------------
  }
  //--------------------------------------------------------------------------
  case class RecordHistory(value: String, id: Long = 0) extends Record[String] {
    //------------------------------------------------------------------------
    val keyword = KEYWORD_HISTORY
    val comment: String = ""
    //------------------------------------------------------------------------
  }
  //--------------------------------------------------------------------------
  case class RecordEnd(id: Long = 0) extends Record[String] {
    //------------------------------------------------------------------------
    val keyword = KEYWORD_END
    val value = ""
    val comment: String = ""
    //------------------------------------------------------------------------
  }
  //--------------------------------------------------------------------------
  case class RecordUnknownKeywordValue(value: String, comment: String = "", id: Long = 0) extends Record[String] {
    //------------------------------------------------------------------------
    val keyword = ""
    //------------------------------------------------------------------------
    override def getFormatted() = s"$value '$comment'"
    //------------------------------------------------------------------------
  }
  //--------------------------------------------------------------------------
  //not on the standard. Used for manage errors parsing record
  case class RecordError(keyword: String
                         , value: String
                         , comment: String = ""
                         , id: Long = 0) extends Record[String] {
    //------------------------------------------------------------------------
    override def getFormatted() = s"'${keyword.trim}' '${value.trim}' ${if(comment.isEmpty) "" else s"'$comment'"}"
    //------------------------------------------------------------------------
  }
  //--------------------------------------------------------------------------
}
//=============================================================================
//End of file RecordString.scala
//=============================================================================
