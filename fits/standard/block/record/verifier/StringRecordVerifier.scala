/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  08/Sep/2023
  * Time:  17h:56m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier
//=============================================================================
import com.common.fits.standard.block.record.{Record, RecordMap}
import com.common.fits.standard.block.record.verifier.common.SimpleRecordVerifierTrait
import com.common.fits.standard.fits.FitsCtxt
//=============================================================================
//=============================================================================
case class StringRecordVerifier(keyword: String
                                , sectionName: String
                                , extraCheck: Option[(RecordMap, Record[_],String,FitsCtxt) => Boolean] = None)
  extends SimpleRecordVerifierTrait[RecordVerifierDataTypeString] {
  //---------------------------------------------------------------------------
  val dataType = RecordVerifierDataTypeString()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SimpleStringRecordVerifier.scala
//=============================================================================