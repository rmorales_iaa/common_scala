/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  11/Sep/2023
  * Time:  12h:19m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier
//=============================================================================
import com.common.fits.standard.block.record.{Record, RecordMap}
import com.common.fits.standard.block.record.verifier.common.SimpleRecordVerifierTrait
import com.common.fits.standard.fits.FitsCtxt
//=============================================================================
//=============================================================================
case class IntegerRecordVerifier(keyword: String
                                 , sectionName: String
                                 , extraCheck: Option[(RecordMap, Record[_], String, FitsCtxt) => Boolean] = None)
  extends SimpleRecordVerifierTrait[RecordVerifierDataTypeInteger] {
  //---------------------------------------------------------------------------
  val dataType = RecordVerifierDataTypeInteger()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SimpleIntegerRecordVerifier.scala
//=============================================================================