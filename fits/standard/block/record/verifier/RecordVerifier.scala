/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  31/Aug/2023
  * Time:  10h:35m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier
//=============================================================================
import com.common.fits.standard.block.record.Record.DEPRECATED_KEYWORDS_SEQ
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.block.record.verifier.section_8.section_8_2.WcsPrimaryAndAlternative.getAlternativeWcsSuffix
import com.common.fits.standard.disconformity.DisconfWarn
import com.common.fits.standard.fits.FitsCtxt
//=============================================================================
//=============================================================================
object RecordVerifier {
  //---------------------------------------------------------------------------
  def checkDeprecatedKeywordSeq(recordMap: RecordMap, ctxt: FitsCtxt) =
    DEPRECATED_KEYWORDS_SEQ.foreach { case (keyword, sectionName, indexed) =>
      if (indexed) {
        val wcsSuffixSeq = getAlternativeWcsSuffix(recordMap)
        wcsSuffixSeq.foreach { wcsSuffix=>
          val maxIndex = recordMap.getWcsAxes(wcsSuffix)
          for (i <- 1L to maxIndex) {
            val key = s"$keyword$i$wcsSuffix"
            if (recordMap.getFirstRecord(key).isDefined)
              DisconfWarn(
                ctxt
                , sectionName
                , s"Structure: ${ctxt.structureID} the keyword '$key' is deprecated")
          }
        }
      }
      else {
        if (recordMap.getFirstRecord(s"$keyword").isDefined)
          DisconfWarn(
            ctxt
            , sectionName
            , s"Structure: ${ctxt.structureID} the keyword '$keyword' is deprecated")
      }
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RecordVerifier.scala
//=============================================================================