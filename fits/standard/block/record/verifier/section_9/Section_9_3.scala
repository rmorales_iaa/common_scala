/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  13/Sep/2023
  * Time:  14h:55m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.section_9
//=============================================================================
import com.common.fits.standard.Keyword.KEYWORD_TIMEUNIT
import com.common.fits.standard.block.record.{Record, RecordMap}
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.block.record.verifier.StringRecordVerifier
import com.common.fits.standard.disconformity.DisconfWarn
import com.common.fits.standard.fits.FitsCtxt
//=============================================================================
//=============================================================================
object Section_9_3 {
  //---------------------------------------------------------------------------
  private val sectionName = SectionName.SECTION_9_3
  //---------------------------------------------------------------------------
  private val recordVerifierSequence = Seq(
    StringRecordVerifier(KEYWORD_TIMEUNIT, sectionName, Some(extraCheckTimeUnit))
  )
  //---------------------------------------------------------------------------
  def getMapEntrySeq() = recordVerifierSequence.map { r => r.keyword -> r }

  //---------------------------------------------------------------------------
  //Table 34
  private val TIME_UNIT_MAP = Map(
      "s"  -> "second (default)"
    , "d"  -> "day (= 86,400 s)"
    , "a"  -> "(Julian) year (= 365.25 d)"
    , "cy" -> "(Julian) century (= 100 a)"
    , "min"-> "minute (= 60 s)"
    , "h"  -> "hour (= 60 min)"
    , "yr" -> "(Julian) year (= ’a’ = 365.25d)"
    , "ta" -> "tropical year"
    , "Ba" -> "Besselian year"
  )
  //---------------------------------------------------------------------------
  private def extraCheckTimeUnit(recordMap: RecordMap
                                  , record: Record[_]
                                  , sectionName: String
                                  , ctxt: FitsCtxt): Boolean = {
    val value = record.value.toString
    if (!TIME_UNIT_MAP.contains(value))
      DisconfWarn(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '${record.value.toString}' unknown time unit")
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Section_9_3.scala
//=============================================================================