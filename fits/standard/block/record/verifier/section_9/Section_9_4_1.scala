/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  13/Sep/2023
  * Time:  14h:55m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.section_9
//=============================================================================
import com.common.fits.standard.Keyword.{KEYWORD_TIMEOFFS}
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.block.record.verifier.DoubleRecordVerifier
//=============================================================================
//=============================================================================
object Section_9_4_1 {
  //---------------------------------------------------------------------------
  private val sectionName = SectionName.SECTION_9_4_1
  //---------------------------------------------------------------------------
  private val recordVerifierSequence = Seq(
    DoubleRecordVerifier(KEYWORD_TIMEOFFS, sectionName)
  )
  //---------------------------------------------------------------------------
  def getMapEntrySeq() = recordVerifierSequence.map { r => r.keyword -> r }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Section_9_4_1.scala
//=============================================================================