/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  13/Sep/2023
  * Time:  14h:55m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.section_9
//=============================================================================
import com.common.fits.standard.Keyword.{KEYWORD_BEPOCH, KEYWORD_DATE_BEG, KEYWORD_DATE_END, KEYWORD_JEPOCH, KEYWORD_MJD_BEG, KEYWORD_MJD_END, KEYWORD_TSTART, KEYWORD_TSTOP}
import com.common.fits.standard.block.record.verifier.section_4_4.Section_4_4_2_1.extraCheckDATE
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.block.record.verifier.{DoubleRecordVerifier, StringRecordVerifier}
//=============================================================================
//=============================================================================
object Section_9_5 {
  //---------------------------------------------------------------------------
  private val sectionName = SectionName.SECTION_9_5
  //---------------------------------------------------------------------------
  private val recordVerifierSequence = Seq(
       StringRecordVerifier(KEYWORD_DATE_BEG, sectionName, Some(extraCheckDATE))
     , StringRecordVerifier(KEYWORD_DATE_END, sectionName, Some(extraCheckDATE))
     , DoubleRecordVerifier(KEYWORD_MJD_BEG, sectionName)
     , DoubleRecordVerifier(KEYWORD_MJD_END, sectionName)
     , DoubleRecordVerifier(KEYWORD_TSTART, sectionName)
     , DoubleRecordVerifier(KEYWORD_TSTOP, sectionName)
     , DoubleRecordVerifier(KEYWORD_JEPOCH, sectionName)
     , DoubleRecordVerifier(KEYWORD_BEPOCH , sectionName)
  )
  //---------------------------------------------------------------------------
  def getMapEntrySeq() = recordVerifierSequence.map { r => r.keyword -> r }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Section_9_5.scala
//=============================================================================