/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  13/Sep/2023
  * Time:  14h:55m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.section_9
//=============================================================================
import com.common.fits.standard.Keyword.{KEYWORD_CPERI, KEYWORD_CZPHS, KEYWORD_WCSAXES}
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.block.record.verifier.common.singleIndex.RecordVerifierSingleIndexTrait
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.block.record.verifier.RecordVerifierDataTypeDouble
import com.common.fits.standard.block.record.verifier.section_8.section_8_2.WcsPrimaryAndAlternative.PRIMARY_WCS_SUFFIX
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object Section_9_6 {
  //---------------------------------------------------------------------------
  private val sectionName = SectionName.SECTION_9_6
  //---------------------------------------------------------------------------
  def verify(recordMap:RecordMap
             , ctxt: FitsCtxt): Boolean = {
    val positionSeq = ArrayBuffer[Long]()

    val wcsAxes = {
      val r = recordMap.getAllKeywordWithPrefix(KEYWORD_WCSAXES).sorted
      if (r.isEmpty) Array(PRIMARY_WCS_SUFFIX)
      else r
    }
    wcsAxes.foreach { keyword =>
      val wcsSuffix = keyword.replace(KEYWORD_WCSAXES, "") //empty wcsSuffix means primary wcs
      if (!RecordVerifierCZPHS(wcsSuffix).verify(recordMap, positionSeq, ctxt)) return false
      if (!RecordVerifierCPERI(wcsSuffix).verify(recordMap, positionSeq, ctxt)) return false
    }
    true
  }
  //---------------------------------------------------------------------------
  case class RecordVerifierCZPHS(wcsSuffix: String) extends RecordVerifierSingleIndexTrait {
    //-------------------------------------------------------------------------
    val keywordBase = KEYWORD_CZPHS
    val dataType = RecordVerifierDataTypeDouble()
    val isIndexed = true
    //-------------------------------------------------------------------------
    def verify(recordMap: RecordMap
               , positionSeq: ArrayBuffer[Long]
               , ctxt: FitsCtxt): Boolean = {
      if (!super.verify(recordMap
        , maxIndex = recordMap.getWcsAxes(wcsSuffix)
        , sectionName
        , ctxt)) return false
      true
    }
    //-------------------------------------------------------------------------
  }
  //===========================================================================
  case class RecordVerifierCPERI(wcsSuffix: String) extends RecordVerifierSingleIndexTrait {
    //-------------------------------------------------------------------------
    val keywordBase = KEYWORD_CPERI
    val dataType = RecordVerifierDataTypeDouble()
    val isIndexed = true
    //-------------------------------------------------------------------------
    def verify(recordMap: RecordMap
               , positionSeq: ArrayBuffer[Long]
               , ctxt: FitsCtxt): Boolean = {

      if (!super.verify(recordMap
        , maxIndex = recordMap.getWcsAxes(wcsSuffix)
        , sectionName
        , ctxt)) return false
      true
    }
    //-------------------------------------------------------------------------
  }
  //===========================================================================
}
//=============================================================================
//=============================================================================
//End of file Section_9_6.scala
//=============================================================================