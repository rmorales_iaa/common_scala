/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  31/Aug/2023
  * Time:  10h:35m
  * Description: None
  */
//===========================================================================
package com.common.fits.standard.block.record.verifier.section_9
//===========================================================================
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.fits.FitsCtxt
//===========================================================================
//===========================================================================
object Section_9 {
  //---------------------------------------------------------------------------
  private final val recordVerifierMap =
    (Section_9_2_1.getMapEntrySeq() ++  //Time scale
     Section_9_2_2.getMapEntrySeq() ++  //Time reference value
     Section_9_2_3.getMapEntrySeq() ++  //Time reference position
     Section_9_2_4.getMapEntrySeq() ++  //Time reference direction
     Section_9_2_5.getMapEntrySeq() ++  //Solar system ephemeris
     Section_9_3.getMapEntrySeq() ++    //Time unit
     Section_9_4_1.getMapEntrySeq() ++  //Time offset
     //Sectrion 9.4.2 'Time resolution and binding' has no checks to apply
     Section_9_4_3.getMapEntrySeq() ++  //Time errors
     Section_9_5.getMapEntrySeq() ++    //Global time keywords
     //Sectrion 9.6 'Other time-coordinate axes' is checked below
     Section_9_7.getMapEntrySeq()       // Durations
    ) .toMap
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , ctxt: FitsCtxt): Boolean = {
    recordVerifierMap.foreach { case (keyword, recordVerifier) =>
      if (recordMap.contains(keyword))
        if (!recordVerifier.verify(recordMap,ctxt)) return false
    }
    Section_9_6.verify(recordMap,ctxt)
  }
  //---------------------------------------------------------------------------
}
//===========================================================================
//===========================================================================
//End of file Section_9.scala
//===========================================================================