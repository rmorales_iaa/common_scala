/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  13/Sep/2023
  * Time:  14h:55m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.section_9
//=============================================================================
import com.common.fits.standard.Keyword._
import com.common.fits.standard.block.record.{Record, RecordMap}
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.block.record.verifier.{DoubleRecordVerifier, StringRecordVerifier}
import com.common.fits.standard.disconformity.{DisconfFatal, DisconfWarn}
import com.common.fits.standard.fits.FitsCtxt
//=============================================================================
//=============================================================================
object Section_9_2_3 {
  //---------------------------------------------------------------------------
  private val sectionName = SectionName.SECTION_9_2_3
  //---------------------------------------------------------------------------
  private val recordVerifierSequence = Seq(
      StringRecordVerifier(KEYWORD_TREFPOS, sectionName, Some(extraCheckRefPos))
    , DoubleRecordVerifier(KEYWORD_OBSGEO_B, sectionName, Some(extraCheckLatitude))
    , DoubleRecordVerifier(KEYWORD_OBSGEO_L, sectionName, Some(extraCheckLongitude))
    , DoubleRecordVerifier(KEYWORD_OBSGEO_H, sectionName, Some(extraCheckAltitude))
    , StringRecordVerifier(KEYWORD_OBSORBIT, sectionName)
  )
  //---------------------------------------------------------------------------
  def getMapEntrySeq() = recordVerifierSequence.map { r => r.keyword -> r }
  //---------------------------------------------------------------------------
  private final val TIME_REFERENCE_TOPOCENTER = "TOPOCENTER"
  private final val TIME_REFERENCE_CUSTOM     = "CUSTOM"

  //Table 31
  private val KNOWN_STANDARD_TIME_REFERENCE_POSITION_VALUES_MAP = Map(
    TIME_REFERENCE_TOPOCENTER   -> "The location from where the ob servation was made"
    , "GEOCENTER"    -> "Geocenter"
    , "BARYCENTER"   -> "Barycenter of the Solar System"
    , "RELOCATABLE"  -> "Greenwich Mean Time"
    , TIME_REFERENCE_CUSTOM       -> "Relocatable: to be used for simulation data only"
    , "HELIOCENTER"  -> "A position specified by coordinates that is\nnot the observatory location"
    , "GALACTIC"     -> "Heliocenter"
    , "EMBARYCENTER" -> "Earth-Moon barycenter"
    , "MERCURY"      -> "Center of Mercury"
    , "VENUS"        -> "Center of Venus"
    , "MARS"         -> "Center of Mars"
    , "JUPITER"      -> "Barycenter of the Jupiter system"
    , "SATURN"       -> "Barycenter of the Saturn system"
    , "URANUS"       -> "Barycenter of the Uranus system"
    , "NEPTUNE"      -> "Barycenter of the Neptune system"
  )
  //---------------------------------------------------------------------------
  //Table 32
  //---------------------------------------------------------------------------
  private val TIME_SCALE_AND_REFERENCE_POSITION_COL_1_MAP = Seq(
    "TT", "TDT", "TAI", "IAT", "GPS", "UTC", "GMT"
  )
  //---------------------------------------------------------------------------
  private val TIME_SCALE_AND_REFERENCE_POSITION_COL_2_MAP = Seq(
    "TCG"
  )
  //---------------------------------------------------------------------------
  private val TIME_SCALE_AND_REFERENCE_POSITION_COL_3_MAP = Seq(
    "TDB"
  )
  //---------------------------------------------------------------------------
  private val TIME_SCALE_AND_REFERENCE_POSITION_COL_4_MAP = Seq(
    "TCB"
  )
  //---------------------------------------------------------------------------
  private val TIME_SCALE_AND_REFERENCE_POSITION_COL_5_MAP = Seq(
    "LOCAL"
  )
  //---------------------------------------------------------------------------
  private final val TIME_SCALE_AND_REFERENCE_POSITION_COL_SEQ = Seq(
      TIME_SCALE_AND_REFERENCE_POSITION_COL_1_MAP
    , TIME_SCALE_AND_REFERENCE_POSITION_COL_2_MAP
    , TIME_SCALE_AND_REFERENCE_POSITION_COL_3_MAP
    , TIME_SCALE_AND_REFERENCE_POSITION_COL_4_MAP
    , TIME_SCALE_AND_REFERENCE_POSITION_COL_5_MAP
  )
  //---------------------------------------------------------------------------
  //Table 32
  private val TIME_SCALE_AND_REFERENCE_POSITION_COMPATIBILITY_MAP = Map(
      "TOPOCENTER"   -> "t;ls;;;"
    , "GEOCENTER"    -> "ls;c;;;"
    , "BARYCENTER"   -> ";;ls;c;"
    , "RELOCATABLE"  -> ";;;;c"
    , "ANY"          -> "re;re;;;"
  )
  //---------------------------------------------------------------------------
  private val TIME_SCALE_AND_REFERENCE_POSITION_COMPATIBILITY_FLAG_MAP = Map(
      "c" -> "correct match; reference position coincides with the spatial origin of\nthe space-time coordinates"
    , "t" -> "correct match on Earth’s surface, otherwise usually linear scaling"
    , "ls" -> "linear relativistic scaling"
    , "re" -> "non-linear relativistic scaling"
  )
  //---------------------------------------------------------------------------
  private def extraCheckRefPos(recordMap: RecordMap
                                , record: Record[_]
                                , sectionName: String
                                , ctxt: FitsCtxt): Boolean = {
    val value = record.value.toString
    if (!KNOWN_STANDARD_TIME_REFERENCE_POSITION_VALUES_MAP.contains(value))
      DisconfWarn(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '${record.value.toString}' unknown time reference position")

    if(!checkMultipleCoordinateSystem(recordMap,ctxt)) return false
    checkCompatTimeScaleAndRefPos(recordMap, value, ctxt)
    if (value == TIME_REFERENCE_TOPOCENTER ||
        value == TIME_REFERENCE_CUSTOM) {
      if (!requireObservingCoord(recordMap, value, ctxt)) return true
    }

    if (value != TIME_REFERENCE_TOPOCENTER)
      requireDirection(recordMap, value, ctxt)
    true
  }
  //---------------------------------------------------------------------------
  private def checkCompatTimeScaleAndRefPos(recordMap: RecordMap
                                            , refPos:String
                                            , ctxt: FitsCtxt): Boolean = {
    val timeScale = recordMap.getTIMESYS().get
    val column = TIME_SCALE_AND_REFERENCE_POSITION_COL_SEQ.indexWhere(_.contains(timeScale)) + 1
    val commonMessage = s"Structure: ${ctxt.structureID} keyword '$KEYWORD_TIMESYS' with value '$timeScale' and  keyword '$KEYWORD_TREFPOS' with value '$refPos'"

    TIME_SCALE_AND_REFERENCE_POSITION_COMPATIBILITY_MAP.get(refPos) match {

      case Some(compatibility) =>
        val components = compatibility.split(";").toList
        val compatFlag = components.lift(column - 1)

        if (compatFlag.isDefined)
          DisconfWarn(
            ctxt
            , sectionName
            , s"$commonMessage compatibility '${TIME_SCALE_AND_REFERENCE_POSITION_COMPATIBILITY_FLAG_MAP(compatFlag.get)}'")
        else
          DisconfWarn(
            ctxt
            , sectionName
            , s"$commonMessage has not a recommended compatibility")


      case None =>
        if ((column == 0) ||  //none column
            (column > 2))   //avoid columns 3,4 and 5
          DisconfWarn(
            ctxt
            , sectionName
            , s"$commonMessage has not a recommended compatibility")
        else
          DisconfWarn(
            ctxt
            , sectionName
            , s"$commonMessage compatibility '${TIME_SCALE_AND_REFERENCE_POSITION_COMPATIBILITY_FLAG_MAP("re")}'")
    }

    true
  }
  //---------------------------------------------------------------------------
  private def requireObservingCoord(recordMap: RecordMap
                                    , value: String
                                    , ctxt: FitsCtxt): Boolean = {
    if(getObservingCoordinateSystem(recordMap).isEmpty) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '$KEYWORD_TREFPOS' with value '$value' requires that the observing coordinates to be specified")
    }
    true
  }
  //---------------------------------------------------------------------------
  private def checkMultipleCoordinateSystem(recordMap: RecordMap, ctxt: FitsCtxt): Boolean = {
    val coordSystemSeq = getObservingCoordinateSystem(recordMap)
    if (coordSystemSeq.size > 1) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword only one observing coordinates definition allowed, but more than one found:${coordSystemSeq.mkString("{", ",", "}")})")
    }
    true
  }
  //---------------------------------------------------------------------------
  private def getObservingCoordinateSystem(recordMap: RecordMap): Seq[String] = {
    ((if(recordMap.contains(KEYWORD_OBSGEO_X) &&
       recordMap.contains(KEYWORD_OBSGEO_Y) &&
       recordMap.contains(KEYWORD_OBSGEO_Z) ) Some("ITRS")
    else None) ++

    (if (recordMap.contains(KEYWORD_OBSGEO_B) &&
        recordMap.contains(KEYWORD_OBSGEO_L) &&
        recordMap.contains(KEYWORD_OBSGEO_H)) Some("geodetic")
    else None) ++

    (if (recordMap.contains(KEYWORD_OBSORBIT)) Some("ephemeris")
    else None)).toSeq
  }
  //---------------------------------------------------------------------------
  private def requireDirection(recordMap: RecordMap
                               , value: String
                               , ctxt: FitsCtxt): Unit =
    if (!recordMap.contains(KEYWORD_TREFDIR))
      DisconfWarn(
        ctxt
        , SectionName.SECTION_9_2_4
        , s"Structure: ${ctxt.structureID} keyword '$KEYWORD_TREFPOS' with value '$value' should have a reference direction")
  //---------------------------------------------------------------------------
  private def extraCheckLatitude(recordMap: RecordMap
                               , record: Record[_]
                               , sectionName: String
                               , ctxt: FitsCtxt): Boolean = {

    val value = record.value.toString
    val latitude = record.value.toString.toDouble
    if (latitude < -90.0 || latitude > 90.0)
      DisconfWarn(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '$value' is not a valid latitude")
    true
  }
  //---------------------------------------------------------------------------
  private def extraCheckLongitude(recordMap: RecordMap
                                 , record: Record[_]
                                 , sectionName: String
                                 , ctxt: FitsCtxt): Boolean = {

    val value = record.value.toString
    val longitude = record.value.toString.toDouble
    if (longitude < -180.0 | longitude > 180.0)
      DisconfWarn(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '$value' is not a valid longitude")
    true
  }
  //---------------------------------------------------------------------------
  private def extraCheckAltitude(recordMap: RecordMap
                                 , record: Record[_]
                                 , sectionName: String
                                 , ctxt: FitsCtxt): Boolean = {

    val value = record.value.toString
    val altitude = record.value.toString.toDouble
    if (altitude < 0)
      DisconfWarn(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '$value' is not a valid altitude")
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Section_9_2_3.scala
//=============================================================================