/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  13/Sep/2023
  * Time:  14h:55m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.section_9
//=============================================================================
import com.common.fits.standard.Keyword.KEYWORD_TIMESYS
import com.common.fits.standard.block.record.{Record, RecordMap}
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.block.record.verifier.StringRecordVerifier
import com.common.fits.standard.disconformity.{DisconfWarn}
import com.common.fits.standard.fits.FitsCtxt
//=============================================================================
//=============================================================================
object Section_9_2_1 {
  //---------------------------------------------------------------------------
  private val sectionName = SectionName.SECTION_9_2_1
  //---------------------------------------------------------------------------
  private val recordVerifierSequence = Seq(
    StringRecordVerifier(KEYWORD_TIMESYS, sectionName, Some(extraCheckTimeSys))
  )
  //---------------------------------------------------------------------------
  def getMapEntrySeq() = recordVerifierSequence.map { r => r.keyword -> r }
  //---------------------------------------------------------------------------
  private final val TIME_SCALE_TDT =  "TDT"   -> "Terrestrial Dynamical Time"
  private final val TIME_SCALE_IAT =  "IAT"   -> "Synonym for TAI"
  private final val TIME_SCALE_ET  =  "ET"    -> "Ephemeris Time"

  private final val TIME_SCALE_TCB   =  "TCB" -> "Barycentric Coordinate Time"
  private final val TIME_SCALE_TDB   =  "TDB" -> "Barycentric Dynamical Time"
  private final val TIME_SCALE_LOCAL =  "LOCAL" -> "For simulation data and for free-running clocks"

  //Table 30
  private val KNOWN_TIME_SCALE_MAP = scala.collection.mutable.LinkedHashMap (
     "TAI"    -> "International Atomic Time"
    , "TT"    -> "Terrestrial Time; IAU standard"
    , TIME_SCALE_TDT
    , TIME_SCALE_ET
    , TIME_SCALE_IAT
    , "UT1"   -> "Universal Time"
    , "UTC"   -> "Universal Time"
    , "GMT"   -> "Greenwich Mean Time"
    , "UT()"    -> "Universal Time, with qualifier"
    , "GPS"   -> "Global Positioning System"
    , "TCG"   -> "Geocentric Coordinate Time"
    , TIME_SCALE_TCB
    , TIME_SCALE_TDB
    , TIME_SCALE_LOCAL
  )

  private val KNOWN_TIME_SCALE_DEPRECATED_MAP = Map(
    TIME_SCALE_TDT
    , TIME_SCALE_IAT
  )

  private val KNOWN_TIME_SCALE_NOT_USED_MAP = Map(
    TIME_SCALE_TDT
    , TIME_SCALE_IAT
  )

  //next three maps are used to implement 'Section 9.8.2 Restrictions on alternate descriptions'
  private val TIME_SCALE_LIMITED_COMBINATION_SET = Set(
      TIME_SCALE_TCB._1
    , TIME_SCALE_TDB._1
    , TIME_SCALE_ET._1
  )

  private final val TIME_SCALE_TO_BE_AVOIDED_IN_COMBINATION_SET =
    KNOWN_TIME_SCALE_MAP.take(9).map (_._1).toSet

  private final val TIME_SCALE_UNIQUE_SET = Set(
    TIME_SCALE_LOCAL._1
  )
  //---------------------------------------------------------------------------
  private def extraCheckTimeSys(recordMap: RecordMap
                                , record: Record[_]
                                , sectionName: String
                                , ctxt: FitsCtxt): Boolean = {

    val value = record.value.toString
    val keyword = record.keyword
    if (!recordMap.contains(keyword)) {
      DisconfWarn(
        ctxt
        , SectionName.SECTION_9_8_2
        , s"Structure: ${ctxt.structureID} keyword '$keyword' is not present , but it is strongly recommended")
      return true
    }

    if (!KNOWN_TIME_SCALE_MAP.contains(value) &&
        !(value.startsWith("UT(") && value.endsWith(")"))) //UT(XXX) is a known time scale. See 'Representations of time coordinates in FITS'. A. Rots (2015) DOI: 10.1051/0004-6361/201424653
      DisconfWarn(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '${record.value.toString}' unknown time scale")

    if (KNOWN_TIME_SCALE_DEPRECATED_MAP.contains(value))
      DisconfWarn(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '${record.value.toString}' is deprecated")

    if (KNOWN_TIME_SCALE_NOT_USED_MAP.contains(value))
      DisconfWarn(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '${record.value.toString}' should not be used for data taken after 1984-01-01")

    checkTimeScaleCombinationRestriction(recordMap, keyword, ctxt)

    true
  }

  //---------------------------------------------------------------------------
  private def isDefinedAvoidTimeScale(timeScaleValueSeq: Set[String]): Boolean = {
    timeScaleValueSeq.map { timeScale =>
      if (TIME_SCALE_TO_BE_AVOIDED_IN_COMBINATION_SET.contains(timeScale) ||
         (timeScale.startsWith("UT(") && timeScale.endsWith(")"))) return true
    }
    false
  }
  //---------------------------------------------------------------------------
  private def checkTimeScaleCombinationRestriction(recordMap: RecordMap
                                                   , keyword: String
                                                   , ctxt: FitsCtxt): Unit = {

    val timeScaleRecordSeq = recordMap.getAllRecordSeq(keyword)
    if (timeScaleRecordSeq.size < 2)  return
    val timeScaleValueSet = (timeScaleRecordSeq map (_.value.toString)).toSet
    val _isDefinedAvoidTimeScale = isDefinedAvoidTimeScale(timeScaleValueSet)

    timeScaleValueSet.foreach { timeScale =>
      if (TIME_SCALE_TO_BE_AVOIDED_IN_COMBINATION_SET.contains(timeScale) &&
        _isDefinedAvoidTimeScale) {
          DisconfWarn(
            ctxt
            , SectionName.SECTION_9_8_2
            , s"Structure: ${ctxt.structureID} keyword '$keyword' with value '$timeScale' cannot be combined with any of the first nine time scales:'${TIME_SCALE_TO_BE_AVOIDED_IN_COMBINATION_SET.mkString("{",",","}")}'")
      }
    }
    if (TIME_SCALE_UNIQUE_SET.intersect(timeScaleValueSet).size > 0) {
      DisconfWarn(
        ctxt
        , SectionName.SECTION_9_8_2
        , s"Structure: ${ctxt.structureID} keyword '$keyword' with value '${TIME_SCALE_UNIQUE_SET.mkString("{",",","}")}' should not be mixed with any of the others time scale'${timeScaleValueSet.mkString("{", ",", "}")}'")
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Section_9_2_1.scala
//=============================================================================