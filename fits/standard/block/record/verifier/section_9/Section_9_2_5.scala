/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  13/Sep/2023
  * Time:  14h:55m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.section_9
//=============================================================================
import com.common.fits.standard.Keyword.KEYWORD_PLEPHEM
import com.common.fits.standard.block.record.{Record, RecordMap}
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.block.record.verifier.StringRecordVerifier
import com.common.fits.standard.disconformity.DisconfWarn
import com.common.fits.standard.fits.FitsCtxt
//=============================================================================
//=============================================================================
object Section_9_2_5 {
  //---------------------------------------------------------------------------
  private val sectionName = SectionName.SECTION_9_2_5
  //---------------------------------------------------------------------------
  private val recordVerifierSequence = Seq(
    StringRecordVerifier(KEYWORD_PLEPHEM, sectionName, Some(extraCheckEphemeris))
  )
  //---------------------------------------------------------------------------
  def getMapEntrySeq() = recordVerifierSequence.map { r => r.keyword -> r }
  //---------------------------------------------------------------------------
  //Table 33
  private val SOLAR_SYSTEM_EPHEMERIS_MAP = Map(
      "DE200" -> "Standish (1990)"
    , "DE405" -> "Standish (1998)"
    , "DE421" -> "Folkner, et al. (2009)"
    , "DE430" -> "Folkner, et al. (2014)"
    , "DE431" -> "Folkner, et al. (2014)"
    , "DE432" -> "Folkner, et al. (2014)"
  )
  //---------------------------------------------------------------------------
  private def extraCheckEphemeris(recordMap: RecordMap
                                  , record: Record[_]
                                  , sectionName: String
                                  , ctxt: FitsCtxt): Boolean = {
    val value = record.value.toString
    if (!SOLAR_SYSTEM_EPHEMERIS_MAP.contains(value))
      DisconfWarn(
          ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '${record.value.toString}' unknown Solar System ephemeris")
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Section_9_2_5.scala
//=============================================================================