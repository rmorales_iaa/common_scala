/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  13/Sep/2023
  * Time:  14h:55m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.section_9
//=============================================================================
import com.common.fits.standard.Keyword.{KEYWORD_TREFDIR}
import com.common.fits.standard.block.record.{Record, RecordMap}
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.block.record.verifier.StringRecordVerifier
import com.common.fits.standard.disconformity.DisconfFatal
import com.common.fits.standard.fits.FitsCtxt
//=============================================================================
//=============================================================================
object Section_9_2_4 {
  //---------------------------------------------------------------------------
  private val sectionName = SectionName.SECTION_9_2_4
  //---------------------------------------------------------------------------
  private val recordVerifierSequence = Seq(
    StringRecordVerifier(KEYWORD_TREFDIR, sectionName, Some(extraCheckDirection))
  )
  //---------------------------------------------------------------------------
  def getMapEntrySeq() = recordVerifierSequence.map { r => r.keyword -> r }
  //---------------------------------------------------------------------------
  private def extraCheckDirection(recordMap: RecordMap
                                 , record: Record[_]
                                 , sectionName: String
                                 , ctxt: FitsCtxt): Boolean = {

    val keyword = record.keyword
    if (!recordMap.contains(keyword)) return true
    val value = record.value.toString
    val keywordSeq = value.split(",")
    if (keywordSeq.size != 2) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '$keyword' with value '$value' has not a valid value. Expected two keywords separated by one comma")
    }
    keywordSeq.foreach { key =>
      if (!recordMap.contains(key)) {
        DisconfFatal(
          ctxt
          , sectionName
          , s"Structure: ${ctxt.structureID} keyword '$keyword' with value '$value' has referenced to keyword '$key' that does not exist")
      }
    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Section_9_2_4.scala
//=============================================================================