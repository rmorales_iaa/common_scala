/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  13/Sep/2023
  * Time:  14h:55m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.section_9
//=============================================================================
import com.common.fits.standard.Keyword.{KEYWORD_DATEREF, KEYWORD_JDREF, KEYWORD_JDREFF, KEYWORD_JDREFI, KEYWORD_MJDREF, KEYWORD_MJDREFF, KEYWORD_MJDREFI}
import com.common.fits.standard.block.record.verifier.section_4_4.Section_4_4_2_1.extraCheckDATE
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.block.record.verifier.{DoubleRecordVerifier, IntegerRecordVerifier, StringRecordVerifier}
//=============================================================================
//=============================================================================
object Section_9_2_2 {
  //---------------------------------------------------------------------------
  private val sectionName = SectionName.SECTION_9_2_2
  //---------------------------------------------------------------------------
  private val recordVerifierSequence = Seq(
      DoubleRecordVerifier(KEYWORD_MJDREF, sectionName)
    , DoubleRecordVerifier(KEYWORD_JDREF, sectionName)
    , StringRecordVerifier(KEYWORD_DATEREF, sectionName, Some(extraCheckDATE))
    , IntegerRecordVerifier(KEYWORD_MJDREFI, sectionName)
    , DoubleRecordVerifier(KEYWORD_MJDREFF, sectionName)
    , IntegerRecordVerifier(KEYWORD_JDREFI, sectionName)
    , DoubleRecordVerifier(KEYWORD_JDREFF, sectionName)
  )
  //---------------------------------------------------------------------------
  def getMapEntrySeq() = recordVerifierSequence.map { r => r.keyword -> r }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Section_9_2_2.scala
//=============================================================================