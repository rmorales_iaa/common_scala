/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  08/Sep/2023
  * Time:  11h:08m
  * Description: None
  */
//=============================================================================
package com.common.fits.standard.block.record.verifier.section_8.section_8_2
//=============================================================================
import com.common.fits.standard.Keyword._
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.block.record.verifier.common.noIndex.{RecordVerifierDoubleNoIndexTrait, RecordVerifierStringNoIndexTrait}
import com.common.fits.standard.block.record.verifier.section_8.section_8_2.RecordVerifierRADESYS._
import com.common.fits.standard.block.record.verifier.section_8.section_8_2.WcsPrimaryAndAlternative.getAlternativeWcsSuffix
import com.common.fits.standard.disconformity.{DisconfFatal, DisconfWarn}
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.sectionName.SectionName
//=============================================================================
//=============================================================================
object Section_8_3 {
  //---------------------------------------------------------------------------
  val sectionName = SectionName.SECTION_8_3
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , ctxt: FitsCtxt): Boolean = {

    val wcsSuffixSeq = getAlternativeWcsSuffix(recordMap)
    wcsSuffixSeq.foreach { wcsSuffix =>
      if (!RecordVerifierRADESYS(wcsSuffix).verify(recordMap, sectionName, ctxt)) return false
      if (!RecordVerifierEQUINOX(wcsSuffix).verify(recordMap, sectionName, ctxt)) return false
      if (!RecordVerifierEPOCH(wcsSuffix).verify(recordMap, sectionName, ctxt)) return false
      //DATE_OBS verifier in class "Section_4_4_2_2"
      if (!RecordVerifierMJD_OBS(wcsSuffix).verify(recordMap, sectionName, ctxt)) return false
      if (!RecordVerifierLONPOLE(wcsSuffix).verify(recordMap, sectionName, ctxt)) return false
      if (!RecordVerifierLATPOLE(wcsSuffix).verify(recordMap, sectionName, ctxt)) return false
    }
    true
  }
  //---------------------------------------------------------------------------
  def getDefaultRADESYS(recordMap: RecordMap
                        , wcsSuffix: String = ""): String = {
    val keyword = KEYWORD_RADESYS + wcsSuffix
    if (recordMap.contains(keyword)) return recordMap.getFirstValue[String](keyword).get
    val keywordEquinox = KEYWORD_EQUINOX + wcsSuffix
    if (!recordMap.contains(keywordEquinox)) return RecordVerifierRADESYS.VALID_REFERENCE_FRAME_ICRS
    val equinox = recordMap.getFirstValue[Double](keywordEquinox).get
    if (equinox < VALID_REFERENCE_FRAME_YEAR_1984) VALID_REFERENCE_FRAME_FK4
    else VALID_REFERENCE_FRAME_FK5
  }

  //---------------------------------------------------------------------------
  private def getDefaultLAT_LONPOLE(recordMap: RecordMap
                                   , isLon: Boolean
                                   , i: Long = 1): Option[Double] = {

    val angleSeq = WcsPrimaryAndAlternative.getRotationDegreeAngle(recordMap).getOrElse(return None)
    val cType = recordMap.getFirstValue[String](KEYWORD_CTYPE + i).getOrElse(return None)
    val celestialCoordEntry = RecordVerifierCTYPE.CELESTIAL_COORDINATE_ALGORITHM_CODE_MAP.get(cType).getOrElse(return None)
    val phi = celestialCoordEntry._1.toDouble //longitude
    val theta = celestialCoordEntry._2.toDouble //latitude

    val deltaRot = {
      //convention 1 and convention 2
      if (recordMap.contains(KEYWORD_CD + "1_1") ||
          recordMap.contains(KEYWORD_PC + "1_1")) {
        angleSeq(1)
      }
      //convention 3
      angleSeq(if (i == 1) 1 else 3)
    }
    if (isLon) {
      if (deltaRot >= theta) Some(phi)
      else Some(phi + 180)
    }
    else {
      if (theta != 0 || deltaRot != 0 || Math.abs(phi) != 90) Some(90D)
      else None
    }
  }
  //---------------------------------------------------------------------------
  def getDefaultLONPOLE(recordMap: RecordMap
                        , wcsSuffix: String
                        , i: Long = 1): Option[Double] = {
    val keyword = KEYWORD_LONPOLE+wcsSuffix
    if (recordMap.contains(keyword)) return Some(recordMap.getFirstValue[Double](keyword).get)
    getDefaultLAT_LONPOLE(recordMap
                          , isLon = true
                          , i)
  }
  //---------------------------------------------------------------------------
  def getDefaultLATPOLE(recordMap: RecordMap
                        , wcsSuffix: String
                        , i: Long = 1): Option[Double] = {
    val keyword = KEYWORD_LATPOLE + wcsSuffix
    if (recordMap.contains(keyword)) return Some(recordMap.getFirstValue[Double](keyword).get)
    getDefaultLAT_LONPOLE(recordMap
                          , isLon = false
                          , i)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
object RecordVerifierRADESYS {
  //---------------------------------------------------------------------------
  final val VALID_REFERENCE_FRAME_ICRS =   "ICRS"  //International Celestial Reference System
  final val VALID_REFERENCE_FRAME_FK4 =    "FK4"   //Mean place, old (Bessel-Newcomb) system
  final val VALID_REFERENCE_FRAME_FK5 =    "FK5"   //Mean place, new (IAU 1984) system
  //---------------------------------------------------------------------------
  final val VALID_REFERENCE_FRAME_YEAR_1984 = 1984.0
  //---------------------------------------------------------------------------
  private final val DEPRECATED_REFERENCE_FRAME_SEQ = Seq(
    VALID_REFERENCE_FRAME_FK4
    , "FK4-NO-E" //Mean place: but without eccentricity terms
  )
  //---------------------------------------------------------------------------
  private final val VALID_REFERENCE_FRAME_SEQ = Seq (
    VALID_REFERENCE_FRAME_ICRS
    , VALID_REFERENCE_FRAME_FK5
    , "GAPPT"    //Geocentric apparent place, IAU 1984 system
  ) ++ DEPRECATED_REFERENCE_FRAME_SEQ

  private final val VALID_QUOTED_REFERENCE_FRAME_SEQ = VALID_REFERENCE_FRAME_SEQ map (s=> s"'$s'")
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierRADESYS(wcsSuffix:String) extends RecordVerifierStringNoIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_RADESYS
  //---------------------------------------------------------------------------
  override def verify(recordMap: RecordMap
                      , sectionName: String
                      , ctxt: FitsCtxt): Boolean = {
    if (!super.verify(recordMap, sectionName, ctxt)) return false
    val keyword = getKeyword
    if (!recordMap.contains(keyword)) return true
    val value = recordMap.getRADESYS(wcsSuffix)
    if (!VALID_REFERENCE_FRAME_SEQ.contains(value)) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '$keyword' with value '$value' has not an allowed value. Allowed value list:${VALID_QUOTED_REFERENCE_FRAME_SEQ.mkString("{",",","}")} ")
    }

    if (DEPRECATED_REFERENCE_FRAME_SEQ.contains(value)) {
      DisconfWarn(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '$keyword' with value '$value' is deprecated")
    }

    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierEQUINOX(wcsSuffix:String) extends RecordVerifierDoubleNoIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_EQUINOX
  //---------------------------------------------------------------------------
  override def verify(recordMap: RecordMap
                     , sectionName: String
                     , ctxt: FitsCtxt): Boolean = {
    if (!super.verify(recordMap, sectionName, ctxt)) return false
    val keyword = getKeyword
    if (!recordMap.contains(keyword)) return true
    val value = recordMap.getFirstRecord(keyword).get.value.toString.toDouble
    if (value < 0) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '$keyword' with value '$value' can not be negative")
    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierEPOCH(wcsSuffix:String) extends RecordVerifierDoubleNoIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_EPOCH

  //---------------------------------------------------------------------------
}
//=============================================================================

//DATE_OBS verifier in class "Section_4_4_2_2"
case class RecordVerifierMJD_OBS(wcsSuffix:String) extends RecordVerifierDoubleNoIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_MJD_OBS
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierLONPOLE(wcsSuffix:String) extends RecordVerifierDoubleNoIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_LONPOLE
  //---------------------------------------------------------------------------
  override def verify(recordMap: RecordMap
                      , sectionName: String
                      , ctxt: FitsCtxt): Boolean = {
    val keyword = getKeyword

    recordMap.getAllKeywordWithPrefix(keyword).map { key=>
      val value = recordMap.getFirstRecord(key).get.value.toString.toDouble
      if (value < 0) {
        DisconfWarn(
          ctxt
          , sectionName
          , s"Structure: ${ctxt.structureID} keyword '$key' negative value: '$value' is discouraged")
      }
    }

    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierLATPOLE(wcsSuffix:String) extends RecordVerifierDoubleNoIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_LATPOLE
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Section_8_3.scala
//=============================================================================