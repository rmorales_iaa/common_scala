/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  31/Aug/2023
  * Time:  10h:35m
  * Description: None
  */
//=============================================================================
package com.common.fits.standard.block.record.verifier.section_8.section_8_2
//=============================================================================
import com.common.fits.standard.Keyword._
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.block.record.verifier.section_8.section_8_2.RecordVerifierCTYPE._
import com.common.fits.standard.block.record.verifier.common.doubleIndex._
import com.common.fits.standard.block.record.verifier.common.singleIndex._
import com.common.fits.standard.block.record.verifier._
import com.common.fits.standard.block.record.verifier.common.singleIndex.RecordVerifierSingleIndexTrait.RECORD_POSITION_NOT_PRESENT_ID
import com.common.fits.standard.disconformity.{DisconfFatal}
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.sectionName.SectionName
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object WcsPrimaryAndAlternative {
  //---------------------------------------------------------------------------
  final val PRIMARY_WCS_SUFFIX         = ""
  final val ALTERNATIVE_WCS_SUFFIX_MIN = 'A'
  final val ALTERNATIVE_WCS_SUFFIX_MAX = 'Z'
  //---------------------------------------------------------------------------
  //(alpha,delta)
  private def getRotationAngleConventionCD(recordMap: RecordMap): Option[Seq[Double]] = {
    val cd_1_1 = recordMap.getFirstValue[Double](KEYWORD_CD + "1_1").getOrElse(return None)
    val cd_1_2 = recordMap.getFirstValue[Double](KEYWORD_CD + "1_2").getOrElse(return None)
    val cd_2_1 = recordMap.getFirstValue[Double](KEYWORD_CD + "2_1").getOrElse(return None)
    val cd_2_2 = recordMap.getFirstValue[Double](KEYWORD_CD + "2_2").getOrElse(return None)

    Some(Seq(Math.atan2(cd_2_1, cd_1_1).toDegrees
            , Math.atan2(-cd_1_2, cd_2_2).toDegrees))
  }
  //---------------------------------------------------------------------------
  //(alpha,delta)
  private def getRotationAngleConventionPC(recordMap: RecordMap): Option[Seq[Double]] = {

    val pc_1_1 = recordMap.getFirstValue[Double](KEYWORD_PC + "1_1").getOrElse(return None)
    val pc_1_2 = recordMap.getFirstValue[Double](KEYWORD_PC + "1_2").getOrElse(return None)
    val pc_2_1 = recordMap.getFirstValue[Double](KEYWORD_PC + "2_1").getOrElse(return None)
    val pc_2_2 = recordMap.getFirstValue[Double](KEYWORD_PC + "2_2").getOrElse(return None)

    Some(Seq(Math.atan2(pc_2_1, pc_1_1).toDegrees
            , Math.atan2(-pc_1_2, pc_2_2).toDegrees))
  }
  //---------------------------------------------------------------------------
  //(alpha,delta)
  private def getRotationAngleConventionCROTA(recordMap: RecordMap): Option[Seq[Double]] = {

    val cRota_1 = recordMap.getFirstValue[Double](KEYWORD_CROTA + "1").getOrElse(return None)
    val cRota_2 = recordMap.getFirstValue[Double](KEYWORD_CROTA + "2").getOrElse(return None)
    val cDelt_1 = recordMap.getFirstValue[Double](KEYWORD_CDELT + "1").getOrElse(return None)
    val cDelt_2 = recordMap.getFirstValue[Double](KEYWORD_CDELT + "2").getOrElse(return None)

    val delta1 = Math.toDegrees(Math.atan(math.sin(Math.toRadians(cRota_1)) / (cDelt_1 * math.cos(Math.toRadians(cRota_1)))))
    val delta2 = Math.toDegrees(Math.atan(math.sin(Math.toRadians(cRota_2)) / (cDelt_2 * math.cos(Math.toRadians(cRota_2)))))

    Some(Seq(cRota_1, delta1, cRota_2, delta2))
  }
  //---------------------------------------------------------------------------
  //(alpha,delta)
  def getRotationDegreeAngle(recordMap: RecordMap): Option[Seq[Double]] = {

    if (recordMap.contains(KEYWORD_CD + "1_1")) return getRotationAngleConventionCD(recordMap)
    if (recordMap.contains(KEYWORD_PC + "1_1")) return getRotationAngleConventionPC(recordMap)
    if (recordMap.contains(KEYWORD_CROTA + "1")) return getRotationAngleConventionCROTA(recordMap)
    None
  }
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , ctxt: FitsCtxt): Boolean = {
    //get primary and alternative axes
    val wcsAxes = {
      val r = recordMap.getAllKeywordWithPrefix(KEYWORD_WCSAXES).sorted
      if (r.isEmpty) Array(PRIMARY_WCS_SUFFIX)
      else {
        if (!r.contains(KEYWORD_WCSAXES)) {
          DisconfFatal(
            ctxt
            , SectionName.SECTION_8_2_1
            , s"Structure: ${ctxt.structureID} exists an alternative WCS definiton but primary definition is not fully provided")
          return false
        }
        r
      }
    }
    //check primary and alternative axes
    val existAdditionalWcsDefinition = wcsAxes.size > 1
    wcsAxes.foreach { keyword =>
      val wcsSuffix = keyword.replace(KEYWORD_WCSAXES,"") //empty wcsSuffix means primary wcs
      val (isValid,isCompleteDefinition) = verify(recordMap,wcsSuffix,ctxt)
      if (!isValid) return false
      if (existAdditionalWcsDefinition && !isCompleteDefinition) {  //is an alternative definition
        DisconfFatal(
          ctxt
          , SectionName.SECTION_8_2_1
          , s"Structure: ${ctxt.structureID} keyword '$keyword' is an alternative WCS definition, must it not have defined the complete set of wcs records")
        return false
      }
    }
    true
  }
  //---------------------------------------------------------------------------
  //(isValid,isCompleteDefinition)
  private def verify(recordMap: RecordMap
                     , wcsSuffix:String
                     , ctxt: FitsCtxt): (Boolean,Boolean) = {

    val positionSeq = ArrayBuffer[Long]()
    val recordWCSAXES = WcsPrimaryAndAlternativeAXES(wcsSuffix)
    if (!recordWCSAXES.verify(recordMap,positionSeq, ctxt)) return (false,false)

    val recordPC = RecordVerifierPC(wcsSuffix)
    if (!recordPC.verify(recordMap, positionSeq, ctxt)) return (false,false)

    val recordCDELT = RecordVerifierCDELT(wcsSuffix)
    if (!recordCDELT.verify(recordMap, positionSeq, ctxt)) return (false,false)

    val recordCD = RecordVerifierCD(wcsSuffix)
    if (!recordCD.verify(recordMap, positionSeq, ctxt)) return (false,false)

    val recordCROTA = RecordVerifierCROTA(wcsSuffix)
    if (!recordCROTA.verify(recordMap, positionSeq, ctxt)) return (false,false)

    //check incompatibilities with PC
    if (!checkIncompatibilityWithPC(recordCROTA, recordPC, ctxt)) return (false,false)
    if (!checkIncompatibilityWithPC(recordCD, recordPC, ctxt)) return (false,false)

    //remain keywords
    val recordCTYPE = RecordVerifierCTYPE(wcsSuffix)
    if (!recordCTYPE.verify(recordMap, positionSeq, ctxt)) return (false,false)

    val recordCUNIT = RecordVerifierCUNIT(wcsSuffix)
    if (!recordCUNIT.verify(recordMap, positionSeq, ctxt)) return (false,false)

    val recordCRPIX = RecordVerifierCRPIX(wcsSuffix)
    if (!recordCRPIX.verify(recordMap, positionSeq, ctxt)) return (false,false)

    val recordCRVAL = RecordVerifierCRVAL(wcsSuffix)
    if (!recordCRVAL.verify(recordMap, positionSeq, ctxt)) return (false,false)

    if (!RecordVerifierPV(wcsSuffix).verify(recordMap, positionSeq, ctxt)) return (false,false)
    if (!RecordVerifierPS(wcsSuffix).verify(recordMap, positionSeq, ctxt)) return (false,false)

    if (!RecordVerifierCRDER(wcsSuffix).verify(recordMap, positionSeq, ctxt)) return (false,false)
    if (!RecordVerifierCSYER(wcsSuffix).verify(recordMap, positionSeq, ctxt)) return (false,false)

    val isPrimaryWcs = wcsSuffix == PRIMARY_WCS_SUFFIX

    //finally check position of WCSAXES
    if (isPrimaryWcs &&
        !recordWCSAXES.veryPosition(recordMap
                                    , positionSeq.toArray.sorted
                                    , ctxt)) return (false,false)

    //check if it is a complete definition
    val wcsConvention_1 = recordPC.isDefined && recordCDELT.isDefined
    val wcsConvention_2 = recordCD.isDefined
    val wcsConvention_3 = recordCROTA.isDefined && recordCDELT.isDefined

    val isCompleteDefinition =
      (wcsConvention_1 || wcsConvention_2 || wcsConvention_3) &&
      recordWCSAXES.isDefined &&
      recordCTYPE.isDefined   &&
      recordCUNIT.isDefined   &&
      recordCRPIX.isDefined   &&
      recordCRVAL.isDefined

    (true,isCompleteDefinition)
  }
  //---------------------------------------------------------------------------
  private def checkIncompatibilityWithPC(otherRecord: RecordVerifierDoubleIndexTrait
                                         , recordPC: RecordVerifierPC
                                         , ctxt: FitsCtxt): Boolean = {
    if(!otherRecord.getPositionSeq.isEmpty &&
       !recordPC.getPositionSeq.isEmpty) {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_8_1
        , s"Structure: ${ctxt.structureID} keyword '${otherRecord.getImageKeywordWithSuffix()}' can not be at the same time that PCi_j")
      return false
    }
    true
  }
  //---------------------------------------------------------------------------
  def getAlternativeWcsSuffix(recordMap: RecordMap): Array[String] = {
    val keywordSeq = recordMap.getAllKeywordWithPrefix(KEYWORD_WCSAXES).sorted
    if (keywordSeq.isEmpty) Array(PRIMARY_WCS_SUFFIX)
    else {
      keywordSeq.map { keyword =>
        keyword.replace(KEYWORD_WCSAXES, "")
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class WcsPrimaryAndAlternativeAXES(wcsSuffix:String) extends RecordVerifierSingleIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_WCSAXES
  val dataType     = RecordVerifierDataTypeInteger()
  val isIndexed    = false
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , positionSeq: ArrayBuffer[Long]
             , ctxt: FitsCtxt): Boolean = {

    val value = recordMap.getWcsAxes(wcsSuffix)
    val naxis = recordMap.getNaxis()
    if (isPrimaryWcs && value < naxis) {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_8_2
        , s"Structure: ${ctxt.structureID} keyword '$getKeyword' with value '$value' must be equal or greater than NAXIS: '$naxis'")
      return false
    }

    val position =
      if (recordMap.contains(getKeyword)) recordMap.getFirstRecord(getKeyword).get.id
      else RECORD_POSITION_NOT_PRESENT_ID

    valueStringSeq += value.toString
    positionSeq += position
    true
  }
  //---------------------------------------------------------------------------
  def veryPosition(recordMap: RecordMap
                   , sortedPositionSeq: Array[Long]
                   , ctxt: FitsCtxt): Boolean = {

    if (sortedPositionSeq.head == RECORD_POSITION_NOT_PRESENT_ID) return true
    val naxisPosition = recordMap.getFirstRecord(KEYWORD_NAXIS).get.id

    //check NAXIS position
    if (naxisPosition >= sortedPositionSeq.head) {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_8_2
        , s"Structure: ${ctxt.structureID} keyword '$getKeyword' must be after NAXIS keyword")
    }

    //check WCSAXES position
    val wcsAxesPosition = recordMap.getFirstRecord(KEYWORD_WCSAXES).get.id
    if (wcsAxesPosition >= sortedPositionSeq.filter(_ != wcsAxesPosition).sorted.head) {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_8_2
        , s"Structure: ${ctxt.structureID} keyword '$getKeyword' must precede all WCS keywords ")
    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierPC(wcsSuffix:String) extends RecordVerifierDoubleIndexSingularMatrixTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_PC
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , positionSeq: ArrayBuffer[Long]
             , ctxt: FitsCtxt): Boolean =
    super.verify(
        recordMap
      , positionSeq
      , SectionName.SECTION_8_2
      , ctxt)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierCDELT(wcsSuffix:String) extends RecordVerifierSingleIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_CDELT
  val dataType     = RecordVerifierDataTypeDouble()
  val isIndexed    = true
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , positionSeq: ArrayBuffer[Long]
             , ctxt: FitsCtxt): Boolean = {

    if (!super.verify(recordMap
                     , maxIndex = recordMap.getWcsAxes(wcsSuffix)
                     , SectionName.SECTION_8_2
                     , ctxt)) return false

    if (valueStringSeq.isEmpty) return true
    val valueSeq = getValueSeqAsDoubleSeq
    if (!valueSeq.forall( _ != 0D)) {
      DisconfFatal(
        ctxt
        , SectionName.SECTION_8_2
        , s"Structure: ${ctxt.structureID} keyword '$getKeyword' has a zero value: '${valueSeq.mkString("{",",","}")}'")
      return false
    }
    positionSeq ++= getPositionSeq
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierCD(wcsSuffix:String) extends RecordVerifierDoubleIndexSingularMatrixTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_CD
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , positionSeq: ArrayBuffer[Long]
             , ctxt: FitsCtxt): Boolean =
    super.verify(
        recordMap
      , positionSeq
      , SectionName.SECTION_8_2
      , ctxt)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierCROTA(wcsSuffix:String) extends RecordVerifierDoubleIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_CROTA
  val dataType     = RecordVerifierDataTypeDouble()
  val isIndexed    = true
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , positionSeq: ArrayBuffer[Long]
             , ctxt: FitsCtxt): Boolean = {
    if (!super.verify(
        recordMap
      , maxIndex = recordMap.getWcsAxes(wcsSuffix)
      , SectionName.SECTION_8_2
      , ctxt)) return false

    if (valueStringSeq.isEmpty) return true
    positionSeq ++= getPositionSeq
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
object RecordVerifierCTYPE {
  //---------------------------------------------------------------------------
  //Table 23
  final val CELESTIAL_COORDINATE_ALGORITHM_CODE_MAP = Map(
      //name-> (phi,theta) = (long,lat)
      //Zenithal (azimuthal) projections
        "AZP" -> ((0, 90))
      , "SZP" -> ((0, 90))
      , "TAN" -> ((0, 90))
      , "STG" -> ((0, 90))
      , "SIN" -> ((0, 90))
      , "ARC" -> ((0, 90))
      , "ZPN" -> ((0, 90))
      , "ZEA" -> ((0, 90))
      , "AIR" -> ((0, 90))

      //Cylindrical projections
      , "CYP" -> ((0, 0))
      , "CEA" -> ((0, 0))
      , "CAR" -> ((0, 0))
      , "MER" -> ((0, 0))

      //Pseudo-cylindrical and related projections
      , "SFL" -> ((0, 0))
      , "PAR" -> ((0, 0))
      , "MOL" -> ((0, 0))
      , "AIT" -> ((0, 0))

      //Conic projections
      , "COP" -> ((0, 0))  //(0,θa) in the table
      , "COE" -> ((0, 0))  //(0,θa) in the table
      , "COD" -> ((0, 0))  //(0,θa) in the table
      , "COO" -> ((0, 0))  //(0,θa) in the table

      //Polyconic and pseudoconic projections
      , "BON" -> ((0, 0))
      , "PCO" -> ((0, 0))

      //Quad-cube projections
      , "TSC" -> ((0, 0))
      , "CSC" -> ((0, 0))
      , "QSC" -> ((0, 0))

      //HEALPix grid projection
      , "HPX" -> ((0, 0))
      )

  //---------------------------------------------------------------------------
  //Table 25
  final val SPECTRAL_COORDINATE_CODE_MAP = Map(
      "FREQ" -> ""
    , "ENER" -> ""
    , "WAVN" -> ""
    , "VRAD" -> ""
    , "WAVE" -> ""
    , "VOPT" -> ""
    , "ZOPT" -> ""
    , "AWAV" -> ""
    , "VELO" -> ""
    , "BERA" -> ""
  )
  //---------------------------------------------------------------------------
  //Table 26
  final val SPECTRAL_NON_LINEAR_ALGORITHM_CODE_MAP = Map(
    //Frequency
      "F2W" -> ""
    , "F2V" -> ""
    , "F2A" -> ""

    //Wavelength
    , "W2F" -> ""
    , "W2V" -> ""
    , "W2A" -> ""

    //Apparent radial vel.
    , "V2F" -> ""
    , "V2W" -> ""
    , "V2A" -> ""

    //Air wavelength
    , "A2F" -> ""
    , "A2W" -> ""
    , "A2V" -> ""

    , "LOG" -> ""  //Logarithm
    , "GRI" -> ""  //Detector
    , "GRA" -> ""  //Detector
    , "TAB" -> ""  //Not regular
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierCTYPE(wcsSuffix:String) extends RecordVerifierSingleIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase = KEYWORD_CTYPE
  val dataType    = RecordVerifierDataTypeString()
  val isIndexed   = true
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , positionSeq: ArrayBuffer[Long]
             , ctxt: FitsCtxt): Boolean = {

    if (!super.verify(
      recordMap
      , maxIndex = recordMap.getWcsAxes(wcsSuffix)
      , SectionName.SECTION_8_2
      , ctxt: FitsCtxt)) return false

    if (valueStringSeq.isEmpty) return true
    valueStringSeq.zipWithIndex.foreach { case (ctype, i) =>
      var knownCTYPE = false
      if (ctype.contains("-")) { //non linear transformation
        val cTypeValue = CtypeParser.parseInput(ctype)
        if (cTypeValue.isDefined) {

          val coordinateType = cTypeValue.get.coordinateType
          val algorithmCode = cTypeValue.get.algorithmCode
          if (SPECTRAL_COORDINATE_CODE_MAP.contains(coordinateType)) { //check is axis is of spectral type
            if (SPECTRAL_NON_LINEAR_ALGORITHM_CODE_MAP.contains(algorithmCode)) knownCTYPE = true
          }
          else {  //axis is not of type spectral
            if (CELESTIAL_COORDINATE_ALGORITHM_CODE_MAP.contains(algorithmCode)) knownCTYPE = true
          }
        }
        if (!knownCTYPE) {
          DisconfFatal(
            ctxt
            , SectionName.SECTION_8_2
            , s"Structure: ${ctxt.structureID} keyword '$getKeyword${i+1}' with value '$ctype' is not a valid algorithm code for celestial coordinates")
        }
      }
    }
    positionSeq ++= getPositionSeq
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierCUNIT(wcsSuffix:String) extends RecordVerifierSingleIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase = KEYWORD_CUNIT
  val dataType    = RecordVerifierDataTypeString()
  val isIndexed   = true
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , positionSeq: ArrayBuffer[Long]
             , ctxt: FitsCtxt): Boolean = {

    if (!super.verify(
      recordMap
      , maxIndex = recordMap.getWcsAxes(wcsSuffix)
      , SectionName.SECTION_8_2
      , ctxt: FitsCtxt)) return false

    if (valueStringSeq.isEmpty) return true
    if (wcsSuffix == WcsPrimaryAndAlternative.PRIMARY_WCS_SUFFIX) {
      valueStringSeq.zipWithIndex.foreach { case (unitName, i) =>
        if (unitName != "deg") {
          DisconfFatal(
            ctxt
            , SectionName.SECTION_8_2
            , s"Structure: ${ctxt.structureID} keyword '$getKeyword${i+1}' with value '$unitName'. Unit must be degrees: 'deg'")
          true
        }
      }
    }
    positionSeq ++= getPositionSeq
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierCRPIX(wcsSuffix:String) extends RecordVerifierSingleIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase = KEYWORD_CRPIX
  val dataType    = RecordVerifierDataTypeDouble()
  val isIndexed   = true
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , positionSeq: ArrayBuffer[Long]
             , ctxt: FitsCtxt): Boolean = {

    if (!super.verify(
      recordMap
      , maxIndex = recordMap.getWcsAxes(wcsSuffix)
      , SectionName.SECTION_8_2
      , ctxt: FitsCtxt)) return false

    if (valueStringSeq.isEmpty) return true
    positionSeq ++= getPositionSeq
    true
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
case class RecordVerifierCRVAL(wcsSuffix:String) extends RecordVerifierSingleIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase = KEYWORD_CRVAL
  val dataType    = RecordVerifierDataTypeDouble()
  val isIndexed   = true
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , positionSeq: ArrayBuffer[Long]
             , ctxt: FitsCtxt): Boolean = {

    if (!super.verify(
      recordMap
      , maxIndex = recordMap.getWcsAxes(wcsSuffix)
      , SectionName.SECTION_8_2
      , ctxt: FitsCtxt)) return false

    if (valueStringSeq.isEmpty) return true
    positionSeq ++= getPositionSeq
    true
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
case class RecordVerifierPV(wcsSuffix:String) extends RecordVerifierDoubleIndexWithParameterTrait {
  //---------------------------------------------------------------------------
  val keywordBase = KEYWORD_PV
  val dataType    = RecordVerifierDataTypeDouble()
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , positionSeq: ArrayBuffer[Long]
             , ctxt: FitsCtxt): Boolean =
    super.verify(
        recordMap
      , positionSeq
      , SectionName.SECTION_8_2
      , ctxt)
  //---------------------------------------------------------------------------
}

//=============================================================================
case class RecordVerifierPS(wcsSuffix:String) extends RecordVerifierDoubleIndexWithParameterTrait {
  //---------------------------------------------------------------------------
  val keywordBase = KEYWORD_PS
  val dataType    = RecordVerifierDataTypeString()
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , positionSeq: ArrayBuffer[Long]
             , ctxt: FitsCtxt): Boolean =
    super.verify(
        recordMap
      , positionSeq
      , SectionName.SECTION_8_2
      , ctxt)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierCRDER(wcsSuffix:String) extends RecordVerifierSingleIndexNonNegativeTrait {
  //---------------------------------------------------------------------------
  val keywordBase = KEYWORD_CRDER
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , positionSeq: ArrayBuffer[Long]
             , ctxt: FitsCtxt): Boolean =
    super.verify(recordMap
                 , positionSeq
                 , SectionName.SECTION_8_2
                 , ctxt: FitsCtxt)
  //---------------------------------------------------------------------------
}

//=============================================================================
case class RecordVerifierCSYER(wcsSuffix:String) extends RecordVerifierSingleIndexNonNegativeTrait {
  //---------------------------------------------------------------------------
  val keywordBase = KEYWORD_CSYER
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , positionSeq: ArrayBuffer[Long]
             , ctxt: FitsCtxt): Boolean =
    super.verify(recordMap
                 , positionSeq
                 , SectionName.SECTION_8_2
                 , ctxt: FitsCtxt)
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file WcsPrimaryAndAlternative.scala
//=============================================================================