/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  02/Nov/2022
  * Time:  20h:34m
  * Description: None
  */
//=============================================================================
package com.common.fits.standard.block.record.verifier.section_8.section_8_2
//=============================================================================
import com.common.logger.MyLogger
import fastparse.NoWhitespace._
import fastparse._
//=============================================================================
case class CTYPE_Value(coordinateType: String, algorithmCode: String)
//-----------------------------------------------------------------------------
object CtypeParser extends MyLogger {
  //---------------------------------------------------------------------------
  private def validChar[_:P] = P(CharIn("a-z")) | P(CharIn("A-Z")) | P(CharIn("0-9"))
  //---------------------------------------------------------------------------
  private def hyphen[_:P] = P("-")
  //---------------------------------------------------------------------------
  private def space[_: P] = P(" ")
  //---------------------------------------------------------------------------
  private def coordinateType[_: P] =
    (validChar.rep(exactly = 1) ~ P(hyphen.rep(exactly = 3)) |
     validChar.rep(exactly = 2) ~ P(hyphen.rep(exactly = 2)) |
     validChar.rep(exactly = 3) ~ P(hyphen.rep(exactly = 1)) |
     validChar.rep(exactly = 4)).!.map(s =>s)
  //---------------------------------------------------------------------------
  private def algorithmCode[_: P] =
    (validChar.rep(exactly = 1) ~ P(space.rep(exactly = 2)) |
     validChar.rep(exactly = 2) ~ P(space.rep(exactly = 1)) |
     validChar.rep(exactly = 3)).!.map(s =>s)
  //---------------------------------------------------------------------------
  private def nonLinear[_: P] = (coordinateType ~ hyphen ~ algorithmCode).map { t=>
    CTYPE_Value(t._1, t._2)
  }
  //---------------------------------------------------------------------------
  def parseInput(s: String): Option[CTYPE_Value] =
    parse(s, nonLinear(_)) match {
      case Parsed.Success(r, index) =>
        if (index == s.size) Some(r)
        else None
      case _ => None
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CtypeParser.scala
//=============================================================================