/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  31/Aug/2023
  * Time:  10h:35m
  * Description: None
  */
//=============================================================================
package com.common.fits.standard.block.record.verifier.section_8
//=============================================================================
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.block.record.verifier.section_8.section_8_2.{Section_8_3, Section_8_4, Section_8_5, WcsPrimaryAndAlternative}
import com.common.fits.standard.fits.FitsCtxt
//=============================================================================
//=============================================================================
object Section_8 {
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , ctxt: FitsCtxt): Boolean = {
    if (!WcsPrimaryAndAlternative.verify(recordMap, ctxt)) return false
    if (!Section_8_3.verify(recordMap, ctxt)) return false
    if (!Section_8_4.verify(recordMap, ctxt)) return false
    if (!Section_8_5.verify(recordMap, ctxt)) return false
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Section_8.scala
//=============================================================================