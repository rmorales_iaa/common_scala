/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  08/Sep/2023
  * Time:  11h:09m
  * Description: None
  */
//=============================================================================
package com.common.fits.standard.block.record.verifier.section_8.section_8_2
//=============================================================================
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.sectionName.SectionName
//=============================================================================
//=============================================================================
object Section_8_5 {
  //---------------------------------------------------------------------------
  val sectionName = SectionName.SECTION_8_5
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , ctxt: FitsCtxt): Boolean = {
    //no verification is required
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Section_8_5.scala
//=============================================================================