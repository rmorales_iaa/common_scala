/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  08/Sep/2023
  * Time:  11h:09m
  * Description: None
  */
//=============================================================================
package com.common.fits.standard.block.record.verifier.section_8.section_8_2
//=============================================================================
import com.common.fits.standard.Keyword._
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.block.record.verifier.RecordVerifierDataTypeString
import com.common.fits.standard.block.record.verifier.common.doubleIndex.RecordVerifierDoubleIndexWithParameterTrait
import com.common.fits.standard.block.record.verifier.common.noIndex.{RecordVerifierDateNoIndexTrait, RecordVerifierDoubleNoIndexTrait, RecordVerifierStringNoIndexTrait}
import com.common.fits.standard.block.record.verifier.section_8.section_8_2.WcsPrimaryAndAlternative.getAlternativeWcsSuffix
import com.common.fits.standard.disconformity.DisconfFatal
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.sectionName.SectionName
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object Section_8_4 {
  //---------------------------------------------------------------------------
  val sectionName = SectionName.SECTION_8_4
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , ctxt: FitsCtxt): Boolean = {

    val wcsSuffixSeq = getAlternativeWcsSuffix(recordMap)
    val positionSeq = ArrayBuffer[Long]()
    wcsSuffixSeq.foreach { wcsSuffix =>
      if (!RecordVerifierCNAME(wcsSuffix).verify(recordMap, positionSeq, ctxt)) return false
      if (!RecordVerifierRESTFRQ(wcsSuffix).verify(recordMap, sectionName, ctxt)) return false
      if (!RecordVerifierRESTWAV(wcsSuffix).verify(recordMap, sectionName, ctxt)) return false

      if (!RecordVerifierDATE_AVG(wcsSuffix).verify(recordMap, sectionName, ctxt)) return false
      if (!RecordVerifierMJD_AVG(wcsSuffix).verify(recordMap, sectionName, ctxt)) return false
      if (!RecordVerifierSPECSYS(wcsSuffix).verify(recordMap, sectionName, ctxt)) return false
      if (!RecordVerifierSSYSOBS(wcsSuffix).verify(recordMap, sectionName, ctxt)) return false
      if (!RecordVerifierOBSGEO_X().verify(recordMap, sectionName, ctxt)) return false
      if (!RecordVerifierOBSGEO_Y().verify(recordMap, sectionName, ctxt)) return false
      if (!RecordVerifierOBSGEO_Z().verify(recordMap, sectionName, ctxt)) return false
      if (!RecordVerifierSSYSSRC(wcsSuffix).verify(recordMap, sectionName, ctxt)) return false
      if (!RecordVerifierVELOSYS(wcsSuffix).verify(recordMap, sectionName, ctxt)) return false
      if (!RecordVerifierZSOURCE(wcsSuffix).verify(recordMap, sectionName, ctxt)) return false
      if (!RecordVerifierVELANGL(wcsSuffix).verify(recordMap, sectionName, ctxt)) return false
    }
    true
  }

  //=============================================================================
  case class RecordVerifierCNAME(wcsSuffix: String) extends RecordVerifierDoubleIndexWithParameterTrait {
    //---------------------------------------------------------------------------
    val keywordBase = KEYWORD_CNAME
    val dataType = RecordVerifierDataTypeString()
    //---------------------------------------------------------------------------
    def verify(recordMap: RecordMap
               , positionSeq: ArrayBuffer[Long]
               , ctxt: FitsCtxt): Boolean =
      super.verify(
        recordMap
        , positionSeq
        , sectionName
        , ctxt)
    //---------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierRESTFRQ(wcsSuffix:String) extends RecordVerifierDoubleNoIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_RESTFRQ

   //There is no way to check unit. It indicates that must be 'Hz' but this
  // keyword has a WCS suffix not an axis index as is required in CUNIT
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierRESTWAV(wcsSuffix:String) extends RecordVerifierDoubleNoIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_RESTWAV
  //There is no way to check unit. It indicates that must be 'm' but this
  // keyword has a WCS suffix not an axis index as is required in CUNIT
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierDATE_AVG(wcsSuffix:String) extends RecordVerifierDateNoIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_DATE_AVG
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierMJD_AVG(wcsSuffix:String) extends RecordVerifierDoubleNoIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_MJD_AVG
  //---------------------------------------------------------------------------
}
//=============================================================================
object RecordVerifierSPECSYS {
  //---------------------------------------------------------------------------
  //table 27
  final val SPECTRAL_REFERENCE_FRAME_MAP = Map (
   "TOPOCENT"  -> "Topocentric"
  , "GEOCENTR" -> "Geocentric"
  , "BARYCENT" -> "Barycentric"
  , "HELIOCEN" -> "Heliocentric"
  , "LSRK"     -> "Local standard of rest (kinematic)"
  , "LSRD"     -> "Local standard of rest (dynamic)"
  , "GALACTOC" -> "Galactocentric"
  , "LOCALGRP" -> "Local Group"
  , "CMBDIPOL" -> "Cosmic-microwave-background dipole"
  , "SOURCE"   -> "Source rest frame"
  )
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
import RecordVerifierSPECSYS._
case class RecordVerifierSPECSYS(wcsSuffix:String) extends RecordVerifierStringNoIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_SPECSYS
  //---------------------------------------------------------------------------
  override def verify(recordMap: RecordMap
                      , sectionName: String
                      , ctxt: FitsCtxt): Boolean = {

    if (!recordMap.contains(getKeyword)) return true
    if (!super.verify(recordMap, sectionName, ctxt)) return false
    val record = recordMap.getFirstRecord(getKeyword).get
    if (!SPECTRAL_REFERENCE_FRAME_MAP.contains(record.value.toString)) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '${record.value.toString}' is not a valid spectral reference system")
    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================

case class RecordVerifierSSYSOBS(wcsSuffix:String) extends RecordVerifierStringNoIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_SSYSOBS
  //---------------------------------------------------------------------------
  override def verify(recordMap: RecordMap
                      , sectionName: String
                      , ctxt: FitsCtxt): Boolean = {

    if (!recordMap.contains(getKeyword)) return true
    if (!super.verify(recordMap, sectionName, ctxt)) return false
    val record = recordMap.getFirstRecord(getKeyword).get
    if (!SPECTRAL_REFERENCE_FRAME_MAP.contains(record.value.toString)) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '${record.value.toString}' is not a valid spectral reference system")
    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierOBSGEO_X() extends RecordVerifierDoubleNoIndexTrait {
  //---------------------------------------------------------------------------
  val wcsSuffix = ""
  val keywordBase  = KEYWORD_OBSGEO_X
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierOBSGEO_Y() extends RecordVerifierDoubleNoIndexTrait {
  //---------------------------------------------------------------------------
  val wcsSuffix = ""
  val keywordBase  = KEYWORD_OBSGEO_Y
  //---------------------------------------------------------------------------
}

//=============================================================================
case class RecordVerifierOBSGEO_Z() extends RecordVerifierDoubleNoIndexTrait {
  //---------------------------------------------------------------------------
  val wcsSuffix = ""
  val keywordBase  = KEYWORD_OBSGEO_Z
  //---------------------------------------------------------------------------
}

//=============================================================================
case class RecordVerifierSSYSSRC(wcsSuffix:String) extends RecordVerifierStringNoIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_SSYSSRC
  //---------------------------------------------------------------------------
  override def verify(recordMap: RecordMap
                      , sectionName: String
                      , ctxt: FitsCtxt): Boolean = {

    if (!recordMap.contains(getKeyword)) return true
    if (!super.verify(recordMap, sectionName, ctxt)) return false
    val record = recordMap.getFirstRecord(getKeyword).get
    val value = record.value.toString
    if (!SPECTRAL_REFERENCE_FRAME_MAP.contains(value) ||
        value == "SOURCE") {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '${record.value.toString}' is not a valid spectral reference system")
    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierVELOSYS(wcsSuffix:String) extends RecordVerifierDoubleNoIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_VELOSYS
  //---------------------------------------------------------------------------
}

//=============================================================================
case class RecordVerifierZSOURCE(wcsSuffix:String) extends RecordVerifierDoubleNoIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_ZSOURCE
  //---------------------------------------------------------------------------
}

//=============================================================================
case class RecordVerifierVELANGL(wcsSuffix:String) extends RecordVerifierDoubleNoIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase  = KEYWORD_VELANGL
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file Section_8_4.scala
//=============================================================================