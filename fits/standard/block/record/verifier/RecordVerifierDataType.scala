/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  31/Aug/2023
  * Time:  11h:24m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier
//=============================================================================
//=============================================================================
import com.common.fits.standard.block.record.RecordCharacter.RecordString
import com.common.fits.standard.block.record.RecordNumber.{RecordFloat, RecordInteger, RecordLogical}
import com.common.fits.standard.block.record.Record
//=============================================================================
object RecordVerifierDataType {
  //---------------------------------------------------------------------------
  final val DATA_TYPE_LOGICAL_NAME  = "logical"
  final val DATA_TYPE_STRING_NAME   = "string"
  final val DATA_TYPE_INTEGER_NAME  = "integer"
  final val DATA_TYPE_FLOAT_NAME    = "float"
  //---------------------------------------------------------------------------
}
//=============================================================================
import RecordVerifierDataType._
//=============================================================================
case class RecordVerifierDataTypeLogical() extends RecordVerifierDataType {
  //---------------------------------------------------------------------------
  val name          = DATA_TYPE_LOGICAL_NAME
  //---------------------------------------------------------------------------
  def isValid(record: Record[_]): Boolean = {
    record match {
      case _: RecordLogical => true
      case _                => false
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierDataTypeString() extends RecordVerifierDataType {
  //---------------------------------------------------------------------------
  val name          = DATA_TYPE_STRING_NAME
  //---------------------------------------------------------------------------
  def isValid(record: Record[_]): Boolean = {
    record match {
      case _: RecordString => true
      case _               => false
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierDataTypeInteger() extends RecordVerifierDataType {
  //---------------------------------------------------------------------------
   val name          = DATA_TYPE_INTEGER_NAME
  //---------------------------------------------------------------------------
  def isValid(record: Record[_]): Boolean = {
    record match {
      case _: RecordInteger => true
      case _                => false
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordVerifierDataTypeDouble() extends RecordVerifierDataType {
  //---------------------------------------------------------------------------
  val name          = DATA_TYPE_FLOAT_NAME
  //---------------------------------------------------------------------------
  def isValid(record: Record[_]): Boolean = {
    record match {
      case _: RecordFloat => true
      case _: RecordInteger => true
      case _              => false
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
trait RecordVerifierDataType {
  //---------------------------------------------------------------------------
  val name:String
  //---------------------------------------------------------------------------
  def isValid(record: Record[_]): Boolean
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RecordVerifierDataType.scala
//=============================================================================