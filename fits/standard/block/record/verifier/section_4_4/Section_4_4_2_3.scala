/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  11/Sep/2023
  * Time:  10h:09m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.section_4_4
//=============================================================================
import com.common.fits.standard.Keyword.{KEYWORD_AUTHOR, KEYWORD_REFERENC}
import com.common.fits.standard.block.record.verifier.StringRecordVerifier
import com.common.fits.standard.sectionName.SectionName
//=============================================================================
//=============================================================================
object Section_4_4_2_3 {
  //---------------------------------------------------------------------------
  private val sectionName = SectionName.SECTION_4_4_2_3
  //---------------------------------------------------------------------------
  private val recordVerifierSequence = Seq(
      StringRecordVerifier(KEYWORD_AUTHOR, sectionName)
    , StringRecordVerifier(KEYWORD_REFERENC, sectionName)
  )
  //---------------------------------------------------------------------------
  def getMapEntrySeq() = recordVerifierSequence.map { r => r.keyword -> r }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Section_4_4_2_3.scala
//=============================================================================