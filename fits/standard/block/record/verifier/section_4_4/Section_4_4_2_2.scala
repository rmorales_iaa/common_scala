/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  11/Sep/2023
  * Time:  10h:09m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.section_4_4
//=============================================================================
import com.common.fits.standard.Keyword.{KEYWORD_DATE_OBS, KEYWORD_INSTRUME, KEYWORD_OBJECT, KEYWORD_OBSERVER, KEYWORD_TELESCOP}
import com.common.fits.standard.block.record.verifier.section_4_4.Section_4_4_2_1.extraCheckDATE
import com.common.fits.standard.block.record.verifier.StringRecordVerifier
import com.common.fits.standard.sectionName.SectionName
//=============================================================================
//=============================================================================
object Section_4_4_2_2 {
  //---------------------------------------------------------------------------
  private val sectionName = SectionName.SECTION_4_4_2_2
  //---------------------------------------------------------------------------
  private val recordVerifierSequence =  Seq(
      StringRecordVerifier(KEYWORD_DATE_OBS,  sectionName, Some(extraCheckDATE))
    , StringRecordVerifier(KEYWORD_TELESCOP,  sectionName)
    , StringRecordVerifier(KEYWORD_INSTRUME,  sectionName)
    , StringRecordVerifier(KEYWORD_OBSERVER,  sectionName)
    , StringRecordVerifier(KEYWORD_OBJECT,    sectionName)
  )
  //---------------------------------------------------------------------------
  def getMapEntrySeq() = recordVerifierSequence.map { r => r.keyword -> r }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Section_4_4_2_2.scala
//=============================================================================