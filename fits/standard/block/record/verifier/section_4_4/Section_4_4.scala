//=============================================================================
/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  08/Sep/2023
  * Time:  11h:08m
  * Description: None
  */
//=============================================================================
package com.common.fits.standard.block.record.verifier.section_4_4
//=============================================================================
import com.common.fits.standard.block.record.{RecordMap}
import com.common.fits.standard.fits.FitsCtxt
//=============================================================================
//=============================================================================
object Section_4_4 {
  //---------------------------------------------------------------------------
  private final val recordVerifierMap =
    (Section_4_4_2_1.getMapEntrySeq() ++    //General descriptive keywords
      Section_4_4_2_2.getMapEntrySeq() ++   //Keywords describing observations
      Section_4_4_2_3.getMapEntrySeq() ++   //Bibliographic keywords
      //Section_4_4_2_4.getMapEntrySeq() ++ //Commentary keywords. It will checked when parsing header
      Section_4_4_2_5.getMapEntrySeq() ++   //Keywords that describe arrays
      Section_4_4_2_6.getMapEntrySeq() ++   //Extension keywords
      Section_4_4_2_7.getMapEntrySeq()      //Data-integrity keywords
    ) .toMap
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , ctxt: FitsCtxt): Boolean = {
    recordVerifierMap.foreach { case (keyword, recordVerifier) =>
      if (recordMap.contains(keyword))
        if (!recordVerifier.verify(recordMap,ctxt)) return false
    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Section_4_4.scala
//=============================================================================