//=============================================================================
/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  08/Sep/2023
  * Time:  11h:08m
  * Description: None
  */
//=============================================================================
package com.common.fits.standard.block.record.verifier.section_4_4
//=============================================================================
import com.common.fits.standard.Keyword._
import com.common.fits.standard.block.record.{Record, RecordMap}
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.block.record.verifier.{DoubleRecordVerifier, IntegerRecordVerifier, StringRecordVerifier}
import com.common.fits.standard.disconformity.{DisconfFatal}
import com.common.fits.standard.structure.hdu.HDU
import com.common.fits.standard.structure.hdu.extension.Extension
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.unit.parser.ParserPhysicalUnit
//=============================================================================
//=============================================================================
object Section_4_4_2_5 {
  //---------------------------------------------------------------------------
  private val sectionName = SectionName.SECTION_4_4_2_5
  //---------------------------------------------------------------------------
  private val recordVerifierBSCALE = DoubleRecordVerifier(KEYWORD_BSCALE, sectionName)
  private val recordVerifierBZERO  = DoubleRecordVerifier(KEYWORD_BZERO, sectionName)
  //---------------------------------------------------------------------------
  private val recordVerifierSequence =  Seq(
      recordVerifierBSCALE
    , recordVerifierBZERO
    , StringRecordVerifier(KEYWORD_BUNIT, sectionName, Some(extraCheckBUNIT))
    , IntegerRecordVerifier(KEYWORD_BLANK, sectionName, Some(extraCheckBLANK))
    , DoubleRecordVerifier(KEYWORD_DATAMAX, sectionName)
    , DoubleRecordVerifier(KEYWORD_DATAMIN, sectionName)
  )
  //for WCS checking, see class 'RecordVerifierWcs'
  //---------------------------------------------------------------------------
  def getMapEntrySeq() = recordVerifierSequence.map { r => r.keyword -> r }
  //---------------------------------------------------------------------------
  private def extraCheckBUNIT(recordMap: RecordMap
                              , record: Record[_]
                              , sectionName: String
                              , ctxt: FitsCtxt): Boolean = {
    if (ParserPhysicalUnit.parseInput(record.value.toString)._1.isEmpty) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '${record.value.toString}' is not a valid physical unit")
      true
    }
    else true
  }
  //---------------------------------------------------------------------------
  private def extraCheckBLANK(recordMap: RecordMap
                              , record: Record[_]
                              , sectionName: String
                              , ctxt: FitsCtxt): Boolean = {

    if (!HDU.isValidRecordBitPix(recordMap, ctxt)) return false
    if (!recordVerifierBSCALE.verify(recordMap, ctxt)) return false
    if (!recordVerifierBZERO.verify(recordMap, ctxt)) return false

    val bitPix = recordMap.getBitPix()
    if (bitPix < 0) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '${record.value.toString}' it can not be used when BITPIX is negative. Current value: '$bitPix'")
    }
    //check default values
    Extension.isValidBlankCommon(recordMap
                                      , ctxt
                                      , KEYWORD_BLANK
                                      , KEYWORD_BSCALE
                                      , KEYWORD_BZERO
                                      , sectionName)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Section_4_4_2_5.scala
//=============================================================================