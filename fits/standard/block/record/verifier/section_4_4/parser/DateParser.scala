/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  02/Nov/2022
  * Time:  20h:34m
  * Description: See sections:
  * "4.4.2.1. General descriptive keywords"
  * ""9.1. Time values"
  */
//=============================================================================
package com.common.fits.standard.block.record.verifier.section_4_4.parser
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
import fastparse.NoWhitespace._
import fastparse._
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import scala.util.{Failure, Success, Try}
//=============================================================================
case class DateValue(year:Int
                     , month:Byte
                     , day:Byte
                     , stringValue: String)
//=============================================================================
case class TimeValue(hour: Byte
                     , minute: Byte
                     , seconds: Short
                     , fractionSeconds: Double
                     , stringValue: String)
//=============================================================================
object DateTimeValue {
  //---------------------------------------------------------------------------
  private final val dateBefore2000Jan01Pattern = DateTimeFormatter.ofPattern("dd/MM/yyyy")
  //---------------------------------------------------------------------------
}
//=============================================================================
import DateTimeValue._
case class DateTimeValue(date:DateValue
                         , time: TimeValue
                         , hasDateFormatBefore2000Jan01: Boolean = false) {
  //---------------------------------------------------------------------------
  def getAsLocalDateTimeString() = date.stringValue + "T" + time.stringValue
  //---------------------------------------------------------------------------
  def getAsLocalDateString() = date.stringValue
  //---------------------------------------------------------------------------
  def getLocalDateTime(): Option[LocalDateTime] = {
    //fix nano seconds
    val nanoSeconds = Math.round(time.fractionSeconds * 10e+8)
    var dateTimeAsString = getAsLocalDateTimeString()
    val startPosOfNanos = dateTimeAsString.indexOf('.')
    if (startPosOfNanos != -1) dateTimeAsString = dateTimeAsString.take(startPosOfNanos+1) + nanoSeconds
    val parsedDateTime: Try[LocalDateTime] = Try(LocalDateTime.parse(dateTimeAsString, DateTimeFormatter.ISO_LOCAL_DATE_TIME))
      parsedDateTime match {
        case Success(_) => Some(parsedDateTime.get)
        case Failure(_) => None
     }
  }

  //---------------------------------------------------------------------------
  def getLocalDate(): Option[LocalDate] = {
    val parsedDate: Try[LocalDate] = Try(LocalDate.parse(getAsLocalDateString(), dateBefore2000Jan01Pattern))
    parsedDate match {
      case Success(_) => Some(parsedDate.get)
      case Failure(_) => None
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//-----------------------------------------------------------------------------
object DateParser extends MyLogger {
  //---------------------------------------------------------------------------
  private def sign[_: P] = P("+" | "-")
  //---------------------------------------------------------------------------
  private def validDigit[_:P] = P(CharIn("0-9"))
  //---------------------------------------------------------------------------
  private def dividerMinus[_:P] = P("-")
  //---------------------------------------------------------------------------
  private def dividerT[_:P] = P("T")
  //---------------------------------------------------------------------------
  private def dividerTwoPoints[_: P] = P(":")
  //---------------------------------------------------------------------------
  private def dividerDot[_: P] = P(".")
  //---------------------------------------------------------------------------
  private def dividerSlash[_: P] = P("/")
  //---------------------------------------------------------------------------
  private def year[_: P] = (sign.! ~ validDigit.rep(exactly = 5).!).map { case (sign, s) => sign + s } |
                            validDigit.rep(exactly = 4).!.map(s => s)
  //---------------------------------------------------------------------------
  private def dateAfter2000Jan01[_: P] =
    year ~ dividerMinus ~
    validDigit.rep(exactly = 2).! ~ dividerMinus ~
    validDigit.rep(exactly = 2).!
  //---------------------------------------------------------------------------
  private def dateBefore2000Jan01[_: P] =
    validDigit.rep(exactly = 2).! ~ dividerSlash ~
    validDigit.rep(exactly = 2).! ~ dividerSlash ~
    validDigit.rep(exactly = 2).!
  //---------------------------------------------------------------------------
  private def time[_: P] =
    dividerT ~
    validDigit.rep(exactly = 2).! ~ dividerTwoPoints ~
    validDigit.rep(exactly = 2).! ~ dividerTwoPoints ~
    validDigit.rep(exactly = 2).! ~
    (dividerDot ~ validDigit.rep).!.?
  //---------------------------------------------------------------------------
  private def dateTimeAfterYear2000Jan01[_: P]  =
    (dateAfter2000Jan01 ~ time.?).map { case (_year,month,day,time) =>
      val dateStringValue = _year + "-" +
                            month + "-" +
                            day
      val year = if (_year.startsWith("-")) _year.drop(1).toInt * -1 else _year.toInt
      val dateValue = DateValue(year.toShort, month.toByte, day.toByte, dateStringValue)
      val timeValue =
        if (time.isDefined) {
          val timeStringValue = time.get._1 + ":" +
                                time.get._2 + ":" +
                                time.get._3 +
                                time.get._4.getOrElse("")
          val fractionSeconds = if(time.get._4.isEmpty) 0.0d else time.get._4.get.toDouble
          TimeValue(time.get._1.toByte
                   , time.get._2.toByte
                   , time.get._3.toByte
                   , fractionSeconds
                   , timeStringValue)
        }
        else {
          val timeStringValue = "00:" +
                                "00:" +
                                "00"
          TimeValue(0.toByte,0.toByte,0.toByte,0d,timeStringValue)
        }
      DateTimeValue(dateValue, timeValue)
    }
  //---------------------------------------------------------------------------
  private def dateTimeBefore2000Jan01[_: P] =
      dateBefore2000Jan01.map { case (day,month,year) =>

        val adjustedYear = year.toShort + 1900
        val dateStringValue = day + "/" +
                              month + "/" +
                              adjustedYear.toString
        val timeStringValue = "00:" +
                              "00:" +
                              "00"

        val dateValue = DateValue(adjustedYear.toShort, month.toByte, day.toByte, dateStringValue)
        DateTimeValue(dateValue
                    , TimeValue(0,0,0,0,timeStringValue)
                    , hasDateFormatBefore2000Jan01 = true)
    }
  //---------------------------------------------------------------------------
  private def dataTime[_: P] =  dateTimeAfterYear2000Jan01 | dateTimeBefore2000Jan01
  //---------------------------------------------------------------------------
  def parseInput(s: String): Option[DateTimeValue] =
    parse(s, dataTime(_)) match {
      case Parsed.Success(dateTimeValue, index) =>
        if (index == s.size) {

          if (!dateTimeValue.hasDateFormatBefore2000Jan01) {
            if (dateTimeValue.getLocalDateTime().isDefined) return Some(dateTimeValue)
          }
          else {
            if (dateTimeValue.getLocalDate().isDefined) return Some(dateTimeValue)
          }
          None
        }
        else None
      case _ => None
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file DateParser.scala
//=============================================================================