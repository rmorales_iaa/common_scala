//=============================================================================
/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  08/Sep/2023
  * Time:  11h:08m
  * Description: None
  */
//=============================================================================
package com.common.fits.standard.block.record.verifier.section_4_4
//=============================================================================
import com.common.fits.standard.Keyword._
import com.common.fits.standard.block.record.verifier.{StringRecordVerifier}
import com.common.fits.standard.block.record.{Record, RecordMap}
import com.common.fits.standard.disconformity.DisconfFatal
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.sectionName.SectionName
import com.common.util.util.Util
//=============================================================================
//=============================================================================
object Section_4_4_2_7 {
  //---------------------------------------------------------------------------
  private val MAX_VALUE_32_BIT_INTEGER = 4294967295L //(2^32) - 1L
  //---------------------------------------------------------------------------
  private val sectionName = SectionName.SECTION_4_4_2_7
  //---------------------------------------------------------------------------
  private val recordVerifierSequence =  Seq(
      StringRecordVerifier(KEYWORD_DATASUM, sectionName, Some(extraCheckDATASUM))
    , StringRecordVerifier(KEYWORD_CHECKSUM, sectionName)
  )
  //---------------------------------------------------------------------------
  def getMapEntrySeq() = recordVerifierSequence.map { r => r.keyword -> r }
  //---------------------------------------------------------------------------
  private def extraCheckDATASUM(recordMap:RecordMap
                               , record: Record[_]
                               , sectionName: String
                               , ctxt: FitsCtxt): Boolean = {
    val value = record.value.toString.trim
    if (!Util.isLong(value)) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '${record.value.toString}' is not a valid 32-bit unsigned int value")
    }

    val dataSum = value.toLong
    if (dataSum > MAX_VALUE_32_BIT_INTEGER) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '$dataSum' is greater than the max valid 32-bit unsigned int value '$MAX_VALUE_32_BIT_INTEGER'")
    }

    if (dataSum < 0) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '$dataSum' can not be negative '$MAX_VALUE_32_BIT_INTEGER'")
    }

    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Section_4_4_2_7.scala
//=============================================================================