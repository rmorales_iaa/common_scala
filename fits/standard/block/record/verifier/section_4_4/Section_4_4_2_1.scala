/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  11/Sep/2023
  * Time:  10h:06m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.section_4_4
//=============================================================================
import com.common.fits.standard.Keyword.{KEYWORD_BLOCKED, KEYWORD_DATE, KEYWORD_EXTEND, KEYWORD_ORIGIN}
import com.common.fits.standard.block.record.{Record, RecordMap}
import com.common.fits.standard.block.record.verifier.{LogicalRecordVerifier, StringRecordVerifier}
import com.common.fits.standard.block.record.verifier.section_4_4.parser.DateParser
import com.common.fits.standard.disconformity.{DisconfFatal, DisconfWarn}
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.sectionName.SectionName
//=============================================================================
//=============================================================================
object Section_4_4_2_1 {
  //---------------------------------------------------------------------------
  private val sectionName = SectionName.SECTION_4_4_2_1
  //---------------------------------------------------------------------------
  private val recordVerifierSequence =  Seq(
      StringRecordVerifier(KEYWORD_DATE,     sectionName, Some(extraCheckDATE))
    , StringRecordVerifier(KEYWORD_ORIGIN,   sectionName)
    , LogicalRecordVerifier(KEYWORD_EXTEND,  sectionName)
    , LogicalRecordVerifier(KEYWORD_BLOCKED, sectionName, Some(extraCheckBLOCKED))
  )
  //---------------------------------------------------------------------------
  def getMapEntrySeq() = recordVerifierSequence.map { r=> r.keyword -> r }
  //---------------------------------------------------------------------------
  def extraCheckDATE(recordMap: RecordMap
                     , record: Record[_]
                     , sectionName: String
                     , ctxt: FitsCtxt): Boolean = {

    val keyword = record.keyword
    val value = record.value.toString
    if (!recordMap.contains(keyword)) {
      DisconfWarn(
        ctxt
        , SectionName.SECTION_9_8
        , s"Structure: ${ctxt.structureID} keyword '$keyword' is not present , but it is strongly recommended in all HDUs")
      return true
    }

    DateParser.parseInput(value).getOrElse{
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '${record.value.toString}' is not a valid date")
    }
    true
  }
  //---------------------------------------------------------------------------
  private def extraCheckBLOCKED(recordMap: RecordMap
                                , record: Record[_]
                                , sectionName: String
                                , ctxt: FitsCtxt) : Boolean = {
    if (record.id >= 36) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '${record.value.toString}' must appear before within the first 36 records in the primary header")
      true
    }
    else true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Section_4_4_2_1.scala
//=============================================================================