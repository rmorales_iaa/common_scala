//=============================================================================
/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  08/Sep/2023
  * Time:  11h:08m
  * Description: None
  */
//=============================================================================
package com.common.fits.standard.block.record.verifier.section_4_4
//=============================================================================
import com.common.fits.standard.Keyword._
import com.common.fits.standard.block.record.verifier.{LogicalRecordVerifier, StringRecordVerifier,IntegerRecordVerifier}
import com.common.fits.standard.sectionName.SectionName
//=============================================================================
//=============================================================================
object Section_4_4_2_6 {
  //---------------------------------------------------------------------------
  private val sectionName = SectionName.SECTION_4_4_2_6
  //---------------------------------------------------------------------------
  private val recordVerifierSequence =  Seq(
      StringRecordVerifier(KEYWORD_EXTNAME, sectionName)
    , IntegerRecordVerifier(KEYWORD_EXTVER, sectionName)
    , IntegerRecordVerifier(KEYWORD_EXTLEVEL, sectionName)
    , LogicalRecordVerifier(KEYWORD_INHERIT, sectionName)
  )
  //---------------------------------------------------------------------------
  def getMapEntrySeq() = recordVerifierSequence.map { r => r.keyword -> r }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Section_4_4_2_6.scala
//=============================================================================