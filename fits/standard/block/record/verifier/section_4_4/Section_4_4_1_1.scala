/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  11/Sep/2023
  * Time:  10h:34m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.section_4_4
//=============================================================================
//=============================================================================
//=============================================================================
object Section_4_4_1_1 {
  //---------------------------------------------------------------------------
  //see implementation in class 'HDU'
  //methods :
  // isValidSimpleRecord
  // isValidRecordBitPix
  // isValidRecordNaxis
  // isValidRecordAxisN
  // isValidRecordEnd
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Section_4_4_1_1.scala
//=============================================================================