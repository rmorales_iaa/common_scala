/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  14/Sep/2023
  * Time:  11h:59m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.section_10
//=============================================================================
import com.common.fits.standard.Keyword._
import com.common.fits.standard.block.record.{Record, RecordMap}
import com.common.fits.standard.block.record.verifier.common.singleIndex.{RecordVerifierSingleIndexTrait}
import com.common.fits.standard.block.record.verifier.{IntegerRecordVerifier, LogicalRecordVerifier, RecordVerifierDataTypeString, StringRecordVerifier}
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.block.record.verifier.RecordVerifierDataTypeInteger
import com.common.fits.standard.block.record.verifier.section_10.Section_10_1_1.extraCheckCompressAlgorithmName
import com.common.fits.standard.disconformity.{DisconfFatal, DisconfWarn}
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object Section_10_1_2 {
  //---------------------------------------------------------------------------
  private final val sectionName = SectionName.SECTION_10_1_2
  //---------------------------------------------------------------------------
  private final val MIN_ZDITHER_VALUE = 1 //inclusive
  private final val MAX_ZDITHER_VALUE = 10000 //inclusive
  //---------------------------------------------------------------------------
  private val recordVerifierSequence = Seq(
      StringRecordVerifier(KEYWORD_ZMASKCMP, sectionName, Some(extraCheckCompressAlgorithmName))
    , StringRecordVerifier(KEYWORD_ZQUANTIZ, sectionName)
    , IntegerRecordVerifier(KEYWORD_ZDITHER0, sectionName, Some(extraCheckDither))
    , LogicalRecordVerifier(KEYWORD_ZSIMPLE, sectionName)
    , LogicalRecordVerifier(KEYWORD_ZEXTEND, sectionName)
    , LogicalRecordVerifier(KEYWORD_ZBLOCKED, sectionName)
    , StringRecordVerifier(KEYWORD_ZTENSION, sectionName)
    , IntegerRecordVerifier(KEYWORD_ZPCOUNT, sectionName)
    , IntegerRecordVerifier(KEYWORD_ZGCOUNT, sectionName)
    , StringRecordVerifier(KEYWORD_ZHECKSUM, sectionName)
    , StringRecordVerifier(KEYWORD_ZDATASUM, sectionName)
  )
  //---------------------------------------------------------------------------
  def getMapEntrySeq() = recordVerifierSequence.map { r => r.keyword -> r }
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , ctxt: FitsCtxt): Boolean = {
    val positionSeq = ArrayBuffer[Long]()

    if (!RecordVerifierZTILE().verify(recordMap, positionSeq, ctxt)) return false
    if (!RecordVerifierZNAME().verify(recordMap, positionSeq, ctxt)) return false
    if (!RecordVerifierZVAL().verify(recordMap, positionSeq, ctxt)) return false
    true
  }
  //===========================================================================
  case class RecordVerifierZTILE() extends RecordVerifierSingleIndexTrait {
    //-------------------------------------------------------------------------
    val keywordBase = KEYWORD_ZTILE
    val dataType = RecordVerifierDataTypeInteger()
    val isIndexed = true
    val wcsSuffix = ""
    //-------------------------------------------------------------------------
    def verify(recordMap: RecordMap
               , positionSeq: ArrayBuffer[Long]
               , ctxt: FitsCtxt): Boolean = {
      if (!super.verify(recordMap
        , maxIndex = recordMap.getZNaxis()
        , sectionName
        , ctxt)) return false
      val znaxis = recordMap.getZNaxis()
      for(i<-1L to znaxis) {
        val keyword = keywordBase + i
        if (recordMap.contains(keyword)) {
          val value = recordMap.getFirstValue[Long](keyword).get
          if (recordMap.getFirstValue[Long](keyword).get < 0) {
            DisconfFatal(
              ctxt
              , sectionName
              , s"Structure: ${ctxt.structureID} keyword '$keyword' with value '$value' must be positive")
          }
        }
        else {
          DisconfWarn(
            ctxt
            , sectionName
            , s"Structure: ${ctxt.structureID} keyword '$keyword' should be defined")
        }
      }
      true
    }
    //-------------------------------------------------------------------------
  }
  //===========================================================================
  case class RecordVerifierZNAME() extends RecordVerifierSingleIndexTrait {
    //-------------------------------------------------------------------------
    val keywordBase = KEYWORD_ZNAME
    val dataType = RecordVerifierDataTypeString()
    val isIndexed = true
    val wcsSuffix = ""
    //-------------------------------------------------------------------------
    def verify(recordMap: RecordMap
               , positionSeq: ArrayBuffer[Long]
               , ctxt: FitsCtxt): Boolean = {
      if (!super.verify(recordMap
        , maxIndex = recordMap.getZNaxis()
        , sectionName
        , ctxt)) return false

      val znaxis = recordMap.getZNaxis()
      for (i <- 1L to znaxis) {
        val keyword = keywordBase + i
        if (!recordMap.contains(keyword))
          DisconfWarn(
            ctxt
            , sectionName
            , s"Structure: ${ctxt.structureID} keyword '$keyword' should be defined")
      }
      true
    }
    //-------------------------------------------------------------------------
  }

  //===========================================================================
  case class RecordVerifierZVAL() extends RecordVerifierSingleIndexTrait {
    //-------------------------------------------------------------------------
    val keywordBase = KEYWORD_ZVAL
    val dataType = RecordVerifierDataTypeString()
    val isIndexed = true
    val wcsSuffix = ""
    //-------------------------------------------------------------------------
    def verify(recordMap: RecordMap
               , positionSeq: ArrayBuffer[Long]
               , ctxt: FitsCtxt): Boolean = {
      if (!super.verify(recordMap
        , maxIndex = recordMap.getZNaxis()
        , sectionName
        , ctxt)) return false

      val znaxis = recordMap.getZNaxis()
      for (i <- 1L to znaxis) {
        val keyword = keywordBase + i
        if (!recordMap.contains(keyword))
          DisconfWarn(
            ctxt
            , sectionName
            , s"Structure: ${ctxt.structureID} keyword '$keyword' should be defined")
      }
      true
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def extraCheckDither(recordMap: RecordMap
                       , record: Record[_]
                       , sectionName: String
                       , ctxt: FitsCtxt): Boolean = {
    val znaxis = recordMap.getZNaxis()
    for (i <- 1L to znaxis) {
      val keyword = KEYWORD_ZNAXIS + i
      val value = recordMap.getFirstValue[Long](keyword).get
      if (recordMap.getFirstValue[Long](keyword).get < MIN_ZDITHER_VALUE) {
        DisconfFatal(
          ctxt
          , sectionName
          , s"Structure: ${ctxt.structureID} keyword '$keyword' with value '$value' must greater or equal to '$MIN_ZDITHER_VALUE' ")
      }
      if (recordMap.getFirstValue[Long](keyword).get > MAX_ZDITHER_VALUE) {
        DisconfFatal(
          ctxt
          , sectionName
          , s"Structure: ${ctxt.structureID} keyword '$keyword' with value '$value' must less or equal to '$MAX_ZDITHER_VALUE' ")
      }
    }
    true
  }
  //===========================================================================
}
//=============================================================================
//End of file Section_10_1_2.scala
//=============================================================================