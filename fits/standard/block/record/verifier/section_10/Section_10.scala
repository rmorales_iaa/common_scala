/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  31/Aug/2023
  * Time:  10h:35m
  * Description: None
  */
//=============================================================================
package com.common.fits.standard.block.record.verifier.section_10
//=============================================================================
import com.common.fits.standard.Keyword.{KEYWORD_ZIMAGE}
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.fits.FitsCtxt
//=============================================================================
//=============================================================================
object Section_10 {
  //---------------------------------------------------------------------------
  private val recordVerifierMap =
    (Section_10_1_1.getMapEntrySeq() ++  //Required keywords
      Section_10_1_2.getMapEntrySeq()   //Other reserved keywords
     ).toMap
   //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , ctxt: FitsCtxt): Boolean = {

    if (!recordMap.contains(KEYWORD_ZIMAGE)) return true
    if (!Section_10_1_1.checkMandatoryKeywordSeq(recordMap,ctxt)) return false
    recordVerifierMap.foreach { case (keyword, recordVerifier) =>
      if (recordMap.contains(keyword))
        if (!recordVerifier.verify(recordMap,ctxt))
          return false
    }
    Section_10_1_2.verify(recordMap,ctxt)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Section_10.scala
//=============================================================================