/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  14/Sep/2023
  * Time:  11h:59m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.section_10
//=============================================================================
import com.common.fits.standard.Keyword.{KEYWORD_ZBITPIX, KEYWORD_ZCMPTYPE, KEYWORD_ZIMAGE, KEYWORD_ZNAXIS}
import com.common.fits.standard.block.record.{Record, RecordMap}
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.block.record.verifier.{IntegerRecordVerifier, LogicalRecordVerifier, StringRecordVerifier}
import com.common.fits.standard.disconformity.{DisconfFatal}
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.structure.hdu.HDU.{MAX_ALLOWED_NAXIS, MIN_ALLOWED_NAXIS, recordSequenceCommonCheck}
//=============================================================================
//=============================================================================
object Section_10_1_1 {
  //---------------------------------------------------------------------------
  private val sectionName = SectionName.SECTION_10_1_1
  //---------------------------------------------------------------------------
  private val recordVerifierSequence = Seq(
      LogicalRecordVerifier(KEYWORD_ZIMAGE, sectionName, Some(extraCheckImage))
    , StringRecordVerifier(KEYWORD_ZCMPTYPE, sectionName, Some(extraCheckCompressAlgorithmName))
    , IntegerRecordVerifier(KEYWORD_ZBITPIX, sectionName, Some(extraCheckBitPix))
    , IntegerRecordVerifier(KEYWORD_ZNAXIS, sectionName, Some(extraCheckNaxis))
  )
  //---------------------------------------------------------------------------
  def getMapEntrySeq() = recordVerifierSequence.map { r => r.keyword -> r }
  //---------------------------------------------------------------------------
  private val MANDATORY_KEYWORD_MAP = Seq(
    KEYWORD_ZIMAGE
    , KEYWORD_ZCMPTYPE
    , KEYWORD_ZBITPIX
    , KEYWORD_ZNAXIS
  )
  //---------------------------------------------------------------------------
  def extraCheckImage(recordMap: RecordMap
                      , record: Record[_]
                      , sectionName: String
                      , ctxt: FitsCtxt): Boolean = {
    val keyword = record.keyword
    val value = record.value.toString

    if (value != "true") {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '$keyword' with value '$value' must have the logical value T")
    }
    true
  }
  //---------------------------------------------------------------------------
  def checkMandatoryKeywordSeq(recordMap: RecordMap
                               , ctxt: FitsCtxt): Boolean = {
    MANDATORY_KEYWORD_MAP.foreach { mandatoryKeyword =>
      if (!recordMap.contains(mandatoryKeyword)) {
        DisconfFatal(
          ctxt
          , sectionName
          , s"Structure: ${ctxt.structureID} keyword '$mandatoryKeyword' is missing")
      }
    }
    true
  }
  //---------------------------------------------------------------------------
  //Table 36
  private val VALID_COMPRESSION_ALGORITHMS_MAP = Map(
    "RICE_1"      -> "Rice algorithm for integer data"
    , "GZIP_1"      -> "Combination of the LZ77 algorithm and Huffman coding, used in GNU Gzip"
    , "GZIP_2"      -> "Like ’GZIP 1’, but with reshuffled byte values"
    , "PLIO_1"      -> "IRAF PLIO algorithm for integer data"
    , "HCOMPRESS_1" -> "H-compress algorithm for twodimensional images"
    , "NOCOMPRESS"  -> "The HDU remains uncompressed"
  )
  //---------------------------------------------------------------------------
  //Table 8
  private val VALID_BITPIX_MAP = Map(
    8 -> "Character or unsigned binary integer"
    , 16 -> "16-bit two’s complement binary integer"
    , 32 -> "32-bit two’s complement binary integer"
    , 64 -> "64-bit two’s complement binary integer"
    ,-32 -> "IEEE single-precision floating point"
    ,-64 -> "IEEE double-precision floating point"
  )
  //---------------------------------------------------------------------------
  def extraCheckCompressAlgorithmName(recordMap: RecordMap
                                      , record: Record[_]
                                      , sectionName: String
                                      , ctxt: FitsCtxt): Boolean = {
    val keyword = record.keyword
    val value = record.value.toString
    if (!VALID_COMPRESSION_ALGORITHMS_MAP.contains(value)) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '$keyword' with value '$value' is not a valid compression algorithm")
    }
    true
  }
  //---------------------------------------------------------------------------
  private def extraCheckBitPix(recordMap: RecordMap
                               , record: Record[_]
                               , sectionName: String
                               , ctxt: FitsCtxt): Boolean = {
    val keyword = record.keyword
    val value = record.value.toString.toInt
    if (!VALID_BITPIX_MAP.contains(value)) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '$keyword' with value '$value' is not a valid bit pix")
    }
    true
  }

  //---------------------------------------------------------------------------
  private def extraCheckNaxis(recordMap: RecordMap
                              , record: Record[_]
                              , sectionName: String
                              , ctxt: FitsCtxt): Boolean = {
    val keyword = record.keyword
    val znaxis = record.value.toString.toInt
    if (znaxis < MIN_ALLOWED_NAXIS || znaxis > MAX_ALLOWED_NAXIS) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} invalid '$keyword' with value '$znaxis' is not valid. Expecting a value between: {$MIN_ALLOWED_NAXIS,$MAX_ALLOWED_NAXIS} both limits included"
        , s"Current value: ${znaxis.toString}")
    }

    val keywordSeq = (for (i <- 1L to znaxis) yield KEYWORD_ZNAXIS + i).toArray
    if(!recordSequenceCommonCheck(
      recordMap
      , sectionName
      , keyword
      , znaxis
      , keywordSeq
      , ctxt: FitsCtxt
      , checkRecordString = false
      , checkAreConsecutive= true)) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} invalid '$keyword' invalid ZNAXISn value"
        , s"Current value: ${znaxis.toString}")
    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Section_10_1_1.scala
//=============================================================================