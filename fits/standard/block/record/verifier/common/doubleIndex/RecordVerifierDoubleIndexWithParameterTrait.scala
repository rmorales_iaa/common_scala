/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  07/Sep/2023
  * Time:  13h:55m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.common.doubleIndex
//=============================================================================
import com.common.fits.standard.block.record.verifier.common.singleIndex.RecordVerifierSingleIndexTrait
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.disconformity.DisconfFatal
import com.common.fits.standard.fits.FitsCtxt
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
trait RecordVerifierDoubleIndexWithParameterTrait extends RecordVerifierDoubleIndexTrait {
  //---------------------------------------------------------------------------
  val isIndexed = true
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , positionSeq: ArrayBuffer[Long]
             , sectionName: String
             , ctxt: FitsCtxt): Boolean = {

    val wcsAxes = recordMap.getWcsAxes(wcsSuffix)
    val indexSeqSeq = (for (i <- 1L to wcsAxes) yield {
      RecordVerifierSingleIndexTrait.getMaxIndex(recordMap
        , s"$keywordBase${i}_"
        , wcsSuffix
        , sectionName
        , ctxt).getOrElse(return false)
    }).toArray

    if (indexSeqSeq.isEmpty) return true
    val maxSize = indexSeqSeq.head.size
    if (!indexSeqSeq.forall( _.size == maxSize)) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '$keywordBase'i_j$wcsSuffix not all in indexes in index 'j' has the same size")
    }

    if (!super.verify(
      recordMap
      , maxIndex  = maxSize
      , sectionName
      , ctxt: FitsCtxt
      , minAllowedIndex = Some(0)
      , maxAllowedIndex = Some(99))) return false

    positionSeq ++= getPositionSeq
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RecordVerifierDoubleIndexWithParameterTrait.scala
//=============================================================================