/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  07/Sep/2023
  * Time:  14h:10m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.common.doubleIndex
//=============================================================================
import com.common.fits.standard.block.record.verifier.common.singleIndex.RecordVerifierSingleIndexTrait
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.fits.FitsCtxt
//=============================================================================
//=============================================================================
trait RecordVerifierDoubleIndexTrait extends RecordVerifierSingleIndexTrait {
  //---------------------------------------------------------------------------
  override def verify(recordMap: RecordMap
                      , maxIndex: Long
                      , sectionName: String
                      , ctxt: FitsCtxt
                      , minAllowedIndex: Option[Long] = None
                      , maxAllowedIndex: Option[Long] = None): Boolean = {
    if (!isValidIndexSeq(recordMap, maxIndex, sectionName, ctxt, minAllowedIndex, maxAllowedIndex)) false
    else isValidValueSeq(recordMap, maxIndex, sectionName, ctxt)
  }
  //---------------------------------------------------------------------------
  private def isValidIndexSeq(recordMap: RecordMap
                              , maxIndex: Long
                              , sectionName: String
                              , ctxt: FitsCtxt
                              , minAllowedIndex: Option[Long] = None
                              , maxAllowedIndex: Option[Long] = None): Boolean = {
    if (!isIndexed) return true
    for (i <- 1L to maxIndex) {
      if (!RecordVerifierSingleIndexTrait.isValidIndex(
        recordMap
        , s"$keywordBase${i}_"
        , wcsSuffix
        , sectionName
        , ctxt
        , maxIndex
        , minAllowedIndex
        , maxAllowedIndex)) return false
    }
    true
  }
  //---------------------------------------------------------------------------
  private def isValidValueSeq(recordMap: RecordMap
                              , maxIndex: Long
                              , sectionName: String
                              , ctxt: FitsCtxt): Boolean = {
    for (i <- 1L to maxIndex) yield {
      if (!RecordVerifierSingleIndexTrait.isValidValue(
        recordMap
        , s"$keywordBase${i}_"
        , wcsSuffix
        , dataType
        , valueStringSeq
        , positionSeq
        , sectionName
        , ctxt)) return false
    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RecordVerifierDoubleIndexTrait.scala
//=============================================================================