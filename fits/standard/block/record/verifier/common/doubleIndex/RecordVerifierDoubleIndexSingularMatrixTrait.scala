/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  07/Sep/2023
  * Time:  13h:55m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.common.doubleIndex
//=============================================================================
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.disconformity.DisconfFatal
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.block.record.verifier.RecordVerifierDataTypeDouble
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
object RecordVerifierDoubleIndexSingularMatrixTrait {
  //---------------------------------------------------------------------------
  private def calculateDeterminant(matrix: Array[Array[Double]]): Double = {
    val n = matrix.length
    if (n == 1) matrix(0)(0) // Base case: For a 1x1 matrix, the determinant is the single element
    else {
      var det = 0.0
      for (col <- 0 until n) {
        // Calculate the cofactor matrix for this column.
        val cofactorMatrix = (for {row <- 1 until n} yield
          matrix(row).patch(col, Nil, 1)).toArray
        // Recursively calculate the determinant using cofactor expansion.
        det += matrix(0)(col) * Math.pow(-1, col) * calculateDeterminant(cofactorMatrix)
      }
      det
    }
  }
  //---------------------------------------------------------------------------
  private def isMatrixSingular(matrix: Array[Array[Double]]): Boolean =
    calculateDeterminant(matrix) == 0
  //---------------------------------------------------------------------------
}
//=============================================================================
import RecordVerifierDoubleIndexSingularMatrixTrait._
trait RecordVerifierDoubleIndexSingularMatrixTrait extends RecordVerifierDoubleIndexTrait {
  //---------------------------------------------------------------------------
  val dataType = RecordVerifierDataTypeDouble()
  val isIndexed = true
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , positionSeq: ArrayBuffer[Long]
             , sectionName: String
             , ctxt: FitsCtxt): Boolean = {
    if (!super.verify(
      recordMap
      , maxIndex = recordMap.getWcsAxes(wcsSuffix)
      , sectionName
      , ctxt)) return false

    if (valueStringSeq.isEmpty) return true
    val maxIndex = recordMap.getWcsAxes(wcsSuffix)
    val valueSeq = getValueSeqAsDoubleSeq.grouped(maxIndex.toInt).toArray

    if (valueSeq.flatten.size != 4) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keywords CD_X_Y require a WCSAXIS 2")
      return false
    }

    if (isMatrixSingular(valueSeq)) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${getImageKeywordWithSuffix()}' is a singular matrix")
    }
    positionSeq ++= getPositionSeq
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RecordVerifierDoubleIndexSingularMatrixTrait.scala
//=============================================================================