/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  07/Sep/2023
  * Time:  13h:54m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.common.singleIndex
//=============================================================================
import com.common.fits.standard.block.record.verifier.RecordVerifierDataTypeDouble
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.disconformity.DisconfFatal
import com.common.fits.standard.fits.FitsCtxt
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
trait RecordVerifierSingleIndexNonNegativeTrait extends RecordVerifierSingleIndexTrait {
  //---------------------------------------------------------------------------
  val dataType = RecordVerifierDataTypeDouble()
  val isIndexed = true
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , positionSeq: ArrayBuffer[Long]
             , sectionName: String
             , ctxt: FitsCtxt): Boolean = {

    if (!super.verify(
      recordMap
      , maxIndex = recordMap.getWcsAxes(wcsSuffix)
      , sectionName
      , ctxt: FitsCtxt)) return false

    if (valueStringSeq.isEmpty) return true
    val valueSeq = getValueSeqAsDoubleSeq
    if (!valueSeq.forall(_ != 0D)) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${getImageKeywordWithSuffix()}' has a negative value: '${valueSeq.mkString("{", ",", "}")}'")
    }
    positionSeq ++= this.getPositionSeq
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RecordVerifierSingleIndexNonNegative.scala
//=============================================================================