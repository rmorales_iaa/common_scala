/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  07/Sep/2023
  * Time:  13h:52m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.common.singleIndex
//=============================================================================
import com.common.fits.standard.Keyword.{KEYWORD_PS, KEYWORD_PV}
import com.common.fits.standard.block.record.verifier.RecordVerifierDataType
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.block.record.verifier.section_8.section_8_2.WcsPrimaryAndAlternative._
import com.common.fits.standard.disconformity.{DisconfFatal, DisconfWarn}
import com.common.fits.standard.fits.FitsCtxt
import com.common.util.util.Util
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object RecordVerifierSingleIndexTrait {
  //---------------------------------------------------------------------------
  final val RECORD_POSITION_NOT_PRESENT_ID = -1L
  //---------------------------------------------------------------------------
  private def getAllKeywordWithPrefixAndSuffix(recordMap: RecordMap
                                               , keyword: String
                                               , suffix: String) = {
    val keywordSeq = recordMap.getAllKeywordWithPrefixAndSuffix(keyword, suffix)
    if (suffix == PRIMARY_WCS_SUFFIX) //filter additional extensions when primary header is being processed
      keywordSeq.filter { s =>
        val lastChar = s.last
        !(lastChar >= ALTERNATIVE_WCS_SUFFIX_MIN &&
          lastChar <= ALTERNATIVE_WCS_SUFFIX_MAX)
      }
    else keywordSeq
  }
  //---------------------------------------------------------------------------
  private def calculateDeterminant(matrix: Array[Array[Double]]): Double = {
    val n = matrix.length
    if (n == 1) matrix(0)(0) // Base case: For a 1x1 matrix, the determinant is the single element
    else {
      var det = 0.0
      for (col <- 0 until n) {
        // Calculate the cofactor matrix for this column.
        val cofactorMatrix = (for {row <- 1 until n} yield
          matrix(row).patch(col, Nil, 1)).toArray
        // Recursively calculate the determinant using cofactor expansion.
        det += matrix(0)(col) * Math.pow(-1, col) * calculateDeterminant(cofactorMatrix)
      }
      det
    }
  }
  //---------------------------------------------------------------------------
  def isMatrixSingular(matrix: Array[Array[Double]]): Boolean =
    calculateDeterminant(matrix) == 0
  //---------------------------------------------------------------------------
  def getMaxIndex(recordMap: RecordMap
                  , keyword: String
                  , suffix: String
                  , sectionName: String
                  , ctxt: FitsCtxt) : Option[Array[Long]] = {

    val keywordSeq = getAllKeywordWithPrefixAndSuffix(recordMap, keyword, suffix)
    if (keywordSeq.isEmpty) return Some(Array()) //no key present
    val indexSeq = keywordSeq map { index =>
      val _key = index.replace(keyword, "") //remove original key
      val key = if (suffix == PRIMARY_WCS_SUFFIX) _key else _key.dropRight(1)
      if (!Util.isLong(key)) {
        DisconfFatal(
          ctxt
          , sectionName
          , s"Structure: ${ctxt.structureID} keyword '$keyword' is indexed but '$index' is not a valid keyword indexed")
      }
      key.toLong
    }

    val indexSorted = indexSeq.sorted
    if (!Util.isConsecutiveSeqLong(indexSorted)) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '$keyword' has no consecutive keywords: '${indexSorted.mkString("{", ",", "}")}'")
    }

    if (keyword.startsWith(KEYWORD_PV) ||
        keyword.startsWith(KEYWORD_PS)) {
        DisconfWarn(
          ctxt
          , sectionName
          , s"Structure: ${ctxt.structureID} keyword '$keyword' should start with sub-index 0")
    }
    else {
      if (indexSorted.head != 1L) {
        DisconfFatal(
          ctxt
          , sectionName
          , s"Structure: ${ctxt.structureID} keyword '$keyword' must start with index 1")
      }
    }
    Some(indexSeq)
  }
  //---------------------------------------------------------------------------
  def isValidIndex(recordMap: RecordMap
                   , keyword: String
                   , suffix: String
                   , sectionName: String
                   , ctxt: FitsCtxt
                   , maxIndex: Long
                   , minAllowedIndex: Option[Long] = None
                   , maxAllowedIndex: Option[Long] = None): Boolean = {
    val indexSeq = getMaxIndex(recordMap
                               , keyword
                               , suffix
                               , sectionName
                               , ctxt).getOrElse(return false)
    if (indexSeq.isEmpty) return true

    val calcMinIndex = indexSeq.head
    val calcMaxIndex = indexSeq.last

    if (calcMaxIndex != maxIndex) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '$keyword'. The max index is: '$calcMaxIndex' but must be equal: '$maxIndex'")
    }

    if (maxAllowedIndex.isDefined) {
      if (maxAllowedIndex.get < maxIndex) {
        DisconfFatal(
          ctxt
          , sectionName
          , s"Structure: ${ctxt.structureID} keyword '$keyword'. The max index is: '$calcMaxIndex' but max allowed index is: '${maxAllowedIndex.get}'")
      }
    }

    if (minAllowedIndex.isDefined) {
      if (minAllowedIndex.get > calcMinIndex) {
        DisconfFatal(
          ctxt
          , sectionName
          , s"Structure: ${ctxt.structureID} keyword '$keyword'. The max index is: '$calcMinIndex' but max allowed index is: '${minAllowedIndex.get}'")
      }
    }
    true
  }
  //---------------------------------------------------------------------------
  def isValidValue(recordMap: RecordMap
                   , keyword: String
                   , suffix: String
                   , dataType : RecordVerifierDataType
                   , valueStringSeq: ArrayBuffer[String]
                   , positionSeq: ArrayBuffer[Long]
                   , sectionName: String
                   , ctxt: FitsCtxt): Boolean = {

    val keywordSeq = getAllKeywordWithPrefixAndSuffix(recordMap,keyword, suffix)
    if (keywordSeq.isEmpty) return true //no key present
    val recordSeq = keywordSeq map (keyword=> recordMap.getFirstRecord(keyword).get)
    Some(recordSeq.zipWithIndex map { case (record, i) =>
      val value = record.value.toString
      if (!dataType.isValid(record)) {
        DisconfFatal(
          ctxt
          , sectionName
          , s"Structure: ${ctxt.structureID} keyword '$keyword$i' with value '$value' is not a valid type: '${dataType.name}'")
      }
      valueStringSeq += value
      positionSeq += record.id
    })
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
trait RecordVerifierNoIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase: String
  val wcsSuffix: String
  val dataType: RecordVerifierDataType
  //---------------------------------------------------------------------------
  def getKeyword = keywordBase + wcsSuffix
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , sectionName: String
             , ctxt: FitsCtxt): Boolean = {
    val keyword = getKeyword
    val record = recordMap.getFirstRecord(keyword).getOrElse(return true)
    if (!dataType.isValid(record)) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '$keyword' with value '${record.value.toString}' is not a valid type: '${dataType.name}'")
    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
trait RecordVerifierSingleIndexTrait {
  //---------------------------------------------------------------------------
  val keywordBase : String
  val wcsSuffix   : String
  val dataType    : RecordVerifierDataType
  val isIndexed   : Boolean
  //---------------------------------------------------------------------------
  val isPrimaryWcs = wcsSuffix == PRIMARY_WCS_SUFFIX
  //---------------------------------------------------------------------------
  protected var valueStringSeq = ArrayBuffer[String]()
  protected var positionSeq = ArrayBuffer[Long]()
  //---------------------------------------------------------------------------
  def getKeyword = keywordBase + wcsSuffix
  //---------------------------------------------------------------------------
  def isDefined = !valueStringSeq.isEmpty
  //---------------------------------------------------------------------------
  //see table 22
  def getImageKeywordWithSuffix() = s"${getKeyword}suffix"
  //---------------------------------------------------------------------------
  def getImageKeywordWithSuffix(i: Long) = s"$getKeyword${i}suffix"
  //---------------------------------------------------------------------------
  def getImageKeywordWithSuffix(i: Long, j: Long) = s"$getKeyword${i}_$getKeyword${j}suffix"
  //---------------------------------------------------------------------------
  def getBinTablePrimaryKeywordWithSuffix() = s"${getKeyword}suffix"
  //---------------------------------------------------------------------------
  def getBinTableAlternativeKeywordWithSuffix() =  getBinTablePrimaryKeywordWithSuffix
  //---------------------------------------------------------------------------
  def getBinTablePrimaryKeywordWithSuffix(n: Long)= s"$getKeyword${n}suffix"
  //---------------------------------------------------------------------------
  def getBinTableAlternativeKeywordWithSuffix(n: Long) = getBinTablePrimaryKeywordWithSuffix(n)
  //---------------------------------------------------------------------------
  def getBinTablePrimaryKeywordWithSuffix(i:Long,n: Long) = s"$i$getKeyword$n"  //no suffix
  //---------------------------------------------------------------------------
  def getBinTableAlternativeKeywordWithSuffix(i: Long, n: Long) = s"$i$getKeyword${n}suffix"
  //---------------------------------------------------------------------------
  def getBinTablePrimaryKeywordWithSuffix(i:Long,n: Long, m:Long) =  s"$i$getKeyword${n}_${m}suffix"
  //---------------------------------------------------------------------------
  def getBinTableAlternativeKeywordWithSuffix(i:Long,n: Long, m:Long) = getBinTablePrimaryKeywordWithSuffix(i,n,m)
  //---------------------------------------------------------------------------
  def getAsciiTablePixelPrimaryKeywordWithSuffix(n: Long) = s"$getKeyword$n"  //no suffix
  //---------------------------------------------------------------------------
  def getAsciiTablePixelAlternativeKeywordWithSuffix(n: Long) = s"$getKeyword$n$wcsSuffix"
  //---------------------------------------------------------------------------
  def getPixelPrimaryKeywordWithSuffix(n: Long, k: Long) = s"$getKeyword${n}_$k$wcsSuffix"
  //---------------------------------------------------------------------------
  def getPixelAlternativeKeywordWithSuffix(n: Long, k: Long) = getPixelPrimaryKeywordWithSuffix(n,k)
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
             , maxIndex: Long
             , sectionName: String
             , ctxt: FitsCtxt
             , minAllowedIndex: Option[Long] = None
             , maxAllowedIndex: Option[Long] = None): Boolean =
    if (!isValidIndex(recordMap, sectionName, ctxt, maxIndex, minAllowedIndex, maxAllowedIndex)) false
    else isValidValue(recordMap, sectionName, ctxt)
  //---------------------------------------------------------------------------
  private def isValidIndex(recordMap: RecordMap
                           , sectionName: String
                           , ctxt: FitsCtxt
                           , maxIndex: Long
                           , minAllowedIndex: Option[Long] = None
                           , maxAllowedIndex: Option[Long] = None): Boolean = {
    if (!isIndexed) true
    else RecordVerifierSingleIndexTrait.isValidIndex(
        recordMap
      , keywordBase
      , wcsSuffix
      , sectionName
      , ctxt
      , maxIndex
      , minAllowedIndex
      , maxAllowedIndex)
  }
  //---------------------------------------------------------------------------
  private def isValidValue(recordMap: RecordMap
                           , sectionName: String
                           , ctxt: FitsCtxt): Boolean =
    RecordVerifierSingleIndexTrait.isValidValue(
        recordMap
      , keywordBase
      , wcsSuffix
      , dataType
      , valueStringSeq
      , positionSeq
      , sectionName
      , ctxt)
  //---------------------------------------------------------------------------
  def getValueSeqAsDoubleSeq = (valueStringSeq map (_.toDouble)).toArray
  //---------------------------------------------------------------------------
  def getValueSeqAsLongSeq = (valueStringSeq map (_.toLong)).toArray
  //---------------------------------------------------------------------------
  def getPositionSeq = positionSeq.toArray
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RecordVerifierSingleIndexTrait.scala
//=============================================================================