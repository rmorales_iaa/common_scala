/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  08/Sep/2023
  * Time:  17h:27m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.common
//=============================================================================
import com.common.fits.standard.block.record.{Record, RecordMap}
import com.common.fits.standard.block.record.verifier.{RecordVerifierDataType}
import com.common.fits.standard.disconformity.DisconfFatal
import com.common.fits.standard.fits.FitsCtxt
//=============================================================================
//=============================================================================
trait SimpleRecordVerifierTrait[T <: RecordVerifierDataType] {
  //---------------------------------------------------------------------------
  val keyword: String
  val dataType: T
  val sectionName: String
  val extraCheck: Option[(RecordMap, Record[_], String, FitsCtxt) => Boolean]
  //---------------------------------------------------------------------------
  def verify(recordMap: RecordMap
              , ctxt: FitsCtxt): Boolean = {
    if (!recordMap.contains(keyword)) return true
    val record = recordMap.getFirstRecord(keyword).get
    if (!dataType.isValid(record)) {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '$keyword' with value '${record.value.toString}' is not a valid type: '${dataType.name}'")
    }
    if (extraCheck.isDefined) extraCheck.get(recordMap
                                             , recordMap.getFirstRecord(keyword).get
                                             , sectionName
                                             , ctxt)
    else true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SimpleRecordVerifierTrait.scala
//=============================================================================