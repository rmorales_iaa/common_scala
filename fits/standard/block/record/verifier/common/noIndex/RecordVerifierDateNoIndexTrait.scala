/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  13/Sep/2023
  * Time:  12h:51m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.common.noIndex
//=============================================================================
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.block.record.verifier.section_4_4.parser.DateParser
import com.common.fits.standard.disconformity.DisconfFatal
import com.common.fits.standard.fits.FitsCtxt
//=============================================================================
//=============================================================================
trait RecordVerifierDateNoIndexTrait extends RecordVerifierStringNoIndexTrait {
  //---------------------------------------------------------------------------
  override def verify(recordMap: RecordMap
                      , sectionName: String
                      , ctxt: FitsCtxt): Boolean = {

    if (!recordMap.contains(getKeyword)) return true
    if (!super.verify(recordMap,sectionName,ctxt)) return false
    val record = recordMap.getFirstRecord(getKeyword).get
    DateParser.parseInput(record.value.toString).getOrElse {
      DisconfFatal(
        ctxt
        , sectionName
        , s"Structure: ${ctxt.structureID} keyword '${record.keyword}' with value '${record.value.toString}' is not a valid date")
    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RecordVerifierDateNoIndexTrait.scala
//=============================================================================