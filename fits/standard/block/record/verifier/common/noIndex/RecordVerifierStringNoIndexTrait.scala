/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  13/Sep/2023
  * Time:  08h:58m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.common.noIndex
//=============================================================================
import com.common.fits.standard.block.record.verifier.RecordVerifierDataTypeString
import com.common.fits.standard.block.record.verifier.common.singleIndex.RecordVerifierNoIndexTrait
//=============================================================================
//=============================================================================
trait RecordVerifierStringNoIndexTrait extends RecordVerifierNoIndexTrait {
  //---------------------------------------------------------------------------
  val dataType = RecordVerifierDataTypeString()
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file RecordVerifierStringNoIndexTrait.scala
//=============================================================================