/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  13/Sep/2023
  * Time:  08h:58m
  * Description: None
  */
package com.common.fits.standard.block.record.verifier.common.noIndex
//=============================================================================
import com.common.fits.standard.block.record.verifier.RecordVerifierDataTypeInteger
import com.common.fits.standard.block.record.verifier.common.singleIndex.RecordVerifierNoIndexTrait
//=============================================================================
//=============================================================================
trait RecordVerifierIntegerNoIndexTrait extends RecordVerifierNoIndexTrait {
  //---------------------------------------------------------------------------
  val dataType = RecordVerifierDataTypeInteger()
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file RecordVerifierIntegerNoIndexTrait.scala
//=============================================================================