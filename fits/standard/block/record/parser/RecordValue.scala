/**
 * Created byRafael Morales (rmorales@iaa.es)
 * Date 28/Sep/2022
 * Time 21h:19m
 * Description:
 *  http://www.lihaoyi.com/post/EasyParsingwithParserCombinators.html
 *  https://com-lihaoyi.github.io/fastparse/
 */
//=============================================================================
package com.common.fits.standard.block.record.parser
//=============================================================================
import com.common.fits.standard.dataType.complex.{ComplexFloat_64, ComplexFloat_32}
//=============================================================================
//=============================================================================
object RecordValue {
  //---------------------------------------------------------------------------
  //complex number
  final def COMPLEX_NUMBER_START_STRING = "("
  final def COMPLEX_NUMBER_DIVIDER_STRING = ","
  final def COMPLEX_NUMBER_END_STRING = ")"
  //---------------------------------------------------------------------------
}
//=============================================================================
trait RecordValue[T] {
  //---------------------------------------------------------------------------
  val value: T
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RecordValueString(value:String) extends RecordValue[String]
case class RecordValueLogical(value:Boolean) extends RecordValue[Boolean]
case class RecordValueInteger(value:Long) extends RecordValue[Long]
case class RecordValueDouble(value:Double) extends RecordValue[Double]
//--------------------------------------------------------------------------
case class RecordValueComplexInteger(value:ComplexFloat_32) extends RecordValue[ComplexFloat_32]
//--------------------------------------------------------------------------
case class RecordValueComplexFloat(value:ComplexFloat_64) extends RecordValue[ComplexFloat_64]
//=============================================================================

//=============================================================================
//End of file RecordValue.scala
//=============================================================================
