/**
 * Created byRafael Morales (rmorales@iaa.es)
 * Date 28/Sep/2022
 * Time 21h:19m
 * Description:
 *  http://www.lihaoyi.com/post/EasyParsingwithParserCombinators.html
 *  https://com-lihaoyi.github.io/fastparse/
 */
//=============================================================================
package com.common.fits.standard.block.record.parser
//=============================================================================
import fastparse._
import NoWhitespace._
import com.common.fits.standard.block.record.RecordCharacter._
import com.common.fits.standard.Keyword.KEYWORD_CONTINUE
//=============================================================================
import com.common.fits.standard.block.record.RecordCharacter.{RecordBlank, RecordEnd}
//=============================================================================
import com.common.fits.standard.block.record.{Record => R}
import com.common.fits.standard.block.record.RecordNumber.{RecordComplexFloat, RecordComplexInteger, RecordFloat, RecordInteger, RecordLogical}
import com.common.fits.standard.block.record.RecordCharacter.{RecordComment, RecordGeneralComment, RecordHistory, RecordString}
import com.common.fits.standard.block.record.parser.Token._
//=============================================================================
//=============================================================================
object Record {
  //---------------------------------------------------------------------------
  //commentary
  private def recordHistory[_:P] = keywordHistory ~ optAsciiString.!.map( s=> RecordHistory(s) )
  private def recordBlank[_:P] = keywordBlank ~ optAsciiString.!.map( s => RecordBlank(s) )
  private def recordComment[_:P] = keywordComment ~ optAsciiString.!.map( s=> RecordComment(s) )
  private def recordGeneralComment_1[_:P] = (keyword.! ~ asciiStringButEqual.! ~ optAsciiString.!).map {
    case (k,c1,c2) => RecordGeneralComment(k,c1 + c2)
  }

  //it is not contains the 'valueIndicator' but 'equal'
  private def recordGeneralComment_2[_:P] = (keyword ~ equal ~ asciiStringButSpace ~ optAsciiString).map {
    case (k, c1, c2) => RecordGeneralComment(k, c1 + c2)
  }

  private def recordCommentary[_:P] : P[R[_]] = recordComment |
                                                recordHistory |
                                                recordBlank |
                                                recordGeneralComment_1 |
                                                recordGeneralComment_2
  //---------------------------------------------------------------------------
  //long record
  private def recordLongCommentValue[_:P] =  optSpaceSeq ~ commentValue.?.!.map( s=>s )
  private def recordLongCommentStart[_:P] = (keyword ~ valueIndicator ~ optSpaceSeq ~ partialString.? ~ recordLongCommentValue)  map {
    case (k:String,v:Option[String],c:String) => RecordString(k, v.getOrElse(""),c, RECORD_STRING_FLAG_LONG_COMMENT_START)
  }
  private def recordLongCommentContinue[_:P] = (keywordContinue ~ optSpaceSeq ~ partialString.? ~ recordLongCommentValue)  map {
    case (v:Option[String],c:String) =>  RecordString(KEYWORD_CONTINUE, v.getOrElse(""),c, RECORD_STRING_FLAG_LONG_COMMENT_CONTINUE)
  }
  private def recordLongCommentEnd[_: P] = (keywordContinue ~ optSpaceSeq ~ string.? ~ recordLongCommentValue) map {
    case (v: Option[String], c: String) => RecordString(KEYWORD_CONTINUE, v.getOrElse(""), c, RECORD_STRING_FLAG_LONG_COMMENT_END)
  }
  private def recordLongComment[_: P] = recordLongCommentStart | recordLongCommentContinue |  recordLongCommentEnd
  //---------------------------------------------------------------------------
  //record
  //Comment: If the value field is not present, the value of the FITS keyword is not defined
  private def recordKeyword[_:P] = (keyword ~ valueIndicator ~ optSpaceSeq ~ recordValue.? ~ optSpaceSeq ~ commentValue.?).map {
    case (k: String, v: Option[RecordValue[_]], c: Option[String]) =>
      if (v.isEmpty) RecordString(k, "", c.getOrElse(""))
      else {
        val comment = c.getOrElse("")
        v.get match {
          case w: RecordValueString         => RecordString(k, w.value, comment)
          case w: RecordValueLogical        => RecordLogical(k, w.value, comment)
          case w: RecordValueInteger        => RecordInteger(k, w.value, comment)
          case w: RecordValueDouble         => RecordFloat(k, w.value,comment)
          case w: RecordValueComplexInteger => RecordComplexInteger(k, w.value, comment)
          case w: RecordValueComplexFloat   => RecordComplexFloat(k, w.value, comment)
        }
      }
  }
  //---------------------------------------------------------------------------
  //end record
  private def recordEnd[_:P] = (keywordEnd  ~ optSpaceSeq).!.map( _=> RecordEnd() )
  //---------------------------------------------------------------------------
  private def record[_: P] = recordEnd | recordCommentary | recordKeyword
  //---------------------------------------------------------------------------
  def parseInput[_: P]: P[R[_]] = record | recordLongComment
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Record.scala
//=============================================================================
