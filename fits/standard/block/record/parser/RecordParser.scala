/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Sep/2022
 * Time:  21h:19m
 * Description:
 * Description: It implements the tokens of the grammar defined in appendix A of FITS standard version 4.0 (2016 July 22)
 * http://www.lihaoyi.com/post/EasyParsingwithParserCombinators.html
 * https://com-lihaoyi.github.io/fastparse/
 */
//=============================================================================
package com.common.fits.standard.block.record.parser
//=============================================================================
import fastparse._
import fastparse.Parsed
//=============================================================================
import com.common.fits.standard.block.record.{Record => R}
import com.common.fits.standard.block.record.RecordCharacter.RecordError
import com.common.fits.standard.ItemSize.RECORD_CHAR_SIZE
import com.common.logger.MyLogger
//=============================================================================
//=============================================================================
object RecordParser extends MyLogger {
  //---------------------------------------------------------------------------
  private def getErrorParserPosition(pos:Int) =
    "." * (pos + 1) +
    "^" +
    "." * (RECORD_CHAR_SIZE - (pos+3))
  //---------------------------------------------------------------------------
  def run(s: String): R[_] = {
    if (s.length != RECORD_CHAR_SIZE) RecordError(s, s"The FITS records must have exactly: $RECORD_CHAR_SIZE char size")
    else {
      parse(s, Record.parseInput( _ )) match {
        case Parsed.Success(r, RECORD_CHAR_SIZE)      => r

        case Parsed.Success(_, index)                 =>
          val parsedInput = s.take(index)
          val remain = s.drop(index)
          val errorMessage =
            s +
            System.lineSeparator() +
              "                   " +
            getErrorParserPosition(index) +
            System.lineSeparator() +
            s" Error at position (first position 0): $index. Parsed input: '$parsedInput' Remain input: '$remain'"
          RecordError(s, errorMessage, "")

        case Parsed.Failure(expected, index, failure) => RecordError(s, s"Error parsing record at index: $index. Expected: '$expected' . Error: '${failure.trace().longAggregateMsg}")
     }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RecordParser.scala
//=============================================================================
