/**
 * Created byRafael Morales (rmorales@iaa.es)
 * Date 28/Sep/2022
 * Time 21h:19m
 * Description:
 *  http://www.lihaoyi.com/post/EasyParsingwithParserCombinators.html
 *  https://com-lihaoyi.github.io/fastparse/
 */
//=============================================================================
package com.common.fits.standard.block.record.parser
//=============================================================================
import fastparse._
import NoWhitespace._
//=============================================================================
import com.common.fits.standard.block.record.Record.{KEYWORD_VALUE_LOGICAL_FALSE, KEYWORD_VALUE_LOGICAL_TRUE}
import com.common.fits.standard.ItemSize.KEYWORD_CHAR_SIZE
import com.common.fits.standard.Keyword._
import com.common.fits.standard.dataType.complex.{ComplexFloat_64, ComplexFloat_32}
//=============================================================================
//=============================================================================
object Token {
  //---------------------------------------------------------------------------
  private def toDouble(s:String) = s.replace("D", "E").toDouble
  //---------------------------------------------------------------------------
  private def isLong(s:String) =
    parse(s, signedInteger(_)) match {
      case Parsed.Success(_, matchedSize) => matchedSize == s.length
      case _ => false
    }
  //---------------------------------------------------------------------------
  // relevant chars
  def space[_:P] = P(" ")
  private def quote[_:P] = P("'")
  private def doubleQuote[_:P] = P("''")
  def equal[_:P] = P("=")
  private def ampersand[_:P] = P("&")
  private def digit[_:P] = P(CharIn("0-9")).!.map( s => s )
  private def sign[_:P] = P("+" | "-").!.map( s => s )
  private def dot[_:P] = P(".").!.map( s => s )
  def keywordChar[_:P]= P(CharIn("0-9") | CharIn("A-Z")) | P("_") | P("-")
  //---------------------------------------------------------------------------
  //value indicator
  def valueIndicator[_:P] = P("= ")
  //---------------------------------------------------------------------------
  //strings
  def optSpaceSeq[_:P] = space.rep
  private def asciiString[_:P] =  P(CharPred { s => (s >= ' ') && (s <= '~') }).rep.!.map ( s=> s )
  def optAsciiString[_:P] =  asciiString.?
  def asciiStringButEqual[_: P] = P(CharPred { s => (s >= ' ') && (s <= '~') && (s != '=')}).rep(1).!.map ( s=> s )
  def asciiStringButSpace[_: P] = P(CharPred { s => (s >= ' ') && (s <= '~') && (s != ' ')}).rep(1).!.map ( s=> s )
  private def asciiCharButQuote[_: P] = P(CharPred { s => (s >= ' ') && (s <= '~') && (s != '\'')}).!

  //Constraint: A string text char is identical to an ascii text char except for the quote char; a quote char is represented by two
  //successive quote chars
  //Constraint: The begin quote and end quote are not part of the character string value but only serve as delimiters. Leading
  //spaces are significant trailing spaces are not
  //Constraint: The ending quote must not be immediately followed by a second quote
  def stringWithDoubleQuote[_:P]: P[String] =  (asciiCharButQuote | doubleQuote).rep.!.map( s=> s )
  def string[_:P]: P[String] =  quote ~ stringWithDoubleQuote ~ quote

  //Constraint: The begin quote, end quote, and ampersand are not part of the character-string value but only serve
  // respectively as delimiters or continuation indicator
  def partialString[_: P]: P[String] = quote ~ stringWithDoubleQuote ~ ampersand ~ quote
  //---------------------------------------------------------------------------
  //logical
  private def logical[_:P] = P(KEYWORD_VALUE_LOGICAL_TRUE | KEYWORD_VALUE_LOGICAL_FALSE).!.map( s => s == KEYWORD_VALUE_LOGICAL_TRUE )
  //---------------------------------------------------------------------------
  //number
  private def integer[_: P] = digit.rep(1)
  private def signedInteger[_: P] = sign.? ~ integer
  private def fractional[_:P] = dot ~ integer.?
  private def exponent[_:P] = P("E" | "D") ~ signedInteger
  //Constraint: At least one of the integer part and fraction part must be present
  //Integers and floats generete an ambiguity in the grammar. It is solved when they are used: 'complexValue' and 'recordValue'
  private def decimalNumber[_:P] = sign.? ~ ((integer ~ fractional) | integer | fractional)
  private def number[_:P] = (decimalNumber ~ exponent.?).!.map ( s=> s )
  //---------------------------------------------------------------------------
  //complex numbers
  private def complexStart[_:P] = P("(") ~ optSpaceSeq
  private def complexDivider[_:P] = optSpaceSeq ~ P(",") ~ optSpaceSeq
  private def complexEnd[_:P] = optSpaceSeq ~ P(")")
  private def complexValue[_:P]: P[RecordValue[_]] = (complexStart ~ number ~ complexDivider ~ number ~ complexEnd).map {
    case (a:String, b:String) =>
      if (isLong(a) && isLong(b))
        RecordValueComplexInteger(ComplexFloat_32(a.toLong, b.toLong))
      else
        RecordValueComplexFloat(ComplexFloat_64(toDouble(a), toDouble(b)))
  }
  //---------------------------------------------------------------------------
  //keywords
  def keywordComment[_: P] = P(KEYWORD_COMMENT ~ space)
  def keywordHistory[_: P] = P(KEYWORD_HISTORY ~ space)
  def keywordBlank[_: P] = P(space.rep(exactly = KEYWORD_CHAR_SIZE))
  def keywordContinue[_: P] = P(KEYWORD_CONTINUE)
  def keywordEnd[_: P] = P(KEYWORD_END)
  //---------------------------------------------------------------------------
  //comment value
  def commentValue[_: P] = P("/" ~ optAsciiString.!).map(s => s.trim)
  //---------------------------------------------------------------------------
  //keyword value
  //Constraint: The total number of characters in the keyword field must be exactly equal to 8
  def keyword[_: P] =
    ( keywordChar.rep(exactly = 1) ~ P(space.rep(exactly = 7)) |
      keywordChar.rep(exactly = 2) ~ P(space.rep(exactly = 6)) |
      keywordChar.rep(exactly = 3) ~ P(space.rep(exactly = 5)) |
      keywordChar.rep(exactly = 4) ~ P(space.rep(exactly = 4)) |
      keywordChar.rep(exactly = 5) ~ P(space.rep(exactly = 3)) |
      keywordChar.rep(exactly = 6) ~ P(space.rep(exactly = 2)) |
      keywordChar.rep(exactly = 7) ~ P(space.rep(exactly = 1)) |
      keywordChar.rep(exactly = 8)).!.map(s => s.trim)
  //---------------------------------------------------------------------------
  //record
  def recordValue[_:P]: P[RecordValue[_]] =
    string.map(RecordValueString( _ )) |
    logical.map(RecordValueLogical( _ )) |
    number.map { s =>
        if (isLong(s)) RecordValueInteger(s.toLong)
        else RecordValueDouble(toDouble(s) )
    } |
    complexValue.map( s=> s )
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Token.scala
//=============================================================================
