/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Sep/2022
 * Time:  21h:06m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.block.record
//=============================================================================
import com.common.fits.standard.block.record.RecordNumber._
import com.common.fits.standard.dataType.complex.{ComplexFloat_32, ComplexFloat_64}
import com.common.fits.standard.block.record.RecordCharacter._
import com.common.fits.standard.ItemSize.{KEYWORD_CHAR_SIZE, RECORD_CHAR_SIZE}
import com.common.fits.standard.Keyword._
import com.common.fits.standard.sectionName.SectionName
import com.common.stream.MyOutputStream
import com.common.util.string.MyString.{leftPadding, rightPadding}
//=============================================================================
//=============================================================================
object Record {
  //-----------------------------------------------------------------------------
  type RecordSeq = Array[Record[_]] //Existential type: https://pjrt.medium.com/existential-types-in-scala-6321f19c4a57
  //-----------------------------------------------------------------------------
  final val RECORD_HEADER_PADDING_STRING = " "
  //-----------------------------------------------------------------------------
  final val VALUE_INDICATOR = "= "
  private final val STRING_ENCLOSER = "'"
  private final val COMMENT_DIVIDER = " / "
  //---------------------------------------------------------------------------
  //logical record value
  final val KEYWORD_VALUE_LOGICAL_TRUE  = "T"
  final val KEYWORD_VALUE_LOGICAL_FALSE = "F"
  //---------------------------------------------------------------------------
  //applicable for values: logical, integer and float
  //Section '4.2.2 Logical'
  //Section '4.2.3 Integer number'
  //Section '4.2.4 Real floating-point number'
  final val PADDING_DEFAULT_VALUE_CHAR_SIZE =  30 - (KEYWORD_CHAR_SIZE + VALUE_INDICATOR.size)

  //string, record comments
  final val PADDING_VALUE_MAX_CHAR_SIZE     =  RECORD_CHAR_SIZE - KEYWORD_CHAR_SIZE - VALUE_INDICATOR.size  //68

  //quotes included
  final val PADDING_STRING_VALUE_CHAR_SIZE  =  PADDING_VALUE_MAX_CHAR_SIZE
  //---------------------------------------------------------------------------
  //(keyword,section of deprecation,is indexed)
  def DEPRECATED_KEYWORDS_SEQ = Seq (
      (KEYWORD_BLOCKED ,SectionName.SECTION_4_4_2_1,false)
    , (KEYWORD_CROTA   ,SectionName.SECTION_8_1,true)
    , (KEYWORD_EPOCH   ,SectionName.SECTION_8_1,false)
    , (KEYWORD_RADECSYS,SectionName.SECTION_8_1,false)
    , (KEYWORD_RESTFREQ,SectionName.SECTION_8_1,false)
    )
  //---------------------------------------------------------------------------
  def getLongCommentSeq(recordSeq: RecordSeq): Array[RecordString] = {
    recordSeq.flatMap { r =>
      r match {
        case t: RecordString =>
          val flag = t.flag
          if ((flag == RECORD_STRING_FLAG_LONG_COMMENT_START) ||
             (flag == RECORD_STRING_FLAG_LONG_COMMENT_CONTINUE) ||
             (flag == RECORD_STRING_FLAG_LONG_COMMENT_END))
            Some(t)
          else None
        case _ => None
      }
    }
  }
  //---------------------------------------------------------------------------
  def getNaxisRecordSeq(axisSeq: Array[Long]) =
    axisSeq.zipWithIndex.map { case (axis, i) =>
      RecordInteger(KEYWORD_NAXIS + (i + 1), axis, s"items in data axis ${i + 1}")
    }
  //---------------------------------------------------------------------------
  def cloneWithNewValue[T](record: Record[_], value: T) =
    record match {
      case r: RecordString         => RecordString(r.keyword, value.asInstanceOf[String], r.comment, r.id)
      case r: RecordBlank          => RecordBlank(value.asInstanceOf[String], r.id)
      case r: RecordComment        => RecordComment(value.asInstanceOf[String], r.id)
      case r: RecordGeneralComment => RecordGeneralComment(r.keyword, value.asInstanceOf[String], r.id)
      case r: RecordHistory        => RecordHistory(value.asInstanceOf[String], r.id)
      case _: RecordEnd            => RecordEnd()
      case r: RecordLogical        => RecordLogical(r.keyword, value.asInstanceOf[Boolean], r.comment, r.id)
      case r: RecordInteger        => RecordInteger(r.keyword, value.asInstanceOf[Long], r.comment, r.id)
      case r: RecordFloat          => RecordFloat(r.keyword, value.asInstanceOf[Double], r.comment, r.id)
      case r: RecordComplexInteger => RecordComplexInteger(r.keyword, value.asInstanceOf[ComplexFloat_32], r.comment, r.id)
      case r: RecordComplexFloat   => RecordComplexFloat(r.keyword, value.asInstanceOf[ComplexFloat_64], r.comment, r.id)
   }
  //---------------------------------------------------------------------------
  def cloneWithNewID(record: Record[_], id: Long) =
    record match {
      case r: RecordString         => RecordString(r.keyword, r.value, r.comment, id)
      case r: RecordBlank          => RecordBlank(r.value,id)
      case r: RecordComment        => RecordComment(r.value, id)
      case r: RecordGeneralComment => RecordGeneralComment(r.keyword, r.value, id)
      case r: RecordHistory        => RecordHistory(r.value, id)
      case _: RecordEnd            => RecordEnd(id)
      case r: RecordLogical        => RecordLogical(r.keyword, r.value, r.comment, id)
      case r: RecordInteger        => RecordInteger(r.keyword, r.value, r.comment, id)
      case r: RecordFloat          => RecordFloat(r.keyword, r.value, r.comment, id)
      case r: RecordComplexInteger => RecordComplexInteger(r.keyword, r.value, r.comment, id)
      case r: RecordComplexFloat   => RecordComplexFloat(r.keyword, r.value, r.comment, id)
    }
  //---------------------------------------------------------------------------
  def formatKeyword(k: String) = rightPadding(k, KEYWORD_CHAR_SIZE)
  //---------------------------------------------------------------------------
  def formatComment(s: String): String = if (s.isEmpty) "" else COMMENT_DIVIDER + s
  //---------------------------------------------------------------------------
  def formatValue(record: Record[_], addValueIndicator: Boolean = true): String = {

    val value = record.value
    val valueIndicator = if (addValueIndicator) VALUE_INDICATOR else ""
    record match {
      case _: RecordGeneralComment => rightPadding(value.toString, PADDING_STRING_VALUE_CHAR_SIZE)
      case _: RecordComment        => rightPadding(value.toString, PADDING_STRING_VALUE_CHAR_SIZE)
      case _: RecordHistory        => rightPadding(value.toString, PADDING_STRING_VALUE_CHAR_SIZE)
      case _: RecordEnd            => rightPadding(value.toString, PADDING_STRING_VALUE_CHAR_SIZE)
      case x: RecordComplexInteger => leftPadding(valueIndicator + x.value.format, PADDING_STRING_VALUE_CHAR_SIZE)
      case x: RecordComplexFloat   => leftPadding(valueIndicator + x.value.format, PADDING_STRING_VALUE_CHAR_SIZE)
      case _ =>
        value match {
          case v: Boolean =>
            valueIndicator +
              leftPadding(if (v) KEYWORD_VALUE_LOGICAL_TRUE else KEYWORD_VALUE_LOGICAL_FALSE
                          , PADDING_DEFAULT_VALUE_CHAR_SIZE)
          case x: String =>
            if (x.isEmpty) "''"
            else {
              val s =
                if (record.keyword == KEYWORD_XTENSION)
                  s"$STRING_ENCLOSER${rightPadding(x, KEYWORD_CHAR_SIZE)}$STRING_ENCLOSER"
                else
                  s"$STRING_ENCLOSER$x$STRING_ENCLOSER"
              valueIndicator + s
            }
          case _ =>
            valueIndicator + leftPadding(value.toString, PADDING_DEFAULT_VALUE_CHAR_SIZE)
        }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import Record._
trait Record[T] {
  //---------------------------------------------------------------------------
  val keyword: String
  val value: T
  val comment: String
  val id: Long
  //---------------------------------------------------------------------------
  override def toString(): String = getFormatted
  //---------------------------------------------------------------------------
  def getFormatted() =
    rightPadding(formatKeyword(keyword) +
                 formatValue(this) +
                 formatComment(comment)
                , RECORD_CHAR_SIZE)
  //---------------------------------------------------------------------------
  def getAsCsv(columnDivider:String) =
    formatKeyword(keyword) + columnDivider +
    formatValue(this)  + columnDivider +
    formatComment(comment) + columnDivider +
    getClass.getName.split("\\$").last
  //---------------------------------------------------------------------------
  def getByteSeq() = getFormatted.getBytes()
  //---------------------------------------------------------------------------
  def save(stream: MyOutputStream) = stream.write(getByteSeq,closeIt = false)
  //---------------------------------------------------------------------------
  def isEmpty = getFormatted.trim.isEmpty
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Record.scala
//=============================================================================
