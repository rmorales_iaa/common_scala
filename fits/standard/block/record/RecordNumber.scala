/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  29/Sep/2022
 * Time:  11h:21m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.block.record
//=============================================================================
import com.common.fits.standard.dataType.complex.{ComplexFloat_32, ComplexFloat_64}
//=============================================================================
//=============================================================================
object RecordNumber {
  //---------------------------------------------------------------------------
  case class RecordLogical(keyword: String
                           , value: Boolean
                           , comment: String = ""
                           , id: Long = 0) extends RecordNumber[Boolean]
  //--------------------------------------------------------------------------
  case class RecordInteger(keyword: String
                           , value: Long
                           , comment: String = ""
                           , id: Long = 0) extends RecordNumber[Long]

  //--------------------------------------------------------------------------
  case class RecordFloat(keyword: String
                         , value: Double
                         , comment: String = ""
                         , id: Long = 0) extends RecordNumber[Double]

  //--------------------------------------------------------------------------
  case class RecordComplexInteger(keyword: String
                                  , value: ComplexFloat_32
                                  , comment: String = ""
                                  , id: Long = 0) extends RecordNumber[ComplexFloat_32]

  //--------------------------------------------------------------------------
  case class RecordComplexFloat(keyword: String
                                , value: ComplexFloat_64
                                , comment: String = ""
                                , id: Long = 0) extends RecordNumber[ComplexFloat_64]
  //--------------------------------------------------------------------------
}
//=============================================================================
trait RecordNumber[T] extends Record[T]
//=============================================================================
//End of file RecordNumber.scala
//=============================================================================
