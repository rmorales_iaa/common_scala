/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Sep/2022
 * Time:  17h:21m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.disconformity
//=============================================================================
import com.common.fits.standard.disconformity.Disconformity.StatsMap
import com.common.fits.standard.fits.FitsCtxt
//=============================================================================
//=============================================================================
object DisconfFatal {
  //---------------------------------------------------------------------------
  private val statsMap = new StatsMap()
  //---------------------------------------------------------------------------
  def apply(ctxt: FitsCtxt
            , standardSection: String
            , message: String
            , additionalInfo: String = ""): DisconfFatal =
    DisconfFatal(ctxt.streamName,standardSection,message,Array(additionalInfo))
  //---------------------------------------------------------------------------
  def getStatsMap = statsMap
  //---------------------------------------------------------------------------
}
//=============================================================================
import DisconfFatal._
case class DisconfFatal(name: String
                        , standardSection:String
                        , message:String
                        , additionalInfo: Array[String]) extends Disconformity {
  //---------------------------------------------------------------------------
  val severity:String = "Fatal"
  //---------------------------------------------------------------------------
  addToStats(this,statsMap)
  //---------------------------------------------------------------------------
  def getStatsMap: StatsMap = statsMap
  //---------------------------------------------------------------------------
  def print() = fatal(toString())
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file DisconfFatal.scala
//=============================================================================
