/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Sep/2022
 * Time:  17h:16m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.disconformity
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
import java.util.concurrent.ConcurrentHashMap
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object Disconformity extends MyLogger {
  //---------------------------------------------------------------------------
  type StatsMap = ConcurrentHashMap[String,ArrayBuffer[Disconformity]]
  //--------------------------------------------------------------------------
  private def formatLong(v: Long) = f"$v%03d"
  //---------------------------------------------------------------------------
  private def getStatCount(map:StatsMap) = {
    var count =  0L
    map.values().forEach { disconfSeq => count += disconfSeq.length}
    count
  }
  //---------------------------------------------------------------------------
  private def getStatCount(name: String, map: StatsMap) = {
    val seq = map.get(name)
    if (seq == null) 0
    else seq.size
  }
  //---------------------------------------------------------------------------
  private def getString(name: String,map: StatsMap, mapName:String): String = {
    val s =
      if (map.isEmpty)  s"None disconformity of type: $mapName"
      else {
        val disconfSeq = map.get(name)
        if (disconfSeq == null) s"None disconformity of type: $mapName"
        else
          disconfSeq.zipWithIndex.map { case (disconf,i) =>
            val disconfPos = s"    Disconformity :${i+1}/${disconfSeq.length}\n    "
            disconf match {
              case x: DisconfFatal => disconfPos + x.toString()
              case x: DisconfWarn  => disconfPos + x.toString()
            }
          }.mkString("\n    ......................................\n")
        }
    s"\n---------------------$mapName-----------------------------" +
    s"\n$s" +
    s"\n-----------------------------------------------------------"
  }
  //---------------------------------------------------------------------------
  def print(name: String) = {
    val totalDisconf = getStatCount(name, DisconfWarn.getStatsMap) +
                       getStatCount(name, DisconfFatal.getStatsMap)
    info(
      s"\n************************************************************************" +
      s"${if (totalDisconf == 0) s"\nNone disconformities found on: '$name'"
         else
           s"\n$totalDisconf Disconfomities of '$name'" +
           s"${getString(name, DisconfWarn.getStatsMap, " Warning ")}" +
           s"${getString(name, DisconfFatal.getStatsMap, "  Fatal  ")}" }" +
      s"\n************************************************************************"
    )
    info(Disconformity.getTotalStats(name))
  }
  //---------------------------------------------------------------------------
  private def getTotalStats(name: String) = {
    val disconfWarnCount  = getStatCount(name, DisconfWarn.getStatsMap)
    val disconfFatalCount = getStatCount(name, DisconfFatal.getStatsMap)
    s"\n++++++++++++++++++++++++++++++++++++++++++++++++" +
      s"\nTotal disconformities found in '$name'" +
      s"\n    Warning disconformities: ${formatLong(disconfWarnCount)}" +
      s"\n    Fatal   disconformities: ${formatLong(disconfFatalCount)}" +
      s"\n++++++++++++++++++++++++++++++++++++++++++++++++"
  }
  //---------------------------------------------------------------------------
  def getTotalStats(dirName: String, dirFitsCount: Long) = {
    val disconfWarnCount = getStatCount(DisconfWarn.getStatsMap)
    val disconfFatalCount = getStatCount(DisconfFatal.getStatsMap)
    s"\n................... Summary ......................" +
      s"\n${formatLong(disconfWarnCount + disconfFatalCount)} disconformities in $dirFitsCount FITS files found in directory: '$dirName'" +
      s"\n    Warning disconformities: ${formatLong(disconfWarnCount)}" +
      s"\n    Fatal   disconformities: ${formatLong(disconfFatalCount)}" +
      s"\n................................................"
  }
  //---------------------------------------------------------------------------
  def existAnyDisconformity =
    (getStatCount(DisconfWarn.getStatsMap) +
     getStatCount(DisconfFatal.getStatsMap))  > 0
  //---------------------------------------------------------------------------
  def hasFatalDisconformity(name:String) = {
    val disconfSeq = DisconfFatal.getStatsMap.get(name)
    disconfSeq != null && disconfSeq.length > 0
  }
  //---------------------------------------------------------------------------
  def hasWarnDisconformity(name: String) = {
    val disconfSeq = DisconfWarn.getStatsMap.get(name)
    disconfSeq != null && disconfSeq.length > 0
  }
  //---------------------------------------------------------------------------
  def clearAllDisconformity(name: String) : Unit = {
    DisconfWarn.getStatsMap.remove(name)
    DisconfFatal.getStatsMap.remove(name)
  }
  //---------------------------------------------------------------------------
  def getWarnigDisconformityCount(name: String) = {
    val stat = DisconfWarn.getStatsMap
    if (stat.isEmpty) 0
    else stat.get(name).length
  }
  //---------------------------------------------------------------------------
  def getFatalDisconformityCount(name: String) = {
    val stat = DisconfFatal.getStatsMap
    if (stat.isEmpty) 0
    else stat.get(name).length
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import Disconformity._
trait Disconformity extends MyLogger {
  //---------------------------------------------------------------------------
  val name              : String  //stream name
  val severity          : String
  val standardSection   : String
  val message           : String
  val additionalInfo    : Array[String]
  //---------------------------------------------------------------------------
  def getStatsMap: StatsMap
  //---------------------------------------------------------------------------
  def print(): Unit
  //---------------------------------------------------------------------------
  override def toString(): String = {
    s"Stream name   :'$name'" +
    s"\n    Severity      :$severity" +
    s"\n    FITS standard :Section $standardSection" +
    s"\n    Message       :$message" +
      (if (additionalInfo.isEmpty || additionalInfo.head.isEmpty) ""
      else s"\n    Extra info    :${additionalInfo.mkString("\n    Error position:")}")
  }
  //---------------------------------------------------------------------------
  protected def addToStats(disconf: Disconformity,statsMap: StatsMap): Unit = {
    var seq = statsMap.get(disconf.name)
    if (seq == null) {
      seq = ArrayBuffer[Disconformity]()
      statsMap.put(disconf.name,seq)
    }
    seq += disconf
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Disconformity.scala
//=============================================================================
