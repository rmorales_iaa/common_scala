/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  09/Feb/2021
 * Time:  20h:47m
 * Description:  It implements the tokens (keywords) defined in
 * FITS standard version 4.0 (2018 August 13)
 */
//=============================================================================
package com.common.fits.standard
//=============================================================================
//=============================================================================
object Keyword {
  //List of all keywords in the same oorder as they appear in the standard, section by section.
  //In one or more keywords are defined in previos sections they are ommited.
  //In case of keywords composed with indexes, only the constant part is defined.
  //---------------------------------------------------------------------------
  //4.2.1.2 Continued string (long-string) keywords
  final val KEYWORD_CONTINUE  = "CONTINUE"
  //---------------------------------------------------------------------------
  //4.4.1.1 primary header
  final val KEYWORD_SIMPLE    = "SIMPLE"
  final val KEYWORD_BITPIX    = "BITPIX"
  final val KEYWORD_NAXIS     = "NAXIS"
  final val KEYWORD_END       = "END"
  //---------------------------------------------------------------------------
  //4.4.1.2 Conforming extensions
  final val KEYWORD_XTENSION  = "XTENSION"
  final val KEYWORD_PCOUNT    = "PCOUNT"
  final val KEYWORD_GCOUNT    = "GCOUNT"  //number of groups present
  //---------------------------------------------------------------------------
  //4.4.2.1 General descriptive keywords
  final val KEYWORD_DATE      = "DATE"
  final val KEYWORD_ORIGIN    = "ORIGN"
  final val KEYWORD_EXTEND    = "EXTEND"
  final val KEYWORD_BLOCKED   = "BLOCKED"
  //---------------------------------------------------------------------------
  //4.4.2.2 Keywords describing observations
  final val KEYWORD_DATE_OBS  = "DATE-OBS"

  //DATEXXXX (BED,AVG,END) will be delared below, in the sections tat are defined

  final val KEYWORD_TELESCOP  = "TELESCOP"
  final val KEYWORD_INSTRUME  = "INSTRUME"
  final val KEYWORD_OBSERVER  = "OBSERVER"
  final val KEYWORD_OBJECT    = "OBJECT"
  final val KEYWORD_AUTHOR    = "AUTHOR"
  final val KEYWORD_REFERENC  = "REFERENC"
  //---------------------------------------------------------------------------
  //4.4.2.2 Comentary keywords
  final val KEYWORD_COMMENT   = "COMMENT"
  final val KEYWORD_HISTORY   = "HISTORY"
  //---------------------------------------------------------------------------
  //4.4.2.5 Keywords that describe arrays
  final val KEYWORD_BSCALE    = "BSCALE"
  final val KEYWORD_BZERO     = "BZERO"
  final val KEYWORD_BUNIT     = "BUNIT"
  final val KEYWORD_BLANK     = "BLANK"
  final val KEYWORD_DATAMAX   = "DATAMAX"
  final val KEYWORD_DATAMIN   = "DATAMIN"
  //---------------------------------------------------------------------------
  //4.4.2.6 extension keywords
  final val KEYWORD_EXTNAME   = "EXTNAME"
  final val KEYWORD_EXTVER    = "EXTVER"
  final val KEYWORD_EXTLEVEL  = "EXTLEVEL"
  final val KEYWORD_INHERIT   = "INHERIT"
  //---------------------------------------------------------------------------
  //4.4.2.7 Data-integrit keywords
  final val KEYWORD_DATASUM   = "DATASUM"
  final val KEYWORD_CHECKSUM  = "CHECKSUM"
  //---------------------------------------------------------------------------
  //6.1.1 Mandatory keywords
  final val KEYWORD_GROUPS    = "GROUPS"

  //---------------------------------------------------------------------------
  //6.1.2 Reserved keywords
  final val KEYWORD_PTYPE     = "PTYPE"
  final val KEYWORD_PSCAL     = "PSCAL"
  final val KEYWORD_PZERO     = "PZERO"

  //---------------------------------------------------------------------------
  //7.2.1 Mandatory keywords
  final val KEYWORD_TFIELDS   = "TFIELDS"
  final val KEYWORD_TBCOL     = "TBCOL"
  final val KEYWORD_TFORM     = "TFORM"

  //---------------------------------------------------------------------------
  //7.2.2 Other reserved keywords
  final val KEYWORD_TTYPE     = "TTYPE"
  final val KEYWORD_TUNIT     = "TUNIT"
  final val KEYWORD_TSCAL     = "TSCAL"
  final val KEYWORD_TZERO     = "TZERO"
  final val KEYWORD_TNULL     = "TNULL"
  final val KEYWORD_TDISP     = "TDISP"
  final val KEYWORD_TDMIN     = "TDMIN"
  final val KEYWORD_TDMAX     = "TDMAX"
  final val KEYWORD_TLMIN     = "TLMIN"
  final val KEYWORD_TLMAX     = "TLMAX"

  //---------------------------------------------------------------------------
  //7.3.2 Other reserved keywords
  final val KEYWORD_THEAP     = "THEAP"
  final val KEYWORD_TDIM      = "TDIM"

  //---------------------------------------------------------------------------
  //8.2 World-coordinate-system representations
  final val KEYWORD_WCSAXES   = "WCSAXES"
  final val KEYWORD_CTYPE     = "CTYPE"
  final val KEYWORD_CUNIT     = "CUNIT"
  final val KEYWORD_CRPIX     = "CRPIX"
  final val KEYWORD_CRVAL     = "CRVAL"
  final val KEYWORD_CDELT     = "CDELT"
  final val KEYWORD_CROTA     = "CROTA"
  final val KEYWORD_PC        = "PC"
  final val KEYWORD_CD        = "CD"
  final val KEYWORD_PV        = "PV"
  final val KEYWORD_PS        = "PS"
  final val KEYWORD_CRDER     = "CRDER"
  final val KEYWORD_CSYER     = "CSYER"

  //---------------------------------------------------------------------------
  //The following keywords are used as column name of tables and defined from a
  // FITS keyword

  //8.2.1 Alternative WCS axis descriptions
  //AsciiTable 22 Reserved WCS keywords
  //Completed with "8.3 Celestial-coordinate-system representations" see below
  //Completed with "8.4 Spectral-coordinate-system representations" see below
  //Completed with "8.4.1 Spectral coordinate reference frames" see below


  final val KEYWORD_CTYP      = "CTYP"
  final val KEYWORD_CTY       = "CTY"
  final val KEYWORD_TCTYP     = "TCTYP"
  final val KEYWORD_TCTY      = "TCTY"

  // KEYWORD_TRPOS defined in section 9.2.3
  final val KEYWORD_TRPOS     = "TRPOS"

  //KEYWORD_TRDIR defined in section  9.2.4
  final val KEYWORD_TRDIR     = "TRDIR"

  final val KEYWORD_CUNI      = "CUNI"
  final val KEYWORD_CUN       = "CUN"
  final val KEYWORD_TCUNI     = "TCUNI"
  final val KEYWORD_TCUN      = "TCUN"

  final val KEYWORD_CRVL      = "CRVL"
  final val KEYWORD_TCRVL     = "TCRVL"

  final val KEYWORD_CDLT      = "CDLT"
  final val KEYWORD_CDL       = "CDE"
  final val KEYWORD_TCDLT     = "TCDLT"
  final val KEYWORD_TCDE      = "TCDE"

  final val KEYWORD_CRPX      = "CRPX"
  final val KEYWORD_TCRPX     = "TCRPX"

  final val KEYWORD_CROT      = "CROT"
  final val KEYWORD_TCROT     = "TCROT"

  final val KEYWORD_TPC       = "TPC"
  final val KEYWORD_TP        = "TP"

  final val KEYWORD_TCD       = "TCD"
  final val KEYWORD_TPV       = "TPV"

  final val KEYWORD_V         = "V"
  final val KEYWORD_TV        = "TV"

  final val KEYWORD_S         = "S"
  final val KEYWORD_TPS       = "TPS"

  final val KEYWORD_WCSNAME   = "WCSNAME"
  final val KEYWORD_WCSN      = "WCSN"
  final val KEYWORD_TWCS      = "TWCS"

  final val KEYWORD_CNA       = "CNA"
  final val KEYWORD_TCNA      = "TCNA"

  final val KEYWORD_CRD       = "CRD"
  final val KEYWORD_CRDE      = "CRDE"
  final val KEYWORD_TCRD      = "TCRD"

  final val KEYWORD_TIMEDEL   = "TIMEDEL"
  final val KEYWORD_TIMEPIXR   = "TIMEPIXR"

  final val KEYWORD_CSY       = "CSY"
  final val KEYWORD_TCSY      = "TCSY"

  final val KEYWORD_WCST      = "WCST"

  final val KEYWORD_WCSX      = "WCSX"

  final val KEYWORD_LONP      = "LONP"

  final val KEYWORD_LATP      = "LATP"

  final val KEYWORD_EQUI      = "EQUI"

  final val KEYWORD_RADECSYS  = "RADECSYS"
  final val KEYWORD_RADE      = "RADE"

  final val KEYWORD_RESTFREQ  = "RESTFREQ"
  final val KEYWORD_RFRQ      = "RFRQ"

  final val KEYWORD_RWAV      = "RWAV"

  final val KEYWORD_SPEC      = "SPEC"

  final val KEYWORD_SOBS      = "SOBS"

  final val KEYWORD_SSRC      = "SSRC"

  final val KEYWORD_OBSGX     = "OBSGX"
  final val KEYWORD_OBSGY     = "OBSGY"
  final val KEYWORD_OBSGZ     = "OBSGZ"

  final val KEYWORD_VSYS      = "VSYS"

  final val KEYWORD_ZSOU      = "ZSOU"

  final val KEYWORD_VANG      = "VANG"

  //AsciiTable 22 Date-time related keywords
  final val KEYWORD_DOBS      = "DOBS"
  final val KEYWORD_MJDOB     = "MJDOB"
  final val KEYWORD_DAVG      = "DAVG"
  final val KEYWORD_MJDA      = "MJDA"

  // KEYWORD_CZPHS defined in section 9.6
  final val KEYWORD_CZPH      = "CZPH"
  final val KEYWORD_CZP       = "CZP"
  final val KEYWORD_TCZPH     = "TCZPH"
  final val KEYWORD_TCZP      = "TCZP"

  // KEYWORD_CPERI defined in section 9.6
  final val KEYWORD_CPER      = "CPER"
  final val KEYWORD_CPR       = "CPR"
  final val KEYWORD_TCPER     = "TCPER"
  final val KEYWORD_TCPR      = "TCPR"
  //---------------------------------------------------------------------------
  //8.3 Celestial-coordinate-system representations
  final val KEYWORD_RADESYS   = "RADESYS"
  final val KEYWORD_EQUINOX   = "EQUINOX"
  final val KEYWORD_EPOCH     = "EPOCH"
  //KEYWORD_DATE_OBS defined in "4.4.2.2 Keywords describing observations"
  final val KEYWORD_MJD_OBS   = "MJD-OBS"
  final val KEYWORD_LONPOLE   = "LONPOLE"
  final val KEYWORD_LATPOLE   = "LATPOLE"
  //---------------------------------------------------------------------------
  //8.4 Spectral-coordinate-system representations
  final val KEYWORD_CNAME     = "CNAME"
  final val KEYWORD_RESTFRQ   = "RESTFRQ"
  final val KEYWORD_RESTWAV   = "RESTWAV"
  //---------------------------------------------------------------------------
  //8.4.1 Spectral coordinate reference frames
  final val KEYWORD_DATE_AVG  = "DATE-AVG"
  final val KEYWORD_MJD_AVG   = "MJD-AVG"
  final val KEYWORD_SPECSYS   = "SPECSYS"
  final val KEYWORD_SSYSOBS   = "SSYSOBS"
  final val KEYWORD_OBSGEO_X  = "OBSGEO_X"
  final val KEYWORD_OBSGEO_Y  = "OBSGEO_Y"
  final val KEYWORD_OBSGEO_Z  = "OBSGEO_Z"
  final val KEYWORD_SSYSSRC   = "SSYSSRC"
  final val KEYWORD_VELOSYS   = "VELOSYS"
  final val KEYWORD_ZSOURCE   = "ZSOURCE"
  final val KEYWORD_VELANGL   = "VELANGL"

  //---------------------------------------------------------------------------
  //9.2.1 Time scale
  final val KEYWORD_TIMESYS   = "TIMESYS"
  //---------------------------------------------------------------------------
  //9.2.2. Time reference value
  final val KEYWORD_MJDREF    = "MJDREF"
  final val KEYWORD_JDREF     = "JDREF"
  final val KEYWORD_DATEREF   = "DATEREF"
  final val KEYWORD_MJDREFI   = "MJDREFI"
  final val KEYWORD_MJDREFF   = "MJDREFF"
  final val KEYWORD_JDREFI    = "JDREFI"
  final val KEYWORD_JDREFF    = "JDREFF"

  //---------------------------------------------------------------------------
  //9.2.3 Time reference position
  final val KEYWORD_TREFPOS   = "TREFPOS"
  final val KEYWORD_OBSGEO_B  = "OBSGEO_B"
  final val KEYWORD_OBSGEO_L  = "OBSGEO_L"
  final val KEYWORD_OBSGEO_H  = "OBSGEO_H"
  final val KEYWORD_OBSORBIT  = "OBSORBIT"
  //---------------------------------------------------------------------------
  //9.2.4 Time reference direction
  final val KEYWORD_TREFDIR   = "TREFDIR"

  //---------------------------------------------------------------------------
  //9.2.5 Solar system ephemeris
  final val KEYWORD_PLEPHEM   = "PLEPHEM"

  //---------------------------------------------------------------------------
  //9.3 Time unit
  final val KEYWORD_TIMEUNIT  = "TIMEUNIT"

  //---------------------------------------------------------------------------
  //9.4.1 Time offset
  final val KEYWORD_TIMEOFFS  = "TIMEOFFS"


  //---------------------------------------------------------------------------
  //9.4.3 Time errors
  final val KEYWORD_TIMSYER   = "TIMSYER"
  final val KEYWORD_TIMRDER   = "TIMRDER"

  //---------------------------------------------------------------------------
  //9.5 Global time keywords
  final val KEYWORD_DATE_BEG  = "DATE-BEG"
  final val KEYWORD_DATE_END  = "DATE-END"
  final val KEYWORD_MJD_BEG   = "MJD-BEG"
  final val KEYWORD_MJD_END   = "MJD-END"
  final val KEYWORD_TSTART    = "TSTART"
  final val KEYWORD_TSTOP     = "TSTOP"
  final val KEYWORD_JEPOCH    = "JEPOCH"
  final val KEYWORD_BEPOCH    = "BEPOCH"
  //---------------------------------------------------------------------------
  //9.6 Other time-coordinate axes
  final val KEYWORD_CZPHS     = "CZPHS"
  final val KEYWORD_CPERI     = "CPERI"
  final val KEYWORD_XPOSURE   = "XPOSURE"
  final val KEYWORD_TELAPSE   = "TELAPSE"

  //---------------------------------------------------------------------------
  //10.1.1 Required keywords
  final val KEYWORD_ZIMAGE    = "ZIMAGE"
  final val KEYWORD_ZCMPTYPE  = "ZCMPTYPE"
  final val KEYWORD_ZBITPIX   = "ZBITPIX"
  final val KEYWORD_ZNAXIS    = "ZNAXIS"
  final val KEYWORD_ZTILE     = "ZTILE"
  final val KEYWORD_ZNAME     = "ZNAME"
  final val KEYWORD_ZVAL      = "ZVAL"
  final val KEYWORD_ZMASKCMP  = "ZMASKCMP"
  final val KEYWORD_ZQUANTIZ  = "ZQUANTIZ"
  final val KEYWORD_ZDITHER0  = "ZDITHER0"
  final val KEYWORD_ZSIMPLE   = "ZSIMPLE"
  final val KEYWORD_ZEXTEND   = "ZEXTEND"
  final val KEYWORD_ZBLOCKED  = "ZBLOCKED"
  final val KEYWORD_ZTENSION  = "ZTENSION"
  final val KEYWORD_ZPCOUNT   = "ZPCOUNT"
  final val KEYWORD_ZGCOUNT   = "ZGCOUNT"
  final val KEYWORD_ZHECKSUM  = "ZHECKSUM"
  final val KEYWORD_ZDATASUM  = "ZDATASUM"
  //---------------------------------------------------------------------------
  //10.1.3 AsciiTable columns
  final val KEYWORD_ZSCALE    = "ZSCALE"
  final val KEYWORD_ZZERO     = "ZZERO"
  final val KEYWORD_ZBLANK    = "ZBLANK"

  //---------------------------------------------------------------------------
  //10.3.1 Required keywords
  final val KEYWORD_ZTABLE    = "ZTABLE"
  final val KEYWORD_ZNAXIS1   = "ZNAXIS1"
  final val KEYWORD_ZNAXIS2   = "ZNAXIS2"
  final val KEYWORD_ZFORM     = "ZFORM  "
  final val KEYWORD_ZCTYP     = "ZCTYP"
  final val KEYWORD_ZTILELEN  = "ZTILELEN"

  //---------------------------------------------------------------------------
  //10.3.3 Compression directive keywords
  final val KEYWORD_FZTILELN  = "FZTILELN"
  final val KEYWORD_FZALGOR   = "FZALGOR"
  final val KEYWORD_FZALG     = "FZALG"

  //---------------------------------------------------------------------------
  //10.3.4 Other reserved keywords
  final val KEYWORD_ZHEAP     = "ZHEAP"
  //---------------------------------------------------------------------------
  //NOT in the standard
  //4.12.4 HIERARCH Convention for Extended Keyword Names (https://heasarc.gsfc.nasa.gov/fitsio/c/f_user/node28.html)
  final val KEYWORD_HIERARCHY  = "HIERARCHY"

  //used internally to indicate a sub-image FITS
  final val KEYWORD_SUBIMAGE   = "SUB_IMAGE"
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Keywords.scala
//=============================================================================
