/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Nov/2022
 * Time:  10h:30m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.dataType
//=============================================================================
//=============================================================================
import com.common.fits.standard.dataType.DataType._
//=============================================================================
object Tzero {
  //-----------------------------------------------------------------------------
  //Singleton Objects
  val T_ZERO_U_8  = TzeroU_8()
  val T_ZERO_I_16 = TzeroInt_16()
  val T_ZERO_I_32 = TzeroInt_32()
  val T_ZERO_I_64 = TzeroInt_64()
  //-----------------------------------------------------------------------------
  def isValid(bitPix: Int) = {
    bitPix.toString match {
      case DATA_TYPE_ID_UINT_8 => true
      case DATA_TYPE_ID_INT_16 => true
      case DATA_TYPE_ID_INT_32 => true
      case DATA_TYPE_ID_INT_64 => true
      case _ => false
    }
  }
  //-----------------------------------------------------------------------------
  def getTzero(bitPix: Int): Tzero  = {
    bitPix.toString match {
      case DATA_TYPE_ID_UINT_8 => T_ZERO_U_8
      case DATA_TYPE_ID_INT_16 => T_ZERO_I_16
      case DATA_TYPE_ID_INT_32 => T_ZERO_I_32
      case DATA_TYPE_ID_INT_64 => T_ZERO_I_64
    }
  }
  //-----------------------------------------------------------------------------
}
//=============================================================================
trait Tzero {
  //---------------------------------------------------------------------------
  val id: String
  val byteSize: Int
  val undefinedValue: Long
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class TzeroU_8() extends Tzero {val id = "B"; val byteSize = 1; val undefinedValue = -128}
sealed case class TzeroInt_16() extends Tzero {val id = "I"; val byteSize = 2; val undefinedValue = 32768}
sealed case class TzeroInt_32() extends Tzero {val id = "J"; val byteSize = 4; val undefinedValue = 0x80000000} //−2147483648
sealed case class TzeroInt_64() extends Tzero {val id = "K"; val byteSize = 8; val undefinedValue = 0x800000000000L} //// -9223372036854775808L
//=============================================================================

//=============================================================================
//End of file Tzero.scala
//=============================================================================
