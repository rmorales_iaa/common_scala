/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  01/Aug/2023
  * Time:  12h:32m
  * Description: None
  */
//=============================================================================
package com.common.fits.standard.dataType
//=============================================================================
import java.nio.{ByteBuffer, ByteOrder}
//=============================================================================
//=============================================================================
object Conversion {
  //---------------------------------------------------------------------------
  def byteSeqToShortSeq(byteSeq: Array[Byte]
                        , byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN) = {
    require((byteSeq.size % 2) == 0, s"'byteSeqToShortSeq'. Array must have even size. Current size: ${byteSeq.size}")
    val buffer = ByteBuffer.wrap(byteSeq)
      .order(byteOrder)
      .asShortBuffer()
    val s = new Array[Short](buffer.remaining())
    buffer.get(s)
    s
  }

  //---------------------------------------------------------------------------
  def byteSeqToIntSeq(byteSeq: Array[Byte]
                      , byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN) = {

    require((byteSeq.size % 4) == 0, s"'byteSeqToIntSeq'. Array size must be multiple of 4. Current size: ${byteSeq.size}")
    val buffer = ByteBuffer.wrap(byteSeq)
      .order(byteOrder)
      .asIntBuffer()
    val s = new Array[Int](buffer.remaining())
    buffer.get(s)
    s
  }

  //---------------------------------------------------------------------------
  def byteSeqToLongSeq(byteSeq: Array[Byte]
                       , byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN) = {
    require((byteSeq.size % 8) == 0, s"'byteSeqToLongSeq'. Array size must be multiple of 8. Current size: ${byteSeq.size}")
    val buffer = ByteBuffer.wrap(byteSeq)
      .order(byteOrder)
      .asLongBuffer()
    val s = new Array[Long](buffer.remaining())
    buffer.get(s)
    s
  }

  //---------------------------------------------------------------------------
  def byteSeqToFloatSeq(byteSeq: Array[Byte]
                        , byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN) = {
    require((byteSeq.size % 4) == 0, s"'byteSeqToFloatSeq'. Array size must be multiple of 4. Current size: ${byteSeq.size}")
    val buffer = ByteBuffer.wrap(byteSeq)
      .order(byteOrder)
      .asFloatBuffer()
    val s = new Array[Float](buffer.remaining())
    buffer.get(s)
    s
  }

  //---------------------------------------------------------------------------
  def byteSeqToDoubleSeq(byteSeq: Array[Byte]
                         , byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN) = {
    require((byteSeq.size % 8) == 0, s"'byteSeqToDoubleSeq'. Array size must be multiple of 8. Current size: ${byteSeq.size}")
    val buffer = ByteBuffer.wrap(byteSeq)
      .order(byteOrder)
      .asDoubleBuffer()
    val s = new Array[Double](buffer.remaining())
    buffer.get(s)
    s
  }
  //---------------------------------------------------------------------------
  def shortToByteSeq(v: Short): Array[Byte] =
    ByteBuffer.allocate(2).putShort(v).array
  //---------------------------------------------------------------------------
  def intToByteSeq(v: Int): Array[Byte] =
    ByteBuffer.allocate(4).putInt(v).array
  //---------------------------------------------------------------------------
  def longToByteSeq(v: Long): Array[Byte] =
    ByteBuffer.allocate(8).putLong(v).array
  //---------------------------------------------------------------------------
  def floatToByteSeq(v: Float): Array[Byte] =
    ByteBuffer.allocate(4).putFloat(v).array
  //---------------------------------------------------------------------------
  def doubleToByteSeq(v: Double): Array[Byte] =
    ByteBuffer.allocate(8).putDouble(v).array
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Conversion.scala
//=============================================================================