/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  17/Oct/2022
 * Time:  19h:57m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.dataType
//=============================================================================
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.dataType.Conversion._
import com.common.fits.standard.dataType.complex.{ComplexFloat_32, ComplexFloat_64}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.BinTableDataType._
import com.common.util.util.Util
//=============================================================================
import scala.reflect.ClassTag
import scala.reflect.runtime.universe.TypeTag
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object DataType {
  //===========================================================================
  //Singleton Objects
  val DATA_TYPE_U_INT_8          = DataTypeU_Int_8()
  val DATA_TYPE_INT_16           = DataTypeInt_16()
  val DATA_TYPE_INT_32           = DataTypeInt_32()
  val DATA_TYPE_INT_64           = DataTypeInt_64()
  val DATA_TYPE_FLOAT_32         = DataTypeFloat_32()
  val DATA_TYPE_FLOAT_64         = DataTypeFloat_64()
  val DATA_TYPE_COMPLEX_INT_64   = DataTypeComplexFloat_32()
  val DATA_TYPE_COMPLEX_FLOAT_64 = DataTypeComplexFloat_64()

  val DATA_TYPE_UNKNOWN  = DataTypeUnknown()
  //---------------------------------------------------------------------------
  final val DATA_TYPE_ID_UINT_8   =   "8"
  final val DATA_TYPE_ID_INT_16   =  "16"
  final val DATA_TYPE_ID_INT_32   =  "32"
  final val DATA_TYPE_ID_INT_64   =  "64"
  final val DATA_TYPE_ID_FLOAT_32 = "-32"
  final val DATA_TYPE_ID_FLOAT_64 = "-64"

  final val DATA_TYPE_ID_UNKNOWN  =  "UNKNOWN"

  final val DATA_TYPE_SEQ = Seq(
      DATA_TYPE_ID_UINT_8
    , DATA_TYPE_ID_INT_16
    , DATA_TYPE_ID_INT_32
    , DATA_TYPE_ID_INT_64
    , DATA_TYPE_ID_FLOAT_32
    , DATA_TYPE_ID_FLOAT_64
    , DATA_TYPE_COMPLEX_INT_64
    , DATA_TYPE_COMPLEX_FLOAT_64
  )
  //---------------------------------------------------------------------------
  def isValid(s:String) = DATA_TYPE_SEQ.contains(s)
  //---------------------------------------------------------------------------
  def build(s:Long) : DataType[_] = build(s.toString)
  //---------------------------------------------------------------------------
  def build(s:String) : DataType[_] = {
    s match {
      case DATA_TYPE_ID_UINT_8   => DATA_TYPE_U_INT_8
      case DATA_TYPE_ID_INT_16   => DATA_TYPE_INT_16
      case DATA_TYPE_ID_INT_32   => DATA_TYPE_INT_32
      case DATA_TYPE_ID_INT_64   => DATA_TYPE_INT_64
      case DATA_TYPE_ID_FLOAT_32 => DATA_TYPE_FLOAT_32
      case DATA_TYPE_ID_FLOAT_64 => DATA_TYPE_FLOAT_64
      case _                     => DATA_TYPE_UNKNOWN
    }
  }
  //---------------------------------------------------------------------------
  def isInteger(d:DataType[_]): Boolean = {
    d match {
      case DATA_TYPE_U_INT_8  => true
      case DATA_TYPE_INT_16   => true
      case DATA_TYPE_INT_32   => true
      case DATA_TYPE_INT_64   => true
      case DATA_TYPE_FLOAT_32 => false
      case DATA_TYPE_FLOAT_64 => false
      case _                  => false
    }
  }
  //---------------------------------------------------------------------------
  def getString(recordMap: RecordMap,keyword:String,dataType: DataType[_]) : Option[String] =
    dataType match {

      case _: DataTypeU_Int_8 =>
        Some(recordMap.getOptTableFieldValue[Short](keyword).getOrElse(return None).toString)

      case _: DataTypeInt_16 =>
        Some(recordMap.getOptTableFieldValue[Short](keyword).getOrElse(return None).toString)

      case _: DataTypeInt_32 =>
        Some(recordMap.getOptTableFieldValue[Int](keyword).getOrElse(return None).toString)

      case _: DataTypeInt_64 =>
        Some(recordMap.getOptTableFieldValue[Long](keyword).getOrElse(return None).toString)

      case _: DataTypeFloat_32 =>
        Some(recordMap.getOptTableFieldValue[Float](keyword).getOrElse(return None).toString)

      case _: DataTypeFloat_64 =>
        Some(recordMap.getOptTableFieldValue[Double](keyword).getOrElse(return None).toString)
    }

  //---------------------------------------------------------------------------
  def getString(v: Double, dataType: DataType[_]): String =
    dataType match {
      case _: DataTypeU_Int_8         => Math.round(v).toShort.toString
      case _: DataTypeInt_16          => Math.round(v).toShort.toString
      case _: DataTypeInt_32          => Math.round(v).toInt.toString
      case _: DataTypeInt_64          => Math.round(v).toString
      case _: DataTypeFloat_32        => v.toFloat.toString
      case _: DataTypeComplexFloat_32 => v.toString
      case _: DataTypeComplexFloat_64 => v.toString
    }

  //--------------------------------------------------------------------------
  def padByteSeq(byteSeq: Array[Byte], size: Int) = {
    if (byteSeq.size == size) byteSeq
    else {
      val padByteCount = size - byteSeq.size
      Array.fill[Byte](padByteCount)(0) ++ byteSeq
    }
  }
  //---------------------------------------------------------------------------
  def getFirstValueAsLong(byteSeq: Array[Byte]
                          , itemByteSize: Int): Long =
    itemByteSize match {
      case DATA_TYPE_U_INT_8.byteSize =>
        Util.unsignedByteToInt(DATA_TYPE_U_INT_8.getFirstValue(byteSeq))
      case DATA_TYPE_INT_16.byteSize  =>
         DATA_TYPE_INT_16.getFirstValue(byteSeq)
      case DATA_TYPE_INT_32.byteSize  =>
         DATA_TYPE_INT_32.getFirstValue(byteSeq)
      case DATA_TYPE_INT_64.byteSize  =>
         DATA_TYPE_INT_64.getFirstValue(byteSeq)
    }
  //---------------------------------------------------------------------------
  def getStringFromFloat(byteSeq: Array[Byte]
                         , itemByteSize: Int): String = {
    itemByteSize match {
      case DATA_TYPE_FLOAT_32.byteSize =>
        DATA_TYPE_FLOAT_32.getFirstValue(padByteSeq(byteSeq,itemByteSize)).toString
      case DATA_TYPE_FLOAT_64.byteSize =>
        DATA_TYPE_FLOAT_64.getFirstValue(padByteSeq(byteSeq,itemByteSize)).toString
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//T is the storage type bigger enough to store the data
trait DataType[T] {
  //---------------------------------------------------------------------------
  val id:String
  val name:String
  val bitSize:Short   //not byte due to complex number
  val byteSize:Short  //not byte due to complex number
  val octalSize:Short  //not byte due to complex number
  val minValue:T  //included
  val maxValue:T  //included
  //---------------------------------------------------------------------------
  def getFirstValue(byteSeq: Array[Byte]): T
  //---------------------------------------------------------------------------
  def getValueSeq(byteSeq: Array[Byte])(implicit classtag: ClassTag[T]): Array[T] =
    byteSeq.grouped(byteSize).map { seq => getFirstValue(seq) }.toArray
  //---------------------------------------------------------------------------
  def getByteSeq(value: T):Array[Byte]
  //---------------------------------------------------------------------------
  def getFirstValueAsDouble(byteSeq: Array[Byte]): Double
  //---------------------------------------------------------------------------
  def linearTransformation(valueSeq: Array[Any]
                           , zero: Double
                           , scale: Double) : Array[Double]
  //---------------------------------------------------------------------------
  def getByteSeq(valueSeq: Array[T])(implicit tyepeTag: TypeTag[T]): Array[Byte] =
    valueSeq.flatMap { getByteSeq(_) }
  //---------------------------------------------------------------------------
  def isValid(s: String): Boolean
  //---------------------------------------------------------------------------
  def isValid(byteSeq: Array[Byte], ignoreCheck: Boolean = false): (Boolean,Array[String]) = {
    val valueSeq = ArrayBuffer[String]()
    byteSeq.grouped(byteSize).foreach { seq =>
      val s = getFirstValue(seq).toString
      if (!ignoreCheck && !isValid(s)) return (false,Array())
      valueSeq += s
    }
    (true,valueSeq.toArray)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class DataTypeU_Int_8() extends DataType[Byte] {
  val id       = BIN_TABLE_DATA_TYPE_B
  val name     = "unsigned integer"
  val bitSize  = 8
  val byteSize = (bitSize / 8).toShort
  val octalSize = ((bitSize / 3) + (bitSize % 3)).toShort
  val minValue = 0
  val maxValue = 255.toByte  //-1 in the representation of signed
  //---------------------------------------------------------------------------
  def getFirstValue(byteSeq: Array[Byte]): Byte = byteSeq.head
  //---------------------------------------------------------------------------
  def getByteSeq(value: Byte) = Array(value)
  //---------------------------------------------------------------------------
  def getFirstValueAsDouble(byteSeq: Array[Byte]): Double =
    Util.unsignedByteToInt(byteSeq.head).toDouble
  //---------------------------------------------------------------------------
  def linearTransformation(valueSeq: Array[Any], zero: Double, scale: Double) : Array[Double] = {
    valueSeq.map { value =>
      val v = value.asInstanceOf[Byte].toShort & 0xFF
      zero + v * scale
    }
  }
  //---------------------------------------------------------------------------
  def isValid(s: String) = {
    if (!Util.isShort(s)) false
    else {
      val v = s.toShort
      (v >= minValue) && (v <= 255)  //forced max value
    }
  }
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class DataTypeInt_16() extends DataType[Short] {
  val id       = BIN_TABLE_DATA_TYPE_I
  val name     = "signed integer"
  val bitSize  = 16
  val byteSize = (bitSize / 8).toShort
  val octalSize = ((bitSize / 3) + (bitSize % 3)).toShort
  val minValue = Short.MinValue //-32768 (0x8000)
  val maxValue = Short.MaxValue //32767 (0x7FFF)
  //---------------------------------------------------------------------------
  def getFirstValue(byteSeq: Array[Byte]): Short = byteSeqToShortSeq(byteSeq).head
  //---------------------------------------------------------------------------
  def getByteSeq(value: Short) = shortToByteSeq(value)
  //---------------------------------------------------------------------------
  def getFirstValueAsDouble(byteSeq: Array[Byte]): Double =
    getFirstValue(byteSeq).toDouble
  //---------------------------------------------------------------------------
  def linearTransformation(valueSeq: Array[Any], zero: Double, scale: Double) =
    valueSeq.map { value => zero + (value.asInstanceOf[Short].toDouble * scale) }
  //---------------------------------------------------------------------------
  def isValid(s: String) = {
    if (!Util.isShort(s)) false
    else {
      val v = s.toShort
      (v >= minValue) && (v <= maxValue)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class DataTypeInt_32() extends DataType[Int] {
  val id       = BIN_TABLE_DATA_TYPE_J
  val name     = "signed integer"
  val bitSize  = 32
  val byteSize = (bitSize / 8).toShort
  val octalSize = ((bitSize / 3) + (bitSize % 3)).toShort
  val minValue = Int.MinValue //−2147483648 (0x80000000)
  val maxValue = Int.MaxValue //2147483647 (0x7FFFFFFF)
  //---------------------------------------------------------------------------
  def getFirstValue(byteSeq: Array[Byte]): Int = byteSeqToIntSeq(byteSeq).head
  //---------------------------------------------------------------------------
  def getByteSeq(value: Int) = intToByteSeq(value)
  //---------------------------------------------------------------------------
  def getFirstValueAsDouble(byteSeq: Array[Byte]): Double =
    getFirstValue(byteSeq).toDouble
  //---------------------------------------------------------------------------
  def linearTransformation(valueSeq: Array[Any], zero: Double, scale: Double) =
    valueSeq.map { value => zero + (value.asInstanceOf[Int].toDouble * scale) }
  //---------------------------------------------------------------------------
  def isValid(s: String) = {
    if (!Util.isInteger(s)) false
    else {
      val v = s.toInt
      (v >= minValue) && (v <= maxValue)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class DataTypeInt_64() extends DataType[Long] {
  val id       = BIN_TABLE_DATA_TYPE_K
  val name     = "signed integer"
  val bitSize  = 64
  val byteSize = (bitSize / 8).toShort
  val octalSize = ((bitSize / 3) + (bitSize % 3)).toShort
  val minValue = Long.MinValue //-9223372036854775808L (0x8000000000000000L)
  val maxValue = Long.MaxValue // 9223372036854775807L (0x7fffffffffffffffL)
  //---------------------------------------------------------------------------
  def getFirstValue(byteSeq: Array[Byte]): Long = byteSeqToLongSeq(byteSeq).head
  //---------------------------------------------------------------------------
  def getByteSeq(value: Long) = longToByteSeq(value)
  //---------------------------------------------------------------------------
  def getFirstValueAsDouble(byteSeq: Array[Byte]): Double =
    getFirstValue(byteSeq).toDouble
  //---------------------------------------------------------------------------
  def linearTransformation(valueSeq: Array[Any], zero: Double, scale: Double) =
    valueSeq.map { value => zero + (value.asInstanceOf[Long].toDouble * scale) }
  //---------------------------------------------------------------------------
  def isValid(s: String) = {
    if (!Util.isLong(s)) false
    else {
      val v = s.toLong
      (v >= minValue) && (v <= maxValue)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class DataTypeFloat_32() extends DataType[Float] {
  val id       = BIN_TABLE_DATA_TYPE_E
  val name     = "float_32"
  val bitSize  = 32
  val byteSize = (bitSize / 8).toShort
  val octalSize = ((bitSize / 3) + (bitSize % 3)).toShort
  val minValue = Float.MinValue
  val maxValue = Float.MaxValue
  //---------------------------------------------------------------------------
  def getFirstValue(byteSeq: Array[Byte]): Float = byteSeqToFloatSeq(byteSeq).head
  //---------------------------------------------------------------------------
  def getByteSeq(value: Float) = floatToByteSeq(value)
  //---------------------------------------------------------------------------
  def getFirstValueAsDouble(byteSeq: Array[Byte]): Double =
    getFirstValue(byteSeq).toDouble
  //---------------------------------------------------------------------------
  def linearTransformation(valueSeq: Array[Any], zero: Double, scale: Double) =
    valueSeq.map { value => zero + (value.asInstanceOf[Float].toDouble * scale) }
  //---------------------------------------------------------------------------
  def isValid(s: String) = {
    if (!Util.isFloat(s)) false
    else {
      val v = s.toFloat
      (v >= minValue) && (v <= maxValue)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class DataTypeFloat_64() extends DataType[Double] {
  val id = BIN_TABLE_DATA_TYPE_D
  val name = "float_64"
  val bitSize  = 64
  val byteSize = (bitSize / 8).toShort
  val octalSize = ((bitSize / 3) + (bitSize % 3)).toShort
  val minValue = Double.MinValue
  val maxValue = Double.MaxValue
  //---------------------------------------------------------------------------
  def getFirstValue(byteSeq: Array[Byte]): Double = byteSeqToDoubleSeq(byteSeq).head
  //---------------------------------------------------------------------------
  def getByteSeq(value: Double) = doubleToByteSeq(value)
  //---------------------------------------------------------------------------
  def getFirstValueAsDouble(byteSeq: Array[Byte]): Double =
    getFirstValue(byteSeq)
  //---------------------------------------------------------------------------
  def linearTransformation(valueSeq: Array[Any], zero: Double, scale: Double) =
    valueSeq.map { value => zero + (value.asInstanceOf[Double] * scale) }
  //---------------------------------------------------------------------------
  def isValid(s: String) = {
    if (!Util.isDouble(s)) false
    else {
      val v = s.toDouble
      (v >= minValue) && (v <= maxValue)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class DataTypeComplexFloat_32() extends DataType[ComplexFloat_32] {
  val id = BIN_TABLE_DATA_TYPE_C
  val name = "complex float"
  val bitSize = (32 * 2).toShort
  val byteSize = ((bitSize / 8) * 2).toShort
  val octalSize = (((bitSize / 3) + (bitSize % 3)) * 2).toShort
  val minValue = ComplexFloat_32(Float.MinValue,Float.MinValue)
  val maxValue = ComplexFloat_32(Float.MaxValue,Float.MaxValue)
  //---------------------------------------------------------------------------
  def getFirstValue(byteSeq: Array[Byte]): ComplexFloat_32 = {
    val valueSeq = byteSeqToFloatSeq(byteSeq)
    ComplexFloat_32(valueSeq.head , valueSeq.last)
  }
  //---------------------------------------------------------------------------
  def getByteSeq(value: ComplexFloat_32) =
    floatToByteSeq(value.real) ++ floatToByteSeq(value.imaginary)
  //---------------------------------------------------------------------------
  def getFirstValueAsDouble(byteSeq: Array[Byte]): Double = getFirstValue(byteSeq).module
  //---------------------------------------------------------------------------
  def linearTransformation(valueSeq: Array[Any], zero: Double, scale: Double) =
    valueSeq.map { value => zero + (value.asInstanceOf[Float] * scale) }
  //---------------------------------------------------------------------------
  def isValid(s: String) = ComplexFloat_32.isValid(s)
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class DataTypeComplexFloat_64() extends DataType[ComplexFloat_64] {
  val id = BIN_TABLE_DATA_TYPE_M
  val name = "complex double"
  val bitSize = (64 * 2).toShort
  val byteSize = ((bitSize / 8) * 2).toShort
  val octalSize = (((bitSize / 3) + (bitSize % 3)) * 2).toShort
  val minValue = ComplexFloat_64(Double.MinValue,Double.MinValue)
  val maxValue = ComplexFloat_64(Double.MaxValue,Double.MaxValue)
  //---------------------------------------------------------------------------
  def getFirstValue(byteSeq: Array[Byte]): ComplexFloat_64 = {
    val valueSeq = byteSeqToDoubleSeq(byteSeq)
    ComplexFloat_64(valueSeq.head, valueSeq.last)
  }
  //---------------------------------------------------------------------------
  def getByteSeq(value: ComplexFloat_64) =
    doubleToByteSeq(value.real) ++ doubleToByteSeq(value.imaginary)
  //---------------------------------------------------------------------------
  def getFirstValueAsDouble(byteSeq: Array[Byte]): Double = getFirstValue(byteSeq).module
  //---------------------------------------------------------------------------
  def linearTransformation(valueSeq: Array[Any], zero: Double, scale: Double) =
    valueSeq.map { value => zero + (value.asInstanceOf[Double] * scale) }
  //---------------------------------------------------------------------------
  def isValid(s: String) = ComplexFloat_32.isValid(s)
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class DataTypeUnknown() extends DataType[Byte] {
  val id = "NONE"
  val name = "UNKNOWN"
  val bitSize = -1
  val byteSize = -1
  val octalSize = -1
  val minValue = -1
  val maxValue = -1
  //---------------------------------------------------------------------------
  def getFirstValue(byteSeq: Array[Byte]) = byteSeq.head
  //---------------------------------------------------------------------------
  def getByteSeq(value: Byte) = Array(value)
  //---------------------------------------------------------------------------
  def getFirstValueAsDouble(byteSeq: Array[Byte]): Double = getFirstValue(byteSeq).toDouble
  //---------------------------------------------------------------------------
  def linearTransformation(valueSeq: Array[Any], zero: Double, scale: Double) = Array()
  //---------------------------------------------------------------------------
  def isValid(s: String) = false
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CsvDataType.scala
//=============================================================================
