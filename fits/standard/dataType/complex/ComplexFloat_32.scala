/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  09/Dec/2022
 * Time:  12h:19m
 * Description: None
 */
//=============================================================================
package com.common.fits.standard.dataType.complex
//=============================================================================
import com.common.fits.standard.block.record.parser.RecordValue.{COMPLEX_NUMBER_DIVIDER_STRING, COMPLEX_NUMBER_END_STRING, COMPLEX_NUMBER_START_STRING}
import com.common.util.util.Util
//=============================================================================
//=============================================================================
object ComplexFloat_32 {
  //---------------------------------------------------------------------------
  def build(valueSeq: Array[Float]) = {
    valueSeq.grouped(2).map { seq => ComplexFloat_32(seq(0), seq(1))
    }.toArray
  }
  //---------------------------------------------------------------------------
  def isValid(s: String): Boolean = {
    if (!s.startsWith(COMPLEX_NUMBER_START_STRING)) return false
    if (!s.endsWith(COMPLEX_NUMBER_END_STRING)) return false
    if (s.indexOf(COMPLEX_NUMBER_DIVIDER_STRING) == -1) return false
    val numberSeq = s.drop(1).dropRight(1).split(COMPLEX_NUMBER_DIVIDER_STRING)
    if (numberSeq.size != 2) return false
    if (!Util.isFloat(numberSeq.head)) return false
    if (!Util.isFloat(numberSeq.last)) return false
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class ComplexFloat_32(real: Float = 0, imaginary: Float = 0) {
  //---------------------------------------------------------------------------
  def module = Math.sqrt(real * real + imaginary * imaginary)

  //---------------------------------------------------------------------------
  def +(other: ComplexFloat_32): ComplexFloat_32 = {
    ComplexFloat_32(real + other.real, imaginary + other.imaginary)
  }
  //---------------------------------------------------------------------------
  def *(other: ComplexFloat_32): ComplexFloat_32 = {
    val realPart = real * other.real - imaginary * other.imaginary
    val imaginaryPart = real * other.imaginary + imaginary * other.real
    ComplexFloat_32(realPart, imaginaryPart)
  }
  //---------------------------------------------------------------------------
  def format = s"$COMPLEX_NUMBER_START_STRING$real$COMPLEX_NUMBER_DIVIDER_STRING$imaginary$COMPLEX_NUMBER_END_STRING"
  //---------------------------------------------------------------------------
  override def toString: String = {
    val a = real.toString
    val b = Math.abs(imaginary).toString
    if (imaginary > 0) a + " + " + b + "j"
    else a + " - " + b + "j"
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ComplexFloat_32.scala
//=============================================================================
