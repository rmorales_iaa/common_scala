/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  14/Apr/2023 
 * Time:  08h:51m
 * Description: None
 */
//=============================================================================
package com.common.fits.metaData
//=============================================================================
import BuildInfo.BuildInfo
import com.common.configuration.MyConf
import com.common.csv.CsvDataType
import com.common.csv.CsvDataType.CsvDataTypeString
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.simpleFits.SimpleFits._
import com.common.hardware.cpu.CPU
import com.common.estimator.skyPosition.EstimatedSkyPosition
import com.common.image.focusType.{ImageFocusMPO, ImageFocusTrait}
import com.common.image.myImage.MyImage
import com.common.image.telescope.TelescopeMatcher.getTelescope
import com.common.image.telescope.{Telescope, TelescopeMatcher}
import com.common.jpl.Spice
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
import com.common.util.time.Time
//=============================================================================
import java.io.{BufferedWriter, FileWriter}
//=============================================================================
//=============================================================================
object MetaData {
  //---------------------------------------------------------------------------
  final val DEFAULT_TELESECOPE = "OSN_1.5"
  //---------------------------------------------------------------------------
  private final val FITS_KEYWORD_OBJECT_NAME_SEQ = Seq(
      KEY_OM_OBJECT
    , KEY_OBJECT
    , KEY_OBJEC
    , KEY_OBJCAT
    , KEY_CAT_NAME
    , KEY_CAT_NAME_1)
  //---------------------------------------------------------------------------
  private final val OBJECT_NAME_TRASLATION_MAP =  Map(
      "2002PN34" -> "2002 PN34"
    , "1999TL66" -> "1999 TL66"
    , "2002AW107"-> "2002 AW107"
  )
  //---------------------------------------------------------------------------
  //(internal SPK ID code, official code)
  def getObservatoryCode(fits: SimpleFits
                         , defaultTelescopeName: Option[String] = None): Option[(String,String)] = {
    val telescopeName = getTelescope(fits)
    if (telescopeName == null || telescopeName.isEmpty) {
      if (defaultTelescopeName.isDefined) {
        warning(s"The telescope:'$telescopeName' is not known, using default telescope:'${defaultTelescopeName.get}'")
      }
      else {
        error(s"The telescope:'$telescopeName' is not known. Please add it or correct it")
        return None
      }
    }

    if (!Telescope.existConfigurationFile(telescopeName)) {
      val telescopeMatcher = TelescopeMatcher.getTelescope(fits)
      if (telescopeMatcher.trim.isEmpty) {
        error(s"Error loading the configuration file of telescope:'$telescopeName'")
        return None
      }
      else return Some((Telescope.getSpkId(telescopeMatcher.trim)
                      , Telescope.getOfficialCode(telescopeMatcher.trim)))
    }
    Some((Telescope.getSpkId(fits)
          ,Telescope.getOfficialCode(fits)))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import MetaData._
case class MetaData(var focus: ImageFocusTrait
                    , inDir: String
                    , outDir: String
                    , _keyValueSeq: List[String]
                    , keepEntries: Boolean
                    , remove: Boolean
                    , outputSpkFile: Option[String]
                    , noneObject: Boolean) extends MyLogger {
  //---------------------------------------------------------------------------
  private val isStar = if (focus == null) false else focus.isStar
  private var oDir = ""
  private val keyValueSeq = getKeyValueSeq()
  private val bwSpk: BufferedWriter = openOutputSpkFile
  private val writtenSpkNameMap = scala.collection.mutable.Map[String, String]()
  //---------------------------------------------------------------------------
  private def openOutputSpkFile(): BufferedWriter = {
    if (outputSpkFile.isDefined)
      new BufferedWriter(new FileWriter(outputSpkFile.get))
    else null
  }
  //---------------------------------------------------------------------------
  private def closeOutputSpkFile() = if (bwSpk != null) bwSpk.close()

  //---------------------------------------------------------------------------
  if (!noneObject) openOutputSpkFile

  if (keyValueSeq != null) {
    if (!noneObject) Spice.init(verbose = false)
    processDirectory()
    if (!noneObject) Spice.close()
  }
  if (!noneObject && bwSpk != null) warning(s"SPCIE SPK name written:'${writtenSpkNameMap.size}'")
  if (!noneObject) closeOutputSpkFile
  //---------------------------------------------------------------------------
  private def getKeyValueSeq(): Array[(String,String)] = {
    if (!remove && _keyValueSeq.size % 2 != 0) {
      error(s"Expecting a pair number of entries as '--key-value-seq'. Current value: $keyValueSeq")
      null
    }
   else
      (_keyValueSeq map (s=> s
                          .replaceAll("'"," ")
                          .trim
                          .replaceAll(" +", " ")
                          .replaceAll(" ","_")
                        ))
        .grouped(2)
        .toArray
        .map(t=> (t.head,t.last))
  }
  //---------------------------------------------------------------------------
  private def processDirectory() = {
    oDir = Path.resetDirectory(outDir)
    val fileNameSeq = Path.getSortedFileListRecursive(inDir, MyConf.c.getStringSeq("Common.fitsFileExtension"))
                      .map ( _.getAbsolutePath).toArray

    new ProcessImageDir(fileNameSeq)
  }
  //---------------------------------------------------------------------------
  private def processImage(filename: String): Unit = {
    info(s"Processing file:'${MyFile.getFileNameNoPathNoExtension(filename)}'")
    val img = MyImage(filename)
    if (img == null)
      error(s"Error loading the image:'${MyFile.getFileNameNoPathNoExtension(filename)}'")
    else {
      val fits = img.getSimpleFits()
      updateKeyValueSeq(fits,keyValueSeq)
      if (!isStar && !noneObject) updateAstrometryInfo(img)
    }
    img.saveAsFits(s"$oDir/${img.getRawName()}.fits"
      , buildInfo = Array((KEY_M2_VERSION, s"'${BuildInfo.version.trim}'")
                         , (KEY_M2_RUN_DATE, s"'${Time.getISO_DateTimeStamp}'"))
    )
  }
  //---------------------------------------------------------------------------
  private def updateAstrometryInfo(img: MyImage
                                  , spkVersion: Option[String] = None): Unit = {
    val fits = img.getSimpleFits()
    if (focus == null)  {
      val r = getFirstValidObjectName(fits)
      if (r.isDefined) focus = r.get
    }

    //copy without modification
    if (focus == null) {
      img.saveAsFits(s"$oDir/${img.getRawName()}.fits"
                      , buildInfo = Array((KEY_M2_VERSION, s"'${BuildInfo.version.trim}'")
                                       , (KEY_M2_RUN_DATE, s"'${Time.getISO_DateTimeStamp}'")))
      return
    }

    val mpo = focus.asInstanceOf[ImageFocusMPO]
    val raDec = EstimatedSkyPosition.getPosAtImageWithFallBack(mpo, img)

    val kvSeq = Seq(
        (KEY_CRPIX_1, Math.round(img.xMax.toFloat / 2).toString)
      , (KEY_CRPIX_2, Math.round(img.yMax.toFloat / 2).toString)
      , (KEY_CRVAL_1, raDec.x.toString)
      , (KEY_CRVAL_2, raDec.y.toString)
    )
    updateKeyValueSeq(fits,kvSeq)
  }
  //---------------------------------------------------------------------------
  private def retrieveFocus(focusName: String) = {
    val r = ImageFocusMPO.build(
        focusName
      , source = null
      , verbose = false)

    if (r.isEmpty) {
      synchronized {
        val s = focusName.replaceAll(" ", "_")
        if (!writtenSpkNameMap.contains(s)) {
          writtenSpkNameMap(s) = s
          bwSpk.write(s + "\n")
        }
      }
      None
    }
    else r
  }
  //---------------------------------------------------------------------------
  private def getFirstValidObjectName(fits: SimpleFits): Option[ImageFocusMPO] = {
    FITS_KEYWORD_OBJECT_NAME_SEQ.foreach { keyword =>
      val focusName = fits.getStringValueOrEmptyNoQuotation(keyword)
      if (focusName != null && !focusName.isEmpty && focusName.toLowerCase != "none") {

        var r = retrieveFocus(focusName)
        if (!r.isEmpty) return r

        //try replacing some chars
        r = retrieveFocus(focusName.replaceAll("_", " "))
        if (!r.isEmpty) return r

        //trying remove year
        r = retrieveFocus(focusName.take(4) + " " + focusName.drop(4))  //sometimes the first 4 four difits are the year
        if (!r.isEmpty) return r

        //trying using map
        if (!OBJECT_NAME_TRASLATION_MAP.contains(focusName)) return None
        r = retrieveFocus(OBJECT_NAME_TRASLATION_MAP(focusName))
        if (!r.isEmpty) return r
      }
    }

    None
  }
  //---------------------------------------------------------------------------
  private def updateKeyValueSeq(fits: SimpleFits
                                , kvSeq: Seq[(String,String)]) =
    kvSeq.foreach { case (key, value) =>
      if (remove) {
        if (fits.existKey(key)) fits.removeKeySeq(kvSeq map (_._1.toUpperCase()))
      }
      else {
        if (!fits.existKey(key) ||
          (fits.existKey(key) && !keepEntries)) {
          val dataType = CsvDataType.guessDataType(value)
          val v = if (dataType.isInstanceOf[CsvDataTypeString]) s"'$value'" else value
          fits.updateRecord(key.toUpperCase, v)
        }
      }
    }
  //---------------------------------------------------------------------------
  private class ProcessImageDir(fileNameSeq: Array[String]) extends ParallelTask[String](
    fileNameSeq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(fileName: String) = {
      processImage(fileName)
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MetaData.scala
//=============================================================================
