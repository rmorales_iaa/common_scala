/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Jul/2020
 * Time:  10h:28m
 * Description: None
 */
//=============================================================================
package com.common.fits.synthetize
//=============================================================================
import com.common.fits.simpleFits.SimpleFits.{KEY_BIT_PIX, KEY_BSCALE, KEY_BZERO, KEY_NAXIS, KEY_NAXIS_1, KEY_NAXIS_2, KEY_SIMPLE}
import com.common.fits.synthetize.source.{Gaussian2D_Source, SyntheticSource, SyntheticSourceGaussian2D_WithCorrelation}
import com.common.geometry.point.Point2D_Double

import java.io.{BufferedWriter, File, FileWriter}
import scala.collection.mutable.ArrayBuffer
import scala.util.Random
//=============================================================================
import com.common.dataType.pixelDataType.PixelDataType._
import com.common.fits.simpleFits.SimpleFits
import com.common.geometry.matrix.matrix2D.Matrix2D
import com.common.image.myImage.MyImage
//=============================================================================
//=============================================================================
case class SyntheticImage(xMax : Int //range [0,xPixMax-1]
                          , yMax : Int //range [0,yPixMax-1]
                          , backgroundValue : PIXEL_DATA_TYPE
                          , backgroundValuePercentageVariation: Double) {
  //---------------------------------------------------------------------------
  //generate random values
  private val data = ArrayBuffer.fill[PIXEL_DATA_TYPE](yMax * xMax) {
    addRandomValue(backgroundValue, backgroundValue * backgroundValuePercentageVariation * 0.01)
  }
  //---------------------------------------------------------------------------
  //Generate a random value between 0.25% and 0.75% of the range
  def addRandomValue(x: Double, range: Double): Double = {
    val randomValue = Random.nextFloat() - 0.25
    val offset = randomValue * range
    x + offset
  }
  //---------------------------------------------------------------------------
  private def getImage =
    MyImage(
      Matrix2D(
        xMax
        , yMax
        , data.toArray)
      , getSimpleFits())
  //---------------------------------------------------------------------------
  def apply(x:Int, y: Int) = data((y * xMax) + x)
  //---------------------------------------------------------------------------
  def addValue(x:Int, y: Int, v:Double) = data((y * xMax) + x) += v
  //---------------------------------------------------------------------------
  def reset() = for (x<- 0 until (yMax * xMax)) data(x) = backgroundValue
  //---------------------------------------------------------------------------
  private def getSimpleFits() = {

    val fits = SimpleFits()

    //build minimal FITS header
    fits.updateRecord(KEY_SIMPLE, "T")
    fits.updateRecord(KEY_BIT_PIX,"-32")
    fits.updateRecord(KEY_NAXIS, "2")
    fits.updateRecord(KEY_NAXIS_1, xMax.toString)
    fits.updateRecord(KEY_NAXIS_2, yMax.toString)
    fits.updateRecord(KEY_BSCALE, "1")
    fits.updateRecord(KEY_BZERO, "0")

    fits.bitPix = -32
    fits.bScale = 1
    fits.bZero = 0
    fits
  }

  //---------------------------------------------------------------------------
  //(coeff at pos, coeff at pos +1)
  private def getPos(pos: Double) = {
    val diff = pos - pos.toInt
    (1d - diff, diff)
  }
  //---------------------------------------------------------------------------
  private def addSource(source: SyntheticSource
                        , pixStepAxisX: Double
                        , pixStepAxisY: Double
                        , dataMultiplier: Double = 1): Unit = {
    source.getData(pixStepAxisX,pixStepAxisY).foreach { case  (pos,value) =>
      val (coeffAtPosX,coeffAtPos_1_X) = getPos(pos.x)
      val (coeffAtPosY,coeffAtPos_1_Y) = getPos(pos.y)

      val intX_Pos = pos.x.toInt
      val intY_Pos = pos.y.toInt

      addValue(intX_Pos, intY_Pos, coeffAtPosX * coeffAtPosY * value * dataMultiplier)
      addValue(intX_Pos + 1, intY_Pos + 1, coeffAtPos_1_X * coeffAtPos_1_Y * value * dataMultiplier)
    }
  }
  //---------------------------------------------------------------------------
  def addRandomGaussian2D_SourceSeq(sourceCount: Int
                                    , fitsFileName: String
                                    , tsvFileName: String
                                    , sigmaMultiplier: Double = 3
                                    , pixStepAxisX: Double = 0.5
                                    , pixStepAxisY: Double = 0.5): Unit = {
    def sep = "\t"
    val bw = new BufferedWriter(new FileWriter(new File(tsvFileName)))
    bw.write(s"source_id$sep" +
      s"centre_x${sep}centre_y$sep" +
      s"min_x${sep}max_x$sep" +
      s"min_y${sep}max_y$sep" +
      s"sigma_x${sep}sigma_y$sep" +
      s"amplitude$sep" +
      "\n")

    val minX = xMax * 0.25 //avoid borders
    val maxX = xMax * 0.75 //avoid borders

    val minY = yMax * 0.25 //avoid borders
    val maxY = yMax * 0.75 //avoid borders

    for (sourceID <- 0 until sourceCount) {
      val centreX = Random.between(minX, maxX)
      val centreY = Random.between(minY, maxY)
      val sigmaX = Random.nextDouble() * sigmaMultiplier + 1.0
      val sigmaY = Random.nextDouble() * sigmaMultiplier + 1.0
      val amplitude = Random.nextDouble() * 1000.0 + 100.0

      val source = Gaussian2D_Source(
        centre = Point2D_Double(centreX, centreY),
        min = Point2D_Double(centreX - sigmaMultiplier * sigmaX, centreY - sigmaMultiplier * sigmaY),
        max = Point2D_Double(centreX + sigmaMultiplier* sigmaX, centreY + sigmaMultiplier * sigmaY),
        sigmaX,
        sigmaY,
        amplitude
      )

      addSource(source, pixStepAxisX, pixStepAxisY)

      //FITS standard origin is (1,1)
      bw.write(s"$sourceID$sep" +
        s"${source.centre.x + 1}$sep${source.centre.y + 1}$sep" +
        s"${source.min.x}$sep${source.max.x}$sep" +
        s"${source.min.y}$sep${source.max.y}$sep" +
        s"${source.sigmaX}$sep${source.sigmaY}$sep" +
        s"${source.amplitude}" + "\n")
    }
    bw.close()

    val userFitsRecordSeq = Array(
      ("OM_TELES","'OSN_1.5'")
     , ("OM_EXPTI","200.0")
     , ("OM_DATEO", "'2009-02-25T19:53:35'")
     , ("CD1_1",	"-1.2679079220671245E-4")
     , ("CD1_2",	"1.460588347320065E-8")
     , ("CD2_1",	"9.35297042400872E-9")
     , ("CD2_2",	"-1.267715975721926E-4")
     , ("CRPIX1",	(xMax / 2.0).toString)
     , ("CRPIX2",	(yMax / 2.0).toString)
     , ("CRVAL1",	"83.42377582933587")
     , ("CRVAL2",	"58.09883583410698")
     )

    getImage.saveAsFits(fitsFileName,userFitsRecordSeq)
  }
  //---------------------------------------------------------------------------
  def addSequenceOfBivariateNormal(sourceCount: Int
                                   , fitsFileName: String
                                   , tsvFileName: String
                                   , sigmaMultiplier: Double = 3
                                   , pixStepAxisX: Double = 0.5
                                   , pixStepAxisY: Double = 0.5
                                   , dataMultiplier: Double = 1): Unit = {
    def sep = "\t"
    val bw = new BufferedWriter(new FileWriter(new File(tsvFileName)))
    bw.write(s"source_id$sep" +
      s"centre_x${sep}centre_y$sep" +
      s"min_x${sep}max_x$sep" +
      s"min_y${sep}max_y$sep" +
      s"sigma_x${sep}sigma_y$sep" +
      s"correlation$sep" +
      "\n")

    val minX = xMax * 0.25 //avoid borders
    val maxX = xMax * 0.75 //avoid borders

    val minY = yMax * 0.25 //avoid borders
    val maxY = yMax * 0.75 //avoid borders

    for (sourceID <- 0 until sourceCount) {
      val centreX = Random.between(minX, maxX)
      val centreY = Random.between(minY, maxY)
      val sigmaX = Random.nextDouble() * sigmaMultiplier + 1.0
      val sigmaY = Random.nextDouble() * sigmaMultiplier + 1.0
      val correlation =  Random.nextDouble() * 2 - 1

      val source = SyntheticSourceGaussian2D_WithCorrelation(
        centre = Point2D_Double(centreX, centreY),
        min = Point2D_Double(centreX - sigmaMultiplier * sigmaX, centreY - sigmaMultiplier * sigmaY),
        max = Point2D_Double(centreX + sigmaMultiplier * sigmaX, centreY + sigmaMultiplier * sigmaY),
        sigmaX,
        sigmaY,
        correlation
      )

      addSource(source, pixStepAxisX, pixStepAxisY, dataMultiplier)

      //FITS standard origin is (1,1)
      bw.write(s"$sourceID$sep" +
        s"${source.centre.x + 1}$sep${source.centre.y + 1}$sep" +
        s"${source.min.x}$sep${source.max.x}$sep" +
        s"${source.min.y}$sep${source.max.y}$sep" +
        s"${source.sigmaX}$sep${source.sigmaY}$sep" +
        s"${source.correlation}" + "\n")
    }
    bw.close()

    getImage.saveAsFits(fitsFileName)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file FitsSynthesize.scala
//=============================================================================