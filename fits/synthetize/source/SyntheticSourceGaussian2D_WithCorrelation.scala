/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  07/Nov/2024
 * Time:  11h:43m
 * Description: None
 */
package com.common.fits.synthetize.source
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.math.distribution.BivariateNormal

//=============================================================================
//=============================================================================
case class SyntheticSourceGaussian2D_WithCorrelation(centre: Point2D_Double
                                                     , min: Point2D_Double
                                                     , max: Point2D_Double
                                                     , sigmaX: Double
                                                     , sigmaY: Double
                                                     , correlation: Double) extends SyntheticSource {
  //---------------------------------------------------------------------------
  private val dataGenerator = BivariateNormal(
      centre.x
    , centre.y
    , sigmaX
    , sigmaY
    , correlation)
  //---------------------------------------------------------------------------
  def eval(x: Double, y:Double) = dataGenerator.eval(x,y)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Gaussian2D_DensityWithCorrelationSource.scala
//=============================================================================