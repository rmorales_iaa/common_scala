/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  07/Nov/2024
 * Time:  11h:37m
 * Description: None
 */
package com.common.fits.synthetize.source
//=============================================================================
import com.common.geometry.point.Point2D_Double
import scala.math.BigDecimal.double2bigDecimal
//=============================================================================
//=============================================================================
trait SyntheticSource {
  //---------------------------------------------------------------------------
  val centre: Point2D_Double
  val min: Point2D_Double
  val max: Point2D_Double
  //---------------------------------------------------------------------------
  def eval(x: Double, y:Double): Double
  //---------------------------------------------------------------------------
  def xRange: Double = max.x - min.x
  //---------------------------------------------------------------------------
  def yRange: Double = max.y - min.y
  //---------------------------------------------------------------------------
  def getData(pixStepAxisX: Double
              , pixStepAxisY: Double): Array[(Point2D_Double,Double)] = {
    (for {
      x <- min.x to max.x by pixStepAxisX
      y <- min.y to max.y by pixStepAxisY
    } yield {
      val pos = Point2D_Double(x.toDouble, y.toDouble)
      (pos,eval(pos.x, pos.y))
    }).toArray
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SyntheticSource.scala
//=============================================================================