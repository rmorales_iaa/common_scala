/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  07/Nov/2024
 * Time:  11h:48m
 * Description: None
 */
package com.common.fits.synthetize.source
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.math.distribution.gaussian.guassian2D.Gaussian2D_Base
//=============================================================================
//=============================================================================
case class Gaussian2D_Source(centre: Point2D_Double
                             , min: Point2D_Double
                             , max: Point2D_Double
                             , sigmaX: Double
                             , sigmaY: Double
                             , amplitude: Double) extends SyntheticSource {
  //---------------------------------------------------------------------------
  private val dataGenerator = new Gaussian2D_Base(
    amplitude
  , centre.x
  , centre.y
  , sigmaX
  , sigmaY)
  //---------------------------------------------------------------------------
  def eval(x: Double, y:Double) = dataGenerator.eval(x,y)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Gaussian2D_Source.scala
//=============================================================================
