/**
 * CreerrorPolynomyOrdered by: Rafael Morales (rmorales@iaa.es)
 * DerrorPolynomyOrdere:  13/Jul/2022
 * Time:  10h:05m
 * Description: Affine transformation WCS (including errors)
 */
//=============================================================================
package com.common.fits.wcs.nonStandard.polynomial
//=============================================================================
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.wcs.WCS
import com.common.fits.wcs.nonStandard.NonStandardPolyWcs
import com.common.fits.wcs.nonStandard.NonStandardPolyWcs.{FITS_KEYWORD_M2_WCS, FITS_KEYWORD_M2_WCS_RESIDUAL, FITS_KEYWORD_WCS_AXIS}
import com.common.geometry.point.Point2D_Double
import com.common.math.regression.two_dimension.parabolicPoly.Parabolic_2D
//=============================================================================
//=============================================================================
object ParabolicPolyWcs {
  //---------------------------------------------------------------------------
  val id                      = "parab_poly"
  val version                 = s"$id v0.1"
  val FITS_KEYWORD_WCS_PREFIX = "PRB_"
  //---------------------------------------------------------------------------
  def getFitsKeywordSeq(wcsPrefix: String, order: Int) =
    for (a <- FITS_KEYWORD_WCS_AXIS;
         i <- 0 until Parabolic_2D.getParameterSeqCount(order)) yield
      (wcsPrefix + a + "_" + i).toUpperCase
  //---------------------------------------------------------------------------
  def build(fits: SimpleFits
            , wcs: WCS): Option[NonStandardPolyWcs] = {

    val order = fits.getStringValueOrEmptyNoQuotation(FITS_KEYWORD_M2_WCS).drop(4).toInt

    val valueSeq = getFitsKeywordSeq(FITS_KEYWORD_WCS_PREFIX, order).map { keyword =>
      fits.getKeyDoubleValueOrDefault(keyword, 0)
    }.grouped(Parabolic_2D.getParameterSeqCount(order)).toArray

    val residualOrder = fits.getStringValueOrEmptyNoQuotation(FITS_KEYWORD_M2_WCS_RESIDUAL).drop(4).toInt
    val residualValueSeq = getFitsKeywordSeq(FITS_KEYWORD_WCS_PREFIX, residualOrder).map { keyword =>
      fits.getKeyDoubleValueOrDefault(keyword, 0)
    }.grouped(Parabolic_2D.getParameterSeqCount(residualOrder)).toArray

    val raPolyFit = Parabolic_2D(valueSeq(0), order)
    val decPolyFit = Parabolic_2D(valueSeq(1), order)
    val xPolyFit = Parabolic_2D(valueSeq(2), order)
    val yPolyFit = Parabolic_2D(valueSeq(3), order)

    raPolyFit.setErrorModel(Parabolic_2D(residualValueSeq(0), residualOrder))
    decPolyFit.setErrorModel(Parabolic_2D(residualValueSeq(1), residualOrder))
    xPolyFit.setErrorModel(Parabolic_2D(residualValueSeq(2), residualOrder))
    yPolyFit.setErrorModel(Parabolic_2D(residualValueSeq(3), residualOrder))

    Some(ParabolicPolyWcs(
      order
      , residualOrder
      , raPolyFit
      , decPolyFit
      , xPolyFit
      , yPolyFit
      , wcs.crpix
      , wcs.crval
    ))

    None
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import ParabolicPolyWcs._
case class ParabolicPolyWcs(order: Int
                           , residualOrder: Int
                           , raPolyFit    : Parabolic_2D
                           , decPolyFit   : Parabolic_2D
                           , xPolyFit     : Parabolic_2D
                           , yPolyFit     : Parabolic_2D
                           , crpix: Point2D_Double
                           , crval: Point2D_Double) extends NonStandardPolyWcs {
  //---------------------------------------------------------------------------
  val fitsKeywordWcsPrefix = FITS_KEYWORD_WCS_PREFIX
  //---------------------------------------------------------------------------
  def getFitsKeywordSeq(wcsPrefix: String, order: Int) =
    ParabolicPolyWcs.getFitsKeywordSeq(wcsPrefix, order)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ParabolicPolyWcs.scala
//=============================================================================
