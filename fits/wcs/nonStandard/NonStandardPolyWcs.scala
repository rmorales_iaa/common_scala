/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Jan/2023
 * Time:  20h:54m
 * Description: None
 */
//=============================================================================
package com.common.fits.wcs.nonStandard
//=============================================================================
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.wcs.WCS
import com.common.fits.wcs.nonStandard.polynomial.{GeneralPolyWcs, ParabolicPolyWcs}
import com.common.geometry.point.Point2D_Double
import com.common.math.regression.two_dimension.RegressionPoly_2D
import com.common.math.regression.two_dimension.generalPoly.{GeneralPoly_2D, GeneralPoly_2D_fit}
import com.common.math.regression.two_dimension.parabolicPoly.Parabolic_2D_fit
//=============================================================================
//=============================================================================
object NonStandardPolyWcs {
  //---------------------------------------------------------------------------
  sealed trait NON_STANDARD_POLY_WCS_TYPE
  case object NON_STANDARD_GEN_POLY_WCS extends NON_STANDARD_POLY_WCS_TYPE
  case object NON_STANDARD_PARABOLIC_POLY_WCS extends NON_STANDARD_POLY_WCS_TYPE
  //---------------------------------------------------------------------------
  //FITS keywords
  final val FITS_KEYWORD_WCS_AXIS = Array("r", "d", "x", "y")
  private final val FITS_KEYWORD_WCS_AXIS_RESIDUAL    = Array("r", "d")
  //---------------------------------------------------------------------------
  private final val FITS_KEYWORD_WCS_RESIDUAL_PREFIX = "RES_" //residual
  //---------------------------------------------------------------------------
  final val FITS_KEYWORD_M2_WCS          = "M2_WCS"
  final val FITS_KEYWORD_M2_WCS_RESIDUAL = "M2_WCS_R"
  //---------------------------------------------------------------------------
  def getFitsRecodSeq(est: RegressionPoly_2D, keywordSeq: Array[String]) = {
    val coeffSeq = est.coeffSeq
    keywordSeq.zipWithIndex map { case (k, i) => k -> coeffSeq(i).toString }
  }
  //---------------------------------------------------------------------------
  private def getFitsKeywordResidualSeq(residualOrder: Int) =
    for (a <- FITS_KEYWORD_WCS_AXIS_RESIDUAL;
         i <- 0 until GeneralPoly_2D.getParameterSeqCount(residualOrder)) yield
      (FITS_KEYWORD_WCS_RESIDUAL_PREFIX + a + "_" + i).toUpperCase

  //---------------------------------------------------------------------------
  def calculateErrorModel(nonStandardWcsType: NON_STANDARD_POLY_WCS_TYPE
                          , firstPolyFit: RegressionPoly_2D
                          , secondPolyFit: RegressionPoly_2D
                          , estValueSeq: Seq[Point2D_Double]
                          , estRefSeq: Array[Point2D_Double]
                          , residualsOrder: Int) : Boolean = {

    nonStandardWcsType match {
      case NON_STANDARD_GEN_POLY_WCS =>
        firstPolyFit.setErrorModel(GeneralPoly_2D_fit(estValueSeq.toArray
          , estRefSeq map (_.x)
          , residualsOrder).fit().getOrElse(return false))

        secondPolyFit.setErrorModel(GeneralPoly_2D_fit(estValueSeq.toArray
          , estRefSeq map (_.y)
          , residualsOrder).fit().getOrElse(return false))

      case NON_STANDARD_PARABOLIC_POLY_WCS =>
        firstPolyFit.setErrorModel(Parabolic_2D_fit(estValueSeq.toArray
          , estRefSeq map (_.x)
          , residualsOrder).fit().getOrElse(return false))

        secondPolyFit.setErrorModel(Parabolic_2D_fit(estValueSeq.toArray
          , estRefSeq map (_.y)
          , residualsOrder).fit().getOrElse(return false))

    }
    true
  }
  //---------------------------------------------------------------------------
  def build(nonStandardWcsType: NON_STANDARD_POLY_WCS_TYPE
            , fits: SimpleFits
            , wcs: WCS): Option[NonStandardPolyWcs] = {

    nonStandardWcsType match {
      case NON_STANDARD_GEN_POLY_WCS       => GeneralPolyWcs.build(fits, wcs)
      case NON_STANDARD_PARABOLIC_POLY_WCS => ParabolicPolyWcs.build(fits, wcs)
    }
  }
  //---------------------------------------------------------------------------
  def build(fits: SimpleFits
            , wcs: WCS): Option[NonStandardPolyWcs] = {
    val v = fits.getStringValueOrEmptyNoQuotation(FITS_KEYWORD_M2_WCS)
    if (v.trim.isEmpty) None
    else {
      val nonStandardWcsType = v.take(4) match {
        case GeneralPolyWcs.FITS_KEYWORD_WCS_PREFIX   => NON_STANDARD_GEN_POLY_WCS
        case ParabolicPolyWcs.FITS_KEYWORD_WCS_PREFIX => NON_STANDARD_PARABOLIC_POLY_WCS
      }

      build(nonStandardWcsType
            , fits
            , wcs)
    }
  }
  //---------------------------------------------------------------------------
  def isPresent(fits: SimpleFits) = fits.existKey(FITS_KEYWORD_M2_WCS)
  //---------------------------------------------------------------------------
}
//=============================================================================
import NonStandardPolyWcs._
trait NonStandardPolyWcs {
  //---------------------------------------------------------------------------
  val order: Int
  val residualOrder: Int
  //---------------------------------------------------------------------------
  val raPolyFit : RegressionPoly_2D
  val decPolyFit: RegressionPoly_2D
  val xPolyFit  : RegressionPoly_2D
  val yPolyFit  : RegressionPoly_2D
  //---------------------------------------------------------------------------
  val crpix: Point2D_Double
  val crval: Point2D_Double
  //---------------------------------------------------------------------------
  val fitsKeywordWcsPrefix: String
  //---------------------------------------------------------------------------
  def getFitsKeywordSeq(wcsPrefix: String, order: Int): Array[String]
  //---------------------------------------------------------------------------
  def pixToSky(pix: Point2D_Double): Point2D_Double = {
    val residual = pix - crpix
    val estimatedValue = Point2D_Double(raPolyFit.estimateValueWithError(residual)
                                        , decPolyFit.estimateValueWithError(residual))
    crval + estimatedValue
  }
  //---------------------------------------------------------------------------
  def skyToPix(raDec: Point2D_Double): Point2D_Double = {
    val residual = raDec - crval
    val estimatedValue = Point2D_Double(xPolyFit.estimateValueWithError(residual)
                                       , yPolyFit.estimateValueWithError(residual))
    crpix + estimatedValue
  }
  //---------------------------------------------------------------------------
  def getFitsRecordSeq() = {
    val mainSeq = getFitsKeywordSeq(fitsKeywordWcsPrefix,order).grouped(GeneralPoly_2D.getParameterSeqCount(order)).toArray
    val mainRecordSeq = Array(
      (FITS_KEYWORD_M2_WCS, "'" + fitsKeywordWcsPrefix +  order.toString + "'" )) ++
      getFitsRecodSeq(raPolyFit, mainSeq(0)) ++
      getFitsRecodSeq(decPolyFit, mainSeq(1)) ++
      getFitsRecodSeq(xPolyFit, mainSeq(2)) ++
      getFitsRecodSeq(yPolyFit, mainSeq(3))

    val residualSeq = getFitsKeywordResidualSeq(residualOrder).grouped(GeneralPoly_2D.getParameterSeqCount(residualOrder)).toArray
    val residualRecordSeq = Array(
      (FITS_KEYWORD_M2_WCS_RESIDUAL, "'" + fitsKeywordWcsPrefix + residualOrder.toString + "'")) ++
      getFitsRecodSeq(raPolyFit.getErrorModel(), residualSeq(0)) ++
      getFitsRecodSeq(decPolyFit.getErrorModel(), residualSeq(1))

    mainRecordSeq ++ residualRecordSeq
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file NonStandardPolyWcs.scala
//=============================================================================
