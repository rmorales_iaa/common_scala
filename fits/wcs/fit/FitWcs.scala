/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  30/Mar/2022
 * Time:  17h:20m
 * Description: based on "fit-wcs" tool of astronomy.net
 */
//=============================================================================
package com.common.fits.wcs.fit
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.database.MatchedImageSource
import com.common.database.mongoDB.database.gaia.{GaiaDB, GaiaPhotometricSource}
import com.common.fits.simpleFits.SimpleFits.KEY_M2_WCS_FIT
import com.common.fits.wcs.fit.algorithm.FitWcsAlgorithm
import com.common.fits.wcs.{SIP, WCS}
import com.common.geometry.point.Point2D_Double
import com.common.geometry.segment.segment2D.Segment2D
import com.common.hardware.cpu.CPU
import com.common.image.astrometry.GaiaCatalog
import com.common.image.myImage.MyImage
import com.common.image.photometry.absolute.PhotometryAbsolute
import com.common.logger.MyLogger
import com.common.stat.StatDescriptive
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
//=============================================================================
import java.awt.{Color, Font}
import java.io.{BufferedWriter, File, FileWriter}
import java.time.LocalDateTime
import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
object FitWcs extends MyLogger {
  //---------------------------------------------------------------------------
  private final val sipOrder            = MyConf.c.getInt("Astrometry.fitWcs.SIP_order")
  private final val sipOrderTable       = MyConf.c.getIntSeq("Astrometry.fitWcs.automaticSelection").grouped(3).toArray
  private final val maxIteration        = MyConf.c.getInt("Astrometry.fitWcs.maxIteration")
  private final val sigmaClipping       = MyConf.c.getDouble("Astrometry.fitWcs.sigmaClipping")
  private final val fitAlgorithmName    = MyConf.c.getString("Astrometry.fitWcs.algorithm")

  private final val raMaxAllowedResidualMas  = MyConf.c.getDouble("Astrometry.fitWcs.raMaxAllowedResidualMas")
  private final val decMaxAllowedResidualMas = MyConf.c.getDouble("Astrometry.fitWcs.decMaxAllowedResidualMas")

  private final val xMaxAllowedResidualPix   = MyConf.c.getDouble("Astrometry.fitWcs.xMaxAllowedResidualPix")
  private final val yMaxAllowedResidualPix   = MyConf.c.getDouble("Astrometry.fitWcs.yMaxAllowedResidualPix")
  //---------------------------------------------------------------------------
  private final val MIN_DIFF_TO_CONSIDER_TWO_DOUBLE_EQUAL = 10e-6
  private final val matchRadiusMas   = MyConf.c.getDouble("Astrometry.fitWcs.matchRadiusMas")
  //---------------------------------------------------------------------------
  private final val fitAlgorithmVersion = FitWcsAlgorithm.getVersion()
  //---------------------------------------------------------------------------
  private final val additionalNoiseTideMultiplier = MyConf.c.getDouble("Astrometry.fitWcs.additionalNoiseTideMultiplier")
  //---------------------------------------------------------------------------
  private final val MIN_ALLOWED_DIFFERENCE_TO_CONSIDER_SAME_VALUE:Double = 1e-10
  //---------------------------------------------------------------------------
  private final val plotArrowDiffRaDecModuleMultiplier  = MyConf.c.getDouble("Astrometry.fitWcs.plotArrowDiffRaDecModuleMultiplier")
  //---------------------------------------------------------------------------
  private def generatePlotDiff(img: MyImage
                               , matchedImageSourceSeq: Array[MatchedImageSource]
                               , raDiffArcoSeq: Array[Double]
                               , decDiffArcoSeq: Array[Double]
                               , pngFileName: String) = {

    val pixScaleX = img.getPixScaleX()
    val pixScaleY = img.getPixScaleY()
    val moduleCorrectionFactor = Math.sqrt((raDiffArcoSeq.max * raDiffArcoSeq.max) +
                                           (decDiffArcoSeq.max * decDiffArcoSeq.max)) * plotArrowDiffRaDecModuleMultiplier
    val arrowDiffMap = ((raDiffArcoSeq zip decDiffArcoSeq).zipWithIndex map { case ((_raDiffArcoSec, _decDiffArcoSec), i) =>
      val raDiffPix = _raDiffArcoSec / pixScaleX
      val decDiffPix = _decDiffArcoSec / pixScaleY
      val modulePix = Math.sqrt((raDiffPix * raDiffPix) + (decDiffPix * decDiffPix)) * moduleCorrectionFactor //radius
      val angleOffsetInRadians =
        if ((raDiffPix > 0) && (decDiffPix < 0)) Math.PI
        else if ((raDiffPix < 0) && (decDiffPix < 0)) -Math.PI
        else 0d
      val angle = Math.atan(decDiffPix / raDiffPix) + angleOffsetInRadians
      val source = matchedImageSourceSeq(i).imageSource
      val endPos = (Point2D_Double(modulePix * Math.cos(angle), modulePix * Math.sin(angle)) + //https://www.mathopenref.com/coordparamcircle.html
        source.getCentroid()).toPoint2D()
      source.id -> endPos
    }).toMap

    MyImage.synthesizeImage(
        pngFileName
      , sourceSeq = matchedImageSourceSeq.map(_.imageSource)
      , imageDimension = img.getDimension()
      , font = Some(new Font(Font.MONOSPACED, Font.PLAIN, 10))
      , fontColor = Color.ORANGE
      , blackAndWhite = true
      , centroidColor = Some(Color.GREEN.getRGB)
      , arrowDiffMap = arrowDiffMap)
  }

  //---------------------------------------------------------------------------
  private def getSipOrder(sourceCount: Int): Int = {
    if (sipOrder != 0) return sipOrder
    else {
      sipOrderTable.foreach { seq => //format string: (SIP_ORDER,MIN_SOURCE_COUNT,MAX_SOURCE_COUNT)
        if ((sourceCount >= seq(1)) && (sourceCount <= seq(2))) return seq.head
      }
    }
    SIP.MAX_SIP_ORDER
  }
  //----------------------------------------------------------------------------
  private def generateDebugInfo(img: MyImage
                                , iteration: Int
                                , matchedSourceSeq: Array[MatchedImageSource]
                                , observingDateMidPoint: String) = {
    val baseName = s"output/${iteration}_${img.getRawName}"
    saveImageMatchGaia(baseName + ".csv"
                       , matchedSourceSeq
                       , observingDateMidPoint
                       , img.getWcs())
    img.getRegion.synthesizeImageWithColor(baseName + ".png"
      , matchedSourceSeq map (_.imageSource)
      , imageDimension = Some(img.getDimension)
    )

    plofDiff(img
              , matchedSourceSeq
              , observingDateMidPoint
              , baseName + "_plot_diff.png")
  }
  //---------------------------------------------------------------------------
  private def saveImageMatchGaia(csvName: String
                                 , matchedImageSourceSeq: Array[MatchedImageSource]
                                 , observigDate: String
                                 , wcs: WCS) {
    val bw = new BufferedWriter(new FileWriter(new File(csvName)))
    val sep = "\t"
    val headerSource = s"m2_id$sep" +
      s"centroid_x${sep}centroid_y$sep" +
      s"ra${sep}dec$sep" +
      s"gaia_source_id$sep" +
      s"gaia_g_mean_mag$sep" +
      s"gaia_ra${sep}gaia_dec$sep" +
      s"gaia_ra_pm${sep}gaia_dec_pm$sep" +
      s"gaia_ra_pm_applied${sep}gaia_dec_pm_applied$sep" +
      s"ra_residual_mas${sep}dec_residual_mas$sep" +
      s"gaia_pm_x_pix${sep}gaia_pm_y_pix" +
      "\n"

    bw.write(headerSource)
    if (matchedImageSourceSeq.isEmpty) bw.write("#None unknown source detected\n")
    else {
      matchedImageSourceSeq.zipWithIndex.foreach { case (matchedImageSource, _) =>

        val gas = matchedImageSource.catalogSource.asInstanceOf[GaiaPhotometricSource]
        val imageSource = matchedImageSource.imageSource

        val gaiaRaDec = gas.getRaDec()
        val gaiaRaDecPM = gas.getRaDecPM
        val gaiaRaDecPM_Applied = gas.getRaDecApplyingPM(LocalDateTime.parse(observigDate))
        val gaiaPM_pix = wcs.skyToPix(gaiaRaDecPM_Applied)

        val sourcePixPos = imageSource.getCentroid()
        val sourceRaDec = imageSource.getCentroidRaDec()
        val residualMas = gas.getResidualRaDecPM_Mas(observigDate, sourceRaDec)

        bw.write(imageSource.id + sep +
          sourcePixPos.x + sep + sourcePixPos.y + sep +
          sourceRaDec.x + sep + sourceRaDec.y + sep +
          gas._id + sep +
          gas.gMeanMag + sep +
          gaiaRaDec.x + sep + gaiaRaDec.y + sep +
          gaiaRaDecPM.x + sep + gaiaRaDecPM.y + sep +
          gaiaRaDecPM_Applied.x + sep + gaiaRaDecPM_Applied.y + sep +
          residualMas.x + sep + residualMas.y + sep +
          gaiaPM_pix.x + sep + gaiaPM_pix.y +
          "\n")
      }
    }
    bw.close

  }
  //---------------------------------------------------------------------------
  private def plofDiff(img: MyImage
                       , matchedImageSourceSeq: Array[MatchedImageSource]
                       , observigDate: String
                       , pngFileName: String) {

    val residualSeq = matchedImageSourceSeq.map { matchedImageSource =>

      val gas = matchedImageSource.catalogSource.asInstanceOf[GaiaPhotometricSource]
      val imageSource = matchedImageSource.imageSource

      val sourceRaDec = imageSource.getCentroidRaDec()
      gas.getResidualRaDecPM_Mas(observigDate, sourceRaDec)
    }
    generatePlotDiff(img
                     , matchedImageSourceSeq
                     , residualSeq map (_.x)
                     , residualSeq map (_.y)
                     , pngFileName: String)

  }
  //---------------------------------------------------------------------------
  private def matchSourceSeqSigmaClipping(img: MyImage
                                          , observingDate: String
                                          , matchedImageSourceSeq: Array[MatchedImageSource]
                                          , forwardConversion: Boolean
                                          , verbose: Boolean) = {
    //-------------------------------------------------------------------------
    def filterResidual(residualSeq: Array[(Double, Int)], maxAllowedValue: Double) = {
      val seq = residualSeq filter ( resPair => Math.abs(resPair._1) < maxAllowedValue)
      StatDescriptive.sigmaClippingDoubleWithPair(seq, sigmaScale = sigmaClipping)
    }
    //-------------------------------------------------------------------------
    val imageName      = img.getRawName()
    val matchMap       = (matchedImageSourceSeq map { m => (m.imageSource.id, m) }).toMap
    val catalogRaDecPM = matchedImageSourceSeq map { m => m.catalogSource.getRaDecWithPM(observingDate) }
    val sourceRaDec    = matchedImageSourceSeq map { m => m.imageSource.getCentroidRaDec() }

    //get the residual
    val residualSeq =
      if (forwardConversion) (catalogRaDecPM zip sourceRaDec) map { case (catalog, source) => catalog.getResidualMas(source) } //catalog is the reference
      else {
        val gaiaPixPos = catalogRaDecPM map (img.skyToPix(_))
        val sourcePixPos = matchedImageSourceSeq map { m => m.imageSource.getCentroid() }
        (sourcePixPos zip gaiaPixPos) map { case (source, catalog) =>
          if (source == Point2D_Double.POINT_INVALID || catalog == Point2D_Double.POINT_INVALID) Point2D_Double(Double.PositiveInfinity, Double.PositiveInfinity)
          else catalog - source  //source is the reference
        }
      }

    val aResidual = residualSeq.zipWithIndex.map { case (residual, i) => (residual.x, matchedImageSourceSeq(i).imageSource.id) }
    val bResidual = residualSeq.zipWithIndex.map { case (residual, i) => (residual.y, matchedImageSourceSeq(i).imageSource.id) }

    //filter the residuals
    val aFiltered =
      if (forwardConversion) filterResidual(aResidual, raMaxAllowedResidualMas)
      else filterResidual(aResidual, xMaxAllowedResidualPix)

    val bFiltered =
      if (forwardConversion) filterResidual(bResidual,  decMaxAllowedResidualMas)
      else filterResidual(bResidual, yMaxAllowedResidualPix)

    val idSeq = (aFiltered map (_._2)).toSet.intersect((bFiltered map (_._2)).toSet)
    val matchFiltered = (idSeq map (i => matchMap(i))).toArray.sortWith(_.imageSource.getCentroid().x < _.imageSource.getCentroid().x)
    val direction = if (forwardConversion) "'forward conversion'" else "'reverse conversion'"
    if (verbose) info(s"Image: '$imageName' has rejected ${residualSeq.length - matchFiltered.length} matched sources due to sigma clipping: $sigmaClipping on (ra,dec) differences in $direction")
    if (matchFiltered.isEmpty) None else Some(matchFiltered)
  }
  //---------------------------------------------------------------------------
  private def matchSourceImageWithCatalog(img: MyImage
                                          , sourceSeq: Array[Segment2D]
                                          , observingDateMidPoint: String
                                          , verbose: Boolean): Option[Array[MatchedImageSource]] = {
    val gaiaDB = synchronized(GaiaDB())
    gaiaDB.buildMapAndKdTree(GaiaCatalog.getCatalog(img))

    val matchedImageSourceSeq =
      gaiaDB.matchImageSourceSeq(
        img
        , sourceSeq
        , matchRadiusMas
        , isBlindSearch = false
        , applyFilterByRaDecError = false
        , verbose)._1
        .sortWith(_.imageSource.getCentroid().x < _.imageSource.getCentroid().x)

      gaiaDB.close()

      if (sigmaClipping == 0) Some(matchedImageSourceSeq)
      else { //apply sigma clipping on matched gaia-image sources
        val r = matchSourceSeqSigmaClipping(img
                                            , observingDateMidPoint
                                            , matchedImageSourceSeq
                                            , forwardConversion = true
                                            , verbose)

        if (r.isEmpty) None
        else
          matchSourceSeqSigmaClipping(img
                                      , observingDateMidPoint
                                      , r.get
                                      , forwardConversion = false
                                      , verbose)
      }
  }
  //----------------------------------------------------------------------------
  private def updateImageWcs(img: MyImage, newWcs: WCS) = {
    val fits = img.getSimpleFits()
    WCS.updateWcs(fits, newWcs.toPairSeq, removeWcs = true)
    img.setWcs(newWcs)
  }
  //----------------------------------------------------------------------------
  private def isNewWcsBetter(lastResidualRaStdDev: Double
                             , lastResidualDecStdDev: Double
                             , prevResidualRaStdDev: Double
                             , prevResidualDecStdDev: Double) = {

    Point2D_Double(lastResidualRaStdDev, lastResidualDecStdDev).getDistance() <
    Point2D_Double(prevResidualRaStdDev, prevResidualDecStdDev).getDistance()
  }

  //----------------------------------------------------------------------------
  private def iterativeFit(img: MyImage
                           , preciseWCS_SIP: Boolean
                           , verbose: Boolean): Boolean = {
    //get fits
    val fits = img.getSimpleFits()

    //get observing mid point
    val observingDateMidPoint = img.getObservingDateMidPoint().toString

    //sources detected
    var sourceSeq: Array[Segment2D] = null

    //variables that control the loop
    var iteration = 0
    var bestFittedWCS = img.getWcs()
    var bestResidualRaStdDev = Double.MaxValue
    var bestResidualDecStdDev = Double.MaxValue
    var bestResidualDistance = Double.MaxValue
    var bestMatchedSourceCount = -1
    val bestPreciseFitting = bestFittedWCS.hasPreciseSIP()
    var bestFitWcsInfo = ""

    var wcsInfo = ""
    var continueFit = true
    var prevResidualRaStdDev = Double.MaxValue
    var prevResidualDecStdDev = Double.MaxValue
    var prevMatchCount = Long.MinValue
    //-----------------------------------------------------------------
    val calculatedCD_Seq = ArrayBuffer[Array[Double]]()//used to check loops calculating the same CD
    //-----------------------------------------------------------------
    def detectCD_Loop(wcs: WCS, sourceMatchCount: Long): Boolean = {
      val newCD = Array(sourceMatchCount.toDouble) ++ wcs.getCD_AsArray()

      calculatedCD_Seq.exists { storedCD =>
        storedCD.zip(newCD).forall { case (a, b) =>
          (a > 0, b > 0) match {
            case (true, false) => false
            case (false, true) => false
            case _ => Math.abs(a - b) < MIN_ALLOWED_DIFFERENCE_TO_CONSIDER_SAME_VALUE
          }
        }
      } match {
        case true =>
          true
        case false =>
          calculatedCD_Seq += newCD
          false
      }
    }
    //-----------------------------------------------------------------

    continueFit = true
    while (iteration <= maxIteration && //this avoid local min solutions
           continueFit) {

      //get  source seq
      if (iteration == 0) {
        //find the sources with the current wcs
        val (_sourceSeq, _) =
          PhotometryAbsolute(img, imageFocus = null, subGaia = null, verbose)
            .getSourceSeq(additionalNoiseTideMultiplier) //detect source just once
        sourceSeq = _sourceSeq.filter (!_.hasLowResolutionCentroid)
      }

      if (verbose)
        info(s"------ Image: '${img.getRawName()}' iteration:$iteration starts -------")


      //update the sky position of the sources
      img.setCentroidRaDec(sourceSeq)

      //get the current source matched source seq using the current wcs
      val matchedImageSourceSeq = matchSourceImageWithCatalog(img
                                                             , sourceSeq
                                                             , observingDateMidPoint
                                                             , verbose)

      //calculate residuals
      val (lastResidualRaStdDev, lastResidualDecStdDev) = MatchedImageSource.getResidualRaDecStatsMas(matchedImageSourceSeq.get, observingDateMidPoint)
      val lastResidualDist = Point2D_Double(lastResidualRaStdDev, lastResidualDecStdDev).getDistance()

      if (matchedImageSourceSeq.isDefined)
        if (verbose) info(s"Image: '${img.getRawName()}' remain: ${matchedImageSourceSeq.get.size} matches between image sources and GAIA sources")

      if (matchedImageSourceSeq.isEmpty || matchedImageSourceSeq.get.isEmpty) {
        wcsInfo = s"0;Ite:$iteration no valid matched sources"
        continueFit = false //stop and keep best wcs
      }
      else {
        val minGaiaSourcesToFit = SIP.getElmentCountTriangularMatrix(getSipOrder(matchedImageSourceSeq.get.size))
        if (matchedImageSourceSeq.get.length < minGaiaSourcesToFit) {
          wcsInfo = s"0;Ite:$iteration no enough matched sources: ${matchedImageSourceSeq.get.length}"
          continueFit = false //stop and keep best wcs
        }
        else {

          if (verbose)
            generateDebugInfo(img, iteration, matchedImageSourceSeq.get, observingDateMidPoint)

          val (fittedWcs, _) = FitWcsAlgorithm.run(
            img
            , catRaDecPosSeq = matchedImageSourceSeq.get map { m => m.catalogSource.getRaDecWithPM(observingDateMidPoint) }
            , imagePixPosSeq = matchedImageSourceSeq.get map { m => m.imageSource.getCentroid() }
            , sipOrder = getSipOrder(matchedImageSourceSeq.get.size)
            , preciseWCS_SIP = preciseWCS_SIP)


          if (fittedWcs.isEmpty) {
            wcsInfo = s"0;Ite:$iteration error fitting wcs"
            if (verbose) warning(s"Image: '${img.getRawName()}' error fitting with algorithm: '$fitAlgorithmName' at iteration: $iteration")
            continueFit = false //stop and keep best wcs
          }
          else {
            if (detectCD_Loop(fittedWcs.get, matchedImageSourceSeq.get.length)) {
              wcsInfo = s"1;Ite:$iteration CD loop detected"
              if (verbose) warning(s"Image: '${img.getRawName()}' CD loop detected fitting with algorithm: '$fitAlgorithmName' at iteration: $iteration")
              continueFit = false //stop and keep best wcs
            }
            else { //found a NEW wcs, update it
              updateImageWcs(img, fittedWcs.get)

              if (verbose)
                warning(s"Image: '${img.getRawName()}' iteration: $iteration matches count:${matchedImageSourceSeq.get.length} (ra,dec) mas stdDev:(${lastResidualRaStdDev.round},${lastResidualDecStdDev.round}) dist:${lastResidualDist.round} mas")

              //avoid more iterations if none improvement is found
              if (matchedImageSourceSeq.get.size == prevMatchCount &&
                Math.abs(lastResidualRaStdDev - prevResidualRaStdDev) < MIN_DIFF_TO_CONSIDER_TWO_DOUBLE_EQUAL &&
                Math.abs(lastResidualDecStdDev - prevResidualDecStdDev) < MIN_DIFF_TO_CONSIDER_TWO_DOUBLE_EQUAL) {
                continueFit = false //stop and keep best wcs
              }
              else { //update wcs
                val newWcsIsBetter = isNewWcsBetter(
                    lastResidualRaStdDev
                  , lastResidualDecStdDev
                  , prevResidualRaStdDev
                  , prevResidualDecStdDev)

                //check if new wcs is better
                if (newWcsIsBetter) {
                  bestFittedWCS = fittedWcs.get
                  bestResidualRaStdDev = lastResidualRaStdDev
                  bestResidualDecStdDev = lastResidualDecStdDev
                  bestMatchedSourceCount = matchedImageSourceSeq.get.size
                  bestResidualDistance = lastResidualDist

                  bestFitWcsInfo =
                    (if (bestPreciseFitting) "1;" else "0;") +
                      s"Ite:$iteration (ra,de) mas std:(" +
                      s"${lastResidualRaStdDev.round}," +
                      s"${lastResidualDecStdDev.round})" +
                      s" src:${matchedImageSourceSeq.get.size}" +
                      s" $fitAlgorithmVersion"
                }
              }
              prevMatchCount = matchedImageSourceSeq.get.size
              prevResidualRaStdDev = lastResidualRaStdDev
              prevResidualDecStdDev = lastResidualDecStdDev
            }
          }
        }
      }
      if (verbose) info(s"------ Image: '${img.getRawName()}' iteration:$iteration ends -------")
      iteration += 1
    }

    //update the best fit wcs
    updateImageWcs(img, bestFittedWCS)
    val finalFitWcsInfo = s"${if (bestFitWcsInfo.isEmpty) wcsInfo else bestFitWcsInfo}"
    warning(s"<-- Image: '${img.getRawName()}' ends fitting info:$bestFitWcsInfo' " +
      s"${if(wcsInfo.isEmpty)"" else s"iteration stop reason:'$wcsInfo'"}-->")
    fits.updateRecord(KEY_M2_WCS_FIT, s"'$finalFitWcsInfo'")
    true
  }
  //---------------------------------------------------------------------------
  private def fitImage(img: MyImage
                       , checkFlag: Boolean
                       , preciseWCS_SIP: Boolean
                       , onlyElapsedDaysLessThan: Option[Int]
                       , verbose: Boolean): Unit = {

     if (img == null) {
      error(s"Can not open the image")
      return
    }

    val imageName = img.getRawName()
    val fits = img.getSimpleFits()
    if (!fits.hasWcs()) {
      error(s"Image:'$imageName' WCS has not found")
      return
    }
    if (checkFlag && fits.existKey(KEY_M2_WCS_FIT)) {
      warning(s"Image:'$imageName' WCS has been previously fitted by m2")
      return
    }

    if (onlyElapsedDaysLessThan.isDefined) {
      val elapsedDayCount = img.getElapsedProcessingTimeinDays()
      if (elapsedDayCount < onlyElapsedDaysLessThan.get) {
        warning(s"Image:'$imageName' ignored because it was processed:'$elapsedDayCount' days ago, but max allowed is:'${onlyElapsedDaysLessThan.get}'")
        return
      }
    }

    if (!iterativeFit(img
                      , preciseWCS_SIP
                      , verbose)) return

    //finally save with the fitted wcs
    if (img.getWcs().hasNonStandardWcs)
      img.saveAsFitsRaw(img.name
                        , userFitsHeader = img.getWcs().getNonStandardWcs.get.getFitsRecordSeq())
    else
      img.saveAsFitsRaw(img.name)
  }
  //---------------------------------------------------------------------------
  def fitDirectory(inputDir: String
                   , checkFlag: Boolean
                   , preciseWCS_SIP: Boolean
                   , onlyElapsedDaysLessThan: Option[Int]
                   , verbose: Boolean): Unit = {
    //-------------------------------------------------------------------------
    val threadCount = CPU.getCoreCount()
    val fitsExtension = MyConf.c.getStringSeq("Common.fitsFileExtension")
    //-------------------------------------------------------------------------
    class MyParallelImageSeq(seq: Array[String]) extends ParallelTask[String](
      seq
      , threadCount
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 1000) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(imageName: String) =
        Try { fitImage(MyImage(imageName)
                       , checkFlag
                       , preciseWCS_SIP
                       , onlyElapsedDaysLessThan
                       , verbose) }    
        match {
          case Success(_) =>
          case Failure(e) =>
            error(e.getMessage + s" Error fitting the WCS on image '$imageName' continuing with the next image")
            error(e.getStackTrace.mkString("\n"))
        }
      //-----------------------------------------------------------------------
    }
    info(s"Fitting the WCS of all images present in directory: '$inputDir'")

    //calculate al files in the input directory
    val fileSeq = Path.getSortedFileList(inputDir, fitsExtension).map(_.getAbsolutePath)
    if (fileSeq.length > 0) new MyParallelImageSeq(fileSeq.toArray)

    //calculate all subdirs
    Path.getSubDirectoryList(inputDir).map(_.getAbsolutePath).foreach { subDirName =>
      info(s"Parsing directory: '$subDirName'")
      val fileSeq = Path.getSortedFileListRecursive(subDirName, fitsExtension).map(_.getAbsolutePath)
      new MyParallelImageSeq(fileSeq.toArray)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file FitWcs.scala
//=============================================================================
