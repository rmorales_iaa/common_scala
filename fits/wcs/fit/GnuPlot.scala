/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Jan/2023
 * Time:  13h:41m
 * Description: None
 */
//=============================================================================
package com.common.fits.wcs.fit
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.util.path.Path
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object GnuPlot {
  //---------------------------------------------------------------------------
  private final val OUTPUT_DIR = "output/tmp/wcs_fitting_plot"
  //---------------------------------------------------------------------------
  private val sep = "\t"
  //---------------------------------------------------------------------------
  private def normalizeNameGnuPlotName(s: String) =
    s.replaceAll("_", """\\\\_""")
  //---------------------------------------------------------------------------
  private def generate(gnuplotFileName: String
                       , fileName: String
                       , csv: String
                       , xAxisCol: Int
                       , yAxisCol: Int
                       , zAxisCol: Int
                       , xAxisLabel: String
                       , yAxisLabel: String
                       , zAxisLabel: String
                       , suffixName: String) = {
    val s =
      s"""
#star of gnu script
#--------------------------------------
#data file separator
set datafile separator "\t"
#--------------------------------------
#variables
my_title = '${normalizeNameGnuPlotName(zAxisLabel)} for image: ${normalizeNameGnuPlotName(fileName)}$suffixName'

my_x_axis_label = '${normalizeNameGnuPlotName(xAxisLabel)}'
my_y_axis_label = '${normalizeNameGnuPlotName(yAxisLabel)}'
my_z_axis_label = '${normalizeNameGnuPlotName(zAxisLabel)}'

my_col_x = $xAxisCol
my_col_y = $yAxisCol
my_col_z = $zAxisCol

csv = '$csv'
#--------------------------------------
#fonts
my_axis_ticks_font = "Courier-New,11"
my_axis_title_font = "Times-Roman,11"
#--------------------------------------
#colors
#background
my_plot_background_color = "#dbdcab"
#--------------------------------------
#title
set title font ",14"
set title my_title
#--------------------------------------
#title
my_point_type = 1 #circle
my_point_size = 1
#--------------------------------------
#plot background color
set object 1 rectangle from screen 0,0 to screen 1,1 fillcolor rgb my_plot_background_color behind
#--------------------------------------
set autoscale #scale axes automatically
unset log #remove any log - scaling
set grid
#--------------------------------------
#axis
set xtics font my_axis_ticks_font
set ytics font my_axis_ticks_font
set ztics font my_axis_ticks_font

set xtic auto
set ytic auto
set ztic auto

set xlabel my_x_axis_label font my_axis_title_font
set ylabel my_y_axis_label font my_axis_title_font
set zlabel my_z_axis_label font my_axis_title_font

set dgrid3d 30,30
set hidden3d
#--------------------------------------
splot csv \\
    using my_col_x:my_col_y:my_col_z with lines pointtype my_point_type pointsize my_point_size lc rgb "blue"
pause - 1
#--------------------------------------
#end of gnu script""".stripMargin

    val bw = new BufferedWriter(new FileWriter(new File(gnuplotFileName)))
    bw.write(s)
    bw.close()
  }
  //---------------------------------------------------------------------------
  def saveGnuPlot(fileName: String
                  , actualPos: Array[Point2D_Double]
                  , targetPos: Array[Point2D_Double]
                  , suffixName: String = "") = {

    val rawName = Path.getOnlyFilenameNoExtension(fileName)
    val dir = Path.getCurrentPath() + Path.resetDirectory(OUTPUT_DIR + "/" + rawName + suffixName)
    val csvName = dir + "wcs.csv"

    val bw = new BufferedWriter(new FileWriter(new File(csvName)))
    val headerSource =
        s"x$sep"+
        s"y$sep"+
        s"z$sep"+
        "\n"

    bw.write(headerSource)

    (actualPos zip targetPos).foreach { case (actual, target) =>
      val residual = actual - target
      val distance = residual.getDistance()
      bw.write(
            target.x + sep + target.y + sep + distance +
          "\n")
    }
    bw.close

    generate(dir + rawName + ".gnuplot"
      , fileName
      , csvName
      , 1
      , 2
      , 3
      , "x"
      , "y"
      , "residual"
      , suffixName
    )
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file GnuPlot.scala
//=============================================================================