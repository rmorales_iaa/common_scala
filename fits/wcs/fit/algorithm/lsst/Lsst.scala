/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  31/Aug/2022
 * Time:  13h:00m
 * Description: None
 */
//=============================================================================
package com.common.fits.wcs.fit.algorithm.lsst
//=============================================================================
import com.common.constant.astronomy.AstronomicalUnit
import com.common.constant.astronomy.AstronomicalUnit.{PRECISE_HALF_PI, PRECISE_PI, PRECISE_RADIANS_TO_DEGREES}
import com.common.coordinate.spherical.SphericalCoordinate
import com.common.fits.simpleFits.SimpleFits.{KEY_EQUINOX, KEY_IMAGEH, KEY_IMAGEW, KEY_LATPOLE, KEY_LONPOLE, KEY_WCSAXES}
import com.common.fits.wcs.fit.algorithm.FitWcsAlgorithm
import com.common.fits.wcs.{SIP, WCS}
import com.common.geometry.point.{Point2D, Point2D_Double}
import com.common.image.astrometry.LsstMatchCatalog
import com.common.image.astrometry.LsstMatchCatalog.SourceMatch
import com.common.image.myImage.MyImage
//=============================================================================
import java.lang.Math._
import scala.collection.mutable.ArrayBuffer
import org.apache.commons.math3.linear.{MatrixUtils, SingularValueDecomposition}
import scala.util.{Failure, Try}
import java.time.LocalDateTime
//=============================================================================
//=============================================================================
object Lsst {
  //---------------------------------------------------------------------------
  val version = "m2-lsst v0.1"
  //---------------------------------------------------------------------------
  private final val MATRIX_UNIT_3x3: Seq[Double] = Seq(  1,0,0
                                                       , 0,1,0
                                                       , 0,0,1)
  //---------------------------------------------------------------------------
  private final val INVERSE_TO_RADIANS_MAT = Seq(  1 / AstronomicalUnit.PRECISE_DEGREES_TO_RADIANS
                                                 , 1 / AstronomicalUnit.PRECISE_DEGREES_TO_RADIANS
                                                 , 0d
                                                 , 0d)
  //---------------------------------------------------------------------------
  private case class MyCoef (p: Int, q: Int, cof: Double)
  //---------------------------------------------------------------------------
  /* LSST: original comment:
     We need a bounding box to define the region over which:
       The forward transformation should be valid
       We list the reverse transformation
     If no BBox is provided, guess one from the input points (extrapolated a bit to allow for fact
     that a finite number of points won't reach to the edge of the image)
   */
  private def getBoundingBox(matchSeq: Array[SourceMatch]) = {
    val boxSeq = matchSeq map {m=>
      val p = m.cartB.pixPos
      Point2D(p.x.toInt,p.y.toInt)
    }
    val boxMin = Point2D.getMin(boxSeq)
    val boxMax = Point2D.getMax(boxSeq)
    val boxDimension = boxMax - boxMin + Point2D.POINT_ONE //lsst origin pix is (0,0)
    val borderFraction = 1 / sqrt(matchSeq.length)
    val borderDouble = Point2D_Double(boxDimension) * borderFraction
    val border = Point2D(borderDouble.x.toInt,borderDouble.y.toInt)
    val newMin = boxMin - border
    val boundingBoxMax = (boxMax + border) - (boxMin - border) + Point2D.POINT_ONE //lsst origin pix is (0,0)
    (newMin - 1, boundingBoxMax)
  }
  //---------------------------------------------------------------------------
  // original list function "std::pair<int, int> indexToPQ(int const index, int const order)"
  private def indexToPQ(index: Int, order: Int) = {
    var p = 0
    var q = index
    var decrement = order

    while( q >= decrement && decrement > 0) {
      q -= decrement
      p += 1
      decrement -= 1
    }
    (p,q)
  }
  //---------------------------------------------------------------------------
  // original list function "Eigen::MatrixXd calculateCMatrix(Eigen::VectorXd const& axis1, Eigen::VectorXd const& axis2,int const order)
  // Given an index and a SIP order, list p and q for the index'th term i^p v^q
  // (Cf. Eqn 2 in http://fits.gsfc.nasa.gov/registry/sip/SIP_distortion_v1_0.pdf)
  private def calculateCD(uvSeq: Array[Point2D_Double], order: Int) = {
    val nTerms = (0.5 * order * (order + 1)).toInt
    val n = uvSeq.size
    val cd = ArrayBuffer.fill(n, nTerms)(0d)
    for (i <-0 until n;
         j <-0 until nTerms) {
      val (p,q) = indexToPQ(j,order)
      val uv = uvSeq(i)
      cd(i)(j) = pow(uv.x,p) * pow(uv.y,q)
    }
    MatrixUtils.createRealMatrix(cd.flatten.toArray.grouped(nTerms).toArray)
  }
  //---------------------------------------------------------------------------
  private def getObservingTimeJulianDate(observingTime: LocalDateTime) = {
    val epoch = SphericalCoordinate.getJulianEpochWithLocalTime(observingTime)
    val epochJulianDate = SphericalCoordinate.getJulianEpochToJulianDate(epoch)
    Point2D_Double(epochJulianDate.djm0,epochJulianDate.djm1)
  }
  //---------------------------------------------------------------------------
  private def getPosFromHipparcosSystem(epochJulianDate: Point2D_Double
                                        , posAsRadians: Point2D_Double) = {
    val pos = SphericalCoordinate.getHipparcosToJ2000(
      posAsRadians.x
      , posAsRadians.y
      , date1 = epochJulianDate.x
      , date2 = epochJulianDate.y)
    Point2D_Double(pos.pos.alpha, pos.pos.delta)
  }
  //---------------------------------------------------------------------------
  //position into the system of the Hipparcos catalogue, assuming zero Hipparcos proper motion
  private def getPosInHiparcosSystem(epochJulianDate: Point2D_Double
                                     , posAsRadians: Point2D_Double) = {
    val pos = SphericalCoordinate.getJ2000ToHipparcos(posAsRadians.x
      , posAsRadians.y
      , date1 = epochJulianDate.x
      , date2 = epochJulianDate.y)
    Point2D_Double(pos.alpha, pos.delta)
  }
  //---------------------------------------------------------------------------
  private def multiplySquareMatrix(matrixA: Array[Double]
                                   , matrixB: Array[Double]
                                   , dim: Int) = {
    val resultMatrix = ArrayBuffer.fill[Double](dim * dim)(0d)
    for (i<-0 until dim;
         j<-0 until dim;
         k<-0 until dim)
      resultMatrix(i * dim + j) += matrixA(i * dim + k) * matrixB(k * dim + j)
    resultMatrix
  }
  //---------------------------------------------------------------------------
  //DIAGONAL matrix multiplication at "matrixmap.c" "AstMatrixMap"
  private def multiplyDiagonalMatrix2x2(left: Seq[Double], right: Seq[Double]) =
    Seq(left(0) * right(0)
        , left(1) * right(0)
        , left(2) * right(1)
        , left(3) * right(1)
    )
  //---------------------------------------------------------------------------
  def inverseSquareMatrix2x2(matrixA: Seq[Double]): Seq[Double] = {
    val a = matrixA(0)
    val b = matrixA(1)
    val c = matrixA(2)
    val d = matrixA(3)
    val determinant = 1 / (a * d - b * c)
    Seq(d * determinant
        , -b * determinant
        , -c * determinant
        , a * determinant)
  }

  //---------------------------------------------------------------------------
  private def multiplyVectorBySquaredMatrix(vector: Array[Double]
                                            , matrix: Array[Double]
                                            , dim: Int): Array[Double] = {
    val result = ArrayBuffer.fill[Double](dim)(0)
    for (i <- 0 until dim) {
      var sum = 0d
      for (j <- 0 until dim) {
        sum += vector(j) * matrix(i * dim + j)
      }
      result(i) = sum
    }
    result.toArray
  }
  //-------------------------------------------------------------------------
  def getPC_ij_x_CD_delt(aWcs: WCS) = {
    val cdelt = aWcs.getCD_delt()
    val inverseCdeltAsMatrix = Seq(1 / cdelt(0), 1 / cdelt(1)) ++ Seq(0d, 0d)

    val pc = aWcs.getPC_ij()
    val inversePC = inverseSquareMatrix2x2(pc)

    val PCxCD  = multiplyDiagonalMatrix2x2(pc,cdelt)
    val inversePCxCD = multiplyDiagonalMatrix2x2(inversePC, inverseCdeltAsMatrix)
    (PCxCD, inversePCxCD)
  }
  //---------------------------------------------------------------------------
  /*Port from AST library
  static AstMatrixMap *MtrRot( AstMatrixMap *this, double theta,
                               const double axis[], int *status )
   */
  private def matrixRotation(theta: Double
                             , axis: Array[Double]
                             , forwardMatrix: Seq[Double] = MATRIX_UNIT_3x3
                             , inverseMatrix: Seq[Double] = MATRIX_UNIT_3x3): (Array[Double],Array[Double]) = {


    val costh = cos(theta)
    val sinth = sin(theta)

    // Return without changing the MatrixMap if the rotation angle is a multiple of 360 degrees.
    if (costh == 1.0) return (axis,axis)

    val axlen2 = axis(0) * axis(0) + axis(1) * axis(1) + axis(2) * axis(2)
    val axlen = sqrt(axlen2)

    // Form commonly used terms in the rotation matrix
    val as = sinth / axlen
    val a = (1.0 - costh) / axlen2
    val b = a * axis(0) * axis(1)
    val c = as * axis(2)
    val d = a * axis(0) * axis(2)
    val e = as * axis(1)
    val f = a * axis(1) * axis(2)
    val g = as * axis(0)

    //Form the 3x3 forward rotation matrix. Theta is the clockwise angle
    //   of rotation looking in the direction of the axis vector.
    val rotmat = ArrayBuffer.fill[Double](9)(0d)
    rotmat(0) = a * axis(0) * axis(0) + costh
    rotmat(1) = b - c
    rotmat(2) = d + e
    rotmat(3) = b + c
    rotmat(4) = a * axis(1) * axis(1) + costh
    rotmat(5) = f - g
    rotmat(6) = d - e
    rotmat(7) = f + g
    rotmat(8) = a * axis(2) * axis(2) + costh

    val calculatedForwardMatrix= multiplySquareMatrix(rotmat.toArray,forwardMatrix.toArray,3)

    rotmat(1) = b + c
    rotmat(2) = d - e
    rotmat(3) = b - c
    rotmat(5) = f + g
    rotmat(6) = d + e

    val calculatedInverseMatrix= multiplySquareMatrix(inverseMatrix.toArray, rotmat.toArray,3)

    (calculatedForwardMatrix.toArray, calculatedInverseMatrix.toArray)
  }
  //---------------------------------------------------------------------------
  //see AST "WcsNative"
  /* It puts the primary meridian of the
    standard system at zero longitude in the rotated system.This results in the
    rotated system being coincident with the standard system */
  private def getAlignmentMatrix(wcs: WCS) = {
    val crvalAsRadians = wcs.getCrvalAsRadians
    val lonLatAsRadians = wcs.getLongitudeLatitudeAsRadians()
    val (fm0, im0) = matrixRotation(-lonLatAsRadians.x, Array(0, 0, 1))
    val (fm1, im1) = matrixRotation(crvalAsRadians.y - PRECISE_HALF_PI, Array(0, 1, 0), fm0, im0)
    matrixRotation(crvalAsRadians.x + PRECISE_PI, Array(0, 0, 1), fm1, im1)
  }
  //---------------------------------------------------------------------------
  //port from AST "fitschan", "SIPMapping"
  private def getCofSeq(coefIndexeq: Array[Seq[Int]]
                        , coefSeq: Array[Double]) = {

    val resultCoefSeq = ArrayBuffer[MyCoef]()
    var k = 0
    for(axis<-0 until 2) {
      for (indexSeq<-coefIndexeq){
        val p = indexSeq.head
        val q = indexSeq.last
        var cof = coefSeq(k)
        if (p == (1 - axis) && q == axis) cof += 1.0
        if (cof != 0.0) resultCoefSeq += MyCoef(p, q, cof)
        k+=1
      }
    }
    resultCoefSeq.toArray
  }
  //---------------------------------------------------------------------------
  private def WCSLIB_forwardProjection(angle: Point2D_Double) = {
    val phi = angle.x
    val theta = angle.y
    val s = SphericalCoordinate.astSind(theta)
    val r = SphericalCoordinate.astCosd(theta) * PRECISE_RADIANS_TO_DEGREES / s
    Point2D_Double(r*SphericalCoordinate.astSind(phi)
                 , -r*SphericalCoordinate.astCosd(phi))
  }
  //---------------------------------------------------------------------------
  private def skyToPix(wcs: WCS
                       , raDecAsRadians: Point2D_Double) = {
    val (_,inverseAlignMatrix) = getAlignmentMatrix(wcs)
    val cartesianPos = SphericalCoordinate.getSphericalPosToCartesian(raDecAsRadians.x, raDecAsRadians.y)
    val cartesianAligPos = multiplyVectorBySquaredMatrix(cartesianPos, inverseAlignMatrix.toArray, cartesianPos.length)
    val sphericalAligPos = Point2D_Double(SphericalCoordinate.cartesianToSpherical(cartesianAligPos)).toDegrees()
    WCSLIB_forwardProjection(sphericalAligPos)
  }
  //---------------------------------------------------------------------------
  //  original list function "CreateWcsWithSip<MatchT>::_calculateForwardMatrices()"
  //(offset,offset,CD,sipA,sipB)
  private def calculateForwardWcs(wcs: WCS
                                  , matchSeq: Array[SourceMatch]
                                  , sipOrder: Int
                                  , observingTime: LocalDateTime) = {
    val crpix = wcs.crpix
    val observingTimeJulianDate = getObservingTimeJulianDate(observingTime)

    val intermediateSeq = matchSeq.map  { m =>

      //iwc (intermediate world coordinates)
      val hipparcosPos = getPosFromHipparcosSystem(observingTimeJulianDate, m.cartA.getRaDecAsRadians)
      val iwc = skyToPix(wcs, hipparcosPos)

      // intermediate pixel coordinates of observed (distorted) positions
      val uv = m.cartB.pixPos - crpix

      (iwc,uv)
    }

    val iwc1_Seq = intermediateSeq.map(_._1.x)
    val iwc2_Seq = intermediateSeq.map(_._1.y)
    val uSeq = intermediateSeq.map(_._2.x)
    val vSeq = intermediateSeq.map(_._2.y)

    val uMax = (uSeq map (abs(_))).max
    val vMax = (vSeq map (abs(_))).max

    // Scale i and v down to [-1,,+1] in order to avoid too large numbers in the polynomials
    val norm = max( uMax,vMax )
    val scaledValueSeq = intermediateSeq.map(_._2 / norm)
    val forwardCD = calculateCD(scaledValueSeq, sipOrder + 1)

    val mu = new SingularValueDecomposition(forwardCD)
      .getSolver
      .solve(MatrixUtils.createRealVector(iwc1_Seq)).toArray

    val nu = new SingularValueDecomposition(forwardCD)
      .getSolver
      .solve(MatrixUtils.createRealVector(iwc2_Seq)).toArray

    val ord = sipOrder + 1
    val CD = Array(
      mu(ord) / norm
      , mu(1) / norm
      , nu(ord) / norm
      , nu(1) / norm
    )

    val CDinv = MatrixUtils.inverse(MatrixUtils.createRealMatrix(CD.grouped(2).toArray))

    // The zeroth elements correspond to a shift in offset
    val newCRPIX = crpix -
      Point2D_Double(mu(0) * CDinv.getEntry(0,0) + nu(0) *  CDinv.getEntry(0,1)
                   , mu(0) * CDinv.getEntry(1,0) + nu(0) *  CDinv.getEntry(1,1))

    // Get Sip terms
    // The rest of the elements correspond to
    // mu[i] == CD11*Apq + CD12*Bpq and
    // nu[i] == CD21*Apq + CD22*Bpq and
    //
    // We solve for Apq and Bpq with the equation
    // (Apq)  = (CD11 CD12)-1  * (mu[i])
    // (Bpq)    (CD21 CD22)      (nu[i])
    val sipA = ArrayBuffer.fill(ord, ord)(0d)
    val sipB = ArrayBuffer.fill(ord, ord)(0d)
    for(i<-0 until mu.length) {
      val (p,q) = indexToPQ(i, ord)
      if (((p + q) > 1) && ((p + q) < ord)) {
        val munu = MatrixUtils.createRealMatrix(Array( Array(mu(i)) , Array(nu(i)) ))
        val AB = CDinv.multiply(munu)

        // Scale back sip coefficients
        val z = 1 / pow(norm, p + q)
        sipA(p)(q) = AB.getEntry(0, 0) * z
        sipB(p)(q) = AB.getEntry(1, 0) * z
      }
    }
    val sip =  SIP(
      SIP.getCoefficientSeq(sipA,sipOrder)
      , SIP.getCoefficientSeq(sipB,sipOrder)
      , sipOrder)

    val newCRVAL = getPosInHiparcosSystem(observingTimeJulianDate,wcs.getCrvalAsRadians).toDegrees()
    val newWcs = WCS(wcs, CD, newCRPIX, newCRVAL, sip)

    WCS(newWcs
        , newWcs.getCD_AsArray
        , newCRPIX + Point2D_Double.POINT_ONE //lsst origin pix is (0,0)
        , newCRVAL)
  }
  //---------------------------------------------------------------------------
  /// original list function  "template <class MatchT> void CreateWcsWithSip<MatchT>::_calculateReverseMatrices()"
  private def calculateReverseWcs(forwardWcs: WCS
                                  , bboxMin: Point2D
                                  , bboxMax: Point2D
                                  , sipInvOrder: Int) = {
    //-------------------------------------------------------------------------
    val x0 = bboxMin.x
    val y0 = bboxMin.y
    val nGrid = sipInvOrder * sipInvOrder //squared
    val dx = bboxMax.x / (nGrid - 1).toDouble
    val dy = bboxMax.y / (nGrid - 1).toDouble
    val UV_Seq = ArrayBuffer[Point2D_Double]()
    val deltaSeq = ArrayBuffer[Point2D_Double]()
    val crPix = forwardWcs.crpix
    val forwardCoefSeq = getCofSeq(SIP.getCoefficientIndexSeq(forwardWcs.sip.aOrder)
                                    , forwardWcs.sip.aSeq ++ forwardWcs.sip.bSeq )

    //-------------------------------------------------------------------------
    def getPointOffset(powerSeq: Array[Array[Double]]
                       , coefSeq: Array[MyCoef]) = {


      val nc = coefSeq.length / 2
      val coefSeqSeq= Seq(coefSeq.take(nc)
                          , coefSeq.drop(nc))
      var term = 0d
      val transformedPoint = ArrayBuffer[Double]()

      //multiply the current term by the exponentiated axis value
      for (out_coord <- 0 until 2) {
        var outval = 0d
        val outcof = coefSeqSeq(out_coord)
        for (ico <- 0 until  nc) {
          val myTerm = outcof(ico)
          val powIndex = Seq(myTerm.p,myTerm.q)
          term = myTerm.cof
          for (in_coord <- 0 until 2) {
            val pow = powIndex(in_coord)
            if (pow > 0) {
              val xp = powerSeq(in_coord)(pow)
              term = term * xp
            }
          }
          outval += term
        }
        transformedPoint += outval
      }
      Point2D_Double(transformedPoint.toArray)
    }
    //-------------------------------------------------------------------------
    //ast library "PolyPowers"
    def getAxisPower(x: Double
                     , maxPower: Int) = {
      val axisPower = ArrayBuffer.fill[Double](maxPower + 1)(0)
      axisPower(0) = 1
      for( ip <- 1 to maxPower )
        axisPower(ip) = axisPower(ip-1) * x
      axisPower.toArray
    }
    //-------------------------------------------------------------------------
    def getGridPointSeq(y: Double) = {
      for (j <- 0 until nGrid) yield {
        val x = x0 + j * dx
        val p = Point2D_Double(x, y) - crPix
        val axisPower = Array(getAxisPower(p.x, forwardWcs.sip.aOrder)
                             , getAxisPower(p.y, forwardWcs.sip.bOrder))

        val pointOffset = getPointOffset(axisPower, forwardCoefSeq)
        pointOffset + crPix //residual. transformedPoint is negative
      }
    }
    //-------------------------------------------------------------------------
    def getShiftedCrPix(sip: SIP) = {

      //new crval to pix
      val pix = skyToPix(
        forwardWcs
        , forwardWcs.getCrvalAsRadians)

      val (_, inversePCxCD_delt) = getPC_ij_x_CD_delt(forwardWcs)
      val inversePCxCD_deltaAsRadians = multiplyDiagonalMatrix2x2(inversePCxCD_delt, INVERSE_TO_RADIANS_MAT)
      val shiftedPix = Point2D_Double(multiplyDiagonalMatrix2x2(inversePCxCD_deltaAsRadians,pix.toSequence()))

      //apply AP and BP SIP
      val axisPower = Seq(getAxisPower(shiftedPix.x, sip.apOrder)
                        , getAxisPower(shiftedPix.y, sip.bpOrder))
      val APBP_CoefSeq = getCofSeq(SIP.getCoefficientIndexSeq(sip.apOrder)
                                   , sip.apSeq ++ sip.bpSeq)
      val pointOffset = getPointOffset(axisPower.toArray, APBP_CoefSeq)
      pointOffset + forwardWcs.crpix + Point2D_Double.POINT_ONE   //back to origin (1,1)
    }
    //-------------------------------------------------------------------------
    //get the grid points
    for (i<-0 until nGrid){
      val y = y0 + i * dy
      val gridPointSeq = getGridPointSeq(y)
      for (j<-0 until nGrid) {
        val x = x0 + j * dx
        // i and v are intermediate pixel coordinates on a grid of positions
        val uv = Point2D_Double(x,y) - crPix
        val UV = gridPointSeq(j) - crPix + Point2D_Double.POINT_ONE //lsst origin pix is (0,0)
        UV_Seq += UV
        deltaSeq += uv - UV
      }
    }

    (UV_Seq map (p=> abs(p.x))).max
    (UV_Seq map (p=> abs(p.y))).max

    // Scale i and v down to [-1,,+1] in order to avoid too large numbers in the polynomials
    val norm = max( (UV_Seq map (p=> abs(p.x))).max,(UV_Seq map (p=> abs(p.y))).max)
    val UV_Scaled = UV_Seq map (_ / norm)

    val cd = calculateCD(UV_Scaled.toArray,sipInvOrder + 1)

    val apSeq = new SingularValueDecomposition(cd)
      .getSolver
      .solve(MatrixUtils.createRealVector((deltaSeq map (_.x)).toArray)).toArray

    val bpSeq = new SingularValueDecomposition(cd)
      .getSolver
      .solve(MatrixUtils.createRealVector((deltaSeq map (_.y)).toArray)).toArray

    val ord = sipInvOrder + 1
    val sipAP = ArrayBuffer.fill(ord, ord)(0d)
    val sipBP = ArrayBuffer.fill(ord, ord)(0d)
    for(i<-0 until apSeq.length) {
      val (p,q) = indexToPQ(i, ord)

      // Scale back sip coefficients
      val z = 1 / pow(norm, p + q)
      sipAP(p)(q) = apSeq(i) * z
      sipBP(p)(q) = bpSeq(i) * z
    }

    //process final WCS
    val sip = SIP(
        forwardWcs.sip.aSeq
      , forwardWcs.sip.bSeq
      , SIP.getCoefficientSeq(sipAP,sipInvOrder)
      , SIP.getCoefficientSeq(sipBP,sipInvOrder)
      , forwardWcs.sip.aOrder
      , sipInvOrder)

    WCS(forwardWcs
      , forwardWcs.getCD_AsArray()
      , getShiftedCrPix(sip)
      , forwardWcs.crval
      , sip)
  }
  //---------------------------------------------------------------------------
  //original list function "CreateWcsWithSip<MatchT>::CreateWcsWithSip(std::vector<MatchT> const& matches,
  //                                           afw::geom::SkyWcs const& linearWcs, int const order,
  //                                          geom::Box2I const& bbox, int const ngrid)"
  def buildWcs(wcs: WCS
               , matchSeq: Array[SourceMatch]
               , sipOrder: Int
               , observingTime: LocalDateTime) : Option[WCS]= {

    val (bboxMin,bboxMax) = getBoundingBox(matchSeq)
    val forwardWcs = calculateForwardWcs(wcs
                                         , matchSeq
                                         , sipOrder
                                         , observingTime)
    Some(calculateReverseWcs(
        forwardWcs
      , bboxMin
      , bboxMax
      , sipOrder + 1))
  }
  //---------------------------------------------------------------------------
  def getKeyValueMap(wcs: WCS, stringSeq: Array[String]) = {
    val pairSeq = stringSeq.flatMap { s=>
      if (s.startsWith("#")) None
      else {
        val seq = s.split("=")
        val k = seq(0).trim
        var value = seq(1).trim
        if(value.startsWith("\""))
          value = value.replaceAll("\"","'")
        Some((k,value))
      }
    }
    (pairSeq ++ Array(
      (KEY_WCSAXES,wcs.axes.toString)
      , (KEY_IMAGEW,wcs.imageW.toString)
      , (KEY_IMAGEH,wcs.imageH.toString)
      , (KEY_LONPOLE,wcs.lonPole.toString)
      , (KEY_LATPOLE,wcs.latPole.toString)
      , (KEY_EQUINOX,wcs.equinox.toString)
    )).toMap
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Lsst(img:MyImage
                , catRaDecSeq: Array[Point2D_Double]
                , imagePixSeq: Array[Point2D_Double] //reference pix(1,1)
                , sipOrder: Int) extends FitWcsAlgorithm {
  //---------------------------------------------------------------------------
  def run(): Option[WCS] = {

    val imageName = img.name
    val wcs = img.getWcs()

    Try {
      val imagePixSeqOrigin=  imagePixSeq map ( _ - Point2D_Double.POINT_ONE)//LSST requires origin at 0,0
      val matchSeq = LsstMatchCatalog.calculate(
        catRaDecSeq
        , imageRaDecSeq = imagePixSeqOrigin map { pix=> wcs.pixToSky ( pix )}
        , catalogPixPosSeq = catRaDecSeq map (raDec=> wcs.skyToPix(raDec) - Point2D_Double.POINT_ONE) //LSST requires origin at 0,0
        , imagePixSeqOrigin)

      if (matchSeq.isEmpty) return None
      else return Lsst.buildWcs(wcs,matchSeq,sipOrder,img.getObservingTime())
    }
    match {
      case Failure(ex) =>
        error(s"Image: '$imageName'.Error in wcs fitting WCS with list algorithm")
        error(ex.toString)
        ex.printStackTrace()
    }
    None
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Lsst.scala
//=============================================================================
