/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  30/Mar/2022
 * Time:  21h:39m
 * Description: based on "fit-wcs" tool of astronomy.net
 */
//=============================================================================
package com.common.fits.wcs.fit.algorithm.astrometry_net
//=============================================================================
import com.common.coordinate.conversion.Conversion.{normalize_XYZ, raDecSeqToXYZ_Seq, xyzSeqToRaDecSeq}
import com.common.fits.wcs.WCS
import com.common.fits.wcs.fit.algorithm.astrometry_net.Util._
import com.common.geometry.point.Point2D_Double
import com.common.logger.MyLogger
import org.apache.commons.math3.linear.Array2DRowRealMatrix
//=============================================================================
import org.apache.commons.math3.linear.SingularValueDecomposition
import java.lang.Math._
import scala.collection.mutable.ArrayBuffer
//=============================================================================
object FitTanProjection extends MyLogger {
  //--------------------------------------------------------------------------
  //original code: 'fit_tan_wcs_solve'
  def getCentredPixPosSeq(pixPosSeq: Array[Point2D_Double]) = {
    var xCentreOfMass = 0d
    var yCentreOfMass = 0d
    pixPosSeq foreach { p =>
      xCentreOfMass += p.x
      yCentreOfMass += p.y
    }
    val centreOfMasses = Point2D_Double(xCentreOfMass / pixPosSeq.length,yCentreOfMass / pixPosSeq.length)
    (centreOfMasses,pixPosSeq map { p=> p - centreOfMasses})
  }
  //--------------------------------------------------------------------------
  //original code : 'fit_tan_wcs_solve'
  def getNormalizedXYZ_Centre(xyzSeq: Array[Array[Double]]) = {
    var xCentreOfMass = 0d
    var yCentreOfMass = 0d
    var zCentreOfMass = 0d
    xyzSeq foreach { a =>
      xCentreOfMass += a(0)
      yCentreOfMass += a(1)
      zCentreOfMass += a(2)
    }
    normalize_XYZ(xCentreOfMass,yCentreOfMass,zCentreOfMass)
  }
  //---------------------------------------------------------------------------
  //original code : 'fit_tan_wcs_solve'
  def projectSourceAndSubtractCentre(xyzSeq: Array[Array[Double]], normalizedXYZ_Centre:Array[Double]):
  Option[Array[Point2D_Double]] = {
    val projectedSourceSeq = xyzSeq map { xyz=>
      val r = star_coords(xyz,normalizedXYZ_Centre, true)
      if (!r._1) {
        error(s"Error proyecting: $xyz")
        return None
      }
      r._2
    }
    var xCentreOfMass = 0d
    var yCentreOfMass = 0d
    projectedSourceSeq foreach { p =>
      xCentreOfMass += p.x
      yCentreOfMass += p.y
    }
    val centreOfMasses = Point2D_Double(xCentreOfMass / xyzSeq.length, yCentreOfMass / xyzSeq.length)
    Some(projectedSourceSeq map { p=> p - centreOfMasses})
  }
  //---------------------------------------------------------------------------
  //original code : 'fit_tan_wcs_solve'
  private def getCovarianceMatrix(projectedCentredSourceSeq: Array[Point2D_Double], centredPixPosSeq: Array[Point2D_Double])
  : Option[(Array[Double],Double)] = {
    val p = projectedCentredSourceSeq flatMap (_.toArray())
    val f = centredPixPosSeq flatMap (_.toArray())
    val covariance = ArrayBuffer.fill[Double](4)(0)
    for (i<-0 until projectedCentredSourceSeq.length;
         j<-0 until 2;
         k<-0 until 2){
      covariance(j * 2 + k) += p(i*2 + k) * f(i * 2 + j)
    }
    val r = covariance forall (v=> !v.isNaN && !v.isNegInfinity && !v.isPosInfinity)
    if (!r) {
      error(s"Covariance invalid")
      return None
    }
    //calculate scale
    var pSum = 0d
    var fSum = 0d
    for (i<-0 until projectedCentredSourceSeq.length;
         j<-0 until 2){
      val _p = p( i* 2 + j)
      val _f = f( i* 2 + j)
      pSum += (_p * _p)
      fSum += (_f * _f)
    }
    Some((covariance.toArray,toDegrees(sqrt(pSum/fSum))))
  }
  //---------------------------------------------------------------------------
  //original code : 'fit_tan_wcs_solve'
  //singular value decomposition
  private def SVD(covariance: Array[Double]): Option[Array[Double]] = {
    val rm = new Array2DRowRealMatrix(covariance.sliding(2,2).toArray)
    val decomposition = new SingularValueDecomposition(rm)
    val U = decomposition.getU
    val V = decomposition.getV
    val R = V.multiply(U.transpose())
    val result = R.getData.flatten
    val r = result.forall (v=> !v.isNaN && !v.isNegInfinity && !v.isPosInfinity)
    if (!r) {
      error(s"Error in the matrix decomposition")
      return None
    }
    Some(result)
  }
  //---------------------------------------------------------------------------
  //original code: 'fit_tan_wcs_solve'
  def calculate(wcs: WCS,catalogPosRaDecSeq: Array[Point2D_Double], pixPosSeq: Array[Point2D_Double])
    : Option[(WCS, Array[Array[Double]])] = {
    val xyzSeq = raDecSeqToXYZ_Seq(catalogPosRaDecSeq)
    val (centredPixPos,centredPixPosSeq) = getCentredPixPosSeq(pixPosSeq)
    val normalizedXYZ_Centre = getNormalizedXYZ_Centre(xyzSeq)
    val projectedCentredSourceSeq = projectSourceAndSubtractCentre(xyzSeq,normalizedXYZ_Centre).getOrElse(return None)
    val (covariance,scale) = getCovarianceMatrix(projectedCentredSourceSeq, centredPixPosSeq).getOrElse(return None)
    val svd = SVD(covariance).getOrElse(return None)

    //calculate the new WCS CD matrix
    val wcsCD = svd map (_ * scale)
    val r = wcsCD.forall (v=> !v.isNaN && !v.isNegInfinity && !v.isPosInfinity)
    if (!r) {
      error(s"Error in the calculating WCS matrix")
      return None
    }
    val CRPIX = centredPixPos
    val CRVAL = xyzSeqToRaDecSeq(Array(normalizedXYZ_Centre)).head
    val newWCS = WCS(
      wcs
    , wcsCD
    , CRPIX
    , CRVAL)

    Some((newWCS, xyzSeq))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FitTanProjection.scala
//=============================================================================
