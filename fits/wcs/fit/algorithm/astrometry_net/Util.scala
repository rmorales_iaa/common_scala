/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  31/Mar/2022
 * Time:  11h:07m
 * Description: based on "fit-wcs" tool of astronomy.net
 */
//=============================================================================
package com.common.fits.wcs.fit.algorithm.astrometry_net
//=============================================================================
import com.common.geometry.point.Point2D_Double
//=============================================================================
import java.lang.Math._
//=============================================================================
//=============================================================================
object Util {
  //---------------------------------------------------------------------------
  //original code: 'star_coords'
  def star_coords(xyz: Array[Double], centroid : Array[Double], useTangentProjection: Boolean) : (Boolean,Point2D_Double) = {
    val sdotr = xyz(0) * centroid(0) + xyz(1) * centroid(1) + xyz(2) * centroid(2)
    if (sdotr <= 0d) return (false,Point2D_Double.POINT_ZERO) // on the opposite side of the sky
    centroid(2) match {
      case 1d =>                //North pole
        val inv_s2 = 1.0 / xyz(2)
        if (useTangentProjection) (true,Point2D_Double(xyz(0) * inv_s2,xyz(1) * inv_s2))
        else (true,Point2D_Double(xyz(0),xyz(1)))

      case -1d =>              //South pole
        val inv_s2 = 1.0 / xyz(2)
        if (useTangentProjection) (true,Point2D_Double(-xyz(0) * inv_s2, xyz(1) * inv_s2))
        else (true,Point2D_Double(-xyz(0),xyz(1)))

      case _ =>
        var etax = -centroid(1)
        var etay = centroid(0)
        val eta_norm = hypot(etax, etay)
        val inv_en = 1d / eta_norm
        etax = etax * inv_en
        etay = etay * inv_en
        val xix = -centroid(2) * etay
        val xiy = centroid(2) * etax
        val xiz = centroid(0) * etay - centroid(1) * etax
        val x = xyz(0) * etax + xyz(1) * etay
        val y = xyz(0) * xix + xyz(1) *  xiy + xyz(2) * xiz
        if (useTangentProjection) {
          val inv_sdotr = 1d / sdotr
          (true,Point2D_Double(x * inv_sdotr, y * inv_sdotr))
        }
        else (true,Point2D_Double(x,y))
    }
  }
  //--------------------------------------------------------------------------
}
//=============================================================================
//End of file Util.scala
//=============================================================================
