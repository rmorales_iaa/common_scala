/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  13/Jul/2022
 * Time:  10h:05m
 * Description: None
 */
//=============================================================================
package com.common.fits.wcs.fit.algorithm.astrometry_net
//=============================================================================
import com.common.configuration.MyConf
import com.common.fits.wcs.{SIP, WCS}
import com.common.fits.wcs.fit.algorithm.FitWcsAlgorithm
import com.common.geometry.point.Point2D_Double
import com.common.image.myImage.MyImage
import com.common.logger.MyLogger
//=============================================================================
//=============================================================================
object Astrometry_net extends MyLogger {
  //---------------------------------------------------------------------------
  val version = "m2_a_net v0.2"
  //---------------------------------------------------------------------------
  private final val doShift = MyConf.c.getInt("Astrometry.fitWcs.astrometryDotNet.doShift")
  //---------------------------------------------------------------------------
  private def checkInput(imageName: String, sipOrder: Int, inputPointCount: Int) = {
    val sipCoefficientCount = SIP.getElmentCountTriangularMatrix(sipOrder) //triangular matrix missing the 0,0 element.
    if (inputPointCount >= sipCoefficientCount) true
    else {
      warning(s"Image: '$imageName' Input point sequence: $inputPointCount is less than min expected: $sipCoefficientCount for a SIP order: $sipOrder")
      false
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import Astrometry_net._
case class Astrometry_net(img: MyImage
                          , catRaDecSeq: Array[Point2D_Double]
                          , imagePixSeq: Array[Point2D_Double]
                          , sipOrder: Int) extends FitWcsAlgorithm {
  //---------------------------------------------------------------------------
  def run(): Option[WCS] = {
    val imageName = img.name
    val wcs = img.getWcs()
    if (!checkInput(imageName,sipOrder,imagePixSeq.length)) return None
    val (newWcsA,xyzSeq) = FitTanProjection.calculate(wcs,catRaDecSeq,imagePixSeq).getOrElse(return None)
    val newCRVAL = newWcsA.pixToSkyLinear(Point2D_Double(wcs.crpixX,wcs.crpixY))  //apply only linear transformation, SIP contribution will be added just below
    val currentWcs = WCS(newWcsA
                     , newWcsA.getCD_AsArray
                     , wcs.crpix
                     , newCRVAL)
    Sip.calculate(currentWcs
                  , xyzSeq
                  , imagePixSeq
                  , sipOrder
                  , doShift == 1)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================

//=============================================================================
//End of file Astrometry_net.scala
//=============================================================================
