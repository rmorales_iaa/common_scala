/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  31/Mar/2022
 * Time:  13h:11m
 * Description: based on "fit-wcs" tool of astronomy.net
 */
//=============================================================================
package com.common.fits.wcs.fit.algorithm.astrometry_net
//=============================================================================
import com.common.configuration.MyConf
import com.common.coordinate.conversion.Conversion.raDecSeqToXYZ_Seq
import com.common.fits.wcs.fit.algorithm.astrometry_net.Util.star_coords
import com.common.fits.wcs.{SIP, WCS}
import com.common.geometry.point.Point2D_Double
import org.apache.commons.math3.linear.{Array2DRowRealMatrix, QRDecomposition}
//=============================================================================
import java.lang.Math._
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object Sip {
  //-------------------------------------------------------------------------
  private final val INVERSE_SIP_GRID_POINT_COUNT  = MyConf.c.getInt("Astrometry.fitWcs.astrometryDotNet.sipInverseGridPointCount")
  //---------------------------------------------------------------------------
  private def getSIP_Polynomial(order: Int, coeffSeq: Array[Array[Double]]) = {
    (for (i<-0 to order;
          j<-0 to order) yield
      if ((i + j) <= order) Some(coeffSeq(i)(j)) else None).toArray.flatten
  }
  //---------------------------------------------------------------------------
  //original code: 'gslutils_solve_leastsquares'
  //solving with QR decomposition
  private def solveCoeff(mCoeff: Array[Array[Double]]
                         , mRa:Array[Double]
                         , mDec:Array[Double]) = {
    val equationCount = mCoeff.length
    val coeffCount = mCoeff(0).length
    val (mCoeffFinal,mRaFinal,mDecFinal) =
      if (equationCount < coeffCount) (mCoeff.take(equationCount), mRa.take(equationCount), mDec.take(equationCount))
      else (mCoeff,mRa,mDec)
    val decomposition = new QRDecomposition(new Array2DRowRealMatrix(mCoeffFinal))
    val solvedRa  = decomposition.getSolver.solve(new Array2DRowRealMatrix(mRaFinal)).getData.flatten
    val solvedDec = decomposition.getSolver.solve(new Array2DRowRealMatrix(mDecFinal)).getData.flatten
    (solvedRa,solvedDec)
  }
  //---------------------------------------------------------------------------
  //original code: 'fit_sip_wcs'
  private def getMatrixSeq(wcs:WCS
                           , xyzSeq: Array[Array[Double]]
                           , pixPosSeq: Array[Point2D_Double]
                           , sipOrder: Int) = {
    val crvalXYZ = raDecSeqToXYZ_Seq(Array(wcs.crval)).head
    val crpix = wcs.crpix
    val mCoeff = ArrayBuffer[Array[Double]]()
    val mRa = ArrayBuffer[Double]()
    val mDec = ArrayBuffer[Double]()
    (pixPosSeq zip xyzSeq) foreach { case (pixPos, xyz) =>
      val uv = pixPos - crpix //intermediate world coordinates in degrees
      val (r, xy) = star_coords(xyz, crvalXYZ, true)
      if (!r) None //Skipping star that cannot be projected to tangent plane
      else {
        val raDec = Point2D_Double(toDegrees(xy.x), toDegrees(xy.y))
        val m = for (order <- 0 to sipOrder;
                     q <- 0 to order) yield {
          val p = order - q
          pow(uv.x, p) * pow(uv.y, q)
        }
        //check m
        if ((m(0) == 1d) &&
          (abs(m(1) - uv.x) < 1e-12) &&
          (abs(m(2) - uv.y) < 1e-12)) {
          mCoeff += m.toArray
          mRa += raDec.x
          mDec+= raDec.y
        }
      }
    }
    (mCoeff.toArray,mRa.toArray,mDec.toArray)
  }
  //---------------------------------------------------------------------------
  private def invertCD(cd: Array[Double]) = {
    val cd_11 = cd(0)
    val cd_12 = cd(1)
    val cd_21 = cd(2)
    val cd_22 = cd(3)
    val determinant = 1 / (cd_11 * cd_22 - cd_12 * cd_21)
    Array( determinant * cd_22
      ,-determinant * cd_12
      ,-determinant * cd_21
      , determinant * cd_11)
  }
  //---------------------------------------------------------------------------
  private def evaluateWithDistorsion(cofSeqA:Array[Array[Double]]
                                      , cofSeqB:Array[Array[Double]]
                                      , order: Int
                                      , u: Double
                                      , v: Double) = {
    //pre-calculate the power
    val powerU = ArrayBuffer.fill(SIP.MAX_SIP_ORDER+2)(0d)
    val powerV = ArrayBuffer.fill(SIP.MAX_SIP_ORDER+2)(0d)
    powerU(0) = 1d
    powerU(1) = u
    powerV(0) = 1d
    powerV(1) = v
    for(i <-2 to order){
      powerU(i) = powerU(i-1) * u
      powerV(i) = powerV(i-1) * v
    }
    //calculate the distorsion
    var fuv = u
    var guv = v
    for(p<-0 to order;
        q<-0 to order) {
      if ((p + q) <= order) {
        fuv += cofSeqA(p)(q) * powerU(p) * powerV(q)
        guv += cofSeqB(p)(q) * powerU(p) * powerV(q)
      }
    }
    (fuv, guv)
  }
  //---------------------------------------------------------------------------
  //original code: 'sip_compute_inverse_polynomials'
  private def calculateInverseCoeff(wcs: WCS, aSeq:Array[Array[Double]], bSeq:Array[Array[Double]]) = {
    val inverseSipOrder = aSeq.length
    val sipOrder = inverseSipOrder - 1

    val nx =  INVERSE_SIP_GRID_POINT_COUNT * (inverseSipOrder + 1)
    val ny =  INVERSE_SIP_GRID_POINT_COUNT * (inverseSipOrder + 1)
    val minu = -wcs.crpix.x
    val maxu = wcs.imageW - wcs.crpix.x
    val minv = -wcs.crpix.y
    val maxv = wcs.imageH - wcs.crpix.y
    val n = (inverseSipOrder + 1) * (inverseSipOrder + 2) / 2  // upper triangle polynomial terms
    val m = ArrayBuffer[ArrayBuffer[Double]]()
    for(_<-0 until nx * ny)
      m += ArrayBuffer.fill[Double](n)(0d)

    //sample grid locations
    val fuvSeq = ArrayBuffer[Double]()
    val guvSeq = ArrayBuffer[Double]()
    var i = 0
    for (gu <-0 until nx;
         gv <-0 until ny) {
      // calculate grid position in original image pixels
      val u = (gu * (maxu - minu) / (nx-1)) + minu
      val v = (gv * (maxv - minv) / (ny-1)) + minv
      val (_U,_V) = evaluateWithDistorsion(aSeq,bSeq,sipOrder,u,v)
      val fuv = _U - u
      val guv = _V - v
      var j = 0

      for (p<- 0 to inverseSipOrder;
           q<-0 to inverseSipOrder)
        if (p + q <= inverseSipOrder) {
          m(i)(j) = pow(_U,p) * pow(_V,q)
          j +=1
        }

      fuvSeq += -fuv
      guvSeq += -guv
      i += 1
    }

    val (solvedCoeffX_Seq,solvedCoeffY_Seq) = solveCoeff((m map(_.toArray)).toArray
      , fuvSeq.toArray
      , guvSeq.toArray)

    //backward coefficients
    val apSeq = ArrayBuffer[ArrayBuffer[Double]]()
    val bpSeq = ArrayBuffer[ArrayBuffer[Double]]()
    for(_<-0 until inverseSipOrder + 1) {
      apSeq += ArrayBuffer.fill[Double](inverseSipOrder + 1)(0d)
      bpSeq += ArrayBuffer.fill[Double](inverseSipOrder + 1)(0d)
    }
    var j = 0
    for(p<-0 to inverseSipOrder;
        q<-0 to inverseSipOrder) {
      if ((p + q) <= inverseSipOrder) {
        apSeq(p)(q) = solvedCoeffX_Seq(j)
        bpSeq(p)(q) = solvedCoeffY_Seq(j)
        j += 1
      }
    }
    ((apSeq map (_.toArray)).toArray,(bpSeq map (_.toArray)).toArray)
  }
  //---------------------------------------------------------------------------
  //original code: 'fit_sip_wcs'
  private def calculateSIP(wcs: WCS
                           , cd:Array[Double]
                           , solvedCoeffRaSeq:Array[Double]
                           , solvedCoeffDecSeq:Array[Double]
                           , sipOrder: Int) = {

    //forward coefficients
    val aSeq = ArrayBuffer[ArrayBuffer[Double]]()
    val bSeq = ArrayBuffer[ArrayBuffer[Double]]()

    for(_<-0 until sipOrder + 1) {
      aSeq += ArrayBuffer.fill[Double](sipOrder + 1)(0d)
      bSeq += ArrayBuffer.fill[Double](sipOrder + 1)(0d)
    }
    val cd_11 = cd(0)
    val cd_12 = cd(1)
    val cd_21 = cd(2)
    val cd_22 = cd(3)
    var i = 0
    for (order <-0 to sipOrder;
         q <-0 to order) {
      val p = order - q
      val ra = solvedCoeffRaSeq(i)
      val dec = solvedCoeffDecSeq(i)
      aSeq(p)(q) = ra * cd_11 + dec * cd_12
      bSeq(p)(q) = ra * cd_21 + dec * cd_22
      i += 1
    }
    // We have already dealt with the shift and linear terms, so zero them out in the SIP coefficient matrix.
    //linear coefficients
    aSeq(0)(0) = 0.0
    aSeq(0)(1) = 0.0
    aSeq(1)(0) = 0.0
    bSeq(0)(0) = 0.0
    bSeq(0)(1) = 0.0
    bSeq(1)(0) = 0.0

    val _aSeq = (aSeq map (_.toArray)).toArray
    val _bSeq = (bSeq map (_.toArray)).toArray
    val(apSeq,bpSeq) = calculateInverseCoeff(wcs,_aSeq,_bSeq)

    SIP(getSIP_Polynomial(sipOrder,_aSeq)
      , getSIP_Polynomial(sipOrder,_bSeq)
      , getSIP_Polynomial(sipOrder + 1,apSeq)
      , getSIP_Polynomial(sipOrder + 1,bpSeq)
      , sipOrder
      , sipOrder + 1)
  }
  //---------------------------------------------------------------------------
  //original code: 'wcs_shift'
  private def wcsCD_Shift(wcs: WCS, pixShift: Point2D_Double) = {
    val newCRPIX = wcs.crpix + pixShift
    val newCRVAL = WCS(wcs,newCRPIX).pixToSky(wcs.crpix)
    var theta = -toRadians(newCRVAL.x - wcs.crval.x)
    theta = theta * sin(toRadians(newCRVAL.y))
    val sintheta = sin(theta)
    val costheta = cos(theta)
    val cd = wcs.getCD_AsArray()
    val newCD = Array(
        costheta * cd(0) - sintheta * cd(1)
      , sintheta * cd(0) + costheta * cd(1)
      , costheta * cd(2) - sintheta * cd(3)
      , sintheta * cd(2) + costheta * cd(3)
    )
    WCS(wcs, newCD, wcs.crpix, newCRVAL)
  }
  //---------------------------------------------------------------------------
  //original code: 'fit_sip_wcs'
  def calculate(wcs:WCS
                , xyzSeq: Array[Array[Double]]
                , pixPosSeq: Array[Point2D_Double]
                , sipOrder: Int
                , doShift: Boolean = true): Option[WCS] = {
    val (mCoeff,mRa,mDec) = getMatrixSeq(wcs, xyzSeq, pixPosSeq, sipOrder)
    val (solvedCoeffRaSeq,solvedCoeffDecSeq) = solveCoeff(mCoeff, mRa, mDec)
    val cd = Array(solvedCoeffRaSeq(1)
                   , solvedCoeffRaSeq(2)
                   , solvedCoeffDecSeq(1)
                   , solvedCoeffDecSeq(2))
    val invertedCD = invertCD(cd)
    val newSip = calculateSIP(wcs, invertedCD, solvedCoeffRaSeq, solvedCoeffDecSeq, sipOrder)
    val newWcs = WCS(wcs,cd,newSip)
    if (doShift) {
      val sx = solvedCoeffRaSeq.head
      val sy = solvedCoeffDecSeq.head
      val sU = invertedCD(0) * sx + invertedCD(1) * sy
      val sV = invertedCD(2) * sx + invertedCD(3) * sy
      val pixShift = (Point2D_Double(sU,sV) + newSip.evalInverse(sU, sV)).inverse
      Some(wcsCD_Shift(newWcs,pixShift))
    }
    else Some(newWcs)
  }
  //---------------------------------------------------------------------------
  private def getSIP_CoeffIndex(order: Int) =
    (for (i <- 0 to order;
          j <- 0 to order) yield
      if ((i + j) <= order) Some((i,j))
      else None).flatten.toArray

  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Sip.scala
//=============================================================================
