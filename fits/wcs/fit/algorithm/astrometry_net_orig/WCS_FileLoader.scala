/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.common.fits.wcs.fit.algorithm.astrometry_net_orig
//=============================================================================
import com.common.fits.simpleFits.SimpleFits._
//=============================================================================
import scala.io.Source
//=============================================================================
//=============================================================================
object WCS_FileLoader {
  //---------------------------------------------------------------------------
  private final val WCS_GENERATED_WCS_FIT_FILE_IGNORED_KEY = Seq(KEY_SIMPLE, KEY_BIT_PIX, KEY_EXTEND, KEY_NAXIS, KEY_END)
  //---------------------------------------------------------------------------
  private def loadSipArray(prefix: String, order: Int, a: Array[Array[String]], map: scala.collection.mutable.LinkedHashMap[String, String]): Unit = {
    for (p <- 0 to order;
         q <- 0 to order) {
      if ((p + q) <= order) map(prefix + "_" + p + "_" + q) = a(p)(q)
    }
  }
  //---------------------------------------------------------------------------
  private def getValueSeq(s: String) =
    s.split(":")(1)
      .replaceAll("\\[", "")
      .replaceAll("\\]", "")
      .trim.replaceAll(" +", " ")
      .split(" ")
  //---------------------------------------------------------------------------
  def galSim(wcsFileName: String, order: Int) = {
    //---------------------------------------------------------------------------
    val map = scala.collection.mutable.LinkedHashMap[String, String]()
    //---------------------------------------------------------------------------
    var item = 0
    for (line <- Source.fromFile(wcsFileName).getLines) {
      item match {
        case 0 =>
          val split = getValueSeq(line)
          map(KEY_CRPIX_1) = split(0).trim
          map(KEY_CRPIX_2) = split(1).trim

        case 1 =>
          val split = getValueSeq(line)
          map(KEY_CRVAL_1) = split(0).trim
          map(KEY_CRVAL_2) = split(1).trim

        case 2 =>
          val split = getValueSeq(line)
          map(KEY_CD_1_1) = split(0).trim
          map(KEY_CD_1_2) = split(1).trim
          map(KEY_CD_2_1) = split(2).trim
          map(KEY_CD_2_2) = split(3).trim

        case 3 =>
          val split = getValueSeq(line)
          map(KEY_A_ORDER) = order.toString
          loadSipArray("A", order, split.take(split.length / 2).grouped(order + 1).toArray, map)

          map(KEY_B_ORDER) = order.toString
          loadSipArray("B", order, split.drop(split.length / 2).grouped(order + 1).toArray, map)

        case 4 =>
          val split = getValueSeq(line)
          map(KEY_AP_ORDER) = order.toString
          loadSipArray("AP", order, split.take(split.length / 2).grouped(order + 1).toArray, map)

          map(KEY_BP_ORDER) = order.toString
          loadSipArray("BP", order, split.drop(split.length / 2).grouped(order + 1).toArray, map)

        case _ =>
      }
      item = item + 1
    }
    map.toArray
  }

  //---------------------------------------------------------------------------
  def astropy(wcsFileName: String) = {
    //---------------------------------------------------------------------------
    val map = scala.collection.mutable.LinkedHashMap[String, String]()
    //---------------------------------------------------------------------------
    var item = 0
    for (line <- Source.fromFile(wcsFileName).getLines.drop(3)) {
      item match {
        case 0 =>
          val split = line.split(":")(1).trim.split(" ")
          map(KEY_CTYPE1) = split(0).replaceAll("\"", "").trim
          map(KEY_CTYPE2) = split(2).replaceAll("\"", "").trim

        case 1 =>
          val split = line.split(":")(1).trim.split(" ")
          map(KEY_CRVAL_1) = split(0).trim
          map(KEY_CRVAL_2) = split(2).trim

        case 2 =>
          val split = line.split(":")(1).trim.split(" ")
          map(KEY_CRPIX_1) = split(0).trim
          map(KEY_CRPIX_2) = split(2).trim

        case 3 =>
          val split = line.split(":")(1).trim.split(" ")
          map(KEY_CD_1_1) = split(0).trim
          map(KEY_CD_1_2) = split(2).trim

        case 4 =>
          val split = line.split(":")(1).trim.split(" ")
          map(KEY_CD_2_1) = split(0).trim
          map(KEY_CD_2_2) = split(2).trim

        case 5 => //NAXIS

        case 6 =>
          val split = line.split(":")(1)
          map(KEY_A_ORDER) = split

        case 7 =>
          val seq = getValueSeq(line)
          val order = map(KEY_A_ORDER).toInt
          loadSipArray("A", order, seq.grouped(order + 1).toArray, map)

        case 8 =>
          val split = line.split(":")(1)
          map(KEY_B_ORDER) = split

        case 9 =>
          val seq = getValueSeq(line)
          val order = map(KEY_B_ORDER).toInt
          loadSipArray("B", order, seq.grouped(order + 1).toArray, map)

        case 10 =>
          val split = line.split(":")(1)
          map(KEY_AP_ORDER) = split

        case 11 =>
          val seq = getValueSeq(line)
          val order = map(KEY_AP_ORDER).toInt
          loadSipArray("AP", order, seq.grouped(order + 1).toArray, map)

        case 12 =>
          val split = line.split(":")(1)
          map(KEY_BP_ORDER) = split

        case 13 =>
          val seq = getValueSeq(line)
          val order = map(KEY_BP_ORDER).toInt
          loadSipArray("BP", order, seq.grouped(order + 1).toArray, map)

        case _ =>
      }
      item = item + 1
    }
    map.toArray
  }

  //---------------------------------------------------------------------------
  def astrometryNet(wcsFile: String) = {
    val map = scala.collection.mutable.LinkedHashMap[String, String]()
    for (line <- Source.fromFile(wcsFile).getLines) {
      line.grouped(HDU_RECORD_BYTE_SIZE).foreach { record =>
        if (!line.startsWith("END")) {
          val seq = record.split("=")
          if (seq.length == 2) {
            val key = seq(0).trim
            val value = seq(1).trim.split("/")(0).trim
            if (!WCS_GENERATED_WCS_FIT_FILE_IGNORED_KEY.contains(key)) map(key) = value
          }
        }
      }
    }
    map.toArray.toMap
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file WCS_FileLoader.scala
//=============================================================================