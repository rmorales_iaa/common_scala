/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  09/Jul/2022
 * Time:  12h:24m
 * Description: None
 */
//=============================================================================
package com.common.fits.wcs.fit.algorithm.astrometry_net_orig
//=============================================================================
import com.common.fits.simpleFits.SimpleFits.{KEY_CRPIX_1, KEY_CRPIX_2, KEY_IMAGEH, KEY_IMAGEW}
import com.common.fits.wcs.WCS
import com.common.fits.wcs.fit.algorithm.FitWcsAlgorithm
import com.common.geometry.point.Point2D_Double
import com.common.image.myImage.MyImage
import com.common.util.file.MyFile
import com.common.util.path.Path
//=============================================================================
import scala.language.postfixOps
import sys.process._
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object Astrometry_net_orig {
  //-------------------------------------------------------------------------
  val version = "a_net_orig v0.1"
  //-------------------------------------------------------------------------
  private final val sep = ","
  //-------------------------------------------------------------------------
  private final val fitWcs = "/home/rafa/proyecto/m2/input/astrometry.net/bin/fit-wcs"
  private final val csvToFitsTable = "/home/rafa/proyecto/m2/input/astrometry.net/bin/gecibat"
  private final val fitOutDir = "output/astrometry/"
  //-------------------------------------------------------------------------
}
//-------------------------------------------------------------------------
import Astrometry_net_orig._
case class Astrometry_net_orig(img: MyImage
                               , catRaDecSeq: Array[Point2D_Double]
                               , imagePixSeq: Array[Point2D_Double]
                               , sipOrder: Int) extends FitWcsAlgorithm {
  //------------------------------------------------------------------------
  def run(): Option[WCS] = {
    Path.ensureDirectoryExist(fitOutDir)
    val fits  = img.getSimpleFits()
    val fitBaseName = MyFile.getFileNameNoPathNoExtension(img.name)
    val fitCsvFile = Path.getCurrentPath() + fitOutDir + fitBaseName + ".csv"
    val fitBinTableFitsFile = Path.getCurrentPath() + fitOutDir + fitBaseName + "_binTable.fits"
    val fitWcsFile = Path.getCurrentPath() + fitOutDir + fitBaseName + ".wcs"
    val width = fits.getKeyIntValue(KEY_IMAGEW)
    val height = fits.getKeyIntValue(KEY_IMAGEH)
    val crpix_1 = fits.getKeyFloatValue(KEY_CRPIX_1)
    val crpix_2 = fits.getKeyFloatValue(KEY_CRPIX_2)

    //create a csv file with the coordinates
    val bw = new BufferedWriter(new FileWriter(new File(fitCsvFile)))
    bw.write(s"FIELD_X${sep}FIELD_Y${sep}INDEX_RA${sep}INDEX_DEC\n") //header required by  'fit-wcs'
    (imagePixSeq zip catRaDecSeq).foreach { case(pixPos,raDec) =>
      bw.write(s"${pixPos.x}$sep${pixPos.y}$sep${raDec.x}$sep${raDec.y}\n")
    }
    bw.close()

    //remove previous files if they exists
    MyFile.deleteFileIfExist(fitBinTableFitsFile)
    MyFile.deleteFileIfExist(fitWcsFile)

    //create the fits file from csv
    var command = s"$csvToFitsTable $fitCsvFile $fitBinTableFitsFile 1"
    s"$command" !!

    //fit the wcs with astrometry.net tool 'fit-wcs'
    command = s"$fitWcs -s $sipOrder -W $width -H $height -C -U $crpix_1 -V $crpix_2 -c $fitBinTableFitsFile -o $fitWcsFile"
    s"$command" !!

    //process the NEW wcs
    val newWcs = WCS(WCS_FileLoader.astrometryNet(fitWcsFile))

    //remove not used files
    MyFile.deleteFile(fitCsvFile)
    MyFile.deleteFile(fitBinTableFitsFile)
    MyFile.deleteFile(fitWcsFile)

    Some(newWcs)
  }
}
//=============================================================================
//=============================================================================
//End of file Astrometry_net_orig.scala
//=============================================================================