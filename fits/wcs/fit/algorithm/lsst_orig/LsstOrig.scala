/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  31/Aug/2022
 * Time:  13h:00m
 * Description: None
 */
//=============================================================================
package com.common.fits.wcs.fit.algorithm.lsst_orig
//=============================================================================
import com.common.configuration.MyConf
import com.common.fits.wcs.WCS
import com.common.fits.wcs.fit.algorithm.FitWcsAlgorithm
import com.common.fits.wcs.fit.algorithm.lsst.Lsst.getKeyValueMap
import com.common.geometry.point.Point2D_Double
import com.common.image.myImage.MyImage
import com.common.util.path.Path
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import sys.process._
//=============================================================================
//=============================================================================
object LsstOrig {
  //---------------------------------------------------------------------------
  val version = "lsst_orig v0.1"
  //---------------------------------------------------------------------------
  private val LSST_WCS_FIT_PROGRAM = MyConf.c.getString("Astrometry.fitWcs.lsst.fit_program")
  private val LSST_TEMP_DIR = "output/tmp/lsst/"
  //---------------------------------------------------------------------------
  private final val LSST_MATCH_Radius  = MyConf.c.getInt("Astrometry.fitWcs.lsst.matchRadiusMas")
  //---------------------------------------------------------------------------
  private val CSV_CAT_RA_COL    = "csv_cat_ra_col"
  private val CSV_CAT_DEC_COL   = "csv_cat_dec_col"
  private val CSV_IMG_PIX_X_COL = "csv_img_pix_x_col"
  private val CSV_IMG_PIX_Y_COL = "csv_img_pix_y_col"

  private val PIX_OFFSET = "-1"  //same value for x and y
  //---------------------------------------------------------------------------
}
//=============================================================================
import LsstOrig._
case class LsstOrig(img: MyImage
                    , catRaDecSeq: Array[Point2D_Double]
                    , imagePixSeq: Array[Point2D_Double]
                    , sipOrder: Int) extends FitWcsAlgorithm {
  //---------------------------------------------------------------------------
  def run(): Option[WCS] = {

    //process temp directory
    val oDir = Path.resetDirectory(Path.getCurrentPath() + LSST_TEMP_DIR + img.getRawName())

    //save image with the current wcs
    val newName = oDir + "image.fits"
    img.saveAsFitsRaw(newName)

    //save csv
    val csv_name = oDir + "cat_match.csv"
    saveCsv(csv_name)

    val command = s"$LSST_WCS_FIT_PROGRAM $newName $csv_name $LSST_MATCH_Radius $sipOrder $CSV_CAT_RA_COL $CSV_CAT_DEC_COL $CSV_IMG_PIX_X_COL $CSV_IMG_PIX_Y_COL $PIX_OFFSET"
    val r = command.!!
    val stringSeq = r.trim.split("\n")
    Path.deleteDirectory(oDir)
    if (stringSeq.length <= 15) None
    else Some(WCS(getKeyValueMap(img.getWcs(),stringSeq)))
  }
  //---------------------------------------------------------------------------
  private def saveCsv(csvName: String) {
    val bw = new BufferedWriter(new FileWriter(new File(csvName)))
    val sep = "\t"
    val headerSource =
      s"$CSV_CAT_RA_COL$sep" +
        s"$CSV_CAT_DEC_COL$sep" +
        s"$CSV_IMG_PIX_X_COL$sep" +
        s"$CSV_IMG_PIX_Y_COL$sep" +
        "\n"

    bw.write(headerSource)
    (catRaDecSeq zip imagePixSeq).foreach { case (catRaDec,imgPixPos) =>
      bw.write(
        catRaDec.x + sep +
          catRaDec.y + sep +
          imgPixPos.x + sep +
          imgPixPos.y + sep +
          "\n")
    }
    bw.close
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file LsstOrig.scala
//=============================================================================
