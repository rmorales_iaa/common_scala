/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Jan/2023
 * Time:  20h:54m
 * Description: None
 */
//=============================================================================
package com.common.fits.wcs.fit.algorithm.nonStandard.polynomy
//=============================================================================
import com.common.fits.wcs.WCS
import com.common.fits.wcs.fit.algorithm.FitWcsAlgorithm
import com.common.fits.wcs.nonStandard.NonStandardPolyWcs._
import com.common.fits.wcs.nonStandard.polynomial.{ParabolicPolyWcs}
import com.common.geometry.point.Point2D_Double
import com.common.image.myImage.MyImage
import com.common.math.regression.two_dimension.parabolicPoly.Parabolic_2D_fit
//=============================================================================
//=============================================================================
object ParabolicPoly {
  //---------------------------------------------------------------------------
  val version = "m2_para_pol v0.1"
  //---------------------------------------------------------------------------
}
//=============================================================================
import ParabolicPoly._
case class ParabolicPolyAlgorithm(img: MyImage
                                  , catRaDecSeq: Array[Point2D_Double]
                                  , imagePixSeq: Array[Point2D_Double]
                                  , sipOrder: Int
                                  , residualPolynomialOrder: Int) extends FitWcsAlgorithm {
  //---------------------------------------------------------------------------
  def run(): Option[WCS] = {

    //(ra,dec)
    val crval = img.getWcs().crval
    val intermediateRaDecSeq = catRaDecSeq map (p => p - crval)

    // pix
    val crpix = img.getWcs().crpix
    val intermediatePixSeq = imagePixSeq map (p => p - crpix)

    //first declination because ra residuals depend on cos(dec)
    val decPolyFit = Parabolic_2D_fit(
      intermediatePixSeq
      , intermediateRaDecSeq map (_.y)
      , sipOrder)
      .fit().getOrElse(return None)

    //right ascension (ra)
    val raPolyFit = Parabolic_2D_fit(
      intermediatePixSeq
      , intermediateRaDecSeq map (_.x)
      , sipOrder)
      .fit().getOrElse(return None)

    val estRaDecSeq = intermediatePixSeq map { p =>
      Point2D_Double(raPolyFit.estimateValue(p), decPolyFit.estimateValue(p))
    }

    val estRaDecErrorSeq = intermediateRaDecSeq.zipWithIndex map { case (p, i) =>
      estRaDecSeq(i) - p
    }

    calculateErrorModel(
      NON_STANDARD_GEN_POLY_WCS
      , raPolyFit
      , decPolyFit
      , estRaDecSeq
      , estRaDecErrorSeq
      , residualPolynomialOrder
    )

    //pix x
    val xPolyFit = Parabolic_2D_fit(
      intermediateRaDecSeq
      , intermediatePixSeq map (_.x)
      , sipOrder)
      .fit().getOrElse(return None)

    //pix y
    val yPolyFit = Parabolic_2D_fit(
      intermediateRaDecSeq
      , intermediatePixSeq map (_.y)
      , sipOrder)
      .fit().getOrElse(return None)

    val estPixSeq = intermediateRaDecSeq map (p => Point2D_Double(xPolyFit.estimateValue(p)
      , yPolyFit.estimateValue(p)))

    val estPixErrorSeq = intermediatePixSeq.zipWithIndex map { case (p, i) =>
      estPixSeq(i) - p
    }

    calculateErrorModel(
      NON_STANDARD_GEN_POLY_WCS
      , raPolyFit
      , decPolyFit
      , estPixSeq
      , estPixErrorSeq
      , residualPolynomialOrder
    )

    //build the non standard wcs
    val wcs = img.getWcs()

    wcs.setNonStandardWcs(
      Some(ParabolicPolyWcs(sipOrder
        , residualPolynomialOrder
        , raPolyFit
        , decPolyFit
        , xPolyFit
        , yPolyFit
        , crpix
        , crval))
    )
    Some(wcs)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ParabolicPolyAlgorithm.scala
//=============================================================================
