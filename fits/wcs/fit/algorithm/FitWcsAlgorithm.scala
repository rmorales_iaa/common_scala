/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  13/Jul/2022 
 * Time:  10h:10m
 * Description: None
 */
//=============================================================================
package com.common.fits.wcs.fit.algorithm
//=============================================================================
import com.common.configuration.MyConf
import com.common.fits.wcs.WCS
import com.common.fits.wcs.fit.algorithm.astrometry_net.Astrometry_net
import com.common.fits.wcs.fit.algorithm.astrometry_net_orig.Astrometry_net_orig
import com.common.fits.wcs.fit.algorithm.lsst.Lsst
import com.common.fits.wcs.fit.algorithm.lsst_orig.LsstOrig
import com.common.fits.wcs.fit.algorithm.nonStandard.polynomy.GeneralPolyAlgorithm
import com.common.fits.wcs.fit.algorithm.nonStandard.polynomy.ParabolicPolyAlgorithm
import com.common.fits.wcs.nonStandard.polynomial.GeneralPolyWcs
import com.common.geometry.point.Point2D_Double
import com.common.image.myImage.MyImage
import com.common.logger.MyLogger
//=============================================================================
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
object FitWcsAlgorithm extends MyLogger {
  //---------------------------------------------------------------------------
  private final val fitAlgorithmName  = MyConf.c.getString("Astrometry.fitWcs.algorithm")
  //---------------------------------------------------------------------------
  private final val ASTROMETRY_NET_M2_IMPLEMENTATION        = "m2_a_net"
  private final val ASTROMETRY_NET_ORIGINAL_IMPLEMENTATION  = "a_net_orig"
  private final val LSST_IMPLEMENTATION                     = "lsst"
  private final val LSST_ORIGINAL_IMPLEMENTATION            = "lsst_orig"
  private final val POLY_GENERAL_IMPLEMENTATION             = "poly_gen"
  private final val POLY_PARABOLIC_IMPLEMENTATION           = "poly_parab"
  //---------------------------------------------------------------------------
  private final val DEFAULT_IMPLEMENTATION                   = ASTROMETRY_NET_M2_IMPLEMENTATION
  //---------------------------------------------------------------------------
  private val residualPolynomialDegree = MyConf.c.getInt("Astrometry.fitWcs.polynomial.residualPolynomyDegree")
  //---------------------------------------------------------------------------
  def run(img: MyImage
          , catRaDecPosSeq: Array[Point2D_Double]
          , imagePixPosSeq: Array[Point2D_Double]
          , sipOrder: Int
          , algorithmName: String = fitAlgorithmName
          , preciseWCS_SIP: Boolean = true): (Option[WCS],Boolean) = {

    var r: Option[WCS] = None
    Try {
      r = algorithmName match {
        case ASTROMETRY_NET_M2_IMPLEMENTATION       => Astrometry_net(img, catRaDecPosSeq, imagePixPosSeq, sipOrder).run()
        case ASTROMETRY_NET_ORIGINAL_IMPLEMENTATION => Astrometry_net_orig(img, catRaDecPosSeq, imagePixPosSeq, sipOrder).run()
        case LSST_IMPLEMENTATION                    => Lsst(img, catRaDecPosSeq, imagePixPosSeq, sipOrder).run()
        case LSST_ORIGINAL_IMPLEMENTATION           => LsstOrig(img, catRaDecPosSeq, imagePixPosSeq, Math.max(2,sipOrder)).run() //lsst do not allow fitting with order 1
        case POLY_GENERAL_IMPLEMENTATION            => GeneralPolyAlgorithm(img, catRaDecPosSeq, imagePixPosSeq, sipOrder, residualPolynomialDegree).run()
        case POLY_PARABOLIC_IMPLEMENTATION          => ParabolicPolyAlgorithm(img, catRaDecPosSeq, imagePixPosSeq, sipOrder, residualPolynomialDegree).run()
        case _                                      =>
          error(s"Unknown WCS algorithm name '$algorithmName'")
          return (None,false)
      }
    }
    match {
      case Success(_) =>
        if (r.isEmpty) return (None,false)
        if (!r.get.hasPreciseSIP()) {
          if (preciseWCS_SIP) return (r,false)
        }
        (r,true)
      case Failure(_) => (None,false)
    }
  }
  //---------------------------------------------------------------------------
  def getVersion(algorithmName: String = fitAlgorithmName): String = {
    algorithmName match {
      case ASTROMETRY_NET_M2_IMPLEMENTATION        => Astrometry_net.version
      case ASTROMETRY_NET_ORIGINAL_IMPLEMENTATION  => Astrometry_net_orig.version
      case LSST_IMPLEMENTATION                     => Lsst.version
      case LSST_ORIGINAL_IMPLEMENTATION            => LsstOrig.version
      case POLY_GENERAL_IMPLEMENTATION             => GeneralPolyWcs.version
      case _                                       => getVersion(DEFAULT_IMPLEMENTATION)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
trait FitWcsAlgorithm extends MyLogger {
  //---------------------------------------------------------------------------
  val img: MyImage
  val catRaDecSeq: Array[Point2D_Double]
  val imagePixSeq: Array[Point2D_Double]
  val sipOrder: Int
  //---------------------------------------------------------------------------
  def run(): Option[WCS]
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FitWcsAlgorithm.scala
//=============================================================================
