
/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  09/Jul/2022
 * Time:  20h:26m
 * Description: None
 */
//=============================================================================
package com.common.fits.wcs.fit.algorithm.moolekamp
//=============================================================================
import com.common.fits.wcs.WCS
import com.common.geometry.point.Point2D_Double
import com.common.logger.MyLogger
import org.apache.commons.math3.analysis.{MultivariateMatrixFunction, MultivariateVectorFunction}
import org.apache.commons.math3.fitting.leastsquares.{LeastSquaresBuilder, LevenbergMarquardtOptimizer}
//=============================================================================
//=============================================================================
/* Transforming a polynomial transformation into SIP format string" (Moolekamp 2016)
  X_00 = c_11 * u + c_12 * v  //CRVAL1 without SIP
  Y_00 = c_21 * u + c_22 * v  //CRVAL2 without SIP
  f_x(u,v, X_00,X_02, X_03, X_04,X_11, X_12, X_13, X_20,X_21,X_22, X_30, X_31,X_40) :=X_00 + X_02*u0*v2 + X_03*u0*v3 + X_04*u0*v4 + X_11*u1*v1 + X_12*u1*v2 + X_13*u1*v3 + X_20*u2 + X_21*u2*v + X_22*u2*v2 + X_30*u3 + X_31*u3*v + X_40*u4;
*/
//=============================================================================
case class MoolekampFit(sourcePosSeq: Array[Point2D_Double]
                        , targetPosSeq: Array[Point2D_Double]
                        , calculateX: Boolean
                        , wcs: WCS
                        , maxEvaluations: Int = 1000
                        , maxIterations: Int = 100) extends MyLogger {
  //-------------------------------------------------------------------------
  //evaluate the function using the sample sequence with the new parameters
  def function: MultivariateVectorFunction = new MultivariateVectorFunction() {
    //-----------------------------------------------------------------------
    override def value(parameterSeq: Array[Double]): Array[Double] = {
      var k=0
      val X_00 = parameterSeq(k);k+=1
      val X_02 = parameterSeq(k);k+=1
      val X_03 = parameterSeq(k);k+=1
      val X_04 = parameterSeq(k);k+=1
      val X_11 = parameterSeq(k);k+=1
      val X_12 = parameterSeq(k);k+=1
      val X_13 = parameterSeq(k);k+=1
      val X_20 = parameterSeq(k);k+=1
      val X_21 = parameterSeq(k);k+=1
      val X_22 = parameterSeq(k);k+=1
      val X_30 = parameterSeq(k);k+=1
      val X_31 = parameterSeq(k);k+=1
      val X_40 = parameterSeq(k);k+=1

      val u0 = 1
      sourcePosSeq.map { p =>
        val u = p.x
        val v = p.y

        val u2 = u * u
        val u3 = u2 * u
        val u4 = u3 * u

        val v2 = v * v
        val v3 = v2 * v
        val v4 = v3 * v

        X_00+X_02*u0*v2+X_03*u0*v3+X_04*u0*v4+X_11*u*v+X_12*u*v2+X_13*u*v3+X_20*u2+X_21*u2*v+X_22*u2*v2+X_30*u3+X_31*u3*v+X_40*u4
      }
      //---------------------------------------------------------------------
    }
    //-----------------------------------------------------------------------
  }
  //-------------------------------------------------------------------------
  //evaluate the jacobian of the function using the sample sequence and the new parameters
  def jacobian: MultivariateMatrixFunction = new MultivariateMatrixFunction() {
    //-----------------------------------------------------------------------
    override def value(parameterSeq: Array[Double]): Array[Array[Double]] = {
      sourcePosSeq.map { p =>
        val u = p.x
        val v = p.y

        val u2 = u * u
        val u3 = u2 * u
        val u4 = u3 * u

        val v2 = v * v
        val v3 = v2 * v
        val v4 = v3 * v

        Array(
            1        //df(i,v)/dX_00   0
          , v2       //df(i,v)/dX_02   1
          , v3       //df(i,v)/dX_03   2
          , v4       //df(i,v)/dX_04   3
          , u * v    //df(i,v)/dX_11   4
          , u * v2   //df(i,v)/dX_12   5
          , u * v3   //df(i,v)/dX_13   6
          , u2       //df(i,v)/dX_20   7
          , u2 * v   //df(i,v)/dX_21   8
          , u2 * v2  //df(i,v)/dX_22   9
          , u3       //df(i,v)/dX_30   10
          , u3 * v   //df(i,v)/dX_31   11
          , u4       //df(i,v)/dX_40   12
        )
      }
    }
    //-----------------------------------------------------------------------
  }

  //-------------------------------------------------------------------------
  def getInitialParameterGuess() =
    Array(if (calculateX) wcs.crval.x else wcs.crval.y) ++
      Moolekamp.XiYiSeq(wcs,calculateX)
  //-------------------------------------------------------------------------
  def getTarget() = {
    if (calculateX) targetPosSeq map (_.x)
    else targetPosSeq map (_.y)
  }
  //-------------------------------------------------------------------------
  def fit() = {
    try {
      val lsb = new LeastSquaresBuilder()
        .model(function, jacobian)
        .target(getTarget())
        .start(getInitialParameterGuess())
        .maxEvaluations(maxEvaluations) //set upper limit of evaluation time
        .maxIterations(maxIterations) //set upper limit of iteration time
      val optimizer = new LevenbergMarquardtOptimizer().optimize(lsb.build)
      val r = optimizer.getPoint.toArray
      r foreach { v => if (v.isNaN) None }
      Some(r)
    }
    catch {
      case e: Exception => println(e.toString)
        None
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MoolekampFit.scala
//=============================================================================