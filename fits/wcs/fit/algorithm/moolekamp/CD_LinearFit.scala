
/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  09/Jul/2022
 * Time:  20h:26m
 * Description: None
 */
//=============================================================================
package com.common.fits.wcs.fit.algorithm.moolekamp
//=============================================================================
import com.common.fits.wcs.WCS
import com.common.geometry.point.Point2D_Double
import com.common.logger.MyLogger
import org.apache.commons.math3.analysis.{MultivariateMatrixFunction, MultivariateVectorFunction}
import org.apache.commons.math3.fitting.leastsquares.{LeastSquaresBuilder, LevenbergMarquardtOptimizer}
//=============================================================================
// Linear fit of CD matrix. SIP contribution ignored
/*
f_x(c_11,c_12):=c_11 * u + c_12 * v;
*/
//=============================================================================
case class CD_LinearFit(sourcePosSeq: Array[Point2D_Double]
                        , targetPosSeq: Array[Point2D_Double]
                        , calculateX: Boolean
                        , wcs: WCS
                        , maxEvaluations: Int = 1000
                        , maxIterations: Int = 100) extends MyLogger {
  //-------------------------------------------------------------------------
  //evaluate the function using the sample sequence with the new parameters
  def function: MultivariateVectorFunction = new MultivariateVectorFunction() {
    //-----------------------------------------------------------------------
    override def value(parameterSeq: Array[Double]): Array[Double] = {
      var k = 0
      val c_11 = parameterSeq(k);k += 1
      val c_12 = parameterSeq(k);k += 1

      sourcePosSeq.map { p =>
        val u = p.x
        val v = p.y
        c_11 * u + c_12 * v
      }
      //---------------------------------------------------------------------
    }
    //-----------------------------------------------------------------------
  }
  //-------------------------------------------------------------------------
  //evaluate the jacobian of the function using the sample sequence and the new parameters
  def jacobian: MultivariateMatrixFunction = new MultivariateMatrixFunction() {
    //-----------------------------------------------------------------------
    override def value(parameterSeq: Array[Double]): Array[Array[Double]] = {
      sourcePosSeq.map { p =>
        val u = p.x
        val v = p.y

        Array(
            u //df_x/dc_11   0
          , v //df_x/dc_12   1
        )
      }
    }
    //-----------------------------------------------------------------------
  }

  //-------------------------------------------------------------------------
  def getInitialParameterGuess() = {
    val seq = wcs.getCD_AsArray()
    if (calculateX) seq.take(2) else seq.drop(2)
  }
  //-------------------------------------------------------------------------
  def getTarget() = {
    if (calculateX) targetPosSeq map (_.x)
    else targetPosSeq map (_.y)
  }
  //-------------------------------------------------------------------------
  def fit() = {
    try {
      val lsb = new LeastSquaresBuilder()
        .model(function, jacobian)
        .target(getTarget())
        .start(getInitialParameterGuess())
        .maxEvaluations(maxEvaluations) //set upper limit of evaluation time
        .maxIterations(maxIterations) //set upper limit of iteration time
      val optimizer = new LevenbergMarquardtOptimizer().optimize(lsb.build)
      val r = optimizer.getPoint.toArray
      r foreach { v => if (v.isNaN) None }
      Some(r)
    }
    catch {
      case e: Exception => println(e.toString)
        None
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CD_LinearFit.scala
//=============================================================================