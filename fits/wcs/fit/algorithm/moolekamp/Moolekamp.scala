//=============================================================================
package com.common.fits.wcs.fit.algorithm.moolekamp
//=============================================================================
import com.common.fits.wcs.{SIP, WCS}
import com.common.fits.wcs.fit.algorithm.FitWcsAlgorithm
import com.common.geometry.point.Point2D_Double
import com.common.image.myImage.MyImage
import com.common.logger.MyLogger
//=============================================================================
//=============================================================================
object Moolekamp extends MyLogger {
  //---------------------------------------------------------------------------
  val version = "m2-moole v0.1"
  //---------------------------------------------------------------------------
  def XiYiSeq(wcs: WCS, calculateX: Boolean) = {
    val aSeq = wcs.sip.getTriangularCoeffSeq(isA_Seq = true)
    val bSeq = wcs.sip.getTriangularCoeffSeq(isA_Seq = false)
    val abSeq = aSeq zip bSeq
    if (calculateX)
      abSeq map { case (a, b) => (wcs.cd_11 * a) + (wcs.cd_12 * b) } //X_ij
    else
      abSeq map { case (a, b) => (wcs.cd_21 * a) + (wcs.cd_22 * b) } //Y_ij
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Moolekamp(img: MyImage
                     , catRaDecSeq: Array[Point2D_Double]
                     , imagePixSeq: Array[Point2D_Double]
                     , sipOrder: Int) extends FitWcsAlgorithm {
  //---------------------------------------------------------------------------
  private val wcs = img.getWcs()
  //---------------------------------------------------------------------------
  def run(): Option[WCS] = {

    val intermediatePixSeq   =  imagePixSeq map ( _ -  wcs.crpix)   // (u,v) in the notation: pix coordinates
    val intermediateRaDecSeq =  catRaDecSeq map ( _ -  wcs.crval) // (x,y) in the notation: intermediate world coordinates

    //CD matrix fit without distortion (SIP)
    val
    cd1_seq = CD_LinearFit(
      intermediatePixSeq
      , intermediateRaDecSeq
      , calculateX = true
      , wcs).fit()
      .getOrElse(return None)

    val cd2_seq = CD_LinearFit(
      intermediatePixSeq
      , intermediateRaDecSeq
      , calculateX = false
      , wcs).fit()
      .getOrElse(return None)

    val newCD_Matrix = cd1_seq ++ cd2_seq

    //calculate fit in x axis
    val updatedWcs = WCS(wcs,newCD_Matrix)
    val xFitSeq = MoolekampFit(
      intermediatePixSeq
      , intermediateRaDecSeq
      , calculateX = true
      , updatedWcs).fit()
      .getOrElse(return None)

    //calculate fit in y axis
    val yFitSeq = MoolekampFit(
      intermediatePixSeq
      , intermediateRaDecSeq
      , calculateX = false
      , updatedWcs).fit()
      .getOrElse(return None)

    val cd_11 = cd1_seq.head
    val cd_12 = cd1_seq.last
    val cd_21 = cd2_seq.head
    val cd_22 = cd2_seq.last

    //build SIP coefficients
    val xySeq = (xFitSeq zip yFitSeq).drop(1) //avoid X_00 and Y_00. (X_00 + Y_00) + wcs.crval = calculated CRVAL
    val aSeq = xySeq map { case (x, y) =>  updatedWcs.inverseDet * ((cd_22 * x) - (cd_12 * y)) }
    val bSeq = xySeq map { case (x, y) =>  updatedWcs.inverseDet * ((cd_11 * y) - (cd_21 * x)) }

    val newWCS = WCS(updatedWcs
                     , SIP(updatedWcs.sip.buidTriangularCoeffSeq(aSeq)
                           , updatedWcs.sip.buidTriangularCoeffSeq(bSeq)
                           , updatedWcs.sip.aOrder))


    Some(WCS(newWCS
           , newWCS.getCD_AsArray()
           , newWCS.crpix
           , newWCS.pixToSky(newWCS.crpix) //crval
           , newWCS.sip))

  }
  //---------------------------------------------------------------------------
}
//=============================================================================

//=============================================================================
//End of file Moolekamp.scala