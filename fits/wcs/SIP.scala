/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  25/May/2021
 * Time:  12h:05m
 * Description: Simle Imaging Polynomial
 * Shupe et al. 2005 "The SIP convention for representing distorsion in FITS image headers"
 */
//=============================================================================
package com.common.fits.wcs
//=============================================================================
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.simpleFits.SimpleFits._
import com.common.geometry.point.Point2D_Double
import com.common.util.string.MyString
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object SIP {
  //---------------------------------------------------------------------------
  final val MAX_SIP_ORDER = 10
  //---------------------------------------------------------------------------
  def apply(aSeq: Array[Double]
            , bSeq: Array[Double]
            , apSeq: Array[Double]
            , bpSeq: Array[Double]
            , aOrder: Int
            , apOrder: Int) : SIP =
    SIP(aOrder, aSeq, aOrder, bSeq, apOrder, apSeq, apOrder, bpSeq)
  //---------------------------------------------------------------------------
  def apply(aSeq: Array[Double], bSeq: Array[Double], sipOrder: Int) : SIP =
    SIP(sipOrder
      , aSeq
      , sipOrder
      , bSeq
      , sipOrder + 1
      , Array.fill((sipOrder + 2) * (sipOrder + 2)) (0d)
      , sipOrder + 1
      , Array.fill((sipOrder + 2) * (sipOrder + 2)) (0d))
  //---------------------------------------------------------------------------
  def apply(sipOrder: Int) : SIP =
    apply(Array.fill(getCoefCount(sipOrder * sipOrder)) (0d)
         , Array.fill(getCoefCount(sipOrder * sipOrder)) (0d)
         , sipOrder)
  //---------------------------------------------------------------------------
  def apply(fits: SimpleFits) : SIP = {
    val aOrder = fits.getKeyIntValueOrDefault(KEY_A_ORDER, 0)
    val bOrder = fits.getKeyIntValueOrDefault(KEY_B_ORDER, 0)

    val apOrder = fits.getKeyIntValueOrDefault(KEY_AP_ORDER, 0)
    val bpOrder = fits.getKeyIntValueOrDefault(KEY_BP_ORDER, 0)

    val aSeqCoef = getCoefficientNameSeq("A",aOrder).map(key=> fits.getKeyDoubleValueOrDefault(key, 0))
    val bSeqCoef = getCoefficientNameSeq("B",bOrder).map(key=> fits.getKeyDoubleValueOrDefault(key, 0))
    val apSeqCoef = getCoefficientNameSeq("AP",apOrder).map(key=> fits.getKeyDoubleValueOrDefault(key, 0))
    val bpSeqCoef = getCoefficientNameSeq("BP",bpOrder).map(key=> fits.getKeyDoubleValueOrDefault(key, 0))

    SIP(aOrder, aSeqCoef, bOrder, bSeqCoef, apOrder, apSeqCoef, bpOrder, bpSeqCoef)
  }
  //---------------------------------------------------------------------------
  def apply(map:Map[String,String]) : SIP = {
    val aOrder = map(KEY_A_ORDER).toInt
    val bOrder = map(KEY_B_ORDER).toInt
    val apOrder = map(KEY_AP_ORDER).toInt
    val bpOrder = map(KEY_BP_ORDER).toInt
    //-------------------------------------------------------------------------
    def getValueFromMap(key:String) = if (map.contains(key))  map(key).toDouble else 0d
    //-------------------------------------------------------------------------
    val aSeqCoef = getCoefficientNameSeq("A",aOrder).map(getValueFromMap(_))
    val bSeqCoef = getCoefficientNameSeq("B",bOrder).map(getValueFromMap(_))
    val apSeqCoef = getCoefficientNameSeq("AP",apOrder).map(getValueFromMap(_))
    val bpSeqCoef = getCoefficientNameSeq("BP",bpOrder).map(getValueFromMap(_))

    SIP(aOrder, aSeqCoef, bOrder, bSeqCoef, apOrder, apSeqCoef, bpOrder, bpSeqCoef)
  }

  //---------------------------------------------------------------------------
  def getElmentCountTriangularMatrix(sipOrder: Int) = (sipOrder + 1) * (sipOrder + 2) / 2
  //---------------------------------------------------------------------------
  def getCoefCount(order: Int)= ((order + 1) * (order + 1 + 1)) / 2
  //---------------------------------------------------------------------------
  private def getCoefficientNameSeq(prefix: String, order: Int) = {
    (for(p<-0 to order;
         q<-0 to order)  yield {
      if ((p + q) <= order) Some(prefix + "_" + p + "_"+q)
      else None
    }).flatten.toArray
  }
  //---------------------------------------------------------------------------
  def getCoefficientSeq(seq: ArrayBuffer[ArrayBuffer[Double]], order: Int) = {
    (for(p<-0 to order;
         q<-0 to order)  yield {
      if ((p + q) <= order) Some(seq(p)(q))
      else None
    }).flatten.toArray
  }

  //---------------------------------------------------------------------------
  def getCoefficientIndexSeq(order: Int) =
    (for (i <- 0 to order;
          j <- 0 to order) yield
      if ((i + j) <= order) Some(Seq(i, j))
      else None).toArray.flatten
  //---------------------------------------------------------------------------
  //for an order 4, expecting v formed as:
  // A_02,A_03,A_04,A_11,A_12,A_13,A_20,A_21,A_22,A_30,A_31,A_40
  def buildCoeffSeq(v: Array[Double], order: Int) = {
    var index = 0
    (for (p <- 0 to order;
          q <- 0 to order) yield {
      val i = p + q
      if (i <= order) {
        val r =
          if (i > 1) {
            val k = v(index)
            index += 1
            k
          }
          else 0D
        Some(r)
      }
      else None
    }).flatten.toArray
  }
  //---------------------------------------------------------------------------
  private def eval(orderA: Int
                   , cofSeqA:Array[Double]
                   , orderB: Int
                   , cofSeqB:Array[Double]
                   , u: Double
                   , v: Double) = {
    //pre-calculate the power
    val maxOrder = Math.max(orderA, orderB)
    val powerU = ArrayBuffer.fill(MAX_SIP_ORDER+2)(0d)
    val powerV = ArrayBuffer.fill(MAX_SIP_ORDER+2)(0d)
    powerU(0) = 1d
    powerU(1) = u
    powerV(0) = 1d
    powerV(1) = v
    for(i <-2 to maxOrder){
      powerU(i) = powerU(i-1) * u
      powerV(i) = powerV(i-1) * v
    }

    //calculatate the distorsion
    var i = 0
    var fuv = 0d
    for(p<-0 to orderA;
        q<-0 to orderA) {
      if ((p + q) <= orderA) {
        fuv += cofSeqA(i) * powerU(p) * powerV(q)
        i += 1
      }
    }

    i=0
    var guv = 0d
    for(p<-0 to orderB;
        q<-0 to orderB) {
      if ((p + q) <= orderB) {
        guv += cofSeqB(i) * powerU(p) * powerV(q)
        i += 1
      }
    }
    Point2D_Double(fuv, guv)
  }
  //---------------------------------------------------------------------------
  def toStringSeq(prefix:String, order: Int,cofSeq: Array[Double]) = {
    if (order == 0) Array("")
    else {
      var i = 0
      Array(MyString.rightPadding(prefix + "_ORDER",8) + "= " + order) ++:
        (for(p<-0 to order;
             q<-0 to order) yield {
          if ((p + q) <= order) {
            i += 1
            Some(MyString.rightPadding(prefix + "_" + p + "_" + q, 8) + "= " + cofSeq(i-1))  //FITS record format string
          }
          else None
        }).flatten.toArray
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class SIP(aOrder: Int
               , aSeq: Array[Double]
               , bOrder: Int
               , bSeq: Array[Double]
               , apOrder: Int
               , apSeq: Array[Double]
               , bpOrder: Int
               , bpSeq: Array[Double]) {
  //---------------------------------------------------------------------------
  def evalForward(x: Double, y: Double) =
    SIP.eval(aOrder, aSeq, bOrder, bSeq, x, y)
  //---------------------------------------------------------------------------
  def evalInverse(ra: Double, dec: Double) =
    SIP.eval(apOrder, apSeq, bpOrder, bpSeq, ra, dec)
  //---------------------------------------------------------------------------
  def evalForward(p: Point2D_Double) =
    SIP.eval(aOrder, aSeq, bOrder, bSeq, p.x, p.y)
  //---------------------------------------------------------------------------
  def evalInverse(raDec: Point2D_Double) =
    SIP.eval(apOrder, apSeq, bpOrder, bpSeq, raDec.x, raDec.y)
  //---------------------------------------------------------------------------
  def toPairSeq() = {
    if(aOrder == 0) Array[(String,String)]()
    else
      toStringSeq map { s =>
        val seq = s.split("=")
        (seq(0).trim,seq(1).trim)
      }
  }
  //---------------------------------------------------------------------------
  def toStringSeq() =
    SIP.toStringSeq("A", aOrder, aSeq) ++
    SIP.toStringSeq("B", bOrder, bSeq) ++
    SIP.toStringSeq("AP", apOrder, apSeq) ++
    SIP.toStringSeq("BP", bpOrder, bpSeq)
  //---------------------------------------------------------------------------
  def toSeq() = toSeqForwardParameterSeq ++ toSeqReverseParameterSeq
  //---------------------------------------------------------------------------
  def toSeqForwardParameterSeq() = aSeq ++ bSeq
  //---------------------------------------------------------------------------
  def toSeqReverseParameterSeq() = apSeq ++ bpSeq
  //---------------------------------------------------------------------------
  //for an order 4:
  //A_02,A_03,A_04,A_11,A_12,A_13,A_20,A_21,A_22,A_30,A_31,A_40
  def getTriangularCoeffSeq(isA_Seq: Boolean = true): Array[Double] = {
    var index = 0
    (for (p <- 0 to aOrder;
          q <- 0 to aOrder) yield {
      val i = p + q
      if (i <= aOrder) {
        val r =
          if (i > 1) {
            if (isA_Seq) Some(aSeq(index)) else Some(bSeq(index))
          }
          else None
        index += 1
        r
      }
      else None
    }
    ).flatten.toArray
  }

  //---------------------------------------------------------------------------
  //avoid the linear matrix coef:  p + q < 2
  def buidTriangularCoeffSeq(valueSeq: Array[Double]
                             , isA_Seq: Boolean = true) = {
    val order = if (isA_Seq) aOrder else bOrder
    val r = ArrayBuffer.fill[Double](SIP.getCoefCount(order))(0)
    var index = 0
    var valueIndex = 0
    for (p <- 0 to order;
         q <- 0 to order) {
      val i = p + q
      if (p+q <= order) {
        if (i >= 2) {
          r(index) = valueSeq(valueIndex)
          valueIndex += 1
        }
        index += 1
      }
    }
    r.toArray
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SIP.scala
//=============================================================================