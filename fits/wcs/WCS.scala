/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/May/2021
 * Time:  00h:18m
 * Description:
 * Based on:
 *  E.W. Greisen and M.R. Calabretta. "Representation od world coordinates in FITS". 2002
 *  J. E. Postma and D. Leahy. "An algorithm for coordinate matching in world coordinate solutions". 2020
 *  D. L. Shupe  et al. "The SIP convention for representing distorsion in FITS image headers". 2005
 */
//=============================================================================
package com.common.fits.wcs
//=============================================================================
//=============================================================================
import com.common.constant.astronomy.AstronomicalUnit.PRECISE_DEGREES_TO_RADIANS
import com.common.coordinate.conversion.Conversion.{toPreciseDegrees, toPreciseRadians}
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.simpleFits.SimpleFits._
import com.common.fits.wcs.nonStandard.NonStandardPolyWcs
import com.common.geometry.point.Point2D_Double
import com.common.image.myImage.MyImage
import com.common.logger.MyLogger
import com.common.math.MyMath
//=============================================================================
import sys.process._
import Math._
//=============================================================================
object WCS extends MyLogger {
  //---------------------------------------------------------------------------
  def apply(fits: SimpleFits): WCS = {
    val wcs = WCS(
        fits.getKeyIntValueOrDefault(KEY_WCSAXES, fits.getKeyIntValue(KEY_NAXIS))
      , fits.getKeyIntValueOrDefault(KEY_IMAGEW, fits.getKeyIntValue(KEY_NAXIS_1))
      , fits.getKeyIntValueOrDefault(KEY_IMAGEH, fits.getKeyIntValue(KEY_NAXIS_2))
      , "'" + fits.getStringValueOrDefault(KEY_CTYPE1, "RA--RAN-SIP").replaceAll("'","") + "'"
      , "'" + fits.getStringValueOrDefault(KEY_CTYPE2, "DEC--TAN-SIP").replaceAll("'","") + "'"
      , "'" + fits.getStringValueOrDefault(KEY_CUNIT_1, "deg").replaceAll("'","") + "'"
      , "'" + fits.getStringValueOrDefault(KEY_CUNIT_2, "deg").replaceAll("'","") + "'"
      , fits.getStringValueOrDefault(KEY_LONPOLE, "180").replaceAll("'","").trim.toDouble
      , fits.getStringValueOrDefault(KEY_LATPOLE, "0").replaceAll("'","").trim.toDouble
      , fits.getKeyDoubleValueOrDefault(KEY_CD_1_1, 1)
      , fits.getKeyDoubleValueOrDefault(KEY_CD_1_2, 0)
      , fits.getKeyDoubleValueOrDefault(KEY_CD_2_1, 1)
      , fits.getKeyDoubleValueOrDefault(KEY_CD_2_2, 0)
      , fits.getKeyDoubleValueOrDefault(KEY_CRPIX_1, 1)
      , fits.getKeyDoubleValueOrDefault(KEY_CRPIX_2, 0)
      , fits.getKeyDoubleValueOrDefault(KEY_CRVAL_1, 1)
      , fits.getKeyDoubleValueOrDefault(KEY_CRVAL_2, 0)
      , fits.getKeyDoubleValueOrDefault(KEY_EQUINOX, 2000)
      , SIP(fits)
    )
    wcs.setNonStandardWcs(NonStandardPolyWcs.build(fits, wcs))
    wcs
  }
  //---------------------------------------------------------------------------
  def apply(map: Map[String,String]) : WCS =
    WCS(
      map(KEY_WCSAXES).toInt
      , map(KEY_IMAGEW).toInt
      , map(KEY_IMAGEH).toInt
      , map(KEY_CTYPE1)
      , map(KEY_CTYPE2)
      , map(KEY_CUNIT_1)
      , map(KEY_CUNIT_2)
      , map(KEY_LONPOLE).toDouble
      , map(KEY_LATPOLE).toDouble
      , map(KEY_CD_1_1).toDouble
      , map(KEY_CD_1_2).toDouble
      , map(KEY_CD_2_1).toDouble
      , map(KEY_CD_2_2).toDouble
      , map(KEY_CRPIX_1).toDouble
      , map(KEY_CRPIX_2).toDouble
      , map(KEY_CRVAL_1).toDouble
      , map(KEY_CRVAL_2).toDouble
      , map(KEY_EQUINOX).toDouble
      , SIP(map)
    )
  //---------------------------------------------------------------------------
  def apply(wcs: WCS, crpix: Point2D_Double): WCS = apply(wcs, wcs.getCD_AsArray(), crpix, wcs.crval)
  //---------------------------------------------------------------------------
  def apply(wcs: WCS, cd: Array[Double]): WCS = apply(wcs, cd, wcs.crpix, wcs.crval)
  //---------------------------------------------------------------------------
  def apply(wcs: WCS, cd: Array[Double], sip: SIP): WCS =
    WCS(wcs.axes
      , wcs.imageW
      , wcs.imageH
      , wcs.ctype_1
      , wcs.ctype_2
      , wcs.cUnit_1
      , wcs.cUnit_2
      , wcs.lonPole
      , wcs.latPole
      , cd(0)
      , cd(1)
      , cd(2)
      , cd(3)
      , wcs.crpix.x
      , wcs.crpix.y
      , wcs.crval.x
      , wcs.crval.y
      , wcs.equinox
      , sip)
  //---------------------------------------------------------------------------
  def apply(wcs: WCS
            , cd: Array[Double]
            , crpix: Point2D_Double
            , crval: Point2D_Double): WCS =
    WCS(wcs.axes
      , wcs.imageW
      , wcs.imageH
      , wcs.ctype_1
      , wcs.ctype_2
      , wcs.cUnit_1
      , wcs.cUnit_2
      , wcs.lonPole
      , wcs.latPole
      , cd(0)
      , cd(1)
      , cd(2)
      , cd(3)
      , crpix.x
      , crpix.y
      , crval.x
      , crval.y
      , wcs.equinox
      , wcs.sip)
  //---------------------------------------------------------------------------
  def apply(wcs: WCS
            , cd: Array[Double]
            , crpix: Point2D_Double
            , crval: Point2D_Double
            , sip: SIP): WCS =
    WCS(wcs.axes
      , wcs.imageW
      , wcs.imageH
      , wcs.ctype_1
      , wcs.ctype_2
      , wcs.cUnit_1
      , wcs.cUnit_2
      , wcs.lonPole
      , wcs.latPole
      , cd(0)
      , cd(1)
      , cd(2)
      , cd(3)
      , crpix.x
      , crpix.y
      , crval.x
      , crval.y
      , wcs.equinox
      , sip)

  //---------------------------------------------------------------------------
  def apply(wcs: WCS, sip: SIP): WCS =
    apply(wcs
      , wcs.getCD_AsArray()
      , wcs.crpix
      , wcs.crval
      , sip)
  //---------------------------------------------------------------------------
  def updateWcs(fits: SimpleFits, newWcs: Seq[(String,String)], removeWcs: Boolean = true) = {
    if (removeWcs) fits.removeWCS()
    val fitsMap = fits.getKeyValueMap
    newWcs.foreach { case(key,value) => fitsMap(key) = value }
  }
  //---------------------------------------------------------------------------
  def getImageWcsFitInfo(img: MyImage): String = {
    val fits = img.getSimpleFits
    if (!fits.existKey(KEY_M2_WCS_FIT)) "'None'"
    else fits.getStringValueOrEmptyNoQuotation(KEY_M2_WCS_FIT)
  }
  //---------------------------------------------------------------------------
  //see: 'sphs2x' of wcsliB
  private def specialCaseRotations(eulerAngles: Array[Double]
                                   , phi: Double
                                   , theta: Double
                                   , nphi: Int  = 1
                                   , _ntheta: Int = 0
                                   , spt: Double = 1d
                                   , sll: Double = 2d
                                  ) : Option[Point2D_Double] = {
    var lng = 0d
    var ntheta = _ntheta
    var mphi = 0
    var mtheta = 0

    if (ntheta > 0) {
      mphi = nphi
      mtheta = ntheta
    }
    else {
      mphi = 1
      mtheta = 1
      ntheta = nphi
    }

    var lngp = lng
    var latp = 0d
    var jphi = 0
    var thetap = theta

    if (eulerAngles(4) == 0.0) {
      if (eulerAngles(1) == 0.0) {
        // Simple change in origin of longitude.
        val dlng = MyMath.fmod(eulerAngles(0) + 180.0 - eulerAngles(2), 360.0)
        for (_ <- 0 until ntheta) {
          var phip = phi + (jphi % nphi) * spt
          for (_ <- 0 until mphi) {
            lngp = phip + dlng
            latp = thetap
            lng = MyMath.normalizeLongitude(eulerAngles(0), lng)

            lngp += sll
            latp += sll

            phip += spt
            jphi += 1
          }
          thetap += spt
        }
      }
      else { // Pole-flip with change in origin of longitude.
        val dlng = MyMath.fmod(eulerAngles(0) + eulerAngles(2), 360.0)
        for (_ <- 0 until ntheta) {
          var phip = phi + (jphi % nphi) * spt
          for (_ <- 0 until mphi) {

            lngp = dlng - phip
            latp = -thetap
            lng = MyMath.normalizeLongitude(eulerAngles(0), lng)

            lngp += sll
            latp += sll

            phip += spt
            jphi += 1
          }
          thetap += spt
        }
      }
      Some(Point2D_Double(lngp,latp))
    }
    None
  }
  //---------------------------------------------------------------------------
  //see: 'sphs2x' of wcsliB
  private def specialCaseRotations2(eulerAngles: Array[Double]
                                    , _lng: Double
                                    , lat: Double
                                    , nlng: Int  = 1
                                    , _nlat: Int = 1
                                    , sll: Double = 2d
                                    , spt: Double = 1d
                                   ) : Option[Point2D_Double] = {
    var mlng = 0
    var mlat = 0
    var nlat = _nlat

    if (nlat > 0) {
      mlng = nlng
      mlat = nlat
    }
    else {
      mlng = 1
      mlat = 1
      nlat = nlng
    }

    var latp  = lat
    val lng    = _lng
    var phip   = 0d
    var thetap = 0d
    if (eulerAngles(4) == 0.0) {
      if (eulerAngles(1) == 0.0) {
        // Simple change in origin of longitude.
        val dphi = MyMath.fmod(eulerAngles(2) - 180.0 - eulerAngles(0), 360.0)
        var jlng   = 0
        var lngp   = lng
        for (_ <- 0 until nlat) {
          lngp = lng + (jlng % nlng)*sll
          for (_ <- 0 until mlng) {
            phip = MyMath.fmod(lngp + dphi, 360.0)
            thetap = latp
            phip = MyMath.normalizeLongitude2(phip)

            phip   += spt
            thetap += spt
            lngp   += sll
            jlng   += 1
          }
          latp += sll
        }
      }
      else { // Pole-flip with change in origin of longitude.
        val dphi = MyMath.fmod(eulerAngles(2) + eulerAngles(0), 360.0)
        var jlng   = 0
        for (_ <- 0 until nlat) {
          var lngp = lng + (jlng%nlng)*sll
          for (_ <- 0 until mlng) {
            phip = MyMath.fmod(dphi - lngp, 360.0)
            thetap = -latp

            phip = MyMath.normalizeLongitude2(phip)

            lngp += sll
            jlng   +=1
            phip   += spt
            thetap += spt
          }
          latp += sll
        }
      }
      Some(Point2D_Double(phip,thetap))
    }
    None
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import WCS._
case class WCS(axes: Int
               , imageW: Int
               , imageH: Int
               , ctype_1: String
               , ctype_2: String
               , cUnit_1: String
               , cUnit_2: String
               , lonPole: Double
               , latPole: Double
               , cd_11: Double
               , cd_12: Double
               , cd_21: Double
               , cd_22: Double
               , crpixX: Double
               , crpixY: Double
               , crvalRa: Double
               , crvalDec: Double
               , equinox: Double
               , sip: SIP) extends MyLogger {
  //--------------------------------------------------------------------------
  val crpix = Point2D_Double(crpixX, crpixY)
  val crval = Point2D_Double(crvalRa, crvalDec)

  private val crpix_x_rad = toPreciseRadians(crpixX)
  private val crpix_y_rad = toPreciseRadians(crpixY)

  private val crval_ra_rad = toPreciseRadians(crvalRa)
  private val crval_dec_rad = toPreciseRadians(crvalDec)

  private val sin_crval_ra = sin(crval_dec_rad)
  private val cos_crval_dec = cos(crval_dec_rad)

  val det = cd_11 * cd_22 - cd_12 * cd_21  //determinant
  val inverseDet = 1 / det //inverse of the determinant
  val icd_11 =  inverseDet * cd_22  //inverse
  val icd_12 = -inverseDet * cd_12  //inverse
  val icd_21 = -inverseDet * cd_21  //inverse
  val icd_22 =  inverseDet * cd_11  //inverse

  private val eulerAngles = getEulerAngles
  //--------------------------------------------------------------------------
  private var nonStandardWCS: Option[NonStandardPolyWcs] = None
  //--------------------------------------------------------------------------
  //see definition in "wcs.h" file from WCSLIB
  val (coorflip  /* 0 if x=RA, y=Dec; 1 if x=Dec, y=RA */
      , imflip   /* If not 0, image is reflected around axis */
      , xinc     /* X coordinate increment (deg) */
      , yinc     /* Y coordinate increment (deg) */
      , pa_north /* Position angle of north (0=horizontal) */
      , pa_east  /* Position angle of east (0=horizontal) */
      , imrot    /* Rotation angle of North Pole */
      , rot      /* rotation around axis (deg) (N through E) */
    ) = getRotMetadata()
  //--------------------------------------------------------------------------
  val orientation = tan_get_orientation() //rotation regading celestial coordinates: North up, East left
  //--------------------------------------------------------------------------
  def hasNonStandardWcs = nonStandardWCS.isDefined
  //--------------------------------------------------------------------------
  def getNonStandardWcs = nonStandardWCS
  //--------------------------------------------------------------------------
  def setNonStandardWcs(n: Option[NonStandardPolyWcs]) = nonStandardWCS = n
  //--------------------------------------------------------------------------
  private def getEulerAngles() = {
    //see sectionName 19.17.2.1 of the wcslib documentation
    val euler_1 = 90.0 - crval.y
    Array(
      crval.x   //celestial longitude of the native pole in deg
      , euler_1   //Celestial colatitude of the native pole or native colatitude of the celestial pole in deg
      , 180       //Native longitude of the celestial pole in deg
      , cos(toPreciseRadians(euler_1)) //cosin of the celestial longitude of the native pole
      , sin(toPreciseRadians(euler_1)))//sin of the celestial longitude of the native pole
  }
  //--------------------------------------------------------------------------
  def getCrpixAsRadians = Point2D_Double(crpix_x_rad,crpix_y_rad)
  //--------------------------------------------------------------------------
  def getCrvalAsRadians = Point2D_Double(crval_ra_rad,crval_dec_rad)
  //--------------------------------------------------------------------------
  //See Calabreta, paper 1, sectionName 2.1.1 Basic formalism
  def getProyection() = ctype_1.drop(5).take(3)
  //--------------------------------------------------------------------------
  //See Calabreta, paper 1, sectionName 2.1.1 Basic formalism
  def getCelestialSystem() = (ctype_1.take(4),ctype_2.take(4))
  //--------------------------------------------------------------------------
  //original wcslib tans2x
  private def sphericalProjection(phiTheta: Point2D_Double) = {
    val xp = sin(toPreciseRadians(phiTheta.x))
    val yp = cos(toPreciseRadians(phiTheta.x))
    val s = sin(toPreciseRadians(phiTheta.y))
    if (s == 0d) Point2D_Double.POINT_ZERO
    else {
      val r =  180.0d/PI * cos(toPreciseRadians(phiTheta.y)) / s
      Point2D_Double(r * xp, -r * yp) //assuming zero fiducial offsets
    }
  }
  //--------------------------------------------------------------------------
  //From intermediate pixel coordinates in TAN projection to intermediate pixel coordiantes
  //original function 'sphs2x' in wcslib
  //used in skyToPix
  private def celestialToIpc(ra: Double, dec: Double)  = {
    val specialCase = specialCaseRotations2(eulerAngles, ra, dec)
    if (specialCase.isDefined) specialCase.get
    else {

      // Do lng dependency.
      val phi = ra - eulerAngles(0)

      // Do lat dependency
      val dlng = 0d
      var dphi = ra
      val coslat = cos(toPreciseRadians(dec))
      val sinlat = sin(toPreciseRadians(dec))
      val coslat3 = coslat * eulerAngles(3)
      val coslat4 = coslat * eulerAngles(4)
      val sinlat3 = sinlat * eulerAngles(3)
      val sinlat4 = sinlat * eulerAngles(4)

      //Compute the native longitude.
      val sinlng = sin(toPreciseRadians(phi))
      val coslng = cos(toPreciseRadians(phi))
      val tol = 1.0e-5

      // Compute the celestial longitude
      var x =  sinlat4 - coslat3*coslng
      if (abs(x) < tol) x = -cos(toPreciseRadians(dec + eulerAngles(1))) +
                            coslat3 * (1.0 - coslng) // Rearrange formula to reduce roundoff errors.

      val y = -coslat * sinlng
      if (x != 0.0 || y != 0.0) dphi = toPreciseDegrees(Math.atan2(y, x))
      else { // Change of origin of longitude.
        if (eulerAngles(1) < 90.0) dphi = dlng + 180.0
        else dphi = -dlng
      }

      val phip = MyMath.normalizeLongitude2(MyMath.fmod(eulerAngles(2) + dphi, 360.0))

      // Compute the native latitude.
      val latp = 0d
      var thetap = 0d
      if (MyMath.fmod(phi, 180.0) == 0.0) thetap = MyMath.normalizeLatitude(latp + coslng * eulerAngles(1))
      else {
        val z = sinlat3 + coslat4 * coslng
        thetap =
          if (Math.abs(z) > 0.99)
            toPreciseDegrees(MyMath.copySign(acos(sqrt(x * x + y * y)), z)) // Use an alternative formula for greater accuracy
          else toPreciseDegrees(asin(z))
      }
      sphericalProjection(Point2D_Double(phip,thetap))
    }
  }
  //--------------------------------------------------------------------------
  //From intermediate pixel coordinates in TAN projection to celestial coordinates
  //original function 'sphx2s' in wcslib
  //used in xyToPixel
  private def ipcTanToCelestial(XY: Point2D_Double) : Point2D_Double = {
    val X = XY.x  //in degrees
    val Y = XY.y  //in degrees

    //assuming zero fiducial offsets
    //wcslib function 'tanx2s'
    val r = sqrt( (X * X) + (Y * Y))
    val phi = if (r == 0) 0d else toPreciseDegrees(atan2(X,-Y))
    val theta = toPreciseDegrees(atan2(180.0/PI,r))
    val tol = 1.0e-5

    val specialCase = specialCaseRotations(eulerAngles, phi, theta)
    if (specialCase.isDefined) specialCase.get
    else {

      // Do phi dependency
      var lng = phi - eulerAngles(2)

      // Do theta dependency
      var dlng = 0d
      val dphi = lng
      val costhe = cos(toPreciseRadians(theta))
      val sinthe = sin(toPreciseRadians(theta))
      val costhe3 = costhe * eulerAngles(3)
      val costhe4 = costhe * eulerAngles(4)
      val sinthe3 = sinthe * eulerAngles(3)
      val sinthe4 = sinthe * eulerAngles(4)

      val sinphi = sin(toPreciseRadians(dphi))
      val cosphi = cos(toPreciseRadians(dphi))

      // Compute the celestial longitude
      var x = sinthe4 - costhe3 * cosphi
      if (abs(x) < tol) x = -cos(toPreciseRadians(theta + eulerAngles(1))) +
                            costhe3 * (1.0 - cosphi) // Rearrange formula to reduce roundoff errors.

      val y = -costhe * sinphi
      if (x != 0.0 || y != 0.0) dlng = toPreciseDegrees(Math.atan2(y, x))
      else { // Change of origin of longitude.
        if (eulerAngles(1) < 90.0) dlng = dphi + 180.0
        else dlng = -dphi
      }

      lng = MyMath.normalizeLongitude(eulerAngles(0), eulerAngles(0) + dlng)

      // Compute the celestial latitude
      var lat = 0d
      if (MyMath.fmod(dphi, 180.0) == 0.0) lat = MyMath.normalizeLatitude(theta + cosphi * eulerAngles(1))
      else {
        val z = sinthe3 + costhe4 * cosphi
        lat =
          if (Math.abs(z) > 0.99) toPreciseDegrees(MyMath.copySign(acos(sqrt(x * x + y * y)), z)) // Use an alternative formula for greater accuracy
          else toPreciseDegrees(asin(z))
      }
      Point2D_Double(lng,lat)
    }
  }
  //---------------------------------------------------------------------------
  private def constrainsDisplacement(delta: Double) = {
    if (abs(delta) < 1.0e-6) {
      if (delta < 0d)  -1.0e-6
      else 1.0e-6
    }
    else {
      if (1d < abs(delta)) {
        if (delta < 0d) -1.0 else 1.0
      }
      else delta
    }
  }
  //---------------------------------------------------------------------------
  //wcslib function 'disx2p'
  //apply the inverse of the distorsion function. By definition the de-distorsion is in the world-to-pixel direction
  private def applyInverseDistortion(raDec: Point2D_Double, verbose: Boolean = false) : Point2D_Double = {
    //------------------------------------------------------------------------
    val TOL = 1.0e-13
    //------------------------------------------------------------------------
    //get zeroth approximation
    //The assumption here and below is that the distortion is small so that, to first order in the neighbourhood of
    // the solution, discrd[j] ~= a + b*rawcrd[j]
    var rawcrd = raDec + sip.evalInverse(raDec - crpix)
    val discrd = raDec

    // Iteratively invert the (well-behaved!) distortion function.
    val iterMax = 100
    var rcrd1 = Point2D_Double.POINT_ZERO
    var dcrd1 = Point2D_Double.POINT_ZERO
    for(iter<-0 until iterMax) {
      val dcrd0 = rawcrd + sip.evalForward(rawcrd - crpix)
      var delta = discrd - dcrd0
      val dd = Point2D_Double(if (abs(discrd.x) < 1) delta.x else delta.x / discrd.x
        , if (abs(discrd.y) < 1) delta.y else delta.y / discrd.y)
      if  ( (abs(dd.x) == 0 &&  abs(dd.y) == 0) ||
            (TOL >= abs(dd.x) && TOL >= abs(dd.y))) return rawcrd

      // Determine a suitable test point for computing the gradient.
      delta = delta / 2d
      delta = Point2D_Double(constrainsDisplacement(delta.x), constrainsDisplacement(delta.y))

      //the gradient of discrd[j] should be dominated by the partial derivative with
      // respect to rawcrd[j], and we can neglect partials with respect to rawcrd[i], where i != j.
      if (iter < iterMax/2) {
        rcrd1 = rawcrd + delta
        dcrd1 =  rcrd1 + sip.evalForward(rcrd1 - crpix)
        rawcrd = rawcrd + ((discrd - dcrd0) * (delta / (dcrd1 - dcrd0))) // Compute the next approximation.
      }
      else { // Convergence should not take more than seven or so iterations.  As it is slow, try computing the gradient in full.¡
        rcrd1 = rawcrd + delta
        dcrd1 = rcrd1  + sip.evalForward(rcrd1 - crpix)
        rawcrd = rawcrd + ((discrd - dcrd0) * (delta / (dcrd1 - dcrd0))) // Compute the next approximation.
        rcrd1 = rcrd1 - delta
      }
    }
    if (verbose) error(s"wcs sip applyInverseDistorsion. Convergence not achieved after $iterMax")
    Point2D_Double.POINT_INVALID
  }
  //--------------------------------------------------------------------------
  //From intermediate pixel coordinates to pixel
  //Shupe Eq. 4
  private def ipcToPixel(XY: Point2D_Double) : Point2D_Double = { //(X,Y) in radians
    val X = XY.x  //in radians
    val Y = XY.y  //in radians
    val x = icd_11 * X + icd_12 * Y + crpixX
    val y = icd_21 * X + icd_22 * Y + crpixY
    applyInverseDistortion(Point2D_Double(x,y))  //apply inverse distortion
  }
  //--------------------------------------------------------------------------
  //From pixel to intermediate pixel coordinates
  //See Greinsen and Calabretta 1. Eq.1
  //See Shupe. Eq 1.
  //See Postma. Eq 3
  private def shupePixToIpc(x: Double, y: Double)  = {
    val u = x - crpixX
    val v = y - crpixY
    val f_g_u_v = sip.evalForward(u, v) //calculate distorsion
    val u_f = u + f_g_u_v.x // i + f(i,v)
    val v_g = v + f_g_u_v.y // v + g(i,v)
    val X = cd_11 * u_f + cd_12 * v_g
    val Y = cd_21 * u_f + cd_22 * v_g
    Point2D_Double(X, Y)  //in degrees
  }
  //--------------------------------------------------------------------------
  //From intermediate pixel coordinates in TAN projection to intermediate pixel coordiantes
  //Shupe Eq. 2
  private def shupeCelestialToIpc(ra: Double, dec: Double)  = {
    val sin_dec = sin(toPreciseRadians(dec))
    val cos_dec = cos(toPreciseRadians(dec))

    val sin_ra_ra_0 = sin(toPreciseRadians(ra - crval_ra_rad))
    val cos_ra_ra_0 = cos(toPreciseRadians(ra - crval_ra_rad))

    val denom = cos_crval_dec * cos_dec * cos_ra_ra_0 + sin_crval_ra * sin_dec
    val X = (cos_dec * sin_ra_ra_0) / denom
    val Y = (cos_crval_dec * sin_dec - cos_dec * sin_crval_ra * cos_ra_ra_0 ) / denom
    Point2D_Double(X,Y)  //in radians
  }
  //--------------------------------------------------------------------------
  //From intermediate pixel coordinates in TAN projection to celestial coordinates
  //Shupe Eq. 3
  private def shupeIpcTanToCelestial(XY: Point2D_Double) : Point2D_Double = {
    val X = XY.x  //in radians
    val Y = XY.y  //in radians
    val ra = crval_ra_rad + atan( X / (cos_crval_dec - Y * sin_crval_ra))
    val dec = asin( (sin_crval_ra + Y * cos_crval_dec) / sqrt( 1 + (X * X) + (Y * Y)) )
    Point2D_Double(ra,dec).toDegrees  //in degrees
  }
  //--------------------------------------------------------------------------
  //Shupe Eq. 4
  private def shupeIpcToPixel(XY: Point2D_Double) : Point2D_Double = { //(X,Y) in radians
    val X = XY.x  //in radians
    val Y = XY.y  //in radians
    val x = icd_11 * X + icd_12 * Y + crpix_x_rad
    val y = icd_21 * X + icd_22 * Y + crpix_y_rad
    val pixCoord = Point2D_Double(x, y).toDegrees
    val distorsion = sip.evalInverse(pixCoord - crpix)
    pixCoord + distorsion
  }
  //--------------------------------------------------------------------------
  //no SIP applied
  def pixToSkyLinear(xy: Point2D_Double) : Point2D_Double = {
    val u = xy.x - crpixX
    val v = xy.y - crpixY
    val X = cd_11 * u + cd_12 * v
    val Y = cd_21 * u + cd_22 * v
    ipcTanToCelestial(Point2D_Double(X, Y))  //in degrees
  }
  //--------------------------------------------------------------------------
  //no SIP applied
  def skyToPixLinear(raDec: Point2D_Double) : Point2D_Double = {
    val XY = celestialToIpc(raDec.x, raDec.y)
    val X = XY.x
    val Y = XY.y
    val x = icd_11 * X + icd_12 * Y + crpixX
    val y = icd_21 * X + icd_22 * Y + crpixY
    Point2D_Double(x,y)
  }
  //--------------------------------------------------------------------------
  def pixToSky(p: Point2D_Double) : Point2D_Double = {
    if (nonStandardWCS.isDefined) nonStandardWCS.get.pixToSky(p)
    else wcslibPixToSky(p.x, p.y)
  }
  //--------------------------------------------------------------------------
  def skyToPix(p: Point2D_Double) : Point2D_Double = {
    if (nonStandardWCS.isDefined) nonStandardWCS.get.skyToPix(p)
    else wcslibSkyToPix(p.x, p.y)
  }
  //--------------------------------------------------------------------------
  //porting from wcslib 7.11 from Mark Calabretta simplified for two axes case and SIP for expressing the distorsions and TAN projection
  //original function 'wcsp2s() transforms pixel coordinates to world coordinates'
  private def wcslibPixToSky(x: Double, y: Double) : Point2D_Double = ipcTanToCelestial(shupePixToIpc(x,y))
  //--------------------------------------------------------------------------
  //porting from wcslib 7.11 from Mark Calabretta simplified for two axes case and SIP for expressing the distorsions and TAN projection
  //original function 'wcss2p() transforms world coordinates to pixel coordinates'
  private def wcslibSkyToPix(ra: Double, dec: Double) : Point2D_Double = {
    val p = ipcToPixel(celestialToIpc(ra,dec))
    if (p == Point2D_Double.POINT_INVALID) shupeSkyToPix(ra,dec)
    else p
  }
  //--------------------------------------------------------------------------
  //implementation using Shupe paper
  private def shupePixToSky(raDec: Point2D_Double) : Point2D_Double =
    shupeIpcTanToCelestial(shupePixToIpc(raDec.x,raDec.y).toRadians())
  //--------------------------------------------------------------------------
  //implementation using Shupe paper
  private def shupeSkyToPix(ra: Double, dec: Double) : Point2D_Double =
    shupeIpcToPixel(shupeCelestialToIpc(ra,dec))
  //--------------------------------------------------------------------------
  private def wcstoolsPixToSky(imageName: String, raDec: Point2D_Double) : Point2D_Double = {
    val command = s"/home/rafa/images/tools/wcstools/bin/xy2sky  -d -n 20 $imageName ${raDec.x} ${raDec.y}"
    val r = command.!!
    val seq = r.trim.replaceAll(" +", " ").split(" ")
    Point2D_Double(seq(0).toDouble, seq(1).toDouble)
  }
  //--------------------------------------------------------------------------
  private def wcstoolsSkyToPix(imageName: String, ra: Double, dec: Double) : Point2D_Double ={
    val command = s"/home/rafa/images/tools/wcstools/bin/sky2xy -o z -n 10  $imageName $ra $dec"
    val r = command.!!
    val seq = r.trim.replaceAll(" +", " ").split(" ")
    Point2D_Double(seq(0).toDouble, seq(1).toDouble)
  }
  //---------------------------------------------------------------------------
  def getCD_AsArray() = Array(cd_11,cd_12,cd_21,cd_22)
  //--------------------------------------------------------------------------
  def getTanWcs() = WCS(this,getCD_AsArray(),crpix,crval,SIP(sip.aOrder))
  //--------------------------------------------------------------------------
  def pretty_print() =
    info(s"\n" +
      s"CTYPE1    : $ctype_1\n"+
      s"CTYPE2    : $ctype_2\n"+
      s"CUNIT1    : $cUnit_1\n"+
      s"CUNIT2    : $cUnit_2\n"+
      s"crpix_x   : ${crpix.x}\n"+
      s"crpix_y   : ${crpix.y}\n"+
      s"crval_ra  : ${crval.x}\n"+
      s"crval_dec : ${crval.y}\n"+
      s"cd_11     : $cd_11\n"+
      s"cd_12     : $cd_12\n"+
      s"cd_21     : $cd_21\n"+
      s"cd_22     : $cd_22\n"+
      sip.toStringSeq.mkString("\n"))
  //--------------------------------------------------------------------------
  def toPairSeq() =
    Seq( (KEY_WCSAXES,axes.toString)
       , (KEY_IMAGEW,imageW.toString)
       , (KEY_IMAGEH,imageH.toString)
       , (KEY_CTYPE1,ctype_1)
       , (KEY_CTYPE2,ctype_2)
       , (KEY_CUNIT_1,cUnit_1)
       , (KEY_CUNIT_2,cUnit_2)
       , (KEY_LONPOLE,lonPole.toString)
       , (KEY_LATPOLE,latPole.toString)
       , (KEY_CD_1_1,cd_11.toString)
       , (KEY_CD_1_2,cd_12.toString)
       , (KEY_CD_2_1,cd_21.toString)
       , (KEY_CD_2_2,cd_22.toString)
       , (KEY_CRPIX_1,crpixX.toString)
       , (KEY_CRPIX_2,crpixY.toString)
       , (KEY_CRVAL_1,crvalRa.toString)
       , (KEY_CRVAL_2,crvalDec.toString)
       , (KEY_EQUINOX,equinox.toString)) ++ sip.toPairSeq
  //---------------------------------------------------------------------------
  // (coorflip, xinc, yinc, pa_north, pa_east, imrot, wcs_rot)
  //adapted from function: 'wcsrotset' in file "wcs.c" of library WCSLIB
  //rotational data regarding celestial coordinates
  private def getRotMetadata() = {

    var xinc = Math.sqrt(cd_11 * cd_11 + cd_21 * cd_21);
    var yinc = Math.sqrt(cd_12 * cd_12 + cd_22 * cd_22);

    xinc = Math.abs(xinc)
    yinc = Math.abs(yinc)

    //Compute position angles of North and East in image
    val raDec = pixToSky(crpix)
    val xc = crpix.x
    val yc = crpix.y

    //set in function 'wcstype' file 'wcs.c'
    //coorflip: 0 if x=RA, y=Dec; 1 if x=Dec, y=RA
    val coorflip = (ctype_1.take(3) == "DEC") || (ctype_1.drop(1).take(3) == "LAT")

    val (xye, xyn) =
      if (coorflip) {
        (skyToPix(Point2D_Double(raDec.x + yinc, raDec.y))
       , skyToPix(Point2D_Double(raDec.x, raDec.y + xinc)))
      }
      else {
        (skyToPix(Point2D_Double(raDec.x + xinc, raDec.y))
       , skyToPix(Point2D_Double(raDec.x, raDec.y + yinc)))
      }

    //pa_north: Position angle of north (0=horizontal)
    var pa_north = toPreciseDegrees(Math.atan2(xyn.y - yc, xyn.x - xc))
    if (pa_north < -90d) pa_north = pa_north + 360

    //pa_east: Position angle of east (0=horizontal)
    var pa_east = toPreciseDegrees(Math.atan2(xye.y - yc, xye.x - xc))
    if (pa_east < -90d) pa_east = pa_east + 360

    //imrot: Rotation angle of north pole
    val imrot =
      if (pa_north < -90d) 270d + pa_north
      else pa_north - 90.0

    //rot: rotation around axis (deg) (N through E)
    var rot =
      if (coorflip) {
        val tmp_wcs_rot = imrot + 90d
        if (tmp_wcs_rot < 0.0) tmp_wcs_rot + 360.0
        else tmp_wcs_rot
      }
      else imrot

    if (rot < 0.0d) rot = rot + 360.0
    if (rot >= 360d) rot = rot - 360.0

    // Set image mirror flag based on axis orientation
    //imflip: If not 0, image is reflected around axis

    var imflip = 0
    if (pa_east  - pa_north < -80.0 && pa_east - pa_north > -100.0) imflip = 1
    if (pa_east  - pa_north < 280.0 && pa_east - pa_north > 260.0) imflip = 1
    if (pa_north - pa_east  > 80.0 && pa_north - pa_east < 100.0) imflip = 1

    if (coorflip) {
      if (imflip == 1) yinc = -yinc
    }
    else {
      if (imflip == 0) xinc = -xinc
    }
    (coorflip, imflip, xinc, yinc, pa_north, pa_east, imrot, rot)
  }

  //---------------------------------------------------------------------------
  //origonal code: 'double tan_get_orientation(const tan_t* tan)' from file: '/astrometry.net/util/sip.c'
  private def tan_get_orientation(): Double = {
    val parity = if (det >= 0) 1.0 else -1.0
    val T = parity * cd_11 + cd_22
    val A = parity * cd_21 - cd_12
    toPreciseDegrees(Math.atan2(A, T)) * -1
  }
  //--------------------------------------------------------------------------
  def hasPreciseSIP() =
    !(sip.aOrder == 1 && sip.aSeq.forall(_ == 0))
  //--------------------------------------------------------------------------
  //AST library "fitschan.c" line 31436
  def getCD_delt() = {
    val diag_1 = if (cd_11 > 0) 1 else -1
    val cdelt_i = Math.sqrt((cd_11 * cd_11) + (cd_12 * cd_12)) * diag_1

    val diag_2 = if (cd_22 > 0) 1 else -1
    val cdelt_j = Math.sqrt((cd_21 * cd_21) + (cd_22 * cd_22)) * diag_2

    Seq(cdelt_i,cdelt_j)
  }
  //--------------------------------------------------------------------------
  def getPC_ij() = {
    val cdelt_ij = getCD_delt()
    Seq(cd_11 / cdelt_ij(0)
    , cd_12 / cdelt_ij(0)
    , cd_21 / cdelt_ij(1)
    , cd_22 / cdelt_ij(1))
  }
  //--------------------------------------------------------------------------
  def getCD_AsRadians() =
    getCD_AsArray() map ( _ * PRECISE_DEGREES_TO_RADIANS)
  //--------------------------------------------------------------------------
  def getLongitudeLatitude() = Point2D_Double(lonPole, latPole)
  //--------------------------------------------------------------------------
  def getLongitudeLatitudeAsRadians() = getLongitudeLatitude.toRadians()
  //--------------------------------------------------------------------------
  //(northIsUp,eastIsLeft)
  def getRotationAngle() =
    toPreciseDegrees(Math.atan2(cd_21,cd_11))

  //--------------------------------------------------------------------------
}
//=============================================================================
//End of file Wcs.scala
//=============================================================================
