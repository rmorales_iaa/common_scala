/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Mar/2019
 * Time:  17h:35m
 * Description: Fast and dirty implementqation of FITS standard usable only to
 * get the FITS header primary HDU using a stream data
 */
//=============================================================================
package com.common.fits.simpleFits
//=============================================================================
import java.nio.file.{Files, Paths}
import scala.collection.mutable.ArrayBuffer
import scala.util.matching.Regex
//=============================================================================
import com.common.logger.MyLogger
import com.common.util.path.Path
//=============================================================================
//=============================================================================
object SimpleFits extends MyLogger {
  //---------------------------------------------------------------------------
  final val HDU_BLOCK_BYTE_SIZE = 2880
  //---------------------------------------------------------------------------
  final val HDU_RECORD_BYTE_SIZE = 80
  //---------------------------------------------------------------------------
  final val KEY_END           = "END"

  final val KEY_SIMPLE        = "SIMPLE"
  final val KEY_BIT_PIX       = "BITPIX"
  final val KEY_BSCALE        = "BSCALE"
  final val KEY_BZERO         = "BZERO"

  final val KEY_NAXIS         = "NAXIS"

  final val KEY_NAXIS_1       = "NAXIS1"


  final val KEY_NAXIS_2       = "NAXIS2"

  //exposure time
  final val KEY_EXPTIME       = "EXPTIME"
  final val KEY_EXPOSURE      = "EXPOSURE"
  final val KEY_EXP_TIME      = "EXP_TIME"
  final val KEY_EXPOSURE_TIME_SEQ = Array(
    KEY_EXPTIME
    , KEY_EXPOSURE
    , KEY_EXP_TIME
  )

  //date and time
  final val KEY_DATEOBS       = "DATE-OBS"
  final val KEY_DATE          = "DATE"

  final val KEY_TIMEOBS       = "TIME-OBS"
  final val KEY_TIME          = "TIME"

  final val KEY_UTSTART       = "UTSTART"
  final val KEY_UT            = "UT"
  final val KEY_UTOBS         = "UTOBS"

  final val KEY_UTCSTART      = "UTCSTART"
  final val KEY_UTC_OBS       = "UTC-OBS"
  final val KEY_UTCEND        = "UTCEND"

  final val KEY_TIMESTMP      = "TIMESTMP"

  //temperature
  final val KEY_CCD_TEMP      = "CCD-TEMP"
  final val KEY_CCDTEMP       = "CCDTEMP"
  final val KEY_TEMP          = "TEMP"

  //object
  final val KEY_OBJECT        = "OBJECT"
  final val KEY_OBJEC         = "OBJEC"
  final val KEY_OBJCAT        = "OBJCAT"
  final val KEY_CAT_NAME      = "CAT-NAME"
  final val KEY_CAT_NAME_1    = "CAT_NAME"

  //observing type
  final val KEY_OBSTYPE       = "OBSTYPE"
  final val KEY_OBS_TYPE      = "OBS-TYPE"
  final val KEY_OBS_TYPE_2    = "OBS_TYPE"

  //image type
  final val KEY_IMAGETYP      = "IMAGETYP"

  //filters
  final val KEY_FILTER            = "FILTER"
  final val KEY_FILTER_HIERARCHY  = "FILT_HIE"

  final val KEY_CAHA_FILTER_HIERARCHY_1 = "HIERARCH CAHA INS FILT1 NAME"
  final val KEY_CAHA_FILTER_HIERARCHY_2 = "HIERARCH CAHA INS FILT2 NAME"
  final val KEY_CAHA_FILTER_HIERARCHY_3 = "HIERARCH CAHA INS FILT3 NAME"
  final val KEY_CAHA_FILTER_HIERARCHY_4 = "HIERARCH CAHA INS FILT4 NAME"

  final val KEY_ESO_FILTER_HIERARCHY_4 = "HIERARCH ESO INS FILT1 NAME"

  final val HIERARCHY_FILTER_KEY_SEQ = Seq(
      KEY_CAHA_FILTER_HIERARCHY_1
    , KEY_CAHA_FILTER_HIERARCHY_2
    , KEY_CAHA_FILTER_HIERARCHY_3
    , KEY_CAHA_FILTER_HIERARCHY_4
    , KEY_ESO_FILTER_HIERARCHY_4)

  val FILTER_NUMBERED_NAME_SEQ = Seq("FILTER1", "FILTER2", "FILTER3", "FILTER4")

  //comment, history, hierarchy
  final val KEY_COMMENT  = "COMMENT"
  final val KEY_HISTORY  = "HISTORY"
  final val KEY_HIERARCH = "HIERARCH"

  //telescope
  final val KEY_TELESCOPE   = "TELESCOP"
  final val KEY_OBSERVATORY = "OBSERVAT"
  final val KEY_ORIGIN      = "ORIGIN"

  //extensions
  final val KEY_GCOUNT    = "GCOUNT"
  final val KEY_EXTNAME   = "EXTNAME"
  final val KEY_EXTVER    = "EXTVER"

  final val KEY_EXTEND    = "EXTEND"
  final val KEY_EXTENSION = "XTENSION"

  //instrument
  final val KEY_INSTRUM   = "INSTRUM"
  final val KEY_INSTRUME  = "INSTRUME"

  //wcs
  final val KEY_CRPIX_1 = "CRPIX1"
  final val KEY_CRPIX_2 = "CRPIX2"

  final val KEY_CTYPE1 = "CTYPE1"
  final val KEY_CTYPE2 = "CTYPE2"

  final val KEY_CDELT_1 = "CDELT1"
  final val KEY_CDELT_2 = "CDELT2"

  final val KEY_CUNIT_1 = "CUNIT1"
  final val KEY_CUNIT_2 = "CUNIT2"

  final val KEY_CRVAL_1 = "CRVAL1"
  final val KEY_CRVAL_2 = "CRVAL2"

  final val KEY_EQUINOX = "EQUINOX"

  final val KEY_A_ORDER = "A_ORDER"
  final val KEY_B_ORDER = "B_ORDER"

  final val KEY_AP_ORDER = "AP_ORDER"
  final val KEY_BP_ORDER = "BP_ORDER"

  final val KEY_CD_1_1 = "CD1_1"
  final val KEY_CD_1_2 = "CD1_2"
  final val KEY_CD_2_1 = "CD2_1"
  final val KEY_CD_2_2 = "CD2_2"

  final val KEY_WCSAXES = "WCSAXES"
  final val KEY_IMAGEW  = "IMAGEW"
  final val KEY_IMAGEH  = "IMAGEH"

  final val KEY_LONPOLE = "LONPOLE"
  final val KEY_LATPOLE = "LATPOLE"

  final val KEY_CROT_1  = "CROT1"
  final val KEY_CROT_2  = "CROT2"

  //binning
  final val KEY_BINNING_1 = "BIN1"
  final val KEY_BINNING_2 = "BINNING"
  final val KEY_BINNING_3 = "BINX"
  final val KEY_BINNING_4 = "XBINNING"
  final val KEY_BINNING_5 = "CCDBINX"
  final val KEY_BINNING_6 = "CRDELT1"
  final val KEY_BINNING_7 = "CCDXBIN"

  final val KEY_BINNING_SEQ = Seq(
      KEY_BINNING_1
    , KEY_BINNING_2
    , KEY_BINNING_3
    , KEY_BINNING_4
    , KEY_BINNING_5
    , KEY_BINNING_6
    , KEY_BINNING_7
  )

  //OM calibration
  final val KEY_OM_VER            = "OM_VER"
  final val KEY_OM_RUN_DATE       = "OM_RUN"
  final val KEY_OM_MASTER         = "OM_MASTE"
  final val KEY_OM_DATEOBS        = "OM_DATEO"
  final val KEY_OM_EXPTIME        = "OM_EXPTI"
  final val KEY_OM_FILTER         = "OM_FILTE"
  final val KEY_OM_TELESCOPE      = "OM_TELES"
  final val KEY_OM_INSTRUMENT     = "OM_INSTR"
  final val KEY_OM_TEMPERATURE    = "OM_TEMP"
  final val KEY_OM_OBJECT         = "OM_OBJEC"
  final val KEY_OM_BINNING        = "OM_BINNI"
  final val KEY_OM_PIX_SCALE_X    = "OM_PIXSX" //arcosec / pix
  final val KEY_OM_PIX_SCALE_Y    = "OM_PIXSY" //arcosec / pix
  final val KEY_OM_FOV_X          = "OM_FOV_X" //field of view in X axis in arc minutes
  final val KEY_OM_FOV_Y          = "OM_FOV_Y" //field of view in X axis in arc minutes
  final val KEY_OM_FLAGS          = "OM_FLAGS"

  //m2 WCS FIT
  final val KEY_M2_WCS_FIT  = "M2_WCS_F"
  final val KEY_M2_VERSION  = "M2_VER"
  final val KEY_M2_RUN_DATE = "M2_RUN"

  final val KEY_OM_SEQ = Seq(
      KEY_OM_VER
    , KEY_OM_RUN_DATE
    , KEY_OM_MASTER
    , KEY_OM_DATEOBS
    , KEY_OM_EXPTIME
    , KEY_OM_FILTER
    , KEY_OM_TELESCOPE
    , KEY_OM_INSTRUMENT
    , KEY_OM_TEMPERATURE
    , KEY_OM_OBJECT
    , KEY_OM_BINNING
    , KEY_OM_PIX_SCALE_X
    , KEY_OM_PIX_SCALE_Y
    , KEY_OM_FOV_X
    , KEY_OM_FOV_Y
    , KEY_OM_FLAGS
    , KEY_M2_WCS_FIT
    , KEY_M2_VERSION
    , KEY_M2_RUN_DATE
  )
  //---------------------------------------------------------------------------
  final val WCS_IGNORE_KEY_SEQ = Seq(KEY_EQUINOX
                                     , KEY_A_ORDER, KEY_B_ORDER, KEY_AP_ORDER, KEY_BP_ORDER
                                     , KEY_CD_1_1, KEY_CD_1_2, KEY_CD_2_1, KEY_CD_2_2
                                     , KEY_CRPIX_1, KEY_CRPIX_2, KEY_CRVAL_1, KEY_CRVAL_2
                                     , KEY_IMAGEW, KEY_IMAGEH
                                     , KEY_LONPOLE, KEY_LATPOLE
                                     , KEY_WCSAXES, KEY_CTYPE1, KEY_CTYPE2
                                     , KEY_CUNIT_1, KEY_CUNIT_2
                                     , KEY_CROT_1, KEY_CROT_2
                                     , KEY_CDELT_1, KEY_CDELT_2
                                     , KEY_M2_WCS_FIT)

  final val WCS_IGNORE_KEY_PATTERN = Seq("A_\\d_\\d".r
                                          , "B_\\d_\\d".r
                                          , "AP_\\d_\\d".r
                                          , "BP_\\d_\\d".r)
  final val AT_WCS_IGNORE_KEY_PATTERN = Seq("AT_ERR_P".r
                                          , "AT_R_.*".r
                                          , "AT_D_.*".r
                                          , "AT_X_.*".r
                                          , "AT_Y_.*".r
                                          , "AT_ER_.*".r
                                          , "AT_ED_.*".r
                                          , "AT_EX_.*".r
                                          , "AT_EY_.*".r)

  //---------------------------------------------------------------------------
  val FITS_MIN_INFO_RECORD_SEQ = Array(
    KEY_SIMPLE
    , KEY_BIT_PIX
    , KEY_NAXIS
    , KEY_NAXIS_1
    , KEY_NAXIS_2
    , KEY_BSCALE
    , KEY_BZERO
  )
  //---------------------------------------------------------------------------
  def apply(fileName: String) : SimpleFits = {
    val f = SimpleFits()
    f.fullFileName = fileName
    f.fileName = Path.getOnlyFilename(fileName)
    f.parseHeader(Files.readAllBytes(Paths.get(fileName)))
    f
  }
  //---------------------------------------------------------------------------
  def apply(stream: Seq[Byte]) : SimpleFits = {
    val f = new SimpleFits()
    f.parseHeader(stream)
    f
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
import com.common.fits.simpleFits.SimpleFits._
case class SimpleFits() extends MyLogger {
  //---------------------------------------------------------------------------
  private val keyValueMap = scala.collection.mutable.LinkedHashMap[String, String]()
  private val keyCommentValueMap = scala.collection.mutable.Map[String, String]()
  //---------------------------------------------------------------------------
  //name
  var fullFileName = ""
  var fileName = ""

  //size
  var width:  Long = 0
  var height: Long = 0
  var bitPix: Byte = 0
  var bScale: Double = 1
  var bZero: Double = 0

  //size
  var hduCount : Int = 0
  var parsedHeaderByteCount : Int = 0
  var imageDataAbsoluteOffset : Int = 0
  var totalByteCount : Long = 0

  private var parsed: Boolean = false
  private var isFirstRecordExtension = true
  //---------------------------------------------------------------------------
  private def findCommentPositionNotInString(s: String): Int = {
    var pos = 0
    var quoteOpen = false
    var lastQuotePos = -1
    var lastCommentPos = -1
    s.foreach { c=>
      pos +=1
      if (c == '\'') {
        if (quoteOpen) quoteOpen = false else quoteOpen = true
        lastQuotePos = pos
      }
      else {
        if (c == '/') {
          lastCommentPos = pos
          if (!quoteOpen) return pos
        }
      }
    }
    if (quoteOpen) lastCommentPos
    else -1
  }
  //---------------------------------------------------------------------------
  private def parseRecord(stream: Seq[Byte], parsingExtension : Boolean = false): Boolean = {
    val s = new String( stream.toArray ).trim

    if ((keyValueMap.size == 0) && !s.startsWith(KEY_SIMPLE))
      return error(s"Invalid FITS format string on file: '$fullFileName' on first record. Waiting key: '$KEY_SIMPLE'")

    //the file has the key for extension but really do not have one
    if(parsingExtension && isFirstRecordExtension && !s.startsWith(KEY_EXTENSION)) return false //stop parsing

    val posComment = findCommentPositionNotInString(s)
    val (s2,comment) =
      if (posComment > 0) (s.substring(0, posComment - 1).trim, s.substring(Math.min(s.length,posComment),s.length).trim)
      else (s.trim,"")

    if (s2 == KEY_END) {
      parsedHeaderByteCount += KEY_END.length
      return false
    }

    val posEqual = s2.indexOf("=")
    if ((posEqual == 8) || s2.startsWith(KEY_HIERARCH)) { //avoid records: comments and history
      val recordKey = s2.substring(0, posEqual).trim
      val rightSide = s2.substring(posEqual+1).trim
      val recordValue =
        if(rightSide.startsWith("'")) {
          val endPos = rightSide.lastIndexOf("'")
          if (endPos == 0) { //only one single quotation mark
            val posComment = rightSide.indexOf("/")
            if (posComment == -1) "'" + rightSide.substring(1) + "'"
            else "'" + rightSide.substring(1, posComment-1).trim + "'"
          }
          else "'" + rightSide.substring(1,rightSide.lastIndexOf("'")).trim + "'"
        } else {
          s2.substring(posEqual + 1).trim
        }
      //append but keeping first value of the key
      if (!parsingExtension) {
        keyValueMap += (recordKey -> recordValue)
        if (!comment.isEmpty) keyCommentValueMap(recordKey) = comment
      }
      else {
        if(!keyValueMap.contains(recordKey)) {
          if (isFirstRecordExtension && recordValue != "'IMAGE'") return false //only parseInput IMAGE extensions. Stop parsing
          keyValueMap += (recordKey -> recordValue)
          isFirstRecordExtension = false
        }
      }
    }
    parsed = true
    true
  }
  //---------------------------------------------------------------------------
  private def parseHeader(stream: Seq[Byte]): Unit = {
    totalByteCount += stream.length
    if (stream.length < HDU_BLOCK_BYTE_SIZE)
      error(s"Invalid FITS format string on file: '$fileName'. Invalid length: ${stream.length}. It must be at least as: $HDU_BLOCK_BYTE_SIZE byte size")
    else{
      while(parseRecord( stream.slice( parsedHeaderByteCount.toInt, parsedHeaderByteCount+HDU_RECORD_BYTE_SIZE )) )
        parsedHeaderByteCount += HDU_RECORD_BYTE_SIZE

      if (parsed) {
        if(hasExtensions)
          parseJustFirstExtension(stream)
        if (isValid()){
          hduCount = (parsedHeaderByteCount / HDU_BLOCK_BYTE_SIZE) + (if ((parsedHeaderByteCount % HDU_BLOCK_BYTE_SIZE) > 0 ) 1 else 0)
          imageDataAbsoluteOffset = hduCount * HDU_BLOCK_BYTE_SIZE

          bScale = if (!existKey(KEY_BSCALE)) 1d else getKeyDoubleValue(KEY_BSCALE)
          bZero = if (!existKey(KEY_BZERO)) 0 else  getKeyDoubleValue(KEY_BZERO)
        }
      }
    }
  }
  //---------------------------------------------------------------------------
  private def parseJustFirstExtension(stream: Seq[Byte]) =  {
    val blockRemain =  parsedHeaderByteCount % HDU_BLOCK_BYTE_SIZE
    val blockCount =  parsedHeaderByteCount / HDU_BLOCK_BYTE_SIZE
    val newPos = (blockCount + (if (blockRemain > 0) 1 else 0))  * HDU_BLOCK_BYTE_SIZE
    parsedHeaderByteCount += (newPos-parsedHeaderByteCount)
    while(parseRecord( stream.slice( parsedHeaderByteCount, parsedHeaderByteCount+HDU_RECORD_BYTE_SIZE ), parsingExtension = true))
      parsedHeaderByteCount += HDU_RECORD_BYTE_SIZE
  }
  //---------------------------------------------------------------------------
  def getStringValue(k: String): Option[String] =
    if (existKey(k)) Some(keyValueMap(k))
    else None
  //---------------------------------------------------------------------------
  def getStringValueOrEmpty(k: String): String =
    if (existKey(k)) keyValueMap(k)
    else ""
  //---------------------------------------------------------------------------
  def getStringValueOrEmptyNoQuotation(k: String): String = {
    if (!existKey(k)) ""
    else getStringValueOrEmpty(k).replaceAll("'","")
  }
  //---------------------------------------------------------------------------
  def getStringValueOrEmptyNoQuotationWithDefault(k: String,default: String): String = {
    if (!existKey(k)) {
      default.replaceAll("'", "")
    }
    else {
      val s = getStringValueOrEmpty(k).replaceAll("'", "")
      if (s.isEmpty) default
      else s
    }
  }
  //---------------------------------------------------------------------------
  def getStringValueOrDefault(k: String,d: String): String =
    if (existKey(k)) keyValueMap(k)
    else d
  //---------------------------------------------------------------------------
  def existKey(k: String) = keyValueMap.contains(k)
  //---------------------------------------------------------------------------
  def getKeyValueMap = keyValueMap
  //---------------------------------------------------------------------------
  def getKeyValueCommentMap = keyCommentValueMap
  //---------------------------------------------------------------------------
  def getKeyValueMapAsArrayBuffer = ArrayBuffer(keyValueMap.toSeq:_*)
  //---------------------------------------------------------------------------
  def getKeyValueMapSize = keyValueMap.size
  //---------------------------------------------------------------------------
  def getKeyIntValue(k: String) = {
    if (!existKey(k)) {
      fatal(s"Can not find the key '$k' in the primary HDU")
      -1
    }
    else keyValueMap(k).replaceAll("'","").toInt
  }
  //---------------------------------------------------------------------------
  def getKeyIntValueOrDefault(k: String, d: Int) = {
    if (existKey(k)) keyValueMap(k).replaceAll("'","").toInt
    else d
  }
  //---------------------------------------------------------------------------
  def getKeyLongValue(k: String) = {
    if (!existKey(k)) {
      fatal(s"Can not find the key '$k' in the primary HDU")
      -1
    }
    else keyValueMap(k).replaceAll("'","").toLong
  }
  //---------------------------------------------------------------------------
  def getKeyFloatValue(k: String) = {
    if (!existKey(k)) {
      fatal(s"Can not find the key '$k' in the primary HDU")
      -1f
    }
    else keyValueMap(k).replaceAll("'","").toFloat
  }
  //---------------------------------------------------------------------------
  def getKeyDoubleValue(k: String) = {
    if (!existKey(k)) {
      fatal(s"Can not find the key '$k' in the primary HDU")
      -1d
    }
    else keyValueMap(k).replaceAll("'","").toDouble
  }
  //---------------------------------------------------------------------------
  def getKeyDoubleValueOrDefault(k: String,d: Double) :  Double =
    if (existKey(k)) {
      val s = keyValueMap(k)
      if (s.isEmpty) d
      else keyValueMap(k).replaceAll("'","").toDouble
    }
    else d
  //-------------------------------------------------------------------------
  def getHeaderRecordSeq() = keyValueMap.toIndexedSeq
  //-------------------------------------------------------------------------
  private def getDimensions() : Unit = {
    if (existKey(KEY_NAXIS_1)) width = getKeyIntValue(KEY_NAXIS_1)
    if (existKey(KEY_NAXIS_2)) height = getKeyIntValue(KEY_NAXIS_2)
    bitPix = getKeyIntValue(KEY_BIT_PIX).toByte
  }
  //-------------------------------------------------------------------------
  def isValid(verbose: Boolean = false) : Boolean = {
    //-----------------------------------------------------------------------
    def checkKey(k: String) =
      if (!existKey(k)) if (verbose) error(s"Invalid FITS file: '$fullFileName'. Does not exist record with key '$k'") else false
      else true
    //-----------------------------------------------------------------------
    //fits standard v.4.0. table 7
    if (!checkKey(KEY_SIMPLE)) return false
    if (!checkKey(KEY_BIT_PIX)) return false
    if (!checkKey(KEY_NAXIS)) return false
    //-----------------------------------------------------------------------
    //check size
    getDimensions()
    val totalPixSize = width * height
    val calculatedTotalImageByteSize = (totalPixSize * Math.abs(bitPix)/8) + parsedHeaderByteCount
    if ((calculatedTotalImageByteSize <= 0) || calculatedTotalImageByteSize > totalByteCount) {
      if (verbose) error(s"Invalid FITS file: '$fullFileName'. The file byte size: $totalByteCount is less than min expected one: $calculatedTotalImageByteSize ")
      return false
    }
    //-----------------------------------------------------------------------
    true
  }
  //-------------------------------------------------------------------------
  //see http://hosting.astro.cornell.edu/~vassilis/isocont/node17.html
  def hasWcs() =
    hasSip ||
        (existKey(KEY_CD_1_1) && existKey(KEY_CD_1_2) &&
         existKey(KEY_CD_2_1) && existKey(KEY_CD_2_2))
  //-------------------------------------------------------------------------
  def hasSip() =
    existKey(KEY_A_ORDER) &&
    existKey(KEY_B_ORDER) &&
    existKey(KEY_AP_ORDER) &&
    existKey(KEY_BP_ORDER)

  //-------------------------------------------------------------------------
  def hasExtensions() = existKey(KEY_GCOUNT)
  //-------------------------------------------------------------------------
  def hasValidSize() = (totalByteCount % HDU_BLOCK_BYTE_SIZE) != 0
  //-------------------------------------------------------------------------
  def getImageDataPixSize() = width * height
  //-------------------------------------------------------------------------
  def getImageDimension() = (width.toInt,height.toInt)
  //-------------------------------------------------------------------------
  def getImageDataByteSize() = getImageDataPixSize * (bitPix.abs / 8)
  //-------------------------------------------------------------------------
  def getAdditionalRecordSeq() = keyValueMap.toSeq.drop(7) //Typically used in 'MyFits.saveAsFits'
  //-------------------------------------------------------------------------
  def filterRecordSeqByKey(recordSeq: Array[String]) =
    keyValueMap.toSeq.filter(record=> !recordSeq.contains(record._1))
  //-------------------------------------------------------------------------
  def getNaxisScaleZero() = {
    Seq((KEY_NAXIS,keyValueMap(KEY_NAXIS))
      , (KEY_NAXIS_1,keyValueMap(KEY_NAXIS_1).toInt)
      , (KEY_NAXIS_2,keyValueMap(KEY_NAXIS_2).toInt)
      , (KEY_BSCALE,keyValueMap(KEY_BSCALE).toDouble)
      , (KEY_BZERO,keyValueMap(KEY_BZERO).toDouble))
  }
  //-------------------------------------------------------------------------
  def getScaleZero() = {
    Seq((KEY_BSCALE,keyValueMap(KEY_BSCALE).toDouble)
       , (KEY_BZERO,keyValueMap(KEY_BZERO).toDouble))
  }
  //-------------------------------------------------------------------------
  def updateRecord(key: String, value: String) = keyValueMap(key) = value
  //-------------------------------------------------------------------------
  def updateRecord(keyValue: (String,String)) =
    keyValueMap(keyValue._1) = keyValue._2
  //---------------------------------------------------------------------------
  def removeKeySeq(keySeq: Seq[String], pattern: Option[Seq[Regex]] = None) = {
    //-------------------------------------------------------------------------
    def matchIgnoredPattern(k: String) : Boolean = {
      pattern.get.foreach { pattern =>
        val w = pattern.findFirstIn(k)
        if (w.isDefined && (w.get.length == k.length)) return true
      }
      false
    }
    //-------------------------------------------------------------------------
    val newMap = keyValueMap.flatMap { record =>
      val key = record._1
      if (keySeq.contains(key)) None
      else {
        if (pattern.isDefined) {
          if(matchIgnoredPattern(key)) None
          else Some(record)
        }
        else Some(record)
      }
    }
    keyValueMap.clear()
    keyValueMap ++= newMap
  }
  //---------------------------------------------------------------------------
  def removeWCS() = {
    removeKeySeq(WCS_IGNORE_KEY_SEQ, Some(WCS_IGNORE_KEY_PATTERN))
    removeKeySeq(WCS_IGNORE_KEY_SEQ, Some(AT_WCS_IGNORE_KEY_PATTERN))
  }
  //---------------------------------------------------------------------------
  def getFirstMatch(keySeq: Array[String]): Option[String] = {
    keySeq.foreach { key=>
      if (keyValueMap.contains(key)) Some(getStringValueOrEmptyNoQuotation(key))
    }
    None
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file SimpleFits.scala
//=============================================================================