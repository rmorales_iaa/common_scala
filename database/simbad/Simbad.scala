/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/Jun/2021
 * Time:  17h:20m
 * Description: None
 */
//=============================================================================
package com.common.database.simbad
//=============================================================================
import com.common.configuration.MyConf
import com.common.image.objectPosition.ObjectPosition
import com.common.logger.MyLogger
//=============================================================================
import scalaj.http.{Http, HttpResponse}
//=============================================================================
//=============================================================================
object Simbad extends MyLogger {
  //---------------------------------------------------------------------------
  private final val conf = MyConf(MyConf.c.getString("Database.simbadDB"))
  //---------------------------------------------------------------------------
  private val SIMBAD_HORIZONS_HTTP_REQUEST = conf.getString("Database.httpRequest")
  private val SIMBAD_CATALOG               = conf.getString("Database.catalog")
  private val SIMBAD_CATALOG_EPOCH         = conf.getDouble("Database.epoch")
  //---------------------------------------------------------------------------
  private val connectionTimeoutMs = 10 * 1000
  private val readTimeoutMs       = 30 * 1000
  //---------------------------------------------------------------------------
  private def httpRequest(url: String) : Option[String] = {
    val result: HttpResponse[String] = Http(url)
      .charset("UTF-8")
      .timeout(connectionTimeoutMs,readTimeoutMs)
      .asString
    if (result.code != 200) return {
      error(s"Error requesting url from JPL Horizons API. Error code: ${result.body}")
      error(s"url:$url")
      error(s"Error message:${result.headers}")
      None
    }
    if (result.headers.isEmpty) None
    else Some(result.body)
  }
  //---------------------------------------------------------------------------
  def getObjectPositionInfo(objectName: String): Option[ObjectPosition] = {

    val formattedObjectName = objectName
      .replaceAll("\\+","%2B")
    val url = s"""$SIMBAD_HORIZONS_HTTP_REQUEST/$SIMBAD_CATALOG&-c=$formattedObjectName&-out.max=1&-out=json&-out.add=_RAJ,_DEJ,pmRA,pmDE&-out.max=1&-c.eq=J2000"""
    info(s"Querying simbad: '$url'")
    val response = httpRequest(url).getOrElse(return None)

    val seq = response.split("\n")
    val pos = seq.indexOf("----------------- ----------------- --------- ---------")
    if (pos == -1 || pos < 30) return None
    val lineData = seq(pos+4).split(" ").filter( !_.isEmpty )
    if (lineData.size != 2 && lineData.size != 4) return None

    val ra = lineData(0).toDouble
    val dec = lineData(1).toDouble
    if (lineData.size == 2)  //no proper motion
      Some(ObjectPosition(
        ra
        , dec
        , 0
        , 0
        , SIMBAD_CATALOG_EPOCH))
    else
    Some(ObjectPosition(
      ra
    , dec
    , lineData(2).toDouble  //pm ra
    , lineData(3).toDouble  //pm dec
    , SIMBAD_CATALOG_EPOCH))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Simbad.scala
//=============================================================================
