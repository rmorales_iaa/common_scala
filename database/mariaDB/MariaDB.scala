/**
  * Created by: Rafael Morales (rmorales@iaa.es) 
  * Date:  28/Jan/2021 
  * Time:  15h:06m
  * Description: None
  */
//=============================================================================
package com.common.database.mariaDB
//=============================================================================
import com.common.configuration.MyConf
import com.common.logger.MyLogger
import scala.collection.mutable.ArrayBuffer
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import java.sql.{Connection, DriverManager, ResultSet, Statement}
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
case class ColumnInfo(name: String, typeName: String, size: Int )
//=============================================================================
case class MariaDB(databaseName: String, tableName: String) extends  MyLogger{
  //---------------------------------------------------------------------------
  private val user = MyConf.c.getString("User.name")
  private val password = MyConf.c.getString("User.password")
  private val urlSimple = MyConf.c.getString("Database.mariaDB.url") + databaseName
  private val urlComplete = urlSimple + s"?user=$user&password=$password"
  info(s"Connecting with MariaDB: url: '$urlSimple'")
  private val connection = connect
  //---------------------------------------------------------------------------
  private def connect()  = {
    var conn : Connection = null
    Class.forName("org.mariadb.jdbc.Driver")
    info(s"Trying to connect with MariaDB: url: '$urlSimple'")
    Try { conn = DriverManager.getConnection(urlComplete) } match {
      case Success(_) => info(s"Connected with MariaDB: url: '$urlSimple'")
      case Failure( e ) =>
        error( e.getMessage + s". Error connecting with MariaDB: url: '$urlSimple'")
        conn = null
    }
    conn
  }
  //---------------------------------------------------------------------------
  def close()  = {
    info(s"Closing MariaDB: url: '$urlSimple' database and table: '$databaseName.$tableName' and user: $user")
    Try { if (connection!= null) connection.close() } match {
      case Success(_) => info(s"Closed MariaDB: url: '$urlSimple'")
      case Failure( e ) => error( e.getMessage + s". Error closing MariaDB: url: '$urlSimple'")
    }
  }
  //---------------------------------------------------------------------------
  def getColumnNameSeq(r: ResultSet) = {
    val metaData = r.getMetaData
    (for(i<- 1 to metaData.getColumnCount) yield metaData.getColumnName(i)).toArray
  }
  //---------------------------------------------------------------------------
  def getColumnNameSeq() = {
    val metadata = connection.getMetaData
    val resultSet = metadata.getColumns(null, null, tableName, "%")
    val r =  ArrayBuffer[ColumnInfo]()
    while(resultSet.next)
      r += ColumnInfo(resultSet.getString("COLUMN_NAME")
                    , resultSet.getString("TYPE_NAME")
                    , resultSet.getInt("COLUMN_SIZE"))
    r
  }
  //---------------------------------------------------------------------------
  def queryWithResultAsCsv(sql: String
                           , csvFilename: String
                           , pathFieldName: Option[String]
                           , fieldDivider : String = "\t"
                          ): Array[String] = {
    val pathSeq = ArrayBuffer[String]()
    val resultSet = getResultSet(sql)
    var rowCount = 0
    if (resultSet != null) {
      val bw = new BufferedWriter(new FileWriter(new File(csvFilename)))
      Try {
        val colNameSeq = getColumnNameSeq(resultSet)
        val colCount = colNameSeq.length
        bw.write(getColumnNameSeq(resultSet).mkString("#",fieldDivider,"\n"))
        while(resultSet.next) {
          rowCount += 1
          if (pathFieldName.isDefined) pathSeq += resultSet.getString(pathFieldName.get)
          for(i<- 1 to colCount){
            val o = resultSet.getObject(i)
            if (o  == null) bw.write(fieldDivider)
            else bw.write(resultSet.getObject(i).toString + fieldDivider)
          }
          bw.write("\n")
        }
      }
      match {
        case Success(_) =>
          resultSet.close
          bw.close()
          info(s"Database has returned: $rowCount rows for the query")
        case Failure(e) =>
          error(e.getMessage + s" Error executing sql '$sql' in MariaDB: url: '$urlSimple'")
          resultSet.close
          bw.close()
      }
    }
    pathSeq.toArray
  }
  //---------------------------------------------------------------------------
  //please remember to close 'resultset' after using it
  private def getResultSet(sql: String) = {
    var statement: Statement = null
    var resultSet: ResultSet = null
    Try {
      statement = connection.createStatement
      resultSet = statement.executeQuery(sql)
    }
    match {
      case Success(_) => statement.close
      case Failure(e) =>
        error(e.getMessage + s" Error executing sql '$sql' in MariaDB: url: '$urlSimple'")
        statement.close
    }
    resultSet
  }
  //---------------------------------------------------------------------------
  def insert(sql: String) = executeUpdateSentence(sql)
  //---------------------------------------------------------------------------
  def createTable(sql: String) = executeUpdateSentence(sql)
  //---------------------------------------------------------------------------
  private def executeUpdateSentence(sql: String): Boolean = {
    var statement: Statement = null
    Try {
      statement = connection.createStatement
      statement.executeUpdate(sql)
    } match {
      case Success(_) =>
        statement.close
        true
      case Failure(e) =>
        error(e.getMessage + s" Error executing sql '$sql' in MariaDB: url: '$urlSimple'")
        statement.close
        false
    }
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file MariaDB.scala
//=============================================================================
