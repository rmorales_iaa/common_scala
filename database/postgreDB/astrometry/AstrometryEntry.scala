//=============================================================================
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Aug/2023
 * Time:  21h:20m
 * Description: None
 */
package com.common.database.postgreDB.astrometry
//=============================================================================
import com.common.logger.MyLogger
import scalasql._
//=============================================================================
object AstrometryEntry extends MyLogger {
  //---------------------------------------------------------------------------
  object images extends Table[AstrometryEntry]() //the name the variable must match with the table at the database
  //---------------------------------------------------------------------------
}
//=============================================================================
case class AstrometryEntry[T[_]](fileName: T[String]
                                  , wcsFit: T[String]
                                  , raMin: T[Double]
                                  , raMax: T[Double]
                                  , decMin: T[Double]
                                  , decMax: T[Double]
                                  , fovX: T[Float] // in arco min
                                  , fovY: T[Float] // in arco min
                                  , pixScaleX: T[Float] // in arcosec/pixel
                                  , pixScaleY: T[Float] // in arcosec/pixel
                                )
//=============================================================================
//End of file AstrometryEntry.scala
//=============================================================================