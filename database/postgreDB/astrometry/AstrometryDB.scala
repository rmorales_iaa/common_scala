/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  04/Nov/2021
 * Time:  18h:22m
 * Description: None
 */
//=============================================================================
package com.common.database.postgreDB.astrometry
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.postgreDB.PostgreScalaDB
import com.common.database.postgreDB.astrometry.AstrometryEntry.images
import com.common.fits.simpleFits.SimpleFits._
import com.common.hardware.cpu.CPU
import com.common.image.myImage.MyImage
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
//=============================================================================
import scalasql._
import scalasql.dialects.PostgresDialect._

import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}
//=============================================================================
object AstrometryDB {
  //---------------------------------------------------------------------------
  final val COL_NAME_FILENAME       = "fileName"
  final val COL_NAME_RA_MIN         = "raMin"
  final val COL_NAME_RA_MAX         = "raMax"
  final val COL_NAME_DEC_MIN        = "decMin"
  final val COL_NAME_DEC_MAX        = "decMax"
  final val COL_NAME_FOV_X          = "fovX" // in arco min
  final val COL_NAME_FOV_Y          = "fovY" // in arco min
  final val COL_NAME_PIX_SCALE_X    = "pixScaleX" // in arcosec/pixel
  final val COL_NAME_PIX_SCALE_Y    = "pixScaleY" // in arcosec/pixel
  //---------------------------------------------------------------------------
  final val COL_NAME_SEQ  = Array(
      COL_NAME_FILENAME
    , COL_NAME_RA_MIN
    , COL_NAME_RA_MAX
    , "observing_date"
    , "telescope"
    , "filter"
    , "instrument"
    , "exposure_time"
    , "xMaxPix"
    , "yMaxPix"
    , "raFov"
    , "decFov"
    , "pixScaleX"
    , "pixScaleY"
    )
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.common.database.postgreDB.astrometry.AstrometryDB._
case class AstrometryDB(inputDir: String = ""
                        , rootPath: Option[String] = None
                        , recursive: Boolean = true)  {
  //---------------------------------------------------------------------------
  private val conf =  MyConf(MyConf.c.getString("Database.astrometry"))
  private val postgreDB = PostgreScalaDB(MyConf.c.getString("Database.postgre.url")
                                         , conf.getString("Database.name"))
  //---------------------------------------------------------------------------
  checkTable()
  //---------------------------------------------------------------------------
  private val fitsExtension = MyConf.c.getStringSeq("Common.fitsFileExtension")
  private val coreCount: Int = CPU.getCoreCount()
  //---------------------------------------------------------------------------
  private def checkTable() =
    postgreDB.executeSQL_script(conf.getString("Database.postgre.createSchemaSQL"))
  //---------------------------------------------------------------------------
  private def createIndex() = {
    info("Creating indexes")
    postgreDB.executeSQL_script(conf.getString("Database.postgre.createIndexSQL"))
  }
  //---------------------------------------------------------------------------
  def process(): Unit = {
    info(s"Parsing directory and subdirectories of '$inputDir'")
    processDir()
  }
  //---------------------------------------------------------------------------
  private def processImage(imageName: String): Unit = {
    val img = MyImage(imageName)
    if (img == null) return
    val fits = img.getSimpleFits()

    if (!fits.hasWcs()) error(s"Image: '$imageName' has no WCS")
    else {
      val pos = img.getRaDecRelevantPos()
      val fileName = if (rootPath.isDefined) rootPath.get +  "/" + img.getRawNameWithExtension() else imageName

      insertIntoDatabase(AstrometryEntry[Sc](
          fileName
        , fits.getStringValueOrEmptyNoQuotation(KEY_M2_WCS_FIT)
        , pos(0)
         ,pos(1)
        , pos(2)
        , pos(3)
        , fits.getStringValueOrEmptyNoQuotation(KEY_OM_FOV_X).toFloat
        , fits.getStringValueOrEmptyNoQuotation(KEY_OM_FOV_Y).toFloat
        , fits.getStringValueOrEmptyNoQuotation(KEY_OM_PIX_SCALE_X).toFloat
        , fits.getStringValueOrEmptyNoQuotation(KEY_OM_PIX_SCALE_Y).toFloat
      ))
    }
  }
  //-------------------------------------------------------------------------
  private def insertIntoDatabase(entry: AstrometryEntry[Sc]) = {
    if (exists(entry.fileName)) drop(entry.fileName)
    val query = images.insert.values(entry)
    postgreDB.runQuery(query)
  }
  //---------------------------------------------------------------------------
  private def exists(name:String) = {
    val query = images.select.filter( _.fileName === name )
    postgreDB.runQuery(query).size > 0
  }
  //---------------------------------------------------------------------------
  private def drop(name:String) = {
    val query = images.delete(_.fileName === name )
    postgreDB.delete(query)
  }
  //---------------------------------------------------------------------------
  private def processDir() = {
    //-------------------------------------------------------------------------
    class MyParallelImageSeq(seq: Array[String]) extends ParallelTask[String](
      seq
      , coreCount
      , isItemProcessingThreadSafe = true) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(imageName: String) =
        Try {
          processImage(imageName)
        }
        match {
          case Success(_) =>
          case Failure(e) =>
            error(e.getMessage + s" Error cataloging the image '$imageName' continuing with the next image")
            error(e.getStackTrace.mkString("\n"))
        }
      //-----------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    if(!Path.directoryExist(inputDir)) error(s"Input directory '$inputDir' does not exist")
    else {
      val fitsSeq = ArrayBuffer[String]()
      if (recursive) {
        //calculate all subdirs
        val currentDir = Path.ensureEndWithFileSeparator(inputDir) +  "."
        val subDirToProcess = Path.getSubDirectoryList(inputDir).map(_.getAbsolutePath)
        fitsSeq ++= Path.getSortedFileList(currentDir,fitsExtension) map (_.getAbsolutePath)
        new MyParallelImageSeq(fitsSeq.toArray)
        fitsSeq.clear()
        subDirToProcess.foreach { subDirName =>
          info(s"Parsing directory: '$subDirName'")
          fitsSeq ++= Path.getSortedFileListRecursive(subDirName, fitsExtension).map (_.getAbsolutePath)
          new MyParallelImageSeq(fitsSeq.toArray)
          fitsSeq.clear()
        }
        fitsSeq.clear()
      }
      else {
        fitsSeq ++= Path.getSortedFileList(inputDir, fitsExtension).map (_.getAbsolutePath)
        new MyParallelImageSeq(fitsSeq.toArray)
      }

      createIndex()
    }
  }
  //---------------------------------------------------------------------------
  def getAstrometryEntrySeq(ra: Double
                            , dec: Double) = {
    val query = images.select.filter(astrometryEntry=>
      astrometryEntry.raMin >= ra  &&
      astrometryEntry.raMax <= ra  &&
      astrometryEntry.decMin >= dec &&
      astrometryEntry.decMax <= dec
    )
    postgreDB.runQuery(query)
  }
  //---------------------------------------------------------------------------
  def getAllAstrometryEntrySeq() = {
    val query = images.select.filter(_ => true)
    postgreDB.runQuery(query)
  }
  //---------------------------------------------------------------------------
  def close() = postgreDB.close
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file AstrometryDB.scala
//=============================================================================
