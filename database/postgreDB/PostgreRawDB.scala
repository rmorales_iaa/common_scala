/**
  * Created by: Rafael Morales (rmorales@iaa.es) 
  * Date:  05/Feb/2021 
  * Time:  10h:00m
  * Description: None
  */
//=============================================================================
package com.common.database.postgreDB
//=============================================================================
import com.common.configuration.MyConf
import com.common.logger.MyLogger
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import java.sql.{Connection, DriverManager, ResultSet, Statement}
import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}
//=============================================================================
case class ColumnInfo(name: String, typeName: String, size: Int )
//=============================================================================
case class PostgreRawDB(databaseName: String = "NONE"
                     , tableName: String = "NONE"
                     , userURL: Option[String] = None) extends  MyLogger {
  //-----------------------------------------------------------"----------------
  private val user = if (userURL.isDefined) "" else MyConf.c.getString("Database.name")
  private val password = if (userURL.isDefined) "" else MyConf.c.getString("Database.password")
  private val urlSimple = if (userURL.isDefined) "" else MyConf.c.getString("Database.url") + databaseName
  private val urlComplete = userURL.getOrElse(urlSimple + s"?user=$user&password=$password")
  private val connection = connect()
  //---------------------------------------------------------------------------
  def isConnected() = connection != null
  //---------------------------------------------------------------------------
  private def connect()  = {
    var conn : Connection = null
    Class.forName("org.postgresql.Driver")
    if (!urlSimple.isEmpty)info(s"Trying to connect with PostgreRawDB: url: '$urlSimple'")
    Try { conn = DriverManager.getConnection(urlComplete) } match {
      case Success(_) =>
        info(s"Connected with PostgreRawDB")
      case Failure( e ) =>
        error( e.getMessage + s". Error connecting with PostgreRawDB: url: '$urlComplete'")
        error(e.getStackTrace.mkString("\n"))
        conn = null
    }
    conn
  }
  //---------------------------------------------------------------------------
  def close()  = {
    info(s"Closing PostgreRawDB")
    Try { if (connection!= null) connection.close() } match {
      case Success(_) =>
        if (!urlSimple.isEmpty) info(s"Closed PostgreRawDB: url: '$urlSimple'")
      case Failure( e ) =>
        if (!urlSimple.isEmpty)  error( e.getMessage + s". Error closing PostgreRawDB: url: '$urlSimple'")
        error(e.getStackTrace.mkString("\n"))
    }
  }
  //---------------------------------------------------------------------------
  def getColumnNameSeq(r: ResultSet) = {
    val metaData = r.getMetaData
    (for(i<- 1 to metaData.getColumnCount) yield metaData.getColumnName(i)).toArray
  }
  //---------------------------------------------------------------------------
  def getColumnNameSeqAsMap(r: ResultSet) = {
    val metaData = r.getMetaData
    (for(i<- 1 to metaData.getColumnCount) yield metaData.getColumnName(i)-> i).toMap
  }
  //---------------------------------------------------------------------------
  def getColumnNameSeq(tName: String = tableName) = {
    val metadata = connection.getMetaData
    val resultSet = metadata.getColumns(databaseName, null, tName, "%")
    val r =  ArrayBuffer[ColumnInfo]()
    while(resultSet.next)
      r += ColumnInfo(resultSet.getString("COLUMN_NAME")
        , resultSet.getString("TYPE_NAME")
        , resultSet.getInt("COLUMN_SIZE"))
    r
  }
  //---------------------------------------------------------------------------
  def queryWithResultAsCsv(sql: String
                           , csvFilename: String
                           , pathFieldName: Option[String]
                           , fieldDivider : String = "\t"
                          ): Array[String] = {
    val pathSeq = ArrayBuffer[String]()
    val resultSet = executeSentence(sql)
    var rowCount = 0
    if (resultSet != null) {
      val bw = new BufferedWriter(new FileWriter(new File(csvFilename)))
      Try {
        val colNameSeq = getColumnNameSeq(resultSet)
        val colCount = colNameSeq.length
        bw.write(getColumnNameSeq(resultSet).mkString("#",fieldDivider,"\n"))
        while(resultSet.next) {
          rowCount += 1
          if (pathFieldName.isDefined) pathSeq += resultSet.getString(pathFieldName.get)
          for(i<- 1 to colCount){
            val o = resultSet.getObject(i)
            if (o  == null) bw.write(fieldDivider)
            else bw.write(resultSet.getObject(i).toString + fieldDivider)
          }
          bw.write("\n")
        }
      }
      match {
        case Success(_) =>
          resultSet.close
          bw.close()
          info(s"Database has returned: $rowCount rows for the query")
        case Failure(e) =>
          error(e.getMessage + s". Error executing sql '$sql' in PostgreRawDB: url: '$urlSimple'")
          error(e.getStackTrace.mkString("\n"))
          resultSet.close
          bw.close()
      }
    }
    pathSeq.toArray
  }
  //---------------------------------------------------------------------------
  def dropTable(tableName: String) = {
    val sql = s"""DROP TABLE IF EXISTS "$tableName""""
    executeUpdateSentence(sql)
  }
  //---------------------------------------------------------------------------
  def truncate(tableName: String) = {
    val sql = s"""TRUNCATE TABLE "$tableName""""
    executeUpdateSentence(sql)
  }
  //---------------------------------------------------------------------------
  def select(sql: String) = executeSentence(sql)
  //---------------------------------------------------------------------------
  def insert(sql: String) = executeUpdateSentence(sql)
  //---------------------------------------------------------------------------
  def update(sql: String) = executeUpdateSentence(sql)
  //---------------------------------------------------------------------------
  def createTable(sql: String) = executeUpdateSentence(sql)
  //---------------------------------------------------------------------------
  def createTableAs(tName:String, select: String) = {
    val sql = s"""CREATE table $tName as \n""" + select
    createTable(sql)
  }
  //---------------------------------------------------------------------------
  def exist(sql: String) = {
    var exist = false
    val resultSet = executeSentence(sql)

    Try { exist = resultSet.next }
    match {
      case Success(_) =>
        resultSet.close
        exist
      case Failure(e) =>
        error(e.getMessage + s" Error executing sql '$sql' in PostgreRawDB: url: '$urlSimple'")
        error(e.getStackTrace.mkString("\n"))
        resultSet.close
        exist
    }
  }
  //---------------------------------------------------------------------------
  def count(sql: String) = {
    var count = -1
    val resultSet= executeSentence(sql)
    Try {var conn : Connection = null
    Class.forName("org.postgresql.Driver")
    info(s"Trying to connect with PostgreRawDB: url: '$urlSimple'")
    Try { conn = DriverManager.getConnection(urlComplete) } match {
      case Success(_) => info(s"Connected with PostgreRawDB: url: '$urlSimple'")
      case Failure( e ) =>
        error( e.getMessage + s". Error connecting with PostgreRawDB: url: '$urlSimple'")
        conn = null
    }
      while(resultSet.next) {  count = resultSet.getInt(1) }
    }
    match {
      case Success(_) =>
        resultSet.close
        count
      case Failure(e) =>
        error(e.getMessage + s" Error executing sql '$sql' in PostgreRawDB: url: '$urlSimple'")
        error(e.getStackTrace.mkString("\n"))
        resultSet.close
        count
    }
  }
  //---------------------------------------------------------------------------
  private def executeUpdateSentence(sql: String): Boolean = {
    var statement: Statement = null
    Try {
      statement = connection.createStatement
      statement.executeUpdate(sql)
    } match {
    case Success(_) =>
      statement.close
      true
    case Failure(e) =>
      error(e.getMessage + s" Error executing sql '$sql' in PostgreRawDB: url: '$urlSimple'")
      error(e.getStackTrace.mkString("\n"))
      statement.close
      false
    }
  }
  //---------------------------------------------------------------------------
  //please remember to close 'resultset' after using it
  private def executeSentence(sql: String) = {
    var statement: Statement = null
    var resultSet: ResultSet = null
    Try {
      statement = connection.createStatement
      resultSet = statement.executeQuery(sql)
    }
    match {
      case Success(_) =>
      case Failure(e) =>
        error(e.getMessage + s" Error executing sql '$sql' in PostgreRawDB: url: '$urlSimple'")
        error(e.getStackTrace.mkString("\n"))
        if (statement != null) statement.close
    }
    resultSet
  }
  //---------------------------------------------------------------------------
  def existDatabase(dbName: String) : Boolean = {
    val sql = s"SELECT datname " +
    "FROM pg_database " +
    s"WHERE lower(datname) = lower('$dbName')"
    exist(sql)
  }
  //---------------------------------------------------------------------------
  def existTable(dName: String, tName: String) : Boolean = {
    val sql = s"""SELECT 1
                 |	FROM information_schema.tables
                 |	WHERE table_catalog= '$dName'
                 |	   AND table_name= '$tName'
                 |	""".stripMargin
    exist(sql)
  }
  //---------------------------------------------------------------------------
  def createDatabase(dbName: String) : Boolean = {
    val sql = s"""CREATE DATABASE "$dbName" OWNER '$user'"""
    executeUpdateSentence(sql)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file PostgreRawDB.scala
//=============================================================================
