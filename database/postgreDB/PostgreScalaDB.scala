/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  13/Sep/2024
 * Time:  11h:06m
 * Description: https://books.underscore.io/essential-slick/
 */
package com.common.database.postgreDB
//=============================================================================
import com.common.logger.MyLogger
import scalasql.PostgresDialect._
import scalasql.core.SqlStr.SqlStringSyntax
import scalasql.core.{Config, DbClient}
import scalasql.query.{Delete, InsertValues, Select}
import scala.reflect.ClassTag
//=============================================================================
//=============================================================================
object PostgreScalaDB {
  //---------------------------------------------------------------------------
  def getUrl(urlTemplate:String, databaseName: String) = {

    val s = urlTemplate.replace("$DATABASE", databaseName)
    s
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.common.database.postgreDB.PostgreScalaDB._
case class PostgreScalaDB(urlTemplate: String
                         , databaseName: String) extends MyLogger {
  //---------------------------------------------------------------------------
  private val dbClient = new DbClient.Connection(
    java.sql.DriverManager
      .getConnection(getUrl(urlTemplate, databaseName), "", ""),
    new Config {
      override def nameMapper(v: String) = v.toLowerCase()
    }
  )
  //---------------------------------------------------------------------------
  private val db = dbClient.getAutoCommitClientConnection
  //---------------------------------------------------------------------------
  def executeSQL_script(sqlScriptName: String, verbose: Boolean = false) = {
    if (verbose) info(s"Executing Postgre SQL:'$sqlScriptName'")
    val sqlScript = scala.io.Source.fromFile(sqlScriptName).mkString("")
    dbClient.transaction { _db =>
      synchronized(_db.updateRaw(sqlScript))
    }
  }
  //---------------------------------------------------------------------------
  def delete[Q](query: Delete[Q]): Int = {
    dbClient.transaction { _db =>
      synchronized(_db.run(query))
    }
  }
  //---------------------------------------------------------------------------
  def runQuery[V[_[_]], R](query: InsertValues[V, R]): Int = {
    dbClient.transaction { _db =>
     synchronized(_db.run(query))
    }
  }
  //---------------------------------------------------------------------------
  def runQuery[Q,R](query:Select[Q, R])
                   (implicit ct: ClassTag[R]): Array[R] = {
    dbClient.transaction { _db =>
      synchronized(_db.run(query))
    }.toArray
  }
  //---------------------------------------------------------------------------
  def renderQuery[Q,R](query:Select[Q, R])=
    println(dbClient.renderSql(query))
  //---------------------------------------------------------------------------
  def createIndex(tableName: String
                  , colNameSeq:Array[String]
                  , unique: Boolean = true) {

    val indexType = if (unique) "UNIQUE" else ""

    val indexName = colNameSeq match {
      case Array(colName) => s"${colName}_idx"
      case _ => s"${colNameSeq.mkString("_")}_idx"
    }

    val query = colNameSeq.length match {
      case 1 =>
        s"CREATE $indexType INDEX CONCURRENTLY $indexName ON $tableName (${colNameSeq.head});"
      case _ =>
        s"CREATE $indexType INDEX CONCURRENTLY $indexName ON $tableName (${colNameSeq.mkString(",")});"
    }

    synchronized(db.updateRaw(query))
  }
  //---------------------------------------------------------------------------
  def rowCount(tableName: String) = {
    dbClient.transaction { _db =>
      synchronized(
      _db.runSql[Long](
        sql"select count(1) from gaia_source_minimal;"
      ).head)
    }
  }
  //---------------------------------------------------------------------------
  def close = db.close()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file PostgreScalaDB.scala
//=============================================================================