/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  21h:08m
 * Description: https://gea.esac.esa.int/archive/documentation/GDR3/Gaia_archive/chap_datamodel/sec_dm_main_source_catalogue/ssec_dm_gaia_source.html
 */
//=============================================================================
package com.common.database.postgreDB.gaia_dr_3
//=============================================================================
import com.common.coordinate.conversion.Conversion
import com.common.csv.CsvFile._
import com.common.geometry.point.Point2D_Double
import com.common.logger.MyLogger
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.bson.conversions.Bson
import org.mongodb.scala.model.Projections.{excludeId, fields, include}
//=============================================================================
import org.mongodb.scala.Document

import java.io.BufferedWriter
import scala.language.existentials
//=============================================================================
//=============================================================================
object Source extends MyLogger {
  //---------------------------------------------------------------------------
  val REFERENCE_EPOCH = 2016.0d //extract from original column 'ref_epoch'
  //---------------------------------------------------------------------------
  val codecRegistry = fromRegistries(fromProviders(classOf[Source]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  final val GAIA_SOURCE_COL_SEQ  = Seq(
    "solution_id"
  , "designation"
  , "source_id"
  , "random_index"
  , "ref_epoch"
  , "ra"
  , "ra_error"
  , "dec"
  , "dec_error"
  , "parallax"
  , "parallax_error"
  , "parallax_over_error"
  , "pm"
  , "pmra"
  , "pmra_error"
  , "pmdec"
  , "pmdec_error"
  , "ra_dec_corr"
  , "ra_parallax_corr"
  , "ra_pmra_corr"
  , "ra_pmdec_corr"
  , "dec_parallax_corr"
  , "dec_pmra_corr"
  , "dec_pmdec_corr"
  , "parallax_pmra_corr"
  , "parallax_pmdec_corr"
  , "pmra_pmdec_corr"
  , "astrometric_n_obs_al"
  , "astrometric_n_obs_ac"
  , "astrometric_n_good_obs_al"
  , "astrometric_n_bad_obs_al"
  , "astrometric_gof_al"
  , "astrometric_chi2_al"
  , "astrometric_excess_noise"
  , "astrometric_excess_noise_sig"
  , "astrometric_params_solved"
  , "astrometric_primary_flag"
  , "nu_eff_used_in_astrometry"
  , "pseudocolour"
  , "pseudocolour_error"
  , "ra_pseudocolour_corr"
  , "dec_pseudocolour_corr"
  , "parallax_pseudocolour_corr"
  , "pmra_pseudocolour_corr"
  , "pmdec_pseudocolour_corr"
  , "astrometric_matched_transits"
  , "visibility_periods_used"
  , "astrometric_sigma5d_max"
  , "matched_transits"
  , "new_matched_transits"
  , "matched_transits_removed"
  , "ipd_gof_harmonic_amplitude"
  , "ipd_gof_harmonic_phase"
  , "ipd_frac_muti_peak"
  , "ipd_frac_odd_win"
  , "ruwe"
  , "scan_direction_strength_k1"
  , "scan_direction_strength_k2"
  , "scan_direction_strength_k3"
  , "scan_direction_strength_k4"
  , "scan_direction_mean_k1"
  , "scan_direction_mean_k2"
  , "scan_direction_mean_k3"
  , "scan_direction_mean_k4"
  , "duplicated_source"
  , "phot_g_n_obs"
  , "phot_g_mean_flux"
  , "phot_g_mean_flux_error"
  , "phot_g_mean_flux_over_error"
  , "phot_g_mean_mag"
  , "phot_bp_n_obs"
  , "phot_bp_mean_flux"
  , "phot_bp_mean_flux_error"
  , "phot_bp_mean_flux_over_error"
  , "phot_bp_mean_mag"
  , "phot_rp_n_obs"
  , "phot_rp_mean_flux"
  , "phot_rp_mean_flux_error"
  , "phot_rp_mean_flux_over_error"
  , "phot_rp_mean_mag"
  , "phot_bp_rp_excess_factor"
  , "phot_bp_n_contaminated_transits"
  , "phot_bp_n_blended_transits"
  , "phot_rp_n_contaminated_transits"
  , "phot_rp_n_blended_transits"
  , "phot_proc_mode"
  , "bp_rp"
  , "bp_g"
  , "g_rp"
  , "radial_velocity"
  , "radial_velocity_error"
  , "rv_method_used"
  , "rv_nb_transits"
  , "rv_nb_deblended_transits"
  , "rv_visibility_periods_used"
  , "rv_expected_sig_to_noise"
  , "rv_renormalised_gof"
  , "rv_chisq_pvalue"
  , "rv_time_duration"
  , "rv_amplitude_robust"
  , "rv_template_teff"
  , "rv_template_logg"
  , "rv_template_fe_h"
  , "rv_atm_param_origin"
  , "vbroad"
  , "vbroad_error"
  , "vbroad_nb_transits"
  , "grvs_mag"
  , "grvs_mag_error"
  , "grvs_mag_nb_transits"
  , "rvs_spec_sig_to_noise"
  , "phot_variable_flag"
  , "l"
  , "b"
  , "ecl_lon"
  , "ecl_lat"
  , "in_qso_candidates"
  , "in_galaxy_candidates"
  , "non_single_star"
  , "has_xp_continuous"
  , "has_xp_sampled"
  , "has_rvs"
  , "has_epoch_photometry"
  , "has_epoch_rv"
  , "has_mcmc_gspphot"
  , "has_mcmc_mc"
  , "in_andromeda_survey"
  , "classprob_dsc_combmod_quasar"
  , "classprob_dsc_combmod_galaxy"
  , "classprob_dsc_combmod_star"
  , "teff_gspphot"
  , "teff_gspphot_lower"
  , "teff_gspphot_upper"
  , "logg_gspphot"
  , "logg_gspphot_lower"
  , "logg_gspphot_upper"
  , "mh_gspphot"
  , "mh_gspphot_lower"
  , "mh_gspphot_upper"
  , "distance_gspphot"
  , "distance_gspphot_lower"
  , "distance_gspphot_upper"
  , "azero_gspphot"
  , "azero_gspphot_lower"
  , "azero_gspphot_upper"
  , "ag_gspphot"
  , "ag_gspphot_lower"
  , "ag_gspphot_upper"
  , "ebpminrp_gspphot"
  , "ebpminrp_gspphot_lower"
  , "ebpminrp_gspphot_upper"
  , "libname_gspphot"
  )
  //---------------------------------------------------------------------------
  final val GAIA_PHOTOMETRY_COL_SEQ = Seq(
    "_id"
    , "ra"
    , "ra_error"
    , "dec"
    , "dec_error"
    , "parallax"
    , "parallax_error"
    , "parallax_over_error"
    , "pmra"
    , "pmra_error"
    , "pmdec"
    , "pmdec_error"
    , "ruwe"
    , "phot_g_mean_mag"
    , "phot_bp_mean_mag"
    , "phot_rp_mean_mag"
    , "radial_velocity"
    , "radial_velocity_error"
  )
  //---------------------------------------------------------------------------
  final val GAIA_CSV_PHOTOMETRY_HEADER = GAIA_PHOTOMETRY_COL_SEQ map ("gaia_" + _)
  //---------------------------------------------------------------------------
  def getDocument(line: String) = {
    val r = line.split(",")
    var index = -1
    //-------------------------------------------------------------------------
    def getNewIndex = {
      index += 1
      index
    }
    //-------------------------------------------------------------------------
    Source(
      getLongOrDefault(r(getNewIndex))     //solution_id
      , r(getNewIndex)                      //designation
      , getLongOrDefault(r(getNewIndex))    //source_id
      , getLongOrDefault(r(getNewIndex))    //random_index
      , getDoubleOrDefault(r(getNewIndex))  //ref_epoch

      , getDoubleOrDefault(r(getNewIndex))  //ra
      , getFloatOrDefault(r(getNewIndex))   //ra_error
      , getDoubleOrDefault(r(getNewIndex))  //dec
      , getFloatOrDefault(r(getNewIndex))   //dec_error

      , getDoubleOrDefault(r(getNewIndex))  //parallax
      , getFloatOrDefault(r(getNewIndex))   //parallax_error
      , getFloatOrDefault(r(getNewIndex))   //parallax_over_error

      , getFloatOrDefault(r(getNewIndex))   //pm
      , getDoubleOrDefault(r(getNewIndex))  //pmra
      , getFloatOrDefault(r(getNewIndex))   //pmra_error
      , getDoubleOrDefault(r(getNewIndex))  //pmdec
      , getFloatOrDefault(r(getNewIndex))   //pmdec_error

      , getFloatOrDefault(r(getNewIndex))   //ra_dec_corr

      , getFloatOrDefault(r(getNewIndex))   //ra_parallax_corr
      , getFloatOrDefault(r(getNewIndex))   //ra_pmra_corr
      , getFloatOrDefault(r(getNewIndex))   //ra_pmdec_corr

      , getFloatOrDefault(r(getNewIndex))   //dec_parallax_corr
      , getFloatOrDefault(r(getNewIndex))   //dec_pmra_corr
      , getFloatOrDefault(r(getNewIndex))   //dec_pmdec_corr

      , getFloatOrDefault(r(getNewIndex))   //parallax_pmra_corr
      , getFloatOrDefault(r(getNewIndex))   //parallax_pmdec_corr
      , getFloatOrDefault(r(getNewIndex))   //pmra_pmdec_corr

      , getShortOrDefault(r(getNewIndex))   //astrometric_n_obs_al
      , getShortOrDefault(r(getNewIndex))   //astrometric_n_obs_ac
      , getShortOrDefault(r(getNewIndex))   //astrometric_n_good_obs_al
      , getShortOrDefault(r(getNewIndex))   //astrometric_n_bad_obs_al
      , getFloatOrDefault(r(getNewIndex))   //astrometric_gof_al
      , getFloatOrDefault(r(getNewIndex))   //astrometric_chi2_al
      , getFloatOrDefault(r(getNewIndex))   //astrometric_excess_noise
      , getFloatOrDefault(r(getNewIndex))   //astrometric_excess_noise_si
      , getByteOrDefault(r(getNewIndex))    //astrometric_params_solved
      , getBooleanOrDefault(r(getNewIndex)) //astrometric_primary_flag


      , getFloatOrDefault(r(getNewIndex))   //nu_eff_used_in_astrometry
      , getFloatOrDefault(r(getNewIndex))   //pseudocolour
      , getFloatOrDefault(r(getNewIndex))   //pseudocolour_error
      , getFloatOrDefault(r(getNewIndex))   //ra_pseudocolour_corr
      , getFloatOrDefault(r(getNewIndex))   //dec_pseudocolour_corr
      , getFloatOrDefault(r(getNewIndex))   //parallax_pseudocolour_corr
      , getFloatOrDefault(r(getNewIndex))   //pmra_pseudocolour_corr
      , getFloatOrDefault(r(getNewIndex))   //pmdec_pseudocolour_corr

      , getShortOrDefault(r(getNewIndex))   //astrometric_matched_transits
      , getShortOrDefault(r(getNewIndex))   //visibility_periods_used
      , getFloatOrDefault(r(getNewIndex))   //astrometric_sigma5d_max
      , getShortOrDefault(r(getNewIndex))   //matched_transits
      , getShortOrDefault(r(getNewIndex))   //new_matched_transits
      , getShortOrDefault(r(getNewIndex))   //matched_transits_removed

      , getFloatOrDefault(r(getNewIndex))   //ipd_gof_harmonic_amplitude
      , getFloatOrDefault(r(getNewIndex))   //ipd_gof_harmonic_phase
      , getByteOrDefault(r(getNewIndex))    //ipd_frac_muti_peak
      , getByteOrDefault(r(getNewIndex))    //ipd_frac_odd_win

      , getFloatOrDefault(r(getNewIndex))   //ruwe

      , getFloatOrDefault(r(getNewIndex))   //scan_direction_strength_k1
      , getFloatOrDefault(r(getNewIndex))   //scan_direction_strength_k2
      , getFloatOrDefault(r(getNewIndex))   //scan_direction_strength_k3
      , getFloatOrDefault(r(getNewIndex))   //scan_direction_strength_k4
      , getFloatOrDefault(r(getNewIndex))   //scan_direction_mean_k1
      , getFloatOrDefault(r(getNewIndex))   //scan_direction_mean_k2
      , getFloatOrDefault(r(getNewIndex))   //scan_direction_mean_k3
      , getFloatOrDefault(r(getNewIndex))   //scan_direction_mean_k4

      , getBooleanOrDefault(r(getNewIndex)) //duplicated_source

      , getShortOrDefault(r(getNewIndex))   //phot_g_n_obs
      , getDoubleOrDefault(r(getNewIndex))  //phot_g_mean_flux
      , getFloatOrDefault(r(getNewIndex))   //phot_g_mean_flux_error
      , getFloatOrDefault(r(getNewIndex))   //phot_g_mean_flux_over_error
      , getFloatOrDefault(r(getNewIndex))   //phot_g_mean_mag

      , getShortOrDefault(r(getNewIndex))   //phot_bp_n_obs
      , getDoubleOrDefault(r(getNewIndex))  //phot_bp_mean_flux
      , getFloatOrDefault(r(getNewIndex))   //phot_bp_mean_flux_error
      , getFloatOrDefault(r(getNewIndex))   //phot_bp_mean_flux_over_error
      , getFloatOrDefault(r(getNewIndex))   //phot_bp_mean_mag

      , getShortOrDefault(r(getNewIndex))   //phot_rp_n_obs
      , getDoubleOrDefault(r(getNewIndex))  //phot_rp_mean_flux
      , getFloatOrDefault(r(getNewIndex))   //phot_rp_mean_flux_error
      , getFloatOrDefault(r(getNewIndex))   //phot_rp_mean_flux_over_error
      , getFloatOrDefault(r(getNewIndex))   //phot_rp_mean_mag

      , getFloatOrDefault(r(getNewIndex))   //phot_bp_rp_excess_factor
      , getShortOrDefault(r(getNewIndex))   //phot_bp_n_contaminated_transits
      , getShortOrDefault(r(getNewIndex))   //phot_bp_n_blended_transits

      , getShortOrDefault(r(getNewIndex))   //phot_rp_n_contaminated_transits
      , getShortOrDefault(r(getNewIndex))   //phot_rp_n_blended_transits

      , getByteOrDefault(r(getNewIndex))    //phot_proc_mode

      , getFloatOrDefault(r(getNewIndex))   //bp_rp
      , getFloatOrDefault(r(getNewIndex))   //bp_g
      , getFloatOrDefault(r(getNewIndex))   //g_rp

      , getFloatOrDefault(r(getNewIndex))   //radial_velocity
      , getFloatOrDefault(r(getNewIndex))   //radial_velocity_error
      , getByteOrDefault(r(getNewIndex))    //rv_method_used
      , getShortOrDefault(r(getNewIndex))   //rv_nb_transits
      , getShortOrDefault(r(getNewIndex))   //rv_nb_deblended_transits
      , getShortOrDefault(r(getNewIndex))   //rv_visibility_periods_used
      , getFloatOrDefault(r(getNewIndex))   //rv_expected_sig_to_noise
      , getFloatOrDefault(r(getNewIndex))   //rv_renormalised_gof
      , getFloatOrDefault(r(getNewIndex))   //rv_chisq_pvalue
      , getFloatOrDefault(r(getNewIndex))   //rv_time_duration
      , getFloatOrDefault(r(getNewIndex))   //rv_amplitude_robust
      , getFloatOrDefault(r(getNewIndex))   //rv_template_teff
      , getFloatOrDefault(r(getNewIndex))   //rv_template_logg
      , getFloatOrDefault(r(getNewIndex))   //rv_template_fe_h
      , getShortOrDefault(r(getNewIndex))   //rv_atm_param_origin

      , getFloatOrDefault(r(getNewIndex))   //vbroad
      , getFloatOrDefault(r(getNewIndex))   //vbroad_error
      , getShortOrDefault(r(getNewIndex))   //vbroad_nb_transits

      , getFloatOrDefault(r(getNewIndex))   //grvs_mag
      , getFloatOrDefault(r(getNewIndex))   //grvs_mag_error
      , getShortOrDefault(r(getNewIndex))   //grvs_mag_nb_transits

      , getFloatOrDefault(r(getNewIndex))   //rvs_spec_sig_to_noise

      , r(getNewIndex)                      //phot_variable_flag

      , getDoubleOrDefault(r(getNewIndex))  //l
      , getDoubleOrDefault(r(getNewIndex))  //b

      , getDoubleOrDefault(r(getNewIndex))  //ecl_lon
      , getDoubleOrDefault(r(getNewIndex))  //ecl_lat

      , getBooleanOrDefault(r(getNewIndex)) //in_qso_candidates
      , getBooleanOrDefault(r(getNewIndex)) //in_galaxy_candidates

      , getShortOrDefault(r(getNewIndex))   //non_single_star

      , getBooleanOrDefault(r(getNewIndex)) //has_xp_continuous
      , getBooleanOrDefault(r(getNewIndex)) //has_xp_sampled
      , getBooleanOrDefault(r(getNewIndex)) //has_rvs
      , getBooleanOrDefault(r(getNewIndex)) //has_epoch_photometry
      , getBooleanOrDefault(r(getNewIndex)) //has_epoch_rv
      , getBooleanOrDefault(r(getNewIndex)) //has_mcmc_gspphot
      , getBooleanOrDefault(r(getNewIndex)) //has_mcmc_mc
      , getBooleanOrDefault(r(getNewIndex)) //in_andromeda_survey

      , getFloatOrDefault(r(getNewIndex))   //classprob_dsc_combmod_quasar
      , getFloatOrDefault(r(getNewIndex))   //classprob_dsc_combmod_galaxy
      , getFloatOrDefault(r(getNewIndex))   //classprob_dsc_combmod_star

      , getFloatOrDefault(r(getNewIndex))   //teff_gspphot
      , getFloatOrDefault(r(getNewIndex))   //teff_gspphot_lower
      , getFloatOrDefault(r(getNewIndex))   //teff_gspphot_upper

      , getFloatOrDefault(r(getNewIndex))   //logg_gspphot
      , getFloatOrDefault(r(getNewIndex))   //logg_gspphot_lower
      , getFloatOrDefault(r(getNewIndex))   //logg_gspphot_upper

      , getFloatOrDefault(r(getNewIndex))   //mh_gspphot
      , getFloatOrDefault(r(getNewIndex))   //mh_gspphot_lower
      , getFloatOrDefault(r(getNewIndex))   //mh_gspphot_upper

      , getFloatOrDefault(r(getNewIndex))   //distance_gspphot
      , getFloatOrDefault(r(getNewIndex))   //distance_gspphot_lower
      , getFloatOrDefault(r(getNewIndex))   //distance_gspphot_upper

      , getFloatOrDefault(r(getNewIndex))   //azero_gspphot
      , getFloatOrDefault(r(getNewIndex))   //azero_gspphot_lower
      , getFloatOrDefault(r(getNewIndex))   //azero_gspphot_upper

      , getFloatOrDefault(r(getNewIndex))   //ag_gspphot
      , getFloatOrDefault(r(getNewIndex))   //ag_gspphot_lower
      , getFloatOrDefault(r(getNewIndex))   //ag_gspphot_upper

      , getFloatOrDefault(r(getNewIndex))   //ebpminrp_gspphot
      , getFloatOrDefault(r(getNewIndex))   //ebpminrp_gspphot_lower
      , getFloatOrDefault(r(getNewIndex))   //ebpminrp_gspphot_upper

      , r(getNewIndex)                      //libname_gspphot
    )
  }
  //---------------------------------------------------------------------------
  def writePhotometricInfo(doc: Document, bw: BufferedWriter, sep: String = "\t") = {
    bw.write(doc("_id").asInt64().getValue + sep)
    bw.write(doc("ra").asDouble.getValue + sep)
    bw.write(doc("ra_error").asDouble.getValue + sep)
    bw.write(doc("dec").asDouble.getValue + sep)
    bw.write(doc("dec_error").asDouble.getValue + sep)
    bw.write(doc("parallax").asDouble.getValue + sep)
    bw.write(doc("parallax_error").asDouble.getValue + sep)
    bw.write(doc("parallax_over_error").asDouble.getValue + sep)
    bw.write(doc("pmra").asDouble.getValue + sep)
    bw.write(doc("pmra_error").asDouble.getValue + sep)
    bw.write(doc("pmdec").asDouble.getValue + sep)
    bw.write(doc("pmdec_error").asDouble.getValue + sep)
    bw.write(doc("ruwe").asDouble.getValue + sep)
    bw.write(doc("phot_g_mean_mag").asDouble.getValue + sep)
    bw.write(doc("phot_bp_mean_mag").asDouble.getValue + sep)
    bw.write(doc("phot_rp_mean_mag").asDouble.getValue + sep)
    bw.write(doc("radial_velocity").asDouble.getValue + sep)
    bw.write(doc("radial_velocity_error").asDouble.getValue + sep)
  }
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String="") =
    classOf[Source].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  def getColNameValueSeq(prefix: String = "") =
    classOf[Source].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = (prefix + f.getName, f.getType)
      f.setAccessible(false)
      res
    }
  //-------------------------------------------------------------------------
  def getProjectionColList(colList:Seq[String] = Seq()): Bson = {
    if (colList.contains("_id")) include(colList: _*)
    else fields(include(colList: _*), excludeId())
  }
  //-------------------------------------------------------------------------
  def getInfo(doc: Document) = {
    val id = doc("_id").asInt64.getValue
    val ra = doc("ra").asDouble.getValue
    val dec = doc("dec").asDouble.getValue
    val r = Conversion.DD_to_HMS(ra)
    val d = Conversion.DD_to_DMS(dec)
    s"\n\t'GaiaDR3 3' ID                   : $id" +
      s"\n\t'GaiaDR3 3' (ra,dec)             : $ra $dec" +
      s"\n\t'GaiaDR3 3' (ra,dec)             : ${r._1 + ":" + r._2 + ":" + r._3} ${d._1 + ":" + d._2 + ":" + d._3} "
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Source(solution_id:                      Long
                  , designation:                    String
                  , _id:                            Long //original name: 'source_id'
                  , random_index:                   Long
                  , ref_epoch:                      Double

                  , ra:                             Double
                  , ra_error:                       Float
                  , dec:                            Double
                  , dec_error:                      Float

                  , parallax:                       Double
                  , parallax_error:                 Float
                  , parallax_over_error:            Float

                  , pm:                             Float
                  , pmra:                           Double
                  , pmra_error:                     Float
                  , pmdec:                          Double
                  , pmdec_error:                    Float

                  , ra_dec_corr:                    Float

                  , ra_parallax_corr:               Float
                  , ra_pmra_corr:                   Float
                  , ra_pmdec_corr:                  Float

                  , dec_parallax_corr:              Float
                  , dec_pmra_corr:                  Float
                  , dec_pmdec_corr:                 Float

                  , parallax_pmra_corr:             Float
                  , parallax_pmdec_corr:            Float
                  , pmra_pmdec_corr:                Float

                  , astrometric_n_obs_al:           Short
                  , astrometric_n_obs_ac:           Short
                  , astrometric_n_good_obs_al:      Short
                  , astrometric_n_bad_obs_al:       Short
                  , astrometric_gof_al:             Float
                  , astrometric_chi2_al:            Float
                  , astrometric_excess_noise:       Float
                  , astrometric_excess_noise_sig:   Float
                  , astrometric_params_solved:      Byte
                  , astrometric_primary_flag:       Boolean

                  , nu_eff_used_in_astrometry:      Float

                  , pseudocolour:                   Float
                  , pseudocolour_error:             Float
                  , ra_pseudocolour_corr:           Float
                  , dec_pseudocolour_corr:          Float
                  , parallax_pseudocolour_corr:     Float
                  , pmra_pseudocolour_corr:         Float
                  , pmdec_pseudocolour_corr:        Float

                  , astrometric_matched_transits:   Short
                  , visibility_periods_used:        Short
                  , astrometric_sigma5d_max:        Float
                  , matched_transits:               Short
                  , new_matched_transits:           Short
                  , matched_transits_removed:       Short

                  , ipd_gof_harmonic_amplitude:     Float
                  , ipd_gof_harmonic_phase:         Float
                  , ipd_frac_muti_peak:             Byte
                  , ipd_frac_odd_win:               Byte

                  , ruwe:                           Float

                  , scan_direction_strength_k1:     Float
                  , scan_direction_strength_k2:     Float
                  , scan_direction_strength_k3:     Float
                  , scan_direction_strength_k4:     Float
                  , scan_direction_mean_k1:         Float
                  , scan_direction_mean_k2:         Float
                  , scan_direction_mean_k3:         Float
                  , scan_direction_mean_k4:         Float

                  , duplicated_source:              Boolean

                  , phot_g_n_obs:                   Short
                  , phot_g_mean_flux:               Double
                  , phot_g_mean_flux_error:         Float
                  , phot_g_mean_flux_over_error:    Float
                  , phot_g_mean_mag:                Float

                  , phot_bp_n_obs:                  Short
                  , phot_bp_mean_flux:              Double
                  , phot_bp_mean_flux_error:        Float
                  , phot_bp_mean_flux_over_error:   Float
                  , phot_bp_mean_mag:               Float

                  , phot_rp_n_obs:                  Short
                  , phot_rp_mean_flux:              Double
                  , phot_rp_mean_flux_error:        Float
                  , phot_rp_mean_flux_over_error:   Float
                  , phot_rp_mean_mag:               Float

                  , phot_bp_rp_excess_factor:       Float
                  , phot_bp_n_contaminated_transits:Short
                  , phot_bp_n_blended_transits:     Short

                  , phot_rp_n_contaminated_transits:Short
                  , phot_rp_n_blended_transits:     Short

                  , phot_proc_mode:                 Byte

                  , bp_rp:                          Float
                  , bp_g:                           Float
                  , g_rp:                           Float

                  , radial_velocity:                Float
                  , radial_velocity_error:          Float
                  , rv_method_used:                 Byte
                  , rv_nb_transits:                 Short
                  , rv_nb_deblended_transits:       Short
                  , rv_visibility_periods_used:     Short
                  , rv_expected_sig_to_noise:       Float
                  , rv_renormalised_gof:            Float
                  , rv_chisq_pvalue:                Float
                  , rv_time_duration:               Float
                  , rv_amplitude_robust:            Float
                  , rv_template_teff:               Float
                  , rv_template_logg:               Float
                  , rv_template_fe_h:               Float
                  , rv_atm_param_origin:            Short

                  , vbroad:                         Float
                  , vbroad_error:                   Float
                  , vbroad_nb_transits:             Short

                  , grvs_mag:                       Float
                  , grvs_mag_error:                 Float
                  , grvs_mag_nb_transits:           Short

                  , rvs_spec_sig_to_noise:          Float

                  , phot_variable_flag:             String

                  , l:                              Double
                  , b:                              Double

                  , ecl_lon:                        Double
                  , ecl_lat:                        Double

                  , in_qso_candidates:              Boolean
                  , in_galaxy_candidates:           Boolean

                  , non_single_star:                Short

                  , has_xp_continuous:              Boolean
                  , has_xp_sampled:                 Boolean
                  , has_rvs:                        Boolean
                  , has_epoch_photometry:           Boolean
                  , has_epoch_rv:                   Boolean
                  , has_mcmc_gspphot:               Boolean
                  , has_mcmc_mc:                    Boolean
                  , in_andromeda_survey:            Boolean

                  , classprob_dsc_combmod_quasar:   Float
                  , classprob_dsc_combmod_galaxy:   Float
                  , classprob_dsc_combmod_star:     Float

                  , teff_gspphot:                   Float
                  , teff_gspphot_lower:             Float
                  , teff_gspphot_upper:             Float

                  , logg_gspphot:                   Float
                  , logg_gspphot_lower:             Float
                  , logg_gspphot_upper:             Float

                  , mh_gspphot:                     Float
                  , mh_gspphot_lower:               Float
                  , mh_gspphot_upper:               Float

                  , distance_gspphot:               Float
                  , distance_gspphot_lower:         Float
                  , distance_gspphot_upper:         Float

                  , azero_gspphot:                  Float
                  , azero_gspphot_lower:            Float
                  , azero_gspphot_upper:            Float

                  , ag_gspphot:                     Float
                  , ag_gspphot_lower:               Float
                  , ag_gspphot_upper:               Float

                  , ebpminrp_gspphot:               Float
                  , ebpminrp_gspphot_lower:         Float
                  , ebpminrp_gspphot_upper:         Float

                  , libname_gspphot:                String
               )  {
  //---------------------------------------------------------------------------
  def getRaDec = Point2D_Double(ra,dec)
  //---------------------------------------------------------------------------
  def getRaDecPM = Point2D_Double(pmra,pmdec)
  //---------------------------------------------------------------------------
  def getRaDecWithPM(observingDate: String) =
    Conversion.properMotion(
      observingDate
      , raDec = getRaDec
      , ref_epoch
      , _properMotion = getRaDecPM
    )
  //---------------------------------------------------------------------------
  def getDistanceTo(raDec:Point2D_Double, raDecObservingDate:String) = {
    val gaiaPosUpdated = getRaDecWithPM(raDecObservingDate)
    val cosDecGaiaUpdated =  Math.cos(Math.toRadians(gaiaPosUpdated.y))
    val diff = (raDec - gaiaPosUpdated).abs()
    val difCosDec = Point2D_Double(diff.x * cosDecGaiaUpdated,diff.y)
    val diffSquared = difCosDec * difCosDec
    Math.sqrt(diffSquared.x + diffSquared.y)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Source.scala
//=============================================================================
