/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  21h:08m
 * Description: None
 */
//=============================================================================
package com.common.database.postgreDB.gaia_dr_3
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
import scalasql._
//=============================================================================
//=============================================================================
object SourceReduced extends MyLogger {
  //---------------------------------------------------------------------------
  final val tableName = "gaia_source_reduced"
  //---------------------------------------------------------------------------
  object gaia_source_reduced extends Table[SourceReduced]() //the name the variable must match with the table at the database
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String="") =
    classOf[SourceReduced[Sc]].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  final val COL_DEFINITION_POS_SEQ = getColNameSeq().map { colName=>
    Source.GAIA_SOURCE_COL_SEQ.indexOf(colName)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class SourceReduced[T[_]](source_id:                        T[Long]

                                , parallax_over_error:            T[Float]

                                , astrometric_n_obs_al:           T[Short]
                                , astrometric_chi2_al:            T[Float]

                                , phot_g_mean_flux:               T[Double]
                                , phot_g_mean_flux_error:         T[Float]
                                , phot_g_mean_flux_over_error:    T[Float]

                                , phot_bp_mean_flux:              T[Double]
                                , phot_bp_mean_flux_error:        T[Float]
                                , phot_bp_mean_flux_over_error:   T[Float]

                                , phot_rp_mean_flux:              T[Double]
                                , phot_rp_mean_flux_error:        T[Float]
                                , phot_rp_mean_flux_over_error:   T[Float]

                                , phot_bp_n_contaminated_transits:T[Short]
                                , phot_bp_n_blended_transits:     T[Short]

                                , phot_rp_n_contaminated_transits:T[Short]
                                , phot_rp_n_blended_transits:     T[Short]

                                , phot_proc_mode:                 T[Byte]

                                , bp_rp:                          T[Float]
                                , bp_g:                           T[Float]
                                , g_rp:                           T[Float]

                                , radial_velocity:                T[Float]
                                , radial_velocity_error:          T[Float]
                              )
//=============================================================================

//=============================================================================
//=============================================================================
//End of file Source.scala
//=============================================================================
