/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Sep/2024
 * Time:  11h:42m
 * Description: https://gea.esac.esa.int/archive/documentation/GDR3/Gaia_archive/chap_datamodel/sec_dm_main_source_catalogue/ssec_dm_gaia_source.html
 */
package com.common.database.postgreDB.gaia_dr_3
//=============================================================================
import com.common.database.postgreDB.PostgreScalaDB
import com.common.database.postgreDB.gaia_dr_3.SourceReduced.gaia_source_reduced
import com.common.logger.MyLogger
import scalasql._
import scalasql.dialects.PostgresDialect._

import java.io.{BufferedReader, File, FileReader}
import java.util.concurrent.ConcurrentLinkedQueue
import scala.jdk.CollectionConverters.CollectionHasAsScala
//=============================================================================
//=============================================================================
object SourceMinimal extends MyLogger {
  //---------------------------------------------------------------------------
  final val tableName = "gaia_source_minimal"
  //---------------------------------------------------------------------------
  final val RA_COL_NAME          = "ra"
  final val DEC_COL_NAME         = "dec"
  final val PARALLAX_COL_NAME    = "parallax"
  //---------------------------------------------------------------------------
  object gaia_source_minimal extends Table[SourceMinimal]() //the name the variable must match with the table at the database
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String="") =
   classOf[SourceMinimal[Sc]].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  private final val COL_DEFINITION_POS_SEQ = getColNameSeq().map { colName=>
    Source.GAIA_SOURCE_COL_SEQ.indexOf(colName)
  }
  //---------------------------------------------------------------------------
  private def getByte(s: String) =  {
    if (s.isEmpty || s == "null") Byte.MinValue
    else s.toByte
  }
  //---------------------------------------------------------------------------
  private def getShort(s: String) =  {
    if (s.isEmpty || s == "null") Short.MinValue
    else s.toShort
  }
  //---------------------------------------------------------------------------
  private def getFloat(s: String) =  {
    if (s.isEmpty || s == "null") Float.NaN
    else s.toFloat
  }
  //---------------------------------------------------------------------------
  private def getDouble(s: String) =  {
    if (s.isEmpty || s == "null") Double.NaN
    else s.toDouble
  }
  //---------------------------------------------------------------------------
  def processFile(postgreDB: PostgreScalaDB
                   , path: String
                   , maxBufferedDocument: Int): Boolean = {
    val br = new BufferedReader(new FileReader(new File(path)))
    val sourceMinimalBuffer = new ConcurrentLinkedQueue[SourceMinimal[Sc]]()
    val sourceReducedBuffer = new ConcurrentLinkedQueue[SourceReduced[Sc]]()
    var line = br.readLine()
    //-------------------------------------------------------------------------
    def insert() = {
      val queryMinimal = gaia_source_minimal.insert.values(sourceMinimalBuffer.asScala.toArray:_*)

      postgreDB.runQuery(queryMinimal)
      sourceMinimalBuffer.clear()

      val queryReduced = gaia_source_reduced.insert.values(sourceReducedBuffer.asScala.toArray:_*)
      postgreDB.runQuery(queryReduced)
      sourceReducedBuffer.clear()
    }
    //-------------------------------------------------------------------------
    line = br.readLine() //avoid header
    while(line != null &&  line.length()!=0 ) {
      if (!line.isEmpty && !line.startsWith("#") && !line.startsWith("solution_id")) {


        val itemSeq = line.split(",")
        var index = -1
        sourceMinimalBuffer.add(SourceMinimal[Sc](
            {index += 1; itemSeq(COL_DEFINITION_POS_SEQ(index)).toLong}

          , {index += 1; getDouble(itemSeq(COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getFloat(itemSeq(COL_DEFINITION_POS_SEQ(index)))}

          , {index += 1; getDouble(itemSeq(COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getFloat(itemSeq(COL_DEFINITION_POS_SEQ(index)))}

          , {index += 1; getDouble(itemSeq(COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getFloat(itemSeq(COL_DEFINITION_POS_SEQ(index)))}

          , {index += 1; getFloat(itemSeq(COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getFloat(itemSeq(COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getFloat(itemSeq(COL_DEFINITION_POS_SEQ(index)))}

          , {index += 1; getFloat(itemSeq(COL_DEFINITION_POS_SEQ(index)))}

          , {index += 1; getFloat(itemSeq(COL_DEFINITION_POS_SEQ(index)))}

          , {index += 1; getDouble(itemSeq(COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getFloat(itemSeq(COL_DEFINITION_POS_SEQ(index)))}

          , {index += 1; getDouble(itemSeq(COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getFloat(itemSeq(COL_DEFINITION_POS_SEQ(index)))}
        ))


        index = -1
        sourceReducedBuffer.add(SourceReduced[Sc](
            {index += 1; itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)).toLong}

          , {index += 1; getFloat(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}

          , {index += 1; getShort(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getFloat(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}

          , {index += 1; getDouble(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getFloat(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getFloat(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}

          , {index += 1; getDouble(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getFloat(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getFloat(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}

          , {index += 1; getDouble(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getFloat(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getFloat(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}

          , {index += 1; getShort(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getShort(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}

          , {index += 1; getShort(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getShort(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}

          , {index += 1; getByte(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}

          , {index += 1; getFloat(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getFloat(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getFloat(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}

          , {index += 1; getFloat(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}
          , {index += 1; getFloat(itemSeq(SourceReduced.COL_DEFINITION_POS_SEQ(index)))}
        ))
        if (sourceMinimalBuffer.size >= maxBufferedDocument) insert()
      }
      line = br.readLine()
    }
    br.close()

    //store the remain buffered docs
    if (sourceMinimalBuffer.size > 0) insert()
    true
  }
  //---------------------------------------------------------------------------
  def getSourceSeqAtSkyArea(minRa: Double
                            , maxRa: Double
                            , minDec: Double
                            , maxDec: Double) =
    gaia_source_minimal.select.filter( source=>
      source.ra >= minRa  &&
      source.ra <= maxRa  &&
      source.dec >= minDec &&
      source.dec <= maxDec
    )
  //---------------------------------------------------------------------------
}
//=============================================================================
case class SourceMinimal[T[_]](
     source_id:    T[Long]

   , ra:          T[Double]
   , ra_error:    T[Float]

   , dec:         T[Double]
   , dec_error:   T[Float]

   , parallax:           T[Double]
   , parallax_error:     T[Float]

   , phot_g_mean_mag:    T[Float]
   , phot_bp_mean_mag:   T[Float]
   , phot_rp_mean_mag:   T[Float]

   , phot_bp_rp_excess_factor: T[Float]

   , ruwe:        T[Float]

   , pmra:        T[Double]
   , pmra_error:  T[Float]

   , pmdec:       T[Double]
   , pmdec_error: T[Float]
 )
//=============================================================================
//End of file SourceMinimal.scala
//=============================================================================