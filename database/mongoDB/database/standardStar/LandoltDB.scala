/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Jun/2024
 * Time:  12h:09m
 * Description: None
 */
package com.common.database.mongoDB.database.standadStar
//=============================================================================
import com.common.database.mongoDB.Helpers.DocumentObservable
import com.common.database.mongoDB.MongoDB
import com.common.geometry.point.Point2D_Double
import com.mongodb.client.model.CollationStrength
import org.mongodb.scala.model.Collation
import org.mongodb.scala.model.Filters.{equal}
//=============================================================================
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
//=============================================================================
object LandoltDB {
  //---------------------------------------------------------------------------
  private val NAME_COL_NAME  = "name"
  private val RA_COL_NAME    = "ra"
  private val DEC_COL_NAME   = "dec"
  private val mongoProjectionList = MongoDB.getProjectionColList(Seq(NAME_COL_NAME
                                                                     , RA_COL_NAME
                                                                     , DEC_COL_NAME))
  //---------------------------------------------------------------------------
  private val collationCaseInsensitive = Collation
    .builder()
    .locale("en")
    .collationStrength(CollationStrength.PRIMARY).build
  //---------------------------------------------------------------------------
  private case class Landolt_2009_DB(databaseName: String, collectionName: String) extends MongoDB {
    //---------------------------------------------------------------------------
    private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) =
       initWithCodeRegister(Landolt_2009_Source.codecRegistry)
    val client: MongoClient = r._1
    val database: MongoDatabase = r._2
    val collection: MongoCollection[Document] = r._3
    //---------------------------------------------------------------------------
    connected = true
    //---------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private case class Landolt_2013_DB(databaseName: String, collectionName: String) extends MongoDB {
    //---------------------------------------------------------------------------
    private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) =
      initWithCodeRegister(Landolt_2013_Source.codecRegistry)
    val client: MongoClient = r._1
    val database: MongoDatabase = r._2
    val collection: MongoCollection[Document] = r._3
    //---------------------------------------------------------------------------
    connected = true
    //---------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
import LandoltDB._
case class LandoltDB() {
  //---------------------------------------------------------------------------
  private val landolt_2009_DB = Landolt_2009_DB("paper","landolt_2009_table_2_5")
  private val landolt_2013_DB = Landolt_2013_DB("paper","landolt_2013_table_2_3_5")
  //---------------------------------------------------------------------------
  private def getRaDecSeq(name: String) = {
    val s = name.trim.toLowerCase
    val docSeq_2009 = landolt_2009_DB.collection
      .find(equal("name", s))
      .collation(collationCaseInsensitive)
      .projection(mongoProjectionList)
      .getResultDocumentSeq

    val docSeq_2013  = landolt_2013_DB.collection
        .find(equal("name", s))
        .collation(collationCaseInsensitive)
        .projection(mongoProjectionList)
        .getResultDocumentSeq

    val r_2009 =
      (docSeq_2009 map { doc =>
        ("LANDOLT_2009:'" + doc(NAME_COL_NAME).asString().getValue + "'"
        , Point2D_Double(doc(RA_COL_NAME).asDouble().getValue
                       , doc(DEC_COL_NAME).asDouble().getValue))
      }).toArray

    val r_2013 =
      (docSeq_2013 map { doc =>
        ("LANDOLT_2013:'" + doc(NAME_COL_NAME) + "'"
        , Point2D_Double(doc(RA_COL_NAME).asDouble().getValue
                      , doc(DEC_COL_NAME).asDouble().getValue))
      }).toArray

    r_2009 ++ r_2013
  }
  //---------------------------------------------------------------------------
  def getStandardStar(name: String
                      , fovMinRa: Double
                      , fovMaxRa: Double
                      , fovMinDec: Double
                      , fovMaxDec: Double): Option[String] = {

    val raDecSeq = getRaDecSeq(name)
    if (!raDecSeq.isEmpty)
      raDecSeq.foreach { case (starName, pos) =>
        if (pos.x >= fovMinRa && pos.x <= fovMaxRa &&
            pos.y >= fovMinDec && pos.y <= fovMaxDec)
          return Some(starName)
      }
    None
  }
  //---------------------------------------------------------------------------
  def close() = {
    landolt_2009_DB.close()
    landolt_2013_DB.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file LandoltDB.scala
//=============================================================================
