/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Jun/2024
 * Time:  12h:14m
 * Description: None
 */
package com.common.database.mongoDB.database.standadStar

//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import scala.language.existentials
//=============================================================================
//=============================================================================
object Landolt_2009_Source extends MyLogger {
  //---------------------------------------------------------------------------
  val codecRegistry = fromRegistries(fromProviders(classOf[Landolt_2009_Source]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
case class Landolt_2009_Source(name : String
                     , ra : Double
                     , dec : Double
                     , V : Double
                     , B_V : Double
                     , U_B : Double
                     , V_R: Double
                     , R_I: Double
                     , V_I: Double
                     , observings: Int
                     , nights: Int
                     , V_mean: Double
                     , B_V_mean: Double
                     , U_B_mean: Double
                     , V_R_mean: Double
                     , R_I_mean: Double
                     , V_I_mean: Double
                     , uca2: String
                     , twoMassID: String
                     , pmra: Double
                     , pmdec: Double
                     , pmRef: Int)
//=============================================================================
//End of file Landolt_2009_Source.scala
//=============================================================================