/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Jun/2024
 * Time:  12h:14m
 * Description: None
 */
package com.common.database.mongoDB.database.standadStar

//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import scala.language.existentials
//=============================================================================
//=============================================================================
object Landolt_2013_Source extends MyLogger {
  //---------------------------------------------------------------------------
  val codecRegistry = fromRegistries(fromProviders(classOf[Landolt_2013_Source]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
case class Landolt_2013_Source(name : String
                     , ra : Double
                     , dec : Double
                     , V : Double
                     , B_V : Double
                     , U_B : Double
                     , V_R: Double
                     , R_I: Double
                     , V_I: Double
                     , observings: Int
                     , nights: Int
                     , V_error: Double
                     , B_V_error: Double
                     , U_B_error: Double
                     , V_R_error: Double
                     , R_I_error: Double
                     , V_I_error: Double
                     , catalog: String
                     , twoMassID: String
                     , pmra: Double
                     , pmdec: Double)
//=============================================================================
//End of file Landolt_2013_Source.scala
//=============================================================================