/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2021
 * Time:  15h:06m
 * Description: Minor Planet Center Object
 */
//=============================================================================
package com.common.database.mongoDB.mpc
//=============================================================================
//=============================================================================
object MPO {  //minor planet object
  //---------------------------------------------------------------------------
  //known MPC ID
  final val MPC_ID_PLUTO  = 134340
  //---------------------------------------------------------------------------
  final val MPC_ID_CERES  = 1
  final val MPC_ID_PALLAS = 2
  final val MPC_ID_VESTA  = 4
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//https://minorplanetcenter.net/Extended_Files/Extended_MPCORB_Data_Format_Manual.pdf
//Table 4
//get the scheme and use it to generate the class. It can be reordered regarding the schema order
//pip3 install genson
//python3 /home/rafa/.local/bin/genson ./mpcorb_extended.json > mpcorb_extended_schema.json
case class MPO(Number: Option[String]                           //Number, if the asteroid has received one; this is the asteroid's permanent designation
               , Name: Option[String]                           //Name, if the asteroid has received one
               , H: Option[Float]                               //Absolute magnitude
               , G: Option[Float]                               //Slope parameter
               , Principal_desig: String                        //Principal provisional designation (if it exists)
               , Epoch: Float                                   //Epoch of the orbit (Julian Date)
               , M: Float                                       //Mean anomaly at the epoch (degrees)
               , Peri: Float                                    //Argument of perihelion, J2000.0 (degrees)
               , Node: Float                                    //Longitude of the ascending node, J2000.0 (degrees)
               , i: Float                                       //Inclination to the ecliptic, J2000.0 (degrees)
               , e: Float                                       //Orbital eccentricity
               , n: Float                                       //Mean daily motion, (degrees/day)
               , a: Float                                       //Semimajor axis, a (AU)
               , Ref: String                                    //Reference
               , Num_opps: Int                                  //Number of oppositions
               , Computer: String                               //Name of orbit computer
               , Hex_flags: String                              //4-hexdigit flags
               , Last_obs: String                               //Date of last observation included in orbit solution
               , Tp: Float
               , Orbital_period: Float                          //Orbital period (years)
               , Perihelion_dist: Float                         //Perihelion distance (AU)
               , Aphelion_dist: Float                           //Aphelion distance (AU)
               , Semilatus_rectum: Float                        //Semilatus rectum distance (AU)
               , Synodic_period: Float                          //Synodic period (years)
               , Orbit_type: String                             //orbit type
               , Num_obs: Option[Int]                           //Number of observations
               , rms: Option[Float]                             //r.m.s. residual
               , U: Option[String]                              //Uncertainty parameter
               , Arc_years: Option[String]                      //Only present for multi-opposition orbits
               , Perturbers: Option[String]                     //Coarse indicator of perturbers used in orbit computation
               , Perturbers_2: Option[String]                   //Precise indicator of perturbers used in orbit computation
               , Other_desigs: Option[List[String]]             //Other provisional designations (if they exist)
               , NEO_flag: Option[Int]                          //Value = 1 if flag raised, otherwise key
               , One_km_NEO_flag: Option[Int]                   //Value = 1 if flag raised, otherwise keyword is absent
               , PHA_flag: Option[Int]                          //Value = 1 if flag raised, otherwise keyword is absent
               , Critical_list_Floated_object_flag: Option[Int] //Value = 1 if flag raised, otherwise keyword is absent
               , One_opposition_obj: Option[Int]                //Value = 1 if flag raised, otherwise keyword is absent
               , Arc_length: Option[Int]                        //Only present for 1-opposition orbits (days)
              )
  //---------------------------------------------------------------------------
//=============================================================================
//End of file MPO.scala
//=============================================================================
