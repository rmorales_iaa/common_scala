/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2021
 * Time:  11h:18m
 * Description: Minor Planet Center Object Database
 */
//=============================================================================
package com.common.database.mongoDB.mpc
//=============================================================================
import com.common.database.mongoDB.Helpers._
import com.common.database.mongoDB.MongoDB
import com.common.orbitIntegrator.celestialBody.CelestialBody
import org.mongodb.scala.model.Filters.equal
//=============================================================================
import com.mongodb.client.model.CollationStrength
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.model.Collation
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
object MpoDB {
  //---------------------------------------------------------------------------
  //http://mongodb.github.io/mongo-scala-driver/2.3/getting-started/quick-tour-case-classes/
  //https://jannikarndt.de/blog/2017/08/writing_case_classes_to_mongodb_in_scala/
  val codecRegistry = fromRegistries(fromProviders(classOf[MPO]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  val collationCaseInsensitive = Collation
    .builder()
    .locale("en")
    .collationStrength(CollationStrength.PRIMARY).build
  //---------------------------------------------------------------------------
  def findByMpcID(id: String): Option[MPO] = {
    val db = MpoDB()
    val mpo = db.getByName(id)
    db.close()
    mpo
  }
  //---------------------------------------------------------------------------
  def findCelestialBodyByMpcID(id: Long): Option[CelestialBody] = {
    val db = MpoDB()
    val mpo = db.getByNumber(id)
    db.close()
    if (mpo.isDefined) Some(CelestialBody(mpo.get))
    else None
  }
  //--------------------------------------------------------------------------
}
//=============================================================================
import com.common.database.mongoDB.mpc.MpoDB._
case class MpoDB() extends MongoDB {
  //---------------------------------------------------------------------------
  val databaseName  = "mpo_catalog"
  val collectionName = "mpc"
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = initWithCodeRegister(codecRegistry)
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection =  r._3
  val collWithRegistry: MongoCollection[MPO] = database.getCollection(collectionName)
  //---------------------------------------------------------------------------
  connected = true

  //---------------------------------------------------------------------------
  def getAllIdSeq() =
    this.getAllDocuments(projectionFieldSeq = Seq("Number","Principal_desig")).flatMap {doc =>
      if (!doc("Number").isNull)
        Some(doc("Number").asString().getValue.toLong)
      else
        None
    }
  //---------------------------------------------------------------------------
  def getByName(name: String, verbose: Boolean = true): Option[MPO] = {
    val r = collWithRegistry
      .find(equal("Name", name))
      .collation(collationCaseInsensitive)
      .results()

    if (r.length == 0) {
      if (verbose) error(s"The mpo with name: '$name' does not exist")
      None
    }
    else Some(r.head)
  }

  //---------------------------------------------------------------------------
  def getByNumber(mpcID: Long, verbose: Boolean = true) = {
    val r = collWithRegistry
      .find(equal("Number", mpcID.toString))
      .results()

    if (r.length == 0) {
      if (verbose) error(s"The mpo with number: '$mpcID' does not exist")
      None
    }
    else Some(r.head)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MpcDB.scala
//=============================================================================
