/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Sep/2021
 * Time:  19h:12m
 * Description: None
 */
//=============================================================================
package com.common.database.mongoDB.database.carmencita
//=============================================================================
import com.common.database.mongoDB.Helpers.DocumentObservable
import com.common.database.mongoDB.MongoDB
import com.common.database.mongoDB.MongoDB.getProjectionColList
import com.common.image.objectPosition.ObjectPosition
import com.mongodb.client.model.CollationStrength
import org.mongodb.scala.model.Collation
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
import org.mongodb.scala.model.Filters.equal
//=============================================================================
object CarmencitaDB {
  //----------------------------------------------------------------------------
  private final val CARMENCITA_DB_NAME        = "carmencita"
  private final val CARMENCITA_COLLECTION_NAME= "m_stars"
  //----------------------------------------------------------------------------
  private final val CARMENCITA_DEFAULT_EPOCH    = 2016.0
  //----------------------------------------------------------------------------
  private final val CARMENCITA_NAME_COL_NAME    = "name"
  private final val CARMENCITA_RA_COL_NAME      = "ra_j2016_deg"
  private final val CARMENCITA_DEC_COL_NAME     = "de_j2016_deg"
  private final val CARMENCITA_PM_RA_COL_NAME   = "mura_masa1"
  private final val CARMENCITA_PM_DEC_COL_NAME  = "mude_masa1"

  private final val CARMENCITA_POSITION_COL_NAME_SEQ = Seq(
    CARMENCITA_NAME_COL_NAME
    , CARMENCITA_RA_COL_NAME
    , CARMENCITA_DEC_COL_NAME
    , CARMENCITA_PM_RA_COL_NAME
    , CARMENCITA_PM_DEC_COL_NAME
  )
  //----------------------------------------------------------------------------
  val collationCaseInsensitive = Collation
    .builder()
    .locale("en")
    .collationStrength(CollationStrength.PRIMARY).build
  //----------------------------------------------------------------------------
}
//=============================================================================
import CarmencitaDB._
case class CarmencitaDB(databaseName: String = CarmencitaDB.CARMENCITA_DB_NAME
                        , collectionName: String = CarmencitaDB.CARMENCITA_COLLECTION_NAME) extends MongoDB {

  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = initWithCodeRegister(CarmencitaSource.codecRegistry)
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  //---------------------------------------------------------------------------
  connected = true
  //---------------------------------------------------------------------------
  //(ra,dec,pmRa,pmDec)
  def findObjectPositionInfo(starName:String): Option[ObjectPosition] = {
    val docSeq = collection.find(equal("_id", starName))
      .projection(getProjectionColList(CARMENCITA_POSITION_COL_NAME_SEQ))
      .collation(collationCaseInsensitive)
      .getResultDocumentSeq
    if(docSeq.isEmpty) return None
    val doc = docSeq.head
    Some(
      ObjectPosition(
         doc(CARMENCITA_RA_COL_NAME).asDouble().getValue
       , doc(CARMENCITA_DEC_COL_NAME).asDouble().getValue
       , doc(CARMENCITA_PM_RA_COL_NAME).asDouble().getValue
       , doc(CARMENCITA_PM_DEC_COL_NAME).asDouble().getValue
       , CARMENCITA_DEFAULT_EPOCH
      ))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CarmencitaDB.scala
//=============================================================================
