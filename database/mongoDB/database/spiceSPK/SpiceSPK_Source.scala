/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  20h:59m
 * Description: http://cdsarc.u-strasbg.fr/ftp/J/A+A/546/A115/ReadMe
 */
//=============================================================================
package com.common.database.mongoDB.spiceSPK
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
import java.io.BufferedWriter
import java.time.LocalDateTime
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import scala.language.existentials
//=============================================================================
//=============================================================================
object SpiceSPK_Source extends MyLogger {
  //---------------------------------------------------------------------------
  val codecRegistry = fromRegistries(fromProviders(classOf[SpiceSPK_Source]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  private val ITEM_DIVIDER = "#"
  //---------------------------------------------------------------------------
}
//=============================================================================
import SpiceSPK_Source._
case class SpiceSPK_Source(_id: String //version
                           , spkID: Long
                           , mpcID: Long
                           , source: String
                           , version: String
                           , mainDesignation: String
                           , alternativeDesignation: String
                           , date: LocalDateTime  //storing date
                           , byteSize: Int
                           , byteSeq: Array[Byte]  //convertible to a spk file
                          ) {
  //---------------------------------------------------------------------------
  override def toString() = getUniqueID()
  //---------------------------------------------------------------------------
 def getUniqueID() = s"$spkID$ITEM_DIVIDER${_id}$ITEM_DIVIDER$mainDesignation"
    .trim
    .replaceAll(" +", " ")
    .replaceAll(" ", "_")
    .replaceAll("/", "_")
  //---------------------------------------------------------------------------
  def printReport() = {
    println(s"+++++++++++++ start of:'$mainDesignation' +++++++++++++")
    println(s"id:                      :'" + _id + "'")
    println(s"spkID:                   :'$spkID'")
    println(s"mpcID:                   :'$mpcID'")
    println(s"source:                  :'$source'")
    println(s"version:                 :'$version'")
    println(s"main designation:        :'$mainDesignation'")
    println(s"alternative designation: :'$alternativeDesignation'")
    println(s"storing date             :'$date'")
    println(s"byte size:               :'$byteSize'")
    println(s"------------- end of:'$mainDesignation' --------------")
  }

  //---------------------------------------------------------------------------
  def writeReport(bw: BufferedWriter
                  , orbitID: String
                  , imageCount: Long) = {
    bw.write(s"#++++++++++++ start of:'$mainDesignation' +++++++++++++\n")
    bw.write(s"main designation        \t'$mainDesignation'\n")
    bw.write(s"alternative designation \t'$alternativeDesignation'\n")
    bw.write(s"spkID                   \t'$spkID'\n")
    bw.write(s"mpcID                   \t'$mpcID'\n")
    bw.write(s"image count             \t'$imageCount'\n")
    bw.write(s"orbit seq               \t'$orbitID'\n")
    bw.write(s"#------------ end of:'$mainDesignation' --------------\n")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SpiceSPK_Source.scala
//=============================================================================
