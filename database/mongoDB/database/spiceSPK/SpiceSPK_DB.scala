/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Aug/2021
 * Time:  19h:04m
 * Description: Complete 2Mass database
 *
 */
//=============================================================================
package com.common.database.mongoDB.spiceSPK
//=============================================================================
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.{DocumentObservable, GenericObservable}
import com.common.database.mongoDB.MongoDB
import com.common.database.mongoDB.MongoDB.{MONGO_COL_ID, getProjectionColList}
import com.common.hardware.cpu.CPU
import com.common.jpl.horizons.Horizons
import com.common.jpl.horizons.db.smallBody.SmallBodyDB
import com.common.jpl.horizons.entry.HorizonsEntry
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
import com.common.util.util.Util
import org.mongodb.scala.model.Sorts._
//=============================================================================
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
import java.nio.file.{Files, Paths}
import java.time.LocalDateTime
import scala.util.{Failure, Success, Try}
import java.io.{File, FileOutputStream}
import scala.collection.mutable.ArrayBuffer
import java.io.BufferedWriter
//=============================================================================
object SpiceSPK_DB extends MyLogger {
  //---------------------------------------------------------------------------
  private val conf = MyConf(MyConf.c.getString("Database.spiceSpkDB"))
  //---------------------------------------------------------------------------
  private val COL_NAME_VERSION = "version"
  private val COL_NAME_DATE    = "date"
  //---------------------------------------------------------------------------
  private val DEFAULT_DATABASE_NAME = conf.getString("Database.name")
  private val DEFAULT_SPK_FILE_EXTENSION_SEQ = conf.getStringSeq("Database.fileExtensionSeq")
  val DEFAULT_BSP_FILE_EXTENSION = ".bsp"
  //---------------------------------------------------------------------------
  private final val SOURCE_VERSION_DIVIDER         = "#"
  //---------------------------------------------------------------------------
  private final val BSP_HEADER_BYTE_SIZE           = 5130
  private final val BSP_HEADER_CHAR_START_POS      = 3282
  private final val BSP_HEADER_VALID_CHAR_size     = 600
  private final val BSP_HEADER_SOURCE_MAGIC_TOKEN  = "source: "
  private final val BSP_HEADER_SOURCE_MAGIC_TOKEN_2="Horizons_SPK"
  private final val BSP_HEADER_VERSION_DIVIDER     = "#"
  private final val BSP_HEADER_SPK_ID_DIVIDER      = ":"
  private final val BSP_HEADER_SPK_ID_MAGIC_TOKEN  = "Coord"
  //---------------------------------------------------------------------------
  private final val BSP_HEADER_MPC_ID_MAGIC_TOKEN  = "Target body     :"
  //---------------------------------------------------------------------------
  private final val BSP_MISSING_ZERO_SPK_ID_PREFIX =  "20"
  //---------------------------------------------------------------------------
  private final val horizons = Horizons()
  //---------------------------------------------------------------------------
  private val yearRegex = """^\d{4}$""".r
  //---------------------------------------------------------------------------
  private def get(id: String): Option[HorizonsEntry] = {
    if (Util.isLong(id)) { //try by SPK ID or MPC ID
      val idAsLong = id.toLong
      var horizonsEntry = horizons.getBySpk_ID(idAsLong, verbose = false)
      if (horizonsEntry.isDefined) return horizonsEntry

      horizonsEntry = horizons.getByMPC_ID(idAsLong, verbose = false)
      if (horizonsEntry.isDefined) return horizonsEntry
    }
    else { //try by main and alternate designation
      var horizonsEntry = horizons.getByName(id, verbose = false)
      if (horizonsEntry.isDefined) return horizonsEntry

      horizonsEntry = horizons.getByAlternateName(id)
      if (horizonsEntry.isDefined) return horizonsEntry
    }
    None
  }
  //---------------------------------------------------------------------------
  private def startWithYear(text: String): Boolean = {
    if (text.length < 4) return false
    // Check if the first four characters are digits
    if (!yearRegex.findFirstIn(text.substring(0, 4)).isDefined) return false
    val year = text.substring(0, 4).toInt
    year >= 1900 && year <= 2100
  }
  //---------------------------------------------------------------------------
  def getSpkID(id:String): Option[Long] = {
    val r = get(id)
    if (r.isEmpty) {
      val r2 = get(id.replaceAll("_"," "))
      if (r2.isDefined) Some(r2.get.spkId)
      else {
        if (startWithYear(id)) {
          val r3 = get(id.take(4).trim + " " + id.drop(4).trim)
          if (r3.isDefined) Some(r3.get.spkId)
          else None
        }
        else {
          if (id.toLowerCase.contains("homdima")) getSpkID("2007 UK126") //avoid weird chars in "229762 G!kun||'homdima (2007 UK126)"
          else
            if (id.toLowerCase.contains("kagara")) getSpkID("20469705") //avoid weird chars in "|=Kagara" (2005 EF298)

          else None
        }
      }
    }
    else Some(r.get.spkId)
  }
  //---------------------------------------------------------------------------
  def report(id: String): Unit = {
    val spkID = getSpkID(id).getOrElse{error(s"Can not find a SPK ID using the id: '$id'");return}
    val db = SpiceSPK_DB(DEFAULT_DATABASE_NAME, spkID.toString)
    val dbItemSeq = db.findAll()
    dbItemSeq.foreach { dbItem => dbItem.printReport() }
    warning(s"Generated:${dbItemSeq.size} results")
    db.close()
  }
  //---------------------------------------------------------------------------
  def writeReport(spkID: String
                  , imageCount: Long
                  , bw: BufferedWriter): Unit = {
    val db = SpiceSPK_DB(DEFAULT_DATABASE_NAME, spkID)
    val dbItemSeq = db.findAll()
    val orbitSeq = dbItemSeq.map{dbItem => dbItem._id + "->"  +  dbItem.date}
    val orbitID = if (orbitSeq.size == 1) orbitSeq.head else orbitSeq.mkString("{",",","}")
    dbItemSeq.head.writeReport(bw, orbitID, imageCount)
    db.close()
  }
  //---------------------------------------------------------------------------
  def drop(id: String): Unit = {
    val spkID = getSpkID(id).getOrElse {
      error(s"Can not find a SPK ID using the id: '$id'"); return
    }
    //delete local file copy
    Path.getFileListStartsWithNamePrefix(SmallBodyDB.DEFAULT_DOWNLOAD_DIRECTORY,spkID.toString).foreach { path=>
      MyFile.deleteFileIfExist(path.getAbsolutePath)
    }
    //delete from database
    val db = SpiceSPK_DB(DEFAULT_DATABASE_NAME, spkID.toString)
    db.dropCollection()
    db.close()
  }
  //---------------------------------------------------------------------------
  def generate(id:String
               , oDir: String
               , lastMoreRecent:Option[Int] //if none, it is retrieved the more recent
               , spkVersion: Option[String]
               , fileNameSeq:Option[ArrayBuffer[String]] = None): Unit = {

    val spkID = getSpkID(id).getOrElse{error(s"Can not find a SPK ID using the id: '$id'");return}
    val count = lastMoreRecent.getOrElse(1)
    val db = SpiceSPK_DB(DEFAULT_DATABASE_NAME, spkID.toString)
    val dbItemSeq = if (spkVersion.isDefined) db.find(spkVersion.get) else db.find(count)
    dbItemSeq.foreach { dbItem =>
      val fileName = s"$oDir/${dbItem.toString()}$DEFAULT_BSP_FILE_EXTENSION"
      if (!MyFile.fileExist(fileName)) {
        val bw = new FileOutputStream(new File(fileName))
        bw.write(dbItem.byteSeq)
        bw.close()
      }
      if (fileNameSeq.isDefined) fileNameSeq.get += fileName
    }
    db.close()
  }
  //---------------------------------------------------------------------------
  def update(dir: String) = {
    val fileNameSeq = Path.getSortedFileListRecursive(dir, DEFAULT_SPK_FILE_EXTENSION_SEQ) map (_.getAbsolutePath)
    new ProcessImageDir(fileNameSeq.toArray)
  }
  //---------------------------------------------------------------------------
  class ProcessImageDir(fileNameSeq: Array[String]) extends ParallelTask[String](
    fileNameSeq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(fileName: String) = {
      Try {
        info(s"Processing file:'$fileName'")
        updateSpiceSpkFile(fileName)
      }
      match {
        case Success(_) =>
        case Failure(e) =>
          error(s"Error processing file:'$fileName'")
          error(e.toString)
          error(e.getStackTrace.mkString("\n"))
      }
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def parseHorizonsEntry(finalSpkID: Long, horizonsEntry: HorizonsEntry) = {
    (finalSpkID
      , horizonsEntry.mpc_id
      , horizonsEntry.name
      , horizonsEntry.alternateNameSeq.mkString("{",",","}"))
  }
  //---------------------------------------------------------------------------
  private def getNameFromHeader(header:String,finalSpkID: Long):(Long,Long,String,String) = {
    val r = header.split(BSP_HEADER_MPC_ID_MAGIC_TOKEN)(1).trim.split(" ")
    if(r(0)(0) != '(') {
      val mainDesignation = r(1)
      val horizonsEntry = horizons.getByName(mainDesignation, verbose = false)
      if (horizonsEntry.isDefined) return parseHorizonsEntry(finalSpkID, horizonsEntry.get)
    }
    var alternativeDesignation = r.take(r.indexOf("")).mkString(" ").trim
    alternativeDesignation = alternativeDesignation.drop(alternativeDesignation.indexOf("(") + 1).trim
    alternativeDesignation = alternativeDesignation.take(alternativeDesignation.indexOf(")")).trim
    alternativeDesignation = alternativeDesignation.trim.replaceAll(" +", " ")

    //try with alternative designation
    var horizonsEntry = horizons.getByAlternateName(alternativeDesignation)
    if (horizonsEntry.isDefined) return parseHorizonsEntry(finalSpkID, horizonsEntry.get)

    //try with main designation
    horizonsEntry = horizons.getByName(alternativeDesignation)
    if (horizonsEntry.isDefined) return parseHorizonsEntry(finalSpkID, horizonsEntry.get)

    error(s"Error: SPK ID:'$finalSpkID' not found in Horizons database")
    (-1L, -1L, "", "")
  }
  //---------------------------------------------------------------------------
  private def getMpcInfo(header:String,spkID:Long):(Long,Long,String,String) = {
    var finalSpkID = spkID
    var r = horizons.getBySpk_ID(spkID, verbose = false)
    if (r.isEmpty) {
      //sometinme the SPK ID has a missing 0 when starts with 2000
      val spkID_string = spkID.toString
      if (spkID_string.startsWith(BSP_MISSING_ZERO_SPK_ID_PREFIX)) {
        finalSpkID = (BSP_MISSING_ZERO_SPK_ID_PREFIX + "0" + spkID_string.drop(BSP_MISSING_ZERO_SPK_ID_PREFIX.size)).toLong
        r = horizons.getBySpk_ID(finalSpkID, verbose = false)
        if (!r.isEmpty) return parseHorizonsEntry(finalSpkID,r.get)
      }
      return getNameFromHeader(header,finalSpkID)
    }
    parseHorizonsEntry(finalSpkID,r.get)
  }
  //---------------------------------------------------------------------------
  private def getBasicInfo(header:String) = {
    val r = header.split(BSP_HEADER_SOURCE_MAGIC_TOKEN)(1).split("}")
    val spkID = r(1).split(BSP_HEADER_SPK_ID_DIVIDER)(1).split(BSP_HEADER_SPK_ID_MAGIC_TOKEN)(0).trim.toLong

    if (r(0).startsWith(BSP_HEADER_SOURCE_MAGIC_TOKEN_2)) {
      val r2 = r(0).split(BSP_HEADER_VERSION_DIVIDER)
      val source = r2(0).split(BSP_HEADER_SPK_ID_DIVIDER)(1)
      val version = r2(1)
      (source,version,spkID)
    }
    else {
      val source = r(0)
      val version = r(0)
      (source,version,spkID)
    }
  }
  //---------------------------------------------------------------------------
  private def updateSpiceSpkFile(spkFile: String) = {
    val byteSeq = Files.readAllBytes(Paths.get(spkFile))
    val header = new String(byteSeq.take(BSP_HEADER_BYTE_SIZE).drop(BSP_HEADER_CHAR_START_POS).take(BSP_HEADER_VALID_CHAR_size))
    val (source,version,_spkID) = getBasicInfo(header)
    val (spkID,mpcID,mainDesignation,alternativeDesignation) = getMpcInfo(header,_spkID)

    if (spkID != -1) {
      val mpo = SpiceSPK_Source(
          source+SOURCE_VERSION_DIVIDER+version
        , spkID
        , mpcID
        , source
        , version
        , mainDesignation
        , alternativeDesignation
        , date = LocalDateTime.now()
        , byteSize = byteSeq.size
        , byteSeq)

        val collName = spkID.toString
        val db = SpiceSPK_DB(DEFAULT_DATABASE_NAME,collName)
        db.add(mpo)
        db.close()
    }
  }
  //---------------------------------------------------------------------------
  def getLastestSpk(oDir:String) = {
    val db = SpiceSPK_DB()
    val spkIdSeq = db.getCollectionNameSeq
    db.close()

    val map = spkIdSeq.map { spkID =>
      val db = SpiceSPK_DB(collectionName = spkID)
      val mpo = db.find().head
      val fileName = s"${Path.getCurrentDirectory()}/$oDir/${mpo.toString()}$DEFAULT_BSP_FILE_EXTENSION"
      val bw = new FileOutputStream(new File(fileName))
      bw.write(mpo.byteSeq)
      bw.close()

      db.close()
      (spkID.toLong,fileName)
    }.toMap
    map
  }

  //---------------------------------------------------------------------------
  def getLastestSpkMap() = {
    val db = SpiceSPK_DB()
    val spkIdSeq = db.getCollectionNameSeq
    db.close()

    val map = spkIdSeq.map { spkID =>
      val db = SpiceSPK_DB(collectionName = spkID)
      db.close()
      (spkID.toLong, spkID.toLong)
    }.toMap
    map
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import SpiceSPK_DB._
case class SpiceSPK_DB(databaseName: String = SpiceSPK_DB.DEFAULT_DATABASE_NAME
                       , collectionName: String  = "tmp" //SPK ID
                      ) extends MongoDB {
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = initWithCodeRegister(SpiceSPK_Source.codecRegistry)
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  val collWithRegistry: MongoCollection[SpiceSPK_Source] = database.getCollection(collectionName)
  //---------------------------------------------------------------------------
  connected = true
  //-------------------------------------------------------------------------
  def findByName(name: String, colList: Seq[String]): Seq[Document] = {
    collection.find(equal("Name", name))
      .projection(getProjectionColList(colList))
      .getResultDocumentSeq
  }
  //-------------------------------------------------------------------------
  def add(dbItem:SpiceSPK_Source): Unit = {
    if (exist(dbItem._id))
      collWithRegistry.replaceOne(equal(MONGO_COL_ID, dbItem._id), dbItem).headResult
    else
      collWithRegistry.insertOne(dbItem).headResult
  }
  //-------------------------------------------------------------------------
  def find(count:Int = 1) = {
    collWithRegistry
      .find()
      .sort(descending(COL_NAME_VERSION))
      .limit(count)
      .results()
  }
  //-------------------------------------------------------------------------
  def find(version:String) = {
    collWithRegistry
      .find(equal(COL_NAME_VERSION,version))
      .sort(ascending(COL_NAME_DATE))
      .results()
  }
  //-------------------------------------------------------------------------
  def findAll() = {
    collWithRegistry
      .find()
      .sort(descending(COL_NAME_VERSION))
      .results()
  }
  //-------------------------------------------------------------------------
  def getVersionSeq() =
    collWithRegistry
      .find()
      .sort(descending(COL_NAME_VERSION))
      .results()
      .map( _.version)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SpiceSPK_DB.scala
//=============================================================================
