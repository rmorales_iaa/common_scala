/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Aug/2021
 * Time:  19h:04m
 * Description: List if MPC observatories codes of MPC
 * https://www.minorplanetcenter.net/iau/lists/ObsCodesF.html
 */
package com.common.database.mongoDB.observatories
//=============================================================================
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.mongodb.client.model.CollationStrength
import org.mongodb.scala.model.Collation
import org.mongodb.scala.model.Filters.{equal, regex}
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
object ObservatoriesDB {
  //---------------------------------------------------------------------------
  private val collationCaseInsensitive =
    Collation
      .builder()
      .locale("en")
      .collationStrength(CollationStrength.PRIMARY).build
  //---------------------------------------------------------------------------
  def apply(): ObservatoriesDB =
    apply(MyConf(MyConf.c.getString("Database.mpcObservatories")))
  //---------------------------------------------------------------------------
  def apply(conf: MyConf): ObservatoriesDB =
    ObservatoriesDB(conf.getString("ObservatoriesCodes.databaseName")
                   , conf.getString("ObservatoriesCodes.collectionName"))
  //---------------------------------------------------------------------------
  def findByID(id: String): Option[Observatory] = {
    val db = ObservatoriesDB()
    val observatory = db.findByID(id)
    db.close()
    observatory
  }
  //---------------------------------------------------------------------------
  def findFirstByName(name: String): Option[Observatory] = {
    val db = ObservatoriesDB()
    val observatorySeq = db.findByPartialName(name)
    db.close()
    observatorySeq.headOption
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import ObservatoriesDB._
case class ObservatoriesDB(databaseName: String, collectionName: String) extends MongoDB {
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = initWithCodeRegister(Observatory.codecRegistry)
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  val collWithRegistry: MongoCollection[Observatory] =  database.getCollection(collectionName)
  //---------------------------------------------------------------------------
  connected = true
  //---------------------------------------------------------------------------
  def findByID(id: String): Option[Observatory] = {
    val r = collWithRegistry.find(equal("_id", id))
      .getResultDocumentSeq
    if (r.isEmpty) None
    else Some(r.head)
  }
  //---------------------------------------------------------------------------
  def findByPartialName(name: String): Seq[Observatory] =
    collWithRegistry.find(regex("name", s"(?i).*$name.*"))
      .collation(collationCaseInsensitive)
      .getResultDocumentSeq
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ObservatoriesDB.scala
//=============================================================================
