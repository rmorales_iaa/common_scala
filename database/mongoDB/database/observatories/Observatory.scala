/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  20h:59m
 * Description: List if MPC observatories codes of MPC
 * https://www.minorplanetcenter.net/iau/lists/ObsCodesF.html
 */
package com.common.database.mongoDB.observatories
//=============================================================================
//=============================================================================
import com.common.logger.MyLogger
import upickle.default.{macroRW, ReadWriter => RW}
//=============================================================================
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._

import scala.language.existentials
//=============================================================================
//=============================================================================
object Observatory extends MyLogger {
  //---------------------------------------------------------------------------
  //http://mongodb.github.io/mongo-scala-driver/2.3/getting-started/quick-tour-case-classes/
  //https://jannikarndt.de/blog/2017/08/writing_case_classes_to_mongodb_in_scala/
  val codecRegistry = fromRegistries(fromProviders(classOf[Observatory]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  implicit val rw: RW[Observatory] = macroRW
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[Observatory].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  def getColNameValueSeq(prefix: String = "") =
    classOf[Observatory].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = (prefix + f.getName, f.getType)
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Observatory(_id: String
                       , name: String       //name of the observatory
                       , latitude: Double   //in degrees
                       , longitude: Double  // in degrees east of Greenwich
                       , altitude: Double   //in m
                       , cosPhiR: Double    //r cos phi where phi' is the geocentric latitude and r is the geocentric distance in earth radii
                       , sinPhiR: Double    //r sin phi where phi' is the geocentric latitude and r is the geocentric distance in earth radii
                       , xAxisRec: Double   //rectangular coordinate X axis in KM
                       , yAxisRec: Double   //rectangular coordinate Y axis in KM
                       , zAxisRec: Double   //rectangular coordinate Z axis in KM
                      ) {
  //---------------------------------------------------------------------------
  def printSpkLocation =
    println(s"//---------------------------------------------------------------------------\n" +
      s"${_id}_CENTER  = 399\n" +
      s"${_id}_FRAME   = 'ITRF93' \n" +
      s"${_id}_IDCODE" + "  = \"TBD!!!\" \n" +
      s"${_id}_XYZ     = ($xAxisRec,$yAxisRec,$zAxisRec)\n" +
            "//---------------------------------------------------------------------------\n")
  //---------------------------------------------------------------------------
  def printSpkDefinition =
    println(s"//---------------------------------------------------------------------------\n" +
      "Location { \n" +
      "  //-------------------------------------------------------------------------\n" +
      "  observatoryCode    = \"" + _id + "\"\n" +
      "  spkObservatoryCode = \"TBD!!!\"   //see input/spice/kernels/spk/observatories/observatory_def\n" +
      s"  latLongAlt         = [$latitude, $longitude, $altitude] //latitude(deg), longitude(deg), altitude(m)\n" +
      "  //-------------------------------------------------------------------------\n" +
      "}\n" +
      "//---------------------------------------------------------------------------\n")
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Observatory.scala
//=============================================================================

