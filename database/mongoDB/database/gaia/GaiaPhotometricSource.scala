/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  29/Sep/2020
 * Time:  13h:27m
 * Description: None
 */
//=============================================================================
package com.common.database.mongoDB.database.gaia
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
import com.common.database.mongoDB.database.{CatalogSource, MatchedImageSource}
import com.common.geometry.segment.segment2D.Segment2D
//=============================================================================
//=============================================================================
object GaiaPhotometricSource {
  //---------------------------------------------------------------------------
  def getCsvLineHeader(sep: String = "\t") = {
    "sequence" + sep +
    Segment2D.getCsvLineHeader(sep) + sep +
    GaiaSourceReduced.getCsvLineHeader(sep)
  }
  //-------------------------------------------------------------------------
  def saveAsCsv(seq: Array[MatchedImageSource], csvFileName: String, sep: String ="\t"): Unit = {
    val bw = new BufferedWriter(new FileWriter(new File(csvFileName)))
    bw.write(getCsvLineHeader(sep) +"\n")
    if (seq.isEmpty)  bw.write("#None unknown source detected\n")
    else {
      seq.zipWithIndex.foreach { case (s, i) =>
        val gas = s.catalogSource.asInstanceOf[GaiaPhotometricSource].gas
        val source = s.imageSource
        val line =
          f"$i%010d" + sep +      //sequence
            source.getAsCsvLine(sep) + sep +
            gas.getAsCsvLine(sep)
        bw.write(line + "\n")
      }
    }
    bw.close()
  }
  //-------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
//=============================================================================
case class GaiaPhotometricSource(gas: GaiaSourceReduced) extends CatalogSource {
  //--------------------------------------------------------------------------
  val _id = gas._id
  val ra = gas.ra
  val dec = gas.dec

  val raError = gas.raError
  val decError = gas.decError

  val pmra = gas.pmra
  val pmdec = gas.pmdec

  val pmra_error = gas.pmra_error
  val pmdec_error = gas.pmdec_error

  val referenceEpoch = gas.referenceEpoch

  val gMeanMag = gas.gMeanMag
  //--------------------------------------------------------------------------
  //flux
  private var estimatedBackground = -1d
  private var fluxPerSecond       = -1d
  private var fluxSNR             = -1d //in decibels
  //---------------------------------------------------------------------------
  def setCalculatedInfo(f: Double, snr: Double, b: Double): Unit = {
    fluxPerSecond = f
    fluxSNR = snr
    estimatedBackground = b
  }
  //---------------------------------------------------------------------------
  def getFluxPerSecond() =  fluxPerSecond
  //---------------------------------------------------------------------------
  def getEstimatedBackground() = estimatedBackground
  //---------------------------------------------------------------------------
  def getFluxSNR() = fluxSNR
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file GaiaSourcePhotometric.scala
//=============================================================================
