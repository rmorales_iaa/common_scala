//=============================================================================
//File: GaiaDB.scala
//=============================================================================
/** It implements an set of tools to manage GAIA data
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.database.mongoDB.database.gaia
//=============================================================================
import com.common.dataType.tree.kdTree.K2d_TreeDoubleWithLong
import com.common.database.mongoDB.database.{CatalogDB, CatalogSource}
import com.common.geometry.point.Point2D_Double
import com.common.util.time.Time
//=============================================================================
import com.common.logger.MyLogger
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers._
import com.common.database.mongoDB.MongoDB._
import com.common.database.mongoDB.MongoDB
import org.mongodb.scala.model.{Collation, CollationStrength}
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
import scala.collection.mutable.ArrayBuffer
//=============================================================================
// Class/Object implementation
//=============================================================================
object GaiaDB extends MyLogger {
  //---------------------------------------------------------------------------
  val CATALOG_NAME    = "GAIA_DR3"
  val REFERENCE_EPOCH = 2016.0d //extract from original column 'ref_epoch'
  //---------------------------------------------------------------------------
  val SUB_GAIA_DATABASE_NAME           = "sub_gaia"
  //---------------------------------------------------------------------------
  //source pos
  final val GAIA_COL_ID                = "_id"
  final val GAIA_COL_RA                = "ra"
  final val GAIA_COL_DEC               = "dec"
  private final val GAIA_COL_RA_ERROR  = "ra_error"
  private final val GAIA_COL_DEC_ERROR = "dec_error"
  private final val GAIA_COL_REF_EPOCH = "ref_epoch"

  //parallax
  final val GAIA_COL_PARALLAX            = "parallax"
  final val GAIA_COL_PARALLAX_ERROR      = "parallax_error"
  final val GAIA_COL_PARALLAX_OVER_ERROR = "parallax_over_error"
  //---------------------------------------------------------------------------
  //proper movement
  private final val GAIA_COL_PM_RA        = "pmra"
  private final val GAIA_COL_PM_DEC       = "pmdec"
  private final val GAIA_COL_PM_RA_ERROR  = "pmra_error"
  private final val GAIA_COL_PM_DEC_ERROR = "pmdec_error"
  //---------------------------------------------------------------------------
  //flux and magnitude
  final val GAIA_COL_PHOTO_G_MEAN_FLUX = "phot_g_mean_flux"
  final val GAIA_COL_PHOTO_G_MEAN_MAG  = "phot_g_mean_mag"

  final val GAIA_COL_PHOTO_BP_MEAN_FLUX = "phot_bp_mean_flux"
  final val GAIA_COL_PHOTO_BP_MEAN_MAG  = "phot_bp_mean_mag"

  final val GAIA_COL_PHOTO_RP_MEAN_FLUX = "phot_rp_mean_flux"
  final val GAIA_COL_PHOTO_RP_MEAN_MAG  = "phot_rp_mean_mag"

  final val GAIA_COL_PHOTO_BP_RP        = "bp_rp"

  final val GAIA_COL_PHOTO_BP_RP_EXECESS_FACTOR = "phot_bp_rp_excess_factor"

  final val GAIA_COL_PHOT_G_MEAN_FLUX_OVER_ERROR = "phot_g_mean_flux_over_error"
  final val GAIA_COL_PHOT_BP_MEAN_FLUX_OVER_ERROR = "phot_bp_mean_flux_over_error"
  final val GAIA_COL_PHOT_RP_MEAN_FLUX_OVER_ERROR = "phot_rp_mean_flux_over_error"
  //---------------------------------------------------------------------------
  //ruwe
  final val GAIA_COL_RUWE = "ruwe"
  //---------------------------------------------------------------------------
  //stats
  final val GAIA_COL_ASTROMETRIC_CHI_2_AL = "astrometric_chi2_al"
  final val GAIA_COL_ASTROMETRIC_N_GOOD_OBS_AL = "astrometric_n_good_obs_al"
  //---------------------------------------------------------------------------
  final val GAIA_SOURCE_COL_NAME_LIST = Seq(MongoDB.MONGO_COL_ID
    , GAIA_COL_RA, GAIA_COL_DEC, GAIA_COL_RA_ERROR, GAIA_COL_DEC_ERROR
    , GAIA_COL_PHOTO_G_MEAN_MAG, GAIA_COL_PHOTO_BP_MEAN_MAG, GAIA_COL_PHOTO_RP_MEAN_MAG, GAIA_COL_PHOTO_BP_RP_EXECESS_FACTOR, GAIA_COL_RUWE
    , GAIA_COL_REF_EPOCH, GAIA_COL_PM_RA, GAIA_COL_PM_DEC, GAIA_COL_PM_RA_ERROR, GAIA_COL_PM_DEC_ERROR)

  //---------------------------------------------------------------------------
  final val GAIA_SOURCE_COL_NAME_SIMPLE_POS_SEQ = Array(MongoDB.MONGO_COL_ID, GAIA_COL_RA, GAIA_COL_DEC)
  //----------------------------------------------------------------------------
  val collationCaseInsensitive = Collation
    .builder()
    .locale("en")
    .collationStrength(CollationStrength.PRIMARY).build
  //---------------------------------------------------------------------------
  private final val conf = MyConf(MyConf.c.getString("Database.mongoDB.gaia"))
  val matchRadiusMas  = conf.getDouble("Database.query.matchRadiusMas")
  //---------------------------------------------------------------------------
  def apply(conf: MyConf): GaiaDB =
    GaiaDB(conf.getString("Database.name")
         , conf.getString("Database.source.collectionName"))
  //---------------------------------------------------------------------------
  def apply(): GaiaDB =
    GaiaDB(MyConf(MyConf.c.getString("Database.mongoDB.gaia")))
  //---------------------------------------------------------------------------
}
//=============================================================================
import GaiaDB._
case class GaiaDB(databaseName: String, collectionName: String) extends CatalogDB {
  //---------------------------------------------------------------------------
  final val catalogName    = CATALOG_NAME
  final val referenceEpoch = REFERENCE_EPOCH
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = init()
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  connected = true
  //---------------------------------------------------------------------------
  private var sourceMap: Map[Long, GaiaSourceReduced] = null
  private var sourceKdtree: K2d_TreeDoubleWithLong = null
  //---------------------------------------------------------------------------
  def getSourceMap = sourceMap
  //---------------------------------------------------------------------------
  def buildMap(gasSeq: Seq[GaiaSourceReduced]) = {
    sourceMap = gasSeq.map { source =>
      source.mapID = source._id
      (source._id, source)
    }.toMap
  }
  //---------------------------------------------------------------------------
  def buildMapAndKdTree(gasSeq: Seq[GaiaSourceReduced]) = {
    if (sourceKdtree == null) {
      sourceKdtree = K2d_TreeDoubleWithLong()
      buildMap(gasSeq)
      val seq = gasSeq.map { s => (s.getRaDec().toSequence(), s.mapID) }
      sourceKdtree.addSeq(seq)
    }
  }
  //---------------------------------------------------------------------------
  private def calculateKdTree() = {
    if (sourceKdtree == null)
      buildMapAndKdTree(getAllDocuments() map (doc => GaiaSourceReduced(doc)))
    else sourceKdtree
  }
  //---------------------------------------------------------------------------
  def findSourceSeqInRectangle(minRa: Double
                               , maxRa: Double
                               , minDec: Double
                               , maxDec: Double
                               , colList: Array[String]
                               , verbose: Boolean = true): Seq[Document] = {

    //To speed up, use only fields with indexes in the query: covered query
    //https://docs.mongodb.com/manual/core/query-optimization/#read-operations-covered-query
    val startTime = System.currentTimeMillis

    //log the query
    val t = collection.find(
      and(gte("ra", minRa)
        , lte("ra", maxRa)
        , gte("dec", minDec)
        , lte("dec", maxDec)))
      .projection(getProjectionColList(colList))
      .getResultDocumentSeq

    if (verbose)
      info(s"MongoDB '$collectionName' has reported: " +
        t.length +
        " sources in " + Time.getFormattedElapseTimeFromStart(startTime))
    t
  }
  //---------------------------------------------------------------------------
  def findClosestSourceSeq(objectRA: Double
                           , objectDEC: Double
                           , objectRaDecObservingDate: String
                           , matchRadiusMas: Double
                           , limitResultTo: Int = 100): Array[CatalogSource] = {
    calculateKdTree()
    val r = ArrayBuffer[(Double,GaiaPhotometricSource)]()

    sourceKdtree.getKNN(Seq(objectRA, objectDEC), requestedK = limitResultTo).foreach { t =>
      val catalogSource = sourceMap(t.v)
      val distanceSourceCatalogToObjectMas = catalogSource.getDistanceTo(Point2D_Double(objectRA, objectDEC)
                                                                         , objectRaDecObservingDate
                                                                         , inMAS = true)
      if (distanceSourceCatalogToObjectMas <= matchRadiusMas) {
        r += ((distanceSourceCatalogToObjectMas,GaiaPhotometricSource(catalogSource)))
        if (r.length >= limitResultTo) return r.toArray.sortWith( _._1 < _._1).map( _._2)
      }
    }
    r.toArray.sortWith( _._1 < _._1).map( _._2)
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
// End of 'GaiaDB.scala' file
//=============================================================================