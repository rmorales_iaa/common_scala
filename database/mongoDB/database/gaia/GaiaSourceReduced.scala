/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  29/Sep/2020
 * Time:  13h:27m
 * Description: None
 */
//=============================================================================
package com.common.database.mongoDB.database.gaia
//=============================================================================
import com.common.database.mongoDB.database.CatalogSource
import com.common.geometry.segment.segment2D.Segment2D

import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
import org.mongodb.scala.Document
//=============================================================================
//=============================================================================
object GaiaSourceReduced {
  //---------------------------------------------------------------------------
  def getCsvLineHeader(sep: String = "\t") =
    s"gaia_id$sep" +
      s"gaia_ra.x$sep" +
      s"gaia_dec.y$sep" +
      s"gaia_ra_error$sep" +
      s"gaia_dec_error$sep" +
      s"gaia_gMeanMag" + sep +
      s"gaia_bpMeanMag" + sep +
      s"gaia_rpMeanMag" + sep +
      s"gaia_bp_rpExcessFactor" + sep +
      s"gaia_ruwe" + sep +
      s"gaia_referenceEpoch$sep" +
      s"gaia_pmra$sep" +
      s"gaia_pmdec$sep" +
      s"gaia_pmra_error$sep" +
      s"gaia_pmdec_error"
  //---------------------------------------------------------------------------
  def apply(source: Segment2D) : GaiaSourceReduced =
    GaiaSourceReduced(ra = source.getCentroidRaDec().x
                    , dec = source.getCentroidRaDec().y)
  //---------------------------------------------------------------------------
  def apply(doc: Document) : GaiaSourceReduced = {
    val id = doc(GaiaDB.GAIA_SOURCE_COL_NAME_LIST(0)).asInt64().getValue
    val ra = doc(GaiaDB.GAIA_SOURCE_COL_NAME_LIST(1)).asDouble().getValue
    val dec = doc(GaiaDB.GAIA_SOURCE_COL_NAME_LIST(2)).asDouble().getValue
    val ra_err = doc(GaiaDB.GAIA_SOURCE_COL_NAME_LIST(3)).asDouble().getValue
    val dec_err = doc(GaiaDB.GAIA_SOURCE_COL_NAME_LIST(4)).asDouble().getValue
    val gMeanMag = doc(GaiaDB.GAIA_SOURCE_COL_NAME_LIST(5)).asDouble().getValue
    val bpMeanMag = doc(GaiaDB.GAIA_SOURCE_COL_NAME_LIST(6)).asDouble().getValue
    val rpMeanMag = doc(GaiaDB.GAIA_SOURCE_COL_NAME_LIST(7)).asDouble().getValue
    val bp_rpExcessFactor = doc(GaiaDB.GAIA_SOURCE_COL_NAME_LIST(8)).asDouble().getValue
    val ruwe = doc(GaiaDB.GAIA_SOURCE_COL_NAME_LIST(9)).asDouble().getValue
    val referenceEpoch = doc(GaiaDB.GAIA_SOURCE_COL_NAME_LIST(10)).asDouble().getValue.toInt
    val pmra = doc(GaiaDB.GAIA_SOURCE_COL_NAME_LIST(11)).asDouble().getValue
    val pmdec = doc(GaiaDB.GAIA_SOURCE_COL_NAME_LIST(12)).asDouble().getValue
    val pmra_error = doc(GaiaDB.GAIA_SOURCE_COL_NAME_LIST(13)).asDouble().getValue
    val pmdec_error = doc(GaiaDB.GAIA_SOURCE_COL_NAME_LIST(14)).asDouble().getValue

    GaiaSourceReduced(
        id
      , ra, dec, ra_err, dec_err
      , gMeanMag, bpMeanMag, rpMeanMag, bp_rpExcessFactor
      , ruwe
      , referenceEpoch
      , pmra, pmdec, pmra_error, pmdec_error)
  }
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String="") =
    classOf[GaiaSourceReduced].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  def saveAsCsv(seq: Array[GaiaSourceReduced], csvFileName: String, sep: String = "\t"): Unit = {
    val bw = new BufferedWriter(new FileWriter(new File(csvFileName)))
    bw.write(s"id$sep" + GaiaSourceReduced.getCsvLineHeader(sep) + "\n")
    if (seq.isEmpty) bw.write("#None unknown source detected\n")
    else {
      seq.zipWithIndex.foreach { case (source, i) =>
        bw.write(
          f"$i%010d" + sep +
            source.getAsCsvLine(sep) +
            "\n"
        )
      }
    }
    bw.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class GaiaSourceReduced(_id: Long = -1
                             , ra: Double
                             , dec: Double
                             , raError: Double = -1
                             , decError: Double = -1
                             , gMeanMag: Double = -1
                             , bpMeanMag: Double = -1
                             , rpMeanMag: Double = -1
                             , bp_rpExcessFactor: Double = -1
                             , ruwe: Double = -1
                             , referenceEpoch: Double = -1
                             , pmra: Double = -1 //proper motion
                             , pmdec: Double = -1 //proper motion
                             , pmra_error: Double = -1
                             , pmdec_error: Double = -1) extends CatalogSource {
  //---------------------------------------------------------------------------
  def getAsCsvLine(sep: String ="\t") =
    f"${_id}%010d" + sep +     //m2_id
      s"$ra" + sep +          //ra
      s"$dec" + sep +         //dec
      s"$raError"  + sep +    //ra error
      s"$decError" + sep +    //dec error
      s"$gMeanMag" + sep +    //phot_g_mean_mag
      s"$bpMeanMag" + sep +   //phot_bp_mean_mag
      s"$rpMeanMag" + sep +   //phot_rp_mean_mag
      s"$bp_rpExcessFactor" + sep + //bp_rp_execess_factor
      s"$ruwe"+ sep +            //ruwe
      s"$referenceEpoch" + sep + //reference epoch
      s"$pmra" + sep +        //ra proper motion
      s"$pmdec" + sep +       //dec proper motion
      s"$pmra_error"  + sep + //ra proper motion error
      s"$pmdec_error"         //dec proper motion error
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file GaiaSourceReduced.scala
//=============================================================================
