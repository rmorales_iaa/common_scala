/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  20h:59m
 * Description: https://heasarc.gsfc.nasa.gov/W3Browse/all/hipparcos.html
 */
//=============================================================================
package com.common.database.mongoDB.database.lcdb.mpc.colorIndex
//=============================================================================
import com.common.database.mongoDB.fortran.FortranColumn
import com.common.logger.MyLogger
import org.mongodb.scala.Document
import org.mongodb.scala.bson.BsonValue
import scala.collection.mutable.ArrayBuffer
//=============================================================================
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import scala.language.existentials
//=============================================================================
//=============================================================================
object LcdbColorIndexSource extends MyLogger {
  //---------------------------------------------------------------------------
  val codecRegistry = fromRegistries(fromProviders(classOf[LcdbColorIndexSource]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  private val debug = false
  //---------------------------------------------------------------------------
  private val mpoColumnData = Array(
      FortranColumn("Number", "I8", 0, 6)
    , FortranColumn("Flag", "A1", 8, 9)
    , FortranColumn("Name", "A29", 10, 40)
    , FortranColumn("Period", "F8.3", 41, 54)
    , FortranColumn("Amp", "F1.3", 55, 59)
    , FortranColumn("BV", "F1.3", 60, 66)
    , FortranColumn("BR", "F1.3", 67, 73)
    , FortranColumn("VR", "F1.3", 74, 80)
    , FortranColumn("VI", "F1.3", 81, 87)
    , FortranColumn("RI", "F1.3", 88, 94)
    , FortranColumn("SGR", "F1.3", 95, 101)
    , FortranColumn("SRI", "F1.3", 102, 108)
    , FortranColumn("SIZ", "F1.3", 107, 114)
    , FortranColumn("ATLco", "F1.3", 115, 125)
  )
  //---------------------------------------------------------------------------
  private val rowByteSize = (mpoColumnData map (_.getCharSize)).sum
  //---------------------------------------------------------------------------
  def buildDocument(_line: String): Option[Document] = {
    if (_line.trim.isEmpty) None
    else {
      val line = _line.padTo(rowByteSize, ' ')
      val row = ArrayBuffer[(String, BsonValue)]()
      var offset: Int = 0
      mpoColumnData.foreach { t =>
        val startCol = offset
        val endCol = offset + t.charSize
        val token = line.substring(startCol, endCol)
        offset += t.charSize
        if (t.name == "divider") {
          if (token != (" " * token.length)) {
            error(s"Error parsing column '${t.name}' the divider is not ' '. Current value:'$token'")
            error(line)
            return None
          }
        }
        else if (t.name != "end_of_column") {
          if (debug) println(s"${t.name} -> '$token' (${t.charSize})")
          t.addColumnToRow(token.trim, row)
        }
      }
      Some(Document(row))
    }
  }
  //---------------------------------------------------------------------------
  def build(lineSeq: List[String]): Seq[LcdbColorIndexSource] = {

    val docSeq = lineSeq.flatMap { line=>
      buildDocument(line)

    }
    val mpcID = docSeq.head("Number").asInt64().getValue.toInt
    val name = docSeq.head("Name").asString().getValue
    docSeq.zipWithIndex.map { case (doc,i) =>
      LcdbColorIndexSource(
        mpcID //Number
        , doc("Flag").asString().getValue //Flag
        , name  //Name
        , doc("Period").asDouble().getValue //Period
        , doc("Amp").asDouble().getValue //Amp
        , doc("BV").asDouble().getValue //BV
        , doc("BR").asDouble().getValue //BR
        , doc("VR").asDouble().getValue //VR
        , doc("VI").asDouble().getValue //VI
        , doc("RI").asDouble().getValue //RI
        , doc("SGR").asDouble().getValue //SGR
        , doc("SRI").asDouble().getValue //SRI
        , doc("SIZ").asDouble().getValue //SIZ
        , doc("ATLco").asDouble().getValue //ATLco
        , if (i == 0)  "LCDB minor planet center" else "LCDB_mpc:" + doc("Name").asString().getValue //Reference
      )
    }
  }
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[LcdbColorIndexSource].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  def getColNameValueSeq(prefix: String = "") =
    classOf[LcdbColorIndexSource].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = (prefix + f.getName, f.getType)
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class LcdbColorIndexSource(Number                :Int
                                , Flag                :String
                                , Name                :String
                                , Period              :Double
                                , Amp                 :Double
                                , BV                  :Double
                                , BR                  :Double
                                , VR                  :Double
                                , VI                  :Double
                                , RI                  :Double
                                , SGR                 :Double
                                , SRI                 :Double
                                , SIZ                 :Double
                                , ATLco               :Double
                                , Reference           :String
                               )
//=============================================================================
//=============================================================================
//End of file LcdbColorIndexSource.scala
//=============================================================================
