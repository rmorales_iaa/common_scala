/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  20h:59m
 * Description: https://heasarc.gsfc.nasa.gov/W3Browse/all/hipparcos.html
 */
//=============================================================================
package com.common.database.mongoDB.database.lcdb.pds.colorIndex
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import scala.language.existentials
//=============================================================================
//=============================================================================
object LcdbColorIndexSource extends MyLogger {
  //---------------------------------------------------------------------------
  val codecRegistry = fromRegistries(fromProviders(classOf[LcdbColorIndexSource]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  def apply(row: Array[String]): LcdbColorIndexSource = {
    LcdbColorIndexSource(
      row(0).toInt //Number
      , row(1) //Name
      , row(2).toDouble //SumPer
      , row(3).toDouble //SumAmp
      , row(4) //ShortRef
      , row(5) //DateObs
      , row(6).toDouble //DetPer
      , row(7).toDouble //DetPerErr
      , row(8).toDouble //DetAmp
      , row(9).toDouble //DetAmpErr
      , row(10).toDouble //BV
      , row(11).toDouble //BVErr
      , row(12).toDouble //BR
      , row(13).toDouble //BRErr
      , row(14).toDouble //VR
      , row(15).toDouble //VRErr
      , row(16).toDouble //VI
      , row(17).toDouble //VIErr
      , row(18).toDouble //RI
      , row(19).toDouble //RIErr
      , row(20).toDouble //SGR
      , row(21).toDouble //SGRErr
      , row(22).toDouble //SRI
      , row(23).toDouble //SRIErr
      , row(24).toDouble //SIZ
      , row(25).toDouble //SIZErr
      , row(26).toDouble //ATLco
      , row(27).toDouble //ATLcoErr
    )
  }
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[LcdbColorIndexSource].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  def getColNameValueSeq(prefix: String = "") =
    classOf[LcdbColorIndexSource].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = (prefix + f.getName, f.getType)
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class LcdbColorIndexSource(Number                :Int
                                , Name                :String
                                , SumPer              :Double
                                , SumAmp              :Double
                                , ShortRef            :String
                                , DateObs             :String
                                , DetPer              :Double
                                , DetPerErr           :Double
                                , DetAmp              :Double
                                , DetAmpErr           :Double
                                , BV                  :Double
                                , BVErr               :Double
                                , BR                  :Double
                                , BRErr               :Double
                                , VR                  :Double
                                , VRErr               :Double
                                , VI                  :Double
                                , VIErr               :Double
                                , RI                  :Double
                                , RIErr               :Double
                                , SGR                 :Double
                                , SGRErr              :Double
                                , SRI                 :Double
                                , SRIErr              :Double
                                , SIZ                 :Double
                                , SIZErr              :Double
                                , ATLco               :Double
                                , ATLcoErr            :Double
                              )
//=============================================================================
//=============================================================================
//End of file LcdbColorIndexSource.scala
//=============================================================================
