/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Aug/2021
 * Time:  19h:04m
 * Description: Complete 2Mass database
 *
 */
//=============================================================================
package com.common.database.mongoDB.database.lcdb.pds.colorIndex
//=============================================================================
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.MongoDB
//=============================================================================
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
object LcdbColorIndexDB {
  //---------------------------------------------------------------------------
  def apply(conf: MyConf = MyConf(MyConf.c.getString("Database.lcdbColorIndex"))): LcdbColorIndexDB =
    LcdbColorIndexDB(conf.getString("Lcdb.pds.databaseName")
                    , conf.getString("Lcdb.pds.collectionName"))
  //---------------------------------------------------------------------------
}
//=============================================================================
case class LcdbColorIndexDB(databaseName: String, collectionName: String) extends MongoDB {
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = initWithCodeRegister(LcdbColorIndexSource.codecRegistry)
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  //---------------------------------------------------------------------------
  connected = true
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file LcdbColorIndexDB.scala
//=============================================================================
