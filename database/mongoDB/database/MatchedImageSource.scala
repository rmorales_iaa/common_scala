/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/Jan/2023
 * Time:  09h:46m
 * Description: None
 */
//=============================================================================
//=============================================================================
package com.common.database.mongoDB.database
//=============================================================================
import com.common.dataType.pixelDataType.PixelDataType
import com.common.geometry.point.{Point2D, Point2D_Double}
import com.common.geometry.segment.segment2D.Segment2D
import com.common.stat.StatDescriptive
//=============================================================================
//=============================================================================
object MatchedImageSource {
  //---------------------------------------------------------------------------
  def apply(pixPos: Point2D_Double): MatchedImageSource = {
    val segment2D = Segment2D(Array(Array(PixelDataType.PIXEL_ZERO_VALUE)), Point2D.POINT_ZERO)
    segment2D.setCentroidRaw(pixPos)
    MatchedImageSource(segment2D,null)
  }
  //---------------------------------------------------------------------------
  def getResidualRaDecMas(matchedImageSourceSeq: Array[MatchedImageSource], observingDate: String) = { //milli arcosec
    matchedImageSourceSeq.map(matchedImageSource =>
      matchedImageSource.catalogSource.getResidualRaDecPM_Mas(observingDate
                                                               , matchedImageSource.imageSource.getCentroidRaDec()))
  }
  //---------------------------------------------------------------------------
  def getResidualRaDecStatsMas(matchedImageSourceSeq: Array[MatchedImageSource], observingDate: String) = { //milli arcosec
    val residualSeq = getResidualRaDecMas(matchedImageSourceSeq, observingDate)
    val raStats = StatDescriptive.getWithDouble(residualSeq map (_.x))
    val decStats = StatDescriptive.getWithDouble(residualSeq map (_.y))
    (raStats.stdDev, decStats.stdDev)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class MatchedImageSource(imageSource: Segment2D
                             , catalogSource: CatalogSource) {
  //---------------------------------------------------------------------------
  def == (s: MatchedImageSource) : Boolean = {
    if(imageSource == null) false
    if(s.imageSource == null) false
    imageSource == s.imageSource
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file MatchedImageSource.scala
//=============================================================================
