/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/Jan/2023
 * Time:  09h:46m
 * Description: None
 */
//=============================================================================
//=============================================================================
package com.common.database.mongoDB.database
//=============================================================================
import com.common.coordinate.conversion.Conversion
import com.common.geometry.point.Point2D_Double
//=============================================================================
import java.time.LocalDateTime
//=============================================================================
trait CatalogSource {
  //---------------------------------------------------------------------------
  val _id: Long
  val ra: Double
  val dec: Double

  val raError: Double
  val decError: Double

  val pmra: Double
  val pmdec: Double

  val pmra_error: Double
  val pmdec_error:  Double

  val referenceEpoch: Double
  //---------------------------------------------------------------------------
  var mapID: Long = -1
  //---------------------------------------------------------------------------
  def getRaDec() = Point2D_Double(ra, dec)
  //---------------------------------------------------------------------------
  def getRaDecPM = Point2D_Double(pmra, pmdec)
  //---------------------------------------------------------------------------
   def getRaDecApplyingPM(observingDate: LocalDateTime):
     Point2D_Double = getRaDecWithPM(observingDate.toString)
  //---------------------------------------------------------------------------
  def getRaDecWithPM(observingDate: String) =
    Conversion.properMotion(
      observingDate
      , raDec = Point2D_Double(ra, dec)
      , referenceEpoch
      , _properMotion = Point2D_Double(pmra, pmdec)
    )
  //---------------------------------------------------------------------------
  def getDistanceTo(raDec: Point2D_Double
                    , raDecObservingDate: String
                    , inMAS: Boolean = false) = {
    val multiplier = if (inMAS) 1d else 1 / 1000d
    val residual = getRaDecWithPM(raDecObservingDate).getResidualMas(raDec).abs() * multiplier
    val residualSquared = residual * residual
    Math.sqrt(residualSquared.x + residualSquared.y)
  }
  //---------------------------------------------------------------------------
  def getResidualRaDecPM_Mas(observingDate: String, raDec: Point2D_Double) =
    getRaDecWithPM(observingDate).getResidualMas(raDec)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CatalogSource.scala
//=============================================================================
