/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  13/Nov/2022
 * Time:  09h:55m
 * Description: None
 */
//=============================================================================
package com.common.database.mongoDB.database.apass
//=============================================================================
import com.common.geometry.point.Point2D_Double
import org.mongodb.scala.bson.codecs.Macros._
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
//=============================================================================
import scala.language.existentials
//=============================================================================
//=============================================================================
object ApassSource {
  //---------------------------------------------------------------------------
  val codecRegistry = fromRegistries(fromProviders(classOf[ApassSource]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String = "") =
    classOf[ApassSource].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }

  //---------------------------------------------------------------------------
  def getColNameValueSeq(prefix: String = "") =
    classOf[ApassSource].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = (prefix + f.getName, f.getType)
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
}
//============================================================================
case class ApassSource( _id  : Int
                       , idstr: String
                       , field_id: String
                       , ra      :Double
                       , ra_sig  :Double
                       , dec     :Double
                       , dec_sig :Double
                       , zone_id :Int
                       , node_id :Int
                       , container_id: Int
                       , container_width:Double
                       , container_height:Double
                       , container_area:Double
                       , good_obs : Int
                       , num_obs_B: Int
                       , num_obs_V: Int
                       , num_obs_su: Int
                       , num_obs_sg: Int
                       , num_obs_sr: Int
                       , num_obs_si: Int
                       , num_obs_sz: Int
                       , num_obs_Ha: Int
                       , num_obs_ZS: Int
                       , num_obs_Y: Int
                       , num_nights_B: Int
                       , num_nights_V: Int
                       , num_nights_su: Int
                       , num_nights_sg: Int
                       , num_nights_sr: Int
                       , num_nights_si: Int
                       , num_nights_sz: Int
                       , num_nights_Ha: Int
                       , num_nights_ZS: Int
                       , num_nights_Y: Int
                       , B :Double
                       , V :Double
                       , su :Double
                       , sg :Double
                       , sr :Double
                       , si :Double
                       , sz :Double
                       , Ha :Double
                       , ZS :Double
                       , Y :Double
                       , B_sig :Double
                       , V_sig :Double
                       , su_sig :Double
                       , sg_sig :Double
                       , sr_sig :Double
                       , si_sig :Double
                       , sz_sig :Double
                       , Ha_sig :Double
                       , ZS_sig :Double
                       , Y_sig :Double) {
  //---------------------------------------------------------------------------
  def getRaDec() = Point2D_Double(ra,dec)
  //---------------------------------------------------------------------------
  def getAsCsvLine(sep: String) =
    this.getClass.getDeclaredFields.map { f =>
      f.setAccessible(true)
      val v = f.get(this).toString
      f.setAccessible(false)
      v
    }.mkString(sep)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ApassSource.scala
//=============================================================================