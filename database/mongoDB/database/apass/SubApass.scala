/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Jan/2023
 * Time:  18h:30m
 * Description: None
 */
//=============================================================================
package com.common.database.mongoDB.database.apass
//=============================================================================
import com.common.dataType.tree.kdTree.K2d_TreeDouble
import com.common.database.mongoDB.database.CatalogSource
import com.common.image.myImage.MyImage
//=============================================================================
//=============================================================================
object SubApass {
  //---------------------------------------------------------------------------
  val db = ApassDB()
  //---------------------------------------------------------------------------
  def apply(img: MyImage):SubApass = {
    val relevantPosSeq = img.getRaDecRelevantPos()
    SubApass(
        relevantPosSeq(0)
      , relevantPosSeq(1)
      , relevantPosSeq(2)
      , relevantPosSeq(3))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class SubApass(minRa: Double
                    , maxRa: Double
                    , minDec: Double
                    , maxDec: Double) {
  //---------------------------------------------------------------------------
  private val (sourceMap,kdTree) = buildMapAndKdTree
  //---------------------------------------------------------------------------
  private def buildMapAndKdTree() = {

    val kdTree = K2d_TreeDouble()
 /*   val sourceMap = imageDB.findSourceSeqInRectangle(minRa, maxRa, minDec, maxDec)
      .zipWithIndex.map { case (source,i) =>
      val as = source.asInstanceOf[ApassSource]
      as.mapID = i
      (i, as)
    }.toMap

    val posSeq = sourceMap.values.map { source =>
      (source.getRaDec().toSequence(), source.mapID)
    }.toArray
    kdTreeCompensatedCentroid.addSeq(posSeq)
    (sourceMap,kdTreeCompensatedCentroid)*/
    (null,null)
  }
  //---------------------------------------------------------------------------
  def findClosestSourceSeq(ra: Double
                           , dec: Double
                           , raDecObservingDate: String
                           , matchRadiusMas: Double
                           , limitResultTo: Int = 100): Array[CatalogSource] = {
    /*
    val r = ArrayBuffer[ApassSource]()
    kdTreeCompensatedCentroid.getKNN(Seq(ra, dec), requestedK = limitResultTo).foreach { t =>
      val source = sourceMap(t.v)
      if (source.getDistanceTo(Point2D_Double(ra, dec), objectRaDecObservingDate, inMAS = true) <= matchRadiusMas) {
        r += source
        if (r.length >= limitResultTo) return r.toArray
      }
    }
    r.toArray*/
    Array[CatalogSource]()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SubApass.scala
//=============================================================================
