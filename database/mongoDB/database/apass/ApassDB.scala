//=============================================================================
//File: Gaia.scala
//=============================================================================
/** It implements an set of tools to manage GAIA data
 *  CockrachDB sextractorTable
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.database.mongoDB.database.apass
//=============================================================================
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
import com.common.logger.MyLogger
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers._
import com.common.database.mongoDB.MongoDB
//=============================================================================
// Class/Object implementation
//=============================================================================
object ApassDB extends MyLogger {
  //---------------------------------------------------------------------------
  final val CATALOG_NAME    = "APass DR 10"
  final val REFERENCE_EPOCH = 2000d
  //---------------------------------------------------------------------------
  //source pos
  final val COL_ID        = "id"
  final val COL_RA        = "ra"
  final val COL_DEC       = "dec"
  //---------------------------------------------------------------------------
  final val SOURCE_COL_NAME_LIST = Seq(MongoDB.MONGO_COL_ID,COL_ID, COL_RA, COL_DEC)
  //---------------------------------------------------------------------------
  private final val conf = MyConf(MyConf.c.getString("Database.apassDR_10"))
  //---------------------------------------------------------------------------
  def apply(): ApassDB =
    ApassDB(conf.getString("Database.name")
          , conf.getString("Database.source.collectionName"))
  //---------------------------------------------------------------------------
  def apply(conf:MyConf): ApassDB =
    ApassDB(conf.getString("Database.name")
      , conf.getString("Database.collectionName"))
  //---------------------------------------------------------------------------
}
//=============================================================================
import ApassDB._
case class ApassDB(databaseName: String, collectionName: String) extends MongoDB {
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = initWithCodeRegister(ApassSource.codecRegistry)
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  val collWithRegistry: MongoCollection[ApassSource] = database.getCollection(collectionName)
  connected = true
  //-------------------------------------------------------------------------
  def findInBoxNoLimit(minRa: Double
                       , maxRa: Double
                       , minDec: Double
                       , maxDec: Double) =
    collWithRegistry.find(and(gte(COL_RA, minRa)
      , lte(COL_RA, maxRa)
      , gte(COL_DEC, minDec)
      , lte(COL_DEC, maxDec)
    )).getResultDocumentSeq
  //-------------------------------------------------------------------------
  def findRangeID(minId: Double     //inclusive
                  , maxID: Double) = //inclusive

    collWithRegistry.find(and(gte(COL_ID, minId)
                            , lte(COL_ID, maxID)
    )).getResultDocumentSeq
  //---------------------------------------------------------------------------
}
//=============================================================================
// End of 'ApassDB.scala' file
//=============================================================================