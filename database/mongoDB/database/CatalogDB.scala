/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/Jan/2023
 * Time:  09h:21m
 * Description: None
 */
//=============================================================================
package com.common.database.mongoDB.database
//=============================================================================
import com.common.database.mongoDB.MongoDB
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.myImage.MyImage
import com.common.image.telescope.Telescope
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
trait CatalogDB extends MongoDB {
  //---------------------------------------------------------------------------
  val catalogName: String
  val referenceEpoch: Double
  //---------------------------------------------------------------------------
  def findClosestSourceSeq(ra: Double
                           , dec: Double
                           , raDecObservingDate: String
                           , matchRadiusMas: Double
                           , limitResultTo: Int = 100
                          ): Array[CatalogSource]
  //---------------------------------------------------------------------------
  //multiple means that database stores at the same query area more then one source
  //(known, unknown, multiple)
  def matchImageSourceSeq(img: MyImage
                          , sourceSeq: Array[Segment2D]
                          , matchRadiusMas: Double
                          , isBlindSearch: Boolean = false
                          , applyFilterByRaDecError: Boolean = false
                          , verbose: Boolean):
  (Array[MatchedImageSource], Array[Segment2D], Array[Segment2D]) = {
    //-------------------------------------------------------------------------
    val unknownSourceSeq  = ArrayBuffer[Segment2D]()
    val knownSourceSeq    = ArrayBuffer[MatchedImageSource]()
    val multipleSourceSeq = ArrayBuffer[Segment2D]()
    //-------------------------------------------------------------------------
    val imageName = img.getRawName()
    if (sourceSeq.size == 0) {
      warning(s"Image: '$imageName' has no input sources, so can not be matched with catalog: '$catalogName'")
      return (knownSourceSeq.toArray, unknownSourceSeq.toArray, multipleSourceSeq.toArray)
    }

    //get the search matchRadiusMas
    val fits = img.getSimpleFits()
    val raDecObservingDate = img.getObservingDateMidPoint().toString

    //get the configuration parameters based on telescope
    val c = Telescope.getConfigurationFile(fits)
    val gaiaMaxAllowedRaError = c.getFloat("Photometry.gaia.maxAllowedRaError")
    val gaiaMaxAllowedDecError = c.getFloat("Photometry.gaia.maxAllowedDecError")
    val gaiaMaxAllowedRaProperMotionError = c.getFloat("Photometry.gaia.maxAllowedRaProperMotionError")
    val gaiaMaxAllowedDecProperMotionFactor = c.getFloat("Photometry.gaia.maxAllowedDecProperMotionError")
    //--------------------------------------------------------------------------
    def isValidByRaDecError(source: CatalogSource): Boolean =
        (source.raError <= gaiaMaxAllowedRaError) &&
        (source.decError <= gaiaMaxAllowedDecError) &&
        (Math.abs(source.pmra_error) <= gaiaMaxAllowedRaProperMotionError) &&
        (Math.abs(source.pmdec_error) <= gaiaMaxAllowedDecProperMotionFactor)
    //--------------------------------------------------------------------------
    if (verbose) info(s"Image: '$imageName' searching with match radius mas: ${f"$matchRadiusMas%.3f"} at: '$raDecObservingDate' and getting closest source")

    sourceSeq.foreach { case source =>

      if (isBlindSearch) unknownSourceSeq += source
      else {
        val imageSourceRaDec = source.getCentroidRaDec()
        val catalogSourceSeq = findClosestSourceSeq(
          imageSourceRaDec.x
          , imageSourceRaDec.y
          , raDecObservingDate
          , matchRadiusMas)

        catalogSourceSeq.length match {
          case 0 => unknownSourceSeq += source

          case 1 =>

            val catalogSource = catalogSourceSeq.head
            val matchedImageSource = MatchedImageSource(source, catalogSource)
            if (applyFilterByRaDecError) {
              if (isValidByRaDecError(catalogSource)) knownSourceSeq += matchedImageSource
              else unknownSourceSeq += source
            }
            else {
              matchedImageSource.imageSource.setCatalogSourceID(matchedImageSource.catalogSource._id)  //set in the image source the source ID of the catalog
              knownSourceSeq += matchedImageSource
            }

          case _ => multipleSourceSeq += source
        }
      }
    }

    if (verbose)
      info(s"Image: '$imageName' catalog: '$catalogName' has" +
        s" known sources:${knownSourceSeq.length}" +
        s" unknown sources:${unknownSourceSeq.length} and" +
        s" mutiple match sources: ${multipleSourceSeq.length}")

    (knownSourceSeq.toArray, unknownSourceSeq.toArray, multipleSourceSeq.toArray)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CatalogDB.scala
//=============================================================================