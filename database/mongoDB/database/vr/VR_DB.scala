/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Aug/2021
 * Time:  19h:04m
 * Description: Complete 2Mass database
 *
 */
//=============================================================================
package com.common.database.mongoDB.database.vr
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.{GenericObservable}
import com.common.database.mongoDB.MongoDB
//=============================================================================
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.bson.codecs.Macros._
//=============================================================================
//=============================================================================
object VR_DB {
  //---------------------------------------------------------------------------
  final val DEFAULT_VR  = 0.5d
  //---------------------------------------------------------------------------
  val codecRegistry = fromRegistries(fromProviders(classOf[VR]
                                                   , classOf[VR_Entry])
                                                   , DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  def apply(): VR_DB =
    VR_DB("mpo_catalog"
         , "vr")
  //---------------------------------------------------------------------------
  def apply(conf: MyConf =  MyConf(MyConf.c.getString("Database.mboss"))): VR_DB =
    VR_DB(conf.getString("Mboss.databaseName")
              , conf.getString("Mboss.collectionName"))
  //---------------------------------------------------------------------------
}
//=============================================================================
import VR_DB._
case class VR_DB(databaseName: String
                 , collectionName: String) extends MongoDB {
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = initDatabase()
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  val collWithRegistry: MongoCollection[VR] = database.getCollection(collectionName)
  //---------------------------------------------------------------------------
  connected = true
  //---------------------------------------------------------------------------
  private def initDatabase() = {
    if (codecRegistry != null) initWithCodeRegister(codecRegistry)
    else init()
  }
  //-------------------------------------------------------------------------
  def findByComposedName(composedName: String): Seq[VR] = {
    collWithRegistry.find(equal("_id", composedName)).results()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file VR_DB.scala
//=============================================================================
