/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  25/Mar/2024
 * Time:  17h:21m
 * Description: None
 */
package com.common.database.mongoDB.database.vr
//=============================================================================
//=============================================================================
case class VR_InitialEntry(id: String
                           , databaseName: String
                           , vr: Double
                           , vrError: Double
                           , reference: String)
//=============================================================================
object VR_Entry {
  //---------------------------------------------------------------------------
  private final val TOKEN_DIVIDER = ","
  final val CSV_HEADER = Seq("databaseName"
                             , "vr"
                             , "vrError"
                             , "reference").mkString(TOKEN_DIVIDER)
  //---------------------------------------------------------------------------
}
//=============================================================================
import VR_Entry._
case class VR_Entry(databaseName: String
                    , vr: Double
                    , vrError: Double
                    , reference: String) {
  //--------------------------------------------------------------------------
  def toCsv() = {
    Seq(databaseName
        , vr.toString
        , vrError.toString
        , reference).mkString(TOKEN_DIVIDER)

  }
  //--------------------------------------------------------------------------
}

//=============================================================================
//End of file VR_Entry.scala
//=============================================================================