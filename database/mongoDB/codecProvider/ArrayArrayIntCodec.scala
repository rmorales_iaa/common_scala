/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  06/Aug/2024
 * Time:  14h:20m
 * Description: None
 */
package com.common.database.mongoDB.database.codecProvider
//=============================================================================
import org.bson._
import org.bson.codecs._
import org.bson.codecs.configuration.{CodecProvider, CodecRegistry}
//=============================================================================
//=============================================================================
object ArrayArrayIntCodec extends CodecProvider {
  //---------------------------------------------------------------------------
  override def get[T](clazz: Class[T]
                      , registry: CodecRegistry): Codec[T] = {
    if (clazz == classOf[Array[Array[Int]]])
      new ArrayArrayIntCodec().asInstanceOf[Codec[T]]
    else null
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
class ArrayArrayIntCodec extends Codec[Array[Array[Int]]] {
  //---------------------------------------------------------------------------
  override def encode(writer: BsonWriter
                      , value: Array[Array[Int]]
                      , encoderContext: EncoderContext): Unit = {
    writer.writeStartArray()
    value.foreach { innerArray =>
      writer.writeStartArray()
      innerArray.foreach(writer.writeInt32)
      writer.writeEndArray()
    }
    writer.writeEndArray()
  }
  //---------------------------------------------------------------------------
  override def decode(reader: BsonReader, decoderContext: DecoderContext): Array[Array[Int]] = {
    val buffer = scala.collection.mutable.ArrayBuffer.empty[Array[Int]]
    reader.readStartArray()
    while (reader.readBsonType() != BsonType.END_OF_DOCUMENT) {
      val innerBuffer = scala.collection.mutable.ArrayBuffer.empty[Int]
      reader.readStartArray()
      while (reader.readBsonType() != BsonType.END_OF_DOCUMENT) {
        innerBuffer += reader.readInt32()
      }
      reader.readEndArray()
      buffer += innerBuffer.toArray
    }
    reader.readEndArray()
    buffer.toArray
  }
  //---------------------------------------------------------------------------
  override def getEncoderClass: Class[Array[Array[Int]]] =
    classOf[Array[Array[Int]]]
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ArrayArrayIntCodec.scala
//=============================================================================