/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  06/Aug/2024
 * Time:  14h:20m
 * Description: None
 */
package com.common.database.mongoDB.database.codecProvider
//=============================================================================
import org.bson._
import org.bson.codecs._
import org.bson.codecs.configuration.{CodecProvider, CodecRegistry}
//=============================================================================
//=============================================================================
object ArrayArrayDoubleCodecProvider extends CodecProvider {
  //---------------------------------------------------------------------------
  override def get[T](clazz: Class[T]
                      , registry: CodecRegistry): Codec[T] =
    if (clazz == classOf[Array[Array[Double]]])
      new ArrayArrayDoubleCodec().asInstanceOf[Codec[T]]
    else null
  //---------------------------------------------------------------------------
}
//=============================================================================
class ArrayArrayDoubleCodec extends Codec[Array[Array[Double]]] {
  //---------------------------------------------------------------------------
  override def encode(writer: BsonWriter
                      , value: Array[Array[Double]]
                      , encoderContext: EncoderContext): Unit = {
    writer.writeStartArray()
    value.foreach { innerArray =>
      writer.writeStartArray()
      innerArray.foreach(writer.writeDouble)
      writer.writeEndArray()
    }
    writer.writeEndArray()
  }
  //---------------------------------------------------------------------------
  override def decode(reader: BsonReader
                      , decoderContext: DecoderContext): Array[Array[Double]] = {
    val buffer = scala.collection.mutable.ArrayBuffer.empty[Array[Double]]
    reader.readStartArray()
    while (reader.readBsonType() != BsonType.END_OF_DOCUMENT) {
      val innerBuffer = scala.collection.mutable.ArrayBuffer.empty[Double]
      reader.readStartArray()
      while (reader.readBsonType() != BsonType.END_OF_DOCUMENT) {
        innerBuffer += reader.readDouble()
      }
      reader.readEndArray()
      buffer += innerBuffer.toArray
    }
    reader.readEndArray()
    buffer.toArray
  }
  //---------------------------------------------------------------------------
  override def getEncoderClass: Class[Array[Array[Double]]] =
    classOf[Array[Array[Double]]]
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ArrayArrayDoubleCodec.scala
//=============================================================================