//=============================================================================
//File: MongoDB.scala
//=============================================================================
/** It implements interface to MyMongoDB.
  //https://github.com/mongodb/mongo-scala-driver/blob/master/examples/src/test/scala/tour/Helpers.scala
  //http://mongodb.github.io/mongo-scala-driver/2.3/getting-started/quick-tour/
  //http://whiletrue.do/2017/04/24/some-examples-of-mongo-scala-driver-usage/
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    28 August 2017
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.database.mongoDB
//=============================================================================
// System import sectionName
//=============================================================================
import com.common.configuration.MyConf
import com.common.logger.MyLogger
//=============================================================================
import org.mongodb.scala.{MongoClientSettings, MongoCompressor}
import org.mongodb.scala._
import scala.collection.JavaConverters._
import scala.concurrent.duration.SECONDS
import scala.language.implicitConversions
import org.mongodb.scala.bson.conversions.Bson
import org.mongodb.scala.model.{BulkWriteOptions, Filters, IndexModel, IndexOptions, Indexes, InsertOneModel}
import org.mongodb.scala.model.Filters.{equal, exists, regex}
import org.mongodb.scala.model.Projections.{excludeId, fields, include}
import org.mongodb.scala.result.{DeleteResult, InsertManyResult, InsertOneResult}
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase, Observable, Observer}
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.model.Aggregates.{count, project}
import org.mongodb.scala.model.Updates.set
import com.mongodb.internal.build.MongoDriverVersion
//=============================================================================
// User import sectionName
//=============================================================================
import Helpers._
//=============================================================================
// Class/Object implementation
//=============================================================================
//http://mongodb.github.io/mongo-scala-driver/2.4/getting-started/quick-tour-admin/
object MongoDB  extends MyLogger {
  //-------------------------------------------------------------------------
  //column names
  final val MONGO_COL_ID = "_id"
  //-------------------------------------------------------------------------
  private final val URI                   = MyConf.c.getString("Database.mongoDB.uri")
  private final val MAX_IDLE_TIME_SECONDS = MyConf.c.getIntWithDefaultValue("Database.mongoDB.maxIdleTime", 300)
  private final val DATA_COMPRESSOR       = MyConf.c.getStringWithDefaultValue("Database.mongoDB.compressor","")
  //-------------------------------------------------------------------------
  private final val COMPRESSOR_SNAPPY     = "snappy"
  private final val COMPRESSOR_ZLIB       = "zlib"
  private final val COMPRESSOR_ZSTD       = "zstd"
  //-------------------------------------------------------------------------
  def getProjectionColList(colList:Seq[String] = Seq()): Bson = {
    if (colList.contains(MONGO_COL_ID)) include(colList: _*)
    else fields(include(colList: _*), excludeId())
  }

  //---------------------------------------------------------------------------
  implicit def getAsRowOfDoubles(doc: Document)  = (doc.toSeq map ( _._2.asDouble().getValue )).toArray
  //---------------------------------------------------------------------------
  implicit def getAsTableOfDoubles(docSeq: Seq[Document]) = (docSeq map (getAsRowOfDoubles(_))).toArray
  //-------------------------------------------------------------------------
  def printVersion() = info(s"Using mongo driver version: ${MongoDriverVersion.VERSION}")
  //---------------------------------------------------------------------------
}
//=============================================================================
import MongoDB._
trait MongoDB extends MyLogger {
  //-------------------------------------------------------------------------
  val databaseName: String
  val collectionName: String
  //-------------------------------------------------------------------------
  // class variable
  //-------------------------------------------------------------------------
  protected val database: MongoDatabase
  protected val client: MongoClient
  protected val collection: MongoCollection[Document]
  protected var connected: Boolean = false
  protected var databaseNameSeq : Array[String] = Array[String]()
  //-------------------------------------------------------------------------
  private def getCompressor(optCompressor: Option[String]): List[MongoCompressor] = {
    val compressor = optCompressor.getOrElse(DATA_COMPRESSOR.trim)
    if (compressor.isEmpty) List()
    else
      compressor match {
        case COMPRESSOR_SNAPPY =>
          List(MongoCompressor.createSnappyCompressor)
        case COMPRESSOR_ZLIB =>
          List(MongoCompressor.createZlibCompressor)
        case COMPRESSOR_ZSTD =>
          List(MongoCompressor.createZstdCompressor)
        case _ =>
          List(MongoCompressor.createSnappyCompressor)  //default case
      }
  }

  //-------------------------------------------------------------------------
  private def getIdleTime(idleTime: Option[Int]): Int =
    idleTime.getOrElse(MAX_IDLE_TIME_SECONDS)
  //-------------------------------------------------------------------------
  private def getSettings(optUri: Option[String] = None
                         , optCompressor: Option[String] = None
                         , optIdleTimeSeconds: Option[Int] = None) = {

    val uri =
      if (optUri.isDefined) {
        optUri.get
      }
      else URI

    assert(uri != null, s"Connecting with Mongo database '$databaseName' and collection '$collectionName' URI is null")

    MongoClientSettings.builder()
      .compressorList(getCompressor(optCompressor).asJava)
      .applyConnectionString(new ConnectionString(uri))
      .applyToConnectionPoolSettings(builder => builder.maxConnectionIdleTime(getIdleTime(optIdleTimeSeconds), SECONDS))
      .build()
  }
  //-------------------------------------------------------------------------
  protected def init(showInfo: Boolean = false
                     , uriOpt: Option[String] = None
                     , optCompressor: Option[String] = None
                     , optIdleTimeSeconds: Option[Int] = None):
                  (MongoClient, MongoDatabase, MongoCollection[Document]) = {
    if (showInfo) info(s"Connecting with Mongo database '$databaseName' and collection '$collectionName'" )
    val c  = MongoClient(getSettings(uriOpt, optCompressor, optIdleTimeSeconds))
    val d = c.getDatabase(databaseName)
    databaseNameSeq = c.listDatabaseNames.getResultDocumentStringSeq.toArray
    (c, d, d.getCollection(collectionName))
  }
  //-------------------------------------------------------------------------
  protected def initWithCodeRegister(codecRegistry: CodecRegistry
                                     , showInfo: Boolean = false
                                     , uriOpt: Option[String] = None
                                     , optCompressor: Option[String] = None
                                     , optIdleTimeSeconds: Option[Int] = None):
    (MongoClient, MongoDatabase, MongoCollection[Document]) = {
    if (showInfo) info(s"Connecting with Mongo database '$databaseName' and collection '$collectionName' and codec register" )
    val client  = MongoClient(getSettings(uriOpt, optCompressor, optIdleTimeSeconds))

    val database = client.getDatabase(databaseName).withCodecRegistry(codecRegistry)
    val collection = database.getCollection(collectionName)
    databaseNameSeq = client.listDatabaseNames.getResultDocumentStringSeq.toArray
    (client, database, collection)
  }
  //-------------------------------------------------------------------------
  def isConnected(reportOn: Boolean = true) : Boolean = {
    if(client == null) {
      if (reportOn) warning(s"MongoDB client of database: '$databaseName' is not initialized")
      return false
    }
    try{
      if (reportOn) info(s"MongoDB '$databaseName' database count: ${databaseNameSeq.length}")
      if (reportOn) info(s"MongoDB '$databaseName' database connected")
      true
    }
    catch {
      case e:Exception => exception(e, " Error in MongoDB connection.")
    }
  }
  //-------------------------------------------------------------------------
  def close(verbose: Boolean = false)   {
    if (connected) {
      if (verbose) info(s"Closing MongoClient with collection name: '$collectionName' ")
      client.close
      if (verbose) info(s"Closed MongoClient with collection name: '$collectionName' ")
      connected = false
    }
  }
  //-------------------------------------------------------------------------
  def getCollection = collection
  //-------------------------------------------------------------------------
  def getDatabaseNameSeq = databaseNameSeq
  //-------------------------------------------------------------------------
  def getConfiguration: Map[String, String] = {
    val uriString =  URI.split("@")
    val host = uriString(1).split("/")(0)
    val remain = uriString(0).split("//")(1)
    val user = remain.split(":")(0)
    val pass = remain.split(":")(1)

    Map("host" -> host
      , "user" -> user
      , "password" -> pass
      , "database" -> databaseName
      , "collection" -> collectionName)
  }
  //---------------------------------------------------------------------------
  def getBasicConnectionInfo(coll: MongoCollection[Document] = collection) =
    s"'$databaseName.${coll.namespace.getCollectionName}'"
  //---------------------------------------------------------------------------
  def getCollection(name:String) = database.getCollection(name)
  //---------------------------------------------------------------------------
  def dropCollection(coll: MongoCollection[Document] = collection) = {
    info(s"Dropping collection from Mongo database: ${getBasicConnectionInfo(coll)}")
    coll.drop().headResult()
  }
  //---------------------------------------------------------------------------
  def dropAllCollectionStartingWith(prefix:String) = {
    database.listCollectionNames().getResultDocumentStringSeq.foreach { collName=>
      if (collName.startsWith(prefix))
        dropCollection(database.getCollection(collName))
    }
  }
  //---------------------------------------------------------------------------
  def dropDatabase() = {
    info(s"Dropping database from Mongo database: '$databaseName'")
    database.drop().headResult()
  }

  //---------------------------------------------------------------------------
  def clearDocumentSeq() = {
    info(s"Deleting all docuemnts from Mongo database: '$databaseName'")
    collection.deleteMany(Document()).headResult()
  }
  //-------------------------------------------------------------------------
  def getAllDocuments(coll: MongoCollection[Document] = collection
                      , projectionFieldSeq: Seq[String] = Seq()) =
    coll.find(Document()).projection(include(projectionFieldSeq: _*)).results()
  //-------------------------------------------------------------------------
  def findById(id: Long): Document = collection.find(equal(MONGO_COL_ID, id)).headResult()
  //-------------------------------------------------------------------------
  def findById(id: String): Document = collection.find(equal(MONGO_COL_ID, id)).headResult()
  //-------------------------------------------------------------------------
  def findById(id: String, coll: MongoCollection[Document] = collection): Document =
    coll.find(equal(MONGO_COL_ID, id)).headResult()
  //------------------------------------------------------------------------
  def deleteById(id: String): Seq[DeleteResult] = collection.deleteOne(Document(MONGO_COL_ID -> id)).results
  //-------------------------------------------------------------------------
  def tryToFind(id: Long) = {
    val r = collection.find(equal(MONGO_COL_ID, id)).results()
    if (r.isEmpty || r.size > 1) None
    else Some(r.head)
  }
  //-------------------------------------------------------------------------
  def dropById(id: String): Seq[DeleteResult] = collection.deleteOne(Document(MONGO_COL_ID -> id)).results
  //-------------------------------------------------------------------------
  def exist(id: String, coll: MongoCollection[Document] = collection): Boolean =
    coll.find(equal(MONGO_COL_ID, id)).resultCount > 0
  //-------------------------------------------------------------------------
  def existDocument(docName: String, coll: MongoCollection[Document] = collection): Boolean =
    coll.find(exists(docName)).resultCount() != 0
  //-------------------------------------------------------------------------
  def findByPattern(fieldName: String
                    , pattern : String
                    , projectionFieldSeq: Seq[String] = Seq()) =
    collection.find(regex(fieldName,pattern))
      .projection(include(projectionFieldSeq: _*)).results()
  //-------------------------------------------------------------------------
  def insert(doc: Document, coll: MongoCollection[Document] = collection): Unit = {
    val insertObservable: Observable[InsertOneResult] = coll.insertOne(doc)
    insertObservable.subscribe(new Observer[InsertOneResult] {
      //---------------------------------------------------------------------
      override def onNext(result: InsertOneResult): Unit = {}
      //---------------------------------------------------------------------
      override def onError(e: Throwable): Unit = {
        error(s"Error inserting in the collection '$collectionName'" + doc)
        error(e.getMessage)
      }
      //---------------------------------------------------------------------
      override def onComplete(): Unit = {}
      //---------------------------------------------------------------------
    })
  }
  //-------------------------------------------------------------------------
  def insertMany(docSeq: IndexedSeq[Document], coll: MongoCollection[Document] = collection): Unit = {
    val insertObservable: Observable[InsertManyResult] = coll.insertMany(docSeq.toArray)
    insertObservable.subscribe(new Observer[InsertManyResult] {
      //---------------------------------------------------------------------
      override def onNext(result: InsertManyResult): Unit = {}
      //---------------------------------------------------------------------
      override def onError(e: Throwable): Unit = error(s"Error inserting in the collection '$collectionName'" + docSeq)
      //---------------------------------------------------------------------
      override def onComplete(): Unit = {}
      //---------------------------------------------------------------------
    })
  }
  //-------------------------------------------------------------------------
  def createIndex(collNameSeq: String, coll: MongoCollection[Document] = collection, _unique: Boolean = true, ascending: Boolean = true): Unit =
    createIndexes(Array(collNameSeq), coll, _unique, ascending)
  //-------------------------------------------------------------------------
  def createIndexes(collNameSeq: Array[String]
                    , coll: MongoCollection[Document] = collection
                    , _unique: Boolean = true
                    , ascending: Boolean = true): Unit =
    coll.createIndexes(
      Array(
        IndexModel(
          if (ascending) Indexes.ascending(collNameSeq:_*) else Indexes.descending(collNameSeq:_*),
          IndexOptions().background(false).unique(_unique)
        ))).results()
  //-------------------------------------------------------------------------
  def existDatabase(s: String): Boolean = databaseNameSeq.contains(s)
  //-------------------------------------------------------------------------
  def existCollection(s: String): Boolean = database.listCollectionNames().getResultDocumentStringSeq.contains(s)
  //-------------------------------------------------------------------------
  def getCollectionNameSeq = database.listCollectionNames().results()
  //-------------------------------------------------------------------------
  def insertBulk(docSeq: Array[Document]
                 , collName: String = collectionName
                 , indexFieldNameSeq: Seq[String] = Seq()
                 , dropCollection: Boolean = true): Boolean = {
    val coll = database.getCollection(collName)
    if (dropCollection && existCollection(collName)) {
      warning(s"The collection: '$collName' is already stored in database '${database.name}'. It will be removed and updated with the NEW content")
      coll.drop().results()
    }
    val r = coll.bulkWrite(docSeq.map(InsertOneModel( _ )).toList, BulkWriteOptions().ordered(false)).results()
    val insertedCount = r.head.getInsertedCount
    if (insertedCount ==  docSeq.size) {
      indexFieldNameSeq.foreach { name=>
        coll.createIndexes(
          Array(
            IndexModel(
              Indexes.ascending(name),
              IndexOptions().background(false).unique(true)
            ))).results()
      }
      info(s"Created the collection '$collName' in database '${database.name} with ${docSeq.size} documents")
    }
    else
      error(s"Error creating the collection '$collName' in database '${database.name} with ${docSeq.size} documents. Only $insertedCount were inserted")
  }
  //---------------------------------------------------------------------------
  def arraySize() = collection.aggregate( List(count()) ).results()
  //---------------------------------------------------------------------------
  def subArraySize(subArrayCollectionName:String, coll: MongoCollection[Document] = collection) =  {
    //https://stackoverflow.com/questions/48119916/mongodb-aggregate-query-in-scala
    val tempFieldName = "_tmp"
    val doc = coll.aggregate(
      List(
        project(
          fields(
            excludeId()
          , Document(tempFieldName -> Document("$size" ->  ("$" + subArrayCollectionName)))
          )
        )
      )
    ).results().head
    doc(tempFieldName).asInt32().getValue
  }
  //---------------------------------------------------------------------------
 def arrayRemoveBytIndex(arrayCollectionaName: String, i: Int) =
   collection.updateOne(
     Filters.empty()
     , set(arrayCollectionaName + "." + i, null)
    ).results()

  //---------------------------------------------------------------------------
  def getNumberOfCollections = collection.find().resultCount()
  //---------------------------------------------------------------------------
  def getDocumentCount() = collection.countDocuments().results()
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
}
//=============================================================================
// End of 'MongoDB.scalala' file
//=============================================================================
