/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  02/nov./2019 
 * Time:  11h:43m
 * Description: None
 */
//=============================================================================

//=============================================================================

//=============================================================================
//=============================================================================
//=============================================================================
//File: MyMongoDBDB.scala
//=============================================================================
/** It implements interface to MyMongoDB.
  //https://github.com/mongodb/mongo-scala-driver/blob/master/examples/src/test/scala/tour/Helpers.scala
  //http://mongodb.github.io/mongo-scala-driver/2.3/getting-started/quick-tour/
  //http://whiletrue.do/2017/04/24/some-examples-of-mongo-scala-driver-usage/
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    28 August 2017
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.database.mongoDB
//=============================================================================
// System import sectionName
//=============================================================================
import java.util.concurrent.TimeUnit
import org.mongodb.scala.bson.BsonValue
import org.mongodb.scala.{Document, Observable}
import scala.concurrent.Await
import scala.concurrent.duration.Duration
//=============================================================================
// User import sectionName
//=============================================================================
//=============================================================================
// Class/Object implementation
//=============================================================================
object Helpers {
  //-------------------------------------------------------------------------
  final val OPERATION_MAX_TIME_S = 60 * 60
  //-------------------------------------------------------------------------
  implicit class DocumentObservable[C](val observable: Observable[Document]) extends ImplicitObservable[Document] {
    override val converter: (Document) => String = (doc) => doc.toJson()
  }
  //-------------------------------------------------------------------------
  implicit class GenericObservable[C](val observable: Observable[C]) extends ImplicitObservable[C] {
    override val converter: (C) => String = doc => doc.toString
  }
  //-------------------------------------------------------------------------
  trait ImplicitObservable[C] {
    //-------------------------------------------------------------------------
    val observable: Observable[C]
    //-------------------------------------------------------------------------
    val converter: (C) => String
    //-------------------------------------------------------------------------
    def results(): Seq[C] = Await.result(observable.toFuture(), Duration(OPERATION_MAX_TIME_S, TimeUnit.SECONDS))
    //-------------------------------------------------------------------------
    def headResult(): C = Await.result(observable.head(), Duration(OPERATION_MAX_TIME_S, TimeUnit.SECONDS))
    //-------------------------------------------------------------------------
    def resultCount(): Int = results().size
    //-------------------------------------------------------------------------
    def printResults(initial: String = ""): Unit = {
      if (initial.length > 0) print(initial)
      results().foreach(res => println(converter(res)))
    }
    //-------------------------------------------------------------------------
    def printHeadResult(initial: String = ""): Unit = println(s"$initial${converter(headResult())}")
    //-------------------------------------------------------------------------
    def getResultDocumentStringSeq: Seq[String] = results().map(s => converter(s))
    //-------------------------------------------------------------------------
    def getResultDocumentSeq: Seq[C] = results()
    //-------------------------------------------------------------------------
    def getResultValueSeq: Seq[BsonValue] =
      getResultDocumentSeq.flatMap(_.asInstanceOf[Document].map(_._2))
    //-------------------------------------------------------------------------
    def getResultLongValueSeq(): Seq[Long] = getResultValueSeq.map(_.asInt64.getValue)
    //-------------------------------------------------------------------------
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Helper.scala
//=============================================================================