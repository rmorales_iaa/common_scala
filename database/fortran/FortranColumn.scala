/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  11/Aug/2021
  * Time:  19h:58m
  * Description: https://pages.mtu.edu/~shene/COURSES/cs201/NOTES/chap05/format.html
  */
//=============================================================================
package com.common.database.mongoDB.fortran
//=============================================================================
import org.mongodb.scala.bson.{BsonDouble, BsonInt32, BsonInt64, BsonString, BsonValue}
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object FortranColumn {
  //---------------------------------------------------------------------------
  def apply(name: String, format:String): FortranColumn = {
    val seq = format.trim.split(" ")
    if (seq.size == 1) new FortranColumn(name,seq.head)
    else {
      val seq2 = seq.last.split("-")
      val m = seq2.head.toInt
      val M = if (seq2.size == 1) m else seq2.last.toInt
      FortranColumn(name, seq(0), m, M)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class FortranColumn(name: String, fortranFormat: String, startingCol: Int = -1, endCol: Int = -1) {
  //------------------------------------------------------------------------
  val dataType = getDataType
  val charSize = getCharSize
  val colSize =  endCol - startingCol
  //------------------------------------------------------------------------
  private def getDataType() = {
    fortranFormat match {
      case "1x" => "string"
      case s if s.startsWith("A") => "string"
      case s if s.startsWith("I") =>
        if (s.drop(1).toInt > 7) "long"
        else "int"
      case s if s.startsWith("F") => "double"
      case s if s.startsWith("E") => "double"
    }
  }

  //------------------------------------------------------------------------
  def getCharSize() = {

    if(startingCol != -1)  endCol - startingCol + 1
    else
      fortranFormat match {
        case "1x" => 1
        case s if s.startsWith("A") => s.drop(1).toInt
        case s if s.startsWith("I") => s.drop(1).split("\\.").head.toInt
        case s if s.startsWith("F") => s.drop(1).split("\\.").head.toInt
        case s if s.startsWith("E") => s.drop(1).split("\\.").head.toInt
    }
  }
  //------------------------------------------------------------------------
  def addColumnToRow(value: String, row: ArrayBuffer[(String, BsonValue)]) = {
    dataType match {
      case "double" =>
        if (value.isEmpty) row += (name -> new BsonDouble(Double.NaN))
        else row += (name -> new BsonDouble(value.toDouble))
      case "int" =>
        if (value.isEmpty) row += (name -> new BsonInt32(-1))
        else row += (name -> new BsonInt32(value.toInt))
      case "long" =>
        if (value.isEmpty) row += (name -> new BsonInt64(-1))
        else row += (name -> new BsonInt64(value.toLong))
      case "string" =>
        if (value.isEmpty) row += (name -> new BsonString(""))
        else row += (name -> new BsonString(value.replaceAll("'","")))
    }
  }
  //------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file FortranColumn.scala
//=============================================================================
