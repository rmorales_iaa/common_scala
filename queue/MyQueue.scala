//=============================================================================
//File: MyQueue.scala
//=============================================================================
/** It implements a concurrent safe processing queue
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.queue

//=============================================================================
// System import sectionName
//=============================================================================
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.atomic.AtomicBoolean
//=============================================================================
// User import sectionName
//=============================================================================
import com.common.util.thread.MyThread
import com.common.util.thread.MyThread._
//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================

//http://alvinalexander.com/scala/differences-java-thread-vs-scala-future

trait MyQueue[T] {

  val name : String
  val maxItemCount : Int = 1024
  val firstSleepTimeMs : Int = 0          //no wait
  val waitingTimeBetweenItemMs : Int = 0  //no wait
  val processAllQueuedItemBeforeClose: Boolean= true
  val autostart: Boolean= true
  val waitingTimeWhenStopping : Int = 0
  val waitingTimeBeforeReTryingAddMs: Int = 50
  //-------------------------------------------------------------------------
  // Variable declaration
  //-------------------------------------------------------------------------

  //queue of items
	private val queue=new ConcurrentLinkedQueue [T]()
	 
	//thread run
	private var threadRun : Thread = null
	
  //flags
	protected var closing = new AtomicBoolean(false)
	protected var paused = new AtomicBoolean(false)
	 
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
	if (autostart) start
  //-------------------------------------------------------------------------
  def processItem(t:T): Unit  //ABSTRACT
 	//-------------------------------------------------------------------------	
	def printQueueMessage(s:String): Unit //ABSTRACT
	//-------------------------------------------------------------------------
	def start(): Unit = {threadRun = myThread(run,"Queue "+name)}
	//-------------------------------------------------------------------------
	def pauseStart(): Unit = paused.set(true)
	//-------------------------------------------------------------------------
	def pauseEnd() : Unit = paused.set(false)
  //-------------------------------------------------------------------------
	def size: Int = queue.size
	//-------------------------------------------------------------------------
  def isClosing = closing.get
	//-------------------------------------------------------------------------
  def isEmpty = queue.size == 0
	//-------------------------------------------------------------------------	
	private def process(t:T): Unit = if (t != null) processItem(t)
	//-------------------------------------------------------------------------
	private def mySleep(timeoutMs: Long) = MyThread.mySleep(name,timeoutMs)
	//-------------------------------------------------------------------------
	private def myAwake : Boolean = { myNotifyAll(threadRun);true}
  //-------------------------------------------------------------------------
	private def myWait = MyThread.myWait(threadRun)
	//-------------------------------------------------------------------------
	private def myJoin(timeoutMs: Long) = MyThread.myJoin(threadRun,timeoutMs)
  //-------------------------------------------------------------------------
	private def run() : Unit = {
	  
    printQueueMessage(s"The processing of items on queue '$name' has started")
	   
	  //first wait
	  Thread.sleep(firstSleepTimeMs)
	   
	  while (!closing.get){
	    if (queue.isEmpty) myWait
	    if(!paused.get) processAllQueuedItem
	  }
    
	  printQueueMessage(s"The processing of items on queue '$name' has stopped")
	}
	//-------------------------------------------------------------------------
  def close() : Unit = {
	  
	  if (!closing.get){  //avoid multiple closings
	    closing.set(true)	    
	    myAwake //awake threadRun
	
	    printQueueMessage(s"Closing queue '$name'")
	    
	    if (processAllQueuedItemBeforeClose){    

	      //wait before to interrupt
        myJoin(waitingTimeWhenStopping)
	      if (threadRun.isAlive()) threadRun.interrupt

	      //calculate remain items
	      if (queue.size >0) {
	        printQueueMessage(s"Queue: '$name' processing "+queue.size +" remain items before closing")
	        processAllQueuedItem    	        
	      }
	    }
	    
	    printQueueMessage(s"Queue '$name' has been closed")
	  }
	  else printQueueMessage(s"Error: queue '$name' is already closed")
	}
	
  //-------------------------------------------------------------------------
	def +=(t:T) : Boolean = {
	  
	  if (queue.size >= maxItemCount){
	     printQueueMessage(s"Queue: '$name' is full, ignoring item '"+t.toString().trim+"'") 
	     return false
	  }	
	  
	  //write in the file
	  synchronized{
	    while(!queue.add(t))
	      Thread.sleep(waitingTimeBeforeReTryingAddMs)//possibly re-starting file  
	  }
      
	  myAwake //awake threadRun
	}
	
  //-------------------------------------------------------------------------
	private def processAllQueuedItem() = {
	  
    while(!queue.isEmpty){	      
      process(queue.poll)
 	    if (waitingTimeBetweenItemMs > 0) mySleep(waitingTimeBetweenItemMs) 	      
	  }
  }
	
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //End of object 'MyQueue'

//=============================================================================
// End of 'MyQueue.scala' file
//=============================================================================
