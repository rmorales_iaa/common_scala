/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  15/Jan/2021
 * Time:  12h:26m
 * Description: None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.plot.bmpImage
//=============================================================================
import javax.imageio.ImageIO
import java.awt.{BasicStroke, Color, Font}
import java.awt.image.BufferedImage
import java.io.File
//=============================================================================
import com.common.geometry.circle.CircleDouble
import com.common.geometry.point.{Point2D, Point2D_Double}
import com.common.geometry.rectangle.Rectangle
import com.common.geometry.triangle.TriangleDouble
//=============================================================================
//=============================================================================
object PlotDimensions {
  //---------------------------------------------------------------------------
  def apply(posSeq: Array[Point2D], plotBorder: Point2D = Point2D(200,100)) : PlotDimensions = {
    //-------------------------------------------------------------------------
    var minPos = Point2D(Int.MaxValue, Int.MaxValue)
    var maxPos = Point2D(Int.MinValue, Int.MinValue)
    //-------------------------------------------------------------------------
    def updatePlotMinMaxSite(p: Point2D) = {
      if (p.x < minPos.x) minPos = Point2D(p.x, minPos.y)
      if (p.y < minPos.y) minPos = Point2D(minPos.x, p.y)
      if (p.x > maxPos.x) maxPos = Point2D(p.x, maxPos.y)
      if (p.y > maxPos.y) maxPos = Point2D(maxPos.x, p.y)
    }
    //-------------------------------------------------------------------------
    //calculate the min and max values of all representable dimensions (not NaNm, not inifinite) of all points
    //append the site sequence
    posSeq.foreach { p => updatePlotMinMaxSite(p) }

    //calculate dimensions
    val plotPointRange = maxPos - minPos + Point2D.POINT_ONE //one more to accomdate the full range
    val plotMaxX = plotPointRange.x + (plotBorder.x * 2) //plotMaxX : left and right border
    val plotMaxY = plotPointRange.y + (plotBorder.y * 2) //plotMaxY : top and bottom border
    val plotRange = Point2D(plotMaxX,plotMaxY) + Point2D.POINT_ONE //one more to accomdate the full range
    val centreOffset = (Point2D_Double(plotRange - plotPointRange) * 0.5).toPoint2D
    val plotPointOffset = minPos.inverse + centreOffset  //centre the points

    PlotDimensions(plotPointRange  //Plot range
      , plotPointOffset // Plot offset to centrate the points in the plot (according with borders)
      , plotMaxX
      , plotMaxY
    )
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class PlotDimensions(plotPointRange: Point2D
                          , plotPointOffset: Point2D
                          , plotMaxX: Int
                          , plotMaxY: Int)
//=============================================================================
case class BmpImagePlot(var pixSizeX: Int
                        , var pixSizeY : Int
                        , offset: Point2D = Point2D.POINT_ZERO
                        , plotType: Int = BufferedImage.TYPE_INT_RGB
                        , imageName: Option[String] = None) {
  //---------------------------------------------------------------------------
  private val bmpImage = getBmpImage
  private val g2d = bmpImage.createGraphics()
  private val lastPixIndexX = pixSizeX - 1
  private val lastPixIndexY = pixSizeY - 1
  //---------------------------------------------------------------------------
  def getBmpImage() = {
    if (imageName.isEmpty) new BufferedImage(pixSizeX, pixSizeY, plotType)
    else {
      val png = ImageIO.read(new File(imageName.get))
      pixSizeX = png.getWidth
      pixSizeY = png.getHeight
      png
    }
  }
  //---------------------------------------------------------------------------
  def restetToColor(color: Color = Color.BLACK) = {
    for (x <- 0 until pixSizeX; y <- 0 until pixSizeY)
      bmpImage.setRGB(x, y, color.getRGB)
  }
  //-------------------------------------------------------------------------
  def getX_Pos(x: Double) : Int = getX_Pos(Math.round(x).toInt)
  //-------------------------------------------------------------------------
  def getX_Pos(x: Int) = Math.min(Math.max(0, x + offset.x),lastPixIndexX)  //be sure that is in the plot
  //-------------------------------------------------------------------------
  def getY_Pos(y: Double) : Int = getY_Pos(Math.round(y).toInt)
  //-------------------------------------------------------------------------
  def getY_Pos(y : Int) = lastPixIndexY - Math.min(Math.max(0, y + offset.y), lastPixIndexY)   //y = 0 axis is at top left, so invert it
  //---------------------------------------------------------------------------
  def getColor(x: Int, y: Int) = bmpImage.getRGB(x,y)
  //---------------------------------------------------------------------------
  def setFont(f: Font)  = g2d.setFont(f)
  //---------------------------------------------------------------------------
  def setColor(c: Color)  = g2d.setColor(c)
  //---------------------------------------------------------------------------
  def setLineWidth(width: Float)  =  g2d.setStroke(new BasicStroke(width))  //line width
  //---------------------------------------------------------------------------
  def string(s: String, p: Point2D, c: Option[Color] = None, f: Option[Font] = None) = {
    if (c.isDefined) setColor(c.get)
    if (f.isDefined) setFont(f.get)
    g2d.drawString(s, getX_Pos(p.x), getY_Pos(p.y))
  }
  //---------------------------------------------------------------------------
  def pixel(p: Point2D, color: Color) =
    bmpImage.setRGB(getX_Pos(p.x), getY_Pos(p.y),  color.getRGB)
  //---------------------------------------------------------------------------
  def pixel(x: Int, y: Int, color: Color) =
    bmpImage.setRGB(getX_Pos(x), getY_Pos(y),  color.getRGB)
  //---------------------------------------------------------------------------
  def rectangleByPosAndRadius(p: Point2D
                            , radius: Int
                            , innerColor: Option[Color] = None
                            , borderColor: Option[Color] = None) : Unit = {
    val r = Rectangle(p.x-radius, p.y-radius , p.x+radius, p.y+radius)
    rectangle(r ,innerColor, borderColor)
  }
  //---------------------------------------------------------------------------
  def rectangle(r: Rectangle
              , innerColor: Option[Color] = None
              , borderColor: Option[Color] = None
              , solid: Boolean = true) : Unit = {
    val ci = if (innerColor.isDefined) innerColor.get else g2d.getColor
    val cb = if (borderColor.isDefined) borderColor.get else g2d.getColor
    val min = r.min
    val max = r.max
    for(x<- r.xMin to r.xMax;y<- r.yMin to r.yMax) {
      val isInTheBorder = (x == min.x) || (y == min.y) || (x == max.x) || (y == max.y)
      if (isInTheBorder) pixel(x,y,cb)
      else if (solid) pixel(x,y,ci)
    }
  }
  //---------------------------------------------------------------------------
  def circle(c: CircleDouble
           , innerColor: Option[Color] = None
           , borderColor: Option[Color] = None) = {
    val min = c.getMin.toPoint2D
    val max = c.getMax.toPoint2D
    val ci = if (innerColor.isDefined) innerColor.get else g2d.getColor
    val cb = if (borderColor.isDefined) borderColor.get else g2d.getColor
    for(x<- min.x to max.x ;y<- min.y to max.y) {
      val isInTheBorder = (x == min.x) || (y == min.y) || (x == max.x) || (y == max.y)
      if (c.isIn(Point2D(x,y))){
        if (isInTheBorder) pixel(x,y,cb)
        else pixel(x,y,ci)
      }
    }
  }
  //-------------------------------------------------------------------------
  def segment(pStart: Point2D
              , pEnd: Point2D
              , c: Option[Color] = None
              , lastPointColor: Option[Color] = None): Unit =
    line(pStart, pEnd,c, lastPointColor)
  //-------------------------------------------------------------------------
  def segmentBasic(pStartX: Int
                  , pStartY: Int
                  , pEndX: Int
                  , pEndY: Int
                  , c: Option[Color] = None
                  , lastPointColor: Option[Color] = None) : Unit =
    segment(Point2D(pStartX, pStartY), Point2D(pEndX,pEndY), c, lastPointColor)
  //-------------------------------------------------------------------------
  def line(pStart: Point2D, pEnd: Point2D, c: Option[Color] = None, lastPointColor: Option[Color] = None) = {
    if (c.isDefined) setColor(c.get)
    if (lastPointColor.isDefined) {
      g2d.drawLine(getX_Pos(pStart.x), getY_Pos(pStart.y), getX_Pos(pEnd.x-1), getY_Pos(pEnd.y - 1))
      pixel(pEnd.x, pEnd.y, lastPointColor.get)
    }
    else g2d.drawLine(getX_Pos(pStart.x), getY_Pos(pStart.y), getX_Pos(pEnd.x), getY_Pos(pEnd.y))
  }
  //-------------------------------------------------------------------------
  def verticalLine(p: Point2D, max: Int, c: Option[Color] = None, lastPointColor: Option[Color] = None) = {
    if (c.isDefined) setColor(c.get)
    if (lastPointColor.isDefined) {
      g2d.drawLine(getX_Pos(p.x), getY_Pos(p.y), getX_Pos(p.y), getY_Pos(p.y + max - 1))
      pixel(p.x, p.y + max, lastPointColor.get)
    }
    else g2d.drawLine(getX_Pos(p.x), getY_Pos(p.y), getX_Pos(p.x), getY_Pos(p.y + max -1))
  }
  //-------------------------------------------------------------------------
  def horizontaline(p: Point2D, max: Int, c: Option[Color] = None, lastPointColor: Option[Color] = None) = {
    if (c.isDefined) setColor(c.get)
    if (lastPointColor.isDefined) {
      g2d.drawLine(getX_Pos(p.x), getY_Pos(p.y), getX_Pos(p.x + max - 1), getY_Pos(p.y))
      pixel(p.x + max, p.y, lastPointColor.get)
    }
    else g2d.drawLine(getX_Pos(p.x), getY_Pos(p.y), getX_Pos(p.x + max - 1), getY_Pos(p.y))
  }
  //-------------------------------------------------------------------------
  def triangle(t: TriangleDouble, c: Color = Color.WHITE) = {
    val a = t.a.toPoint2D
    val b = t.b.toPoint2D
    val c = t.c.toPoint2D
    g2d.drawLine(getX_Pos(a.x), getY_Pos(a.y), getX_Pos(b.x), getX_Pos(b.y))
    g2d.drawLine(getX_Pos(a.x), getY_Pos(a.y), getX_Pos(c.x), getX_Pos(c.y))
    g2d.drawLine(getX_Pos(b.x), getY_Pos(b.y), getX_Pos(c.x), getX_Pos(c.y))
  }
  //-----------------------------------------------------------------------
  //rectangle with an optional string string
  def site(p: Point2D
          , siteRadius: Int = 1
          , c: Color = Color.BLUE
          , f: Option[Font] = Some(new Font(Font.MONOSPACED, Font.PLAIN, 10))
          , fontOffset : Point2D = Point2D(-20,8)): Unit =  {
    rectangleByPosAndRadius(p,siteRadius, Some(c), Some(c))
    if (f.isDefined) string(p.toString, p + fontOffset, Some(c), f)
  }
  //---------------------------------------------------------------------------
  def siteBasic(x: Int
                , y: Int
                , siteRadius: Int = 1
                , c: Color = Color.BLUE
                , f: Option[Font] = Some(new Font(Font.MONOSPACED, Font.PLAIN, 10))
                , fontOffset: Point2D = Point2D(-20, 8)): Unit =
    site(Point2D(x,y), siteRadius, c, f, fontOffset)
  //---------------------------------------------------------------------------
  def save(filename: String, imageFormat: String = "png"): Unit = {
    val name = if (filename.endsWith("." + imageFormat)) filename else filename + "." + imageFormat
    ImageIO.write(bmpImage, imageFormat, new File(name))
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file BmpImagePlot.scala
//=============================================================================
