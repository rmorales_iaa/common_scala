/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  11/Nov/2020
 * Time:  14h:11m
 * Description: http://maxima.sourceforge.net/
 */
//=============================================================================
package com.common.plot.maxima
//=============================================================================
import com.common.geometry.circle.CircleDouble
import com.common.geometry.point.Point2D_Double
import com.common.geometry.triangle.TriangleDouble
//=============================================================================
import scala.reflect.ClassTag
//=============================================================================
//=============================================================================
object MaximaPlot {
  //---------------------------------------------------------------------------
  private val RANDOM_COLOR = Array("skyblue", "aquamarine", "aquamarine", "khaki", "goldenrod"
                                   , "orange", "violet", "forest_green", "light_yellow", "turquoise")
  //---------------------------------------------------------------------------
  def getPlot2D[T:ClassTag](
    a: Array[T]
    , name: String = "point_list"
    , colCount : Int = 5): String = {
    var r = s"$name : [discrete,[ \n"
    var k = 0
    a.grouped(colCount).foreach { t =>
       t.foreach { w=> r += "[" + k + ","+ w + "],"; k+=1}
       r += "\n"
    }
    r.dropRight(2) + "\n" + "] ];\n" +
    s"plot2d($name, [style,[lines]]);\n"
  }
  //---------------------------------------------------------------------------
  def getPlot3D[T:ClassTag](
    a: Array[Array[T]]
    , xOffset: Int = 0
    , yOffset: Int = 0): String = {

    val yRange = a.length
    val xRange = a(0).length

    var r = s"draw3d( mesh(\n"
    for(y<- yOffset until yRange + yOffset) {
      r += "["
      for(x<- xOffset until xRange + xOffset)
        r += s"[$x,$y,${a(y)(x)}],"
      r = r.dropRight(1) + "],\n"
    }
    r.dropRight(2) + "))\n"
  }
  //---------------------------------------------------------------------------
  def getPlotPolynomy2D[T:ClassTag](vertexSeq: Array[Array[T]]): String = {
    //-------------------------------------------------------------------------
    def declareVertexSeq(i: Int) = {
      var r = ""
      r += s"\nvertex_seq_$i : [ "
      vertexSeq(i).grouped(2).foreach { t => r += t.mkString("[", ",", "],") }
      r = r.dropRight(1) + " ];"
      r
    }
    //-------------------------------------------------------------------------
    def plotPoly(i: Int) = {
      val color = RANDOM_COLOR(i % RANDOM_COLOR.length)
      var r = ""
      r+= s"\n\t , fill_color = $color"
      r+= s"\n\t , polygon(vertex_seq_$i)"
      r+= s"\n\t , points(vertex_seq_$i)"
      r+="\n"
      r
    }
    //-------------------------------------------------------------------------
    var r = s"load(draw);"
    for (i<-0 to vertexSeq.length-1)  r+= declareVertexSeq(i)

    //commonHenosis options
    r+= "\ndraw2d("
    r+= "\n\t   color = black"
    r+= "\n\t , point_size = 1"
    r+= "\n\t , point_type = filled_circle\n"

    //plot all polynomials
    r+= plotPoly(0)
    for (i<-1 to vertexSeq.length-1)  r+= plotPoly(i)
    r+= ");"
    r
  }
  //---------------------------------------------------------------------------
  def getPlot2D_Triangle[T:ClassTag](a: Array[T]): String = {
    val vertexSeq = (a.grouped(2) map { t =>  t.mkString("[", ",", "]") })
      .toArray
      .mkString(",")
    var r = ""
    r+= "\ndraw2d("
    r+= "\n\t   color = red"
    r+= "\n\t , fill_color = white"
    r+= "\n\t , proportional_axes=xy"
    r+= "\n\t , triangle(" + vertexSeq + ")"
    r+= "\n);"
    r
  }
  //---------------------------------------------------------------------------
  def getPlot2D_Circle(c: CircleDouble): String =
    getPlot2D_Circle(c.centre, c.radius)
  //---------------------------------------------------------------------------
  def getPlot2D_Circle(centre: Point2D_Double, radius: Double): String = {
    var r = ""
    r+= "\nx0 : " + centre.x + ";"
    r+= "\ny0 : " + centre.y + ";"
    r+= "\nr  : " + radius + ";"
    r+= "\ncircle(x,y) := sqrt((x-x0)^2 + (y-y0)^2 ) = r;"

    r+= "\ndraw2d("
    r+= "\n\t   color = red"
    r+= "\n\t , fill_color = white"
    r+= "\n\t , proportional_axes=xy"
    r+= s"\n\t ,implicit(circle(x,y),x,x0+r,x0-r,y,y0+r,y0-r)"
    r+= "\n);"
    r
  }
  //---------------------------------------------------------------------------
  def getPlot2D_TriangleInnerCircle(t: TriangleDouble): String = {
    val vertexSeq = (Point2D_Double.toSeq(t.toPointSeq).grouped(2) map { t =>  t.mkString("[", ",", "]") })
      .toArray
      .mkString(",")
    val circle = t.getInCentreCircle
    val centre = circle.centre
    val radius = circle.radius
    var r = ""
    r+= "\nx0 : " + centre.x + ";"
    r+= "\ny0 : " + centre.y + ";"
    r+= "\nr  : " + radius + ";"
    r+= "\ncircle(x,y) := sqrt((x-x0)^2 + (y-y0)^2 ) = r;"

    r+= "\ndraw2d("
    r+= "\n\t   color = red"
    r+= "\n\t , fill_color = white"
    r+= "\n\t , proportional_axes=xy"
    r+= "\n\t , triangle(" + vertexSeq + ")"
    r+= "\n\t , color = blue"
    r+= s"\n\t , implicit(circle(x,y),x,x0+r,x0-r,y,y0+r,y0-r)"
    r+= "\n);"
    r
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Maxima.scala
//=============================================================================
