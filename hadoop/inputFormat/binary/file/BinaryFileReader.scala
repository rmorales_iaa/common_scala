/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  15/dic/2018
  * Time:  02h:55m
  * Description: None
  */
//=============================================================================
package com.common.hadoop.inputFormat.binary.reader.file
//=============================================================================
//=============================================================================
import com.common.hadoop.inputFormat.binary.record.BinaryRecordReader
import org.apache.hadoop.mapreduce.{InputSplit, TaskAttemptContext}
//=============================================================================
//=============================================================================
class BinaryFileReader(recordByteLength: Long) extends BinaryRecordReader(recordByteLength) {
  //---------------------------------------------------------------------------
  def initializationError(message: String): Unit ={
    recordsInCurrentSplit = 0
    close()
    error(message)
  }
  //---------------------------------------------------------------------------
  override def initialize(inputSplit: InputSplit, taskContext: TaskAttemptContext): Unit ={
    if (!initialized) init(inputSplit, taskContext)
    if (isCompressedStream) initializationError("Split is compressed and it is incompatible with a record")
    else super.initialize(inputSplit, taskContext)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file FileRecordReader.scala
//=============================================================================