/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  15/dic/2018
  * Time:  21h:59m
  * Description: None
  */
//=============================================================================
package com.common.hadoop.inputFormat.binary.reader.file
//=============================================================================
import com.common.hadoop.inputFormat.binary.BinaryFileInputFormat
import org.apache.hadoop.mapreduce.{InputSplit, TaskAttemptContext}
//=============================================================================
abstract class BinaryFile(recordByteLength: Int) extends BinaryFileInputFormat {
  //---------------------------------------------------------------------------
  //Override the RecordReader class with the custom FitsRecordReader
  override def createRecordReader(inputSplit: InputSplit
                                  , context: TaskAttemptContext): BinaryFileReader = {
    initRecordReader(context)
    if (recordByteLength <= 0) {
      error("InputFormat " + recordByteLength + " is invalid.  It should be set to a value greater than zero")
      null
    }
    else new BinaryFileReader(recordByteLength)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file FileInputFormat.scala
//=============================================================================