/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  25/Sep/2018
  * Time:  18h:48m
  * Description: None
  */
//=============================================================================
package com.common.hadoop.inputFormat.binary
//=============================================================================
//=============================================================================
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.compress.CompressionCodecFactory
import org.apache.hadoop.mapreduce.lib.input.{FileInputFormat}
import org.apache.hadoop.mapreduce.{JobContext, TaskAttemptContext}
//=============================================================================
import com.common.hadoop.inputFormat.binary.record.BinaryRecord.{BinaryRecordKeyType, BinaryRecordValueType}
import com.common.logger.MyLogger
//=============================================================================
//=============================================================================
abstract class BinaryFileInputFormat extends FileInputFormat[BinaryRecordKeyType
                                                             , BinaryRecordValueType] with MyLogger {
  //---------------------------------------------------------------------------
  protected var configuration: Configuration = null
  //---------------------------------------------------------------------------
  protected val recordByteLength: Int
  //---------------------------------------------------------------------------
  protected def initRecordReader(context: TaskAttemptContext) =
    if (configuration== null) configuration = context.getConfiguration
  //---------------------------------------------------------------------------
  override protected def isSplitable(context: JobContext, file: Path): Boolean =
    new CompressionCodecFactory(context.getConfiguration).getCodec(file) == null
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file MyFileInputFormat.scala
//=============================================================================