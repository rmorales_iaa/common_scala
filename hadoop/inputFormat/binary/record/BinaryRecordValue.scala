/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  24/Aug/2018
  * Time:  12h:47m
  * Description: None
  */
//=============================================================================
package com.common.hadoop.inputFormat.binary.record
//=============================================================================
import org.apache.hadoop.io.BytesWritable
//=============================================================================
//=============================================================================
class BinaryRecordValue(seq:Seq[Byte]) extends BytesWritable(seq.toArray) with Serializable
//=============================================================================
//=============================================================================
//End of file MyBinaryRecordValuedValue.scala
//=============================================================================