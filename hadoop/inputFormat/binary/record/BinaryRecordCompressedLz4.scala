/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  24/Aug/2018
  * Time:  16h:05m
  * Description: None
  */
//=============================================================================
package com.common.hadoop.inputFormat.binary.record
//=============================================================================
import com.common.compression.lz4.Lz4
import org.apache.hadoop.io.{BytesWritable, LongWritable}
//=============================================================================
//=============================================================================
case class BinaryRecordCompressedLz4(key: LongWritable, value: BytesWritable) extends Serializable {
  //---------------------------------------------------------------------------
  def getValueAsByteSeq: Array[Byte] = value.getBytes
  //---------------------------------------------------------------------------
  def getValueByteLength: Int = value.getBytes.length
  //---------------------------------------------------------------------------
  def getValueStringSeq(recordSize: Int, divider : String= ",")  =
    Lz4().decompress(value.getBytes).get.grouped(recordSize).flatMap ( s=> new String( s ).trim.split( divider) ).toIndexedSeq
  //---------------------------------------------------------------------------
  def getKey = key.get
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file MyRecordLz4String.scala
//=============================================================================