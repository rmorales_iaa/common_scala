/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  24/Aug/2018
  * Time:  16h:05m
  * Description: None
  */
//=============================================================================
package com.common.hadoop.inputFormat.binary.record
//=============================================================================
import com.common.hadoop.inputFormat.binary.record.BinaryRecord.{BinaryRecordKeyType, BinaryRecordValueType}
import org.apache.hadoop.io.{BytesWritable, LongWritable}
//=============================================================================
object BinaryRecord{
  //---------------------------------------------------------------------------
  type BinaryRecordKeyType = LongWritable
  type BinaryRecordValueType = BytesWritable
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
case class BinaryRecord(key: BinaryRecordKeyType
                        , value: BinaryRecordValueType) extends Serializable {
  //---------------------------------------------------------------------------
  def this (k: BinaryRecordKeyType, v: Seq[Byte]) = this(k, new BinaryRecordValue(v))
  //---------------------------------------------------------------------------
  def getValueAsByteArray: Array[Byte] = value.getBytes
  //---------------------------------------------------------------------------
  def getValueAsString: String = new String(value.getBytes)
  //---------------------------------------------------------------------------
  def getValueByteLength: Int = value.getBytes.length
  //---------------------------------------------------------------------------
  def getKey: Long = key.get
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file MyBinaryRecordRecord.scala
//=============================================================================