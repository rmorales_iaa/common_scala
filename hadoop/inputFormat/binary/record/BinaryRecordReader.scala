/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  03/Aug/2018
  * Time:  10h:44m
  * Description:
*/
//=============================================================================
package com.common.hadoop.inputFormat.binary.record
//=============================================================================
//=============================================================================
import com.common.hadoop.MyHadoop
import com.common.math.MyMath
import com.common.stream.remote.{RemoteCompressedInputStream, RemoteInputStream}
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.io.compress.{CompressionCodec, CompressionCodecFactory}
import org.apache.hadoop.mapreduce.lib.input.FileSplit
import org.apache.hadoop.mapreduce.{InputSplit, RecordReader, TaskAttemptContext}
//=============================================================================
import com.common.hadoop.inputFormat.binary.record.BinaryRecord.{BinaryRecordKeyType, BinaryRecordValueType}
import com.common.logger.MyLogger
import com.common.stream.MyInputStream
//=============================================================================
//based on  "org.apache.com.common.hadoop.mapreduce.lib.input.FixedLengthRecordReader"
class BinaryRecordReader(recordByteLength: Long) extends
  RecordReader[BinaryRecordKeyType, BinaryRecordValueType] with MyLogger {
  //---------------------------------------------------------------------------
  //Hadoop file System
  protected var hdfs: FileSystem = _
  protected var canonicalServiceName =  ""

  //Hadoop record
  protected var recordKey : BinaryRecordKeyType = _
  protected var recordValue : BinaryRecordValueType = _
  protected var recordsInCurrentSplit : Long = 0

  //Hadoop split positions(file offset)
  protected var split: FileSplit = _
  protected var splitStart: Long = 0
  protected var splitEnd : Long = 0
  protected var splitByteSize: Long = 0

  protected var splitID: Long = -1

  //compression and decompression
  private var codec : CompressionCodec = _
  protected var isCompressedStream: Boolean = false

  //remote file
  protected var remoteFileByteSize: Long = 0
  protected var remotePathFile: Path = _
  protected var localPathFile: String = ""

  //stream
  protected var stream: MyInputStream = _

  //initialization flag
  protected var initialized: Boolean = false

  //debug flag
  protected var reportDebugInfo: Boolean = true
  //---------------------------------------------------------------------------
  //records and splits star its numbering with index 0
  //---------------------------------------------------------------------------
  protected def getAbsoluteStreamReadPos = stream.getReadPosition
  //---------------------------------------------------------------------------
  protected  def getRelativeStreamReadPos = getAbsoluteStreamReadPos - splitStart
  //---------------------------------------------------------------------------
  protected  def getRelativeRecordIndex = getRelativeStreamReadPos / recordByteLength
  //---------------------------------------------------------------------------
  protected def getStreamSkipByteCount: Long =  {
    val splitStartOffset = splitStart % recordByteLength
    if (splitStartOffset != 0) {
      val skip = recordByteLength - splitStartOffset //due to a partial record at beginning of the split
      errorMessage(s"The split is not aligned with the star of a record, skipping $skip bytes")
      skip
    }
    else splitStart
  }
  //---------------------------------------------------------------------------
  private def initSplitPositions() = {
    //get the positions (file offsets) of the split, and set the read position
    splitStart = split.getStart
    splitEnd = splitStart + split.getLength
    splitByteSize = splitEnd - splitStart

    splitID = splitStart  / splitByteSize
    recordsInCurrentSplit = MyMath.roundIntegerDivision(splitByteSize
                                                        , recordByteLength)
  }
  //---------------------------------------------------------------------------
  protected def init(inputSplit: InputSplit
                     , taskContext: TaskAttemptContext): Unit ={

    //inputSplit is indeed a FileSplit object
    split = inputSplit.asInstanceOf[FileSplit]

    //get the info about remote file
    remotePathFile = split.getPath

    //get the configuration of the com.common.hadoop task
    val taskConf = taskContext.getConfiguration

    //get file system
    hdfs = remotePathFile.getFileSystem(taskConf)
    canonicalServiceName = MyHadoop.HDFS_PREFIX + hdfs.getCanonicalServiceName

    //get local path
    localPathFile = "/" +
      remotePathFile
        .toString.split("/")
        .drop(3)
        .mkString("/")

    //get some info
    remoteFileByteSize = hdfs.getContentSummary(remotePathFile).getLength
    splitByteSize = hdfs.getFileStatus(remotePathFile).getBlockSize

    //get codec
    codec = new CompressionCodecFactory(taskConf)
      .getCodec(remotePathFile)

    //open the stream
    if (codec != null)  {
      isCompressedStream = true
      reportMessage("Splits are compressed. So, processing the first structure with header")
      stream = RemoteCompressedInputStream(hdfs
                                           , remotePathFile
                                           , codec
                                           , remotePathFile.getName)
    }
    else stream = RemoteInputStream(hdfs
                                    , localPathFile)

    //get the positions (file offsets) of the split
    initSplitPositions()

    initialized = true
  }
  //---------------------------------------------------------------------------
  override def initialize(inputSplit: InputSplit
                          , taskContext: TaskAttemptContext): Unit = {
    if (!initialized) init(inputSplit, taskContext)
    val skipBytes = getStreamSkipByteCount
    if (skipBytes == -1 ){
      recordsInCurrentSplit = 0
      reportMessage(s"Record initializing: Split $splitID. Split has no records to calculate")
      close()
    }
    else {
      streamLongSkip(skipBytes)
      reportMessage(s"Record initialized")
    }
  }
  //---------------------------------------------------------------------------
  protected def streamLongSkip(byteLength: Long) = stream.longSkip(byteLength)
  //---------------------------------------------------------------------------
  protected def streamLongRead(byteLength: Long) = stream.longRead(byteLength)
  //---------------------------------------------------------------------------
  protected def getValueFromStream(seq: Seq[Byte]): Array[Byte] = seq.toArray
  //---------------------------------------------------------------------------
  protected def readValueFromStream(byteLength: Long): Boolean = {
    val r = streamLongRead(byteLength)
    if (!r._1)
      return errorMessage(s"Split $splitID. Error reading record " + getRelativeRecordIndex)
    else {
      recordValue = new BinaryRecordValue(getValueFromStream(r._2))
      recordValue.setSize(recordValue.getLength) //length can not be long type. If it required a long value, re-write 'BytesWritable'
    }
    true
  }
  //---------------------------------------------------------------------------
  protected def readKeyAndValueFromStream(byteLength: Long): Boolean = {
    if (byteLength < 0 ) return errorMessage(s"Trying to read a negative size: $byteLength")
    recordKey = new BinaryRecordKeyType(getAbsoluteStreamReadPos)
    readValueFromStream(byteLength)
  }
  //---------------------------------------------------------------------------
  override def nextKeyValue() : Boolean = {
    val streamReadPos = getAbsoluteStreamReadPos
    if (recordsInCurrentSplit == 0) {
      reportMessage("End of record processing. No more records to process")
      return false //no records in this split, so stop processing split
    }
    if (streamReadPos >= splitEnd) { //split fully processed?
      if (streamReadPos != splitEnd)
        errorMessage(s"Split $splitID. The expected file read position at the end of split: $splitEnd does not match with the current file pos: " + getAbsoluteStreamReadPos)
      else
        reportMessage("End of record processing. Split end reached")
      return false
    }

    val byteCountToRead = Math.min(recordByteLength, stream.getRemainByteCount)
    if (stream.canRead(byteCountToRead))
      readKeyAndValueFromStream(byteCountToRead)
    else
      return warningMessage(s"Split $splitID. Last split is broken. It has not enough bytes to process a record. Trying to read $byteCountToRead bytes ")

    true
  }
  //---------------------------------------------------------------------------
  //close the file associated with this split
  override def close(): Unit = if (stream != null) stream.close
  //---------------------------------------------------------------------------
  override def getCurrentKey: BinaryRecordKeyType =  recordKey
  //---------------------------------------------------------------------------
  override def getCurrentValue: BinaryRecordValueType  = recordValue
  //---------------------------------------------------------------------------
  override def getProgress: Float = {
    if (splitStart == splitEnd) 0.0f
    else Math.min(1.0f, (getAbsoluteStreamReadPos - splitStart) / (splitEnd - splitStart).toFloat)
  }
  //---------------------------------------------------------------------------
  protected def getInfo: String =
    s". Split:$splitID->(S:$splitStart,E:$splitEnd,L:" + split.getLength + s",SP:" +getAbsoluteStreamReadPos+") " +
      s"Record:$getRelativeRecordIndex/$recordsInCurrentSplit"
  //---------------------------------------------------------------------------
  protected def reportMessage(s: String): Boolean = {
    if (reportDebugInfo) info(s + getInfo)
    true
  }
  //---------------------------------------------------------------------------
  protected def errorMessage(s: String): Boolean = {
    if (reportDebugInfo) error(s)
    false
  }
  //---------------------------------------------------------------------------
  protected def warningMessage(s: String): Boolean = {
    if (reportDebugInfo) warning(s + getInfo)
    false
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file FixedRecordReader
//=============================================================================