/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  15/dic/2018
  * Time:  01h:40m
  * Description: None
  */
//=============================================================================
package com.common.hadoop.inputFormat.binary.record
//=============================================================================
//=============================================================================
case class BinaryRecordCompressed(key: Long, value: Seq[Byte]) extends Serializable
//=============================================================================
//End of file MyRecordCompressed.scala
//=============================================================================