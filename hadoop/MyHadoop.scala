//=============================================================================
package com.common.hadoop
//=============================================================================
import org.apache.hadoop.conf.Configuration
import java.io.File
import org.apache.hadoop.fs.{BlockLocation, FileSystem, Path}
import scala.language.implicitConversions
//=============================================================================
import com.common.util.path.Path._
import com.common.logger.MyLogger
//=============================================================================
//=============================================================================
object MyHadoop {
  //---------------------------------------------------------------------------
  val HDFS_PREFIX = "hdfs://"
  //---------------------------------------------------------------------------
}
//=============================================================================
import MyHadoop._
case class MyHadoop(fs: FileSystem, hadoopConf: Configuration) extends Serializable with MyLogger {
  //---------------------------------------------------------------------------
  private val canonicalServiceName = HDFS_PREFIX + fs.getCanonicalServiceName
  val defaultServiceName = hadoopConf.get("fs.defaultFS")
  //---------------------------------------------------------------------------
  val minBlockByteSize = hadoopConf.get("dfs.namenode.fs-limits.min-block-size")
  //---------------------------------------------------------------------------
  implicit def toPath(s: String) : Path = new Path(s)
  //---------------------------------------------------------------------------
  def getFileSize (p: Path): Long =  fs.getContentSummary(p).getLength
  //---------------------------------------------------------------------------
  def fileExist (p: Path): Boolean   =  fs.exists(p) && !fs.isDirectory(p)
  def fileExist (l: List[String]) : Boolean = l.forall(s => fileExist(new Path(s)))
  def fileExist (s: String) : Boolean = fileExist(new Path(s))
  //---------------------------------------------------------------------------
  def getFileContent (s: String) = {
    val bis = new java.io.BufferedInputStream(fs.open(new Path(s)))
    Stream.continually(bis.read).takeWhile( _  != -1).map(_.toByte).toArray
  }
  //---------------------------------------------------------------------------
  def dirExist (p: Path): Boolean  =  fs.exists(p) && fs.isDirectory(p)
  def dirExist (l: List[String]): Boolean =  l.forall(s => dirExist(new Path(s)))
  def dirExist (s: String): Boolean = dirExist(new Path(s))
  //---------------------------------------------------------------------------
  def dirParentExist (p: Path): Boolean  =  dirExist(p.getParent)
  //---------------------------------------------------------------------------
  def getDirContent(dir: String, fileExtensionFilter: String): Array[Path] = {
    if (dir.isEmpty || fs == null) return Array()
    val status = fs.listStatus(new Path(dir))
    status.flatMap{ f =>
      val p = f.getPath
      if (p.toString.endsWith(fileExtensionFilter)) Some(p)
      else None}.sortWith( _.getName  < _.getName )
  }
  //---------------------------------------------------------------------------
  def getDirContentNoRecursive(path: String, extensionList: List[String]): Array[String] = {
    if (dirExist(path)) {
      fs.listStatus(path).flatMap { f =>
        val p = f.getPath.getName
        if (f.isFile && extensionList.exists(p.endsWith)) Some(path + FileSeparator + p)
        else None
      }
    }
    else Array()
  }
  //---------------------------------------------------------------------------
  def getDirContentRecursive(path: String, extensionList: List[String]): Array[String] = {

    val rootFileSeq = getDirContentNoRecursive(path, extensionList)
    val remainFileSeq = if (dirExist(path)) {
      val statusSeq = fs.listStatus(path)
      val result =statusSeq.flatMap { f =>
        if (f.isDirectory) Some(getDirContentRecursive(path + FileSeparator + f.getPath.getName, extensionList))
        else None
      }
      result.flatten
    }
    else Array[String]()

    rootFileSeq ++ remainFileSeq
  }
  //---------------------------------------------------------------------------
  def removeFsPrefix(p: Path): String = {
    if(!p.toString.startsWith(canonicalServiceName)) p.toString
    else File.separator + p.toString.split(File.separator).drop(3).mkString(File.separator)
  }
  //---------------------------------------------------------------------------
  def getCanonicalServiceName(p : String): String = {
    if (p.startsWith(canonicalServiceName)) p.trim
    else canonicalServiceName + p.trim
  }

  //---------------------------------------------------------------------------
  def getDefaultServiceName(p: String): String = {
    if (p.startsWith(defaultServiceName)) p.trim
    else defaultServiceName + p.trim
  }
  //---------------------------------------------------------------------------
  def getParentPathString (s: String) =  new Path(s).getParent.toString
  //---------------------------------------------------------------------------
  def dirEndsWithFileDivider (s: String) =  s.endsWith(File.separator)
  //---------------------------------------------------------------------------
  def createDir(p: Path) =  fs.mkdirs(p)
  //---------------------------------------------------------------------------
  def createFile(p: String) = fs.create(new Path(p))
  //---------------------------------------------------------------------------
  def deleteDir(p: Path,isRecursive: Boolean = true) =  fs.delete(p,isRecursive)
  //---------------------------------------------------------------------------
  def deleteFile(p: Path): Unit = if (fs.exists(p)) fs.delete(p, true)
  def deleteFile(s: String): Unit =  deleteFile(new Path(s))
  //---------------------------------------------------------------------------
  def getCanonicalPathName(s: String) =  canonicalServiceName + s
  //---------------------------------------------------------------------------
  def copyToLocal(src: String, dst: String) = fs.copyToLocalFile(src, dst)  //remote to local
  //---------------------------------------------------------------------------
  def copyToRemote(src: String, dst: String) =  fs.copyFromLocalFile(src, dst) //local to remote
  //---------------------------------------------------------------------------
  def getFileBlockLocation(p: Path): Option[Array[BlockLocation]] =  {

    if (!fileExist(p)) {
      error(s"The file '$p' does not exist in the file system")
      None
    }
    else Some(fs.getFileBlockLocations(p, 0, Long.MaxValue))
  }
  //---------------------------------------------------------------------------
  def getFileBlockHostList(p: Path) =  {

    if (!fileExist(p)) {
      error(s"The file '$p' does not exist in the file system")
      None
    }
    val r = fs.getFileBlockLocations(p, 0, Long.MaxValue)
    r.map{ l => (l.getHosts, l.getCachedHosts)}
  }
  //---------------------------------------------------------------------------
  //https://github.com/cmccabe/get_block_locations_test/blob/master/src/java/main/com/cloudera/GetBlockLocations.java
  def printFileBlockLocation(p: Path) =  {

    val r = getFileBlockLocation(p)
    if(r.isDefined){

      r.get.zipWithIndex.foreach{ case (l,i) =>
        info(s"Block $i")
        info("\thosts =         " + l.getHosts.mkString(","))
        info("\tcachedHosts =   " + l.getCachedHosts.mkString(","))
        info("\tnames    =      " + l.getNames.mkString(","))
        info("\ttopologyPaths = " + l.getTopologyPaths.mkString(","))
        info("\toffset =        " + l.getOffset)
        info("\tlength =        " + l.getLength)
        info("\tcorrupt =       " + l.isCorrupt)
      }
    }
  }
  //---------------------------------------------------------------------------
  def close() = fs.close()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MyHadoop.scala
//======================================================================