/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  15/May/2024
 * Time:  14h:12m
 * Description: None
 */
package com.common.hadoop.fits.file
//=============================================================================
import com.common.hadoop.fits.record.FitsRecordReader
import com.common.hadoop.inputFormat.binary.BinaryFileInputFormat
import org.apache.hadoop.mapreduce.{InputSplit, TaskAttemptContext}
//=============================================================================
//=============================================================================
object BinaryFileFITS {
  //---------------------------------------------------------------------------
  private final val RECORD_BYTE_LENGTH = com.common.fits.standard.ItemSize.BLOCK_BYTE_SIZE
  //---------------------------------------------------------------------------
}
//=============================================================================
import BinaryFileFITS._
class BinaryFileFITS() extends BinaryFileInputFormat {
  //---------------------------------------------------------------------------
  val recordByteLength = RECORD_BYTE_LENGTH
  //---------------------------------------------------------------------------
  override def createRecordReader(inputSplit: InputSplit
                                  , context: TaskAttemptContext): FitsRecordReader = {
    initRecordReader(context)
    if (recordByteLength <= 0) {
      error("BinaryFileFITS inputFormat " + recordByteLength + " is invalid.  It should be set to a value greater than zero")
      null
    }
    else new FitsRecordReader(recordByteLength) //override the RecordReader class
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file BinaryFileFITS.scala
//=============================================================================