/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Aug/2018
 * Time:  10h:44m
 * Description:
 * Key idea 1: Hadoop records and Hadoop InputSplit are whole multiple of FITS blocks.
 * Key idea 2: use a fixed length Hadoop Input Formats (http://hadooptutorial.info/com.common.hadoop-input-formats/) and a fixed length Hadoop records
 *
 * IMPORTANT NOTE: If the Hadoop splits and Hadoop records are not a whole multiple of FITS blocks byte size (2880 bytes) ,
 *   then, the FITS file is not aligned (see below) and this class is not valid, and it would be necessary to recalculate
 *  how to accommodate the FIS blocks on Hadoop records.
 *
 * Hadoop divides a file in InputSplit chunks (also known as split), typically with the size of a Hadoop dataBlock (128MB by default).
 * Then each split is processed by a unique executor calculate. All the splits of a file are processed in parallel.
 *
 * The executor will calculate only one single record each time. So. the split must be divided in records.
 *  A record is a pair of (recordKey,values). Usually, the recordKey is the offset in the file of values (data of the split)
 *
 * The executor, first call to "initialize" to set properly the parameters of the split to be processed (mainly where starts the first record).
 * The will call "nextKeyValue" to get the recordKey of the next record to calculate, until all records of the split were processed.
 *
 * This class will splits in records only one of the structures of the FITS file, following the structure definition of the FITS standard:
 *  Version 4.0. 2017 Dec 2015: 3. FITS file organization
 *  The structure selected is considered as a binary sequence. If the structure has an ASCII content,
 *  it wil be treated also as binary data (i.e. headers ). It is possible skip the header of the structure.
 *  Because it is possible to select the structure and skip the header of the structure, the split must not be compressed and
 *  it is necessary to modify the class FixedLengthRecordReader
 *  The FITS structure with index 0 is always the PrimaryHDU (header and data unit).
 *
 *  According to the standard, a structure,  is composed by blocks of 2880 byte size.
 *  So, in order to align the FITS structure, the split and the records byte size must be a multiply of
 *  the FITS dataBlock size (2880 bytes = 45 record 64 = 45 2**6). This implies that the Hadoop dataBlock size (split size) must be updated properly before to store the file.
 *
 * The file will be open n-times (n = number of executors = number of splits) simultaneously, so it is important
 * to have a fast FITS file parser to point properly to the required data (avoiding the structured and headers not requested)
 */

//=============================================================================
package com.common.hadoop.fits.record
//=============================================================================
import com.common.dataType.pixelDataType.PixelDataType
import com.common.fits.standard.ItemSize.BLOCK_BYTE_SIZE
import com.common.fits.standard.block.Block
import com.common.fits.standard.fits.Fits
import com.common.fits.standard.structure.hdu.primary.Primary
import com.common.hadoop.inputFormat.binary.record.BinaryRecordReader
import com.sparkFits.sparkProcess.ProcessFITS
//=============================================================================
import org.apache.hadoop.mapreduce.{InputSplit, TaskAttemptContext}
//=============================================================================
//All FITS structures size to calculate must be have a whole multiple of Hadoop record size
//=============================================================================
class FitsRecordReader(recordByteLength: Long) extends BinaryRecordReader(recordByteLength) {
  //---------------------------------------------------------------------------
  //FITS header
  private var primaryHDU: Primary = _

  //bitpix and scale
  private var bitPix: Int = 0
  private var bScale = 0d
  private var bZero = 0d

  //header relevant pos
  private  var headerByteSize = 0L

  //data relevant pos
  private var dataStartPos = 0L  //zero based
  private var dataEndPos = 0L    //zero based
  private var dataByteSize = 0L
  protected var dataOffset: Long = 0 //zero based

  //padding bytes
  private var lastBlockPaddingByteSize = 0L
  private var paddingByteSizeAfterLastBlock = 0L
  //---------------------------------------------------------------------------
  def getDataBlockSize = Block.getBlockCount(dataByteSize)
  //---------------------------------------------------------------------------
  protected var fits : Fits = _
  //---------------------------------------------------------------------------
  def initializationError(message: String): Unit ={
    recordsInCurrentSplit = 0
    close()
    error(message)
  }
  //---------------------------------------------------------------------------
  protected def fitsInitialize() = {}  //extra job after fits is loaded
  //---------------------------------------------------------------------------
  override def initialize(inputSplit: InputSplit
                          , taskContext: TaskAttemptContext): Unit ={
    if (!initialized) init(inputSplit, taskContext)
    if (isCompressedStream) initializationError("Split is compressed and it is incompatible with a FITS record")
    else {
      //load FITS metadata but not the data it self
      fits = ProcessFITS.loadHDFS_Fits(
          hdfs
        , localPathFile
        , readDataBlockFlag = false).getOrElse {
        initializationError(s"FitsRecordReader.Error loading FITS file. InputSplit start:${inputSplit.getLocations.mkString("{", "," ,"}")}")
        return
      }

      //get the main header info
      primaryHDU = fits.getPrimaryHdu()
      val recordMap = primaryHDU.recordMap
      bitPix = recordMap.getBitPix().toInt
      bScale = recordMap.getBscale()
      bZero = recordMap.getBzero()
      val naxisSeq = recordMap.getAxisSeq()

      //header relevant pos
      headerByteSize = primaryHDU.getDataStartPos

      //data relevant pos
      dataByteSize = (naxisSeq.head * naxisSeq.last) * Math.abs(bitPix / 8)
      dataStartPos = headerByteSize    //zero based
      dataEndPos = dataStartPos + dataByteSize  //zero based
      dataOffset = headerByteSize //zero based

      //calculate padding bytes
      lastBlockPaddingByteSize = (getDataBlockSize * BLOCK_BYTE_SIZE) - dataByteSize
      paddingByteSizeAfterLastBlock = remoteFileByteSize - headerByteSize - (getDataBlockSize * BLOCK_BYTE_SIZE)

      fitsInitialize()  //used in the inherited class

      //parent initialization
      super.initialize(inputSplit, taskContext)
    }
  }
  //---------------------------------------------------------------------------
  def getPixSeq(byteSeq: Array[Byte]): Option[Array[Double]] = {
    val pixelSeq = PixelDataType.getPixelSeq(byteSeq, bitPix)
    val scaledPixSeq = PixelDataType.scalePixelSeq(pixelSeq, bScale, bZero)
    Some(scaledPixSeq)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FitsRecordReader
//=============================================================================