//=============================================================================
//File: scheduler.scala
//=============================================================================
/** It implements task manager that executes them at programmed time
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.scheduler
//=============================================================================
// System import sectionName
//=============================================================================
import akka.actor.{Actor, ActorSystem, Cancellable, Props}
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.TimeUnit.SECONDS
import scala.concurrent.duration._
//=============================================================================
// User import sectionName
//=============================================================================

//=============================================================================
// Class/Object implementation
//=============================================================================

object Scheduler {
  
  //-------------------------------------------------------------------------
  //Messages
  abstract class SchedulerMessage
  final case object MyStart extends SchedulerMessage
  final case object MyCancel extends SchedulerMessage
  //-------------------------------------------------------------------------
}

//=============================================================================
//=============================================================================

class Scheduler(actorSystem:ActorSystem) extends MyLogger {
  
  //-------------------------------------------------------------------------
  // Local import
  //-------------------------------------------------------------------------
  import actorSystem.dispatcher
  import Scheduler._
  
  //-------------------------------------------------------------------------
  // Variable declaration
  //-------------------------------------------------------------------------
  private var taskID = new AtomicInteger   
  private var actorMap = Map[Int, Cancellable]()
  
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
  info("Scheduler is starting")
  taskID.set(0)
    
  //-------------------------------------------------------------------------
  def close(){
    info("Sheduler is closed")
  }
  
  //-------------------------------------------------------------------------  
  def add(name: String
    , work: => Unit
    , once: Boolean = false
    , periodInS: Int =  10
    , initalDelayInS: Int = 0): Int = {
    
    var task: Cancellable = null        
    try {
      val actor = actorSystem.actorOf(SchedulerTask.props(() => work)
                                      ,  name=name)
      val delay=new FiniteDuration(initalDelayInS,SECONDS)
      val period=new FiniteDuration(periodInS,SECONDS)
              
      once match {       
        case true => task = actorSystem.scheduler.scheduleOnce(delay,actor,MyStart)
        case false => task = actorSystem.scheduler.schedule(delay,period,actor,MyStart)         
      }
    }     
    catch {
      case e: Exception => error(s"Error adding task: '$name' to scheduler." + e.toString())
      return -1
    }
    
    info(s"Task: '$name' has been added to scheduler with ID:"+taskID.get)
        
    //append to storage
    actorMap += (taskID.get->task)
                                                          
    //increase the ask m2_id
    taskID.incrementAndGet
  }
 
  //-------------------------------------------------------------------------
  def terminate() = actorMap.keys.foreach(x =>cancelTask(x))
  
  //-------------------------------------------------------------------------
  
  def cancelTask(taskID: Int): Boolean ={
    try {
      actorMap(taskID).cancel
      logger.info(s"Cancelling  task '$taskID'")
      actorMap -= (taskID)
    }     
    catch {
      case e: Exception => error(s"Error canceling task: '$taskID'" + e.toString())
      return false
    }
    true
  }
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of object 'Scheduler'
//=============================================================================
// End of 'Scheduler.scala' file
//=============================================================================
