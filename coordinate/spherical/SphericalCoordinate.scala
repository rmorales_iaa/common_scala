/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  29/Jun/2021
 * Time:  08h:57m
 * Description: Code for some calculations with speherical coordinates
 * Based on java import of SOFA library: http://www.iausofa.org/
 */
//=============================================================================
package com.common.coordinate.spherical
//=============================================================================
import com.common.constant.astronomy.AstronomicalUnit
import com.common.constant.astronomy.AstronomicalUnit.{PRECISE_DEGREES_TO_RADIANS, PRECISE_HALF_PI}
import com.common.util.time.Time
import org.jastronomy.jsofa.JSOFA.jauEpj
//=============================================================================
import org.jastronomy.jsofa.JSOFA._
import java.time.LocalDateTime
//=============================================================================
//=============================================================================
case class ObservedApparentPosition(azimuth: Double //in degrees
                                    , zenith: Double //in degrees
                                    , hourAngle:Double //in degrees
                                    , rightAscension:Double //in degrees
                                    , declination:Double //in degrees
                                    , altitude: Double //in meters
                                    , airMass: Double //none
                                    , extinction: Double //R/V
                                    , parallacticAngle: Double) { //degrees
  //---------------------------------------------------------------------------
  def print() = {
    println("//--------------------------------")
    println(s"azimuth(deg)        : $azimuth")
    println(s"zenith(deg)         : $zenith")
    println(s"hour angle(deg)     : $hourAngle")
    println(s"right ascension(deg): $rightAscension")
    println(s"declination(deg )   : $declination")
    println(s"altitude(m)         : $altitude")
    println(s"airMass(deg)        : $airMass")
    println(s"extinction          : $extinction")
    println(s"parallactic angle   : $parallacticAngle")
    println("//--------------------------------")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
object SphericalCoordinate {
  //---------------------------------------------------------------------------
  def getJulian_2_PartTime(t: LocalDateTime) = {
    jauDtf2d("UTC"
      , t.getYear
      , t.getMonthValue
      , t.getDayOfMonth
      , t.getHour
      , t.getMinute
      , t.getSecond + Time.nanosToSeconds(t.getNano))
  }
  //--------------------------------------------------------------------------
  //International Atomic Time
  def getTaiTime(t: JulianDate) = jauUtctai(t.djm0, t.djm1)
  //---------------------------------------------------------------------------
  // Terrestrial Time
  def getTT_Time(t: JulianDate) = jauTaitt(t.djm0, t.djm1)
  //---------------------------------------------------------------------------
  def getJulianEpoch(t: JulianDate) = jauEpj(t.djm0, t.djm1)
  //---------------------------------------------------------------------------
  def getJulianEpochToJulianDate(t: Double) = jauEpj2jd(t)
  //---------------------------------------------------------------------------
  //Transform an FK5 (J2000.0) star position into the system of the Hipparcos catalogue, assuming zero Hipparcos proper motion.
  def getJ2000ToHipparcos(raFK5: Double, decFK5: Double, date1: Double, date2: Double) =
    jauFk5hz(raFK5, decFK5, date1, date2 )
  //---------------------------------------------------------------------------
  //Transform a Hipparcos star position into FK5 J2000.0, assuming zero Hipparcos proper motion.
  def getHipparcosToJ2000(raHipparcos: Double = 0, decHipparcos: Double = 0, date1: Double, date2: Double) =
    jauHfk5z(raHipparcos, decHipparcos, date1, date2)
  //---------------------------------------------------------------------------
  def getTerrestrialTime(t: LocalDateTime) =
    getTT_Time(getTaiTime(getJulian_2_PartTime(t)))
  //---------------------------------------------------------------------------
  //Convert spherical coordinates to Cartesian.
  def getSphericalPosToCartesian(ra: Double, dec: Double) = {
    jauS2c(ra, dec)
  }
  //---------------------------------------------------------------------------
  def getJulianEpochWithLocalTime(t: LocalDateTime) =
    getJulianEpoch(getTerrestrialTime(t))
  //---------------------------------------------------------------------------
  //Converts form ICRS coordinates, epoch J2000.0, to CIRS
  //ICRS: International Celestial Reference System
  //CIRS: Celestial Intermediate Reference System
  //objectRightAscension in degress
  //objectDeclination in degress
  def fromICRS_to_CIRS(t: LocalDateTime
                       , rightAscension: Double
                       , declination: Double
                       , rightAscensionProperMotion : Double = 0
                       , declinationProperMotion : Double = 0
                       , parallax : Double = 0
                       , radialVelocity: Double = 0) = {
    val tt = getTerrestrialTime(t)
    jauAtci13(
        Math.toRadians(rightAscension)
      , Math.toRadians(declination)
      , rightAscensionProperMotion
      , declinationProperMotion
      , parallax
      , radialVelocity
      , tt.djm0
      , tt.djm1)
  }
  //---------------------------------------------------------------------------
  def getAirmass(airMassAlgorithm: String = "Pickering_2002"
                 , zenithDeg: Double //in degress
                ): Double = {
    val zenithInRad = Math.toRadians(zenithDeg)
    airMassAlgorithm match {
      case "Pickering_2002" => //https://en.wikipedia.org/wiki/Air_mass_(astronomy)
        val h = 90 - zenithDeg
        1 / Math.sin((h + 244 /(165 +  47 * Math.pow(h, 1.1))).toRadians)

      case "Young_1994" => //https://en.wikipedia.org/wiki/Air_mass_(astronomy)
        val cosZ =  Math.cos(zenithInRad)
        val cos_Z2 =  cosZ * cosZ
        val cos_Z3 =  cos_Z2 * cosZ
        ((1.002432 *  cos_Z2) + (0.148386 * cosZ) + 0.0096467) / (cos_Z3 + (0.149864 * cos_Z2) + (0.0102963 * cosZ) + 0.000303978)

      case "Young_Irving_1967"  => // Young and Irvine (1967, Astron. J, 72, pp945-950.)
        val sec = 1d / Math.cos(zenithInRad)
        sec * (1d - 0.0012d * ((sec * sec) - 1d))
    }
  }
  //---------------------------------------------------------------------------
  //Apparent results (from the observer): (azimuth,  zenith, hour angle, apparentRightAscension, apparentDeclination)
  //Airless calculations: set to 0:  observerPressure, observerAmbientTemperature and observerRelativeHumidity
  def getObservedApparentPosition(
    t: LocalDateTime
    , observerLatitude: Double
    , observerLongitude: Double
    , observerAltitude: Double
    , objectRightAscension: Double
    , objectDeclination: Double
    , observerRightAscensionProperMotion : Double = 0
    , observerDeclinationProperMotion : Double = 0
    , observerPressure : Double = 0
    , observerAmbientTemperature : Double = 0
    , observerRelativeHumidity : Double = 0
    , observerWavelength : Double = 0
    , objectRightAscensionProperMotion : Double = 0
    , objectDeclinationProperMotion : Double = 0
    , objectParallax : Double = 0
    , objectRadialVelocity: Double = 0
    , UT1_UTC: Double = 0
    , airMassAlgorithm: String = "Pickering_2002"  //"Pickering_2002" "Young_1994" "Young_Irving_1967"
     ) = {

    val cirs = fromICRS_to_CIRS(t
      , objectRightAscension
      , objectDeclination
      , objectRightAscensionProperMotion
      , objectDeclinationProperMotion
      , objectParallax
      , objectRadialVelocity)

    val utc = getJulian_2_PartTime(t)

    val r = jauAtio13(cirs.pos.alpha
      , cirs.pos.delta
      , utc.djm0
      , utc.djm1
      , UT1_UTC
      , Math.toRadians(observerLongitude)
      , Math.toRadians(observerLatitude)
      , observerAltitude
      , observerRightAscensionProperMotion
      , observerDeclinationProperMotion
      , observerPressure
      , observerAmbientTemperature
      , observerRelativeHumidity
      , observerWavelength)

    val zenithDeg = Math.toDegrees(r.zob)
    val airMass = getAirmass(airMassAlgorithm,zenithDeg)
    val parallacticAngle = Math.toDegrees(jauHd2pa(r.hob,Math.toRadians(objectDeclination),Math.toRadians(observerLatitude)))
    ObservedApparentPosition(
        Math.toDegrees(r.aob)  //azimuth
      , zenithDeg              //zenith
      , Math.toDegrees(r.hob)  //hourAngle
      , Math.toDegrees(r.rob)  //rightAscension
      , Math.toDegrees(r.dob)  //declination
      , 90d - zenithDeg        //altitude
      , airMass                //airmass
      , 0.1335d * airMass      //extinction
      , parallacticAngle       //parallactic angle
    )
  }
  //---------------------------------------------------------------------------
  //greenwich mean sidereal time in hours
  def getMST(t: LocalDateTime) = {

    //encode date and time fields into a 2-part Julian Date
    val jd = getJulian_2_PartTime(t)

    //get greenwich mean sidereal time (consistent with IAU 2006 precession)
    Math.toDegrees(jauGmst06(jd.djm0,
      jd.djm1,
      jd.djm0,
      jd.djm1)) / 15.0
  }
  //---------------------------------------------------------------------------
  //apparent greenwich mean sidereal time in hours
  def getApparentMST(t: LocalDateTime) = {
    //encode date and time fields into 2-part Julian Date
    val jd = getJulian_2_PartTime(t)

    //get apparent greenwich mean sidereal time (consistent with IAU 2006 precession)
    Math.toDegrees(jauGst06a(jd.djm0,
      jd.djm1,
      jd.djm0,
      jd.djm1)) / 15.0
  }
  //---------------------------------------------------------------------------
  //local greenwich mean sidereal time in hours
  def getLocalMST(t: LocalDateTime, observerLongitude: Double) =
    (getMST(t) - ((-observerLongitude) / 15.0)) % 24  //Using East longitude (positive towards east)
  //---------------------------------------------------------------------------
  //apparent local greenwich mean sidereal time in hours
  def getApparentLocalMST(t: LocalDateTime, observerLongitude: Double) =
    (getApparentMST(t) - ((-observerLongitude) / 15.0)) % 24  //Using East longitude (posive towards east)
  //---------------------------------------------------------------------------
  //result in hours
  def getLocalHourAngle(t: LocalDateTime, observerLongitude: Double, objectRightAscension: Double) =
    getLocalMST(t,observerLongitude) - objectRightAscension / 15.0
  //---------------------------------------------------------------------------
  //result in hours
  def getApparentLocalHourAngle(t: LocalDateTime, observerLongitude: Double, objectRightAscension: Double) =
    getApparentLocalMST(t,observerLongitude) - objectRightAscension / 15.0
  //---------------------------------------------------------------------------
  def normalizeAngle(angle: Double) = jauAnpm(angle)
  //---------------------------------------------------------------------------
  def cartesianToSpherical(cartPos: Seq[Double]): (Double,Double) = {
    val x  = cartPos(0)
    val y  = cartPos(1)
    val z  = cartPos(2)

    val mxerr = Math.abs(1000.0 * z) * AstronomicalUnit.DBL_EPSILON
    val polarlong = 0

    if( Math.abs( x ) < mxerr && Math.abs( y ) < mxerr ) {
      if (y < 0) ( polarlong, -PRECISE_HALF_PI)
      else {
        if (y > 0) (polarlong, PRECISE_HALF_PI)
        else (Double.MinPositiveValue, Double.MinPositiveValue) //error
      }
    }
    else {
      val d2 = x * x + y * y
      val theta = if (d2 == 0.0) 0.0 else Math.atan2(y, x);
      val phi = if (z == 0.0) 0.0 else Math.atan2(z, Math.sqrt(d2));
      (theta, phi)
    }
  }
  //---------------------------------------------------------------------------
  def astCosd(angle: Double): Double = {
    val resid = Math.abs(angle % 360.0)
    if (resid == 0.0) {
      1.0
    } else if (resid == 90.0) {
      0.0
    } else if (resid == 180.0) {
      -1.0
    } else if (resid == 270.0) {
      0.0
    } else {
      Math.cos(angle * PRECISE_DEGREES_TO_RADIANS)
    }
  }
  //---------------------------------------------------------------------------
  def astSind(angle: Double): Double = {
    val resid = (angle - 90.0) % 360.0
    val residAbs = Math.abs(resid)

    if (residAbs == 0.0) {
      1.0
    } else if (residAbs == 90.0) {
      0.0
    } else if (residAbs == 180.0) {
      -1.0
    } else if (residAbs == 270.0) {
      0.0
    } else {
      Math.sin(angle * PRECISE_DEGREES_TO_RADIANS)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SphericalCoordinate.scala
//=============================================================================
