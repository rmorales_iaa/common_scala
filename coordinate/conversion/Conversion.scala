//=============================================================================
package com.common.coordinate.conversion
//=============================================================================
import com.common.constant.astronomy.AstronomicalUnit.{PRECISE_COS_OBLIQ_J2000, PRECISE_DEGREES_TO_RADIANS, PRECISE_RADIANS_TO_DEGREES, PRECISE_SIN_OBLIQ_J2000}
import com.common.geometry.point.Point2D_Double
import com.common.util.time.Time
import com.common.util.util.Util
//=============================================================================
import org.apache.commons.math3.util.FastMath
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.lang.Math.{asin, atan2, cos, sin, sqrt, toDegrees, toRadians}
//=============================================================================
//=============================================================================
//HMS = hour minute second (Typically used to specify right ascension: ra)
//DMS = degree minute second (typically used to specify declination: dec)
//DD = decimal degree

//MOA = Minute Of arc
//mas = Milliarcseconds
//SOA = Second of arc

//http://www.shaileshchaure.com/howconverDminuts_Ddecimal.php
//https://www.swift.psu.edu/secure/toop/convert.htm
//https://www.justintools.com/unit-conversion/angle.php?k1=degrees&k2=milliarcseconds
//https://www.rapidtables.com/convert/number/degrees-to-degrees-minutes-seconds.html
//=============================================================================
object Conversion {
  //---------------------------------------------------------------------------
  //MAS = milli arco sec
  final val DEGREE_TO_MAS = 3600 * 1000
  final val MOA_TO_MAS    = 59999.95709692
  final val SOA_TO_MAS    = 1000.000041253

  final val MAS_TO_DEGREE = 1 / DEGREE_TO_MAS
  final val MAS_TO_MOA    = 1 / MOA_TO_MAS
  final val MAS_TO_SOA    = 1 / SOA_TO_MAS
  final def MAS_PER_DEGREE = DEGREE_TO_MAS
  //---------------------------------------------------------------------------
  final val ARCSEC_TO_MAS = 1000
  //---------------------------------------------------------------------------
  final val ARCMIN_TO_MAS = 60 * 1000
  final val ARCMIN_TO_DE = 60 * 1000
  //---------------------------------------------------------------------------
  final def ONE_SECONDS_IN_HMS = 0.0041666667
  final def ONE_SECONDS_IN_DMS = 0.0002777778
  //---------------------------------------------------------------------------
  //https://www.linz.govt.nz/data/geodetic-system/coordinate-conversion/geodetic-datum-conversions/equations-used-datum
  //https://en.wikipedia.org/wiki/Geodetic_datum
  final val WGS_84_a = 6378137d           //major semiaxis in meters. World Geodetic System 1984 (WGS 84)
  final val WGS_84_b = 6356752.3142       //minor semiaxis in meters. World Geodetic System 1984 (WGS 84)
  final val WGS_84_a_2 = WGS_84_a * WGS_84_a  //squared major semiaxis in meters. World Geodetic System 1984 (WGS 84)
  final val WGS_84_b_2 = WGS_84_b * WGS_84_b  //squared minor semiaxis in meters. World Geodetic System 1984 (WGS 84)
  final val WGS_84_f = 1/298.257223563d   //reciprocal of flattening in World Geodetic System 1984 (WGS 84)
  final val WGS_84_e2 = WGS_84_f * (2 - WGS_84_f)  //First eccentricity squared
  //---------------------------------------------------------------------------
  final val VALID_SEPARATOR_SEQ = Seq(" ", "-", ":")
  //---------------------------------------------------------------------------
  private def splitInTheDouble(t: String): Option[(Double,Double,Double)] = {
    VALID_SEPARATOR_SEQ.foreach { sep =>
      if (t.contains(sep)) {
        val seq = t.split(sep)
        if (seq.size != 3) return None
        for (i <- 0 until 3)
          if (!Util.isDouble(seq(i))) return None
        return Some((seq(0).toDouble, seq(1).toDouble, seq(2).toDouble))
      }
    }
    None
  }
  //---------------------------------------------------------------------------
  def getRa(s: String): Option[Double] = {
    val t = s.trim
    if (Util.isDouble(t)) Some(t.toDouble)
    else {
      val seq = splitInTheDouble(s).getOrElse(return None)
      Some(HMS_to_DD(seq._1, seq._2, seq._3))
    }
  }
  //---------------------------------------------------------------------------
  def getDec(s: String): Option[Double] = {
    val t = s.trim
    if (Util.isDouble(t)) Some(t.toDouble)
    else {
      val seq = splitInTheDouble(s).getOrElse(return None)
      Some(DMS_to_DD(seq._1, seq._2, seq._3))
    }
  }
  //---------------------------------------------------------------------------
  def HMS_to_DD(s:String, divider: String = ":") : Double = {
    val t = s.split(divider)
    require(t.size == 3)
    HMS_to_DD(t(0).toDouble, t(1).toDouble, t(2).toDouble)
  }
  //---------------------------------------------------------------------------
  def DMS_to_DD(s:String, divider: String = ":") : Double = {
    val t = s.split(divider)
    require(t.size == 3)
    DMS_to_DD(t(0).toDouble, t(1).toDouble, t(2).toDouble)
  }
  //---------------------------------------------------------------------------
  def HMS_to_DD(hour: Double, min: Double, sec: Double)=
    (hour * 15.0 ) + (min / 4.0) + (sec / 240.0)
  //---------------------------------------------------------------------------
  def DMS_to_DD(deg: Double, min: Double, sec: Double) = {
    val sign = if (deg < 0 || deg.toString.head == '-') -1 else 1
    deg + (sign * (min / 60.0)) + (sign * (sec / 3600.0))
  }
  //---------------------------------------------------------------------------
  def DD_to_HMS(ra: Double) = {
    val h = (ra / 15.0).toInt
    val t = ra - (h * 15.0)
    val m = (t * 4.0).toInt
    val s = (t - (m / 4.0)) * 240.0
    (h,m,s)
  }
  //---------------------------------------------------------------------------
  def DD_to_HMS_WithDivider(v: Double, divider: String  = " ", threeDecimalPlaces: Boolean = true) =
    formatTriplet(DD_to_HMS(v), divider, threeDecimalPlaces)
  //---------------------------------------------------------------------------
  def DD_to_DMS(dec: Double) = {
    val sign = if (dec < 0) -1 else 1
    val d = dec.toInt
    val t0 = sign * d
    val t = if (sign <0 ) dec + t0 else dec - t0
    val m = (sign * t * 60.0).toInt
    val s = sign * (t - (sign * (m / 60.0))) * 3600.0
    (d,m,s)
  }
  //---------------------------------------------------------------------------
  def DD_to_DMS_WithDivider(v: Double, divider: String = " ",  threeDecimalPlaces: Boolean = true) = {
    val c = DD_to_DMS(v)
    val r = formatTriplet(c, divider, threeDecimalPlaces)
    if (c._1 >= 0) "+" + r else r
  }
  //---------------------------------------------------------------------------
  def formatTriplet(v: (Int, Int, Double), divider: String = " ", threeDecimalPlaces: Boolean = true): String = {
    val a = f"${v._1}%02d" // Days: 2 digits, padded with 0
    val b = f"${v._2}%02d" // Minutes: 2 digits, padded with 0

    // Conditional formatting for seconds with fractional part
    val c = if (threeDecimalPlaces)
      f"${v._3}%06.3f" // Seconds: 2 digits, fractional part with 3 decimal places
    else
      f"${v._3}%09.6f" // Seconds: 2 digits, fractional part with 6 decimal places

    // Combine the components with the divider and replace any commas with dots (for decimal separation)
    (a + divider + b + divider + c).replace(",", ".")
  }
  //---------------------------------------------------------------------------
  def DD_to_mas(dd: Double) = {
    val (d,m,s) = DD_to_DMS(dd)
    val r = Math.round(
      Math.abs(d * DEGREE_TO_MAS) +
      (m * MOA_TO_MAS) +
      (s.toInt * SOA_TO_MAS) +
      (s - s.toInt) * 1000)
    if ( dd <  0) r * -1 else r
  }
  //---------------------------------------------------------------------------
  def mas_to_DD(v: Double) = {
    val d = (v * MAS_TO_DEGREE).toLong
    var remain = Math.abs(v) - (Math.abs(d) * DEGREE_TO_MAS)
    val m = (remain * MAS_TO_MOA).toLong
    remain -= (m * MOA_TO_MAS)
    val s = remain * MAS_TO_SOA
    DMS_to_DD(d.toDouble,m.toDouble,s.toDouble)
  }
  //---------------------------------------------------------------------------
  //latitude expected format string : degrees:minutes:secons.fraction seconds
  //longitude expected format string: degrees:minutes:secons.fraction seconds
  //longitude expresed from East (if it expresed from West, please append 180º)
  def latitudinalToDegrees(lat: String, lon: String) =
    Point2D_Double(DMS_to_DD(lat), DMS_to_DD(lon))
  //---------------------------------------------------------------------------
  //latitude in degress
  //longitude in degrees
  //If you are converting the position of an observatory, plese take into
  //account that matchRadiusMas = Earth matchRadiusMas in KM = 6371 plus the height of the observatory
  //(x,y,z) in KM
  def latitudinalToRectangular(_lat: Double, _lon: Double, radius: Double) = {
    val lat = Math.toRadians(_lat)
    val lon = Math.toRadians(_lon)
    val x = radius * Math.cos(lat) * Math.cos(lon)
    val y = radius * Math.cos(lat) * Math.sin(lon)
    val z = radius * Math.sin(lat)
    (x,y,z)
  }
  //---------------------------------------------------------------------------
  //latitude in degress
  //longitude in degrees
  //heigh in metres
  //u squared major semiaxis in meters
  //b squared minor semiaxis in meters
  // result; (x,y,z) in KM
  //https://www.linz.govt.nz/data/geodetic-system/coordinate-conversion/geodetic-datum-conversions/equations-used-datum
  //https://en.wikipedia.org/wiki/Geodetic_datum
  def latitudinalToRectangularWgs84(_lat: Double, _lon: Double, height: Double) =
    latitudinalToRectangular(_lat,_lon,height,WGS_84_a_2, WGS_84_b_2)
  //---------------------------------------------------------------------------
  //latitude in degres
  //longitude in degrees
  //heigh in metres
  //u squared major semiaxis in meters
  //b squared minor semiaxis in meters
  // result; (x,y,z) in KM
  //https://www.linz.govt.nz/data/geodetic-system/coordinate-conversion/geodetic-datum-conversions/equations-used-datum
  //https://en.wikipedia.org/wiki/Geodetic_datum
  //"Conversion of GPS data to cartesian coordinates via an application development adatped to a cad modelling system", Stelios Th. Kouzeleas, 2007
  //https://en.wikipedia.org/wiki/Geographic_coordinate_conversion#From_geodetic_to_ECEF_coordinates
  def latitudinalToRectangular(_lat: Double, _lon: Double, height: Double, a_2: Double, b_2: Double) = {
    val lat = FastMath.toRadians(_lat)
    val lon = FastMath.toRadians(_lon)
    val sinLat = FastMath.sin(lat)
    val cosLat = FastMath.cos(lat)
    val sinLon = FastMath.sin(lon)
    val cosLon = FastMath.cos(lon)
    val zDistance = a_2 / FastMath.sqrt(a_2 * (cosLat * cosLat) + b_2 * (sinLat * sinLat))  //distance from the surface to the Z-axis along the ellipsoid normal
    val r = (zDistance + height) * cosLat
    val x = r * cosLon
    val y = r * sinLon
    val z = ((b_2/a_2) * zDistance + height) * sinLat
    (x/1000, y/1000, z/1000)
  }
  //---------------------------------------------------------------------------
  //If you are converting the position of an observatory, please take into
  //account that matchRadiusMas = Earth matchRadiusMas in KM = 6371 plus the height of the observatory
  //(x,y,z) in KM
  def rectangularToLatitudinal(x:Double, y: Double, z: Double, radius: Double) =
    Point2D_Double(Math.toDegrees(Math.asin(z / radius)),
                   Math.toDegrees(Math.atan2(y, x)))
  //---------------------------------------------------------------------------
  private def properMotion(elapsedYear : Double
                           , raDec: Point2D_Double
                           , properMotion: Point2D_Double) : Point2D_Double =
    Point2D_Double(raDec.x + (((elapsedYear * properMotion.x) / MAS_PER_DEGREE) / Math.cos(Math.toRadians(raDec.y)) ),
                   raDec.y + ( (elapsedYear * properMotion.y) / MAS_PER_DEGREE) )
  //---------------------------------------------------------------------------
  def properMotion(observingDate: String
                   , raDec: Point2D_Double
                   , referenceEpochJulianYears: Double
                   , _properMotion: Point2D_Double) : Point2D_Double = {
    val elapsedYear = Time.toJulianYear(LocalDateTime.parse(observingDate,DateTimeFormatter.ISO_DATE_TIME)) - referenceEpochJulianYears
    properMotion(elapsedYear, raDec, _properMotion)
  }
  //---------------------------------------------------------------------------
  def properMotion(observingDate: String
                   , raDec: Point2D_Double
                   , referenceEpoch: String
                   , _properMotion: Point2D_Double) : Point2D_Double = {
    val elapsedYear = (Time.toJulian(observingDate) - Time.toJulian(referenceEpoch)) / Time.DAYS_IN_ONE_JULIAN_YEAR
    properMotion(elapsedYear, raDec, _properMotion)
  }
  //---------------------------------------------------------------------------
  def arcoMinToDecimalDegree(arcoMin: Double) = arcoMin / 60
  //---------------------------------------------------------------------------
  def arcoSecToDecimalDegree(arcoSec: Double) = arcoSec / 3600
  //---------------------------------------------------------------------------
  def decimalDegreeToArcSec(dd: Double) = dd * 3600
  //---------------------------------------------------------------------------
  def decimalDegreeToArcSec(p: Point2D_Double) = p * 3600
  //---------------------------------------------------------------------------
  def pixToDecimalDegree(pix: Double, pixScaleArcoSecPerPix: Double) =
    arcoSecToDecimalDegree(pix * pixScaleArcoSecPerPix)
  //---------------------------------------------------------------------------
  //apache commons
  def sphericalToRectangular(r: Double, theta: Double, phi: Double) : (Double,Double,Double)= {
    val cosTheta = FastMath.cos(theta)
    val sinTheta = FastMath.sin(theta)
    val cosPhi = FastMath.cos(phi)
    val sinPhi = FastMath.sin(phi)

    // rectangular coordinates
      (r * cosTheta * sinPhi
     , r * sinTheta * sinPhi
     , r * cosPhi)
  }
  //---------------------------------------------------------------------------
  //http://www.stargazing.net/kepler/rectang.html
  def sphericalToRectangular(raDec: Point2D_Double) = {
    val raDecAsRadians = raDec.toRadians()
    val cosDec = Math.cos(raDecAsRadians.y)
    ( Math.cos(raDecAsRadians.x) * cosDec
     , Math.sin(raDecAsRadians.x) * cosDec
     , Math.sin(raDecAsRadians.y))
  }
  //---------------------------------------------------------------------------
  //https://gist.github.com/NetzwergX/90da2ef4630ddeab2718ce135e27e1b1
  def rectangularToSpherical(x: Double, y:Double, z: Double) = {
    val r = Math.sqrt((x * x) + (y * y) + (z * z))
    val fi = Math.atan2(y , x)
    val theta = Math.acos(z / r)
    Point2D_Double(fi,theta).toDegrees()
  }
  //---------------------------------------------------------------------------
  def normalize_XYZ(x: Double, y: Double, z: Double) = {
    val invlen = 1.0 / sqrt(x * x + y * y + z * z)
    Array(x * invlen, y * invlen, z * invlen)
  }
  //--------------------------------------------------------------------------
  //original code: 'radecdeg2xyzarrmany'
  def raDecSeqToXYZ_Seq(raDecSeq: Array[Point2D_Double]) = {
    raDecSeq map { p =>
      val ra = toRadians(p.x)
      val dec = toRadians(p.y)
      val cosdec = cos(dec)
      Array(cosdec * cos(ra), cosdec * sin(ra), sin(dec))
    }
  }
  //--------------------------------------------------------------------------
  //original code: 'xy2ra'
  def xy2ra(x: Double, y: Double) = {
    var a = atan2(y, x);
    if (a <= 0) a += 2.0 * Math.PI
    a
  }
  //--------------------------------------------------------------------------
  def z2dec(z: Double) = asin(z)
  //--------------------------------------------------------------------------
  def xyzSeqToRaDecSeq(xyzSeq: Array[Array[Double]]) =
    xyzSeq map { xyz => Point2D_Double(toDegrees(xy2ra(xyz(0), xyz(1)))
                                     , toDegrees(z2dec(xyz(2)))) }
  //--------------------------------------------------------------------------
  def toPreciseDegrees (angrad: Double) = angrad * PRECISE_RADIANS_TO_DEGREES
  //--------------------------------------------------------------------------
  def toPreciseRadians (deg: Double) = deg * PRECISE_DEGREES_TO_RADIANS
  //--------------------------------------------------------------------------
  def addToRa(raDegrees: Double
              , secondsToAdd: Double): Double = {
    val raHours = raDegrees / 15.0
    val secondsInHours = secondsToAdd / 3600.0
    val newRaHours = raHours + secondsInHours
    newRaHours * 15.0
  }
  //--------------------------------------------------------------------------
  def addToDec(decDegrees: Double
               , secondsToAdd: Double): Double = {
    //------------------------------------------------------------------------
    val (degrees, minutes, seconds) = DD_to_DMS(decDegrees)
    var newSeconds = seconds + secondsToAdd
    var newMinutes = minutes
    var newDegrees = degrees

    // Handle the normalization of seconds and minutes
    if (newSeconds < 0) {
      newMinutes -= 1
      newSeconds += 60
    } else if (newSeconds >= 60) {
      newMinutes += 1
      newSeconds -= 60
    }

    // Handle the normalization of minutes and degrees
    if (newMinutes < 0) {
      newDegrees -= 1
      newMinutes += 60
    } else if (newMinutes >= 60) {
      newDegrees += 1
      newMinutes -= 60
    }

    // Note: We do not need to normalize degrees because the range of declination is limited from -90 to +90 degrees.
    DMS_to_DD(newDegrees, newMinutes, newSeconds)
  }

  //---------------------------------------------------------------------------
  def equatorialToEcliptic(vect: Array[Double]) =
    Array(vect(1) * PRECISE_COS_OBLIQ_J2000 + vect(2) * PRECISE_SIN_OBLIQ_J2000
         , vect(2) * PRECISE_COS_OBLIQ_J2000 - vect(1) * PRECISE_SIN_OBLIQ_J2000)
  //---------------------------------------------------------------------------
  def eclipticToEquatorial(vect: Array[Double]) =
    Array(vect(1) * PRECISE_COS_OBLIQ_J2000 - vect(2) * PRECISE_SIN_OBLIQ_J2000
          , vect(2) * PRECISE_COS_OBLIQ_J2000 + vect(1) * PRECISE_SIN_OBLIQ_J2000)
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Conversion.scala
//=============================================================================