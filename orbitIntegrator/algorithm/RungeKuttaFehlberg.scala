/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  30/Jul/2024
 * Time:  13h:57m
 * Description: Runge-Kutta-Fehlberg fifth-order integrator
 * Adapted from: https://github.com/Bill-Gray/lunar/blob/master/integrat.cpp
 */
package com.common.orbitIntegrator.algorithm
//=============================================================================
import com.common.constant.astronomy.AstronomicalUnit._
import com.common.jpl.ephemeris.JplEphemeris
import com.common.jpl.ephemeris.JplEphemerisItem._
import com.common.orbitIntegrator.celestialBody.CelestialBody
import com.common.orbitIntegrator.celestialBody.NonClassic.cometPosnAndVel
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object RungeKuttaFehlberg {
  //---------------------------------------------------------------------------
  final val N_VALUES      = 6  /* i.e.,  a state vector consumes six values: x, y, z, vx, vy, vz */
  //---------------------------------------------------------------------------
  final val N_PERTURBERS  = 13
  //---------------------------------------------------------------------------
  private final val B_VALS: Array[Double] = Array(
    2.0 / 9.0, 1.0 / 12.0, 1.0 / 4.0, 69.0 / 128.0, -243.0 / 128.0, 135.0 / 64.0,
    -17.0 / 12.0, 27.0 / 4.0, -27.0 / 5.0, 16.0 / 15.0, 65.0 / 432.0, -5.0 / 16.0,
    13.0 / 16.0, 4.0 / 27.0, 5.0 / 144.0, 47.0 / 450.0, 0.0, 12.0 / 25.0, 32.0 / 225.0,
    1.0 / 30.0, 6.0 / 25.0, -1.0 / 150.0, 0.0, 0.03, -16.0 / 75.0, -0.05, 0.24
  )
  //---------------------------------------------------------------------------
  private final val AVALS: Array[Double] = Array(0.0, 2.0 / 9.0, 1.0 / 3.0, 0.75, 1.0, 5.0 / 6.0)
  //---------------------------------------------------------------------------
  private final val FUDGE_FACTOR  = 0.9
  //---------------------------------------------------------------------------
  private final val PLANET_RADIUS_FUDGE_FACTOR = PLANET_RADIUS map ( _ * FUDGE_FACTOR)
  //---------------------------------------------------------------------------
  private val positionCacheSize = 0
  //---------------------------------------------------------------------------
  def calculate(cb: CelestialBody
                , jdStart: Double
                , jdEnd: Double
                , maxErrInit: Double
                , perturberMask: Long
                , posSeq: Array[Double]
                , jplEphemeris: JplEphemeris): (Array[Double],Int)= {

    val inputValueSeq  = ArrayBuffer.fill[Double](N_VALUES)(0D)
    var t0 = jdStart
    var step = jdEnd - jdStart
    var failedStepCount = 0
    val maxErr = maxErrInit * maxErrInit
    val failedStepCountFactor = 0.9
    var resultValueSeq: Array[Double] = null

    while (t0 != jdEnd) {
      val (calculatedValueSeq,errSeq) =
        takeStep(cb
                 , t0
                 , inputValueSeq.toArray
                 , step
                 , perturberMask
                 , posSeq
                 , jplEphemeris)

      resultValueSeq = calculatedValueSeq
      var errVal = 0.0
      for (i <- 0 until N_VALUES)
        errVal += errSeq(i) * errSeq(i)

      if (errVal < maxErr) { // It was a good step
        inputValueSeq.clear()
        inputValueSeq ++= calculatedValueSeq
        t0 += step
      }
      else failedStepCount += 1

      step *= failedStepCountFactor * math.exp(math.log(maxErr / errVal) / 5.0)

      if (t0 < jdEnd && t0 + step > jdEnd) step = jdEnd - t0
      if (jdEnd < t0 && t0 + step < jdEnd) step = jdEnd - t0
    }
    (resultValueSeq,failedStepCount)
  }
  //---------------------------------------------------------------------------
  private def takeStep(cb: CelestialBody
                       , jd: Double
                       , inputValueSeq: Array[Double]
                       , stepSize: Double
                       , perturberMask: Long
                       , posSeq: Array[Double]
                       , jplEphemeris: JplEphemeris): (Array[Double],Array[Double]) = {


    val posnData = ArrayBuffer[Double]()

    if (math.abs(stepSize - posSeq(1)) < 0.000001) {
      val cacheLoc = math.floor((jd - posSeq(0)) / stepSize + 0.5).toInt
      if (cacheLoc >= 0 && cacheLoc < positionCacheSize) {
        posnData.clear()
        posnData ++= posSeq.slice(2 + cacheLoc * 6 * N_PERTURBERS * 3, posSeq.size)
      }
    }

    //(posAndVelocitySeq,deriveSeq)
    val r = ArrayBuffer[(Array[Double],Array[Double])]()

    r += computeDerivatives(
      cb
      , jd
      , inputValueSeq
      , posnData.toArray
      , perturberMask
      , jplEphemeris)

    val ivalsP = ArrayBuffer.fill[Double](7,6)(0d)
    ivalsP(0) = ArrayBuffer[Double]() ++ r.head._2
    var bValIndex  = 0

    for (j <- 1 until 7) {
      val newInputValueSeq =  (for (i <- 0 until N_VALUES) yield {
        var tval = 0.0
        for (k <- 0 until j)
          tval += B_VALS(k + bValIndex) * ivalsP(k)(i)
        tval * stepSize + inputValueSeq(i)
      }).toArray

      bValIndex += j

      if (j != 6) {
        r += computeDerivatives(
          cb
          , jd + stepSize * AVALS(j)
          , inputValueSeq = newInputValueSeq
          , posnData = if (!posnData.isEmpty) posnData.slice(j * N_PERTURBERS * 3, posnData.length).toArray else Array[Double]()
          , perturberMask
          , jplEphemeris)
        ivalsP(j) = ArrayBuffer[Double]() ++ r.last._2
      }
      else
        ivalsP(j) = ArrayBuffer[Double]() ++ newInputValueSeq
    }

    //calculate error
    val errorSeq = (
      for (i <- 0 until N_VALUES) yield {
         var tval = 0.0
         for (k <- 0 until 6)
           tval += B_VALS(k + bValIndex) * ivalsP(k)(i)
         stepSize * tval
    }).toArray

    (ivalsP(6).toArray,errorSeq)
  }
  //---------------------------------------------------------------------------
  def computePerturber(perturberNo: Int
                       , jd: Double
                       , jplEphemeris: JplEphemeris): Array[Double] = {
    val perturberLoc = ArrayBuffer.fill(3)(0d)

    //No VSOP data loaded, using JPL ephemeris
    val earthMoonMassRatio = 1.0 + jplEphemeris.earthMoonMassRatio
    var jd0 = -1d
    val (posns, _) = jplEphemeris.jplState(
      jd
      , interpolationRequestTypeSeq = Array.tabulate(14)(i => if (i <= 10) 1 else 0)
      , barycenter = 0)
    for (i <- 0 until 3) {
      posns(EARTH_MOO_BARYCENTER_EPHEMERIS_HEADER_ITEM_INDEX)(i) -= posns(MOON_EPHEMERIS_HEADER_ITEM_INDEX)(i) / earthMoonMassRatio
      posns(MOON_EPHEMERIS_HEADER_ITEM_INDEX)(i) += posns(EARTH_MOO_BARYCENTER_EPHEMERIS_HEADER_ITEM_INDEX)(i)
    }
    jd0 = jd

    for (i <- 0 until SUN_EPHEMERIS_HEADER_ITEM_INDEX) {
      val temp = posns(i)(1) * PRECISE_COS_OBLIQ_J2000 + posns(i)(2) * PRECISE_SIN_OBLIQ_J2000
      posns(i)(2) = posns(i)(2) * PRECISE_COS_OBLIQ_J2000 - posns(i)(1) * PRECISE_SIN_OBLIQ_J2000
      posns(i)(1) = temp
    }

    //rotate equatorial J2000.0 into ecliptical J2000
    for (i <- 0 until 3)
      perturberLoc(i) = posns(perturberNo - 1)(i)

    perturberLoc.toArray
  }
  //---------------------------------------------------------------------------
  private def computeDerivatives(cb: CelestialBody
                                 , jd: Double
                                 , inputValueSeq: Array[Double]
                                 , posnData: Array[Double]
                                 , perturberMask: Long
                                 , jplEphemeris: JplEphemeris): (Array[Double], Array[Double]) = {
    val (location, velocity) = cometPosnAndVel(cb, jd)

    val posAndVelocitySeq = location.take(3) ++ velocity
    val accel = setDifferentialAcceleration(posAndVelocitySeq, inputValueSeq)

    for (i <- 0 until N_PERTURBERS) {
      val r = (perturberMask >> i) & 1L
      if (r != 0) {
        val perturberLoc =
          if (!posnData.isEmpty) posnData.drop(i * 3)
          else {
            if (i < 10) computePerturber(i + 1, jd, jplEphemeris)
            else Array(1e8d, 1e8d, 1e8d)
          }

        val diff = ArrayBuffer.fill(3)(0.0)
        var diffSquared = 0.0
        var radiusSquared = 0.0
        for (j <- 0 until 3) {
          diff(j) = perturberLoc(j) - (posAndVelocitySeq(j) + inputValueSeq(j))
          diffSquared += diff(j) * diff(j)
          radiusSquared += perturberLoc(j) * perturberLoc(j)
        }

        val d = math.sqrt(diffSquared)
        val r = math.sqrt(radiusSquared)
        var dFactor = PLANET_RELATIVE_MASS(i + 1) / (diffSquared * d)
        var rFactor = PLANET_RELATIVE_MASS(i + 1) / (radiusSquared * r)

        if (i < 10) {
          if (d < PLANET_RADIUS_FUDGE_FACTOR(i)) dFactor *= computeAccelMultiplier(d / PLANET_RADIUS_FUDGE_FACTOR(i))
          if (r < PLANET_RADIUS_FUDGE_FACTOR(i)) rFactor *= computeAccelMultiplier(r / PLANET_RADIUS_FUDGE_FACTOR(i))
        }

        for (j <- 0 until 3)
          accel(j) += diff(j) * dFactor - perturberLoc(j) * rFactor
      }
    }

    val derivSeq_1 =
      (for (i <- 0 until 3) yield
        inputValueSeq(i + 3)).toArray
    val derivSeq_2 =
      (for (i <- 0 until 3) yield
        SOLAR_GM * accel(i)).toArray

    (posAndVelocitySeq, derivSeq_1 ++ derivSeq_2)

  }
  //---------------------------------------------------------------------------
  private def computeAccelMultiplier(_fraction: Double): Double = {
    var fraction = _fraction
    val r0 = 0.8d
    var rval = 0.0d

    assert(fraction >= 0d && fraction <= 1d)
    if (fraction < r0) rval = 0.0
    else {
      fraction = (fraction - r0) / (1.0 - r0)
      assert(fraction >= 0d && fraction <= 1d)
      rval = fraction * fraction * (3.0 - 2.0 * fraction)
    }
    rval
  }
  //---------------------------------------------------------------------------
  private def addRelativisticAcceleration(accel: ArrayBuffer[Double]
                                         , posAndVelocitySeq: Array[Double]): Unit = {

    // Constants
    val c = ASTRONOMICAL_UNIT_PER_DAY // Speed of light in AU per day
    val rSquared = posAndVelocitySeq.take(3).map(x => x * x).sum
    val vSquared = posAndVelocitySeq.slice(3, 6).map(x => x * x).sum
    val vDotR = posAndVelocitySeq.take(3).zip(posAndVelocitySeq.slice(3, 6)).map { case (r, v) => r * v }.sum
    val r = math.sqrt(rSquared)
    val rCubedC2 = rSquared * r * c * c

    // Compute components
    val rComponent = (4.0 * SOLAR_GM / r - vSquared) / rCubedC2
    val vComponent = 4.0 * vDotR / rCubedC2

    // Update acceleration vector
    for (i <- 0 until 3)
      accel(i) += rComponent * posAndVelocitySeq(i) + vComponent * posAndVelocitySeq(i + 3)
  }
  //---------------------------------------------------------------------------
  private def setDifferentialAcceleration(posAndVelocitySeq: Array[Double]
                                         , delta: Array[Double]): ArrayBuffer[Double] = {

    // Temporary array to hold the adjusted position-velocity vector
    val posAndVelocitySeq_2 = posAndVelocitySeq.zip(delta).map { case (pv, d) => pv + d }

    // Compute squared norms
    val pSquared = posAndVelocitySeq.take(3).map(x => x * x).sum
    val rSquared = posAndVelocitySeq_2.take(3).map(x => x * x).sum

    // Compute factors
    val pfactor = 1.0 / (pSquared * math.sqrt(pSquared))
    val rfactor = 1.0 / (rSquared * math.sqrt(rSquared))

    // Compute acceleration
    val accel = ArrayBuffer.fill(3)(Double.NaN)
    for (i <- 0 until 3)
      accel(i) = pfactor * posAndVelocitySeq(i) - rfactor * posAndVelocitySeq_2(i)

    // Add relativistic acceleration
    addRelativisticAcceleration(accel, posAndVelocitySeq_2)
    accel
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//=============================================================================
//End of file RungeKuttaFehlberg.scala
//=============================================================================