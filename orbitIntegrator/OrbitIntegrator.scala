/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  29/Jul/2024
 * Time:  20h:19m
 * Description: Date integration
 * https://github.com/Bill-Gray/lunar/blob/master/integrat.cpp
 */
package com.common.orbitIntegrator
//=============================================================================
import celestialBody.CelestialBody
import com.common.constant.astronomy.AstronomicalUnit.JD_2000_EPOCH
import com.common.database.mongoDB.mpc.MPO._
import com.common.database.mongoDB.mpc.MpoDB
import com.common.hardware.cpu.CPU
import com.common.jpl.ephemeris.JplEphemeris
import com.common.logger.MyLogger
import com.common.orbitIntegrator.algorithm.RungeKuttaFehlberg
import com.common.orbitIntegrator.algorithm.RungeKuttaFehlberg.{N_PERTURBERS, computePerturber}
import com.common.orbitIntegrator.celestialBody.Classic.classicOrbitalElements
import com.common.orbitIntegrator.celestialBody.NonClassic.cometPosnAndVel
import com.common.util.parallelTask.ParallelTask
import com.common.util.time.Time

import scala.collection.JavaConverters.mapAsScalaConcurrentMapConverter
//=============================================================================
import java.time.LocalDateTime
import java.util.concurrent.ConcurrentLinkedQueue
import scala.collection.JavaConverters.iterableAsScalaIterableConverter
import scala.collection.mutable.ArrayBuffer
import java.util.concurrent.ConcurrentHashMap
//=============================================================================
//=============================================================================
object OrbitIntegrator extends MyLogger {
  //---------------------------------------------------------------------------
  private final val DEFAULT_INTEGRATE_ORBIT_ERROR = 1e-12
  //---------------------------------------------------------------------------
  //perturbers
   private final val DEFAULT_PERTURBER_MASK_MAP = Map(
      1-> 1023L  //ceres
    , 2-> 2047L  //pallas
    , 3-> 4095L  //juno
    , 4-> 4095L  //vesta
  )
  private final val DEFAULT_PERTURBER_MASK = 8191L
  //---------------------------------------------------------------------------
  //MPO to ignore
  private final val MPO_ID_SEQ_TO_IGNORE = Seq(
    MPC_ID_PLUTO
  )
  //---------------------------------------------------------------------------
  //cache
  private val positionCache = new ConcurrentHashMap[String,(SOF,Double,Array[Double])]()
  private final val CACHE_AVALS = Array(0.0, 2.0 / 9.0, 1.0 / 3.0, 0.75, 1.0, 5.0 / 6.0)
  //---------------------------------------------------------------------------
  private class LoadSOF_Seq(mpcIdSeq: Array[Long]
                            , mpoMap: ConcurrentLinkedQueue[SOF]
                            , mpoDB: MpoDB
                            , verbose: Boolean) extends ParallelTask[Long](
    mpcIdSeq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100
    , verbose = verbose
    , message = Some("loading MPO from database and building SOF")) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(mpcID: Long) = {
      val mpo = mpoDB.getByNumber(mpcID)
      if (mpo.isDefined) mpoMap.add(SOF(mpo.get, CelestialBody(mpo.get)))
      else error(s"Unknown MPO with mpcID:'$mpcID'")
    }
    //-----------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  class IntegrateOrbitSeq(sofSeq: Array[SOF]
                          , targetDateJD: Double
                          , stepSizeInDays: Double
                          , maxIntegrateError: Double
                          , jplEphemeris: JplEphemeris
                          , updateCelestialBodySeq: ConcurrentHashMap[Int,CelestialBody]
                          , verbose: Boolean) extends ParallelTask[SOF](
    sofSeq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100
    , verbose = verbose
    , message = Some("processing sof")) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(sof: SOF) = {

      //get perturber mask
      val perturberMask = DEFAULT_PERTURBER_MASK_MAP.get(sof.name.toInt)
                                                    .getOrElse(DEFAULT_PERTURBER_MASK)
      //update cache
      val posSeq =
        if (positionCache.get(sof.name) == null) {
          val _posSeq = makePositionCache(
              sof.cb.epoch
            , targetDateJD
            , stepSizeInDays
            , perturberMask
            , jplEphemeris)
         positionCache.put(sof.name, (sof, targetDateJD, _posSeq))
          _posSeq
        }
        else positionCache.get(sof.name)._3

      //integrate orbit
      integrateOrbit(
        sof.cb
        , _jdStart = sof.cb.epoch
        , jdEnd = targetDateJD
        , stepSizeInDays
        , maxIntegrateError
        , perturberMask
        , posSeq
        , jplEphemeris)

      updateCelestialBodySeq.put(sof.name.toInt,sof.cb)
    }
    //-----------------------------------------------------------------------
  }

  //---------------------------------------------------------------------------
  def allMpoIntegration(timeStamp:LocalDateTime
                        , stepSizeInDays: Int
                        , outputDir: String
                        , maxIntegrateError: Double = DEFAULT_INTEGRATE_ORBIT_ERROR
                        , verbose: Boolean = true) = {

    if (verbose) info("Opening the MPO database")
    if (verbose) info("Loading the MPO to be integrated")
    val mpoStorage = new ConcurrentLinkedQueue[SOF]()
    val mpoDB = MpoDB()
    val mpcIdSeq = mpoDB
      .getAllIdSeq()
      .toArray
      .filter(mpcID => !MPO_ID_SEQ_TO_IGNORE.contains(mpcID))

    if (verbose) info(s"Loading:'${mpcIdSeq.length}' MPOs with assigned MPC number")
    if (verbose) info(s"Ignored:'${MPO_ID_SEQ_TO_IGNORE.length}' MPOs:'${MPO_ID_SEQ_TO_IGNORE.mkString("{",",","}")}'")

    new LoadSOF_Seq(
        mpcIdSeq //avoid certain MPO
      , mpoStorage
      , mpoDB
      , verbose)

    mpoDB.close()
    if (verbose) info("Closing the MPO database")
    if (verbose) info(s"Processing:'${mpoStorage.size()}' MPOs with assigned MPC number")

    val updateCelestialBodySeq = new ConcurrentHashMap[Int, CelestialBody]()
    val jplEphemeris = JplEphemeris.loadFromAsciiDir().get
    new IntegrateOrbitSeq(
        mpoStorage.asScala.toArray
      , targetDateJD = Time.toJulian(timeStamp)
      , stepSizeInDays
      , maxIntegrateError
      , jplEphemeris
      , updateCelestialBodySeq
      , verbose)

    val tsvName = timeStamp.toLocalDate.toString + "_" + timeStamp.plusDays(stepSizeInDays).toLocalDate.toString + ".tsv"

    CelestialBody.saveToTSV(mpcIdSeq
                            , updateCelestialBodySeq.asScala.toArray.map(_._2)
                            , s"$outputDir/$tsvName")

    info(s"Created orbit integration file:'$tsvName'")
  }
  //---------------------------------------------------------------------------
  def run(timeStamp: LocalDateTime
          , mpcIdSeq: Array[Long]
          , stepSizeInDays: Double = 10
          , maxIntegrateError: Double = DEFAULT_INTEGRATE_ORBIT_ERROR
          , verbose: Boolean) = {

    if (verbose) info("Opening the MPO database")
    if (verbose) info("Loading the MPO to be integrated")
    val mpoStorage = new ConcurrentLinkedQueue[SOF]()
    val mpoDB = MpoDB()
    new LoadSOF_Seq(mpcIdSeq.filter(mpcID => !MPO_ID_SEQ_TO_IGNORE.contains(mpcID)) //avoid certain MPO
      , mpoStorage
      , mpoDB
      , verbose)
    mpoDB.close()
    if (verbose) info("Closing the MPO database")
    if (verbose) info(s"Loaded:'${mpoStorage.size()}' MPOs")
    if (verbose) info(s"Processing:'${mpoStorage.size()}' MPOs")

    val updateCelestialBodySeq = new ConcurrentHashMap[Int,CelestialBody]()
    val jplEphemeris = JplEphemeris.loadFromAsciiDir().get
    new IntegrateOrbitSeq(mpoStorage.asScala.toArray
                         , targetDateJD = Time.toJulian(timeStamp)
                         , stepSizeInDays
                         , maxIntegrateError
                         , jplEphemeris
                         , updateCelestialBodySeq
                         , verbose)
    updateCelestialBodySeq.asScala
  }
  //--------------------------------------------------------------------------
  private def calculateEndIntegrationDateJD(jdStart: Double
                                           , jdEnd: Double
                                           , stepSizeInDays: Double) = {
    var jd2 = math.floor((jdStart - 0.5) / stepSizeInDays + 0.5) * stepSizeInDays + 0.5
    if (jdStart < jdEnd) { // integrating forward
      jd2 += stepSizeInDays
      if (jd2 > jdEnd) jd2 = jdEnd // going past the end; truncate step
    }
    else { // integrating backward
      jd2 -= stepSizeInDays
      if (jd2 < jdEnd) jd2 = jdEnd
    }
    jd2
  }
  //--------------------------------------------------------------------------
  // Original code
  //'integrate_orbit' integrates the elements over the desired time span to
  //   the desired maximum error,  using the number of steps requested.  The
  //   orbit is broken up into that many steps,  and 'full_rk_step' is then
  //   called for each step.  The individual steps will probably be taken in
  //   one RKF step,  but if their errors prove to be too great,  they'll
  //   be broken into sub-steps
  //int integrate_orbit( ELEMENTS *elem, double jd, const double jd_end,
  //                              const double max_err, const double stepsize)
  private def integrateOrbit(cb: CelestialBody
                             , _jdStart: Double
                             , jdEnd: Double
                             , stepSizeInDays: Double
                             , maxError: Double
                             , perturberMask: Long
                             , posSeq: Array[Double]
                             , jplEphemeris: JplEphemeris) = {
    var jdStart = _jdStart
    var stepCount = 0

    while(jdStart != jdEnd) {

      val endIntegrationDate  = calculateEndIntegrationDateJD(jdStart, jdEnd, stepSizeInDays)

      val (resultValueSeq,_) = RungeKuttaFehlberg.calculate(
        cb
        , jdStart
        , endIntegrationDate
        , maxError
        , perturberMask
        , posSeq
        , jplEphemeris)

        jdStart  = endIntegrationDate
        val (location,velocity) = cometPosnAndVel(cb, jdStart)
        cb.epoch = jdStart
        val locAndVeloSeq = ((location.take(3) ++ velocity) zip resultValueSeq) map { t =>  t._1 + t._2 }
        classicOrbitalElements(cb
          , locAndVeloSeq
          , jdStart
          , 1)
        stepCount += 1
    }

    val (location,velocity) = cometPosnAndVel(cb, jdStart)
    val locAndVeloSeq = location.take(3) ++ velocity
    cb.epoch = jdEnd
    classicOrbitalElements(
        cb
      , locAndVeloSeq
      , jdEnd
      , 1)
  }
  //---------------------------------------------------------------------------
  private def makePositionCache(_startJd: Double
                                , targetDateJD: Double
                                , stepSizeInDays: Double
                                , perturberMask: Long
                                , jplEphemeris: JplEphemeris): Array[Double] = {
    val maxJd = JD_2000_EPOCH
    var startJd = _startJd
    var endJd = targetDateJD

    if (startJd > endJd) {
      val tval = startJd
      startJd = endJd
      endJd = tval
    }

    if (startJd > maxJd) startJd = maxJd

    startJd = Math.floor((startJd - 0.5) / stepSizeInDays) * stepSizeInDays + 0.5
    val nSteps = Math.floor((endJd - startJd) / stepSizeInDays).toInt + 5
    startJd -= stepSizeInDays * 2

    val rval = ArrayBuffer[Double]()
    rval += startJd
    rval += stepSizeInDays

    for (_ <- 0 until nSteps) {
      for (j <- CACHE_AVALS.indices) {
        for (i <- 0 until N_PERTURBERS) {
          if (i < 10 && ((perturberMask >> i) & 1) == 1)
            rval ++= computePerturber(i + 1
                                      , startJd + CACHE_AVALS(j) * stepSizeInDays
                                      , jplEphemeris)
          else
            rval ++= Array(1.0e8d, 1.0e8d, 1.0e8d)   //put it far,  far away where it won't do anything:
        }
      }
      startJd += stepSizeInDays
    }
    rval.toArray
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file OrbitIntegration.scala
//=============================================================================
