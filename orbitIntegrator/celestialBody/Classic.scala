/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  31/Jul/2024
 * Time:  11h:16m
 * Description: Adaptation of:
 * https://github.com/Bill-Gray/lunar/blob/master/classel.cpp
 */
package com.common.orbitIntegrator.celestialBody
//=============================================================================
import com.common.constant.astronomy.AstronomicalUnit.PRECISE_PI
import com.common.orbitIntegrator.celestialBody.Util.{asine, asinh, vectorCrossProduct}
//=============================================================================
import java.lang.Math._
//=============================================================================
//=============================================================================
object Classic {
  //---------------------------------------------------------------------------
  def classicOrbitalElements(cb:CelestialBody
                             , locAndVeloSeq: Array[Double]
                             , t: Double
                             , ref: Int): Unit = {
    val v = locAndVeloSeq.drop(3)
    val rDotV = dotProduct(locAndVeloSeq, v)
    val dist = vector3Length(locAndVeloSeq.take(3))
    val v2 = dotProduct(v, v)
    var invMajorAxis = 2.0 / dist - v2 / cb.gm
    var h0, n0, tval = 0.0
    var ecc2 = 0.0
    var ecc = 0.0

    assert(cb.gm != 0.0)
    val h = vectorCrossProduct(locAndVeloSeq,v)
    n0 = h(0) * h(0) + h(1) * h(1)
    h0 = n0 + h(2) * h(2)
    assert(dist > 0.0)
    assert(v2 > 0.0)
    assert(h0 > 0.0)
    n0 = sqrt(n0)
    h0 = sqrt(h0)

    if ((ref & 1) != 0) {
      if (n0 == 0.0) cb.ascNode = 0.0
      else cb.ascNode = atan2(h(0), -h(1))
      cb.incl = asine(n0 / h0)
      if (h(2) < 0.0) cb.incl = PRECISE_PI - cb.incl
    }

    var e = vectorCrossProduct(v, h)
    e = (for (i <- e.indices) yield e(i) / cb.gm - locAndVeloSeq(i) / dist).toArray
    tval = dotProduct(e, h) / h0
    for (i <- e.indices) e(i) -= h(i) * tval
    ecc2 = dotProduct(e, e)
    if (abs(ecc2 - 1.0) < 1e-14) ecc2 = 1.0
    cb.minorToMajor = sqrt(abs(1.0 - ecc2))
    ecc = sqrt(ecc2)
    cb.ecc = ecc

    if (ecc == 0.0)
      e = (for (i <- e.indices) yield locAndVeloSeq(i) / dist).toArray
    else
      e = (for (i <- e.indices) yield e(i) / ecc).toArray

    if (ecc < 0.9)
      cb.q = (1.0 - ecc) / invMajorAxis
    else {
      val gmOverH0 = cb.gm / h0
      val perihelionSpeed = gmOverH0 * (1.0 + sqrt(1.0 - invMajorAxis * h0 * h0 / cb.gm))
      assert(h0 != 0.0)
      assert(gmOverH0 != 0.0)
      assert(isFinite(invMajorAxis))
      assert(isFinite(gmOverH0))
      assert(isFinite(perihelionSpeed))
      assert(perihelionSpeed != 0.0)
      cb.q = h0 / perihelionSpeed
      assert(cb.q != 0.0)
      invMajorAxis = (1.0 - ecc) / cb.q
    }

    assert(cb.q != 0.0)
    assert(cb.q > 0.0)

    if (invMajorAxis != 0.0) {
      cb.majorAxis = 1.0 / invMajorAxis
      cb.t0 = cb.majorAxis * sqrt(abs(cb.majorAxis) / cb.gm)
    }

    cb.sideways = vectorCrossProduct(h, e)

    if ((ref & 1) != 0) {
      val cosArgPer = if (n0 != 0.0) (h(0) * e(1) - h(1) * e(0)) / n0 else e(0)
      cb.argPer =
        if (cosArgPer < 0.7 && cosArgPer > -0.7)
          acos(cosArgPer)
        else {
          val sinArgPer =
            if (n0 != 0.0)
              (e(0) * h(0) * h(2) + e(1) * h(1) * h(2) - e(2) * n0 * n0) / (n0 * h0)
            else
              e(1) * h(2) / h0
          var argPer = abs(asin(sinArgPer))
          if (cosArgPer < 0.0) argPer = PRECISE_PI - argPer
          if (e(2) < 0.0) argPer = PRECISE_PI + PRECISE_PI - argPer
          argPer
        }
    }

    if (invMajorAxis != 0.0 && cb.minorToMajor != 0.0) {
      val isNearlyParabolic = ecc > 0.99999 && ecc < 1.00001
      val rCosTrueAnom = dotProduct(locAndVeloSeq, e)
      val rSinTrueAnom = dotProduct(locAndVeloSeq, cb.sideways) / h0
      val sinE = rSinTrueAnom * invMajorAxis / cb.minorToMajor
      assert(cb.minorToMajor != 0.0)
      assert(isFinite(ecc))
      assert(isFinite(h0))
      assert(isFinite(rCosTrueAnom))
      assert(isFinite(rSinTrueAnom))
      assert(isFinite(sinE))

      if (invMajorAxis > 0.0) {
        val cosE = rCosTrueAnom * invMajorAxis + ecc
        val eccAnom = atan2(sinE, cosE)
        assert(isFinite(cosE))
        assert(isFinite(eccAnom))
        if (isNearlyParabolic)
          cb.meanAnomaly = eccAnom * (1 - ecc) - ecc * eccAnom * remainingTerms(-eccAnom * eccAnom)
        else
          cb.meanAnomaly = eccAnom - ecc * sinE
        assert(isFinite(cb.meanAnomaly))
        cb.perihTime = t - cb.meanAnomaly * cb.t0
      }
      else {
        val eccAnom = asinh(sinE)
        if (isNearlyParabolic)
          cb.meanAnomaly = eccAnom * (1 - ecc) - ecc * eccAnom * remainingTerms(eccAnom * eccAnom)
        else
          cb.meanAnomaly = eccAnom - ecc * sinE
        assert(isFinite(cb.meanAnomaly))
        assert(cb.t0 <= 0.0)
        cb.perihTime = t - cb.meanAnomaly * abs(cb.t0)
        h0 = -h0
      }
    } else {
      var tau = sqrt(dist / cb.q - 1.0)
      if (rDotV < 0.0) tau = -tau
      cb.w0 = (3.0 / sqrt(2)) / (cb.q * sqrt(cb.q / cb.gm))
      cb.perihTime = t - tau * (tau * tau / 3.0 + 1.0) * 3.0 / cb.w0
    }

    cb.perihVec  = e
    cb.sideways = (for (i<-0 until 3) yield cb.sideways(i) / h0).toArray

    cb.angularMomentum = h0
  }
  //---------------------------------------------------------------------------
  private def remainingTerms(ival: Double): Double = {
    var rval = 0.0
    var z = 1.0
    val tolerance = 1e-30
    var i = 2

    do {
      z *= ival / (i * (i + 1))
      rval += z
      i += 2
    } while (abs(z) > tolerance)
    rval
  }
  //---------------------------------------------------------------------------
  private def dotProduct(a: Array[Double], b: Array[Double]): Double =
    a.zip(b).map { case (x, y) => x * y }.sum
  //---------------------------------------------------------------------------
  def vector3Length(a: Array[Double]): Double =
    sqrt(a.map(x => x * x).sum)
  //---------------------------------------------------------------------------
  private def isFinite(d: Double): Boolean = !d.isInfinity && !d.isNaN
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Classic.scala
//=============================================================================