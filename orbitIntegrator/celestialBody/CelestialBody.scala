/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  29/Jul/2024
 * Time:  11h:03m
 * Description: Adaptation of 'Elemets' and associated calculations
 * of a celestial body implemented in repository:
 * https://github.com/Bill-Gray/lunar
 */
package com.common.orbitIntegrator.celestialBody
//=============================================================================
import com.common.constant.astronomy.AstronomicalUnit._
import com.common.constant.astronomy.Earth.getTopoLoc
import com.common.coordinate.conversion.Conversion
import com.common.coordinate.conversion.Conversion.{eclipticToEquatorial, toPreciseDegrees, toPreciseRadians}
import com.common.database.mongoDB.mpc.MPO
import com.common.database.mongoDB.observatories.{ObservatoriesDB, Observatory}
import com.common.logger.MyLogger
import com.common.math.MyMath
import com.common.orbitIntegrator.OrbitIntegrator
import com.common.orbitIntegrator.celestialBody.Classic.vector3Length
import com.common.orbitIntegrator.celestialBody.NonClassic.{cometPosnAndVel, deriveQuantities}
import com.common.util.time.Time
//=============================================================================
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._

import java.io.{BufferedWriter, FileWriter}
import java.nio.file.{Files, Paths}
import java.time.LocalDateTime
import scala.language.existentials
//=============================================================================
//=============================================================================
object CelestialBody {
  //---------------------------------------------------------------------------
  val codecRegistry = fromRegistries(fromProviders(classOf[CelestialBody]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  def apply(mpo: MPO): CelestialBody = {
    val cb = CelestialBody(
      perihTime = Double.NaN
      , q = mpo.a * (1d - mpo.e)
      , ecc = mpo.e
      , incl = mpo.i * PRECISE_ANGLE_TO_RAD
      , argPer = mpo.Peri * PRECISE_ANGLE_TO_RAD
      , ascNode = mpo.Node * PRECISE_ANGLE_TO_RAD
      , epoch = mpo.Epoch
      , meanAnomaly = mpo.M * PRECISE_ANGLE_TO_RAD
      , lonPer = Double.NaN
      , minorToMajor = Double.NaN
      , perihVec = Array(Double.NaN)
      , sideways = Array(Double.NaN)
      , angularMomentum = Double.NaN
      , majorAxis = mpo.a
      , t0 = Double.NaN
      , w0 = Double.NaN
      , absMag = if (mpo.H.isDefined) mpo.H.get.toDouble else Double.NaN
      , slopeParam = if (mpo.G.isDefined) mpo.G.get.toDouble else Double.NaN
      , gm = SOLAR_GM
      , isAsteroid = 1
      , centralObj = 0
    )
    deriveQuantities(cb)
  }

  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[CelestialBody].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  def getColNameValueSeq(prefix: String = "") =
    classOf[CelestialBody].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = (prefix + f.getName, f.getType)
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  def saveToTSV(mpcIdSeq: Array[Long]
                , celestialBodies: Seq[CelestialBody]
                , fileName: String): Unit = {
    val file = Paths.get(fileName)
    if (!Files.exists(file.getParent)) Files.createDirectories(file.getParent)

    val writer = new BufferedWriter(new FileWriter(fileName))

    try {
      // Write header
      val headers = Array("mpcID") ++ CelestialBody.getColNameSeq("").dropRight(1)
      writer.write(headers.mkString("\t"))
      writer.newLine()

      // Write data rows
      celestialBodies.zipWithIndex.foreach { case (cb, i) => cb.saveAsTsv(writer, mpcIdSeq(i))}

    } finally {
      writer.close()
    }
  }
  //---------------------------------------------------------------------------
  //converts an angle in radians to an integer representation scaled by 32768/PI, and rounds it to the nearest integer.
  private def integerizeAngle(angle: Double): Short =
    math.floor(angle * 32768.0 / PRECISE_PI + 0.5).toInt.toShort

  //---------------------------------------------------------------------------
  //https://github.com/Bill-Gray/lunar/blob/master/delta_t.cpp
  private def tdMinusUT(jd: Double): Double = {
    val year = 2000.0 + (jd - JD_2000_EPOCH) / 365.25
    if (year < 1620.0) return Double.NaN

    val index_loc = (year - 1620.0) / 2.0
    val index = index_loc.toInt
    val tableSize = DELTA_T_TABLE.length

    var finalIndex = index
    if (index > tableSize - 2 || jd > 3e+7)
      finalIndex = tableSize - 2

    var dt = index_loc - finalIndex.toDouble
    val tptr = DELTA_T_TABLE.slice(finalIndex, finalIndex + 2)
    var rvalCalc = tptr(0) + (tptr(1) - tptr(0)) * dt
    rvalCalc /= 100.0

    if (dt > 1.0) {
      dt = (dt - 1.0) / 50.0
      rvalCalc += 32.5 * dt * dt
    }

    if (year < 1620.0) {
      val n = -23.8946
      rvalCalc -= 0.000091 * (n + 26.0) * (year - 1955.0) * (year - 1955.0)
    }

    rvalCalc
  }

  //---------------------------------------------------------------------------
  def computeLocation(mpcID: Int
                      , timeStamp: LocalDateTime
                      , observatoryID: String
                      , stepSizeInDays: Int = 15): Option[(Double, Double)] = {
    val observatory = ObservatoriesDB.findByID(observatoryID).get

    val updatedCelestialBodyMap = OrbitIntegrator.run(
      timeStamp
      , Array(mpcID)
      , stepSizeInDays
      , verbose = true)

    val r = updatedCelestialBodyMap(mpcID)
      .computeLocation(observatory
                      , timeStamp)
    if (r.isDefined) Some((r.get._1, r.get._2))
    else None
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.common.orbitIntegrator.celestialBody.CelestialBody._
case class CelestialBody(var perihTime: Double
                         , var q: Double
                         , var ecc: Double             //Orbital eccentricity
                         , var incl: Double            //Inclination to the ecliptic, J2000.0 (rad)
                         , var argPer: Double          //Argument of perihelion, J2000.0 (rad)
                         , var ascNode: Double         //Longitude of the ascending node, J2000.0 (rad)
                         , var epoch: Double           //Epoch of the orbit (Julian Date)
                         , var meanAnomaly: Double     //Mean anomaly at the epoch (rad)
                         , var lonPer: Double          //Calculated.
                         , var minorToMajor: Double    //Calculated.
                         , var perihVec: Array[Double] //Calculated.
                         , var sideways: Array[Double] //Calculated.
                         , var angularMomentum: Double //Calculated.
                         , var majorAxis: Double       //Calculated.
                         , var t0: Double              //Calculated. t0 = 1/n = time to move one radian in mean anomaly = orbital period / (2*pi)
                         , var w0: Double              //Calculated.
                         , var absMag: Double          //Calculated. Absolute magnitude
                         , var slopeParam: Double      //Calculated. Slope paraneter
                         , var gm: Double              //Calculated. SOLAR_GM
                         , var isAsteroid: Int         //Calculated.
                         , var centralObj: Int         //Calculated.
                        ) extends MyLogger {
  //---------------------------------------------------------------------------
  //https://github.com/Bill-Gray/lunar/blob/master/astcheck.cpp
  //( ra in decimal degrees
  // , dec in decimal degrees
  // , ra in HMS
  // , dec in DMS
  // , r1
  // , sun distance)
  def computeLocation(observatory: Observatory
                      , timeStamp: LocalDateTime): Option[(Double, Double, String, String, Double, Double)] = {

    val jd0  = Time.toJulian(timeStamp)
    val deltaT = tdMinusUT(jd0) / Time.SECONDS_IN_ONE_DAY
    val jd = jd0 + deltaT
    val earthLoc = getTopoLoc(jd
                             , toPreciseRadians(observatory.longitude)
                             , observatory.cosPhiR
                             , observatory.sinPhiR)
    var newDistance = 0.0
    var dist = 0.0
    var nIterations = 0
    var asteroidLoc: Array[Double] = null

    //light-time lag:  should converge very fast it'll almost always require exactly two iterations
    do {
      dist = newDistance
      asteroidLoc = cometPosnAndVel(this, jd - dist / ASTRONOMICAL_UNIT_PER_DAY)._1
      val loc = (asteroidLoc.take(3) zip earthLoc.take(3)) map { t => t._1 - t._2}
      newDistance = vector3Length(loc)
      asteroidLoc = loc ++ asteroidLoc.drop(3)
      if (nIterations >= 15) {
        error(s"Error calculation the location of celestial body with eccentricity :'$ecc'")
        return None
      }
      nIterations += 1
    } while (math.abs(dist - newDistance) > 0.002)

    val equatorialLoc = eclipticToEquatorial(asteroidLoc)

    val raRad = math.atan2(equatorialLoc(0), asteroidLoc(0))
    val decRad = math.asin(equatorialLoc(1) / newDistance)

    val ra = MyMath.normalizeAngle(toPreciseDegrees(raRad))
    val dec = toPreciseDegrees(decRad)

    Some(ra
         , dec
         , Conversion.DD_to_HMS_WithDivider(ra, threeDecimalPlaces= false)
         , Conversion.DD_to_DMS_WithDivider(dec, threeDecimalPlaces= false)
         , newDistance
         , asteroidLoc(3))  //sun distance
  }
  //---------------------------------------------------------------------------
  def saveAsTsv(writer: BufferedWriter, mpcID: Long): Unit = {
    val valueSeq = Array(mpcID) ++
      getColNameValueSeq("")
      .dropRight(1)
      .map { case (fieldName, _) =>
      val field = this.getClass.getDeclaredField(fieldName.stripPrefix("_")) // Access the field
      field.setAccessible(true)
      val value = field.get(this) // Get the value of the field

      val result = value match {
        case arr: Array[Double] => arr.mkString(",") // Handle Array[Double] as comma-separated values
        case other => other.toString // Convert other types to string
      }
      field.setAccessible(false)
      result
    }

    writer.write(valueSeq.mkString("\t"))
    writer.newLine()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CelestialBody.scala
//=============================================================================