/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  31/Jul/2024
 * Time:  12h:49m
 * Description: Adaptation of:
 * https://github.com/Bill-Gray/lunar/blob/master/precess.cpp
 */
package com.common.orbitIntegrator.celestialBody.earth
//=============================================================================
import com.common.constant.astronomy.AstronomicalUnit._
import com.common.orbitIntegrator.celestialBody.Util.{meanObliquity, setIdentityMatrix}
//=============================================================================
import java.lang.Math._
//=============================================================================
object EarthPrecesion {
  //---------------------------------------------------------------------------
  private final val SEMIRANDOM_GARBAGE1 = 314.8145501e+12
  private final val SEMIRANDOM_GARBAGE2 = -9.19001473e-08
  //---------------------------------------------------------------------------
  private def spinMatrix(matrix: Array[Double], axis: Int, angle: Double): Unit = {
    val c = cos(angle)
    val s = sin(angle)
    axis match {
      case 0 =>
        val a10 = matrix(3)
        val a11 = matrix(4)
        val a12 = matrix(5)
        matrix(3) = a10 * c + matrix(6) * s
        matrix(4) = a11 * c + matrix(7) * s
        matrix(5) = a12 * c + matrix(8) * s
        matrix(6) = matrix(6) * c - a10 * s
        matrix(7) = matrix(7) * c - a11 * s
        matrix(8) = matrix(8) * c - a12 * s
      case 1 =>
        val a00 = matrix(0)
        val a01 = matrix(1)
        val a02 = matrix(2)
        matrix(0) = a00 * c - matrix(6) * s
        matrix(1) = a01 * c - matrix(7) * s
        matrix(2) = a02 * c - matrix(8) * s
        matrix(6) = matrix(6) * c + a00 * s
        matrix(7) = matrix(7) * c + a01 * s
        matrix(8) = matrix(8) * c + a02 * s
      case 2 =>
        val a00 = matrix(0)
        val a01 = matrix(1)
        val a02 = matrix(2)
        matrix(0) = a00 * c + matrix(3) * s
        matrix(1) = a01 * c + matrix(4) * s
        matrix(2) = a02 * c + matrix(5) * s
        matrix(3) = matrix(3) * c - a00 * s
        matrix(4) = matrix(4) * c - a01 * s
        matrix(5) = matrix(5) * c - a02 * s
      case _ => throw new IllegalArgumentException("Invalid axis")
    }
  }
  //---------------------------------------------------------------------------
  private def invertOrthonormalMatrix(matrix: Array[Double]): Unit = {
    val tmp = Array(matrix(0), matrix(1), matrix(2))
    matrix(0) = matrix(0)
    matrix(1) = matrix(3)
    matrix(2) = matrix(6)
    matrix(3) = matrix(1)
    matrix(4) = matrix(4)
    matrix(5) = matrix(7)
    matrix(6) = matrix(2)
    matrix(7) = matrix(5)
    matrix(8) = matrix(8)
  }
  //---------------------------------------------------------------------------
  private def setupEclipticPrecessionFromJ2000(matrix: Array[Double], year: Double): Int = {
    val t = (year - 2000.0) / 100.0
    val S2R = (PRECISE_PI / 180.0) / 3600.0 // converts arcSeconds to Radians
    val eta = t * (47.0029 * S2R + (-0.03302 * S2R + 6.0e-5 * S2R * t) * t)
    val pie = 174.876384 * PRECISE_PI / 180.0 - t * (869.8089 * S2R - 0.03536 * S2R * t)
    val p = t * (5029.0966 * S2R + (1.11113 * S2R - 6.0e-5 * S2R * t) * t)

    setIdentityMatrix(matrix)
    matrix(0) = cos(pie)
    matrix(1) = sin(pie)
    matrix(3) = -matrix(1)
    matrix(4) = matrix(0)

    spinMatrix(matrix, 1, -eta)
    spinMatrix(matrix, 0, -p)
    spinMatrix(matrix, 1, pie)
    0
  }
  //---------------------------------------------------------------------------
  private def setupEquatorialPrecessionFromJ2000(matrix: Array[Double], year: Double): Int = {
    val tCen = (year - 2000.0) / 100.0
    val ka = 2306.2181
    val kb = 2004.3109
    val arcsecToRadians = (PRECISE_PI / 180.0) / 3600.0

    val zeta = tCen * (ka + tCen * (0.30188 + 0.017998 * tCen)) * arcsecToRadians
    val z = tCen * (ka + tCen * (1.09468 + 0.018203 * tCen)) * arcsecToRadians
    val theta = tCen * (kb + tCen * (-0.42665 - 0.041833 * tCen)) * arcsecToRadians

    val czeta = cos(zeta)
    val szeta = sin(zeta)
    val cz = cos(z)
    val sz = sin(z)
    val ctheta = cos(theta)
    val stheta = sin(theta)

    matrix(0) = czeta * ctheta * cz - szeta * sz
    matrix(1) = -szeta * ctheta * cz - czeta * sz
    matrix(2) = -stheta * cz

    matrix(3) = czeta * ctheta * sz + szeta * cz
    matrix(4) = -szeta * ctheta * sz + czeta * cz
    matrix(5) = -stheta * sz

    matrix(6) = czeta * stheta
    matrix(7) = -szeta * stheta
    matrix(8) = ctheta

    0
  }
  //---------------------------------------------------------------------------
  private def setupEclipticPrecession(matrix: Array[Double]
                                      , yearFrom: Double
                                      , yearTo: Double): Int = {

    var prevYearFrom = SEMIRANDOM_GARBAGE1
    var prevYearTo = SEMIRANDOM_GARBAGE2
    val prevMatrix = Array.fill(9)(0.0)

    if (abs(yearFrom - yearTo) < 1.0e-5) {
      setIdentityMatrix(matrix)
      return 0
    }

    if (yearFrom == prevYearFrom && yearTo == prevYearTo) {
      Array.copy(prevMatrix, 0, matrix, 0, 9)
      return 0
    }

    if (yearFrom == prevYearTo && yearTo == prevYearFrom) {
      Array.copy(prevMatrix, 0, matrix, 0, 9)
      invertOrthonormalMatrix(matrix)
      return 0
    }

    if (yearFrom == 2000.0) {
      setupEclipticPrecessionFromJ2000(matrix, yearTo)
    } else {
      setupEclipticPrecessionFromJ2000(matrix, yearFrom)
      invertOrthonormalMatrix(matrix)
      if (yearTo != 2000.0) {
        val product = Array.fill(9)(0.0)
        val tmatrix = Array.fill(9)(0.0)

        setupEclipticPrecessionFromJ2000(tmatrix, yearTo)
        for (i <- 0 until 3) {
          for (j <- 0 until 3) {
            product(j + i * 3) = matrix(i * 3) * tmatrix(j) +
              matrix(i * 3 + 1) * tmatrix(j + 3) +
              matrix(i * 3 + 2) * tmatrix(j + 6)
          }
        }
        Array.copy(product, 0, matrix, 0, 9)
      }
    }
    Array.copy(matrix, 0, prevMatrix, 0, 9)
    prevYearFrom = yearFrom
    prevYearTo = yearTo
    0
  }
  //---------------------------------------------------------------------------
  private def preSpinMatrix(matrix: Array[Double]
                            , axis: Int, angle: Double): Unit =
    spinMatrix(matrix, axis, angle)
  //---------------------------------------------------------------------------
  private def nutation(dLon: Array[Double], dObliq: Array[Double]): Unit = {
    dLon(0) = 0.0 // Placeholder
    dObliq(0) = 0.0 // Placeholder
  }
  //---------------------------------------------------------------------------
  private def precessVector(matrix: Array[Double], v1: Array[Double], v2: Array[Double]): Unit = {
    val tempV = Array.fill(3)(0.0)
    if (v1 sameElements v2) {
      Array.copy(v1, 0, tempV, 0, 3)
      for (i <- 0 until 3)
        v2(i) = matrix(i * 3) * tempV(0) + matrix(i * 3 + 1) * tempV(1) + matrix(i * 3 + 2) * tempV(2)
    }
    else
      for (i <- 0 until 3)
        v2(i) = matrix(i * 3) * v1(0) + matrix(i * 3 + 1) * v1(1) + matrix(i * 3 + 2) * v1(2)
  }
  //---------------------------------------------------------------------------
  private def deprecessVector(matrix: Array[Double], v1: Array[Double], v2: Array[Double]): Unit = {
    val tempV = Array.fill(3)(0.0)
    if (v1 sameElements v2) {
      Array.copy(v1, 0, tempV, 0, 3)
      for (i <- 0 until 3)
        v2(i) = matrix(i) * tempV(0) + matrix(i + 3) * tempV(1) + matrix(i + 6) * tempV(2)
    }
    else
      for (i <- 0 until 3)
        v2(i) = matrix(i) * v1(0) + matrix(i + 3) * v1(1) + matrix(i + 6) * v1(2)
  }
  //---------------------------------------------------------------------------
  private def precessRaDec(matrix: Array[Double]
                           , pOut: Array[Double]
                           , pIn: Array[Double], backward: Boolean): Unit = {
    val v1 = Array(cos(pIn(0)) * cos(pIn(1)), sin(pIn(0)) * cos(pIn(1)), sin(pIn(1)))
    val v2 = Array.fill(3)(0.0)
    if (backward) deprecessVector(matrix, v1, v2)
    else precessVector(matrix, v1, v2)
    if (v2(1) != 0.0 || v2(0) != 0.0) pOut(0) = atan2(v2(1), v2(0))
    else pOut(0) = 0.0
    pOut(1) = asin(v2(2))
    while (pOut(0) - pIn(0) > PRECISE_PI) pOut(0) -= PRECISE_TWO_PI
    while (pOut(0) - pIn(0) < -PRECISE_PI) pOut(0) += PRECISE_TWO_PI
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file EarthPrecesion.scala
//=============================================================================