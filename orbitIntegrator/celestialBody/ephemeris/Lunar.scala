/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  31/Jul/2024
 * Time:  12h:19m
 * Description: Adaptation of:
 * https://github.com/Bill-Gray/lunar/blob/master/lunar2.cpp
 */
package com.common.orbitIntegrator.celestialBody.ephemeris
//=============================================================================
import com.common.constant.astronomy.AstronomicalUnit.{PRECISE_ANGLE_TO_RAD, PRECISE_PI}
import com.common.orbitIntegrator.celestialBody.Util.{get32bits, getDouble}
//=============================================================================
//=============================================================================
object Lunar {
  //---------------------------------------------------------------------------
  private val LON_R_TERM_SIZE = 12
  private val LAT_TERM_SIZE = 8
  private val N_TERMS = 60
  private val LUNAR_LON_DIST_OFFSET = 59354
  private val LUNAR_LAT_OFFSET = LUNAR_LON_DIST_OFFSET + LON_R_TERM_SIZE * N_TERMS
  private val LUNAR_FUND_OFFSET = LUNAR_LAT_OFFSET + LAT_TERM_SIZE * N_TERMS
  //---------------------------------------------------------------------------
  def lunarFundamentals(data: Array[Byte]
                       , timeInCenturies: Double): Array[Double] = {
    val fund = Array.fill(9)(0.0)
    val tptr = data.slice(LUNAR_FUND_OFFSET, data.length)
    var tpow = timeInCenturies

    assert(get32bits(tptr, 0) == 0x6ed5a0b1)
    assert(get32bits(data, 0) == 0x00260000)

    for (i <- 0 until 5) {
      fund(i) = getDouble(tptr, 8 * i)
      tpow = timeInCenturies
      for (j <- 4 until 0 by -1) {
        tpow *= timeInCenturies
        fund(i) += tpow * getDouble(tptr, 8 * (5 + j))
      }
    }

    fund(5) = 119.75 + 131.849 * timeInCenturies
    fund(6) = 53.09 + 479264.290 * timeInCenturies
    fund(7) = 313.45 + 481266.484 * timeInCenturies
    fund(8) = timeInCenturies

    for (i <- 0 until 8)
      fund(i) = (fund(i) % 360).ensuring(_ >= 0) * PRECISE_ANGLE_TO_RAD
    fund
  }
  //---------------------------------------------------------------------------
  def lunarLonAndDist(data: Array[Byte]
                      , fund: Array[Double]
                      , precision: Long): (Double, Double) = {
    val tptr = data.slice(LUNAR_LON_DIST_OFFSET, data.length)
    var slSum = 0.0
    var srSum = 0.0
    val T = fund(8)
    val e = 1.0 - 0.002516 * T - 0.0000074 * T * T

    assert(get32bits(tptr, 0) == 0x00010000)

    for (i <- 0 until N_TERMS) {
      val sl = get32bits(tptr, 4 + i * LON_R_TERM_SIZE)
      val sr = get32bits(tptr, 8 + i * LON_R_TERM_SIZE)

      if (math.abs(sl) > precision || math.abs(sr) > precision) {
        val d = tptr(0 + i * LON_R_TERM_SIZE)
        val m = tptr(1 + i * LON_R_TERM_SIZE)
        val mp = tptr(2 + i * LON_R_TERM_SIZE)
        val f = tptr(3 + i * LON_R_TERM_SIZE)
        val arg = d * fund(1) + m * fund(2) + mp * fund(3) + f * fund(4)

        if (sl != 0) {
          var term = sl * math.sin(arg)
          for (_ <- 0 until math.abs(m)) term *= e
          slSum += term
        }

        if (sr != 0) {
          var term = sr * math.cos(arg)
          for (_ <- 0 until math.abs(m)) term *= e
          srSum += term
        }
      }
    }

    if (precision < 3959L) {
      slSum += 3958.0 * math.sin(fund(5)) + 1962.0 * math.sin(fund(0) - fund(4)) + 318.0 * math.sin(fund(6))
    }

    var lon = fund(0) * 180.0 / PRECISE_PI + slSum * 1e-6
    lon = (lon % 360 + 360) % 360

    val r = 385000.56 + srSum / 1000.0

    (lon, r)
  }
  //---------------------------------------------------------------------------
  def lunarLat(data: Array[Byte]
               , fund: Array[Double]
               , precision: Long): Double = {
    val tptr = data.slice(LUNAR_LAT_OFFSET, data.length)
    var rval = 0.0
    val T = fund(8)
    val e = 1.0 - 0.002516 * T - 0.0000074 * T * T

    assert(get32bits(tptr, 0) == 0x01000000)

    for (i <- 0 until N_TERMS) {
      val sb = get32bits(tptr, 4 + i * LAT_TERM_SIZE)

      if (math.abs(sb) > precision) {
        val d = tptr(0 + i * LAT_TERM_SIZE)
        val m = tptr(1 + i * LAT_TERM_SIZE)
        val mp = tptr(2 + i * LAT_TERM_SIZE)
        val f = tptr(3 + i * LAT_TERM_SIZE)
        val arg = d * fund(1) + m * fund(2) + mp * fund(3) + f * fund(4)

        var term = sb * math.sin(arg)
        for (_ <- 0 until math.abs(m)) term *= e
        rval += term
      }
    }

    if (precision < 2236L) {
      rval += -2235.0 * math.sin(fund(0)) + 382.0 * math.sin(fund(7)) + 175.0 * math.sin(fund(5) - fund(4)) +
        175.0 * math.sin(fund(5) + fund(4)) + 127.0 * math.sin(fund(0) - fund(3)) - 115.0 * math.sin(fund(0) + fund(3))
    }

    rval * 1e-6
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Lunar.scala
//=============================================================================