/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  31/Jul/2024
 * Time:  17h:09m
 * Description: Adaptation of:
 * https://github.com/Bill-Gray/lunar/blob/master/getplane.cpp
 */
package com.common.orbitIntegrator.celestialBody.ephemeris
//=============================================================================
import Lunar.{lunarFundamentals, lunarLat, lunarLonAndDist}
import com.common.constant.astronomy.AstronomicalUnit.{ASTRONOMICAL_UNIT_KM, PRECISE_ANGLE_TO_RAD}
import com.common.orbitIntegrator.celestialBody.Util.{meanObliquity, rotateVector, setIdentityMatrix}
//=============================================================================
import java.lang.Math._
//=============================================================================
//=============================================================================
object Planet {
  //---------------------------------------------------------------------------
  def computePlanet(vsopData: Array[Byte]
                    , planetNo: Int
                    , timeInCenturies: Double) = {
    var lat  = 0D
    var lon = 0D
    var r = 0D
    val obliquit = meanObliquity()
    val matrix = Array.fill(9)(0.0)
    val obliq2000 = 23.4392911 * PRECISE_ANGLE_TO_RAD

    if (planetNo != 10) {
     //no VSOP data
    }
    else {
      val fund = lunarFundamentals(vsopData, timeInCenturies)
      lat = lunarLat(vsopData, fund, 0L)
      val (_lon,_r) = lunarLonAndDist(vsopData, fund, 0L)
      lon = _lon * PRECISE_ANGLE_TO_RAD
      lat = lat * PRECISE_ANGLE_TO_RAD
      r = _r / ASTRONOMICAL_UNIT_KM
    }

    val ovals = Array(
        lon
      , lat
      ,  r
      , cos(lon) * cos(lat) * r
      , sin(lon) * cos(lat) * r
      , sin(lat) * r
    )

    Array.copy(ovals, 3, ovals, 6, 3)
    rotateVector(ovals.slice(6, 9), obliquit, 0)

    setupPrecession(matrix, 2000.0 + timeInCenturies * 100.0, 2000.0)
    precessVector(matrix, ovals.slice(6, 9), ovals.slice(9, 12))

    Array.copy(ovals, 9, ovals, 12, 3)
    rotateVector(ovals.slice(12, 15), -obliq2000, 0)
  }
  //---------------------------------------------------------------------------
  private def setupPrecession(matrix: Array[Double], yearFrom: Double, yearTo: Double): Unit = {
    // Placeholder for setting up precession matrix
    setIdentityMatrix(matrix)
  }
  //---------------------------------------------------------------------------
  private def precessVector(matrix: Array[Double], vec: Array[Double], outVec: Array[Double]): Unit = {
    for (i <- 0 until 3) {
      outVec(i) = 0.0
      for (j <- 0 until 3) {
        outVec(i) += matrix(i * 3 + j) * vec(j)
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Planet.scala
//=============================================================================