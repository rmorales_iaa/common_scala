/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  31/Jul/2024
 * Time:  11h:16m
 * Description: Adaptation of:
 * https://github.com/Bill-Gray/lunar/blob/master/astfuncs.cpp
 */
package com.common.orbitIntegrator.celestialBody
//=============================================================================
import com.common.constant.astronomy.AstronomicalUnit._
import com.common.orbitIntegrator.celestialBody.Util.vectorCrossProduct
//=============================================================================
import java.lang.Math._
import scala.collection.mutable.ArrayBuffer
//=============================================================================
object NonClassic {
  //---------------------------------------------------------------------------
  private final val THRESH = 1e-12
  //---------------------------------------------------------------------------
  private final val EPSILON = 1e-15
  //---------------------------------------------------------------------------
  private final val KEPLER_EQUATION_MAX_ITERATIONS = 7
  //---------------------------------------------------------------------------
  private def cubeRoot(x: Double): Double = math.exp(math.log(x) / 3.0)
  //---------------------------------------------------------------------------
  private def setupOrbitVectors(cb: CelestialBody): CelestialBody = {
    val sinIncl = math.sin(cb.incl)
    val cosIncl = math.cos(cb.incl)

    val minorToMajor = math.sqrt(math.abs(1.0 - cb.ecc * cb.ecc))
    val lonPer = cb.ascNode + math.atan2(math.sin(cb.argPer) * cosIncl, math.cos(cb.argPer))

    val perihVec = ArrayBuffer(
      math.cos(lonPer) * cosIncl
      , math.sin(lonPer) * cosIncl
      , sinIncl * math.sin(lonPer - cb.ascNode)
    )

    val vecLen = math.sqrt(cosIncl * cosIncl + perihVec(2) * perihVec(2))
    val adjustedVecLen = if (cosIncl < 0.0) -vecLen else vecLen

    for (i <- perihVec.indices)
      perihVec(i) /= adjustedVecLen

    val up = Array(
      math.sin(cb.ascNode) * sinIncl
      , -math.cos(cb.ascNode) * sinIncl
      , cosIncl
    )

    val sideways = vectorCrossProduct(up, perihVec.toArray)

    val ma = if (cb.meanAnomaly > PRECISE_PI) cb.meanAnomaly - 2 * PRECISE_PI
    else cb.meanAnomaly

    cb.copy(perihTime = cb.epoch - ma * cb.t0
      , minorToMajor = minorToMajor
      , lonPer = lonPer
      , perihVec = perihVec.toArray
      , sideways = sideways
      , angularMomentum = math.sqrt(SOLAR_GM * cb.q * (1d + cb.ecc))
    )
  }

  //---------------------------------------------------------------------------
  private def cometPosnPartII(cb: CelestialBody
                              , t: Double) = {
    var trueAnom = 0.0
    var r = 0.0
    var x = 0.0
    var y = 0.0
    var r0 = 0.0
    val ecc = cb.ecc

    if (ecc == 1.0) {
      val g = cb.w0 * t * 0.5
      y = cubeRoot(g + sqrt(g * g + 1.0))
      trueAnom = 2.0 * atan(y - 1.0 / y)
    }
    else {
      val eccAnom = keplerEquation(ecc, cb.meanAnomaly)
      if (ecc > 1.0) {
        x = ecc - cosh(eccAnom)
        y = sinh(eccAnom)
      }
      else {
        x = cos(eccAnom) - ecc
        y = sin(eccAnom)
      }
      y *= cb.minorToMajor
      trueAnom = atan2(y, x)
    }

    r0 = cb.q * (1.0 + cb.ecc)
    r = r0 / (1.0 + cb.ecc * cos(trueAnom))
    x = r * cos(trueAnom)
    y = r * sin(trueAnom)
    val location = Array(
        cb.perihVec(0) * x + cb.sideways(0) * y
      , cb.perihVec(1) * x + cb.sideways(1) * y
      , cb.perihVec(2) * x + cb.sideways(2) * y
      , r)

    val velocity =
      if (cb.angularMomentum == 0) Array[Double]()
      else {
        val angularComponent = cb.angularMomentum / (r * r)
        val radialComponent =  cb.ecc * sin(trueAnom) * cb.angularMomentum/ (r * r0)
        val x1 = x * radialComponent - y * angularComponent
        val y1 = y * radialComponent + x * angularComponent
        Array(cb.perihVec(0) * x1 + cb.sideways(0) * y1
              , cb.perihVec(1) * x1 + cb.sideways(1) * y1
              , cb.perihVec(2) * x1 + cb.sideways(2) * y1)
      }
    (location, velocity)
  }

  //---------------------------------------------------------------------------
  def cometPosnAndVel(cb: CelestialBody
                      , _t: Double) = {
    var t = _t - cb.perihTime
    if (cb.ecc != 1.0) { //not parabolic
      t /= cb.t0
      if(cb.ecc < 1d) { //elliptical case;  throw out extra orbits to fit mean anomaly between -PI and PI
        t = math.IEEEremainder( t, PRECISE_PI * 2d);
        if( t < -PRECISE_PI) t += 2d * PRECISE_PI;
        if( t >  PRECISE_PI) t -= 2d * PRECISE_PI;
      }
      cb.meanAnomaly = t
    }
    cometPosnPartII(cb, t)
  }
  //---------------------------------------------------------------------------
  def deriveQuantities(e: CelestialBody
                       , gm: Double = SOLAR_GM): CelestialBody = {
    val (majorAxis, t0, w0) =
      if (e.ecc != 1.0) {
        val majorAxis = e.q / math.abs(1.0 - e.ecc)
        val t0 = majorAxis * math.sqrt(majorAxis / gm)
        (majorAxis, t0, 0.0)
      } else {
        val w0 = (3.0 / math.sqrt(2.0)) / (e.q * math.sqrt(e.q / gm))
        (0.0, 0.0, w0)
      }

    val angularMomentum = math.sqrt(gm * e.q * (1.0 + e.ecc))

    val updatedBody = e.copy(
      majorAxis = majorAxis,
      t0 = t0,
      w0 = w0,
      angularMomentum = angularMomentum,
      gm = gm
    )
    setupOrbitVectors(updatedBody)
  }

  //---------------------------------------------------------------------------
  private def nearParabolic(eccAnom: Double, e: Double): Double = {
    val anom2 = if (e > 1.0) eccAnom * eccAnom else -eccAnom * eccAnom
    var term = e * anom2 * eccAnom / 6.0
    var rval = (1.0 - e) * eccAnom - term
    var n = 4
    while (abs(term) > EPSILON) {
      term *= anom2 / (n * (n + 1)).toDouble
      rval -= term
      n += 2
    }
    rval
  }

  //---------------------------------------------------------------------------
  private def keplerEquation(ecc: Double
                            , meanAnom: Double): Double = {
    var curr = meanAnom
    var err = 0.0
    var thresh = THRESH * math.abs(1.0 - ecc)
    var offset = 0.0
    var deltaCurr = 1.0
    var isNegative = false
    var nIter = 0

    if (meanAnom == 0.0) return 0.0

    if (ecc < 1.0) {
      if (meanAnom < -PRECISE_PI || meanAnom > PRECISE_PI) {
        var tmod = meanAnom % (PRECISE_PI * 2.0)
        if (tmod > PRECISE_PI) tmod -= 2.0 * PRECISE_PI
        else if (tmod < -PRECISE_PI) tmod += 2.0 * PRECISE_PI
        offset = meanAnom - tmod
        curr = tmod
      }

      if (ecc < 0.9) {
        curr = math.atan2(math.sin(meanAnom), math.cos(meanAnom) - ecc)
        do {
          err = (curr - ecc * math.sin(curr) - meanAnom) / (1.0 - ecc * math.cos(curr))
          curr -= err
        } while (math.abs(err) > THRESH)
        return curr + offset
      }
    }

    if (meanAnom < 0.0) {
      curr = -curr
      isNegative = true
    }

    if (ecc > 1.0 && meanAnom / ecc > 3.0) curr = math.log(meanAnom / ecc) + 0.85
    else if ((ecc > 0.8 && meanAnom < PRECISE_PI / 3.0) || ecc > 1.0) {
      var trial = meanAnom / math.abs(1.0 - ecc)
      if (trial * trial > 6.0 * math.abs(1.0 - ecc)) trial = cubeRoot(6.0 * meanAnom)
      curr = trial
      if (thresh > THRESH) thresh = THRESH
    }

    if (ecc < 1.0) {
      while (math.abs(deltaCurr) > thresh) {
        if (nIter > KEPLER_EQUATION_MAX_ITERATIONS) err = nearParabolic(curr, ecc) - meanAnom
        else err = curr - ecc * sin(curr) - meanAnom
        deltaCurr = -err / (1.0 - ecc * cos(curr))
        curr += deltaCurr
        assert(nIter < 20)
        nIter += 1
      }
    } else {
      while (math.abs(deltaCurr) > thresh) {
        if (nIter > KEPLER_EQUATION_MAX_ITERATIONS && ecc < 1.01) err = -nearParabolic(curr, ecc) - meanAnom
        else err = ecc * math.sinh(curr) - curr - meanAnom
        deltaCurr = -err / (ecc * math.cosh(curr) - 1.0)
        curr += deltaCurr
        assert(nIter < 20)
        nIter += 1
      }
    }
    if (isNegative) offset - curr else offset + curr
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file NonClassic.scala
//=============================================================================