/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  31/Jul/2024
 * Time:  12h:40m
 * Description: None
 */
package com.common.orbitIntegrator.celestialBody
//=============================================================================
import com.common.constant.astronomy.AstronomicalUnit.{PRECISE_ANGLE_TO_RAD, PRECISE_HALF_PI, PRECISE_PI}

import scala.collection.mutable.ArrayBuffer
//=============================================================================
import java.lang.Math._
//=============================================================================
//=============================================================================
object Util {
  //---------------------------------------------------------------------------
  def get16bits(data: Array[Byte], offset: Int): Int =
    ((data(offset) & 0xFF) << 8) | (data(offset + 1) & 0xFF)
  //---------------------------------------------------------------------------
  def get32bits(data: Array[Byte], offset: Int): Int =
    ((data(offset) & 0xFF) << 24) | ((data(offset + 1) & 0xFF) << 16) |
      ((data(offset + 2) & 0xFF) << 8) | (data(offset + 3) & 0xFF)
  //---------------------------------------------------------------------------
  def getDouble(data: Array[Byte], offset: Int): Double =
    java.lang.Double.longBitsToDouble(get64bits(data, offset))

  //---------------------------------------------------------------------------
  def get64bits(data: Array[Byte], offset: Int): Long =
    ((data(offset) & 0xFF).toLong << 56) | ((data(offset + 1) & 0xFF).toLong << 48) |
      ((data(offset + 2) & 0xFF).toLong << 40) | ((data(offset + 3) & 0xFF).toLong << 32) |
      ((data(offset + 4) & 0xFF).toLong << 24) | ((data(offset + 5) & 0xFF).toLong << 16) |
      ((data(offset + 6) & 0xFF).toLong << 8) | (data(offset + 7) & 0xFF).toLong

  //---------------------------------------------------------------------------
  def asinh(x: Double) = log(x + sqrt(x * x + 1))
  //---------------------------------------------------------------------------
  def rotateVector(angle: Double, x: Array[Double], y: Array[Double]): Unit = {
    val sinAngle = math.sin(angle)
    val cosAngle = math.cos(angle)
    val temp = cosAngle * x(0) - sinAngle * y(0)

    y(0) = sinAngle * x(0) + cosAngle * y(0)
    x(0) = temp
  }
  //---------------------------------------------------------------------------
  def rotateVector(vec: Array[Double]
                   , angle: Double
                   , axis: Int): Array[Double] = {
    val c = cos(angle)
    val s = sin(angle)
    val result = ArrayBuffer[Double]() ++ vec
    axis match {
      case 0 =>
        val y = vec(1)
        result(1) = y * c - vec(2) * s
        result(2) = y * s + vec(2) * c
      case 1 =>
        val x = vec(0)
        result(0) = x * c + vec(2) * s
        result(2) = -x * s + vec(2) * c
      case 2 =>
        val x = vec(0)
        result(0) = x * c - vec(1) * s
        result(1) = x * s + vec(1) * c
      case _ =>
        throw new IllegalArgumentException("Invalid axis")
    }
    result.toArray
  }
  //---------------------------------------------------------------------------
  def phaseAngleCorrectionToMagnitude(phaseAngle: Double
                                     , slopeParam: Double): Double = {
    val epsilon = 1e-10
    val logTanHalfPhase = log(tan(phaseAngle / 2.0) + epsilon)
    val phi1 = exp(-3.33 * exp(logTanHalfPhase * 0.63))
    val phi2 = exp(-1.87 * exp(logTanHalfPhase * 1.22))

    -2.5 * log10((1.0 - slopeParam) * phi1 + slopeParam * phi2)
  }
  //---------------------------------------------------------------------------
  def acose(v: Double): Double = {
    if (v >= 1d)return 0d
    if (v <= -1d) return PRECISE_PI
    acos(v)
  }
  //---------------------------------------------------------------------------
  def asine(v: Double): Double = {
    if (v >= 1d) return PRECISE_HALF_PI
    if (v <= -1d) return -PRECISE_HALF_PI
    asin(v)
  }
  //---------------------------------------------------------------------------
  def vectorCrossProduct(a: Array[Double], b: Array[Double]): Array[Double] =
    Array(
      a(1) * b(2) - a(2) * b(1),
      a(2) * b(0) - a(0) * b(2),
      a(0) * b(1) - a(1) * b(0)
    )
  //---------------------------------------------------------------------------
   def meanObliquity(): Double = 23.43929111111111 * PRECISE_ANGLE_TO_RAD
  //---------------------------------------------------------------------------
  def setIdentityMatrix(matrix: Array[Double]): Unit = {
    matrix(0) = 1.0
    matrix(1) = 0.0
    matrix(2) = 0.0
    matrix(3) = 0.0
    matrix(4) = 1.0
    matrix(5) = 0.0
    matrix(6) = 0.0
    matrix(7) = 0.0
    matrix(8) = 1.0
  }
  //---------------------------------------------------------------------------
  def swap64BitVal(ptr: Array[Byte], count: Long): Unit = {
    var idx = 0
    while (idx < count) {
      var tchar: Byte = 0
      swapMacro(ptr, idx, 0, 7, tchar)
      swapMacro(ptr, idx, 1, 6, tchar)
      swapMacro(ptr, idx, 2, 5, tchar)
      swapMacro(ptr, idx, 3, 4, tchar)
      idx += 1
    }
  }

  private def swapMacro(ptr: Array[Byte], idx: Int, a: Int, b: Int, tchar: Byte): Unit = {
    val temp = ptr(idx * 8 + a)
    ptr(idx * 8 + a) = ptr(idx * 8 + b)
    ptr(idx * 8 + b) = temp
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Util.scala
//=============================================================================