/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  27/Jul/2024
 * Time:  20h:33m
 * Description: sof (Standard Orbit Format) orbit
 * converter
 * https://github.com/Bill-Gray/lunar/blob/master/mpc2sof.cpp
 */
package com.common.orbitIntegrator
//=============================================================================
import celestialBody.CelestialBody
import com.common.database.mongoDB.mpc.MPO
import com.common.util.time.Time
import com.common.util.time.Time.getFractionDay
//=============================================================================
import java.time.format.DateTimeFormatter
import java.time.{Duration, LocalDate}
//=============================================================================
//=============================================================================
object SOF {
   //--------------------------------------------------------------------------
   private final val dateFormatterYYMMDD = DateTimeFormatter.ofPattern("yyyyMMdd")
   //--------------------------------------------------------------------------
   private def getFormmatedYear(jd: Double) ={
     val t = Time.fromJulian(jd)
     t.format(dateFormatterYYMMDD) +
     getFractionDay(t).toString.drop(1)
   }
   //--------------------------------------------------------------------------
   private def getFitsObs(yearRange: Option[String]
                          , lastObs: LocalDate): String = {
     yearRange match {
       case Some(range) if range.trim.nonEmpty =>
         val year = range.split("-").head
         val startObs = LocalDate.parse(year + "0101", dateFormatterYYMMDD)
         val jdLastObs = Time.toJulian(lastObs)
         val elapsedDays = Duration.between(startObs.atStartOfDay(), lastObs.atStartOfDay()).toDays
         getFormmatedYear(jdLastObs - elapsedDays)
       case _ => ""
     }
   }
   //--------------------------------------------------------------------------
   def apply(mpo: MPO
            , cb: CelestialBody): SOF = {

      val coarsePerturber = mpo.Perturbers.getOrElse("")
      val precisePerturber = mpo.Perturbers_2.getOrElse("")
      val pertuber = (if (coarsePerturber.isEmpty) "" else coarsePerturber + " ") + precisePerturber
      val lastObs = LocalDate.parse(mpo.Last_obs)

      SOF(name = mpo.Number.getOrElse(mpo.Name.get)
         , tp = getFormmatedYear(cb.perihTime)
         , te = getFormmatedYear(cb.epoch)
         , q = cb.q
         , i = mpo.i
         , Om = mpo.Node
         , om = mpo.Peri
         , e = mpo.e
         , rms = if (mpo.rms.isDefined) mpo.rms.get.toDouble else Double.NaN
         , no = mpo.Num_obs.getOrElse(0)
         , tFirst = getFitsObs(mpo.Arc_years, lastObs)
         , tLast = lastObs.format(dateFormatterYYMMDD)
         , perts = pertuber
         , h = if (mpo.H.isDefined) mpo.H.get else Double.NaN
         , g = if (mpo.G.isDefined) mpo.G.get else Double.NaN
        , cb = cb
      )
   }
   //--------------------------------------------------------------------------
}
//=============================================================================
case class SOF(name: String //number or name
               , tp: String //perihelion time (fractional days)
               , te: String //Epoch of the orbit (fractional days)
               , q: Double
               , i: Double //Inclination to the ecliptic, J2000.0 (degrees)
               , Om: Double //Longitude of the ascending node, J2000.0 (degrees)
               , om: Double //Argument of perihelion, J2000.0 (degrees)
               , e: Double //Orbital eccentricity
               , rms: Double //r.m.s. residual
               , no: Int //Number of observations
               , tFirst: String //Date of first observation included in orbit solution
               , tLast: String //Date of last observation included in orbit solution
               , perts: String //Coarse indicator of perturbers used in orbit computation
               , h: Double //Absolute magnitude
               , g: Double //Slope parameter
               , cb: CelestialBody
               ) {
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SOF.scala
//=============================================================================