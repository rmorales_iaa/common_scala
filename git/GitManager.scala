/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  04/Nov/2019
 * Time:  13h:46m
 * Description: Git manager. Adapted from:
 * https://geekytheory.com/libreria-jgit-como-utilizar-git-desde-java
 * https://github.com/GeekyTheory/JGit-Example/tree/master/src/gitcontrol
 */
//=============================================================================
package com.common.git
//=============================================================================
import com.common.logger.MyLogger
import com.common.util.path.Path
//=============================================================================
import org.eclipse.jgit.api.{Git, Status}
import org.eclipse.jgit.lib.{Ref, Repository}
import org.eclipse.jgit.revwalk.{RevCommit, RevWalk}
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import org.eclipse.jgit.transport.PushResult
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider
import scala.util.{Failure, Success, Try}
import collection.JavaConverters._
//=============================================================================
import java.io.File
import java.util.Iterator
//=============================================================================
//=============================================================================
object GitManager extends  MyLogger {
  //---------------------------------------------------------------------------
  final val GIT_EXTENSION = ".git"
  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/16665387/replace-last-occurrence-of-a-character-in-a-string/16665524
  def replaceLast(s: String, subS: String, replace: String=""): String = {
    val index = s.lastIndexOf(subS)
    if (index == -1)  s
    else s.substring(0, index) + replace + s.substring(index + subS.length)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import GitManager._
case class GitManager(localPath: String
                      , remoteRepoURL: String
                      , username: String=""
                      , token: String = "") extends MyLogger {
  //---------------------------------------------------------------------------
  private val localRepo = openLocalRepo
  private val git = if (localRepo == null) null else new Git(localRepo)
  private val credential = new UsernamePasswordCredentialsProvider(username, token)
  //---------------------------------------------------------------------------
  private def exitsLocalRepo() = Path.directoryExist(localPath)
  //-------------------------------------------------------------------------
  private def openLocalRepo() : Repository = {
    var repo: Repository  = null
    Try {
      repo = new FileRepositoryBuilder()
        .setGitDir(new File(localPath,GIT_EXTENSION))
        .readEnvironment
        .findGitDir
        .build
    }
    match
    {
      case Success(_) => info(s"Local repository: '$localPath' successfully open")
      case Failure( _ ) => error(s"Error opening local repository: '$localPath'")
    }
    repo
  }
  //-------------------------------------------------------------------------
  //https://github.com/centic9/jgit-cookbook/blob/master/src/main/java/org/dstadler/jgit/api/GetCommitMessage.java
  def getHeadCommit(ref: Ref) : RevCommit = {
    val walk = new RevWalk(localRepo)
    val commit = walk.parseCommit(ref.getObjectId)
    walk.close
    commit
  }
  //-------------------------------------------------------------------------
  def getCommitMessage(ref: Ref) : String = {
    if (ref == null) "No commit message"
    else getHeadCommit(ref).getFullMessage.trim
  }
  //---------------------------------------------------------------------------
  def cloneRepo(): Unit = {

    info(s"Cloning remote repository :'$remoteRepoURL'")
    val command =  Git.cloneRepository
    command.setURI(remoteRepoURL)
    command.setDirectory(new File(localPath))
    command.setProgressMonitor(GitProgressMonitor("cloneRepo"))
    Try { command.call }
    match
    {
      case Success(_) => info(s"Cloned remote repository '$remoteRepoURL' successfully")
      case Failure(e) => error("Method: 'clone' " + e.getMessage)
    }
  }
  //---------------------------------------------------------------------------
  def add(): Unit = {

    info(s"Adding to remote repository :'$remoteRepoURL'")
    val command = git.add
    command.addFilepattern(".").call
    Try { command.call }
    match
    {
      case Success(_) => info(s"Added to remote repository '$remoteRepoURL' successfully")
      case Failure(e) => error("Method: 'append' " + e.getMessage)
    }
  }
  //---------------------------------------------------------------------------
  def commit(message: String): Unit = {

    info(s"Committing to remote repository :'$remoteRepoURL'")
    val command = git.commit
    command.setMessage(message)
    Try { command.call }
    match
    {
      case Success(_) => info(s"Commit of remote repository '$remoteRepoURL' successfully")
      case Failure(e) => error("Method: 'commit' " + e.getMessage)
    }
  }
  //---------------------------------------------------------------------------
  def push(): Boolean = {

    info(s"Pushing to remote repository :'$remoteRepoURL'")
    val command = git.push
    command.setCredentialsProvider(credential).setForce(true).setPushAll
    command.setProgressMonitor(GitProgressMonitor("push"))
    Try {
      val it: Iterator[PushResult] = command.call.iterator
      if (it.hasNext) info(it.next.toString)
    }
    match
    {
      case Success(_) => info(s"Pushing to remote repository '$remoteRepoURL' successfully")
      case Failure(e) => error("Method: 'push' " + e.getMessage)
    }
  }
  //---------------------------------------------------------------------------
  def pull(): Boolean = {

    info(s"Pulling from remote repository :'$remoteRepoURL'")
    val command = git.pull
    command.setCredentialsProvider(credential)
    command.setProgressMonitor(GitProgressMonitor("pull"))
    Try { command.call }
    match
    {
      case Success(_) => info(s"Pulling from remote repository '$remoteRepoURL' successfully")
      case Failure(e) => error("Method: 'pull' " + e.getMessage)
    }
  }
  //---------------------------------------------------------------------------
  def status(): Boolean = {

    info(s"Getting status from remote repository :'$remoteRepoURL'")
    val command = git.status
    command.setProgressMonitor(GitProgressMonitor("status"))
    var status: Status = null
    Try { status = command.call }
    match
    {
      case Success(_) => info(s"Status of remote repository '$remoteRepoURL' successfully obtained")
      case Failure(e) => return error("Method: 'status' " + e.getMessage)
    }

    var someChange = false
    status.getAdded.asScala.foreach { s=> someChange = true; info(s"Added file: '$s'")}
    status.getChanged.asScala.foreach { s=> someChange = true; info(s"Changed file: '$s'")}
    status.getRemoved.asScala.foreach { s=> someChange = true; info(s"Removed file: '$s'")}
    status.getConflicting.asScala.foreach { s=> someChange = true; info(s"Conflicting file: '$s'")}
    status.getMissing.asScala.foreach { s=> someChange = true; info(s"Missing file: '$s'")}
    status.getUntracked.asScala.foreach { s=> someChange = true; info(s"Untracked file: '$s'")}
    status.getUntrackedFolders.asScala.foreach { s=> someChange = true; info(s"Untracked directory: '$s'")}
    if (!someChange) info(s"No changes detected in the git server: '$remoteRepoURL'")
    someChange
  }
  //-------------------------------------------------------------------------
  def remoteList : scala.collection.mutable.Map[String, Ref]  = {

    info(s"Listing the remote repository :'$remoteRepoURL'")
    val command = Git.lsRemoteRepository
    command.setHeads(true)
    command.setTags(true)
    command.setRemote(remoteRepoURL)
    var map : scala.collection.mutable.Map[String, Ref]  = null
    Try { map = command.callAsMap.asScala }
    match
    {
      case Success(_) => info(s"List of remote repository '$remoteRepoURL' successfully obtained")
      case Failure(e) => error("Method: 'lsRemoteRepository' " + e.getMessage)
    }
    map
  }
  //-------------------------------------------------------------------------
  def getRemoteHeadRef : Ref  = {

    info(s"Listing remote repository :'$remoteRepoURL'")
    val command = Git.lsRemoteRepository
    command.setHeads(true)
    command.setRemote(remoteRepoURL)
    var headRef : Ref  = null
    Try { headRef = command.call.asScala.head }
    match
    {
      case Success(_) => info(s"List of remote repository '$remoteRepoURL' successfully obtained")
      case Failure(e) => error("Method: 'lsRemoteRepository' " + e.getMessage)
    }
    headRef
  }
  //---------------------------------------------------------------------------
  def close() = {

    info(s"Closing remote repository: '$remoteRepoURL'")
    git.close
  }
  //---------------------------------------------------------------------------
  private def getLocalHeadRef =
    if (localRepo == null) null else localRepo.exactRef("refs/heads/main")
  //---------------------------------------------------------------------------
  private def getLocalHeadMessage(localHeadRef: Ref) =
    if (localRepo == null) null else getCommitMessage(localHeadRef)
  //---------------------------------------------------------------------------
  def isLocalRepoUpdated (localHeadRef: Ref, remoteHeadRef: Ref) : Boolean = {
    if ((localHeadRef == null) || (remoteHeadRef==null)) false
    else localHeadRef.getObjectId == remoteHeadRef.getObjectId
  }
  //---------------------------------------------------------------------------
  if (!exitsLocalRepo) cloneRepo
  if (localRepo != null) {
    val localHeadRef = getLocalHeadRef
    val localHeadMessage = getLocalHeadMessage(localHeadRef)
    val remoteHeadRef = getRemoteHeadRef

    info("Local git repository head reference: 0x" + localHeadRef.getObjectId.hashCode.toHexString)
    info("Local git repository head message  : " + localHeadMessage)
    if (remoteHeadRef != null)
      info("Remote repo git head reference     : 0x"+remoteHeadRef.getObjectId.hashCode.toHexString)

    if (isLocalRepoUpdated(localHeadRef,remoteHeadRef)) warning("Running last software version, no additional actions required")
    else {
      if (pull) warning("Software update calculate finished")
      else error("Error updating to last version")
    }
    close
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file GitManager.scala
//=============================================================================
