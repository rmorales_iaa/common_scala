
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  04/Nov/2019
 * Time:  17h:45m
 * Description: None
 */
//=============================================================================
package com.common.git
//=============================================================================
import org.eclipse.jgit.lib.ProgressMonitor
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
case class GitProgressMonitor(name: String) extends ProgressMonitor with MyLogger {
  //---------------------------------------------------------------------------
  private var title_  = "None"
  private var totalWork_  = 0
  private var _isCancelled  = false
  //---------------------------------------------------------------------------
  override def beginTask(title: String, totalWork: Int): Unit = {
    title_ = title
    totalWork_ = totalWork
    info(s"GitProgressMonitor: '$name' has started the task name: $title of total work: $totalWork")
  }
  //---------------------------------------------------------------------------
  override def endTask(): Unit =
    info(s"GitProgressMonitor: '$name' has finalized the task name: $title_ of total work: $totalWork_")
  //---------------------------------------------------------------------------
  override def isCancelled(): Boolean = {
    if (_isCancelled)
      info(s"GitProgressMonitor: '$name' is cancelled. Task name: $title_ of total work: $totalWork_")
    _isCancelled
  }
  //---------------------------------------------------------------------------
  def cancel() = _isCancelled = true
  //---------------------------------------------------------------------------
  override def start(totalTask : Int): Unit =
    info(s"GitProgressMonitor: '$name' has started with: $totalTask work unit")
  //---------------------------------------------------------------------------
  override def update(completed : Int): Unit =
    info(s"GitProgressMonitor: '$name'. Number of work unit completed since the last call: $completed")
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file GitProgressMonitor.scala
//=============================================================================
