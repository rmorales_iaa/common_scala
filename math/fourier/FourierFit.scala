/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  25/Apr/2022
 * Time:  11h:36m
 * Description: fit a fourier series sine-cosine form  (https://en.wikipedia.org/wiki/Fourier_series)
 */
//=============================================================================
package com.common.math.fourier
//=============================================================================
import org.apache.commons.math3.analysis.{MultivariateMatrixFunction, MultivariateVectorFunction}
import org.apache.commons.math3.fitting.leastsquares.{LeastSquaresBuilder, LevenbergMarquardtOptimizer}
import org.apache.commons.math3.util.FastMath._
//=============================================================================
//=============================================================================
case class FourierFit(degree: Int
                      , xSeq: Array[Double]
                      , ySeq: Array[Double]
                      , initialParameterGuess: Array[Double] = Array[Double]() //Array(amplitude, a_i,b_i)
                      , maxEvaluations: Int = 1000
                      , maxIterations: Int = 100) {
  //---------------------------------------------------------------------------
  private val lsb = new LeastSquaresBuilder
  //-------------------------------------------------------------------------
  //model function
  class FunctionModel extends MultivariateVectorFunction {
    @throws[IllegalArgumentException]
    def value(paremeterSeq: Array[Double]) =
      for(x<-xSeq) yield evaluateFunction(x, paremeterSeq)
  }
  //-------------------------------------------------------------------------
  //model function jacobian
  class JacobianOfFunctionModel extends MultivariateMatrixFunction {
    @throws[IllegalArgumentException]
    def value(paremeterSeq: Array[Double]) =
      for(x<-xSeq) yield evaluateJacobian(x, paremeterSeq)
  }
  //---------------------------------------------------------------------------
  //evaluates the circular gaussian
  def evaluateFunction(t: Double, parameterSeq: Array[Double]) = {
    val amplitude = parameterSeq.head
    val aSeq = parameterSeq.drop(1).sliding(1,2).toArray.flatten
    val bSeq = parameterSeq.drop(2).sliding(1,2).toArray.flatten

    //evaluate
    Fourier(degree,amplitude,aSeq,bSeq).eval(t)
  }
  //---------------------------------------------------------------------------
  //evaluates the jacobian of a circular gaussian
  def evaluateJacobian(t: Double, parameterSeq : Array[Double]): Array[Double] = {
    //-------------------------------------------------------------------------
    val cosSinSeq = (for(i<-1 to degree) yield {
      val x = 2 * PI * i * t
      Seq(cos(x),sin(x))
    }).flatten.toArray

    //process the array of partial derivates
    Array(0.5d) ++ cosSinSeq
  }
  //-------------------------------------------------------------------------
  def getInitialParameterSeq() =
    if (!initialParameterGuess.isEmpty) initialParameterGuess
    else Array.fill[Double](degree * 2 + 1)(0d)
  //-------------------------------------------------------------------------
  def getTargetSeq() = ySeq
  //---------------------------------------------------------------------------
  def fit():Option[Fourier] = {
    //-------------------------------------------------------------------------
    try {
      //set model distribution.function and its jacobian
      lsb.model(new FunctionModel, new JacobianOfFunctionModel)
        .target(getTargetSeq())
        .start(getInitialParameterSeq)
        .maxEvaluations(maxEvaluations) //set upper limit of evaluation time
        .maxIterations(maxIterations)   //set upper limit of iteration time

      val optimizer = new LevenbergMarquardtOptimizer().optimize(lsb.build)
      val parameterSeq = optimizer.getPoint.toArray
      val amplitude = parameterSeq.head
      val aSeq = parameterSeq.drop(1).sliding(1,2).toArray.flatten
      val bSeq = parameterSeq.drop(2).sliding(1,2).toArray.flatten
      Some(Fourier(degree,amplitude,aSeq,bSeq))
    }
    catch {
      case _: Exception =>  None
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FourierFit.scala
//=============================================================================