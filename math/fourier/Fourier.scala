/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/May/2022
 * Time:  01h:26m
 * Description: https://en.wikipedia.org/wiki/Fourier_series
 * Sine-cosine form
 */
//=============================================================================
package com.common.math.fourier
//=============================================================================
import org.apache.commons.math3.util.FastMath._
//=============================================================================
//=============================================================================
object Fourier {
  //---------------------------------------------------------------------------
  def apply() : Fourier = Fourier(0,0d,Array[Double](0),Array[Double]())
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Fourier(degree: Int
                   , amplitude: Double
                   , aSeq: Array[Double]
                   , bSeq: Array[Double]) {
  //---------------------------------------------------------------------------
  def eval(t: Double) = {
    var r = 0d
    val vSeq =  for(i<-1 to degree) yield 2 * PI * t * i
    for(i<-0 until degree)
      r += (aSeq(i) * cos(vSeq(i))) +
           (bSeq(i) * sin(vSeq(i)))
    (amplitude / 2) + r
  }
  //---------------------------------------------------------------------------
  override def toString: String = {
    val s = (aSeq zip bSeq).zipWithIndex.map { case (p,i) =>
      s"(a${i+1},b${i+1})=(" + f"${p._1}%.3f" + "," + f"${p._2}%.3f" + ") "
    }.mkString
    val a = f"$amplitude%.3f"
    (s"n=$degree A=$a " + s).trim
  }

  //---------------------------------------------------------------------------
  def coeffToSimpleString(): Seq[String] = {
    val r = Seq(degree.toString, amplitude.toString) ++
      (aSeq zip bSeq).map { p => Seq(p._1.toString, p._2.toString) }.flatten.toSeq

    val expectedTermCount = (degree * 2) + 1
    val remainTerm = expectedTermCount - r.length
    r ++ (for (i<-0 until remainTerm) yield "0") //fill with zeros. This avoid solutions with no all terms
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Fourier.scala
//=============================================================================