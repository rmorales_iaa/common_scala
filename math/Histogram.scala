/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  27/may/2019
  * Time:  02h:46m
  * Description: None
  */
//=============================================================================
package com.common.math

//=============================================================================
import scala.collection.immutable
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//https://stackoverflow.com/questions/24536215/scala-simple-histogram
object Histogram{
  //---------------------------------------------------------------------------
  type HistogramType = Int
  //---------------------------------------------------------------------------
  def classic(xs: Seq[HistogramType]): (HistogramType, ArrayBuffer[Long]) = {

    val xsSorted = xs.sorted
    val min = xsSorted.head
    val histogram = ArrayBuffer.fill[Long](xsSorted.last - xsSorted.head + 1)(0)
    xs foreach( p =>  histogram( p - min ) += 1)
    (min,histogram)
  }
  //---------------------------------------------------------------------------
  def accumulatedInBin(n_bins: Int, userLowerUpperBound: Option[(HistogramType, HistogramType)] = None)(xs: Seq[HistogramType]): immutable.IndexedSeq[((Long, Long), HistogramType)] = {
    //-------------------------------------------------------------------------
    def binContains(bin:(Long,Long),x: Long) = (x >= bin._1) && (x < bin._2)
    //-------------------------------------------------------------------------
    val (mn, mx) = userLowerUpperBound.getOrElse ((xs.min, xs.max))
    val epsilon = 0.0001
    val binSize = Math.round((mx - mn) / n_bins * (1 + epsilon))
    val bins = (0 to n_bins).map(mn + _ * binSize).sliding(2).map(xs => (xs(0), xs(1)))
    bins.map(bin => (bin, xs.count(binContains(bin,_)))).toIndexedSeq
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Histogram.scala
//=============================================================================
