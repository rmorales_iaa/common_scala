/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/Jul/2021
 * Time:  11h:21m
 * Description: https://exercism.io/tracks/scala/exercises/complex-numbers/solutions/b3631c42b935481fb89b655b406707c8
 */
//=============================================================================
package com.common.math.complexNumber
//=============================================================================
import java.lang.Math._
//=============================================================================
object ComplexNumber {
  //---------------------------------------------------------------------------
  def exp(c: ComplexNumber)  = {
    val r = cosh(c.real) + sinh(c.real)
    ComplexNumber(cos(c.imaginary), sin(c.imaginary)) * r
  }
  //---------------------------------------------------------------------------
  //https://rosettacode.org/wiki/Fast_Fourier_transform#Scala
  //it equivalent to "FastFourierTransformer" from "org.apache.commons.math3.transform"
  private def _fft(cSeq: Seq[ComplexNumber]
                   , direction: ComplexNumber
                   , scalar: Int): Seq[ComplexNumber] = {
    if (cSeq.length == 1) {
      return cSeq
    }
    val n = cSeq.length
    assume(n % 2 == 0, "The Cooley-Tukey FFT algorithm only works when the length of the input is even.")

    val evenOddPairs = cSeq.grouped(2).toSeq
    val evens = _fft(evenOddPairs map (_(0)), direction, scalar)
    val odds  = _fft(evenOddPairs map (_(1)), direction, scalar)

    //--------------------------------------------------------------------------
    def leftRightPair(k: Int): (ComplexNumber, ComplexNumber) = {
      val base = evens(k) / scalar
      val offset = ComplexNumber.exp(direction * (PI * k / n)) * odds(k) / scalar
      (base + offset, base - offset)
    }
    //--------------------------------------------------------------------------
    val pairs = (0 until n/2) map leftRightPair
    val left  = pairs map (_._1)
    val right = pairs map (_._2)
    left ++ right
  }
  //---------------------------------------------------------------------------
  def fft(cSeq: Seq[ComplexNumber]) = _fft(cSeq, ComplexNumber(0,  2), 1)
  //---------------------------------------------------------------------------
  def ifft(cSeq: Seq[ComplexNumber]) = _fft(cSeq, ComplexNumber(0, -2), 2)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class ComplexNumber(real: Double = 0, imaginary: Double = 0) {
  //---------------------------------------------------------------------------
  lazy val conjugate: ComplexNumber = this.copy(imaginary = -imaginary)
  lazy val abs: Double = sqrt(real * real + imaginary * imaginary) //module
  //---------------------------------------------------------------------------
  def + (other: ComplexNumber) = ComplexNumber(real + other.real, imaginary + other.imaginary)
  //---------------------------------------------------------------------------
  def - (other: ComplexNumber) = ComplexNumber(real - other.real, imaginary - other.imaginary)
  //---------------------------------------------------------------------------
  def + (scalar: Double) = ComplexNumber(real + scalar, imaginary)
  //---------------------------------------------------------------------------
  def - (scalar: Double) = ComplexNumber(real - scalar, imaginary)
  //---------------------------------------------------------------------------
  def * (other: ComplexNumber) = ComplexNumber(real * other.real - imaginary * other.imaginary
    , imaginary * other.real + real * other.imaginary)
  //---------------------------------------------------------------------------
  def * (scalar: Double) = ComplexNumber(real * scalar, imaginary * scalar)
  //---------------------------------------------------------------------------
  def / (scalar: Double) = ComplexNumber(real / scalar, imaginary / scalar)
  //---------------------------------------------------------------------------
  def / (other: ComplexNumber): ComplexNumber = {
    val denominator = other.real * other.real + other.imaginary * other.imaginary
    val divReal = (real * other.real + imaginary * other.imaginary) / denominator
    val divImaginary = (imaginary * other.real - real * other.imaginary) / denominator
    ComplexNumber(divReal, divImaginary)
  }
  //---------------------------------------------------------------------------
  def exp() = {
    val ex = Math.exp(real)
    ComplexNumber(ex * cos(imaginary), ex * sin(imaginary))
  }
  //---------------------------------------------------------------------------
  //https://rosettacode.org/wiki/Fast_Fourier_transform#Scala
  override def toString: String = {
    val a = real.toString
    val b = Math.abs(imaginary).toString
    (a,b) match {
      case (_, "0.000") => a
      case ("0.000", _) => b + "j"
      case (_, _) if imaginary > 0 => a + " + " + b + "j"
      case (_, _) => a + " - " + b + "j"
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ComplexNumber.scala
//=============================================================================
