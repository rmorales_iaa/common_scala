/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  07/Nov/2024
 * Time:  10h:28m
 * Description:https://en.wikipedia.org/wiki/Multivariate_normal_distribution#Bivariate_case
 */
package com.common.math.distribution
//=============================================================================
import com.common.constant.astronomy.AstronomicalUnit.PRECISE_PI
import org.apache.commons.math3.distribution.PoissonDistribution
//=============================================================================
import scala.util.Random
//=============================================================================
//=============================================================================
case class BivariateNormal(x1_centre: Double //also used the median of x1. In general value around which the distribution is centered.
                           , x2_centre: Double //also used the median of x2. In general value around which the distribution is centered.
                           , x1_sigma: Double //standard deviation of x1
                           , x2_sigma: Double //standard deviation of x2
                           , rho: Double) {    //correlation coefficient between X and Y,
  //---------------------------------------------------------------------------
  private val x1_sigmaSquared = x1_sigma * x1_sigma
  private val x2_sigmaSquared = x2_sigma * x2_sigma
  private val rhoCalc = 1 - (rho * rho)
  private val normalizationValue = 1 / (2 * PRECISE_PI * x1_sigma * x2_sigma * Math.sqrt(rhoCalc))
  private val exponentMultiplier = -1 / (2 * rhoCalc)
  private val sigmaMult = x1_sigma * x2_sigma
  //---------------------------------------------------------------------------
  def eval(x: Double, y: Double) = {
    val centredX = x - x1_centre
    val centredY = y - x2_centre

    val exponentValue = exponentMultiplier *
                        (((centredX * centredX)  / x1_sigmaSquared) +
                        ((centredY * centredY)  / x2_sigmaSquared) -
                        ((2 * rho)  * ((centredX * centredY) / sigmaMult)))
    normalizationValue * Math.exp(exponentValue)
  }
  //---------------------------------------------------------------------------
  def evalWithGaussianNoisy(x: Double, y: Double, noiseLevel: Double) =
     eval(x, y) * (1 + noiseLevel * Random.nextGaussian())
  //---------------------------------------------------------------------------
  def evalWithPoissonNoisy(x: Double, y: Double, poissonMean: Double) = {
    val poissonGenerator = new PoissonDistribution(poissonMean)
    eval(x, y) * (1 + poissonGenerator.sample())
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Gaussian2D_Density_WithCorrelations.scala
//=============================================================================