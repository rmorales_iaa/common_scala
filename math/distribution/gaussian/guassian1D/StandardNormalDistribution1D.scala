/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

//=============================================================================
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  01/Nov/2020
 * Time:  00h:38m
 * Description: https://en.wikipedia.org/wiki/Normal_distribution
 * It is just a case of a normal distribution where variance = 1 and average = 0
 * Please note that: 'variance' = 1 implies that 'std '= 1
 * class StandardNormalDistribution1D() extends Gaussian1D(0d,1d)
 */
//=============================================================================
package com.common.math.distribution.gaussian.guassian1D
//=============================================================================
//=============================================================================
case class StandardNormalDistribution1D() {
  //---------------------------------------------------------------------------
  val average  = 0d
  val variance = 1d
  val std_dev = 1d
  //---------------------------------------------------------------------------
  private val a = 1d / Math.sqrt(2 * Math.PI)
  //---------------------------------------------------------------------------
  def eval(x: Double) = a * Math.exp (-0.5 * (x * x) )
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file NormalDistribution.scala
//=============================================================================

