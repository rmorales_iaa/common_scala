/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Jul/2020
 * Time:  18h:35m
 * Description: None
 */
//=============================================================================
package com.common.math.distribution.gaussian.guassian1D
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.math.MyMath
import com.common.math.regression.Regression.getGoodnessOfFit
import org.apache.commons.math3.fitting.{GaussianCurveFitter, WeightedObservedPoint}
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
//https://en.wikipedia.org/wiki/Gaussian_function
//Definition with Maxima: (http://maxima.sourceforge.net/):
//f(x,a,b,c):=a*exp((-1)/2*((x-b)/c)^2)
class Gaussian1D_Function(
    a: Double       //'height'
  , b: Double     //'centre',
  , c: Double) {  //'width'
  //---------------------------------------------------------------------------
  require(a > 0,s"Error creating GaussianFunction. Height(parameter 'a') must be > 0. Actual value: $a ")
  require(c > 0,s"Error creating GaussianFunction. Width(paramter 'c') must be > 0. Actual value: $c ")
  //---------------------------------------------------------------------------
  def getHeight () = a
  //---------------------------------------------------------------------------
  def getCentre () = b
  //---------------------------------------------------------------------------
  def getWidth () = c
  //---------------------------------------------------------------------------
  def eval(x: Double) = {
    val xc = (x - b) / c
    a * Math.exp(-0.5 * (xc * xc))
  }
  //---------------------------------------------------------------------------
  //integrate(f(x,a,b,c), x)
  //(sqrt(%pi)*a*c*erf(x/(sqrt(2)*c)-b/(sqrt(2)*c)))/sqrt(2)
  def integratePoint(x: Double) =
    (MyMath.sqrtPi * a * c * MyMath.erf((x - b) / Math.sqrt(2 * c))) / MyMath.sqrt2
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Simpson%27s_rule
  def integrateRangeNumeric(min:Double, max: Double) =
    ((max - min) / 6) * (eval(min) + (4 * eval((min+max)/2)) + eval(b))
  //---------------------------------------------------------------------------
  //integrate(f(x,a,b,c),x,m,M):= a*(-(sqrt(%pi)*c*erf((sqrt(2)*m-sqrt(2)*b)/(2*c)))/sqrt(2)-(sqrt(%pi)*erf((sqrt(2)*b-sqrt(2)*M)/(2*c))*c)/sqrt(2))
  def integrationRangeSymbolic(min:Double, max: Double) = {
   val left  = MyMath.erf( ((MyMath.sqrt2 * min) - (MyMath.sqrt2 * b)) / (2 * c) )
   val right = MyMath.erf( ((MyMath.sqrt2 * b) - (MyMath.sqrt2 * max)) / (2 * c) )
   (-left -right) *  ((a * MyMath.sqrtPi * c)  / MyMath.sqrt2)
  }
  //---------------------------------------------------------------------------
  //diff(f(x,a,b,c), a)
  //%e^(-(x-b)^2/(2*c^2))
  def getPartialDerivateA(x: Double) = {
    val xc = x - b
    val xc2 = xc * xc
    Math.exp( -xc2 / (2 * c * c) )
  }
  //---------------------------------------------------------------------------
  //diff(f(x,a,b,c), x)
  //-(a*(x-b)*%e^(-(x-b)^2/(2*c^2)))/c^2
  def getPartialDerivateX(x: Double) =
    - ( (a * (x - b) * getPartialDerivateA(x)) / (c * c))
  //---------------------------------------------------------------------------
  //diff(f(x,a,b,c), b)
  //(a*(x-b)*%e^(-(x-b)^2/(2*c^2)))/c^2
  def getPartialDerivateB(x: Double) =
    - getPartialDerivateX(x)
  //---------------------------------------------------------------------------
  //diff(f(x,a,b,c), c)
  //(a*(x-b)^2*%e^(-(x-b)^2/(2*c^2)))/c^3
  def getPartialDerivateC(x: Double) = {
    getPartialDerivateB(x) * ((x-b) /c)
  }
  //---------------------------------------------------------------------------
  def getJacobian(x: Double) =
    Array(
      getPartialDerivateA(x)
      , getPartialDerivateB(x)
      , getPartialDerivateC(x)
    )
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Gaussian_function
  def getArea() =
    MyMath.sqrt2 * a * Math.abs(c) * MyMath.sqrtPi
  //---------------------------------------------------------------------------
  //https://observablehq.com/@jobleonard/gaussian-kernel-calculater
  def getKernel(size: Double, step: Double = 1d) = {
    assert(size%2 == 1,"The kernel size must be odd")
    val end = 0.5 * size
    val start = -end
    val coeff = ArrayBuffer[Double]()
    var x = start
    var sum = 0d
    while (x <= end) {
      val c = integrationRangeSymbolic(x,x+step)
      coeff.append(c)
      sum += c
      x += step
    }
    //normalize
    val normalizeFactor = 1d / sum
    for (i <-0 until coeff.length)
      coeff(i) = coeff(i) * normalizeFactor

    coeff.toArray
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//Special Gaussian function that measures the probability density function of of a normally
// distributed random variable with expected value µ = b and variance s2 = c2.

//https://en.wikipedia.org/wiki/Random_variable
// A random variable is understood as a measurable distribution.function defined on a probability space that maps from the sample space to the real numbers.

//https://en.wikipedia.org/wiki/Probability_density_function
//In probability theory, a probability density distribution.function (PDF), or density of a continuous random variable,
// is a distribution.function whose value at any given sample (or point) in the sample space
// (the set of possible values taken by the random variable) can be interpreted as providing a relative likelihood
// that the value of the random variable would equal that sample
//Definition with Maxima: (http://maxima.sourceforge.net/):
//f(x,s,µ):=1/(s*sqrt(2*%pi))*exp((-1)/2*((x-µ)/s)^2)
object Gaussian1D {
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Gaussian_function sectionName Estimation of parameters
  //For gaussian1D , 3 parameters must be estimated : (a, b, c)
  //https://commons.apache.org/proper/commons-math/javadocs/api-3.4/org/apache/commons/math3/fitting/Gaussianfit.html
  def gaussian1D_Fitter(xySeq: Seq[Point2D_Double]) = {
    import scala.collection.JavaConverters._
    val pointSeq = xySeq map( p=> new WeightedObservedPoint(1, p.x, p.y) )
    val estimatedGaussian = GaussianCurveFitter.create.fit(pointSeq.asJava)
    val amplitude = estimatedGaussian(0)
    val average = estimatedGaussian(1)
    val stdDev = estimatedGaussian(2)
    val gaussian = new Gaussian1D(amplitude, average, stdDev)
    val seq = xySeq map ( _.toSequence())
    val predictedValueSeq = seq map (p=> gaussian.eval(p(0)))
    (gaussian, getGoodnessOfFit(seq, predictedValueSeq))
  }
  //---------------------------------------------------------------------------
  def fit(xySeq: Seq[Point2D_Double]) = gaussian1D_Fitter(xySeq: Seq[Point2D_Double])
  //---------------------------------------------------------------------------
}
//=============================================================================
//diff(f(x,a,b,c), c)
//(a*(x-b)^2*%e^(-(x-b)^2/(2*c^2)))/c^3
class Gaussian1D(amplitude : Double = 1, average: Double, stdDev: Double)
  extends Gaussian1D_Function(amplitude, average, stdDev) {
  //---------------------------------------------------------------------------
  val variance = stdDev * stdDev
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Full_width_at_half_maximum
  def getFwhm() = MyMath.FWHM_COEFFICIENT * stdDev
  //---------------------------------------------------------------------------
  //half width at half maximum
  def getHwhm() = getFwhm / 2
  //---------------------------------------------------------------------------
  //integrate(f(x,s,µ), x)
  //-erf(µ/(sqrt(2)*s)-x/(sqrt(2)*s))/2
  override def integratePoint(x: Double) =
    -0.5 * MyMath.erf((average - x) / (MyMath.sqrt2 * stdDev))
  //---------------------------------------------------------------------------
  //integrate(f(x,s,µ), x,a,b) := ((sqrt(%pi)*erf((sqrt(2)*µ-sqrt(2)*a)/(2*s))*s)/sqrt(2)-(sqrt(%pi)*erf((sqrt(2)*µ-sqrt(2)*b)/(2*s))*s)/sqrt(2))/(sqrt(2)*sqrt(%pi)*s)
  override def integrationRangeSymbolic(min:Double, max: Double) = {
    val scale = (MyMath.sqrtPi * stdDev) / MyMath.sqrt2
    val left  = scale * MyMath.erf( ((MyMath.sqrt2 * average) - (MyMath.sqrt2 * min)) / (2 * stdDev) )
    val right = scale * MyMath.erf( ((MyMath.sqrt2 * average) - (MyMath.sqrt2 * max)) / (2 * stdDev) )
    (left - right) / (MyMath.sqrt2 * MyMath.sqrtPi * stdDev)
  }
  //---------------------------------------------------------------------------
  //diff(f(x,s,µ), x)
  //-((x-µ)*%e^(-(x-µ)^2/(2*s^2)))/(sqrt(2)*sqrt(%pi)*s^3)
  override def getPartialDerivateX(x: Double) = {
    val xc = (x - average) / stdDev
    (-(x-average) * Math.exp(-0.5 * (xc * xc))) / (MyMath.sqrt2 * MyMath.sqrtPi * stdDev * stdDev * stdDev)
  }
  //---------------------------------------------------------------------------
  //diff(f(x,s,µ), µ)
  //(((x-µ)*%e^(-(x-µ)^2/(2*s^2)))/(sqrt(2)*sqrt(%pi)*s^3)
  def getPartialDerivateAverage(x: Double) =
    - getPartialDerivateX(x)
  //---------------------------------------------------------------------------
  //diff(f(x,s,µ), s)
  //((x-µ)^2*%e^(-(x-µ)^2/(2*s^2)))/(sqrt(2)*sqrt(%pi)*s^4)-%e^(-(x-µ)^2/(2*s^2))/(sqrt(2)*sqrt(%pi)*s^2)
  def getPartialDerivateStdDev(x: Double) = {
    val xc = (x - average) / stdDev
    (getPartialDerivateAverage(x) * ( (x-average) / stdDev)) -
     (Math.exp(-0.5 * (xc * xc)) / (MyMath.sqrt2 * MyMath.sqrtPi * stdDev * stdDev))
  }
  //---------------------------------------------------------------------------
   def getJacobian(x: Double, y: Double) =
     Array(
       getPartialDerivateAverage(x)
       , getPartialDerivateStdDev(x)
     )
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Gaussian1D.scala
//=============================================================================