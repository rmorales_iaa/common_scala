/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Jul/2020
 * Time:  18h:35m
 * Description: None
 */
//=============================================================================
package com.common.math.distribution.gaussian.guassian2D
//=============================================================================
import com.common.math.MyMath
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
//=============================================================================
//https://en.wikipedia.org/wiki/Gaussian_function
// extension to two dimension of Gaussain1D
//Definition with Maxima: (http://maxima.sourceforge.net/):
//f(x,y,A,x_0,y_0,s_x,s_y):=A*exp((-1)/2*(((x-x_0)/s_x)^2+((y-y_0)/s_y)^2))
//---------------------------------------------------------------------------
class Gaussian2D_Base(amplitude: Double
                      , xCentroid: Double //x coordinate of the centroid
                      , yCentroid: Double //y coordinate of the centroid
                      , width: Double
                      , height: Double) {
  //---------------------------------------------------------------------------
  require(amplitude > 0, s"Error creating Gaussian2D. Amplitude must be > 0. Actual value: $amplitude ")
  require(width > 0, s"Error creating Gaussian2D. Width(standard deviation in X axis) must be > 0. Actual value: $width ")
  require(height > 0,s"Error creating Gaussian2D. Height(standard deviation in Y axis,) must be > 0. Actual value: $height ")
  //---------------------------------------------------------------------------
  def eval(x: Double, y: Double) = {
    val vX = ((x - xCentroid) * (x - xCentroid)) / (2 * (width * width))
    val vY = ((y - yCentroid) * (y - yCentroid)) / (2 * (height * height))
    amplitude * Math.exp(-(vX + vY))
  }
  //---------------------------------------------------------------------------
  //integrate(f(x,a,b,c), x)
  //(sqrt(%pi)*a*c*erf(x/(sqrt(2)*c)-b/(sqrt(2)*c)))/sqrt(2)
  def integratePoint(x: Double, y: Double) =
    0.5 * Math.PI * amplitude * MyMath.erf((xCentroid - x) / (MyMath.sqrt2 * width)) * width *
                                MyMath.erf((yCentroid - y) / (MyMath.sqrt2 * height)) * height
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Simpson%27s_rule
  //https://math.stackexchange.com/questions/2121611/simpson-rule-for-double-integral
  def integrateRangeNumeric(minX:Double, maxX: Double, minY:Double, maxY: Double) : Double = {
    val ba = (maxX+minX) / 2
    val dc = (maxY+minY) / 2
    ((16 * eval(ba,dc)) + 4 * eval(ba,maxY) + 4 * eval(ba,minY) + 4 * eval(maxX,dc) + eval(maxX,maxY) +
      eval(maxX,minY) + 4 * eval(minX,dc) + eval(minX,maxY) + eval(minX,minY)) * ( ((maxX-minX) * (maxY-minY))/36)
  }
  //---------------------------------------------------------------------------
  def integrateRangeNumeric(min:Double, max: Double): Double = integrateRangeNumeric(min,max,min,max)
  //---------------------------------------------------------------------------
  // integrate(integrate(f(x,y,A,x_0,y_0,s_x,s_y), x,m,M),y,m,M):=
  //(A*((%pi*erf((sqrt(2)*x_m-sqrt(2)*x_0)/(2*s_x))-%pi*erf((sqrt(2)*x_M-sqrt(2)*x_0)/(2*s_x)))*s_x*erf((sqrt(2)*y_m-sqrt(2)*y_0)/(2*s_y))+(%pi*erf((sqrt(2)*x_M-sqrt(2)*x_0)/(2*s_x))-%pi*erf((sqrt(2)*x_m-sqrt(2)*x_0)/(2*s_x)))*s_x*erf((sqrt(2)*y_M-sqrt(2)*y_0)/(2*s_y)))*s_y)/2
  def integrationRangeSymbolic(minX:Double, maxX: Double, minY:Double, maxY: Double) = {
    val mx_x0 = MyMath.erf( (MyMath.sqrt2 * minX - MyMath.sqrt2 * xCentroid) / (2 * width) )
    val Mx_x0 = MyMath.erf( (MyMath.sqrt2 * maxX - MyMath.sqrt2 * xCentroid) / (2 * width) )
    val my_y0 = MyMath.erf( (MyMath.sqrt2 * minY - MyMath.sqrt2 * yCentroid) / (2 * height) )
    val My_y0 = MyMath.erf( (MyMath.sqrt2 * maxY - MyMath.sqrt2 * yCentroid) / (2 * height) )
    (amplitude * height * (
      (Math.PI * mx_x0 - Math.PI * Mx_x0) * width * my_y0 +
      (Math.PI * Mx_x0 - Math.PI * mx_x0) * width * My_y0)) / 2
  }
  //---------------------------------------------------------------------------
  def integrationRangeAlgebraic(min:Double, max: Double) : Double =
    integrationRangeSymbolic(min,max,min,max)
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Gaussian_function
  def getVolumen() = 2 * Math.PI * amplitude * width * height
  //---------------------------------------------------------------------------
  //https://softwarebydefault.com/2013/06/08/calculating-gaussian-kernels/
  def getKernel(size: Int, scale: Double = 1) = {
    assert(size%2 == 1,"The kernel size must be odd")
    val kernelRadius = size / 2
    val coeff = ArrayBuffer.fill[Double](size, size)(0)
    var sum = 0d
    for(y<- 0 until size;x<- 0 until size) {
      val c = eval(Math.abs(x - kernelRadius), Math.abs(y - kernelRadius)) * scale
      sum += c
      coeff(y)(x) = c
    }
    //normalize
    val normalizeFactor = 1d / sum
    for(y<-0 until size;x<-0 until size)
      coeff(y)(x) = coeff(y)(x) * normalizeFactor

    coeff.toArray map (_.toArray)
  }

  //---------------------------------------------------------------------------
  //https://observablehq.com/@jobleonard/gaussian-kernel-calculater
  def getDiscreteKernel(size: Double, step: Double = 1d) = {
    val end = 0.5 * size
    val start = -end
    var x = start
    var y = start
    val coeff = ArrayBuffer[ArrayBuffer[Double]]()
    var sum = 0d

    while (y <= end) {
      val xCoeff = ArrayBuffer[Double]()
      coeff.append(xCoeff)
      x = start
      while (x <= end) {
        val c =  integrationRangeSymbolic(x, x + step, y, y + step)
        xCoeff.append(c)
        sum += c
        x += step
      }
      y += step
    }

    //normalize
    val normalizeFactor = 1d / sum
    val xAxis = coeff.length
    val yAxis = coeff(0).length
    for (y <-0 until yAxis;x <-0 until xAxis)
      coeff(y)(x) = coeff(y)(x) * normalizeFactor

    coeff.toArray map (_.toArray)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
// This distribution function calculates the probability density distribution.function of a normally distributed random variable in 2D (https://en.wikipedia.org/wiki/Multivariate_normal_distribution)
//Definition with Maxima: (http://maxima.sourceforge.net/):
//f_xy(x,y,µ_x,µ_y,s_x,s_y)=%e^(-(y-µ_y)^2/(2*s_y^2)-(x-µ_x)^2/(2*s_x^2))/(2*%pi*s_x*s_y)
class Gaussian2D(xAverage: Double, yAverage: Double, xStdDev: Double, yStdDev: Double)
  extends Gaussian2D_Base(amplitude = 1 / (2 * Math.PI * xStdDev * yStdDev)
                          , xAverage
                          , yAverage
                          , xStdDev
                          , yStdDev) {
  //---------------------------------------------------------------------------
  //integrate((integrate(f_xy(x,y,µ_x,µ_y,s_x,s_y), x),y) := (erf(µ_x/(sqrt(2)*s_x)-x/(sqrt(2)*s_x))*erf(µ_y/(sqrt(2)*s_y)-y/(sqrt(2)*s_y)))/4
  override def integratePoint(x: Double, y: Double) =
     0.25 * MyMath.erf((xAverage - x) / (MyMath.sqrt2 * xStdDev))  *
            MyMath.erf((yAverage - y) / (MyMath.sqrt2 * yStdDev))
  //---------------------------------------------------------------------------
  //integrate(integrate(g_xy(x,y,µ_x,µ_y,s_x,s_y), x,x_m,x_M),y,y_m,y_M) :=
  //((%pi*erf((sqrt(2)*µ_x-sqrt(2)*x_m)/(2*s_x))-%pi*erf((sqrt(2)*µ_x-sqrt(2)*x_M)/(2*s_x)))*s_x*erf((sqrt(2)*µ_y-sqrt(2)*y_m)/(2*s_y))+(%pi*erf((sqrt(2)*µ_x-sqrt(2)*x_M)/(2*s_x))-%pi*erf((sqrt(2)*µ_x-sqrt(2)*x_m)/(2*s_x)))*s_x*erf((sqrt(2)*µ_y-sqrt(2)*y_M)/(2*s_y)))/(4*%pi*s_x)
  override def integrationRangeSymbolic(xMin:Double, xMax: Double, yMin:Double, yMax: Double) = {
    val ux_xm = MyMath.erf( (MyMath.sqrt2 * xAverage - MyMath.sqrt2 * xMin ) / (2 * xStdDev) )
    val ux_xM = MyMath.erf( (MyMath.sqrt2 * xAverage - MyMath.sqrt2 * xMax ) / (2 * xStdDev) )
    val uy_ym = MyMath.erf( (MyMath.sqrt2 * yAverage - MyMath.sqrt2 * yMin) / (2 * yStdDev) )
    val uy_yM = MyMath.erf( (MyMath.sqrt2 * yAverage - MyMath.sqrt2 * yMax ) / (2 * yStdDev) )

    ((Math.PI * ux_xm - Math.PI * ux_xM) * xStdDev * uy_ym +
    (Math.PI * ux_xM - Math.PI * ux_xm) * xStdDev * uy_yM) / (4 * Math.PI * xStdDev)
  }
  //---------------------------------------------------------------------------
  override def integrationRangeAlgebraic(min:Double, max: Double) : Double =
    integrationRangeSymbolic(min,max,min,max)
  //---------------------------------------------------------------------------
}
//=============================================================================
//https://dsp.stackexchange.com/questions/23460/how-to-calculate-gaussian-kernel-for-a-small-support-size
//f_xy(x,y,s):=1/(2*%pi*s^2)*exp((-1)/2*(x^2+y^2)/s^2);
class Gaussian2D_ZeroAverageSymmetrical(stdDev: Double)
  extends Gaussian2D(0 ,0, stdDev, stdDev) {
  //---------------------------------------------------------------------------
  //integrate(integrate(f_xy(x,y,s), x),y) := (erf(x/(sqrt(2)*s))*erf(y/(sqrt(2)*s)))/4
  override def integratePoint(x: Double, y: Double) =
    0.25 * MyMath.erf(x / (MyMath.sqrt2 * stdDev))  *
           MyMath.erf(y / (MyMath.sqrt2 * stdDev))
  //---------------------------------------------------------------------------
  //integrate(integrate(f_xy(x,y,s), x,x_m,x_M),y,y_m,y_M) :=
  //((%pi*erf(x_m/(sqrt(2)*s))-%pi*erf(x_M/(sqrt(2)*s)))*erf(y_m/(sqrt(2)*s))+ (%pi*erf(x_M/(sqrt(2)*s))-%pi*erf(x_m/(sqrt(2)*s)))*erf(y_M/(sqrt(2)*s)))/(4*%pi)
  override def integrationRangeSymbolic(xMin:Double, xMax: Double, yMin:Double, yMax: Double) : Double = {
    val xm = MyMath.erf( xMin / (MyMath.sqrt2 * stdDev) )
    val xM = MyMath.erf( xMax / (MyMath.sqrt2 * stdDev) )
    val ym = MyMath.erf( yMin / (MyMath.sqrt2 * stdDev) )
    val yM = MyMath.erf( yMax / (MyMath.sqrt2 * stdDev) )

    ((Math.PI * xm - Math.PI * xM) * ym +
     (Math.PI * xM - Math.PI * xm) * yM) / (4 * Math.PI)
  }
  //---------------------------------------------------------------------------
  override def integrationRangeAlgebraic(min:Double, max: Double) : Double =
    integrationRangeSymbolic(min,max,min,max)
  //---------------------------------------------------------------------------
  //diff(f_xy(x,y,s), s) := ((x-µ_x)^2*%e^(-(y-µ_y)^2/(2*s_y^2)-(x-µ_x)^2/(2*s_x^2)))/(2*%pi*s_x^4*s_y)-%e^(-(y-µ_y)^2/(2*s_y^2)-(x-µ_x)^2/(2*s_x^2))/(2*%pi*s_x^2*s_y)
  def getPartialDerivateStdDev(x: Double, y: Double) = {
    val x2 = x * x
    val y2 = y * y
    val x2_y2 = x2 + y2
    val stdDev2 = stdDev * stdDev
    val exp = Math.exp(-0.5 * (x2_y2 / stdDev2))
    ((x2_y2 * exp) / (2 * Math.PI * stdDev * stdDev * stdDev * stdDev * stdDev)) -
    (exp / (Math.PI * stdDev * stdDev * stdDev) )
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//https://dsp.stackexchange.com/questions/23460/how-to-calculate-gaussian-kernel-for-a-small-support-size
//f_xy(x,y,A,s):=A/(2*%pi*s^2)*exp((-1)/2*(x^2+y^2)/s^2)
class Gaussian2D_ZeroAverageSymmetricalScaled(a: Double,stdDev: Double)
  extends Gaussian2D_ZeroAverageSymmetrical(stdDev) {
  //---------------------------------------------------------------------------
  override def eval(x: Double, y: Double) =  a * super.eval(x,y)
  //---------------------------------------------------------------------------
  //integrate(integrate(f_xy(x,y,s), x),y) := (A*erf(x/(sqrt(2)*s))*erf(y/(sqrt(2)*s)))/4
  override def integratePoint(x: Double, y: Double) = a * super.integratePoint(x,y)
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Gaussian2D_Fit.scala
//=============================================================================