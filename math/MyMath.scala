/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  15/Nov/2018
  * Time:  09h:52m
  * Description: None
  */
//=============================================================================
package com.common.math
//=============================================================================
import scala.annotation.tailrec
import scala.collection.mutable.ArrayBuffer
//=============================================================================
import com.common.util.util.Util
import com.common.math.Histogram.HistogramType
import com.common.util.pattern.Pattern._
//=============================================================================
//=============================================================================
object MyMath {
  //---------------------------------------------------------------------------
  val erfA1 = 0.254829592d
  val erfA2 = -0.284496736d
  val erfA3 = 1.421413741d
  val erfA4 = -1.453152027d
  val erfA5 = 1.061405429d
  val erfP  = 0.3275911d
  //---------------------------------------------------------------------------
  val sqrt2 = Math.sqrt(2d)
  //---------------------------------------------------------------------------
  val sqrtMinus1 = Math.sqrt(-1d)
  //---------------------------------------------------------------------------
  val sqrtPi = Math.sqrt(Math.PI)
  //---------------------------------------------------------------------------
  val sqrt2Pi = Math.sqrt(2 * Math.PI)
  //---------------------------------------------------------------------------
  val FWHM_COEFFICIENT= 2 * Math.sqrt(2 * Math.log(2))
  //---------------------------------------------------------------------------
  def isInteger(s: String): Boolean =
    parsePattern(s, INTEGER_PATTERN, "", reportErrorMessage = false)
  //---------------------------------------------------------------------------
  def isFloat(s: String): Boolean =
    parsePattern(s, FLOAT_PATTERN, "", reportErrorMessage = false)
  //---------------------------------------------------------------------------
  def isEven(i: Long): Boolean = (i % 2) == 0
  //---------------------------------------------------------------------------
  def isOdd(i: Long): Boolean = !isEven(i)
  //---------------------------------------------------------------------------
  def isEvenSeq[T](sq: Seq[T]): Boolean = isEven(sq.length)
  //---------------------------------------------------------------------------
  def isOddSeq[T](sq: Seq[T]): Boolean = isOdd(sq.length)
  //---------------------------------------------------------------------------
  //http://blog.iannelson.systems/rounding-up-the-result-of-integer-division/
  def roundIntegerDivision(m: Long, n: Long): Long = (m + n - 1) / n
  //---------------------------------------------------------------------------
  def log2: Long => Double = (x: Long) => Math.log10(x) / Math.log10(2.0)
  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/36035074/how-can-i-find-an-overlap-between-two-given-ranges
  def getOverlappingTwoRanges(sA: Long, eA: Long, sB: Long, eB: Long): Long = {
    val totalRange = Math.max(eA, eB) - Math.min(sA, sB)
    val sumOfRanges = (eA - sA) + (eB - sB)
    if (sumOfRanges > totalRange) Math.min(eA, eB) - Math.max(sA, sB)
    else 0
  }
  //---------------------------------------------------------------------------
  def isOverlappingTwoRanges(sA: Long, eA: Long, sB: Long, eB: Long): Boolean = getOverlappingTwoRanges(sA, eA, sB, eB) > 0
  //---------------------------------------------------------------------------
  def factorize(x: Long): List[Long] = {
    def foo(x: Long, a: Long): List[Long] = if (a * a > x) List(x) else
      x % a match {
        case 0 => a :: foo(x / a, a)
        case _ => foo(x, a + 1)
      }
    foo(x, 2)
  }
  //---------------------------------------------------------------------------
  def factorial(n: Int): Int = n match {
    case 0 => 1
    case _ => n * factorial(n-1)
  }
  //---------------------------------------------------------------------------
  //https://gist.github.com/vsalvati/3947871
  //Greatest commonHenosis divisor
  @tailrec
  def gcd(a: Int,b: Int): Int = if(b == 0 ) a else gcd(b, a % b)
  //---------------------------------------------------------------------------
  def average(s: IndexedSeq[Int]): Int = s.sum / s.size
  //-------------------------------------------------------------------------
  def stdDouble(data: Seq[Double], isPoblation : Boolean = false) : Double = {
    val n  = data.size.toDouble
    val average = data.sum / n.toDouble
    val variance = data.map( v=> Math.pow(average - v, 2) ).sum / (if (isPoblation) n else n-1)
    Math.sqrt(variance)
  }
  //---------------------------------------------------------------------------
  def mode(v: IndexedSeq[Int]): Int = {
    val histogram = Array.ofDim[Long]((v.max + 1) )
    v foreach( histogram( _ ) += 1 )
    histogram.indexOf(histogram.max)
  }
  //---------------------------------------------------------------------------
  def isIncreasing[T] (s: Seq[T]) (implicit ord: Ordering[T]): Boolean = {
    s.sorted.zipWithIndex foreach { case(v,i)=> if ( v != s(i)) return false}
    true
  }
  //https://stackoverflow.com/questions/11106886/scala-doubles-and-precision
  //---------------------------------------------------------------------------
  def roundFloat(f: Float, n: Byte): Float = {
    val t = Math.pow(10,n)
    (Math.rint(f * t) / t).toFloat
  }
  //---------------------------------------------------------------------------
  def roundFloatAt2(f: Float): Float = roundFloat(f,2) //(Math.rint( f * 100) / 100).toFloat
  //---------------------------------------------------------------------------
  def roundAt(p: Double)(n: Double): Double = { val s = Math pow (10, p); (Math round n * s) / s }
  def roundDoubleAt2(p: Double): Double = roundAt(2)(p)
  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/5294955/how-to-scale-down-a-range-of-numbers-with-a-known-min-and-max-value
  def rescale(x: Double
              , domain0: Double
              , domain1: Double
              , range0: Double
              , range1: Double): Double = {
    //-------------------------------------------------------------------------
    def interpolate(x: Double) = range0 * (1 - x) + range1 * x
    //-------------------------------------------------------------------------
    def uninterpolate(x: Double) =  {
      val b = if ((domain1 - domain0) != 0) domain1 - domain0 else 1 / domain1
      (x - domain0) / b
    }
    //-------------------------------------------------------------------------
    interpolate(uninterpolate(x))
  }
  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/5294955/how-to-scale-down-a-range-of-numbers-with-a-known-min-and-max-value
  def rescaleSeq(xSeq: IndexedSeq[Double], targetRangeMin: Double, targetRangeMax: Double): IndexedSeq[Double] = {
    //-------------------------------------------------------------------------
    val sourceRangeMin = xSeq.min
    val sourceRangeMax = xSeq.max
    //-------------------------------------------------------------------------
    def interpolate(x: Double) = targetRangeMin * (1 - x) + targetRangeMax * x
    //-------------------------------------------------------------------------
    def uninterpolate(x: Double) =  {
      val b = if ((sourceRangeMax - sourceRangeMin) != 0) sourceRangeMax - sourceRangeMin else 1 / sourceRangeMax
      (x - sourceRangeMin) / b
    }
    //-------------------------------------------------------------------------
    xSeq.map( x=> interpolate(uninterpolate( x )) )
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def getIntPercentileFloat(p: Int, seq: Seq[Float], isSortedSeq: Boolean = false): Float = {
    require(0 <= p && p <= 100)                      // some value requirements
    require(seq.nonEmpty)                            // more value requirements
    val sortedSeq = if (isSortedSeq) seq else seq.sorted
    val k = Math.ceil((seq.length - 1) * (p / 100.toDouble)).toInt
    sortedSeq(k)
  }
  //---------------------------------------------------------------------------
  def getIntPercentileDouble(p: Int, seq: Seq[Double], isSortedSeq: Boolean = false): Double = {
    require(0 <= p && p <= 100)                      // some value requirements
    require(seq.nonEmpty)                            // more value requirements
    val sortedSeq = if (isSortedSeq) seq else seq.sorted
    val k = Math.ceil((seq.length - 1) * (p / 100.toDouble)).toInt
    sortedSeq(k)
  }
  //---------------------------------------------------------------------------
  def getIntPercentileInt(p: Int, seq: Seq[Int], isSortedSeq: Boolean = false): Int = {
    require(0 <= p && p <= 100)                      // some value requirements
    require(seq.nonEmpty)                            // more value requirements
    val sortedSeq = if (isSortedSeq) seq else seq.sorted
    val k = Math.ceil((seq.length - 1) * (p / 100.toDouble)).toInt
    sortedSeq(k)
  }
  //---------------------------------------------------------------------------
  def getIntPercentileLong(p: Int, seq: Seq[Long], isSortedSeq: Boolean = false): Long = {
    require(0 <= p && p <= 100)                      // some value requirements
    require(seq.nonEmpty)                            // more value requirements
    val sortedSeq = if (isSortedSeq) seq else seq.sorted
    val k = Math.ceil((seq.length - 1) * (p / 100.toDouble)).toInt
    sortedSeq(k)
  }
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Normalization_(image_processing)
  def getIntNormalizationWithMinMax(seq: IndexedSeq[Int], oldMin: Int, oldMax: Int, newMin: Int, newMax: Int ): IndexedSeq[Int] ={
    val proportion = ((newMax - newMin) / (oldMax - oldMin.toFloat))
    seq map( p => Math.min(newMax,Math.round(( p - oldMin) *  proportion +  newMin)))
  }
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Histogram_equalization#Implementationhint
  def getIntNormalizationWithHistogramEqualization(seq: IndexedSeq[Int], greyLevels: Int = 256 ): (HistogramType, ArrayBuffer[Int]) ={
    val histogram = Histogram.classic(seq)
    var accumulated : Long = 0
    val cdfSeq= histogram._2.map{ h =>   // cumulative distribution distribution.function
      val r = h + accumulated
      accumulated += h
      r
    }
    val cdfMin = cdfSeq.head
    val divisor = (seq.size - cdfMin).toFloat
    (histogram._1, cdfSeq map ( cdf=> Math.round( ((cdf - cdfMin) / divisor )   * (greyLevels -1))))
  }
  //---------------------------------------------------------------------------
  def combinationCount(n: Int, k: Int): Int =
    if (k == 0 || k == n) 1
    else combinationCount(n - 1, k - 1) + combinationCount(n - 1, k)
  //---------------------------------------------------------------------------
  //http://aperiodic.net/phil/scala/s-99/p26.scala
  def getCombinationSeq[A](ls: List[A], n: Int): List[List[A]] =
    if (n == 0) List(Nil)
    else Util.flatMapSubLists(ls) { sl =>
      getCombinationSeq(sl.tail , n - 1) map {sl.head :: _}
    }
  //---------------------------------------------------------------------------
  //https://www.geeksforgeeks.org/find-the-centroid-of-a-non-self-intersecting-closed-polygon/
  //Points are numbered in clockwise order
  def getPolygonCentroid(v: Array[Array[Double]]) = {
    val ans = new Array[Double](2)
    val n = v.length
    var signedArea : Double = 0
    // For all vertices
    for (i <- 0 until n) {
      val x0 = v(i)(0)
      val y0 = v(i)(1)
      val x1 = v((i + 1) % n)(0)
      val y1 = v((i + 1) % n)(1)
      // Calculate value of A using shoelace formula
      val A = (x0 * y1) - (x1 * y0)
      signedArea += A
      // Calculating coordinates of centroid of polygon
      ans(0) += (x0 + x1) * A
      ans(1) += (y0 + y1) * A
    }
    signedArea = signedArea * 0.5
    ans(0) = ans(0) / (6 * signedArea)
    ans(1) = ans(1) / (6 * signedArea)
    (ans(0),ans(1) )
  }
  //---------------------------------------------------------------------------
  //https://es.wikipedia.org/wiki/Funci%C3%B3n_error
  //https://www.johndcook.com/blog/2009/01/19/stand-alone-error-function-erf/
  //Abramowitz and Stegun, Handbook of Mathematical Functions, Chapter 7 (7.1.1)
  //error function
  def erf(x: Double) = {
    val absX = Math.abs(x)
    val t = 1.0 / (1.0 + erfP * absX)
    val y = 1.0 - (((((erfA5 * t + erfA4) * t) + erfA3) * t + erfA2) * t + erfA1) * t * Math.exp(-absX * absX)
    val sign = if (x == 0) 0 else if (x < 0) -1 else 1
    sign * y
  }
  //---------------------------------------------------------------------------
  def getPartitionInt(min: Int, max: Int, pSize: Int) : Array[Array[Int]] = { //(min,max) included
    assert(pSize > 0)
    val range = max - min + 1
    if (range <= pSize) (for(i<-min to max) yield Array(i)).toArray
    else{
      val completePartitionCount = range  / pSize
      val remainPartition = range  % pSize
      val r = for(i<-0 until pSize) yield
          ArrayBuffer.tabulate(completePartitionCount)(n=> min + (i *completePartitionCount) + n)
      val offset = r.last.last + 1
      for(i<-0 until remainPartition)
         r(i) += (i + offset)
      (r map (_.toArray)).toArray
    }
  }
  //---------------------------------------------------------------------------
  def truncate(value: Double)  =  value - value % 1
  //---------------------------------------------------------------------------
  //Find the bit (i.e. power of 2) immediately greater than or equal to N
  //https://stackoverflow.com/questions/2891925/get-length-of-bits-used-in-int
  def bitCeil(n: Int) = 1 << (Math.log(n)/Math.log(2)+1).toInt
  //---------------------------------------------------------------------------
  //http://www.java2s.com/example/java-utility-method/float-number-mod/fmod-double-a-double-b-bc124.html
  def fmod(a: Double, b: Double): Double = {
    val result = Math.floor(a / b).toInt
    a - result * b
  }
  //---------------------------------------------------------------------------
  def normalizeLongitude(eulerAngle0: Double, lngp: Double) : Double = {
    if (eulerAngle0 >= 0.0) {
      if (lngp < 0.0) return lngp + 360.0
    }
    else {
      if (lngp > 0.0) return lngp - 360.0
    }

    if (lngp > 360.0) lngp - 360.0
    else {
      if (lngp < -360.0) lngp + 360.0
      else lngp
    }
  }
  //---------------------------------------------------------------------------
  def normalizeLongitude2(phi: Double) : Double = {
    if (phi > 180.0) phi - 360.0
    else
      if (phi < -180.0) phi + 360.0
      else phi
  }
  //---------------------------------------------------------------------------
  def normalizeLatitude(latp: Double) : Double = {
    if (latp >  90.0) 180.0 - latp
    else {
      if (latp < -90.0) -180.0 - latp
      else latp
    }
  }
  //---------------------------------------------------------------------------
  def copySign(x:Double, y: Double) = if(y < 0) -Math.abs(x) else Math.abs(x)
  //---------------------------------------------------------------------------
  def getPowerSeq(v: Double, order: Int): Array[Double] = {
    var lastValue = 1d
    (for (_ <- 0 to order) yield {
      val prev = lastValue
      lastValue = v * lastValue
      prev
    }).toArray
  }

  //---------------------------------------------------------------------------
  def linealToDecibels(linealValue: Double) = 10 * Math.log10(linealValue)
  //---------------------------------------------------------------------------
  def decibelsToLineal(dB: Double) =  Math.pow(10,dB/10)
  //---------------------------------------------------------------------------
  //quotient in decibels: snrA / snrB
  def getDivisionOfSNR_lineal(snrA: Double  //Dividend
                              , snrB: Double //divisor
                             ) =
    10 * Math.log10(snrA/snrB)
  //---------------------------------------------------------------------------
  //quotient in decibels: snrA / snrB
  def getDivisionOfSNR_decibels(snrA: Double //Dividend
                                , snrB: Double //divisor
                               ) =
    Math.pow(10,(snrA/snrB)/10)
  //---------------------------------------------------------------------------
  def calculateDeterminant(matrix: Array[Array[Double]]): Double = {
    val n = matrix.length
    if (n == 1) matrix(0)(0) // Base case: For a 1x1 matrix, the determinant is the single element
    else {
      var det = 0.0
      for (col <- 0 until n) {
        // Calculate the cofactor matrix for this column.
        val cofactorMatrix = (for {row <- 1 until n} yield
          matrix(row).patch(col, Nil, 1)).toArray
        // Recursively calculate the determinant using cofactor expansion.
        det += matrix(0)(col) * Math.pow(-1, col) * calculateDeterminant(cofactorMatrix)
      }
      det
    }
  }
  //---------------------------------------------------------------------------
  def isMatrixSingular(matrix: Array[Array[Double]]): Boolean =
    calculateDeterminant(matrix) == 0
  //---------------------------------------------------------------------------
  //in degrees
  def normalizeAngle(angle: Double): Double = {
    val normalizedAngle = angle % 360
    if (normalizedAngle < 0) normalizedAngle + 360
    else normalizedAngle
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file MyMath.scala
//=============================================================================