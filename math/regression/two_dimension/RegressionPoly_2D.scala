/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Dec/2022
 * Time:  18h:06m
 * generic fitting for a polynomy defined with order = 2 as:
  e(x,y,a_00,a_01,a_02,a_10,a_11,a_12,a_20,a_21,a_22):=a_00·x^0·y^0+a_01·x^0·y^1+a_02·x^0·y^2+a_10·x^1·y^0+a_11·x^1·y^1+a_12·x^1·y^2+a_20·x^2·y^0+a_21·x^2·y^1+a_22·x^2·y^2
 */
//=============================================================================
package com.common.math.regression.two_dimension
//=============================================================================
import com.common.geometry.point.Point2D_Double
//=============================================================================
//=============================================================================
trait RegressionPoly_2D {
  //---------------------------------------------------------------------------
  val coeffSeq: Array[Double]
  val order: Int
  //---------------------------------------------------------------------------
  var errorModel: RegressionPoly_2D     = null
  //---------------------------------------------------------------------------
  def estimateValue(pos: Point2D_Double): Double
  //---------------------------------------------------------------------------
  def estimateValueWithError(pos: Point2D_Double): Double = {
    val estValue = estimateValue(pos)
    if (errorModel == null) estValue
    else estValue - errorModel.estimateValue(pos) //subtract the error
  }
  //---------------------------------------------------------------------------
  def getErrorModel() = errorModel
  //---------------------------------------------------------------------------
  def setErrorModel(rm: RegressionPoly_2D) = errorModel = rm
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RegressionGeneralPoly_2D.scala
//=============================================================================
