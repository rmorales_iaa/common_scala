/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  09/Jul/2022
 * Time:  20h:26m
 * Description:
 * generic fitting for a polynomy defined with order = 2 as:
  e(x,y,a_00,a_01,a_02,a_10,a_11,a_12,a_20,a_21,a_22):=a_00*x0*y0+a_01*x0*y1+a_02*x0*y2+a_10*x1*y0+a_11*x1*y1+a_12*x1*y2+a_20*x2*y0+a_21*x2*y1+a_22*x2*y2
 */
//=============================================================================
package com.common.math.regression.two_dimension.parabolicPoly
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.math.regression.two_dimension.RegressionPoly_2D_Fit
//=============================================================================
import org.apache.commons.math3.analysis.{MultivariateMatrixFunction, MultivariateVectorFunction}
import org.apache.commons.math3.fitting.leastsquares.{LeastSquaresBuilder, LevenbergMarquardtOptimizer}
//=============================================================================
//=============================================================================
object Parabolic_2D_fit {
  //parapolic function in 2D degree 4
  //---------------------------------------------------------------------------
  def eval(p: Point2D_Double, order: Int, parameterSeq: Array[Double]) = {

    val x = p.x
    val y = p.y
    
    val x2 = x * x
    val x3 = x2 * x
    val x4 = x3 * x

    val y2 = y * y
    val y3 = y2 * y
    val y4 = y3 * y
    
    val a_0 = parameterSeq(0)
    val a_1 = parameterSeq(1)
    val a_2 = parameterSeq(2)
    val a_3 = parameterSeq(3)
    val a_4 = parameterSeq(4)
    val a_5 = parameterSeq(5)
    val a_6 = parameterSeq(6)
    val a_7 = parameterSeq(7)
    val a_8 = parameterSeq(8)
    val a_9 = parameterSeq(9)
    val a_10 = parameterSeq(10)
    val a_11 = parameterSeq(11)
    val a_12 = parameterSeq(12)
    val a_13 = parameterSeq(13)
    val a_14 = parameterSeq(14)

    a_0*x4+a_1*x3*y+a_2*x2*y2+a_3*x*y3+a_4*y4+a_5*x3+a_6*x2*y+a_7*x*y2+a_8*y3+a_9*x2+a_10*x*y+a_11*y2+a_12*x+a_13*y+a_14
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import Parabolic_2D_fit._
case class Parabolic_2D_fit(sourcePosSeq: Array[Point2D_Double]
                             , targetPosSeq: Array[Double]
                             , order: Int
                             , initialParameterGuess: Option[Array[Double]] = None
                             , maxEvaluations: Int = 10000
                             , maxIterations: Int = 1000) extends RegressionPoly_2D_Fit {
  //---------------------------------------------------------------------------
  //evaluate the function using the sample sequence with the NEW parameters
  private def function: MultivariateVectorFunction = new MultivariateVectorFunction() {
    //-------------------------------------------------------------------------
    override def value(parameterSeq: Array[Double]): Array[Double] = {
      sourcePosSeq.map { p => eval(p, order, parameterSeq) }
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //evaluate the jacobian of the function using the sample sequence and the NEW parameters
  private def jacobian: MultivariateMatrixFunction = new MultivariateMatrixFunction() {
    //-------------------------------------------------------------------------
    override def value(parameterSeq: Array[Double]): Array[Array[Double]] = {

      sourcePosSeq.map { p =>
        val x = p.x
        val y = p.y

        val x2 = x * x
        val x3 = x2 * x
        val x4 = x3 * x

        val y2 = y * y
        val y3 = y2 * y
        val y4 = y3 * y

        Array(
          x4
        , x3*y
        , x2*y2
        , x*y3
        , y4
        , x3
        , x2*y
        , x*y2
        , y3
        , x2
        , x*y
        , y2
        , x
        , y
        , 1)
      }
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private def getInitialParameterGuess(): Array[Double] =
    initialParameterGuess.getOrElse(
      Array.fill[Double](Parabolic_2D.getParameterSeqCount(order))(0)
    )
  //---------------------------------------------------------------------------
  private def getTarget() = targetPosSeq
  //---------------------------------------------------------------------------
  def fit(): Option[Parabolic_2D] = {
    try {
      val lsb = new LeastSquaresBuilder()
        .model(function, jacobian)
        .target(getTarget())
        .start(getInitialParameterGuess())
        .maxEvaluations(maxEvaluations) //set upper limit of evaluation time
        .maxIterations(maxIterations) //set upper limit of iteration time
      val optimizer = new LevenbergMarquardtOptimizer().optimize(lsb.build)
      val r = optimizer.getPoint.toArray
      r foreach { v => if (v.isNaN) return None }
      Some(Parabolic_2D(r,order))
    }
    catch {
      case e: Exception => println(e.toString)
        None
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Parabolic_2D_fit.scala
//=============================================================================