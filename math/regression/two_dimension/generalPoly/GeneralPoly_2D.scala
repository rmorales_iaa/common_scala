/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Dec/2022
 * Time:  18h:06m
 * generic fitting for a polynomy defined with order = 2 as:
  e(x,y,a_00,a_01,a_02,a_10,a_11,a_12,a_20,a_21,a_22):=a_00·x^0·y^0+a_01·x^0·y^1+a_02·x^0·y^2+a_10·x^1·y^0+a_11·x^1·y^1+a_12·x^1·y^2+a_20·x^2·y^0+a_21·x^2·y^1+a_22·x^2·y^2
 */
//=============================================================================
package com.common.math.regression.two_dimension.generalPoly
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.math.regression.two_dimension.RegressionPoly_2D
//=============================================================================
//=============================================================================
object GeneralPoly_2D {
  //---------------------------------------------------------------------------
  def getParameterSeqCount(order: Int) = (order + 1) * (order + 1)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class GeneralPoly_2D(coeffSeq: Array[Double], order: Int) extends RegressionPoly_2D {
  //---------------------------------------------------------------------------
  def estimateValue(pos: Point2D_Double): Double =
    GeneralPoly_2D_fit.eval(pos, order, coeffSeq)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file GeneralPoly_2D.scala
//=============================================================================
