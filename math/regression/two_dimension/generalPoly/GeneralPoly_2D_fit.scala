/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  09/Jul/2022
 * Time:  20h:26m
 * Description:
 * generic fitting for a polynomy defined with order = 2 as:
  e(x,y,a_00,a_01,a_02,a_10,a_11,a_12,a_20,a_21,a_22):=a_00·x^0·y^0+a_01·x^0·y^1+a_02·x^0·y^2+a_10·x^1·y^0+a_11·x^1·y^1+a_12·x^1·y^2+a_20·x^2·y^0+a_21·x^2·y^1+a_22·x^2·y^2
 */
//=============================================================================
package com.common.math.regression.two_dimension.generalPoly
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.math.MyMath
import com.common.math.regression.two_dimension.RegressionPoly_2D_Fit
//=============================================================================
import org.apache.commons.math3.analysis.{MultivariateMatrixFunction, MultivariateVectorFunction}
import org.apache.commons.math3.fitting.leastsquares.{LeastSquaresBuilder, LevenbergMarquardtOptimizer}
//=============================================================================
//=============================================================================
object GeneralPoly_2D_fit {
    //---------------------------------------------------------------------------
  def eval(p: Point2D_Double, order: Int, parameterSeq: Array[Double]) = {
    val xPowerSeq = MyMath.getPowerSeq(p.x, order)
    val yPowerSeq = MyMath.getPowerSeq(p.y, order)
    var acc = 0d
    var k = 0
    for (x <- 0 to order;
         y <- 0 to order) {
      acc += parameterSeq(k) * xPowerSeq(x) * yPowerSeq(y)
      k += 1
    }
    acc
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import GeneralPoly_2D_fit._
case class GeneralPoly_2D_fit(sourcePosSeq: Array[Point2D_Double]
                              , targetPosSeq: Array[Double]
                              , order: Int
                              , initialParameterGuess: Option[Array[Double]] = None
                              , maxEvaluations: Int = 10000
                              , maxIterations: Int = 1000) extends RegressionPoly_2D_Fit {
  //---------------------------------------------------------------------------
  //evaluate the function using the sample sequence with the NEW parameters
  private def function: MultivariateVectorFunction = new MultivariateVectorFunction() {
    //-------------------------------------------------------------------------
    override def value(parameterSeq: Array[Double]): Array[Double] = {
      sourcePosSeq.map { p => eval(p, order, parameterSeq) }
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //evaluate the jacobian of the function using the sample sequence and the NEW parameters
  private def jacobian: MultivariateMatrixFunction = new MultivariateMatrixFunction() {
    //-------------------------------------------------------------------------
    override def value(parameterSeq: Array[Double]): Array[Array[Double]] = {

      sourcePosSeq.map { p =>
        val xPowerSeq = MyMath.getPowerSeq(p.x, order)
        val yPowerSeq = MyMath.getPowerSeq(p.y, order)
        (for (x <- 0 to order;
              y <- 0 to order) yield {
          xPowerSeq(x) * yPowerSeq(y)
        }).toArray
      }
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private def getInitialParameterGuess(): Array[Double] =
    initialParameterGuess.getOrElse(
      Array.fill[Double](GeneralPoly_2D.getParameterSeqCount(order))(0)
    )
  //---------------------------------------------------------------------------
  private def getTarget() = targetPosSeq
  //---------------------------------------------------------------------------
  def fit(): Option[GeneralPoly_2D] = {
    try {
      val lsb = new LeastSquaresBuilder()
        .model(function, jacobian)
        .target(getTarget())
        .start(getInitialParameterGuess())
        .maxEvaluations(maxEvaluations) //set upper limit of evaluation time
        .maxIterations(maxIterations) //set upper limit of iteration time
      val optimizer = new LevenbergMarquardtOptimizer().optimize(lsb.build)
      val r = optimizer.getPoint.toArray
      r foreach { v => if (v.isNaN) return None }
      Some(GeneralPoly_2D(r,order))
    }
    catch {
      case e: Exception => println(e.toString)
      None
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file GeneralPoly_2D_fit.scala
//=============================================================================