/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Sep/2021
 * Time:  19h:13m
 * Description: None
 */
//=============================================================================
package com.common.math.regression.spline
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
case class Spline(p: PiecewiseFunction) extends MyLogger{
  //---------------------------------------------------------------------------
  private val storage = ArrayBuffer[PiecewiseFunction](p)
  //---------------------------------------------------------------------------
  def +=(p: PiecewiseFunction) : Boolean = {
    storage foreach { p2=>
      //check that definition limits does not overlap
      if (p2.isInButNotInBoundaries(p.s) ||
          p2.isInButNotInBoundaries(p.e))
        return error(s"The piecewiseFunction: ${p.toString} overlaps the interval definition limits of: ${p2.toString}")

      //check p is not a subInterval
      if (p2.s > p.s && p2.e > p.e)
        return error(s"The piecewiseFunction: ${p.toString} is a sub-interval of: ${p2.toString}")

      //add to storage
      storage += p
    }
    true
  }
  //----------------------------------------------------------------------------
  def evaluate(x: Double, weight: Double = 1) : Double = {
    storage foreach { pw =>
      if (pw.isIn(x)) return pw.evaluate(x).get * weight
    }
    0
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Spline.scala
//=============================================================================
