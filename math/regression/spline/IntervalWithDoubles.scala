//=============================================================================
package com.common.math.regression.spline
//=============================================================================
//=============================================================================
object IntervalWithDoubles{
  //---------------------------------------------------------------------------
  private final val MIN_DISTANCE_TO_BE_EQUAL = 10e-6
  //---------------------------------------------------------------------------
  def compare[T](a:IntervalWithDoubles[T], b:IntervalWithDoubles[T]) = a compare b
  //---------------------------------------------------------------------------
}
//=============================================================================
//No overlapping interval. start < end
import IntervalWithDoubles._
trait IntervalWithDoubles[T] extends Ordered[IntervalWithDoubles[T]] {
  //---------------------------------------------------------------------------
  type IntervalDataType = Double
  //---------------------------------------------------------------------------
  val s: IntervalDataType //star
  val e: IntervalDataType //end
  val v: T                //value
  //---------------------------------------------------------------------------
  require(s <= e, s"The interval with start: $s must be greater or equal to the end of interval: $e")
  //---------------------------------------------------------------------------
  def compare(itv: IntervalWithDoubles[T]): Int =
    if (equal(itv)) 0
    else s compareTo itv.s
  //---------------------------------------------------------------------------
  def equal(itv: IntervalWithDoubles[T]): Boolean = {
    if (itv == null) return false
    if (Math.abs(s - itv.s) <= MIN_DISTANCE_TO_BE_EQUAL) return false
    if (Math.abs(e - itv.e) <= MIN_DISTANCE_TO_BE_EQUAL) return false
    true
  }
  //---------------------------------------------------------------------------
  def isIn(p: IntervalDataType): Boolean = (p >= s) && (p <= e)
  //---------------------------------------------------------------------------
  def isInButNotInBoundaries(p: IntervalDataType): Boolean = (p > s) && (p < e)
  //---------------------------------------------------------------------------
  def intersects(itv: IntervalWithDoubles[T]): Boolean =
    isIn(itv.s) || isIn(itv.e) || itv.isIn(s) || itv.isIn(e)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file IntervalWithDoubles.scala
//=============================================================================
