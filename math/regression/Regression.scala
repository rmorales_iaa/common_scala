/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  13/Sep/2018
  * Time:  09h:57m
  * Description:
  *   https://github.com/otobrglez/regressor
  * For large regression, use com.common.hadoop regression:
  *   https://jaceklaskowski.gitbooks.io/mastering-apache-com.common.hadoop/content/com.common.hadoop-mllib/com.common.hadoop-mllib-LinearRegression.html
  */
//=============================================================================
package com.common.math.regression
//=============================================================================
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.logger.MyLogger
//=============================================================================
import scala.concurrent.duration.Duration
import scala.math.{pow, sqrt}
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import org.apache.commons.math3.fitting.{PolynomialCurveFitter, WeightedObservedPoint}
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.apache.commons.math3.analysis.polynomials.PolynomialFunction
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object Regression extends  MyLogger {
  //---------------------------------------------------------------------------
  //_1 => slope
  //_2 => intercept
  // y = slope*x + intercept
  def linear[T](pairs: Array[Seq[Double]], verbose: Boolean = false) : (Double,Double,Double) = {
    val n = pairs.size

    val fSumI = Future(pairs.map(x => x(0)).sum)
    val fSumXi = Future(pairs.map(x => x(1)).sum)
    val fSumX2i = Future(pairs.map(x => pow(x(0), 2)).sum)
    val fSumY2i = Future(pairs.map(x => pow(x(1), 2)).sum)
    val fSumXYi = Future(pairs.map(x => x(0) * x(1)).sum)

    val sums = for {
      sumXi <- fSumI
      sumYi <- fSumXi
      sumX2i <- fSumX2i
      sumY2i <- fSumY2i
      sumXYi <- fSumXYi
    } yield (sumXi, sumYi, sumX2i, sumY2i, sumXYi)

    val (sumX, sumY, sumX2, sumY2, sumXY) = Await.result(sums, Duration.Inf)
    val dn = n * sumX2 - pow(sumX, 2)
    if (dn == 0.0) {
      if (verbose) error("Can not solve the linear regression")
      return (0d,0d,0d)
    }

    val fSlopei = Future {((n * sumXY) - (sumX * sumY)) / dn}
    val fIntercepti = Future {((sumY * sumX2) - (sumX * sumXY)) / dn}
    val fT1i = Future {((n * sumXY) - (sumX * sumY)) * ((n * sumXY) - (sumX * sumY))}
    val fT2i = Future { (n * sumX2) - pow(sumX, 2)}
    val fT31 = Future {(n * sumY2) - pow(sumY, 2)}

    val poms = for {
      slopei <- fSlopei
      intercepti <- fIntercepti
      t1i <- fT1i
      t2i <- fT2i
      t31 <- fT31
    } yield (slopei, intercepti, t1i, t2i, t31)

    val (slope, intercept, t1, t2, t3) = Await.result(poms, Duration.Inf)

    if (t2 * t3 != 0.0) (slope, intercept, t1 / (t2 * t3))
    else (slope, intercept, 0.0)
  }
  //---------------------------------------------------------------------------
  // _1 => slope
  // _2 => intercept
  // _3 => slope error
  // _4 => intercept error
  // _5 => distribution.function for computing errors
  // y = slope*x - intercept
  def leastSquares(pairs: IndexedSeq[Seq[Double]]) = {
    val n = pairs.size

    val fSumXi = Future(pairs.map(x => x(0)).sum)
    val fSumYi = Future(pairs.map(x => x(1)).sum)
    val fSumXYi = Future(pairs.map(x => x(0) * x(1)).sum)
    val fSumXSqi = Future(pairs.map(x => pow(x(0), 2)).sum)

    val pro = for {
      sumXi <- fSumXi
      sumYi <- fSumYi
      sumXYi <- fSumXYi
      sumXSqi <- fSumXSqi
    } yield (sumXi, sumYi, sumXYi, sumXSqi)

    val (sumX, sumY, sumXY, sumXSq) = Await.result(pro, Duration.Inf)
    val m = (sumXY - sumX * sumY / n) / (sumXSq - sumX * sumX / n)
    val b = sumY / n - m * sumX / n

    // Errors
    var sum = 0.0
    for (pair <- pairs)
      sum += (pair(1) - b - m * pair(0)) *  (pair(1) - b - m * pair(0))

    val delta = n * sumXSq - pow(sumX, 2)
    val vari = 1.0 / (n - 2.0) * sum
    val b_err = sqrt(vari / delta * sumXSq)

    val m_err = sqrt(n / delta * vari)

    (m, b, m_err, b_err, (x: Double) => { m * x + b })
  }
  //---------------------------------------------------------------------------
  //https://rosettacode.org/wiki/Polynomial_regression#Scala
  //return a, b c => cx2 + bx + a
  def quadraticPolynomial(xy: Seq[(Int, Int)]) =  {

    val r = xy.indices

    def average[U](ts: Iterable[U])(implicit num: Numeric[U]) = num.toDouble(ts.sum) / ts.size

    def x3m: Double = average(r.map(a => a * a * a))
    def x4m: Double = average(r.map(a => a * a * a * a))
    def x2ym = xy.reduce((a, x) => (a._1 + x._1 * x._1 * x._2, 0))._1.toDouble / xy.size
    def xym = xy.reduce((a, x) => (a._1 + x._1 * x._2, 0))._1.toDouble / xy.size

    val x2m: Double = average(r.map(a => a * a))
    val (xm, ym) = (average(xy.map(_._1)), average(xy.map(_._2)))
    val (sxx, sxy) = (x2m - xm * xm, xym - xm * ym)
    val sxx2: Double = x3m - xm * x2m
    val sx2x2: Double = x4m - x2m * x2m
    val sx2y: Double = x2ym - x2m * ym
    val c: Double = (sx2y * sxx - sxy * sxx2) / (sxx * sx2x2 - sxx2 * sxx2)
    val b: Double = (sxy * sx2x2 - sx2y * sxx2) / (sxx * sx2x2 - sxx2 * sxx2)
    val a: Double = ym - b * xm - c * x2m
    (a,b,c)
  }
  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/51659166/2d-interpolation-using-nth
  import scala.collection.JavaConverters._
  //---------------------------------------------------------------------------
  //(Polinomialfunction,(R2, std dev of residuals))
  def fit(degree: Int, xySeq: Seq[Point2D_Double], maxIterations: Int = 1000)  = {
    val fitter = PolynomialCurveFitter.create(degree)
                                       .withMaxIterations(maxIterations)
    val pointSeq = xySeq map( p=> new WeightedObservedPoint(1, p.x, p.y) )
    val fit = fitter.fit(pointSeq.asJava)
    val fittedPolinomy = new PolynomialFunction(fit)
    val seq = xySeq map ( _.toSequence())
    val predictedValueSeq = seq map (p=> fittedPolinomy.value(p(0)))
    (fittedPolinomy, getGoodnessOfFit(seq, predictedValueSeq))
  }
  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/49349580/calculate-r-square-for-polynomialfit-in-apache-commons-math3
  //(r,std dev of the residuals)
  def getGoodnessOfFit(pointList: Seq[Seq[Double]], predictedValueSeq: Seq[Double]) = {
    val predictedY_ValueSeq = ArrayBuffer[Double]()
    var residualSumOfSquaresY = 0d
    val descriptiveStatisticsY = new DescriptiveStatistics

    pointList.zipWithIndex foreach { case (p,i) =>
      val predictedValueY = predictedValueSeq(i)
      val actualValueY = p(1)
      val z = predictedValueY - actualValueY
      residualSumOfSquaresY += (z * z)
      descriptiveStatisticsY.addValue(actualValueY)
      predictedY_ValueSeq += predictedValueY
    }
    var totalSumOfSquaresY = 0d
    val averageActualY_ValueSeq = descriptiveStatisticsY.getMean
    predictedY_ValueSeq foreach { predictedValueY =>
      val z = predictedValueY - averageActualY_ValueSeq
      totalSumOfSquaresY += z * z}

    val stdDevResiduals = Math.sqrt(residualSumOfSquaresY / predictedY_ValueSeq.length)
    (1.0 - (residualSumOfSquaresY / totalSumOfSquaresY), stdDevResiduals)
  }
  //---------------------------------------------------------------------------
  def evaluatePolynomy(coefSeq : Seq[Double], x: Double) = {
    var r: Double = 0
    var k = 0
    for(degree<-coefSeq.length-1 to 0 by -1) {
      r += coefSeq(k) * Math.pow(x,degree)
      k += 1
    }
    r
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Regression.scala
//=============================================================================