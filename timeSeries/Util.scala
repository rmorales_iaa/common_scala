/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.common.timeSeries
//=============================================================================
import com.common.math.complexNumber.ComplexNumber
import com.common.logger.MyLogger
import com.common.math.MyMath
//=============================================================================
import org.apache.commons.math3.complex.Complex
import org.apache.commons.math3.transform.{DftNormalization, FastFourierTransformer, TransformType}
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object Util extends MyLogger {
  //---------------------------------------------------------------------------
  //Extirpolate the values (x, y) onto an integer grid range(N), using lagrange polynomial weights on the M nearest points.
  //from python3 function 'extirpolate'
  def extirpolate(x: Array[Double]   // array of abscissas
                  , y: Array[ComplexNumber] //array of ordinates
                  , _N: Int // number of integer bins to use.
                  , M: Int //number of adjoining points on which to extirpolate
         ) = {

    //Now use legendre polynomial weights to populate the results array;
    //This is an efficient recursive implementation (See Press et al. 1989)
    val N = if (_N == 0) (x.max + 0.5 * M + 1).toInt else _N
    val result = ArrayBuffer[ComplexNumber]()
    result ++= (for(_<-0 until N) yield ComplexNumber())

    //first take care of the easy cases where x is an integer
    val xIsInteger = x.zipWithIndex.flatMap{ case (_x,i) =>  if (_x % 1 == 0) Some((_x.toInt,i)) else None }
    xIsInteger.foreach{ case(i,j) => result(i) = y(j) }
    val xIntegerPos = xIsInteger map (_._2)

    //filter the intergers positions
    val _x = x.zipWithIndex.flatMap{ case (a,i)=> if(!xIntegerPos.contains(i)) Some(a) else None }
    val _y = y.zipWithIndex.flatMap{ case (b,i)=> if(!xIntegerPos.contains(i)) Some(b) else None }

    //clipping within the range 0...N
    val xCentre = Math.floor(M/2)
    val xMin = 0
    val xMax = N - M
    val iLower = _x map { v =>
      val c = (v - xCentre).toInt
      if (c <= xMin) xMin
      else
        if (c >= xMax) xMax
        else c
    }

    val base = (_x zip iLower)map { case (v,intV) =>  v - intV }
    val numerator = (base zip _y) map { case (b,y) =>
      y * (for(i<-0 until M) yield b - i).toArray.foldLeft(1d) (_ * _)
    }
    var denominator = MyMath.factorial(M - 1)

    for (j<-0 until M){
      if (j > 0) denominator = (denominator *  j) / (j - M)
      val ind = iLower map ( _ + (M - 1 - j))
      val divSeq = (numerator zip _x).zipWithIndex.map {case ((n,x),i) => n / (denominator * (x - ind(i))) }
      var k = 0
      for (i<- ind) {
         result(i) = result(i) + divSeq(k)
         k += 1
      }
    }
    result.toArray
  }
  //-------------------------------------- -------------------------------------
  //Compute (approximate) trigonometric sums for a number of frequencies
  //from python3 function 'trig_sum'
  def trigSum(_t: Array[Double]          // array of input times
              , _h: Array[Double]        // array weights for the sum
              , _df: Double              // frequency spacing
              , N: Int                  // number of frequency bins to return
              , _f0: Double = 0          // The low frequency to use
              , freq_factor: Int = 1    // Factor which multiplies the frequency
              , use_fft: Boolean = true // if True, use the approximate FFT algorithm to compute the result. This uses the FFT with Press & Rybicki's Lagrangian extirpolation.
              , oversampling: Int= 5    //roughly the number of time samples across the highest-frequency sinusoid. Not referenced if use_fft is False.
              , Mfft: Int = 4           //The number of adjacent points to use in the FFT approximation. Not referenced if use_fft is False
             ): Option[(Array[Double],Array[Double])] = {

    val df = _df * freq_factor
    val f0 = _f0 * freq_factor

    if(df < 0) {error(s"trigSum.Value: df=$df must be positive"); return None}
    if (!use_fft) {
      val k = (for(i<-0 until N) yield {2 * Math.PI * (f0 + df * i)}).toArray
      val base = _t map { v=> k map ( _ * v) }
      val baseCos = base.map{ a=> a.map(Math.cos(_))}
      val baseSin = base.map{ a=> a.map(Math.sin(_))}

      val baseInnerSize = base.head.length
      val C = (for(i<-0 until baseInnerSize) yield {
        val a = baseCos map ( _ (i))
        ((a zip _h) map {case (x,y)=> x * y}).sum
      }).toArray

      val S = (for(i<-0 until baseInnerSize) yield {
        val a = baseSin map ( _ (i))
        ((a zip _h) map {case (x,y)=> x * y}).sum
      }).toArray
      return Some((C,S))
    }

    val Nfft = MyMath.bitCeil(N * oversampling)
    val t0 = _t.min
    if(t0 == 0) {error(s"trigSum.Value: t0=$t0 (min value) must be non zero"); return None}
    val cn = new ComplexNumber(0,2) * Math.PI * f0
    val tCentred = _t map (_ - t0)
    val h = (tCentred zip _h) map { case (t,h) => (cn * t).exp() * h }
    val tNorm = tCentred map (v=> (v * Nfft * df) % Nfft)
    val grid = extirpolate(tNorm,h,Nfft,Mfft)

    //compute the one-dimensional inverse discrete Fourier Transform.
    val gridAsJavaComplexSeq = grid map ( c=> new Complex(c.real,c.imaginary) )
    val fastFourierTransformer = new FastFourierTransformer(DftNormalization.STANDARD)
    val fftgrid = fastFourierTransformer.transform(gridAsJavaComplexSeq, TransformType.INVERSE)
    val r = (for(i<-0 until N) yield {
      val c = ComplexNumber(0, 2 * Math.PI * t0 * ( f0 + df  * i)).exp
      ComplexNumber(fftgrid(i).getReal,fftgrid(i).getImaginary) * c * Nfft
    }).toArray
    Some((r.map(_.imaginary),r.map(_.real)))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Util.scala
//=============================================================================
