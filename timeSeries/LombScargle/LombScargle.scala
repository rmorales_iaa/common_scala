/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  22/Apr/2022
 * Time:  16h:38m
 * Description: ported from: python3 LombScargle algorithm (fast version)
 *
 */
//=============================================================================
package com.common.timeSeries.LombScargle
//=============================================================================
import com.common.logger.MyLogger
import com.common.timeSeries.Util._
//=============================================================================
//=============================================================================
object LombScargle extends MyLogger {
  //---------------------------------------------------------------------------
  sealed trait NORMALIZATION_TYPE
  //---------------------------------------------------------------------------
  case object NORMALILATION_STANDARD  extends NORMALIZATION_TYPE
  case object NORMALILATION_MODEL     extends NORMALIZATION_TYPE
  case object NORMALILATION_LOG       extends NORMALIZATION_TYPE
  case object NORMALILATION_PSD       extends NORMALIZATION_TYPE
  //---------------------------------------------------------------------------
  //(frequency,spectral_power)
  def calculate(tSeq: Array[Double] //observing time
                , vSeq: Array[Double] //observed value at the observing time
                , tErrorSeq: Array[Double] = Array[Double]() //error or sequence of observational errors associated with observing time
                , method: String = "fast" //specify the lomb scargle implementation to use. Fixed to 'fast' in this porting to Scala
                , freq_samples_per_peak: Int = 5 //The approximate number of desired samples across the typical peak
                , freq_nyquist_factor: Int = 5 //The multiple of the average nyquist frequency used to choose the maximum frequency if maximum_frequency is not provided.
                , freq_minimum_frequency: Option[Double] = None //If specified, then use this minimum frequency rather than one chosen based on the size of the baseline.
                , freq_maximum_frequency: Option[Double] = None //If specified, then use this maximum frequency rather than one chosen based on the average nyquist frequency.
                , freq_return_freq_limits: Boolean  = false //if True, return only the frequency limits rather than the full frequency grid
                , center_data : Boolean = true //if True, pre-center the data by subtracting the weighted mean of the input data. This is especially important if fit_mean = False
                , fit_mean: Boolean = true //if True, include a constant offset as part of the model at each frequency. This can lead to more accurate results, especially in thecase of incomplete phase coverage.
                , nterms: Int = 1 //number of terms to use in the Fourier fit
                , normalization: NORMALIZATION_TYPE = NORMALILATION_STANDARD //Normalization to use for the periodogram: "standard", "model", "log", "psd"
                , assume_regular_frequency: Boolean = true
    ): Option[(Array[Double],Array[Double])] = {  // Normalization to use for the periodogram.

    val freqSeq = autofrequency(tSeq, freq_samples_per_peak, freq_nyquist_factor, freq_minimum_frequency, freq_maximum_frequency, freq_return_freq_limits)
    if (!assume_regular_frequency) {
      error(s"LombScargle implemetation assume_regular_frequency='$assume_regular_frequency' is not implemented yet")
      return None
    }

    if (freqSeq.size < 2) {
      warning(s"LombScargle. Not enough frequencies:${freqSeq.length} to continue")
      return None
    }

    method match {
      case "fast" =>
        val power = lombScargleFast(tSeq, vSeq, tErrorSeq, freqSeq, center_data, fit_mean, normalization, assume_regular_frequency).getOrElse(return None)

        val filteredResult = (freqSeq zip power) flatMap { case (a, b) =>
            if (a.isNaN || a.isInfinity |
                b.isNaN || b.isInfinity) None
            else Some ((a, b) )
        }
        Some((filteredResult map (_._1), filteredResult map (_._2)))
      case _ =>       error(s"LombScargle implemetation '$method' is not implemented yet")
        None
    }
  }
  //---------------------------------------------------------------------------
  //compute the Lomb-Scargle power at the given frequencies
  private def lombScargleFast(tSeq: Array[Double]                     //observing time
                           , vSeq: Array[Double]
                           , tErrorSeq: Array[Double]
                           , freqSeq: Array[Double]
                           , center_data : Boolean
                           , fit_mean: Boolean
                           , normalization: NORMALIZATION_TYPE
                           , assume_regular_frequency: Boolean
                   ): Option[Array[Double]] = {
    val f0 = freqSeq(0)
    val df = freqSeq(1)-freqSeq(0)
    val Nf = freqSeq.size

    if(f0 < 0) {error("Frequencies must be positive"); return None}
    if(df <= 0) {error("Frequency steps must be positive"); return None}
    if(Nf <= 0) {error("Number of frequencies must be positive"); return None}

    val tError= if(tErrorSeq.isEmpty) Array.fill[Double](tSeq.length)(1) else  tErrorSeq
    var w = tError map (Math.pow(_,-2))
    val wSum = w.sum
    w = w map(_/wSum)
    val yCentre = ((w zip vSeq) map { case (e,y)=> (e * y)}).toArray.sum
    val y = if (center_data || fit_mean) vSeq map (_ - yCentre) else vSeq
    val yWeighted = (w zip y) map {case (_x,_y)=> _x * _y}

    // step 1. compute functions of the time-shift tau at each frequency
    val (sh,ch) = trigSum(tSeq, yWeighted, df, Nf, f0).getOrElse(return None)
    val (s2,c2) = trigSum(tSeq, w, df, Nf, f0, freq_factor = 2).getOrElse(return None)
    var S : Array[Double] = Array()
    var C : Array[Double] = Array()
    val tan_2omega_tau =
      if (fit_mean) {
        val r  = trigSum(tSeq, w, df, Nf, f0).getOrElse(return None)
        S = r._1
        C = r._2
        (for(i<-0 until S.length) yield
          (s2(i) - 2 * S(i) * C(i)) / (c2(i) - (C(i) * C(i) - S(i) * S(i)))).toArray
      }
      else
        (for(i<-0 until s2.length) yield { s2(i) / c2(i)}).toArray

    //compute:
    //omega_tau = 0.5 * np.arctan(tan_2omega_tau)
    //S2w, C2w = np.sin(2 * omega_tau), np.cos(2 * omega_tau)
    //Sw, Cw = np.sin(omega_tau), np.cos(omega_tau)
    val sqrt0_5 = Math.sqrt(0.5)
    val S2w = tan_2omega_tau map { sw2=> sw2 / Math.sqrt(1 + sw2 * sw2)}
    val C2w = tan_2omega_tau map { c2w=> 1 / Math.sqrt(1 + c2w * c2w)}
    val Cw = C2w map { c2w=> sqrt0_5 * Math.sqrt(1 + c2w)}
    val Sw = (S2w zip C2w) map { case (s2w, c2w)=>
      val s2wSign = if (s2w < 0) -1 else if(s2w ==0) 0 else 1
      sqrt0_5 * s2wSign * Math.sqrt(1 - c2w)
    }

    //Compute the periodogram, following Zechmeister & Kurster and using tricks from Press & Rybicki
    val YY = ((w zip y) map {case (_w,_y)=> _w * _y * _y}).sum
    val YC = (for(i<-0 until ch.length) yield ch(i) * Cw(i) + sh(i) * Sw(i)).toArray
    val YS = (for(i<-0 until sh.length) yield sh(i) * Cw(i) - ch(i) * Sw(i)).toArray
    var CC = (for(i<-0 until c2.length) yield 0.5 * (1 + c2(i) * C2w(i) + s2(i) * S2w(i))).toArray
    var SS = (for(i<-0 until c2.length) yield 0.5 * (1 - c2(i) * C2w(i) - s2(i) * S2w(i))).toArray

    if (fit_mean) {
      CC = (for(i<-0 until C.length) yield {
        val v = C(i) * Cw(i) + S(i) * Sw(i)
        CC(i) - (v * v)
      }).toArray

      SS = (for(i<-0 until S.length) yield {
        val v = S(i) * Cw(i) - C(i) * Sw(i)
        SS(i) - (v * v)
      }).toArray
    }
    val power = (for(i<-0 until YC.length) yield YC(i) * YC(i) / CC(i) + YS(i) * YS(i) / SS(i)).toArray
    Some(normalization match {
      case NORMALILATION_STANDARD => (for(i<-0 until power.length) yield power(i) / YY).toArray
      case NORMALILATION_MODEL    => (for(i<-0 until power.length) yield power(i) / (YY - power(i))).toArray
      case NORMALILATION_LOG      => (for(i<-0 until power.length) yield -Math.log(1 - power(i) / YY)).toArray
      case NORMALILATION_PSD      =>
        val k = (for(i<-0 until tError.length) yield Math.pow(tError(i),-2)).toArray.sum
        (for(i<-0 until power.length) yield power(i) * 0.5 * k).toArray
    })
  }
  //---------------------------------------------------------------------------
  //get The heuristically-determined optimal frequency bin
  def autofrequency(tSeq: Array[Double]
                    , freq_samples_per_peak: Int = 5
                    , freq_nyquist_factor: Int = 5
                    , freq_minimum_frequency: Option[Double] = None
                    , freq_maximum_frequency: Option[Double] = None
                    , freq_return_freq_limits: Boolean
                   ) = {
    val baseline = tSeq.max - tSeq.min
    val nSamples = tSeq.length
    val df = 1.0 / baseline / freq_samples_per_peak
    val minimum_frequency = freq_minimum_frequency.getOrElse(0.5 * df)
    val maximum_frequency = freq_maximum_frequency.getOrElse(freq_nyquist_factor *  (0.5 * nSamples / baseline) )
    val freqNumber = (1 + Math.round((maximum_frequency - minimum_frequency) / df)).toInt
    if (freq_return_freq_limits) Array(minimum_frequency, minimum_frequency + df * (freqNumber - 1))
    else Array.tabulate(freqNumber)(n=> minimum_frequency + (n * df))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file LombScargle.scala
//=============================================================================
