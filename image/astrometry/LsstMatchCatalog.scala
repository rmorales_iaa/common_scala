//=============================================================================
package com.common.image.astrometry
//=============================================================================
import com.common.configuration.MyConf
import com.common.geometry.point.Point2D_Double
//=============================================================================
import java.lang.Math._
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object LsstMatchCatalog {
  //---------------------------------------------------------------------------
  private final val LSST_MATCH_Radius  = MyConf.c.getInt("Astrometry.fitWcs.lsst.matchRadiusMas")
  //---------------------------------------------------------------------------
  private def getCartesianCoordinates(raDec: Point2D_Double) = {
    val raDecAsRadians = raDec.toRadians()
    val cosDec = cos(raDecAsRadians.y)
    (raDecAsRadians.x
      , raDecAsRadians.y
      , cos(raDecAsRadians.x) * cosDec
      , sin(raDecAsRadians.x) * cosDec
      , sin(raDecAsRadians.y))
  }
  //---------------------------------------------------------------------------
  case class SourceMatch(cartA: CartesianPos
                         , cartB: CartesianPos
                         , d: Double)
  //---------------------------------------------------------------------------
  case class CartesianPos(pixPos: Point2D_Double, raDec: Point2D_Double) {
    val (raAsRadians, decAsRadians, x, y, z) = getCartesianCoordinates(raDec)
    //-------------------------------------------------------------------------
    def getDistance = x * x + y * y + z * z

    //-------------------------------------------------------------------------
    def getDistance(c: CartesianPos) = {
      val dx = x - c.x
      val dy = y - c.y
      val dz = z - c.z
      dx * dx + dy * dy + dz * dz
    }

    //-------------------------------------------------------------------------
    def getRaDecAsRadians = Point2D_Double(raAsRadians, decAsRadians)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //original list function ""lsst::geom::Angle fromUnitSphereDistanceSquared(double d2)"
  private def fromUnitSphereDistanceSquared(d: Double) = 2 * asin(0.5 * sqrt(d))
  //---------------------------------------------------------------------------
  //original list function "lsst::afw::table fromUnitSphereDistanceSquared(double d2)"
  private def toUnitSphereDistanceSquared(d: Double) = 2 * (1 - cos(d))
  //---------------------------------------------------------------------------
  //original list function "lss::afw::table::matchRaDec(Cat const &cat,lsst::geom::Angle radius,MatchControl const &mc)"
  def calculate(catalogRaDecSeq: Array[Point2D_Double]
                , imageRaDecSeq: Array[Point2D_Double]
                , catalogPixPosSeq: Array[Point2D_Double]
                , imagePixPosSeq: Array[Point2D_Double]
                , radiusMas: Int = LSST_MATCH_Radius): Array[SourceMatch] = {
    //-------------------------------------------------------------------------
    val matchSeq = ArrayBuffer[SourceMatch]()
    //-------------------------------------------------------------------------
    val radiusAsRadians = radiusMas * (PI / (180.0 * 3.6e6))
    val d2Limit = toUnitSphereDistanceSquared(radiusAsRadians) //squared distance between two unit vectors separated by an angle
    val catalogCartesianSeq = ((catalogPixPosSeq zip catalogRaDecSeq) map { case (p, rd) => CartesianPos(p, rd) }).sortWith(_.decAsRadians < _.decAsRadians)
    val imageCartesianSeq = ((imagePixPosSeq zip imageRaDecSeq) map { case (p, rd) => CartesianPos(p, rd) }).sortWith(_.decAsRadians < _.decAsRadians)
    val maxCatalogItem = catalogCartesianSeq.length
    //-------------------------------------------------------------------------
    var startIndex = 0
    catalogCartesianSeq.foreach { catCart =>
      val minDec = catCart.decAsRadians - radiusAsRadians
      val maxDec = catCart.decAsRadians + radiusAsRadians

      for (i <- startIndex until maxCatalogItem)
        if (imageCartesianSeq(i).decAsRadians < minDec) startIndex += 1

      if (startIndex == maxCatalogItem) return matchSeq.toArray

      //-----------------------------------------------------------------------
      def findBestMatch(): Unit = {
        for (k <- startIndex until maxCatalogItem) {
          val imageCart = imageCartesianSeq(k)
          if (imageCart.decAsRadians > maxDec) return
          val d2 = catCart.getDistance(imageCart)
          if (d2 < d2Limit) { //find only the closest
            matchSeq += SourceMatch(catCart
              , imageCart
              , fromUnitSphereDistanceSquared(d2))
            return
          }
        }
      }
      //-----------------------------------------------------------------------
      findBestMatch
    }
    matchSeq.toArray
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file LsstMatchCatalog.scala
//=============================================================================