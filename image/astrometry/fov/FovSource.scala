/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  07/Apr/2022
 * Time:  10h:20m
 * Description: None
 */
//=============================================================================
package com.common.image.astrometry.fov
//=============================================================================
import com.common.dataType.tree.kdTree.K2d_TreeDouble
import com.common.geometry.point.Point2D_Double
import com.common.image.catalog.{ImageCatalog, SourceDB_Item}
import com.common.image.photometry.absolute.photometricModel.mpo.MPO_EstMagDB_Item
import com.common.image.photometry.absolute.photometricModel.ImageFocusDB_Item
import com.common.image.photometry.magnitudeEstimation.MagnitudeEstimation
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object FovSource {
  //---------------------------------------------------------------------------
  private val sep = "\t"
  //---------------------------------------------------------------------------
  private val CSV_COLUMN_NAME = Seq(
    "m2_id"
    , "focus_id"
    , "gaia_id"
    , "x_pix"
    , "y_pix"
    , "ra"
    , "dec"
    , "flux_per_second"
    , "normalized_est_mag"
    , "snr"
    , "est_mag"
    , "est_relative"
  )
  //---------------------------------------------------------------------------
  def build(imageDB: ImageCatalog,
            imageName: String): Array[FovSource] =
    imageDB.getSourceSeq(imageName).map { source => FovSource(source,List[MagnitudeEstimation]()) }
  //---------------------------------------------------------------------------
  def apply(source: SourceDB_Item, imageFocus: ImageFocusDB_Item): FovSource =
    FovSource(source, imageFocus.magnitude_estimation_seq)
  //---------------------------------------------------------------------------
  def apply(source: SourceDB_Item,
            est_mag_seq: List[MagnitudeEstimation]): FovSource =
    FovSource(m2_id = source.m2_id
      , focus_id = source.focus_id
      , gaia_id = source.gaia_id
      , x_pix = source.x_pix
      , y_pix = source.y_pix
      , ra = source.ra
      , dec = source.dec
      , flux_per_second = source.flux_per_second
      , normalized_est_mag = Double.NaN
      , snr = source.snr
      , est_mag_seq)
  //---------------------------------------------------------------------------
  def saveAsCSV(csvName: String
                , seq: Array[FovSource]
                , estMagID: String
                , imageNameSeq: Array[String] = Array()) = {
    val bw = new BufferedWriter(new FileWriter(new File(csvName)))
    var header = CSV_COLUMN_NAME.mkString(sep)
    header = if (imageNameSeq.isEmpty) header else header + sep + "image_name"
    bw.write(header + "\n")
    seq.zipWithIndex.foreach{ case (s,i)  =>
      if (imageNameSeq.isEmpty) bw.write(s.toCsvLine(estMagID) + "\n")
      else bw.write(s.toCsvLine(estMagID) + sep + imageNameSeq(i) + "\n")
    }
    bw.close()
  }
  //---------------------------------------------------------------------------
  def buildTree(sourceSeq: Seq[FovSource]) = {
    val kdTree = K2d_TreeDouble()
    val map = scala.collection.mutable.Map[Int, FovSource]()
    val seq = sourceSeq.map { s =>
      map(s.m2_id) = s
      (s.getRaDec().toSequence(), s.m2_id)
    }
    kdTree.addSeq(seq)
    (kdTree,map)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import FovSource._
case class FovSource(m2_id: Int
                     , focus_id: String
                     , gaia_id: Long
                     , x_pix: Double
                     , y_pix: Double
                     , ra: Double //decimal degree
                     , dec: Double //decimal degree
                     , var flux_per_second: Double
                     , var normalized_est_mag: Double
                     , snr: Double
                     , var est_mag_seq: List[MagnitudeEstimation] = List[MagnitudeEstimation]()
                     , var est_relative_flux: Double = Double.NaN
                     , var absolute_est_mag: Double = Double.NaN
                     , estMagID: String = ""
                    ) {
  //---------------------------------------------------------------------------
  def simpleClone(): FovSource = {
    new FovSource(
        m2_id = this.m2_id
      , focus_id = this.focus_id
      , gaia_id = this.gaia_id
      , x_pix = this.x_pix
      , y_pix = this.y_pix
      , ra = this.ra
      , dec = this.dec
      , flux_per_second = this.flux_per_second
      , normalized_est_mag = this.normalized_est_mag
      , snr = this.snr
      , est_mag_seq = this.est_mag_seq
      , est_relative_flux = this.est_relative_flux
      , absolute_est_mag = this.absolute_est_mag
      , estMagID = this.estMagID
    )
  }
  //---------------------------------------------------------------------------
  val isFocus = !focus_id.isEmpty && focus_id.toLowerCase != "none"
  //---------------------------------------------------------------------------
  var estimatedMagnitudeReference = 0d
  //---------------------------------------------------------------------------
  def getCentroidPix() = Point2D_Double(x_pix,y_pix)
  //---------------------------------------------------------------------------
  def getRaDec() = Point2D_Double(ra,dec)
  //---------------------------------------------------------------------------
  def hasEstMag(estMagID: String) =
    !est_mag_seq.filter( _.getID == estMagID).isEmpty
  //---------------------------------------------------------------------------
  def isValidEstMag(estMagID: String) = !getEstMag(estMagID).isNaN
  //---------------------------------------------------------------------------
  def getEstMag(estMagID: String,useAbsoluteMag: Boolean = false): Double = {
    val (catalog, catalogFilter) = MagnitudeEstimation.getCatalogAndFilter(estMagID)
    if (est_mag_seq.isEmpty) Double.NaN //it means that was not possible to create a polyomial fitting
    else {
      val estSeq = est_mag_seq.filter { estMag =>
        (estMag.catalog == catalog) &&
        (estMag.catalog_filter == catalogFilter)
      }
      if (estSeq.isEmpty) Double.NaN
      else {
        if (useAbsoluteMag) estSeq.head.asInstanceOf[MPO_EstMagDB_Item].est_absolute_magnitude
        else estSeq.head.est_magnitude
      }
    }
  }
  //---------------------------------------------------------------------------
  def updateEstMag(estMagID: String, newEstaMagValue: Double) = {
    val (catalog, catalogFilter) = MagnitudeEstimation.getCatalogAndFilter(estMagID)
    est_mag_seq.filter { me =>
      (me.catalog == catalog) &&
        (me.catalog_filter == catalogFilter)
    }.head.est_magnitude = newEstaMagValue
  }
  //---------------------------------------------------------------------------
  def toCsvLine(estMagID:String) =
    m2_id + sep +
      focus_id + sep +
      gaia_id + sep +
      x_pix+ sep +
      y_pix+ sep +
      ra + sep +
      dec + sep +
      flux_per_second + sep +
      normalized_est_mag + sep +
      snr + sep +
      MagnitudeEstimation.getEstMag(est_mag_seq, estMagID) + sep +
      (if (isFocus) 0 else est_relative_flux)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FovSource.scala
//=============================================================================
