/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/May/2022
 * Time:  17h:30m
 * Description: None
 */
//=============================================================================
package com.common.image.astrometry.fov
//=============================================================================
import com.common.coordinate.conversion.Conversion
import com.common.geometry.point.Point2D_Double
import com.common.geometry.rectangle.RectangleDouble
import com.common.hardware.cpu.CPU
import com.common.image.astrometry.fov.FovSeq.saveRaw
import com.common.image.catalog.ImageCatalog
import com.common.image.focusType.ImageFocusTrait
import com.common.image.myImage.MyImage
import com.common.image.photometry.absolute.photometricModel.ImageFocusDB
import com.common.image.photometry.absolute.photometricModel.mpo.MPO_CatalogDB
import com.common.image.photometry.absolute.photometricModel.star.StarCatalogDB
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
import com.common.util.time.Time
//=============================================================================
import scala.collection.mutable.ArrayBuffer
import java.io.{BufferedWriter, File, FileWriter}
import java.time.temporal.ChronoUnit
//=============================================================================
//=============================================================================
object CommonFov extends MyLogger {
  //---------------------------------------------------------------------------
  val DEFAULT_OUTPUT_COMMON_FOV_DIR            = "common_fov"
  val DEFAULT_OUTPUT_COMMON_SOURCE_EVOL_DIR    = "common_source_evolution"
  private val SUMMARY_CSV_FILE_NAME            = "summary.csv"
  //---------------------------------------------------------------------------
  val sep = "\t"
  //---------------------------------------------------------------------------
  private def getFovSeqNotInSolution(fovMap: Map[Int,Fov]
                                     , solutionSeq: Array[FovSeq]) = {
    val imageSeqInSolutionSeq = (solutionSeq flatMap (solution=> solution.seq.map(_.id))).toSet
    val allIdSet = fovMap.values.map(_.id).toSet
    val imageNotInSolution = allIdSet.diff(imageSeqInSolutionSeq)
    (FovSeq(imageNotInSolution.map{ id => fovMap(id) }.toArray), imageSeqInSolutionSeq.size)
  }
  //---------------------------------------------------------------------------
  private def getFov(imageFocus: ImageFocusTrait
                     , fovID: Int
                     , imageName: String
                     , imageDB: ImageCatalog
                     , focusDB: ImageFocusDB) = {

    val img = imageDB.get(imageName)
    val rec = RectangleDouble(Point2D_Double(img.ra_min,img.dec_min),Point2D_Double(img.ra_max,img.dec_max))
    val julianDate = if (imageFocus.isStar) img.julian_date_mid_point
    else focusDB.asInstanceOf[MPO_CatalogDB].getJuliandDateCorrected(imageName)

    val (_, _, _, _, _, _, _, _, objectName) = MyImage.getMetadaFromImageName(MyFile.getFileNameNoPathNoExtension(imageName))

    Fov(fovID
      , imageName
      , rec
      , Time.fromJulian(julianDate)
      , img.telescope
      , img.filter
      , img.instrument
      , img.exposure_time
      , img.x_pix_size
      , img.y_pix_size
      , img.ra_fov
      , img.dec_fov
      , img.x_pix_scale
      , img.y_pix_scale
      , objectName
    )
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import CommonFov._
case class CommonFov(imageFocus: ImageFocusTrait
                     , fovIntersectionPercentage: Double
                     , minImagesPerSolution: Int
                     , minCommonSource: Int
                     , photometryRootPath: String
                     , debugStorage : Boolean = false
                     , verbose: Boolean) extends MyLogger {
  //---------------------------------------------------------------------------
  private val objectName =  imageFocus.getID
  private val collectionName =  if (debugStorage) imageFocus.getDebuggingID else imageFocus.getID
  private val oDir =  Path.resetDirectory(s"$photometryRootPath/$DEFAULT_OUTPUT_COMMON_FOV_DIR/")
  processFilterSeq()
  //---------------------------------------------------------------------------
  private def createSummaryFile(summaryFileName:String) = {
    val writeHeader = !MyFile.fileExist(summaryFileName)
    val bw = new BufferedWriter(new FileWriter(new File(summaryFileName)))
    if (writeHeader) bw.write(s"image_filter${sep}image_count${sep}solution_count${sep}images_in_any_solution${sep}images_not_in_solution\n")
    bw
  }
  //---------------------------------------------------------------------------
  private def processFilterSeq() = {
    info(s"Loading the sequence of images for object: '$objectName'")

    val imageDB = ImageCatalog(collectionName)
    val focusDB = if(imageFocus.isStar) StarCatalogDB(collectionName) else MPO_CatalogDB(collectionName)
    val imageByFilterMap = imageDB.getNameSeqGroupByFilterMap(focusDB.getImageNameMap())
    val summaryFile: BufferedWriter =  createSummaryFile(oDir + SUMMARY_CSV_FILE_NAME)

    for ((imageFilter, imageNameSeq) <- imageByFilterMap) {
      var fovID = 0
      info(s"Getting the common field of view for object: '$objectName' and image filter: '$imageFilter' with: ${imageNameSeq.size} images")

      //process the fov seq for the current filter
      val fovSeq = (
        for(imageName <-imageNameSeq) yield {
          fovID += 1
          getFov(imageFocus, fovID, imageName, imageDB, focusDB)
        }).toArray

      if (fovSeq.isEmpty)
        error(s"There is no valid images to calculate the differential photometry on object: '$objectName' and filter: '$imageFilter'")
      else {
        //process the fov seq for the current filter
        val outputDir = Path.resetDirectory(s"$oDir/$imageFilter")
        processFilter(imageFilter, fovSeq, summaryFile, outputDir)
      }
    }
    summaryFile.close()
    imageDB.close()
    focusDB.close()
  }
  //---------------------------------------------------------------------------
  private def processFilter(imageFilter: String
                            , fovSeq: Array[Fov]
                            , summaryFile: BufferedWriter
                            , outputDir: String) = {

    info(s"Processing ${fovSeq.size} images for object: '$objectName' and filter: '$imageFilter'")
    if (fovSeq.size == 0) error(s"No images for '$objectName' and filter: '$imageFilter' ")

    val minRa  = Conversion.arcoMinToDecimalDegree((fovSeq map ( _.raFov)).min) /  (100d / fovIntersectionPercentage)
    val minDec = Conversion.arcoMinToDecimalDegree((fovSeq map (_.decFov)).min) /  (100d / fovIntersectionPercentage)
    info(s"Min right ascension intersection            : ${f"$minRa%.3f"} (dec deg)")
    info(s"Min declination intersection                : ${f"$minDec%.3f"} (dec deg)")

    val fovMap = (fovSeq map (fov=> (fov.id,fov))).toMap

    val initialSolutionSeq = processFovMap(
      fovMap
      , minRa
      , minDec
      , outputDir + "/discarded_solutions_initial/")

    val refinedSolution = refineSolutionSeq(initialSolutionSeq, outputDir, minRa, minDec, imageFilter)

    //refine each solution individually
    val finalSolutionSeq = (refinedSolution map { fovSeq =>
      refineSolutionSeq(Array(fovSeq), outputDir, minRa, minDec,imageFilter) }).flatten

    //calculate images images present in a solution
    val (finalImagesNotInSolutionFovSeq, finalImageSeqInSolutionCount) =
      getFovSeqNotInSolution(fovMap, finalSolutionSeq)

    //save summary
    saveSummary(imageFilter
      , summaryFile
      , fovMap
      , finalSolutionSeq
      , finalImageSeqInSolutionCount)

    //save images not present in any solutions
    val noInSolutionsDir = if (fovMap.isEmpty) "" else Path.resetDirectory(outputDir + "discarded_images")
    if (!fovMap.isEmpty) {

      saveRaw(noInSolutionsDir + "image_seq.csv", finalImagesNotInSolutionFovSeq.seq.toArray)

      //all
      info("Retrieving all images from all valid solutions")
      saveRaw(outputDir + "image_seq.csv", finalSolutionSeq.map(_.seq).flatten)

      if (!finalSolutionSeq.isEmpty) saveSolutionSeq(finalSolutionSeq, outputDir, remoteHost = None)
      finalSolutionSeq.zipWithIndex.foreach { case (fovSeq, i) => fovSeq.isValid(minRa, minDec, i) }
    }
  }
  //---------------------------------------------------------------------------
  private def saveSummary(imageFilter: String
                          , summaryFile: BufferedWriter
                          , fovMap: Map[Int,Fov]
                          , finalSolutionSeq: Array[FovSeq]
                          , finalImageSeqInSolutionCount: Int) = {
    summaryFile.write(
      imageFilter + sep +
        fovMap.size + sep +
        finalSolutionSeq.size + sep +
        finalImageSeqInSolutionCount + sep + {
        fovMap.size - finalImageSeqInSolutionCount
      } + sep + "\n")

    //summary
    info(s"Final solution summary for image filter        : '$imageFilter'")
    info(s"\tImage filter                                : '$imageFilter'")
    info(s"\tNumber of total images                      : ${fovMap.size}")
    info(s"\tNumber of solutions                         : ${finalSolutionSeq.size}")
    info(s"\tNumber of images stored in a solution       : $finalImageSeqInSolutionCount")
    info(s"\tNumber of images not stored in any solution : ${fovMap.size - finalImageSeqInSolutionCount}")
  }
  //---------------------------------------------------------------------------
  private def saveSolutionSeq(solutionSeq: Array[FovSeq], oDir: String, remoteHost: Option[String]) = {

    //save solutions checking its validity before
    val bw = new BufferedWriter(new FileWriter(new File(oDir + "solution_seq.csv")))
    bw.write(s"solution_name${sep}image_count${sep}minRa${sep}minDec${sep}maxRa${sep}maxDec${sep}area(decimal_degree)${sep}intersection_area_percentage${sep}min_date${sep}max_date${sep}elapsed_hours" + "\n")

    //get the max rectangle
    val templateFov = solutionSeq.head.seq.head
    val raRange = Conversion.pixToDecimalDegree(templateFov.xMaxPix, templateFov.pixScaleX)
    val decRange = Conversion.pixToDecimalDegree(templateFov.yMaxPix, templateFov.pixScaleY)
    val maxRecArea = raRange * decRange

    solutionSeq
      .sortWith(_.size() > _.size())
      .zipWithIndex.foreach { case (solution, i) =>
      val solutionName = s"solution_${f"$i%02d"}"
      val rec = solution.getIntersection().get
      val intersectionAreaPercentage = f"${((rec.width * rec.height) / maxRecArea) * 100}%.3f"

      val minDate = solution.seq.head.observingDate
      val maxDate = solution.seq.last.observingDate
      val elapsedHours = ChronoUnit.HOURS.between(minDate, maxDate)

      bw.write(s"$solutionName$sep${solution.size}$sep${rec.min.x}$sep${rec.min.y}$sep${rec.max.x}$sep${rec.max.y}$sep${rec.area}" +
        s"$sep$intersectionAreaPercentage$sep$minDate$sep$maxDate$sep$elapsedHours" + "\n")
      solution.save(oDir + s"/$solutionName")
    }
    bw.close()
  }
  //---------------------------------------------------------------------------
  private def tryToAddFovToSolution(solutionSeq: Array[FovSeq]
                                    , fovSeq: FovSeq
                                    , minRa: Double
                                    , minDec: Double) = {
    val solRecSeq = solutionSeq.map {
      _.getIntersection().get
    }
    //find the max intersection beteween fov and a solution
    val remainFovNotInSolution = fovSeq.seq.flatMap { fov =>
      val rec = fov.rec
      val solIntSeq = solRecSeq.zipWithIndex.flatMap { case (solRec, i) =>
        val intRec = solRec.getIntersection(rec)
        if (intRec.isEmpty) None
        else {
          val sol = solutionSeq(i)
          val newSol = FovSeq(sol.seq += fov)
          if (!newSol.isValid(minRa, minDec, i, reportError = false)) None
          else Some((sol, intRec.get.area))
        }
      }
      if (!solIntSeq.isEmpty) {
        val sol = solIntSeq.sortWith(_._2 > _._2).head._1 //get the solution with greater interserction with fov
        sol.seq += fov
        None
      }
      else Some(fov) //fov can not be added to any solution
    }
    FovSeq(remainFovNotInSolution)
  }
  //---------------------------------------------------------------------------
  private def refineSolutionSeq(_solSeq: Array[FovSeq]
                                , oDir: String
                                , minRa:Double
                                , minDec: Double
                                , imageFilter: String) = {
    //-------------------------------------------------------------------------
    var iteration = 0
    //-------------------------------------------------------------------------
    def refine(solSeq: Array[FovSeq]) = {
      val solMap = solSeq.map(_.getMap()).flatten.toMap

      val iterationString = s"${f"$iteration%02d"}"
      val (imagesNotInSolutionFovSeq, _) = getFovSeqNotInSolution(solMap, solSeq)

      //try to append the images not present in a solution (included the ones come from discarded groups) to one present solution
      val nextImagesNotInSolutionFovSeq = tryToAddFovToSolution(solSeq, imagesNotInSolutionFovSeq, minRa, minDec)

      //try to build a new solution with images not present in any solution
      val newSolutionSeq = processFovMapSeq(nextImagesNotInSolutionFovSeq.getMap()
        , minRa
        , minDec
        , oDir + s"/discarded_solutions_$iterationString/")

      val finalSolutionSeq = solSeq ++ newSolutionSeq
      val finalImageSeqInSolutionCount = finalSolutionSeq.map(_.size()).sum

      info(s"Solution summary in iteration: $iterationString image filter : '$imageFilter'")
      info(s"\tNumber of total images                      : ${solMap.size}")
      info(s"\tNumber of solutions                         : ${finalSolutionSeq.size}")
      info(s"\tNumber of images stored in a solution       : $finalImageSeqInSolutionCount")
      info(s"\tNumber of images not stored in any solution : ${solMap.size - finalImageSeqInSolutionCount}")
      finalSolutionSeq
    }

    //-------------------------------------------------------------------------
    var continue = true
    var solSeq = _solSeq
    var previousSolSize = 0
    var previousSolImageSize = 0
    while (continue) {
      previousSolSize = solSeq.size
      previousSolImageSize = solSeq.map(_.size()).sum
      val lastSolSeq = refine(solSeq)
      if ((lastSolSeq.size == previousSolSize) &&
        (solSeq.map(_.size()).sum == previousSolImageSize)) continue = false
      else solSeq = lastSolSeq
      iteration += 1
    }
    solSeq
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private def processFovMapSeq(fovMap: Map[Int, Fov]
                               , minRa: Double
                               , minDec: Double
                               , discardedOutputDir: String) = {
    //-------------------------------------------------------------------------
    //filtering
    var continue = true
    var iteration = 0
    var candidateFovSeq = FovSeq(fovMap.values.toArray.sortWith { case (a, b) => a.observingDate.isBefore(b.observingDate) }) //this are the items used to grow
    val solutionSeq = ArrayBuffer[FovSeq]() //append as seed all the individual items and the try to grow from them
    var imagesNotInSolutionCount = 0

    //continue until no more solutions can be created
    while (continue) {

      //get the solutions with the current candidates
      val partialSolutionSeq = calculateCandidatesParallel(candidateFovSeq, minRa, minDec, iteration)

      //process partial result
      var remainSolutionSeq = partialSolutionSeq.filter(_.size >= minImagesPerSolution)
      info(s"Iteration: $iteration. Solution count before filtering solutions with at least $minImagesPerSolution images: ${remainSolutionSeq.length}")
      remainSolutionSeq = FovSeq.deduplicate(remainSolutionSeq).toArray
      info(s"Iteration: $iteration. Solution count after removing duplicates: ${remainSolutionSeq.length}")

      //append to final result
      solutionSeq ++= remainSolutionSeq
      val (fovSeqNotInSolution, _) = getFovSeqNotInSolution(fovMap, solutionSeq.toArray)
      val fovSeqNotInSolutionCount = fovSeqNotInSolution.size()
      info(s"Iteration: $iteration. Image count not present in any solution: $fovSeqNotInSolutionCount")

      //check whether stop
      if (fovSeqNotInSolution.isEmpty() ||
        (fovSeqNotInSolutionCount == imagesNotInSolutionCount))
        continue = false
      else candidateFovSeq = fovSeqNotInSolution //try to append the fov not present in any solution

      //update vars
      imagesNotInSolutionCount = fovSeqNotInSolutionCount
      iteration += 1
    }

    //merge results
    continue = true
    iteration = 0
    var solutionCount = solutionSeq.size
    while (continue) {
      //merge solutions
      info(s"Simplyfing results. Iteration: $iteration. Before applying merge on ${solutionSeq.length} solutions")
      merge(solutionSeq, minRa, minDec)
      info(s"Simplyfing results. Iteration: $iteration. After applying merge on ${solutionSeq.length} solutions")

      if (solutionSeq.size == solutionCount) continue = false
      else solutionCount = solutionSeq.size
      iteration += 1
    }

    //avoid to reuse images(fov) in different solutions.
    info(s"Before avoiding to reuse fov: ${solutionSeq.length} solutions")
    val usedFOV = scala.collection.mutable.Set[Int]()

    val finalSolutionSeq = solutionSeq
      .sortWith(_.seq.size > _.seq.size)
      .zipWithIndex.flatMap { case (fovSeq, i) =>
      val fovIdSet = fovSeq.getIdSet
      val notUsedFov = fovIdSet.diff(usedFOV)
      usedFOV ++= notUsedFov

      //calculate images not in any solution
      val newFovSeq = FovSeq(fovSeq.seq.filter(fov => notUsedFov.contains(fov.id)))
      if (newFovSeq.size < minImagesPerSolution) {
        val solutionName = s"solution_${f"$i%02d"}"
        val oDir = Path.resetDirectory(discardedOutputDir + s"$solutionName/")
        newFovSeq.save(oDir)
        None
      }
      else Some(newFovSeq.sortByDate())
    }

    info(s"After avoiding to reuse fov:  ${finalSolutionSeq.length} solutions")

    //check the validity of the groups
    finalSolutionSeq.zipWithIndex
      .filter { case (fovSeq, i) => fovSeq.isValid(minRa, minDec, i) }.map(_._1).toArray
  }
  //-------------------------------------------------------------------------
  private def merge(solutionSeq: ArrayBuffer[FovSeq]
                    , minRa: Double
                    , minDec: Double): Unit = {
    //-----------------------------------------------------------------------
    def canMerge(): Boolean = {
      for (fovSeqA <- solutionSeq) {
        solutionSeq.zipWithIndex.foreach { case (fovSeqB, i) =>
          if ((fovSeqA != fovSeqB) &&
            fovSeqA.tryToMerge(fovSeqB, minRa, minDec)) {
            solutionSeq.remove(solutionSeq.indexOf(fovSeqB))
            return true
          }
        }
      }
      false
    }
    //-----------------------------------------------------------------------
    while (canMerge) {}
    //-----------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private def processFovMap(fovMap: Map[Int, Fov]
                            , minRa: Double
                            , minDec: Double
                            , discardedOutputDir: String) = {
    //-------------------------------------------------------------------------
    //filtering
    var continue = true
    var iteration = 0
    var candidateFovSeq = FovSeq(fovMap.values.toArray.sortWith { case (a, b) => a.observingDate.isBefore(b.observingDate) }) //this are the items used to grow
    val solutionSeq = ArrayBuffer[FovSeq]() //append as seed all the individual items and the try to grow from them
    var imagesNotInSolutionCount = 0

    //continue until no more solutions can be created
    while (continue) {

      //get the solutions with the current candidates
      val partialSolutionSeq = calculateCandidatesParallel(candidateFovSeq, minRa, minDec, iteration)

      //process partial result
      var remainSolutionSeq = partialSolutionSeq.filter(_.size >= minImagesPerSolution)
      info(s"Iteration: $iteration. Solution count before filtering solutions with at least $minImagesPerSolution images: ${remainSolutionSeq.length}")
      remainSolutionSeq = FovSeq.deduplicate(remainSolutionSeq).toArray
      info(s"Iteration: $iteration. Solution count after removing duplicates: ${remainSolutionSeq.length}")

      //append to final result
      solutionSeq ++= remainSolutionSeq
      val (fovSeqNotInSolution, _) = getFovSeqNotInSolution(fovMap, solutionSeq.toArray)
      val fovSeqNotInSolutionCount = fovSeqNotInSolution.size()
      info(s"Iteration: $iteration. Image count not present in any solution: $fovSeqNotInSolutionCount")

      //check whether stop
      if (fovSeqNotInSolution.isEmpty() ||
        (fovSeqNotInSolutionCount == imagesNotInSolutionCount))
        continue = false
      else candidateFovSeq = fovSeqNotInSolution //try to append the fov not present in any solution

      //update vars
      imagesNotInSolutionCount = fovSeqNotInSolutionCount
      iteration += 1
    }

    //merge results
    continue = true
    iteration = 0
    var solutionCount = solutionSeq.size
    while (continue) {
      //merge solutions
      info(s"Simplifying results. Iteration: $iteration. Before applying merge on ${solutionSeq.length} solutions")
      merge(solutionSeq, minRa, minDec)
      info(s"Simplifying results. Iteration: $iteration. After applying merge on ${solutionSeq.length} solutions")

      if (solutionSeq.size == solutionCount) continue = false
      else solutionCount = solutionSeq.size
      iteration += 1
    }

    //avoid to reuse images(fov) in different solutions.
    info(s"Before avoiding to reuse fov: ${solutionSeq.length} solutions")
    val usedFOV = scala.collection.mutable.Set[Int]()

    val finalSolutionSeq = solutionSeq
      .sortWith(_.seq.size > _.seq.size)
      .zipWithIndex.flatMap { case (fovSeq, i) =>
      val fovIdSet = fovSeq.getIdSet
      val notUsedFov = fovIdSet.diff(usedFOV)
      usedFOV ++= notUsedFov

      //calculate images not in any solution
      val newFovSeq = FovSeq(fovSeq.seq.filter(fov => notUsedFov.contains(fov.id)))
      if (newFovSeq.size < minImagesPerSolution) {
        val solutionName = s"solution_${f"$i%02d"}"
        val oDir = Path.resetDirectory(discardedOutputDir + s"$solutionName/")
        newFovSeq.save(oDir)
        None
      }
      else Some(newFovSeq.sortByDate())
    }

    info(s"After avoiding to reuse fov: ${finalSolutionSeq.length} solutions")

    //check the validity of the groups
    finalSolutionSeq.zipWithIndex
      .filter { case (fovSeq, i) => fovSeq.isValid(minRa, minDec, i) }.map(_._1).toArray
  }
  //---------------------------------------------------------------------------
  private def calculateCandidatesParallel(candidateFovSeq: FovSeq
                                          , minRa: Double
                                          , minDec: Double
                                          , iteration: Int) = {
    //-----------------------------------------------------------------------
    val result = candidateFovSeq.seq.map { FovSeq(_)}.toArray //append as seed all the individual items and the try to grow from them
    info(s"Iteration: $iteration. Candidate count: ${candidateFovSeq.seq.length}")
    //-------------------------------------------------------------------------
    class ProcessingParallel() extends ParallelTask[FovSeq](
      result
      , CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 500) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(fovSeqA: FovSeq) = {
        var remainFovSeq = candidateFovSeq
        var continue = true
        while (continue) { //try to grow from candidates as much as possible
          val idSetfovSeqA = fovSeqA.getIdSet
          val fovSeqB = FovSeq(remainFovSeq.seq.filter(fov => !idSetfovSeqA.contains(fov.id)).toArray) //remove from remainFovSeq the fov already present in fovSeqA
          val r = FovSeqHeuristic.fovWithBiggerIntersection(fovSeqA, fovSeqB, minRa, minDec)
          if (r.isEmpty || r.get.seq.isEmpty) continue = false
          else remainFovSeq = r.get
        }
      }
      //-----------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    new ProcessingParallel()
    result
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CommonFov.scala
//=============================================================================
