/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  25/May/2022
 * Time:  11h:56m
 * Description: None
 */
//=============================================================================
package com.common.image.astrometry.fov
//=============================================================================
//=============================================================================
object FovSeqHeuristic {
  //-------------------------------------------------------------------------
  //find the fov in fovB that maximizes the intersection with the instersection of FovSeqA
  def fovWithBiggerIntersection(fovSeqA: FovSeq
                                , fovSeqB: FovSeq
                                , minRa: Double
                                , minDec: Double) = {
    val intFovSeq = FovSeq.getIntesectionRectangle(fovSeqA, fovSeqB, minRa, minDec)
      .sortWith(_._1 < _._1) //sort by area
    if (intFovSeq.isEmpty) None
    else {
      //append to fovSeqA
      fovSeqA += intFovSeq.head._2  //FOV in fovSeqB with bigger area
      val seqID = fovSeqA.getIdSet
      val remainFov = FovSeq(fovSeqB.seq.filter(fov=> !seqID.contains(fov.id)))
      Some(remainFov)
    }
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file FovSeqHeuristic.scala
//=============================================================================
