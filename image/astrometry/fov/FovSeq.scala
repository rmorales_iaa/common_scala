/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  13/May/2022
 * Time:  18h:05m
 * Description: None
 */
//=============================================================================
package com.common.image.astrometry.fov
//=============================================================================

import com.common.database.postgreDB.astrometry.AstrometryDB
import com.common.geometry.point.Point2D_Double
import com.common.geometry.rectangle.RectangleDouble
import com.common.logger.MyLogger
import com.common.util.path.Path
//=============================================================================
import scala.collection.mutable.ArrayBuffer
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object FovSeq {
  //---------------------------------------------------------------------------
  private val sep = "\t"
  //-------------------------------------------------------------------------
  def apply(fov: Fov) : FovSeq = FovSeq(ArrayBuffer[Fov](fov))
  //-------------------------------------------------------------------------
  def apply(seq: Array[Fov]) : FovSeq = FovSeq(ArrayBuffer[Fov]() ++ seq)
  //-------------------------------------------------------------------------
  def deduplicate(seq: Array[FovSeq]) = {
    //-----------------------------------------------------------------------
    def isSubSet(setA: ArrayBuffer[FovSeq], setB:FovSeq): Boolean = {
      for(setA<-setA) if (setB.isSubSetOf(setA)) return true
      false
    }
    //-----------------------------------------------------------------------
    def canRemoveOneDuplicate(setSeq: ArrayBuffer[FovSeq]): Boolean = {
      if (setSeq.length < 2) return false
      setSeq.zipWithIndex.foreach { case (setA, i)=>
        if (isSubSet(setSeq.drop(i+1), setA)) setSeq.remove(i+1); return true
      }
      false
    }
    //-----------------------------------------------------------------------
    val setSeq = ArrayBuffer[FovSeq]() ++ seq
    var continue = true
    while(continue) {
      val previousSolutionCount = setSeq.length
      canRemoveOneDuplicate(setSeq)
      continue = previousSolutionCount != setSeq.length
    }
    setSeq
    //-----------------------------------------------------------------------
  }
  //-------------------------------------------------------------------------
  private def rectangleToString(rec: RectangleDouble) = {
    s"min RA_DEC:(${f"${rec.min.x}%.5f"},${f"${rec.min.y}%.5f"})" +
    s" max RA_DEC:(${f"${rec.max.x}%.5f"},${f"${rec.max.y}%.5f"})"
  }
  //-------------------------------------------------------------------------
  def saveRaw(csvName: String, fovSeq: Array[Fov]) = {
    val bw = new BufferedWriter(new FileWriter(new File(csvName)))
    bw.write(AstrometryDB.COL_NAME_SEQ.mkString(sep) + "\n")
    fovSeq
      .sortWith{case (a,b) => a.observingDate.isBefore(b.observingDate)}
      .foreach{ fov => bw.write(fov.getAsLine(sep) + "\n") }
    bw.close()
  }
  //-------------------------------------------------------------------------
  def getIntesectionRectangle(fovSeqA: FovSeq, fovSeqB: FovSeq, minRa: Double, minDec: Double) = {
    val recA = fovSeqA.getIntersection().get
    fovSeqB.seq.flatMap { fovB =>
      if (fovSeqA.contains(fovB)) None
      else {
        val intersectedRectangle = recA.getIntersection(fovB.rec)
        if (!intersectedRectangle.isDefined) None
        else if (intersectedRectangle.isDefined &&
          intersectedRectangle.get.width > minRa &&
          intersectedRectangle.get.height > minDec) Some((intersectedRectangle.get.area, fovB))
        else None
      }
    }
  }
  //-------------------------------------------------------------------------
}
//===========================================================================
import FovSeq._
case class FovSeq(seq: ArrayBuffer[Fov] = ArrayBuffer[Fov]()) extends MyLogger {
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  def size() = seq.size
  //-------------------------------------------------------------------------
  def isEmpty() = seq.isEmpty
  //-------------------------------------------------------------------------
  def += (fov: Fov) = seq += fov
  //-------------------------------------------------------------------------
  def += (fovSeq: FovSeq) = seq ++= fovSeq.seq
  //-------------------------------------------------------------------------
  def getIdSet  = seq.map(_.id).toSet
  //-------------------------------------------------------------------------
  def isSubSetOf(fovSeqB: FovSeq) = fovSeqB.getIdSet.subsetOf(getIdSet)
  //-------------------------------------------------------------------------
  def contains(fov: Fov) = seq.find(_.id == fov.id).isDefined
  //-------------------------------------------------------------------------
  def getIntersection(): Option[RectangleDouble] = {
    var currentIntRec = seq.head.rec
    if(seq.length > 1)
      seq.drop(1).map { fov =>
        val r = currentIntRec.getIntersection(fov.rec)
        if (r.isEmpty) return None
        else currentIntRec = r.get
      }
    Some(currentIntRec)
  }
  //-------------------------------------------------------------------------
  def hasValidIntersection(fovB: Fov, minArea: Double): Boolean = {
    if (contains(fovB)) return false
    val recA = getIntersection().get
    val recB = fovB.rec
    val recInt = recA.getIntersection(recB)
    if (recInt.isDefined && recInt.get.area > minArea) true
    else false
  }
  //-------------------------------------------------------------------------
  override def toString: String =  (seq map (_.id)).mkString(";")
  //-------------------------------------------------------------------------
  def isValid(minRa: Double, minDec: Double, pos: Int, reportError: Boolean = true): Boolean = {
    var currentIntRec = seq.head.rec
    seq.zipWithIndex.map { case (fov,i) =>
      val newIntRec = currentIntRec.getIntersection(fov.rec)
      if (newIntRec.isDefined) currentIntRec = newIntRec.get
      else {
        if (reportError) error(s"Solution: $pos is invalid. Empty intersection bettewen current FOV: $currentIntRec and FOV $i:${fov.rec} ")
        return false
      }
    }
    val isValid = currentIntRec.width > minRa &&
                  currentIntRec.height > minDec

    if (isValid && reportError) info(s"Valid solution: $pos image count: ${seq.length} FOV intersection: ${rectangleToString(currentIntRec)} area: ${f"${currentIntRec.area}%.3f"}")
    isValid
  }
  //-------------------------------------------------------------------------
  def save(outputDir: String) = {
    Path.ensureDirectoryExist(outputDir)

    //all
    val oDir = Path.resetDirectory(outputDir)
    saveRaw(oDir + "image_seq.csv", seq.toArray)
  }
  //-------------------------------------------------------------------------
  //(raRange,decRange)
  def getRaDecRange() = {
    var minRa = Double.MaxValue
    var maxRa = Double.MinValue
    var minDec = Double.MaxValue
    var maxDec = Double.MinValue
    seq.map{ fov=>
      val min = fov.rec.min
      val max = fov.rec.max

      if (min.x < minRa) minRa = min.x
      if (min.x > maxRa) maxRa = min.x

      if (max.x < minRa) minRa = max.x
      if (max.x > maxRa) maxRa = max.x

      if (min.y < minDec) minDec = min.y
      if (min.y > maxDec) maxDec = min.y

      if (max.y < minDec) minDec = max.y
      if (max.y > maxDec) maxDec = max.y
    }
    (Point2D_Double(minRa,maxRa),Point2D_Double(minDec,maxDec))
  }
  //-------------------------------------------------------------------------
  def tryToMerge(fovSeqB: FovSeq
                , minRa: Double
                , minDec : Double) : Boolean = {
    val recA = getIntersection().getOrElse(return false)
    val recB = fovSeqB.getIntersection().getOrElse(return false)
    val newIntersection = recA.getIntersection(recB).getOrElse(return false)
    val isValidIntersection = newIntersection.width > minRa &&
                              newIntersection.height > minDec
    if (isValidIntersection) {
      val newFovIdSeq = fovSeqB.getIdSet.diff(getIdSet)
      val newFovSeq = fovSeqB.seq.filter(fov=> newFovIdSeq.contains(fov.id))
      seq ++= newFovSeq
      true
    }
    else false
  }
  //-------------------------------------------------------------------------
  def sortByDate() = {
    val sortedFOV = seq.sortWith{case (a,b) => a.observingDate.isBefore(b.observingDate)}
    seq.clear()
    seq ++= sortedFOV
    this
  }
  //-------------------------------------------------------------------------
  def getMap() = (seq map (fov=> (fov.id,fov))).toMap
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file FovSeq.scala
//=============================================================================
