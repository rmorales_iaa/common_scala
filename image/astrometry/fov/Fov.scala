/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/May/2022
 * Time:  21h:39m
 * Description: None
 */
//=============================================================================
package com.common.image.astrometry.fov
//=============================================================================

import com.common.database.postgreDB.astrometry.AstrometryDB
import com.common.geometry.rectangle.RectangleDouble
//=============================================================================
import java.time.LocalDateTime
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
object Fov {
  //---------------------------------------------------------------------------
  private def sep = "\t"
  //---------------------------------------------------------------------------
  def saveAsCsv(fileName: String, fovSeq: Seq[Fov]) = {
    val bw = new BufferedWriter(new FileWriter(new File(fileName)))
    bw.write(AstrometryDB.COL_NAME_SEQ.mkString(sep) + "\n")
    fovSeq.foreach { fov => bw.write(fov.getAsLine(sep) + "\n") }
    bw.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Fov(id: Int
               , imageName: String
               , rec: RectangleDouble
               , observingDate: LocalDateTime
               , telescope: String
               , filter: String
               , instrument: String
               , expTime: Double
               , xMaxPix: Int
               , yMaxPix: Int
               , raFov: Double
               , decFov: Double
               , pixScaleX: Double
               , pixScaleY: Double
               , objectName: String
               , sourceSeq: Seq[FovSource] = Seq[FovSource]()) {
  //-------------------------------------------------------------------------
  override def toString: String = s"$imageName"
  //-------------------------------------------------------------------------
  def getAsLine(sep: String) =
    s"$imageName$sep${rec.min.x}$sep${rec.max.x}$sep" +
    s"$observingDate$sep$telescope$sep$filter$sep$instrument$sep$expTime$sep" +
    s"$xMaxPix$sep$yMaxPix$sep$raFov$sep$decFov$sep$pixScaleX$sep$pixScaleY"
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file Fov.scala
//=============================================================================
