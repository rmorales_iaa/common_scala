/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  20/Jan/2023
 * Time:  18h:32m
 * Description: None
 */
//=============================================================================
package com.common.image.astrometry.orbit
//=============================================================================
import com.common.configuration.MyConf
import com.common.hardware.cpu.CPU
import com.common.image.catalog.{ImageCatalog, ObjectDB_Item}
import com.common.image.focusType.ImageFocusTrait
import com.common.image.photometry.absolute.photometricModel.mpo.MPO_DB_Item
import com.common.logger.MyLogger
import com.common.stat.{SimpleStatDescriptive, StatDescriptive}
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import scala.util.{Try,Success,Failure}
import java.util.concurrent.ConcurrentLinkedQueue
import scala.collection.JavaConverters.collectionAsScalaIterableConverter
//=============================================================================
//=============================================================================
object OrbitStats extends MyLogger {
  //---------------------------------------------------------------------------
  final val JPL_ORBIT_RESIDUAL_DETAILED_ROOT_PATH = "jpl_orbit_residual_detailed"
  //---------------------------------------------------------------------------
  private final val sep = "\t"
  //---------------------------------------------------------------------------
  private final val conf                          = MyConf(MyConf.c.getString("LightCurve.configurationFile"))
  private final val sigmaClipping                 = conf.getDouble("LightCurve.sigmaScale")
  private final val fitAlgorithmName              = MyConf.c.getString("Astrometry.fitWcs.algorithm")
  private final val centroidAlgorithmName         = MyConf.c.getStringSeq("SourceCentroid.fallbackAlgorithmSeq").mkString("'",",","'")

  private final val additionalNoiseTideMultiplier = MyConf.c.getString("Astrometry.fitWcs.additionalNoiseTideMultiplier")

  private final val raMaxAllowedResidualMas = MyConf.c.getDouble("Astrometry.fitWcs.raMaxAllowedResidualMas")
  private final val decMaxAllowedResidualMas = MyConf.c.getDouble("Astrometry.fitWcs.decMaxAllowedResidualMas")

  private final val xMaxAllowedResidualPix = MyConf.c.getDouble("Astrometry.fitWcs.xMaxAllowedResidualPix")
  private final val yMaxAllowedResidualPix = MyConf.c.getDouble("Astrometry.fitWcs.yMaxAllowedResidualPix")

  //---------------------------------------------------------------------------
  private def writeStatsFile(objectName: String
                             , orbitVersion: String
                             , statsFileName: String
                             , raDiffSigmaClipping: Array[(Double,String)]
                             , decDiffSigmaClipping: Array[(Double,String)]
                            ) = {
    //-------------------------------------------------------------------------
    val textStatsFile = new BufferedWriter(new FileWriter(new File(statsFileName)))
    //-------------------------------------------------------------------------
    def write(s: String) = {
      info(s)
      textStatsFile.write(s + "\n")
    }
    //-------------------------------------------------------------------------
    def writeStats(stats: SimpleStatDescriptive, prefix: String): Unit = {
      write(s"$prefix object              : $objectName")
      write(s"$prefix orbit version       : $orbitVersion")
      write(s"$prefix count               : ${stats.count}")
      write(s"$prefix average             : ${f"${stats.average}%.6f"} (mas)")
      write(s"$prefix standard deviation  : ${f"${stats.stdDev}%.6f"} (mas)")
      write(s"$prefix median              : ${f"${stats.median}%.6f"} (mas)")
      write(s"$prefix variance            : ${f"${stats.variance}%.6f"} (mas)")
      write(s"$prefix min value           : ${f"${stats.min}%.6f"} (mas)")
      write(s"$prefix max value           : ${f"${stats.max}%.6f"} (mas)")
    }
    //-------------------------------------------------------------------------
    //write the stats file
    val raDiffArcSecStat  = StatDescriptive.getWithDouble(raDiffSigmaClipping map (_._1))
    val decDiffArcSecStat = StatDescriptive.getWithDouble(decDiffSigmaClipping map (_._1))
    val subTitle = s"$objectName (orbit: $orbitVersion)"
    write(s"#------- Orbit statistics starts '$subTitle' --------")
    write("#-----------------")
    write(s"statistics sigma clipping        : $sigmaClipping")
    write(s"wcs fitting algorithm            : $fitAlgorithmName")
    write(s"centroid algorithm fall back seq : $centroidAlgorithmName")

    write("#-----------------")
    write(s"additional noise tide multiplier : $additionalNoiseTideMultiplier")
    write(s"ra max allowed residual (mas)    : $raMaxAllowedResidualMas")
    write(s"dec max allowed residual (mas)   : $decMaxAllowedResidualMas")
    write(s"x max allowed residual (pix)     : $xMaxAllowedResidualPix")
    write(s"y max allowed residual (pix)     : $yMaxAllowedResidualPix")
    write("#-----------------")

    writeStats(raDiffArcSecStat, "Residual ra ")
    write("#-----------------")
    writeStats(decDiffArcSecStat, "Residual dec")
    write("#-----------------")

    write("####################################")
    write(s"orbit residual average (ra,dec) : (${f"${raDiffArcSecStat.average}%.6f"},${f"${decDiffArcSecStat.average}%.6f"}) (mas)")
    write(s"orbit residual stddev  (ra,dec) : (${f"${raDiffArcSecStat.stdDev}%.6f"},${f"${decDiffArcSecStat.stdDev}%.6f"}) (mas)")
    write("####################################")

    write(s"#------- Orbit statistics ends '$subTitle' ----------")

    textStatsFile.close()
  }
  //---------------------------------------------------------------------------
  private def writeResidualCsv(name: String
                               , seq: Array[(Double, String)]
                               , mpoDB_Array: Array[MPO_DB_Item]
                               , isRa: Boolean) = {
    val bw = new BufferedWriter(new FileWriter(new File(name)))
    val header = {
      if (isRa) s"image${sep}residual_ra${sep}image_ra${sep}jpl_ra\n"
      else s"image${sep}dec_residual${sep}image_dec${sep}jpl_dec\n"
    }
    bw.write(header)
    seq.foreach { item =>
      val info = mpoDB_Array.find(_._id == item._2).get
      if (isRa)
        bw.write(s"${item._2}$sep${item._1}$sep${info.m2_est_ra}$sep${info.m2_jpl_ra_orbit}\n")
      else
        bw.write(s"${item._2}$sep${item._1}$sep${info.m2_est_dec}$sep${info.m2_jpl_dec_orbit}\n")
    }
    bw.close
  }
  //---------------------------------------------------------------------------
  private def processAxis(residualSeq: Array[Double]
                          , imageNameSeq: Array[String]
                          , axisName: String): (Array[(Double,String)],Array[(Double,String)])= {

    if (residualSeq.isEmpty) {
      error(s"Axis: '$axisName' no residual to process")
      return (Array(), Array())
    }

    if (imageNameSeq.isEmpty) {
      error(s"Axis: '$axisName' no images remain to process")
      return (Array(),Array())
    }

    warning(s"Axis: '$axisName' starting with ${residualSeq.length} images before any sigma clipping")

    val residualMap = residualSeq.zipWithIndex.map { case (residual, i) => (i, residual) }.toMap
    val residualPairSeq = (residualMap map {case (i,residual) => (residual,i)}).toArray
    val imageMap = imageNameSeq.zipWithIndex.map { case (imageName, i) => (i, imageName) }.toMap

    val remainPairSeq = StatDescriptive.sigmaClippingDoubleWithPair(residualPairSeq, sigmaScale = sigmaClipping)
    val discardedID_Seq = residualMap.keys.toSet.diff((remainPairSeq map (_._2)).toSet)
    val remainID_Seq = remainPairSeq map (_._2)

    warning(s"Axis: '$axisName' removed: ${discardedID_Seq.size} images due to '$axisName' sigma clipping")
    warning(s"Axis: '$axisName' remain : ${remainID_Seq.length} images after '$axisName' sigma clipping")

    val finalRemainID_Seq = remainID_Seq.flatMap { i=>
      if (residualMap.contains(i) && imageMap.contains(i)) Some(i) else None
    }
    ( finalRemainID_Seq map (i=> (residualMap(i),imageMap(i)))
    , (discardedID_Seq map (i=> (residualMap(i),imageMap(i)))).toArray )
  }
  //---------------------------------------------------------------------------
  private def buildReport(objectName: String
                          , infoArr: Array[MPO_DB_Item]
                          , oDir: String) = {
    //-------------------------------------------------------------------------
    val imageCatalogDB = ImageCatalog(objectName)
    //-------------------------------------------------------------------------
    val textStatsName               = oDir + "orbit_stats.txt"
    val raResidualCsvName           = oDir + "orbit_residual_ra.csv"
    val decResidualCsvName          = oDir + "orbit_residual_dec.csv"
    val raResidualDiscardedCsvName  = oDir + "orbit_residual_discarded_ra.csv"
    val decResidualDiscardedCsvName = oDir + "orbit_residual_discarded_dec.csv"
    //-------------------------------------------------------------------------
    val imageNameSeq = infoArr map (_._id)

    val (raRemainSeq,raDiscardedSeq)    = processAxis(infoArr map (_.m2_est_ra_residual.toDouble), imageNameSeq, "ra")
    val (decRemainSeq,decDiscardedSeq)  = processAxis(infoArr map (_.m2_est_dec_residual.toDouble), imageNameSeq, "dec")

    //-------------------------------------------------------------------------
    //csv
    writeResidualCsv(raResidualCsvName, raRemainSeq, infoArr, isRa = true)
    writeResidualCsv(decResidualCsvName, decRemainSeq, infoArr, isRa = false)
    writeResidualCsv(raResidualDiscardedCsvName, raDiscardedSeq, infoArr, isRa = true)
    writeResidualCsv(decResidualDiscardedCsvName, decDiscardedSeq, infoArr, isRa = false)

    //write the stats file
    writeStatsFile(objectName
                   , infoArr.head.m2_jpl_orbit_version
                   , textStatsName
                   , raRemainSeq
                   , decRemainSeq)

    imageCatalogDB.close()
  }
  //---------------------------------------------------------------------------
  class ProcessInfoImageSeq(imageInfoSeq: Array[ObjectDB_Item]
                            , spkVersion: Option[String] = None
                            , infoSeq:ConcurrentLinkedQueue[MPO_DB_Item]) extends ParallelTask[ObjectDB_Item](
    imageInfoSeq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(infoImage: ObjectDB_Item) = {
      Try {
        val mpo = MPO_DB_Item.build(
          infoImage._id
          , infoImage.source_seq.head
          , infoImage.telescope
          , infoImage.observing_time
          , infoImage.exposure_time
          , vr = 0.5
          , spkVersion)

        if (mpo.isDefined) infoSeq.add(mpo.get)
        else
          error(s"Error processing ObjectDB_Item:'${infoImage._id}''")
      }
      match {
        case Success(_) =>
        case Failure(e) =>
          error(s"Error processing file:'${infoImage._id}'")
          error(e.toString)
          error(e.getStackTrace.mkString("\n"))
      }
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def calculate(imageFocus: ImageFocusTrait
                , outputRootPath: String
                , debugStorage : Boolean
                , spkVersion: Option[String] = None) = {

    val objectID = if (debugStorage) imageFocus.getDebuggingID else imageFocus.getID
    val db = ImageCatalog(objectID)

    val infoSeq = new ConcurrentLinkedQueue[MPO_DB_Item]()
    new ProcessInfoImageSeq(
      db.getImageSeq().toArray
      , spkVersion
      , infoSeq
    )
    db.close()

    val infoArr = infoSeq.asScala.toArray
    val oDir = Path.resetDirectory(outputRootPath + s"$JPL_ORBIT_RESIDUAL_DETAILED_ROOT_PATH/jpl_orbit_${infoArr.head.m2_jpl_orbit_version}")

    buildReport(objectID
                , infoArr
                , oDir)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file OrbitStats.scala
//=============================================================================
