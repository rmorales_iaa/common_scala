/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  01/Oct/2024
 * Time:  23h:29m
 * Description: None
 */
package com.common.image.astrometry
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.database.gaia.GaiaSourceReduced
import com.common.database.postgreDB.PostgreScalaDB
import com.common.database.postgreDB.gaia_dr_3.SourceMinimal
import com.common.image.myImage.MyImage
import com.common.logger.MyLogger
import net.robowiki.knn.implementations.Rednaxela3rdGenTreeKNNSearch
import net.robowiki.knn.util.KNNEntry
//=============================================================================
//=============================================================================
object GaiaCatalog extends MyLogger {
  //---------------------------------------------------------------------------
  type KnnTreeType = Rednaxela3rdGenTreeKNNSearch
  //---------------------------------------------------------------------------
  private val postgreDB = PostgreScalaDB(MyConf.c.getString("Database.postgre.url")
                          , MyConf(MyConf.c.getString("Database.gaia_dr_3")).getString("Database.name"))
  //---------------------------------------------------------------------------
  def getCatalog(img: MyImage): Array[GaiaSourceReduced] = {
    val posSeq = img.getRaDecRelevantPos()
    getCatalog(
        posSeq(0)
      , posSeq(1)
      , posSeq(2)
      , posSeq(3))
  }

  //---------------------------------------------------------------------------
  def getCatalog(minRa: Double
                 , maxRa:Double
                 , minDec: Double
                 , maxDec:Double
                 , verbose: Boolean = false): Array[GaiaSourceReduced] = {
    val query = SourceMinimal.getSourceSeqAtSkyArea(
        minRa
      , maxRa
      , minDec
      , maxDec)

    val r = postgreDB.runQuery(query)
    if (verbose) info(s"Found: ${r.size} GAIA objects in the requested area: ra_range($minRa,$maxRa) dec_range($minDec,$maxDec)")
    r.map { sourceMinimal=>
      GaiaSourceReduced(
        _id = sourceMinimal.source_id
        , ra = sourceMinimal.ra
        , dec = sourceMinimal.dec
        , raError = sourceMinimal.ra_error
        , decError = sourceMinimal.dec_error
        , gMeanMag = sourceMinimal.phot_g_mean_mag
        , bpMeanMag = sourceMinimal.phot_bp_mean_mag
        , rpMeanMag = sourceMinimal.phot_rp_mean_mag
        , bp_rpExcessFactor = sourceMinimal.phot_bp_rp_excess_factor
        , ruwe = sourceMinimal.ruwe
        , referenceEpoch = com.common.database.postgreDB.gaia_dr_3.Source.REFERENCE_EPOCH
        , pmra = sourceMinimal.pmra
        , pmdec = sourceMinimal.pmdec
        , pmra_error = sourceMinimal.pmra_error
        , pmdec_error = sourceMinimal.pmdec_error)
    }
  }

  //---------------------------------------------------------------------------
  def getCatalogAsKnnTree(img: MyImage): KnnTreeType = {
    val posSeq = img.getRaDecRelevantPos()
    getCatalogAsKnnTree(
        posSeq(0)
      , posSeq(1)
      , posSeq(2)
      , posSeq(3))
  }
  //---------------------------------------------------------------------------
  def getCatalogAsKnnTree(minRa: Double
                          , minDec: Double
                          , maxRa: Double
                          , maxDec: Double
                          , verbose: Boolean = false): KnnTreeType = {

    val query = SourceMinimal.getSourceSeqAtSkyArea(
      minRa
      , minDec
      , maxRa
      , maxDec)

    val r = postgreDB.runQuery(query)
    if (verbose) info(s"Found: ${r.size} GAIA objects in the requested area: ra_range($minRa,$maxRa) dec_range($minDec,$maxDec)")
    val knnTree = new KnnTreeType(2) //point 2D

    r.map { sourceMinimal =>
      knnTree.addDataPoint(new KNNEntry(sourceMinimal.source_id.toString
                                         , Array(sourceMinimal.ra, sourceMinimal.dec)))
    }

    knnTree
  }

  //---------------------------------------------------------------------------
}

//=============================================================================
//=============================================================================
//End of file GaiaCatalog.scala
//=============================================================================