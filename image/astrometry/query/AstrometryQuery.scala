/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  04/Nov/2021
 * Time:  18h:22m
 * Description: None
 */
//=============================================================================
package com.common.image.astrometry.query
//=============================================================================
import com.common.configuration.MyConf
import com.common.csv.CsvRead
import com.common.database.mongoDB.database.vr.VR_DB
import com.common.database.postgreDB.astrometry.AstrometryDB
import com.common.estimator.skyPosition.EstimatedSkyPosition
import com.common.geometry.point.Point2D_Double
import com.common.geometry.rectangle.RectangleDouble
import com.common.image.astrometry.fov.Fov
import com.common.image.focusType.{ImageFocusMPO, ImageFocusStar, ImageFocusTrait}
import com.common.image.myImage.MyImage
import com.common.image.script.Script
import com.common.image.telescope.Telescope
import com.common.jpl.Spice
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.path.Path
import com.common.util.time.Time
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import java.time.LocalDate
import java.util.concurrent.ConcurrentHashMap
import scala.collection.mutable.ArrayBuffer
import scala.sys.process._
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
object AstrometryQuery extends MyLogger {
  //---------------------------------------------------------------------------
  case class CsvItem(isStar:Boolean,name: String, vr: Double)
  //---------------------------------------------------------------------------
  final val RESULT_IMAGE_FILENAME = "image_list.csv"
  //---------------------------------------------------------------------------
  private final val DEFAULT_MPO_OUTPUT_PATH = "output/query/mpo/"
  //---------------------------------------------------------------------------
  private final val sep = "\t"
  //---------------------------------------------------------------------------
  private final val telescopeFilter = MyConf.c.getStringSeq("Astrometry.telescopeFilter.name") map (_.toLowerCase)
  //---------------------------------------------------------------------------
  private final val nameMap = new ConcurrentHashMap[String,String]() //for image duplicated checking
  private final val imageDuplicatedSeq = ArrayBuffer[String]()  //duplicated images
  //---------------------------------------------------------------------------
  private final val CSV_COMMENT_CHAR = "#"
  private final val CSV_ITEM_DIVIDER = ";"
  //---------------------------------------------------------------------------
  def clearInfoStorage()  = {
    nameMap.clear()
    imageDuplicatedSeq.clear()
  }
  //---------------------------------------------------------------------------
  private def copyImageSeq(objectName: String
                           , imageSeq: Array[String]
                           , linkImages: Boolean
                           , remoteHost: Option[String]
                           , imagePath: String) = {
    //copy/link files
    info(s"--->Found: ${imageSeq.length} images containig the object: '$objectName'")
    if (!imageSeq.isEmpty) {
      val odirImages = Path.resetDirectory(imagePath)
      if (linkImages) info(s"Linking files on: '$odirImages'")
      else info(s"Copying files on: '$odirImages'")

      val imageCount = imageSeq.length
      imageSeq.zipWithIndex.foreach { case (s, i) =>
        info(s"Remain images: ${i + 1}/$imageCount")
        if (remoteHost.isDefined) {
          Try { s"scp ${remoteHost.get}:$s $odirImages".!!}
          match {
            case Success(_) =>
            case Failure(_) => error(s"Error copying remote file: '$s'")
          }
        }
        else { //linking
          if (!MyFile.fileExist(s)) error(s"The file: '$s' has not found in the local storage")
          else {
            val name = Path.getOnlyFilename(s)
            if (linkImages) {
              if (MyFile.fileExist(odirImages + name)) error(s"The file: '$s' m2_id duplicated, ignoring it")
              else s"ln -s $s $odirImages".!!
            }
            else MyFile.copyFile(s, odirImages + name)
          }
        }
      }
    }
  }
  //------------------------------------------------------------------------
  def queryStar(imageFocus: ImageFocusTrait
                , remoteHost: Option[String] = None
                , noImages: Boolean
                , userImageDir: Option[String]
                , linkImages: Boolean
               ): Unit = {
    generateScript(imageFocus, vr = -1, imageDir = None)

    val dir = if (userImageDir.isDefined) Path.resetDirectory(s"${userImageDir.get}/${imageFocus.getID}")
              else Path.resetDirectory(s"./images/${imageFocus.getID}/")
    if (!noImages) saveImageSeq(imageFocus, remoteHost, dir, linkImages)
  }
  //------------------------------------------------------------------------
  private def saveImageSeq(imageFocus: ImageFocusTrait
                           , remoteHost: Option[String]
                           , imagePath: String
                           , linkImages: Boolean) = {
    val raDec = imageFocus.asInstanceOf[ImageFocusStar].raDec
    info(s"Querying astrometry database for star: '${imageFocus.getID}' at position (ra,dec): $raDec")
    val db = AstrometryDB()
    val entrySeq = db.getAstrometryEntrySeq(raDec.x, raDec.y)

    if (entrySeq == null || entrySeq.isEmpty)
      info(s"None image was stored in the astrometry database for object: '${imageFocus.getID}' at position: $raDec")
    else {
      copyImageSeq(
        imageFocus.getID
        , entrySeq map (_.fileName)
        , linkImages
        , remoteHost
        , imagePath)
    }
    db.close()
  }
  //---------------------------------------------------------------------------
  def generateScript(imageFocus: ImageFocusTrait
                      , vr: Double
                      , imageDir: Option[String]
                      , scriptDir: String = "./") = {
    //generate script
    val scriptName = scriptDir + s"${imageFocus.getID}.bash"

    Script(imageFocus
           , scriptName
           , vr
           , imageDir)

    //set script as executable
    s"chmod +x $scriptName".!!
    scriptName
  }
  //---------------------------------------------------------------------------
  def parseCsv(csvFileName: String): Seq[CsvItem] = {
    val csv = CsvRead(csvFileName
                      , hasHeader = true
                      , commentLinePrefix = Seq(CSV_COMMENT_CHAR)
                      , itemDivider = CSV_ITEM_DIVIDER)
    if (!csv.read()) return {
      error(s"Error parsing csv file: '$csvFileName'")
      Seq()
    }
    val rowSeq = csv.getRowSeq
    if (rowSeq.isEmpty) {
      error(s"No valid lines in the csv file: '$csvFileName'")
      Seq()
    }
    rowSeq.map { row=>
      val objectType = row("type")
      val name = row("name")
      val vr = row.getDoubleOrDefault("vr",VR_DB.DEFAULT_VR)
      if (objectType != "mpo" &&
          objectType != "star") {
        error(s"Unknown object type: '$objectType'. Expecting 'mpo' or 'start'. Ignoring this object")
        return Seq()
      }
      CsvItem(objectType == "star",name, vr)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.common.image.astrometry.query.AstrometryQuery._
case class AstrometryQuery(imageFocusSeq: Seq[ImageFocusTrait]
                           , vrSeq: Seq[Double]
                           , remoteHost: Option[String]
                           , noImages: Boolean
                           , userStartDate: Option[LocalDate]
                           , userEndDate: Option[LocalDate]
                           , userTelescope: Option[String]
                           , userFilter: Option[String]
                           , userInstrument: Option[String]
                           , userExposureTime: Option[Double]
                           , userImageDir: Option[String]
                           , userOutputDir: Option[String] = None
                           , linkImages : Boolean
                           , avoidDuplicatedImages: Boolean = true) extends MyLogger {
  //---------------------------------------------------------------------------
  private var imagePath = ""
  private var oDir = ""
  Path.ensureDirectoryExist(DEFAULT_MPO_OUTPUT_PATH)
  private var imageCount = 0L
  //---------------------------------------------------------------------------
  (imageFocusSeq zip vrSeq).foreach {case (imageFocus, vr) =>
    generateScript(imageFocus, vr, userImageDir)
    findObject(imageFocus,noImages,linkImages)
  }
  //---------------------------------------------------------------------------
  def getImageCount = imageCount
  //---------------------------------------------------------------------------
  //(spk path,mpc_id)
  private def findObject(imageFocus: ImageFocusTrait
                         , noImages: Boolean
                         , linkImages: Boolean) : Unit = {
    val objectName = imageFocus.getID
    info(s"Processing object: '$objectName'")

    if (!noImages) {
      imagePath = if (userImageDir.isDefined) Path.resetDirectory(s"${userImageDir.get}/${imageFocus.getID}")
      else Path.resetDirectory(s"./images/$objectName/")
    }

    oDir = if (userOutputDir.isDefined) userOutputDir.get
           else Path.resetDirectory(DEFAULT_MPO_OUTPUT_PATH + imageFocus.getID)
    saveImageSeq(imageFocus, noImages, linkImages)

    if (!imageDuplicatedSeq.isEmpty)
      info(s"Duplicated images in the database: ${imageDuplicatedSeq.length}")
   }
  //---------------------------------------------------------------------------
  private def isInTimeRange(observingDate: LocalDate) = {
    if (userStartDate.isDefined) {
      if (userEndDate.isDefined)
        (observingDate.compareTo(userStartDate.get) >= 0) && (observingDate.compareTo(userEndDate.get) <= 0)
      else
        observingDate.compareTo(userStartDate.get) >= 0
    }
    else true
  }
  //---------------------------------------------------------------------------
  private def isValidTelescope(name: String) = {
    if (userTelescope.isDefined) name.toLowerCase() == userTelescope.get.toLowerCase()
    else
      !telescopeFilter.contains(name.toLowerCase())
  }
  //---------------------------------------------------------------------------
  private def isValidFilter(name: String) = {
    if (userFilter.isDefined) name.toLowerCase() == userFilter.get.toLowerCase()
    else true
  }
  //---------------------------------------------------------------------------
  private def isValidInstrument(instrument: String) = {
    if (userInstrument.isDefined) instrument.toLowerCase() == userInstrument.get.toLowerCase()
    else true
  }
  //---------------------------------------------------------------------------
  private def isValidExposureTime(expTime: Double) = {
    if (userExposureTime.isDefined) expTime >= userExposureTime.get
    else true
  }
  //---------------------------------------------------------------------------
  private def isDuplicated(name:String) : Boolean =
    if (nameMap.containsKey(name)) true
    else {
      nameMap.put(name,name)
      false
    }
  //---------------------------------------------------------------------------
  private def saveImageSeq(imageFocus: ImageFocusTrait
                          , noImages: Boolean
                          , linkImages: Boolean
                          , spkVersion: Option[String] = None): Unit = {

    val objectName = imageFocus.getID

    //find the position of the mpo at the observing time of the image and check if the image field of view (fov) has this position
    val astrometryDB = AstrometryDB()
    if (!imageFocus.isStar) {
      val spkPath = imageFocus.asInstanceOf[ImageFocusMPO].getSpkFilePath(spkVersion)
      if (!MyFile.fileExist(spkPath)) {
        error(s"The spk file: '$spkPath' does not exits, please download it from:'https://ssd.jpl.nasa.gov/horizons/app.html#/'")
        return
      }
      info(s"Loading: '${imageFocus.name}' Spice kernel file")
      if (!Spice.loadKernel(spkPath, verbose = false)) return
    }

    val imageSeq = ArrayBuffer[String]()
    val fovSeq = astrometryDB
      .getAllAstrometryEntrySeq()
      .zipWithIndex.flatMap{ case (astrometryEntry,i) =>

      val filePath      = astrometryEntry.fileName

      val imageName = MyFile.getFileNameNoPathNoExtension(filePath)
      if (imageName.startsWith("science_")) None
      else {
        val(observingTime, telescope, filter, xPixSize, yPixSize, exposureTime, _, instrument, objectName)
        = MyImage.getMetadaFromImageName(imageName)

        val raMin         = astrometryEntry.raMin
        val raMax         = astrometryEntry.raMax
        val decMin        = astrometryEntry.decMin
        val decMax        = astrometryEntry.decMax
        val raFov         = astrometryEntry.fovX
        val decFov        = astrometryEntry.fovY
        val pixScaleX     = astrometryEntry.pixScaleX
        val pixScaleY     = astrometryEntry.pixScaleY

        val onlyFitsName  = Path.getOnlyFilenameNoExtension(filePath)

        val canBeStored =
          isInTimeRange(observingTime.toLocalDate) &&
            isValidTelescope(telescope) &&
            isValidFilter(filter) &&
            isValidInstrument(instrument) &&
            isValidExposureTime(exposureTime) &&
            !isDuplicated(onlyFitsName)

        if(canBeStored)  {
          val expectedPos = EstimatedSkyPosition.getPosWithFallBack(
            imageFocus.asInstanceOf[ImageFocusMPO]
            , Telescope.getOfficialCode(telescope)
            , Telescope.getSpkId(telescope)
            , observingTime.toString
            , verbose = false)

          val mpoRaPos = expectedPos.x
          val mpoDecPos = expectedPos.y
          if ((mpoRaPos >= raMin && mpoRaPos <= raMax) &&
            (mpoDecPos >= decMin && mpoDecPos <= decMax)) {
            imageSeq += filePath

            Some(Fov(i
              , filePath
              , RectangleDouble(Point2D_Double(raMin,decMin),Point2D_Double(raMax,decMax))
              , observingTime
              , telescope
              , filter
              , instrument
              , exposureTime
              , xPixSize
              , yPixSize
              , raFov
              , decFov
              , pixScaleX
              , pixScaleY
              , objectName))
          }
          else None
        }
        else None
      }
    }

   //unload spice kernels
   if (!imageFocus.isStar || !noImages)
     Spice.unloadKernel(imageFocus.asInstanceOf[ImageFocusMPO].getSpkFilePath(spkVersion), verbose = true)

    //saving image list
    Fov.saveAsCsv(oDir + s"/$RESULT_IMAGE_FILENAME", fovSeq)

    //copy files
    if (!noImages)
      copyImageSeq(objectName
                   , imageSeq.toArray
                   , linkImages
                   , remoteHost
                   , imagePath)
    imageCount = imageSeq.length
    warning(s"Found valid:'$imageCount' images for mpo:'$objectName'")
    astrometryDB.close()
  }
  //---------------------------------------------------------------------------
  private def saveTimeRange(csvFileName: String, fovSeq: Seq[Fov]) =  {

    val sortedFov = fovSeq.sortWith{case (a,b) => a.observingDate.isBefore(b.observingDate)}

    //write time range
    val bw = new BufferedWriter(new FileWriter(new File(csvFileName)))
    bw.write(s"min_date${sep}max_date${sep}min_date_jd${sep}max_date_jd${sep}image_name_min_date${sep}image_name_max_date\n")
    val minFov = sortedFov.head
    val maxFov = sortedFov.last
    bw.write(s"${minFov.observingDate}$sep${maxFov.observingDate}$sep" +
      s"${Time.toJulian(minFov.observingDate)}$sep${Time.toJulian(maxFov.observingDate)}$sep" +
      s"${Path.getOnlyFilenameNoExtension(minFov.imageName)}$sep${Path.getOnlyFilenameNoExtension(maxFov.imageName)}$sep"+"\n")
    bw.close()

    //print time range
    info(s"Fov time range(${fovSeq.length}): \n" +
      s"\t\tmin date           : ${minFov.observingDate} \t${Time.toJulian(minFov.observingDate)} (julian date)\n" +
      s"\t\tmax date           : ${maxFov.observingDate} \t${Time.toJulian(maxFov.observingDate)} (julian date)\n" +
      s"\t\timage name min date: '${Path.getOnlyFilenameNoExtension(minFov.imageName)}'\n" +
      s"\t\timage name max date: '${Path.getOnlyFilenameNoExtension(maxFov.imageName)}'"
    )
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file AstrometryQuery.scala
//=============================================================================