/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  04/Jan/2023
 * Time:  17h:24m
 * Description: None
 */
//=============================================================================
package com.common.image.astrometry.query.scansky
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.geometry.rectangle.RectangleDouble
import com.common.hardware.cpu.CPU
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
//=============================================================================
import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
object AstrometryScanSky extends MyLogger {
  //---------------------------------------------------------------------------
  private class MyParallelImageSeq(seq: Array[RectangleDouble]
                                   , odir: String
                                   , parseRectangle: (RectangleDouble,String) => Unit)
    extends ParallelTask[RectangleDouble](
      seq
      , threadCount = CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 2000) {
    //-----------------------------------------------------------------------
    def userProcessSingleItem(rectangle: RectangleDouble) =
      Try { parseRectangle(rectangle, odir) }
      match {
        case Success(_) =>
        case Failure(e) =>
          error(e.getMessage + s" Error processing rectangle: $rectangle")
          error(e.getStackTrace.mkString("\n"))
      }
  }
  //---------------------------------------------------------------------------
  private def getIterations(v: Double, step: Double) = {
    val k = (v / step).toInt
    val remainIteration = if (v % step == 0) 0 else 1
    k + remainIteration
  }
  //---------------------------------------------------------------------------
  private def normalizeAlpha(alpha: Double) =
    if (alpha > 360) alpha - 360
    else alpha
  //---------------------------------------------------------------------------
  private def getRectangleSeq(alfaStart: Double  //decimal degree
                              , alfaEnd: Double  //decimal degree
                              , deltaStart: Double //decimal degree
                              , deltaEnd: Double  //decimal degree
                              , deltaStep: Double  //decimal degree
                             ): Array[RectangleDouble] = {
    //--------------------------------------------------------------------------
    var rectangleID = 0L
    var continueDelta = true
    var currentDelta = deltaStart
    val rectangleSeq = ArrayBuffer[RectangleDouble]()
    //--------------------------------------------------------------------------
    def addRectangle(r: RectangleDouble) = {
      rectangleSeq += r
      rectangleID += 1
      r
    }
    //--------------------------------------------------------------------------

    while(continueDelta) {

      var currentAlpha = alfaStart
      val alfaStep = deltaStep / Math.cos(currentDelta.toRadians)
      val alphaIterarions =
        if (alfaStart > alfaEnd)
          getIterations(360 - alfaStart,alfaStep) + getIterations(alfaEnd,alfaStep)
        else getIterations(alfaEnd - alfaStart,alfaStep)

      //iterate alpha
      for(i <- 1 to alphaIterarions) {

        val minPos = Point2D_Double(normalizeAlpha(currentAlpha), currentDelta)
        val maxPos = Point2D_Double(normalizeAlpha(currentAlpha + alfaStep), currentDelta + deltaStep)

        if (minPos.x > maxPos.x) {  //360 degree limit reached, split in two rectangles
          addRectangle(RectangleDouble(minPos
                                      , Point2D_Double(360d,maxPos.y)
                                      , rectangleID))

          addRectangle(RectangleDouble(Point2D_Double(0,minPos.y)
                                       , maxPos
                                       , rectangleID))
        }
        else
          addRectangle( RectangleDouble(minPos
                                       , maxPos
                                       , rectangleID))

        currentAlpha = rectangleSeq.last.max.x
      }

      //update delta
      currentDelta += deltaStep
      if (currentDelta >= deltaEnd) continueDelta = false
    }
    rectangleSeq.toArray
  }
  //---------------------------------------------------------------------------
  def run(alfaStart: Double //decimal degree
          , alfaEnd: Double //decimal degree
          , deltaStart: Double //decimal degree
          , deltaEnd: Double //decimal degree
          , deltaStep: Double //decimal degree
          , parseRectangle: (RectangleDouble,String) => Unit
          , oDir:String
         ) : Boolean = {

    info(s"Parsing sky area: alfa:($alfaStart,$alfaEnd), delta:($deltaStart,$deltaEnd) deltaStep:$deltaStep")
    val rectangleSeq = getRectangleSeq(alfaStart
      , alfaEnd
      , deltaStart
      , deltaEnd
      , deltaStep)

    info(s"Parsing: ${rectangleSeq.length} rectangles")
    new MyParallelImageSeq(rectangleSeq,oDir,parseRectangle)
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file AstrometryScanSky.scala
//=============================================================================
