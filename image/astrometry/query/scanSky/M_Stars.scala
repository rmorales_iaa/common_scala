/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Jan/2023
 * Time:  09h:33m
 * Description: None
 */
//=============================================================================
package com.common.image.astrometry.query.scansky
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.MongoDB
import com.common.database.mongoDB.database.gaia.GaiaDB
import com.common.database.mongoDB.database.gaia.GaiaDB._
import com.common.logger.MyLogger
import com.common.geometry.rectangle.RectangleDouble
import com.common.util.path.Path
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import java.util.concurrent.ConcurrentLinkedQueue
//=============================================================================
//=============================================================================
object M_Stars extends MyLogger {
  //---------------------------------------------------------------------------
  private val sourceID = MongoDB.MONGO_COL_ID
  private val projectionField = Array(sourceID, GAIA_COL_RA, GAIA_COL_DEC, GAIA_COL_PARALLAX, GAIA_COL_PHOTO_G_MEAN_MAG, GAIA_COL_PHOTO_BP_RP)
  private val sep = ","
  //-----------------------------------------------------------------------
  case class ResultStar(id: Long,ra: Double,dec: Double, parallax: Double, gMag: Double, bpRp: Double)
  //-----------------------------------------------------------------------
  private val summaryResult= new ConcurrentLinkedQueue[(RectangleDouble,Long)]
  //-----------------------------------------------------------------------
  private val gaiaDB = GaiaDB(MyConf(MyConf.c.getString("Database.mongoDB.gaia")))
  //-----------------------------------------------------------------------
  //query parameters for M stars
  private val MIN_PARSECS = 75d
  private val MAX_G_MAG   = 17d
  private val MIN_COLOR   = 2.2d
  private val MAX_COLOR   = 3d
  //-----------------------------------------------------------------------
  private def writeSummary(name: String): Unit = {

    if (summaryResult.isEmpty) return

    val bw = new BufferedWriter(new FileWriter(new File(name)))
    val headerSource =
      s"rec_id$sep" +
      s"rec_min_ra$sep" +
      s"rec_min_dec$sep" +
      s"rec_max_ra$sep" +
      s"rec_max_dec$sep" +
      s"rec_centre_ra$sep" +
      s"rec_centre_dec$sep" +
      s"area$sep" +
      s"m_stars_count" +
      "\n"

    bw.write(headerSource)

    summaryResult.forEach { case (rec,count) =>
      bw.write(
        rec.id + sep +
        rec.min.x + sep +
        rec.min.y + sep +
        rec.max.x + sep +
        rec.max.y + sep +
        rec.getCentroid().x + sep +
        rec.getCentroid().y + sep +
        rec.area2 + sep +
        count +
        "\n"
      )
    }

    bw.close()
  }

  //-----------------------------------------------------------------------
  private def writeRectangle(name:String, rec: RectangleDouble) = {
    val bwRec = new BufferedWriter(new FileWriter(name))
    bwRec.write(s"centre = ${rec.getCentroid()}\n")
    bwRec.write(s"min(ra,dec) = ${rec.min}\n")
    bwRec.write(s"max(ra,dec) = ${rec.max}\n")
    bwRec.write(s"area = ${rec.area2}\n")
    bwRec.close()
  }
  //-----------------------------------------------------------------------
  private def writeSourceSeqCsv(csvName:String, sourceSeq:Seq[ResultStar]) = {

    val bw = new BufferedWriter(new FileWriter(new File(csvName)))
    val headerSource = s"$sourceID" + sep +
      s"$GAIA_COL_RA" + sep +
      s"$GAIA_COL_DEC" + sep +
      s"$GAIA_COL_PARALLAX" + sep +
      s"$GAIA_COL_PHOTO_G_MEAN_MAG" + sep +
      s"$GAIA_COL_PHOTO_BP_RP" + sep +
      "\n"

    bw.write(headerSource)

    sourceSeq.foreach { s=>
      bw.write(s.id + sep +
        s.ra + sep +
        s.dec + sep +
        s.parallax + sep +
        s.gMag + sep +
        s.bpRp + sep +
        "\n")
    }
    bw.close
  }
  //-----------------------------------------------------------------------
  private def parseRectangle(rec: RectangleDouble, rootDir: String) = {

    val sourceSeq = gaiaDB.findSourceSeqInRectangle(
        rec.min.x
      , rec.max.x
      , rec.min.y
      , rec.max.y
      , projectionField).flatMap { doc =>

      val parallax = doc(GAIA_COL_PARALLAX).asDouble().getValue //distance
      val gMag = doc(GAIA_COL_PHOTO_G_MEAN_MAG).asDouble().getValue //brillance
      val bpRp = doc(GAIA_COL_PHOTO_BP_RP).asDouble().getValue //color

      if ((parallax > 1000d / MIN_PARSECS) &&     // distance
        (gMag < MAX_G_MAG) &&                     // brillance
        (bpRp >= MIN_COLOR && bpRp <= MAX_COLOR)) // color

        Some(ResultStar(
            doc(sourceID).asInt64().getValue
          , doc(GAIA_COL_RA).asDouble().getValue
          , doc(GAIA_COL_DEC).asDouble().getValue
          , parallax
          , gMag
          , bpRp))

      else None
    }

    if(sourceSeq.length > 0) {
      val oDir = Path.resetDirectory(s"$rootDir/rectangle_seq/${rec.id}_${sourceSeq.length}")
      writeSourceSeqCsv(oDir + "gaia_source_match.csv",sourceSeq)
      writeRectangle(oDir + "rectangle.txt", rec)
      summaryResult.add((rec,sourceSeq.length))
      info(s"Generated directory:'$oDir'")
    }
  }
  //---------------------------------------------------------------------------
  def run() = {

    val oDir = Path.resetDirectory("output/scan_sky/m_stars")
    AstrometryScanSky.run(
        alfaStart  = 0d
      , alfaEnd    = 225d
      , deltaStart = 20d
      , deltaEnd   = 90d
      , deltaStep  = 0.25d
      , parseRectangle
      , oDir
    )

    writeSummary(oDir + "summary.csv")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file M_Stars.scala
//=============================================================================
