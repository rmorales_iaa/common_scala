/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Jun/2020
 * Time:  20h:20m
 * Description: https://github.com/quatrope/astroalign/blob/master/astroalign.py
 */
package com.common.image.registration
//=============================================================================
import com.common.geometry.affineTransformation.AffineTransformation2D
import com.common.image.myImage.MyImage
import com.common.image.occultation.occImage.OccImage
import com.common.util.util.Util
//=============================================================================
//=============================================================================
object RegistrationByAstroAlignPy {
  //---------------------------------------------------------------------------
  // Algorithm for registration (alignment) imageA(source) and imageB(destination)
  // It calculates the operations (translation,scale, rotation) required to transform imageA(source) in imageB(destination).
  // Registration definition: transforming different sets of data into one coordinate systems. See https://en.wikipedia.org/wiki/Image_registration
  // Using a call to python code astroalign, decribed in "Astroalign: A Python module for astronomical image registration" M. Beroiz, J.B. Cabral, B. Sánchez
  //it uses a Sextractor as source detection algorithm
  //---------------------------------------------------------------------------
  def calculate(imageA: OccImage
                , imageB: OccImage
                , commandPyCall: String) : Option[AffineTransformation2D] = {
    //-------------------------------------------------------------------------
    val result = Util.runShellCommandAndGetStringOutput(commandPyCall, Seq(imageA.path, imageB.path))
    commonAction(result)
  }
  //---------------------------------------------------------------------------
  def calculate(imageA: String
                , imageB: String
                , commandPyCall: String): Option[AffineTransformation2D] = {
    //-------------------------------------------------------------------------
    val result = Util.runShellCommandAndGetStringOutput(commandPyCall, Seq(imageA, imageB))
    commonAction(result)
  }
  //-------------------------------------------------------------------------
  private def getRow(s: String) =
    s.replace("[", "")
      .replace("],", "")
      .trim
      .split(",")
      .map(_.toDouble)

  //-------------------------------------------------------------------------
  private def commonAction(result: String): Option[AffineTransformation2D] = {
    if (result.trim.isEmpty) return None
    val split = result.split("\n")
    if (split(0).contains("Error")) return None
    val row_1 = getRow(split(1))
    val row_2 = getRow(split(2))
    Some(AffineTransformation2D(row_1 ++ row_2))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file RegistrationByAstroAlignPy.scala
//=============================================================================