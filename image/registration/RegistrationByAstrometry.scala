/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  24/Apr/2023
 * Time:  10h:48m
 * Description: registration using the astrometry
 */
//=============================================================================
package com.common.image.registration
//=============================================================================
import com.common.configuration.MyConf
import com.common.geometry.affineTransformation.AffineTransformation2D
import com.common.geometry.point.{Point2D_Double}
import com.common.image.myImage.MyImage
import com.common.image.occultation.occImage.OccImage
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object RegistrationByAstrometry {
  //---------------------------------------------------------------------------
  private val conf = MyConf(MyConf.c.getString("Registration.configurationFile"))
  //---------------------------------------------------------------------------
  private final val raGridSampling  = conf.getInt("Registration.algorithm.astrometry.raGridSampling")
  private final val decGridSampling = conf.getInt("Registration.algorithm.astrometry.decGridSampling")
  //----------------------------------------------------------------------------
  def calculate(catalogOccImage: OccImage
                , sampleOccImage: OccImage)
    : Option[AffineTransformation2D] = {
    val r = calculate(MyImage(catalogOccImage.path)
                    , MyImage(sampleOccImage.path))
    if (r.isDefined) Some(r.get)
    else None
  }
  //----------------------------------------------------------------------------
//return the affine transformation to transform a point in sample point sequence
// to be converted into a point in catalog point
  def calculate(catalogImage: MyImage
                 , sampleImage: MyImage)
    : Option[AffineTransformation2D] = {

    if (!catalogImage.hasWcs() || !sampleImage.hasWcs()) return None

    val catRec = catalogImage.getRaDecRectangle()
    val sampleRec = sampleImage.getRaDecRectangle()

    val intersectionRec = catRec.getIntersection(sampleRec).getOrElse(return None)

    val raStep = intersectionRec.width / raGridSampling.toDouble
    val decStep = intersectionRec.height / decGridSampling.toDouble

    //sample the rectangle to ge the points to be used to calculate the aft
    var currentRa = intersectionRec.min.x
    val maxRa = intersectionRec.max.x

    var currentDec = intersectionRec.min.y
    val maxDec     = intersectionRec.max.y

    val catalogGridPixPos = ArrayBuffer[Point2D_Double]()
    val sampleGridPixPos = ArrayBuffer[Point2D_Double]()
    while(currentDec < maxDec) {
      currentRa  = intersectionRec.min.x
      while(currentRa < maxRa) {
        val raDecPos = Point2D_Double(currentRa,currentDec)
        catalogGridPixPos += catalogImage.skyToPix(raDecPos)
        sampleGridPixPos += sampleImage.skyToPix(raDecPos)
        currentRa += raStep
      }
      currentDec += decStep
    }
    AffineTransformation2D.umeyana2D(sampleGridPixPos.toArray
                                     , catalogGridPixPos.toArray)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RegistrationByAstrometry.scala
//=============================================================================
