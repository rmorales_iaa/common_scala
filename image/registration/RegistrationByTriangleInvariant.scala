package com.common.image.registration
//=============================================================================
import com.common.image.registration.Registration.REGISTRATION_TRIANGLE_INVARIANT

import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.util.Random
//=============================================================================
import com.common.configuration.MyConfWithDefault
import com.common.logger.MyLogger
import com.common.geometry.circle.CircleFloat
import com.common.geometry.point.Point2D_Float
import com.common.geometry.triangle.TriangleFloat
import com.common.dataType.tree.kdTree.K2d_TreeFloat
import com.common.geometry.affineTransformation.AffineTransformation2D
import com.common.math.MyMath
import com.common.configuration.MyConf
import com.common.image.occultation.occImage.OccImage
//=============================================================================
//=============================================================================
object RegistrationByTriangleInvariant extends  MyLogger {
  //---------------------------------------------------------------------------
  private val conf =  MyConf(MyConf.c.getString("Registration.configurationFile"))
 //---------------------------------------------------------------------------
  private final val COMMON_KEY = "Registration.algorithm.triangleInvariant."
  //---------------------------------------------------------------------------
  private final var minAllowedImageSourceCount                      = conf.getInt(COMMON_KEY + "minAllowedImageSourceCount")

  private final var maxSourceCount                                  = conf.getInt(COMMON_KEY + "maxSourceCountConsidered")

  private final var searchCircleRadius                              = conf.getFloat(COMMON_KEY + "searchCircleRadius")

  private final var ransacMinAllowedMatches                         = conf.getInt(COMMON_KEY + "ransac.minAllowedMatches")

  private final var ransacMaxAllowedPixDistanceTolerance            = conf.getInt(COMMON_KEY + "ransac.maxAllowedPixDistanceTolerance")
  private final var ransacMaxIterations                             = conf.getInt(COMMON_KEY + "ransac.maxIterations")

  private final var ransacMinMatchPercentageToValidateModel         = conf.getFloat(COMMON_KEY + "ransac.minMatchPercentageToValidateModel")
  private final var ransacMaxMatchPercentageToValidateModel         = conf.getFloat(COMMON_KEY + "ransac.maxMatchPercentageToValidateModel")
  private final var ransacPercentageStepToValidateModel             = conf.getFloat(COMMON_KEY + "ransac.percentageStepToValidateModel")


  private final var asterismPolygonSideCount                        = conf.getInt(COMMON_KEY + "asterismPolygonSideCount")
  //---------------------------------------------------------------------------
  def reloadConfiguration(newConf: MyConfWithDefault, commonKey:String) = {

    minAllowedImageSourceCount              = newConf.getInt(commonKey + "minAllowedImageSourceCount")

    maxSourceCount                          = newConf.getInt(commonKey + "maxSourceCountConsidered")

    searchCircleRadius                      = newConf.getFloat(commonKey + "searchCircleRadius")

    ransacMinAllowedMatches                 = newConf.getInt(commonKey + "ransac.minAllowedMatches")

    ransacMaxAllowedPixDistanceTolerance    = newConf.getInt(commonKey + "ransac.maxAllowedPixDistanceTolerance")
    ransacMaxIterations                     = newConf.getInt(commonKey + "ransac.maxIterations")

    ransacMinMatchPercentageToValidateModel = newConf.getFloat(commonKey + "ransac.minMatchPercentageToValidateModel")
    ransacMaxMatchPercentageToValidateModel = newConf.getFloat(commonKey + "ransac.maxMatchPercentageToValidateModel")
    ransacPercentageStepToValidateModel     = newConf.getFloat(commonKey + "ransac.percentageStepToValidateModel")


    asterismPolygonSideCount                = newConf.getInt(commonKey + "asterismPolygonSideCount")
  }
  //---------------------------------------------------------------------------
  // Algorithm for registration (alignment) of regionA to regionB
  // It calculates the operations (translation,scale, rotation) required to transform imageA(soure) in imageB(destination).

  // Registration definition: transforming different sets of data into one coordinate systems. See https://en.wikipedia.org/wiki/Image_registration
  // Adapted from python code astroalign, decribed in "Astroalign: A Python module for astronomical image registration" M. Beroiz, J.B. Cabral, B. Sánchez

  //The brightest sources (Segmented2D) of to images (A and B) are selected and its centroids are placed into a k2-tree.
  // Each centroid is considered a vertex. The closets vertex are found (using a k2tree) generating a sequence of triangles.
  // So, each triangle is composed by the closest centroids of the brightest sources of an image.
  // Any triangles is considered an asterism:  visually obvious collection of stars and the lines used to mentally connect them:  https://en.wikipedia.org/wiki/Asterism_(astronomy)
  // Then is calculated a R2 invariant of each triangle (proportion of its side lengths) that is kept in any rotation,scale or translation of the image
  // All invariants are placed into a k2-tree. Each invariants of image A, used to find the closest invariants at image B.
  // So, foreach invariant of A, there is an sequence of invariants in B.
  // Any pair of closest invariants is considered, implying 2 triangles and 6 vertexs (6 sources),
  // Those six-pack is used fo find an affine transformation (rotation, scale, translation)  between the A's vertexs tp B's vertexs
  // The quality of any transformation is calculated using the brightest sources of A and B and the  RANSAC method https://en.wikipedia.org/wiki/Random_sample_consensus
  // The first affine transformation which with enough quality (typically over 80%) is returned as the transformation of image A to B.

  // source <=> vertex <=> triangle <=> invariant
  //  R2    <=>   R2   <=>   R3     <=>   R2
  //---------------------------------------------------------------------------
  //to increase the speed and the imprecision decrease 'maxPercentage'
  //to increase the speed, reduce the number of sources in the regions, increasing the noise tide to list them
  def calculate(refOccImage: OccImage
                , otherOccImage: OccImage
                , findRansacBestResult: Boolean
                , verbose: Boolean)  : Option[AffineTransformation2D] = {
    //-------------------------------------------------------------------------
    def validate(): Boolean = {
      if (refOccImage.getSourceCount < minAllowedImageSourceCount) {
        warning(s"Registration: '$REGISTRATION_TRIANGLE_INVARIANT' reference image:'${refOccImage.getShortName}' has not enough sources: ${refOccImage.getSourceCount} ")
        return false
      }
      if (otherOccImage.getSourceCount < minAllowedImageSourceCount) {
        warning(s"Error in Registration. Image:'${otherOccImage.getShortName}' has not enough sources: ${otherOccImage.getSourceCount}")
        return false
      }
      true
    }
    //-------------------------------------------------------------------------
    if (!validate) return None

    //calculate the triangles of the brightest sources, and put its invariants into a tree
    //for fast calculation of the neighbours
    val (treeInvTriA, triangleMapA) = buildTriangleInvariantTree(refOccImage)
    val (treeInvTriB, triangleMapB) = buildTriangleInvariantTree(otherOccImage)

    if (treeInvTriA.isEmpty || treeInvTriB.isEmpty) return None
    //for all triangles in A, list all the compatible (matched) triangles in B => those triangles in A which its invariants are close to invariants in B
    //Then group the vertex in two lists: referenceVertexSeq and otherVertexSeq. The same index in both lists indicates a pair of vertex that are compatible in both images
    //Any valid affine transformation must be valid at any index (with same allowed error)

    val referenceVertexSeq = ArrayBuffer[Point2D_Float]()
    val otherVertexSeq = ArrayBuffer[Point2D_Float]()

    treeInvTriA.toNodeSeq foreach { invariantA =>
      val closestNodeB_Seq = treeInvTriB.circleSearch(CircleFloat(Point2D_Float(invariantA.data), searchCircleRadius))
      if (!closestNodeB_Seq.isEmpty) {
        referenceVertexSeq ++= triangleMapA(invariantA.v).getSortedVertexSeq
        otherVertexSeq ++= triangleMapB(closestNodeB_Seq.head.v).getSortedVertexSeq
      }
    }
    //find the affine transformation of the compatible triangles
    findTransformation(
        refOccImage
      , otherOccImage
      , (referenceVertexSeq.toArray, otherVertexSeq.toArray)
      , verbose)
  }
  //-------------------------------------------------------------------------
  //Detect the most brilliant sources of the image, list its centroids and use them to process triangles
  // with the closest centroids. Finally process invariants of these triangles
  private def buildTriangleInvariantTree(occImage: OccImage):
     (K2d_TreeFloat, mutable.Map[Int, TriangleFloat]) = {

    val centroidSeq = occImage.sortByFluxPerSecond(maxSourceCount)
      .map(occSource => (occSource.centroid.toPoint2D_Float, occSource.id))

    //store the centroids in the kdtree
    val centroidTree = K2d_TreeFloat(s"Centroids of ${occImage.getShortName}")
    centroidTree.addSeq(centroidSeq map (t => (t._1.toSequence, t._2)))

    //storage for the results
    val invariantMap = scala.collection.mutable.Map[Point2D_Float, TriangleFloat]()
    val triangleMap = scala.collection.mutable.Map[Int, TriangleFloat]()
    //-----------------------------------------------------------------------
    //generate all possible combinations of vertices
    val verticeIndexSeq = MyMath.getCombinationSeq(List.range(0, centroidSeq.size), asterismPolygonSideCount)
    verticeIndexSeq.foreach { vertexIndex =>
      //get the vertex
      val v0 = centroidSeq(vertexIndex(0))._1
      val v1 = centroidSeq(vertexIndex(1))._1
      val v2 = centroidSeq(vertexIndex(2))._1

      //try to build a triangle
      if (TriangleFloat.isValid(v0, v1, v2)) {

        //build triangle
        val triangle = TriangleFloat(v0, v1, v2)

        //filters duplicated triangles and the wrong ones
        val invariant = triangle.getInvariant(triangle.id).getAsPonint2D
        if (!invariant.isNan() &&
          !invariantMap.contains(invariant)) {
          triangleMap(triangle.id) = triangle
          invariantMap(invariant) = triangle
        }
      }
    }

    //store the invariants into a balanced kdtree
    val k2d_TreeTriInv = K2d_TreeFloat(s"Triangle invariants of ${occImage.getShortName}")
    k2d_TreeTriInv.addSeq((for ((inv, t) <- invariantMap) yield (inv.toSequence(), t.id)).toSeq)

    //return the storage
    (k2d_TreeTriInv, triangleMap)
  }
  //-------------------------------------------------------------------------
  private def findTransformation(refOccImage: OccImage
                                 , otherOccImage: OccImage
                                 , compatibleVertexPairSeq: (Array[Point2D_Float], Array[Point2D_Float])
                                 , verbose: Boolean = false): Option[AffineTransformation2D] = {
    val compatibleVertexPairCount = compatibleVertexPairSeq._1.length
    val asterimsSideCount = asterismPolygonSideCount
    val vertexSeq = (compatibleVertexPairSeq._1.take(compatibleVertexPairCount * asterimsSideCount), compatibleVertexPairSeq._2.take(compatibleVertexPairCount * asterimsSideCount))
    var maxMachesFound = 0
    val percentageIterator = Iterator.iterate(ransacMaxMatchPercentageToValidateModel)(_ - ransacPercentageStepToValidateModel)
      .takeWhile(_ >= ransacMinMatchPercentageToValidateModel)

       //limit the min matches to validate the model
    for (percentage <- percentageIterator) {
      val (aft, _maxMachesFound) = ransac(
        vertexSeq //dataPoint
        , ransacMaxAllowedPixDistanceTolerance //threshold
        , asterimsSideCount                    //minModelDataPoint
        , ransacMaxIterations)                 //maxIterations

      if (_maxMachesFound > maxMachesFound) maxMachesFound = _maxMachesFound
      val minMatches = Math.round(vertexSeq._1.length * (percentage * 0.01)).toInt
      if (aft.isDefined &&
        (_maxMachesFound >= minMatches ||
          _maxMachesFound >= ransacMinAllowedMatches)) {
        if (verbose) info(s"Reference image:'${refOccImage.getShortName}' and image:'${otherOccImage.getShortName}' max matches found: $maxMachesFound using invariant triangle")
        return aft
      }
    }
    if (verbose) info(s"Reference image:'${refOccImage.getShortName}' and image:'${otherOccImage.getShortName}' max matches found: $maxMachesFound using invariant triangle")
    None
  }
  //-------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Random_sample_consensus
  //dataPoint        : a set of observed data points
  //thresh           : value to determine when a data point fits a model, in that case existLong a match between the observed data point and the provided by the model
  //minModelPoint    : min number of matches to consider a valid model
  //minModelMatches  : min number of matches to assert the the model fits properly to the observed data points
  //maxIterations    : maximum number of iterations allowed in the algorithm.
  private def ransac(dataPoint: (Array[Point2D_Float], Array[Point2D_Float])
                     , threshold: Float
                     , minModelPoint: Int
                     , maxIterations: Int): (Option[AffineTransformation2D], Int) = {
    //-----------------------------------------------------------------------
    var iterationCount = 0
    //-----------------------------------------------------------------------
    def getNextDataPointCombination(dataPointsIndex: ListBuffer[List[Int]]) = {
        //random combinations
        splitDataPointSeq(Random.shuffle(dataPointsIndex.toList).flatten) //split the input data in two random subsets
    }
    //-----------------------------------------------------------------------
    def splitDataPointSeq(indexList: List[Int]) = {
      if (indexList.length == asterismPolygonSideCount)
        ((dataPoint._1, dataPoint._2), (dataPoint._1, dataPoint._2))
      else {
        val (maybeDataIndex, testDataIndex) = indexList.splitAt(minModelPoint)
        val maybeData = ((maybeDataIndex map (dataPoint._1(_))).toArray, (maybeDataIndex map (dataPoint._2(_))).toArray)
        val testData = ((testDataIndex map (dataPoint._1(_))).toArray, (testDataIndex map (dataPoint._2(_))).toArray)
        (maybeData, testData)
      }
    }
    //-----------------------------------------------------------------------
    def getModelFittingPointCount(testPointSeq: (Array[Point2D_Float], Array[Point2D_Float])
                                  , maybeModel: Option[AffineTransformation2D]): Int = {
      if (maybeModel.isEmpty) return 0
      if (testPointSeq._1.isEmpty) return Int.MinValue //no enough test points, no exit
      val estimatedValueSeq = testPointSeq._1 map (maybeModel.get(_))
      val residualSeq = (estimatedValueSeq zip testPointSeq._2) map (t => t._1.getDistance(t._2))
      residualSeq.filter(_ <= threshold).length
    }
    //-----------------------------------------------------------------------
    def fitTheModel(maybeInliers: (Array[Point2D_Float], Array[Point2D_Float])): Option[AffineTransformation2D] = {
      AffineTransformation2D.umeyana2D(maybeInliers._1, maybeInliers._2) //fit the model
    }
    //-----------------------------------------------------------------------
    def canContinue() = {
      iterationCount < maxIterations
    }
    //-----------------------------------------------------------------------
    var bestModel: Option[AffineTransformation2D] = None
    var bestMaybeModelFittingPointCount: Int = 0
    val dataPointsIndex = scala.collection.mutable.ListBuffer() ++ List.range(0, dataPoint._1.length).grouped(minModelPoint).toList

    while (canContinue) {
      val (maybeInliers, testPointSeq) = getNextDataPointCombination(dataPointsIndex)

      val maybeModel = fitTheModel(maybeInliers) //get aft
      val maybeModelFittingPointCount = getModelFittingPointCount(testPointSeq, maybeModel)

      //all test points matched?
      if (maybeModelFittingPointCount == testPointSeq._1.length) return (maybeModel, maybeModelFittingPointCount)
      else {
        if (maybeModelFittingPointCount > bestMaybeModelFittingPointCount) {
          bestModel = maybeModel
          bestMaybeModelFittingPointCount = maybeModelFittingPointCount
        }
      }
      iterationCount += 1
    }
    (bestModel, bestMaybeModelFittingPointCount)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file RegistrationByInvariantTriangle.scala
//=============================================================================