/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  24/Apr/2023
 * Time:  10h:48m
 * Description: assuming no big rotation or scale is present. USe when only small trasnlations
 * is expected (as in occultations)
 */
//=============================================================================
package com.common.image.registration
//=============================================================================
import com.common.configuration.MyConf
import com.common.dataType.tree.kdTree.K2d_TreeDouble
import com.common.geometry.affineTransformation.AffineTransformation2D
import com.common.geometry.point.Point2D_Double
import com.common.image.occultation.occImage.OccImage
import com.common.image.occultation.occSource.OccSource
import com.common.stat.StatDescriptive
//=============================================================================
//=============================================================================
object RegistrationByClosestSource {
  //---------------------------------------------------------------------------
  private val conf = MyConf(MyConf.c.getString("Registration.configurationFile"))
  //---------------------------------------------------------------------------
  private final val maxAllowedPixDistanceMatch = conf.getInt("Registration.algorithm.closestSource.maxAllowedPixDistanceMatch")
  private final val useResidualMedianForAFT    = conf.getInt("Registration.algorithm.closestSource.useResidualMedianForAFT") == 1
  private final val residualMedianSigmaClipping= conf.getDouble("Registration.algorithm.closestSource.residualMedianSigmaClipping")
  //----------------------------------------------------------------------------
  private def matchSourceCentroid(referenceSourceSeq: Array[OccSource]
                                  , otherSourceSeq: Array[OccSource]
                                  , maxAllowedDistance: Double):Array[(Point2D_Double,Point2D_Double)] = {

    if (referenceSourceSeq.isEmpty || otherSourceSeq.isEmpty) return Array()
    val kdTreeOtherCentroid = K2d_TreeDouble()
    val otherSourceMap = scala.collection.mutable.Map[Int, OccSource]()
    val pairSeq = otherSourceSeq.map { otherSource =>
      otherSourceMap(otherSource.id) = otherSource
      (otherSource.centroid.toSequence(), otherSource.id)
    }
    kdTreeOtherCentroid.addSeq(pairSeq)

    referenceSourceSeq.flatMap { refSource =>
      val refCentroid = refSource.centroid
      val closestOtherSource = otherSourceMap(kdTreeOtherCentroid.getKNN(refCentroid.toSequence()).head.v)
      val otherCentroid = closestOtherSource.centroid
      val dist = otherCentroid.getDistance(refCentroid)
      if (dist <= maxAllowedDistance) Some((refCentroid, otherCentroid))
      else None
    }
  }
  //---------------------------------------------------------------------------
  //otheraAft to transform position of 'otherImage' in 'referenceImage'
  def calculate(refOccImage: OccImage
                , otherOccImage: OccImage
                , closestAlgorithmUserParamSeq: Option[(Int,Boolean,Double)] =
                   Some((maxAllowedPixDistanceMatch,useResidualMedianForAFT,residualMedianSigmaClipping))
  ): Option[AffineTransformation2D] = {

    val matchedSourceSeq = matchSourceCentroid(refOccImage.getSourceSeq()
                                               , otherOccImage.getSourceSeq()
                                               , closestAlgorithmUserParamSeq.get._1)
    if (matchedSourceSeq.isEmpty) None
    else buildAft(matchedSourceSeq
                  , closestAlgorithmUserParamSeq.get._2
                  , closestAlgorithmUserParamSeq.get._3)
  }

  //---------------------------------------------------------------------------
  //median of the sigma clipped values of the residual position
  def buildAft(residualPos: Array[Point2D_Double]
               , residualMedianSigmaClipping: Double): AffineTransformation2D = {
    val sigmaClipped_X_residualSeq = StatDescriptive.sigmaClippingDouble(residualPos map (_.x), sigmaScale = residualMedianSigmaClipping)
    val sigmaClipped_Y_residualSeq = StatDescriptive.sigmaClippingDouble(residualPos map (_.y), sigmaScale = residualMedianSigmaClipping)

    val residualPixX_Median = StatDescriptive.getWithDouble(sigmaClipped_X_residualSeq).median
    val residualPixY_Median = StatDescriptive.getWithDouble(sigmaClipped_Y_residualSeq).median

    AffineTransformation2D.buildTranslatedAft(residualPixX_Median, residualPixY_Median)
  }
  //---------------------------------------------------------------------------
  private def buildAft(matchedSourceSeq: Array[(Point2D_Double,Point2D_Double)]
                      , useResidualMedianForAFT: Boolean
                      , residualMedianSigmaClipping: Double): Option[AffineTransformation2D] = {
    if (matchedSourceSeq.isEmpty) None
    else {
      if (useResidualMedianForAFT) {
        val residualPos = matchedSourceSeq map (t => t._2 - t._1)
        Some(buildAft(residualPos, residualMedianSigmaClipping))
      }
      else
        AffineTransformation2D.umeyana2D(matchedSourceSeq map (_._1), matchedSourceSeq map (_._2))
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RegistrationByClosestSource.scala
//=============================================================================
