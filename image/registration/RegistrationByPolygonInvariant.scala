package com.common.image.registration
//=============================================================================
import com.common.image.myImage.MyImage
import com.common.image.nsaImage.geometry.asterism.MatchAsterism
import com.common.image.nsaImage.geometry.point.Point2D
import com.common.configuration.MyConf
import com.common.geometry.affineTransformation.AffineTransformation2D
import com.common.geometry.rectangle.RectangleDouble
import com.common.image.occultation.occImage.OccImage
import com.common.logger.MyLogger
import com.common.util.path.Path
//=============================================================================
import java.awt.Font
//=============================================================================
//=============================================================================
object RegistrationByPolygonInvariant extends  MyLogger {
  //---------------------------------------------------------------------------
  case class RegistrationResult(background: Double
                                , backgroundRMS: Double
                                , noiseTide: Double
                                , midObservingTime: String
                                , exposureTime: Double
                                , imageDim: Point2D[Double]
                                , raDecRectangle: RectangleDouble
                                , xPixScale: Double
                                , yPixScale: Double
                                , aft:AffineTransformation2D)
    //---------------------------------------------------------------------------
    private val conf = MyConf(MyConf.c.getString("Registration.configurationFile"))
    //---------------------------------------------------------------------------
    final val REGISTRATION_RESULT_TSV = "registration_result.tsv"

    final val REGISTRATION_TSV_COL_NAME                                = "image_name"
    final val REGISTRATION_TSV_BACKGROUND_COL_NAME                     = "background"
    final val REGISTRATION_TSV_BACKGROUND_RMS_COL_NAME                 = "rms"
    final val REGISTRATION_TSV_NOISE_TIDE_COL_NAME                     = "noise_tide"
    final val REGISTRATION_TSV_MID_OBSERVING_TIME_COL_NAME             = "mid_observing_time"
    final val REGISTRATION_TSV_EXPOSURE_COL_NAME                       = "exposure_time"

    final val REGISTRATION_TSV_AFT_IMAGE_X_PIX_COL_NAME                = "image_x_pix"
    final val REGISTRATION_TSV_AFT_IMAGE_Y_PIX_COL_NAME                = "image_y_pix"

    final val REGISTRATION_TSV_AFT_IMAGE_MIN_RA_COL_NAME               = "image_min_ra"
    final val REGISTRATION_TSV_AFT_IMAGE_MIN_DEC_COL_NAME              = "image_min_dec"

    final val REGISTRATION_TSV_AFT_IMAGE_MAX_RA_COL_NAME               = "image_max_ra"
    final val REGISTRATION_TSV_AFT_IMAGE_MAX_DEC_COL_NAME              = "image_max_dec"

    final val REGISTRATION_TSV_X_AXIS_PIX_SCALE_COL_NAME               = "x_axis_pix_scale"
    final val REGISTRATION_TSV_Y_AXIS_PIX_SCALE_COL_NAME               = "y_axis_pix_scale"

    final val REGISTRATION_TSV_AFT_COL_NAME                            = "aft"

    final val REGISTRATION_TSV_HEADER = Seq(
      REGISTRATION_TSV_COL_NAME
      , REGISTRATION_TSV_BACKGROUND_COL_NAME
      , REGISTRATION_TSV_BACKGROUND_RMS_COL_NAME
      , REGISTRATION_TSV_NOISE_TIDE_COL_NAME
      , REGISTRATION_TSV_MID_OBSERVING_TIME_COL_NAME
      , REGISTRATION_TSV_EXPOSURE_COL_NAME
      , REGISTRATION_TSV_AFT_IMAGE_X_PIX_COL_NAME
      , REGISTRATION_TSV_AFT_IMAGE_Y_PIX_COL_NAME
      , REGISTRATION_TSV_AFT_IMAGE_MIN_RA_COL_NAME
      , REGISTRATION_TSV_AFT_IMAGE_MIN_DEC_COL_NAME
      , REGISTRATION_TSV_AFT_IMAGE_MAX_RA_COL_NAME
      , REGISTRATION_TSV_AFT_IMAGE_MAX_DEC_COL_NAME
      , REGISTRATION_TSV_X_AXIS_PIX_SCALE_COL_NAME
      , REGISTRATION_TSV_Y_AXIS_PIX_SCALE_COL_NAME
      , REGISTRATION_TSV_AFT_COL_NAME
    )
    //---------------------------------------------------------------------------
    private final val COMMON_KEY = "Registration.algorithm.polygonInvariant."
    //---------------------------------------------------------------------------
    private final val DEBUG_REGISTRATION_DIR =  "output/debugging/registration"
    //---------------------------------------------------------------------------
    private final val maxSourceCountConsideredForMatching   = conf.getInt(COMMON_KEY + "maxSourceCountConsideredForMatching")

    private final val maxSourceCountConsideredForTesting    = conf.getInt(COMMON_KEY + "maxSourceCountConsideredForTesting")
    private final val percentageOfTestingSourcesToMatch     = conf.getInt(COMMON_KEY + "percentageOfTestingSourcesToMatch")

    private final val minAsterismOrder                      = conf.getInt(COMMON_KEY + "minAsterismOrder")

    //source detection
    private final val  sourceDetectionSigmaMultiplierStart  = conf.getDouble(COMMON_KEY + "sourceDetection.sigmaMultiplier.start")
    private final val  sourceDetectionSigmaMultiplierEnd    = conf.getDouble(COMMON_KEY + "sourceDetection.sigmaMultiplier.end")
    private final val  sourceDetectionSigmaMultiplierStep   = conf.getDouble(COMMON_KEY + "sourceDetection.sigmaMultiplier.step")

    private final val  sourceDetectionMinPixCount = conf.getInt(COMMON_KEY + "sourceDetection.minPixCount")
    private final val  sourceDetectionMaxPixCount = conf.getInt(COMMON_KEY + "sourceDetection.maxPixCount")

    //aft sample point testing
    private final val aftSamplePointTestToApply                      = conf.getInt(COMMON_KEY + "aftSamplePointTest.testToApply")
    private final val aftSamplePointTestMinPassPercentage            = conf.getInt(COMMON_KEY + "aftSamplePointTest.minPassPercentage")
    private final val aftSamplePointTestMinAllowedPixelDistance      = conf.getDouble(COMMON_KEY + "aftSamplePointTest.minAllowedPixelDistance")
    //-------------------------------------------------------------------------
    //registration by astroalign (fallback)
    private final val REGISTRATION_ASTROALIGN_PY_CALL = conf.getString("Registration.algorithm.astroAlignPy.call")
    //-------------------------------------------------------------------------
    def calculate(catalogOccImage: OccImage
                  , sampleOccImage: OccImage
                  , verbose: Boolean): Option[AffineTransformation2D] = {
      val r = calculate(MyImage(catalogOccImage.path)
               , MyImage(sampleOccImage.path)
               , verbose)
      if (r.isDefined) Some(r.get.aft)
      else None
    }
  //---------------------------------------------------------------------------
  //return the affine transformation to transform a point in sample point sequence
  // to be converted into a point in catalog point
  //sometimes it is required to iterate through several sigma clipping values
  // to restrict the detected sources
    def calculate(catalogImage: MyImage
                  , sampleImage: MyImage
                  , verbose: Boolean):Option[RegistrationResult] = {

      val step = if (sourceDetectionSigmaMultiplierStart > sourceDetectionSigmaMultiplierEnd) -sourceDetectionSigmaMultiplierStep
      else sourceDetectionSigmaMultiplierStep
      val sigmaMultipliers =  BigDecimal(sourceDetectionSigmaMultiplierStart) to BigDecimal(sourceDetectionSigmaMultiplierEnd) by step

      sigmaMultipliers
        .iterator
        .foreach { sigmaMultiplier =>
          val (catalogNoiseTide, catalogSourceRestriction) = getNoiseTideAndSourceRestriction(catalogImage, sigmaMultiplier.toDouble)
          val (sampleNoiseTide, sampleSourceRestriction) = getNoiseTideAndSourceRestriction(sampleImage, sigmaMultiplier.toDouble)

          val r = matchAsterism(getCentroidSeq(catalogImage, catalogNoiseTide, catalogSourceRestriction, sigmaMultiplier.toDouble, verbose)
                              , getCentroidSeq(sampleImage, sampleNoiseTide, sampleSourceRestriction, sigmaMultiplier.toDouble, verbose))

          if (r._1.isDefined) {
            //get the default values of source detection of the image
            // , because it will be affected after the aft application
            val (samBackground, samBackgroundRMS, samNoiseTide, _ , _) = sampleImage.getSourceDetectionFromConfiguration()
            sampleImage.setNoiseTide(samNoiseTide)
            val aft = r._1.get
            sampleImage.setAft(aft)

            val raDecRec = sampleImage.getRaDecRectangle()

            return Some(RegistrationResult(
                samBackground
              , samBackgroundRMS
              , samNoiseTide
              , sampleImage.getObservingDateMidPoint().toString
              , sampleImage.getExposureTime()
              , Point2D[Double](sampleImage.getDimension().x, sampleImage.getDimension().y)
              , RectangleDouble(raDecRec.min.x, raDecRec.min.y
                                , raDecRec.max.x, raDecRec.max.y)
              , sampleImage.getPixScaleX()
              , sampleImage.getPixScaleY()
              , aft))
          }
        }
      None
    }
    //---------------------------------------------------------------------------
    private def getNoiseTideAndSourceRestriction(img: MyImage, sigmaMultiplier: Double) = {
      (img.getBackground() + (img.getBackgroundRMS() * sigmaMultiplier)
        , Some((sourceDetectionMinPixCount, sourceDetectionMaxPixCount)))
    }
    //---------------------------------------------------------------------------
    private def getCentroidSeq(img: MyImage
                               , noiseTide: Double
                               , sourceRestriction: Option[(Int,Int)]
                               , sigmaMultiplier: Double
                               , verbose: Boolean) = {
      val sourceSeq = img
        .findSourceSeq(noiseTide, sourceSizeRestriction = sourceRestriction)
        .toSegment2D_seq()

      if (verbose) {
        val dir = Path.createDirectoryIfNotExist(s"$DEBUG_REGISTRATION_DIR/$sigmaMultiplier/")
        MyImage.synthesizeImage(s"$dir/${img.getRawName()}"
          , sourceSeq
          , img.getDimension()
          , font = Some(new Font(Font.MONOSPACED, Font.PLAIN, 10))
        )
      }

      sourceSeq
        .sortWith(_.getFlux() > _.getFlux())
        .map(_.getCentroid())
        .map(p => Point2D[Double](p.x, p.y))
    }
    //---------------------------------------------------------------------------
    private def matchAsterism(sourceCatalogCentroidSeq: Array[Point2D[Double]]
                             , sampleCatalogCentroidSeq: Array[Point2D[Double]])
    : (Option[AffineTransformation2D],Double,Double) = {

      val maxAsterismPolygonOrder =
        Math.min(Math.min(sourceCatalogCentroidSeq.length,sampleCatalogCentroidSeq.length)
                ,Math.round(maxSourceCountConsideredForMatching * percentageOfTestingSourcesToMatch * 0.01))

      val matchAsterism =
        MatchAsterism(
          sourceCatalogCentroidSeq.take(maxSourceCountConsideredForMatching) map (p => Point2D[Double](p.x, p.y))
          , sampleCatalogCentroidSeq.take(maxSourceCountConsideredForMatching) map (p => Point2D[Double](p.x, p.y))
          , sourceCatalogCentroidSeq.take(maxSourceCountConsideredForTesting) map(p => Point2D[Double](p.x, p.y))
          , sampleCatalogCentroidSeq.take(maxSourceCountConsideredForTesting) map (p => Point2D[Double](p.x, p.y))
          , maxAsterismPolygonOrder.toInt
          , minAsterismPolygonOrder = minAsterismOrder
          , AFT_SamplePointTestToApply = aftSamplePointTestToApply
          , AFT_SamplePointTestMinPassPercentage = aftSamplePointTestMinPassPercentage
          , AFT_SamplePointTestMinAllowedPointDistance = aftSamplePointTestMinAllowedPixelDistance)

      matchAsterism.calculate()
    }
    //---------------------------------------------------------------------------
  }
//=============================================================================
//=============================================================================
//End of file RegistrationByPolygonInvariant.scala
//=============================================================================