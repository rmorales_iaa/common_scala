/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/Jun/2020
 * Time:  09h:09m
 * Description: None
 */
//=============================================================================
package com.common.image.registration
//=============================================================================
import com.common.configuration.MyConf
import com.common.geometry.affineTransformation.AffineTransformation2D
import com.common.hardware.cpu.CPU
import com.common.image.myImage.MyImage
import com.common.image.occultation.occImage.OccImage
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
//=============================================================================
import java.util.concurrent.ConcurrentHashMap
//=============================================================================
//=============================================================================
object Registration extends MyLogger {
  //---------------------------------------------------------------------------
  sealed trait RegistrationAlgorithm
  case object REGISTRATION_TRIANGLE_INVARIANT extends RegistrationAlgorithm
  case object REGISTRATION_POLYGON_INVARIANT extends RegistrationAlgorithm
  case object REGISTRATION_BY_ASTROALIGN_PY extends RegistrationAlgorithm
  case object REGISTRATION_BY_CLOSEST_SOURCE extends RegistrationAlgorithm
  case object REGISTRATION_BY_ASTROMETRY extends RegistrationAlgorithm
  //---------------------------------------------------------------------------
  private val conf = MyConf(MyConf.c.getString("Registration.configurationFile"))
  //---------------------------------------------------------------------------
  private final val REGISTRATION_ALGORITHM_SEQ                    = conf.getIntSeq("Registration.algorithmSeq") map (getAlgorithm(_))
  //---------------------------------------------------------------------------
  private final val REGISTRATION_MIN_OVERLAPPING_PERCENTAGE       = conf.getFloat("Registration.minOverlappingPercentage")
  //---------------------------------------------------------------------------
  //registration by astroalign
  private final val REGISTRATION_ASTROALIGN_PY_CALL = conf.getString("Registration.algorithm.astroAlignPy.call")
  //---------------------------------------------------------------------------
  //registration by closest source
  private final val REGISTRATION_ALGORITHM_CLOSEST_MAX_PIX        = conf.getInt("Registration.algorithm.closestSource.maxAllowedPixDistanceMatch")
  private final val REGISTRATION_ALGORITHM_CLOSEST_USE_MEDIAN     = conf.getInt("Registration.algorithm.closestSource.useResidualMedianForAFT") == 1
  private final val REGISTRATION_ALGORITHM_CLOSEST_SIGMA_CLIPPING = conf.getDouble("Registration.algorithm.closestSource.residualMedianSigmaClipping")

  private final val REGISTRATION_ALGORITHM_CLOSEST_PARAM_SEQ = (
      REGISTRATION_ALGORITHM_CLOSEST_MAX_PIX
    , REGISTRATION_ALGORITHM_CLOSEST_USE_MEDIAN
    , REGISTRATION_ALGORITHM_CLOSEST_SIGMA_CLIPPING)
  //---------------------------------------------------------------------------
  //No pixels values are modified, only its position
   def getAlgorithm(algorithmCode: Int) = {
    algorithmCode match {
      case 1  => REGISTRATION_BY_ASTROALIGN_PY
      case 2  => REGISTRATION_TRIANGLE_INVARIANT
      case 3  => REGISTRATION_POLYGON_INVARIANT
      case 4  => REGISTRATION_BY_CLOSEST_SOURCE
      case 5  => REGISTRATION_BY_ASTROMETRY
      case _  => REGISTRATION_BY_ASTROALIGN_PY   //default
    }
  }
  //---------------------------------------------------------------------------
  def calculateWithFallbackAlgorithm(refImage: OccImage
                                     , otherImage: OccImage
                                     , algorithmSeq: Seq[Registration.RegistrationAlgorithm] = REGISTRATION_ALGORITHM_SEQ
                                     , closestAlgorithmUserParamSeq: Option[(Int,Boolean,Double)] = Some(REGISTRATION_ALGORITHM_CLOSEST_PARAM_SEQ)
                                     , verbose: Boolean = false
                                    ): Option[(AffineTransformation2D, Double)] = {

    algorithmSeq.zipWithIndex.foreach { case (algorithm,i) =>

      val r = calculate(refImage
                        , otherImage
                        , algorithm
                        , closestAlgorithmUserParamSeq, verbose)

      //if registration algorithm succedds then return else try a fallback
      if(r.isDefined) return r
      else {
        val nextStep = if (i != algorithmSeq.size -1) s". Trying new algorithm: '${algorithmSeq(i+1)}'" else ""
        if (verbose) warning (s"Registration algorithm: '$algorithm' has failed registering '${refImage.getShortName}' and '${otherImage.getShortName}' $nextStep")
      }
    }
    None
  }
  //---------------------------------------------------------------------------
  //calculate the transformation (translation, rotation and scale) of  image 'img'
  // regarding the image 'referenceImage' using a registration algorithm
  //In other words, calculate the transformation required to map a position in
  //image 'img' to a position in image 'referenceImage'
  //return (affineTransformation,overlapping percentage)
  def calculate(refImage: OccImage
                , otherImage: OccImage
                , algorithm: Registration.RegistrationAlgorithm
                , closestAlgorithmUserParamSeq: Option[(Int,Boolean,Double)] = None
                , verbose: Boolean
               )
    :  Option[(AffineTransformation2D, Double)] = {
    //-------------------------------------------------------------------------
    var aft: AffineTransformation2D = null
    try {
      aft = register(refImage
                     , otherImage
                     , algorithm
                     , closestAlgorithmUserParamSeq
                     , verbose).getOrElse(return None)
    }
    catch {
      case e: Exception =>
        error(s"Error registering reference image:'${refImage.getShortName()}' and image:'${otherImage.getShortName()}' ")
        e.printStackTrace()
      return None
    }
    val (_, _, width, height) = MyImage.aftOnDimensions(aft, refImage.xPixMax, refImage.yPixMax)
    val overlappingPercentage = refImage.getOverlappingPercentage(width, height)
    if (aft.isValid && aft.isNotUnit && overlappingPercentage >= REGISTRATION_MIN_OVERLAPPING_PERCENTAGE) {
      //compose and set the affine transformation on the other (right image)
      otherImage.composeRightAft(aft)
      Some((aft, overlappingPercentage))
    }
    else None
  }
  //---------------------------------------------------------------------------
  private def register(refImage: OccImage
                       , otherImage: OccImage
                       , algorithm: Registration.RegistrationAlgorithm
                       , closestAlgorithmUserParamSeq: Option[(Int,Boolean,Double)] = None
                       , verbose: Boolean): Option[AffineTransformation2D] = {

    algorithm match {
      case REGISTRATION_BY_ASTROALIGN_PY =>
        RegistrationByAstroAlignPy.calculate(otherImage
                                            , refImage
                                            , REGISTRATION_ASTROALIGN_PY_CALL) //imageA and imageB must exists in disk

      case REGISTRATION_TRIANGLE_INVARIANT =>
        RegistrationByTriangleInvariant.calculate(otherImage
                                                  , refImage
                                                  , findRansacBestResult = true
                                                  , verbose = verbose)

      case REGISTRATION_POLYGON_INVARIANT =>
        RegistrationByPolygonInvariant.calculate(refImage
                                                 , otherImage
                                                 , verbose)

      case REGISTRATION_BY_CLOSEST_SOURCE =>
        RegistrationByClosestSource.calculate(otherImage
                                              , refImage
                                              , closestAlgorithmUserParamSeq)

      case REGISTRATION_BY_ASTROMETRY =>
        RegistrationByAstrometry.calculate(refImage
                                          , otherImage)
    }
  }

  //---------------------------------------------------------------------------
  def registerImageDir(inputDir: String
                      , registrationResultMap: ConcurrentHashMap[String, AffineTransformation2D]
                      , algorithmSeq: Seq[Registration.RegistrationAlgorithm] = REGISTRATION_ALGORITHM_SEQ)
                   : List[String] = {
    //-------------------------------------------------------------------------
    class ImagePairRegistration(imageNamePairSeq: Array[(String, String)])
      extends ParallelTask[(String, String)](
        imageNamePairSeq
        , CPU.getCoreCount()
        , isItemProcessingThreadSafe = true
        , randomStartMaxMsWait = 100
        , verbose = true
        , message = Some("registering images")) {
      //-------------------------------------------------------------------------
      def userProcessSingleItem(imageNamePair: (String, String)): Unit = {
        val refImage = MyImage(imageNamePair._1)
        val otherImage = MyImage(imageNamePair._2)

        val aft = calculateWithFallbackAlgorithm(OccImage(refImage)
                                                , OccImage(otherImage)
                                                , algorithmSeq)
        if (aft.isDefined)
          registrationResultMap.put(otherImage.getRawName(), aft.get._1)
        else
          error(s"Can not find a transformation between reference image:'${refImage.getRawName()}' and image:'${otherImage.getRawName()}'")
      }
      //-------------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    val imageNameSeq = Path.getSortedFileList(inputDir
      , MyConf.c.getStringSeq("Common.fitsFileExtension"))
      .map(_.getAbsolutePath)
      .sortWith(_ < _)

    if (imageNameSeq.length < 2) {
      error(s"Not enough images:'${imageNameSeq.length}'");
      return List()
    }
    new ImagePairRegistration(imageNameSeq.sliding(2).toArray.map(l => (l.head, l.last)))
    imageNameSeq
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Registration.scala
//=============================================================================