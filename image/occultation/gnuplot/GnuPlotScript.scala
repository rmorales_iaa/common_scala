/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Apr/2023
 * Time:  21h:20m
 * Description: None
 */
//=============================================================================
package com.common.image.occultation.gnuplot
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object GnuPlotScript {
  //---------------------------------------------------------------------------
  private def normalizeNameGnuPlotName(s: String) = {
    val r = s.replaceAll("_", """\\\\_""")
    if (r.startsWith("'")) r else "'" + r + "'"
  }
  //---------------------------------------------------------------------------
  def plotSourceEvolution(sourceName: String, gnuplotFileName: String, csvDataFilename: String) = {
    val s =
      s"""
#start of gnu script
#--------------------------------------
set datafile separator "\t"
set encoding utf8
#--------------------------------------
#variables
my_title="{/:Bold Object and source: ${normalizeNameGnuPlotName(sourceName)}}"
my_x_axis_label='Observing time'
my_y_axis_label='Flux per second'

col_x      = 3
col_flux_s = 6 #flux per second
#--------------------------------------
my_data_file="$csvDataFilename"
#--------------------------------------
#fonts
my_axis_ticks_font = "Courier-New,9"
my_axis_title_font = "Times-Roman:Bold,12"
#--------------------------------------
#colors
#background
my_plot_background_color = "#b0afaa"
#--------------------------------------
#title
set title sprintf("{/Arial:Bold=14 %s}", my_title)
#--------------------------------------
#plot background color
set object 1 rectangle from screen 0,0 to screen 1,1 fillcolor rgb my_plot_background_color behind
#--------------------------------------
set xlabel my_x_axis_label
set ylabel my_y_axis_label
#--------------------------------------
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
set xtic auto                          # set xtics automatically
set ytic auto                          # set ytics automatically
set grid
#--------------------------------------
#no fit log file
set fit quiet
set fit logfile '/dev/null'
#--------------------------------------
#set the stats columns in the data file
stats my_data_file using col_flux_s nooutput
#--------------------------------------
#axis
set xtics font my_axis_ticks_font
set ytics font my_axis_ticks_font

set xtic auto
set ytic auto

set xdata time
set timefmt "%Y-%m-%dT%H:%M:%S"
set format x "%H:%M:%S"
set xtics rotate 90

set xlabel my_x_axis_label font my_axis_title_font
set ylabel my_y_axis_label font my_axis_title_font
#--------------------------------------
plot my_data_file using col_x:col_flux_s with linespoints lc rgb 'blue' title 'Source flux per second', \\
     STATS_median lc rgb 'dark-violet' title 'Median flux per sercond', \\
     my_data_file using col_x:col_flux_s smooth bezier with lines lc rgb 'red' title 'Bezier interpolation'
#--------------------------------------
pause -1
#--------------------------------------
#end of gnu script"""

    val fileName = gnuplotFileName
    val bw = new BufferedWriter(new FileWriter(new File(fileName)))
    bw.write(s)
    bw.close()
  }
  //---------------------------------------------------------------------------
  def plotOccultation(sourceName: String
                      , imageCount: Int
                      , referenceSourceCount: Int
                      , medianStdevFluxRelative: String
                      , location: String
                      , timeRange: String
                      , apertureInfo: String
                      , gnuplotFileName: String
                      , csvDataFilename: String) = {
    val s =
      s"""
#start of gnu script
#--------------------------------------
set datafile separator "\t"
set encoding utf8
#--------------------------------------
#variables
my_title="{/'Times New Roman':Bold=13 Occultation of object: ${normalizeNameGnuPlotName(sourceName)}}\\n{/'Times New Roman':Normal=11 Location: '$location' {/'Times New Roman':Normal=11 $timeRange}\\n{/'Times New Roman':Normal=11 Using: $imageCount images and $referenceSourceCount common sources with a std dev: $medianStdevFluxRelative}\\n{/'Times New Roman':Normal=11 Apertures: $apertureInfo}"

my_x_axis_label='Observing time'
my_y_axis_label='Normalized flux per second'

col_x          = 3
col_focus_flux = 4 #flux per second
col_SNR        = 5
#--------------------------------------
my_data_file="$csvDataFilename"
#--------------------------------------
#fonts
my_axis_ticks_font = "Courier-New,9"
my_axis_title_font = "Times-Roman:Bold,12"
#--------------------------------------
#colors
#background
my_plot_background_color = "#b0afaa"
#--------------------------------------
#title
set title my_title font "Arial,14"
set font "Times-Roman,12"
#--------------------------------------
#plot background color
set object 1 rectangle from screen 0,0 to screen 1,1 fillcolor rgb my_plot_background_color behind
#--------------------------------------
set xlabel my_x_axis_label
set ylabel my_y_axis_label
#--------------------------------------
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
set grid
#--------------------------------------
#no fit log file
set fit quiet
set fit logfile '/dev/null'
#--------------------------------------
#set the stats columns in the data file
stats my_data_file using col_focus_flux nooutput
#--------------------------------------
#axis
set xtic auto                          # set xtics automatically
set ytic auto                          # set ytics automatically

set xlabel my_x_axis_label font my_axis_title_font
set ylabel my_y_axis_label font my_axis_title_font

set xdata time
set timefmt "%Y-%m-%dT%H:%M:%S"
set format x "%H:%M:%S"
set xtics rotate 90
set xtics 30 #show tics each 30 seconds

#time range example
#set xtics 2  #show tics each 2 seconds
#set xrange ["2011-04-23T01:34:50":"2011-04-23T01:37:00"]
#--------------------------------------
plot my_data_file using col_x:col_focus_flux with lines  lc rgb 'forest-green' title 'Focus flux per second', \\
           my_data_file using col_x:col_focus_flux:col_SNR  with yerrorbars pt 7 lc rgb 'blue' notitle, \\
           STATS_median lc rgb 'dark-violet' title 'Focus median flux per second'
#--------------------------------------
pause -1
#--------------------------------------
#end of gnu script"""

    val fileName = gnuplotFileName
    val bw = new BufferedWriter(new FileWriter(new File(fileName)))
    bw.write(s)
    bw.close()
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//=============================================================================
//End of file GnuPlotScript.scala
//=============================================================================
