/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Apr/2023
 * Time:  12h:07m
 * Description: None
 */
//=============================================================================
package com.common.image.occultation.occSource
//=============================================================================
import com.common.csv.CsvRead
import com.common.dataType.pixelDataType.PixelDataType.PIXEL_DATA_TYPE
import com.common.database.mongoDB.database.MatchedImageSource
import com.common.database.mongoDB.database.gaia.GaiaPhotometricSource
import com.common.geometry.point.Point2D_Double
import com.common.image.occultation.occImage.OccImage
//=============================================================================
//=============================================================================
object OccSource {
  //---------------------------------------------------------------------------
  final val COL_NAME_SOURCE_ID              = "source_id"
  final val COL_NAME_CENTROID_X             = "centroid_x"
  final val COL_NAME_CENTROID_Y             = "centroid_y"
  final val COL_NAME_PIX_SIZE               = "pix_size"
  final val COL_NAME_FLUX_PER_SECOND        = "flux_per_second"
  final val COL_NAME_FLUX_SNR               = "flux_snr"

  final val COL_NAME_SEQ = Seq(
      COL_NAME_SOURCE_ID
    , COL_NAME_CENTROID_X
    , COL_NAME_CENTROID_Y
    , COL_NAME_PIX_SIZE
    , COL_NAME_FLUX_PER_SECOND
    , COL_NAME_FLUX_SNR
  )
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String = "") =
    classOf[OccSource].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  def apply(source: MatchedImageSource
            , occImage: OccImage): OccSource = {
    val centroid = source.imageSource.getCentroid()
    val pixSize = source.imageSource.getPixCount()
    val fluxPerSecond = source.catalogSource.asInstanceOf[GaiaPhotometricSource].getFluxPerSecond()
    val fluxSNR = source.catalogSource.asInstanceOf[GaiaPhotometricSource].getFluxSNR()
    val occSource = OccSource(source.imageSource.id
                              , centroid
                              , pixSize
                              , fluxPerSecond
                              , fluxSNR
                              , occImage)
    occSource
  }

  //---------------------------------------------------------------------------
  def apply(pixPos: Point2D_Double
            , pixValue: PIXEL_DATA_TYPE
            , occImage: OccImage): OccSource = {

    val source = MatchedImageSource(pixPos)
    val centroid = pixPos
    val pixSize = 1
    val fluxPerSecond = pixValue / occImage.exposureTime
    val fluxSNR = 0
    OccSource(
        source.imageSource.id
      , centroid
      , pixSize
      , fluxPerSecond
      , fluxSNR
      , occImage)
  }

  //---------------------------------------------------------------------------
  def load(csvName: String
           , occImage: OccImage) = {
    val csv = CsvRead(csvName)
    csv.read()
    val colNameSeq = OccSource.COL_NAME_SEQ
    csv.getRowSeq.map { row =>
      new OccSource(
         row.getInt(colNameSeq(0))
        , Point2D_Double(row.getDouble(colNameSeq(1)), row.getDouble(colNameSeq(2)))
        , row.getInt(colNameSeq(3))
        , row.getDouble(colNameSeq(4))
        , row.getDouble(colNameSeq(5))
        , occImage
      )
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class OccSource(id: Int
                     , centroid: Point2D_Double
                     , pixSize: Int
                     , var fluxPerSecond: Double
                     , fluxPersSecondSNR: Double
                     , occImage: OccImage
                    ) {
  //---------------------------------------------------------------------------
  private var relativeFluxPerSecond     = Double.NaN
  private var relativeFluxPerSecondSeq: Array[Double] = Array()
  private var relativeFluxPerSecondSNR  = Double.NaN
  //---------------------------------------------------------------------------
  def getAsCsvLine(sep: String) =
    id + sep +
    centroid.x + sep +
    centroid.y + sep +
    pixSize + sep +
    fluxPerSecond  + sep +
    fluxPersSecondSNR
  //---------------------------------------------------------------------------
  def getRelativeFluxPerSecond = relativeFluxPerSecond
  //---------------------------------------------------------------------------
  def setRelativeFluxPerSecond(f: Double) = relativeFluxPerSecond = f
  //---------------------------------------------------------------------------
  def getRelativeFluxSeq() = relativeFluxPerSecondSeq
  //---------------------------------------------------------------------------
  def setRelativeFluxSeq(seq: Array[Double]) = relativeFluxPerSecondSeq = seq
  //---------------------------------------------------------------------------
  def getRelativeFluxSNR() = relativeFluxPerSecondSNR
  //---------------------------------------------------------------------------
  def setRelativeFluxSNR(f: Double) = relativeFluxPerSecondSNR = f
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file OccSource.scala
//=============================================================================
