/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Apr/2023
 * Time:  19h:36m
 * Description: None
 */
//=============================================================================
package com.common.image.occultation.occSource
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.image.myImage.MyImage
import com.common.image.occultation.{OccConfiguration, SourceOccultation}
import com.common.image.occultation.gnuplot.GnuPlotScript
import com.common.image.occultation.occImage.{OccImage, OccImageAperturePhotometry, OccSourceMatch}
import com.common.logger.MyLogger
import com.common.stat.StatDescriptive
import com.common.util.path.Path
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object OccSourceEvolution extends MyLogger {
  //---------------------------------------------------------------------------
  val sep = "\t"
  //---------------------------------------------------------------------------
  final val COL_NAME_SOURCE_ID                = OccSource.COL_NAME_SOURCE_ID
  final val COL_NAME_SOURCE_FLUX_PER_SECOND   = OccSource.COL_NAME_FLUX_PER_SECOND

  final val COL_NAME_SEQ = Seq(
    OccImage.COL_NAME_IMAGE_NAME
    , OccImage.COL_NAME_OBSERVING_TIME_MID_POINT_JD
    , OccImage.COL_NAME_OBSERVING_TIME
    , COL_NAME_SOURCE_ID
    , OccSource.COL_NAME_CENTROID_X
    , OccSource.COL_NAME_CENTROID_Y
    , COL_NAME_SOURCE_FLUX_PER_SECOND
    , "relative_flux"
  )
  //---------------------------------------------------------------------------
  final val SOURCE_CSV     = "source.csv"
  final val SOURCE_GNUPLOT = "source.gnuplot"
  //---------------------------------------------------------------------------
  def find(occImageSeq: Array[OccImage]
           , inputDir: String
           , sourceEvolutionDir: String
           , occConf: OccConfiguration): Array[OccSourceEvolution] = {

    val occEvolutionSeq = OccImage.findEvolutionSeq(occImageSeq
                                                    , Path.resetDirectory(sourceEvolutionDir)
                                                    , occConf)
    OccSourceEvolution.generateSummary(s"$sourceEvolutionDir/${SourceOccultation.SOURCE_EVOLUTION_STATS_CSV}", occEvolutionSeq)
    occEvolutionSeq
  }
  //---------------------------------------------------------------------------
  def saveSeq(focusName: String
              , focus: OccSource
              , occEvolutionSeq: Array[OccSourceEvolution]
              , sourceEvolutionDir: String) = {
    //save csv
    occEvolutionSeq.foreach { sourceEvolution => sourceEvolution.saveAsCsv(s"$sourceEvolutionDir/", focus.id) }

    //save png
    occEvolutionSeq.foreach { occEvolution =>
      val id = occEvolution.sourceSeq.head.id
      val suffix =
        if (occEvolution.refSource.id == focus.id) {
         Path.deleteDirectoryIfExist(s"$sourceEvolutionDir/source_$id")
        "_focus"
        }
        else ""
      val rootName = s"$sourceEvolutionDir/source_$id$suffix/"

      GnuPlotScript.plotSourceEvolution(
        s"${focusName}_$id"
        , s"$rootName/$SOURCE_GNUPLOT"
        , Path.getCurrentPath() + s"$rootName/$SOURCE_CSV")
    }
  }
  //---------------------------------------------------------------------------
  private def generateSummary(summaryCsv: String
                              , evolutionSeq: Array[OccSourceEvolution]) = {
    val bw = new BufferedWriter(new FileWriter(new File(summaryCsv)))
    val colNameSeq = Seq("source_id"
      , "flux_per_second_stdev"
      , "flux_per_second_average"
      , "flux_per_second_min"
      , "flux_per_second_max"
      , "flux_per_second_median")

    val headerSource = colNameSeq.mkString(sep) + "\n"
    bw.write(headerSource)
    evolutionSeq.sortWith(_.fluxPerSecondStats.stdDev > _.fluxPerSecondStats.stdDev).foreach { evolution =>
      val relativeFluxStats = evolution.fluxPerSecondStats
      bw.write(
        evolution.sourceSeq.head.id+ sep + +
          relativeFluxStats.stdDev + sep +
          relativeFluxStats.average + sep +
          relativeFluxStats.min + sep +
          relativeFluxStats.max + sep +
          relativeFluxStats.median + sep +
          "\n")
    }
    bw.close
  }
  //---------------------------------------------------------------------------
  private def savedNotMatchedSource(occImage: OccImage
                                    , currentRefImage: OccImage
                                    , currentRefSource: OccSource
                                    , notMatchedReferenceSourceDir: String
                                    , notMatchedReferenceSourceCsv: String
                                    , sourceMatch: Option[OccSourceMatch]) =  {
    Path.createDirectoryIfNotExist(notMatchedReferenceSourceDir)
    val bw = new BufferedWriter(new FileWriter(new File(notMatchedReferenceSourceCsv)))
    bw.write(OccSourceMatch.COL_NAME_SEQ.mkString(sep) + "\n")
    if (sourceMatch.isDefined) bw.write(sourceMatch.get.getAsCsvLine(sep))
    else
      bw.write(
          occImage.getShortName + sep +         //COL_NAME_SOURCE_IMAGE_NAME
          currentRefImage.getShortName + sep +  //COL_NAME_REF_IMAGE_NAME
          s"${currentRefSource.id}" + sep +     //COL_NAME_REF_SOURCE_ID

          "none" + sep +                        //COL_NAME_CLOSEST_SOURCE_ID
          "none" + sep +                        //COL_NAME_DIST_TO_REF_SOURCE

          "none" + sep +                        //COL_NAME_CLOSEST_SOURCE_CENTROID
          s"${currentRefSource.centroid.getFormattedShortString()}" + sep + //COL_NAME_REF_SOURCE_CENTROID

          "none" + sep +                        //COL_NAME_CLOSEST_SOURCE_FLUX_PER_SECOND
          s"${currentRefSource.fluxPerSecond}" + sep +                        //COL_NAME_REF_SOURCE_FLUX_PER_SECOND

          "none" + sep +                          //COL_NAME_CLOSEST_SOURCE_PIX_SIZE
          s"${currentRefSource.pixSize}" + sep +  //COL_NAME_REF_SOURCE_PIX_SIZE

           s"${occImage.getAft()(currentRefSource.centroid).getFormattedShortString()}" + sep + //COL_NAME_AFT_REF_SOURCE_CENTROID
          "\n")
    bw.close()
  }
  //---------------------------------------------------------------------------
  private def getSourceMatch(refImage: OccImage
                             , occImage: OccImage
                             , refSource: OccSource
                             , occConf: OccConfiguration) = {
    if (refImage.hasWcs && occImage.hasWcs) { //use wcs to match sources
      val refRaDec = refImage.wcs.get.pixToSky(refSource.centroid)
      val estimatedPixPos = occImage.wcs.get.skyToPix(refRaDec)
      val (found, (matchedSource, dist)) = occImage.findClosestSourceByPixPos(estimatedPixPos,occConf.MAX_ALLOWED_PIX_DISTANCE_MATCH)
      if (found) Some(OccSourceMatch(occImage, refImage, matchedSource, refSource, dist, valid = found))
      else None
    }
    else
       occImage.getRefSourceMatch(refSource.id)
  }
  //---------------------------------------------------------------------------
  def find(remainImageSeq: Array[OccImage]
           , refImage: OccImage
           , refSource: OccSource
           , notMatchedReferenceSourceDir: String
           , occConf: OccConfiguration): Array[OccSource] = {
    val notMatchedReferenceSourceCsv = s"$notMatchedReferenceSourceDir/${refSource.id}.csv"
    var currentRefImage = refImage
    var currentRefSource = refSource

    val evolutionSeq = ArrayBuffer[OccSource](refSource)
    remainImageSeq.foreach { occImage =>
      val sourceMatch = getSourceMatch(currentRefImage
                                      , occImage
                                      , currentRefSource
                                      , occConf)
      if (sourceMatch.isDefined && sourceMatch.get.valid) {
        evolutionSeq += sourceMatch.get.closestSource
        currentRefSource = evolutionSeq.last
        currentRefImage = occImage
      }
      else {
        savedNotMatchedSource(
            occImage
          , currentRefImage
          , currentRefSource
          , notMatchedReferenceSourceDir
          , notMatchedReferenceSourceCsv
          , sourceMatch)
        return Array()
      }
    }
    evolutionSeq.toArray
  }
  //---------------------------------------------------------------------------
  def filterSeq(sourceEvolutionSeq: Array[OccSourceEvolution]
                , discardedOutputDir: String
                , occConf: OccConfiguration) = {
    val discardedSourceEvolutionSeq = sourceEvolutionSeq.filterNot(evolution=>
      evolution.fluxPerSecondStats.stdDev < occConf.MAX_ALLOWED_NORMALIZED_STD_DEV)

    discardedSourceEvolutionSeq.foreach { souceEvolution =>
      souceEvolution.saveAsCsv(discardedOutputDir)
    }

    val discardedCommonSourcSeqID = discardedSourceEvolutionSeq.map(_.sourceSeq.head.id).mkString("{",",","}")
    if (discardedSourceEvolutionSeq.size > 0)
     warning(s"Discarded: ${discardedSourceEvolutionSeq.size} common sources due to big std dev on normalized flux (max allowed: ${occConf.MAX_ALLOWED_NORMALIZED_STD_DEV}): $discardedCommonSourcSeqID")
    else  {
      Path.deleteDirectoryIfExist(discardedOutputDir)
      warning("None common sources discarded in the filtering")
    }
    sourceEvolutionSeq.filter(evolution=> evolution.fluxPerSecondStats.stdDev <= occConf.MAX_ALLOWED_NORMALIZED_STD_DEV)
  }
  //---------------------------------------------------------------------------
  //estimate the centroid using previous image and the aft of the current image
  private def getEstimatedCentroidFocus(occImage: OccImage
                                       , occImageSeq: Array[OccImage]
                                       , i: Int) = {
    val prevFocus = occImageSeq(i).getFocus()
    val prevImgFocusCentroid = prevFocus.centroid //i and not i-1 becasue we dropped the very first one
    occImage.getAft()(prevImgFocusCentroid)
  }
  //---------------------------------------------------------------------------
  private def getNewFocus(occImage: OccImage
                          , occImageSeq: Array[OccImage]
                          , i: Int
                          , currentRefSource: OccSource
                          , occConf: OccConfiguration
                          , verbose: Boolean): OccSource = {

    if (!occConf.FOCUS_PIX_MATCH_CLOSEST_SOURCE) {
      val estCentroidFocus = getEstimatedCentroidFocus(occImage, occImageSeq, i)
      val pixValue = MyImage(occImage.path).get(Math.round(estCentroidFocus.x).toInt
                                              , Math.round(estCentroidFocus.y).toInt)
      return OccSource(estCentroidFocus, pixValue, occImage)
    }

    var newFocus: OccSource = null
    if (currentRefSource.id == -1) { //previous image also have no focus, try to find it by position
      val (found, (closestSource, _)) = occImage.findClosestSourceByPixPos(occImage.getAft()(currentRefSource.centroid)
        , occConf.MAX_ALLOWED_PIX_DISTANCE_MATCH)
      if (found) newFocus = closestSource
    }

    if (newFocus == null) {
      val estCentroidFocus = getEstimatedCentroidFocus(occImage, occImageSeq, i)
      if (verbose) info(s"Image:'${occImage.getShortName}' missing focus but estimated focus position:${estCentroidFocus.getFormattedShortString()}")

      //estimate aperture in the estimated centre
      //set a fake source in the position of the focus with the measured flux
      newFocus = OccImageAperturePhotometry.calculateAperturePhotometryOnPixPos(
          occImage
        , estCentroidFocus
        , occConf
        , verbose)
    }
    newFocus
  }
  //---------------------------------------------------------------------------
  def calculateFocusEvolution(occImageSeq: Array[OccImage]
                              , sourceEvolutionSeq: Array[OccSourceEvolution]
                              , occConf: OccConfiguration
                              , verbose: Boolean = false): Array[OccSourceEvolution] = {

    var currentRefSource = occImageSeq.head.getFocus()

    //load the images where focus where detected
    occImageSeq
      .drop(1)  //avoid the very first image because it is used as reference
      .zipWithIndex.foreach { case (occImage, i) =>

      val sourceMatch = occImage.getRefSourceMatch(currentRefSource.id)
      if (sourceMatch.isDefined && sourceMatch.get.valid) {
        occImage.setFocus(sourceMatch.get.closestSource)
        currentRefSource = sourceMatch.get.closestSource
      }
      else {
         val newFocus = getNewFocus(occImage
                                    , occImageSeq
                                    , i
                                    , currentRefSource
                                    , occConf
                                    , verbose)
        newFocus.setRelativeFluxPerSecond(1)
        newFocus.setRelativeFluxSNR(newFocus.fluxPersSecondSNR)
        occImage.setFocus(newFocus)
        currentRefSource = newFocus
      }
    }

    //build a source evolution for the focus
    val focusEvolution = OccSourceEvolution(occImageSeq.head.getFocus(), occImageSeq.map(_.getFocus()))
    focusEvolution +: sourceEvolutionSeq
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import OccSourceEvolution._
case class OccSourceEvolution(refSource: OccSource
                              , sourceSeq: Array[OccSource]) {
  //---------------------------------------------------------------------------
  val fluxPerSecondStats = StatDescriptive.getWithDouble(sourceSeq map (_.fluxPerSecond))
  //---------------------------------------------------------------------------
  def apply(i: Int) = sourceSeq(i)
  //---------------------------------------------------------------------------
  def getTimeRange = s"from:${sourceSeq.head.occImage.observingTime} to:${sourceSeq.last.occImage.observingTime}"
  //---------------------------------------------------------------------------
  def getLocation = sourceSeq.head.occImage.location
  //---------------------------------------------------------------------------
  def calculateMedianRelativeFlux() =
    StatDescriptive.getWithDouble(
      (sourceSeq map (_.getRelativeFluxPerSecond)).filter(!_.isNaN)
    ).median
  //---------------------------------------------------------------------------
  def calculateMedianRelativeFluxSNR() =
    StatDescriptive.getWithDouble(
      (sourceSeq map (_.getRelativeFluxSNR())).filter(!_.isNaN)
    ).median
  //---------------------------------------------------------------------------
  def calculateStdevRelativeFlux() =
    StatDescriptive.getWithDouble(
      (sourceSeq map (_.getRelativeFluxPerSecond)).filter(!_.isNaN)
    ).stdDev
  //---------------------------------------------------------------------------
  def saveAsCsv(outputDir: String, focusID: Int = -1) = {
    val id = sourceSeq.head.id
    val suffix = if (refSource.id == focusID) "_focus" else ""
    val dir = Path.createDirectoryIfNotExist(s"$outputDir/source_$id$suffix/")
    val bw = new BufferedWriter(new FileWriter(new File(s"$dir/$SOURCE_CSV")))
    val headerSource = COL_NAME_SEQ.mkString(sep) +"\n"
    bw.write(headerSource)

    sourceSeq.sortWith(_.occImage.observingTimeMidPointJD < _.occImage.observingTimeMidPointJD).foreach { source=>
      bw.write(
        source.occImage.getShortName + sep +
          source.occImage.observingTimeMidPointJD + sep +
          source.occImage.observingTime + sep +
          source.id + sep +
          source.centroid.x + sep +
          source.centroid.y + sep +
          source.fluxPerSecond + sep +
          source.getRelativeFluxPerSecond + sep +
          "\n")
    }
    bw.close
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file OccSourceEvolution.scala
//=============================================================================