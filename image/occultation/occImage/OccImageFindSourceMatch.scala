/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Apr/2023
 * Time:  13h:48m
 * Description: None
 */
//=============================================================================
package com.common.image.occultation.occImage
//=============================================================================
import com.common.geometry.affineTransformation.AffineTransformation2D
import com.common.hardware.cpu.CPU
import com.common.image.occultation.{ManualTracking, OccConfiguration}
import com.common.image.registration.{Registration, RegistrationByClosestSource}
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
//=============================================================================
//=============================================================================
object OccImageFindSourceMatch extends MyLogger {
  //---------------------------------------------------------------------------
  //occImageSeq must be sorted
  def calculate(occImageSeq: Array[OccImage]
                , occConf: OccConfiguration
                , verbose: Boolean): Unit = {
    //set as ref image the previous image
    occImageSeq.sliding(2).toArray foreach { a => a(1).setRefOccImage(a(0)) }

    //register the image by pairs
    registerWithPrevImage(occImageSeq, occConf, verbose)

    //calculate source match
    new CalculateImagePairSourceMatch(occImageSeq.drop(1), occConf , verbose) //avoid the very first image
  }
  //---------------------------------------------------------------------------
  private def registerWithPrevImage(occImageSeq: Array[OccImage]
                                    , occConf: OccConfiguration
                                    , verbose: Boolean): Unit = {
    //affine trasnformation between the oldest (left or referece) and the newest (right)
    //split the sorted image sequence in consecutive pairs and calculate them
    //Only the aft of the left in updated
    val occImageSeqPair = occImageSeq.grouped(2).toArray flatMap { a =>
      if (a.size == 2) {
        if(a(0).hasWcs  && a(1).hasWcs) None  //avoid image pairs where the wcs is present. In that case is not required to use a registration
        else Some((a(0), a(1)))
      }
      else None
    }

    if(!occImageSeqPair.isEmpty) new CalculateAftImagePair(occImageSeqPair,occConf,verbose)

    //now calculate the remain pairs, where the left side has been not updated yet
    val occImageSeqPair2 = occImageSeq.drop(1).grouped(2).toArray flatMap { a =>
      if (a.size == 2) {
        if (a(0).hasWcs && a(1).hasWcs) None //avoid image pairs where the wcs is present. In that case is not required to use a registration
        else Some((a(0), a(1)))
      }
      else None
    }
    if (!occImageSeqPair2.isEmpty) new CalculateAftImagePair(occImageSeqPair2, occConf,verbose)
  }
  //---------------------------------------------------------------------------
  private class CalculateAftImagePair(occImageSeqPair: Array[(OccImage, OccImage)]
                                      , occConf: OccConfiguration
                                      , verbose: Boolean)
    extends ParallelTask[(OccImage, OccImage)](
      occImageSeqPair
      , CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 100) {
    //-------------------------------------------------------------------------
    def getAftByManualTracking(refImage: OccImage
                               , otherImage: OccImage): Option[AffineTransformation2D]  = {
      val fixedSourceMatchMap = ManualTracking.getSourceFixedIdMap()
      val manualSourceMatchMap = ManualTracking.getSourceMatchMap(
         otherImage.getShortName
        , refImage.getShortName)

      if (fixedSourceMatchMap.isEmpty && manualSourceMatchMap.isEmpty) return None

      val residualPos =
        if (fixedSourceMatchMap.isDefined)
          fixedSourceMatchMap.get.toSeq.map { case (refSourceID, sourceID) =>

            if (!refImage.containsSourceID(refSourceID) ||
                !otherImage.containsSourceID(sourceID)) return None

            val refSource = refImage.getBySourceID(refSourceID)
            val source = otherImage.getBySourceID(sourceID)
            source.centroid - refSource.centroid
          }
        else
           manualSourceMatchMap.get.toSeq.map { case (refSourceID, sourceID) =>

             if (!refImage.containsSourceID(refSourceID) ||
               !otherImage.containsSourceID(sourceID)) return None

             val refSource = refImage.getBySourceID(refSourceID)
             val source = otherImage.getBySourceID(sourceID)
             source.centroid - refSource.centroid
           }

      warning(s"Using manual tracking between reference image:'${refImage.getShortName}' and image:'${otherImage.getShortName}'")
      Some(RegistrationByClosestSource.buildAft(residualPos.toArray, occConf.REGISTRATION_ALGORITHM_CLOSEST_SIGMA_CLIPPING))
    }
    //-------------------------------------------------------------------------
    def userProcessSingleItem(imagePair: (OccImage, OccImage)): Unit = {
      val refImage = imagePair._1
      val otherImage = imagePair._2

      //try to find a match using the manual tracking
      val aftByManualTracking = getAftByManualTracking(refImage,otherImage)

      val calculatedAft =
        if(aftByManualTracking.isDefined) aftByManualTracking.get
        else {
          //get the affine transformation
          val r = Registration.calculateWithFallbackAlgorithm(
            refImage
            , otherImage
            , occConf.REGISTRATION_ALGORITHM_SEQ
            , closestAlgorithmUserParamSeq = Some(occConf.REGISTRATION_ALGORITHM_CLOSEST_PARAM_SEQ)
            , verbose)

          if (r.isEmpty) {
            warning(s"Can not find a valid affine transformation between reference image:'${refImage.getShortName}' and image:'${otherImage.getShortName}' assuming unit trasnformation")
            return
          }
          r.get._1
        }

      otherImage.composeLeftAft(calculatedAft)
      if (verbose)
        info(s"Aft:${otherImage.getAft().toShortString} between " +
          s"reference image:'${refImage.getShortName}' " +
          s"and image:'${otherImage.getShortName}'")
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private class CalculateImagePairSourceMatch(occImageSeq: Array[OccImage]
                                              , occConf: OccConfiguration
                                              , verbose: Boolean)
    extends ParallelTask[OccImage](
      occImageSeq
      , CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 100) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(occImage: OccImage): Unit = {
      occImage.findSourceMatchMap(occConf.MAX_ALLOWED_PIX_DISTANCE_MATCH)
      if(verbose) info(s"Image:'${occImage.getShortName}' has found: ${occImage.getSourceMatchCount} source mathes using as image reference:'${occImage.getRefOccImage().getShortName}'")
    }
    //-------------------------------------------------------------------------
  }
  //----------------------------------------------------------------------------
}
//=============================================================================
//End of file OccImageFindSourceMatch.scala
//=============================================================================
