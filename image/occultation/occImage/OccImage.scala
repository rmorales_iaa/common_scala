/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Apr/2023
 * Time:  12h:06m
 * Description: None
 */
//=============================================================================
package com.common.image.occultation.occImage
//=============================================================================
import com.common.csv.CsvRead
import com.common.dataType.tree.kdTree.K2d_TreeDouble
import com.common.fits.wcs.WCS
import com.common.geometry.affineTransformation.AffineTransformation2D
import com.common.geometry.affineTransformation.AffineTransformation2D.AFFINE_TRANSFORMATION_2D_UNIT
import com.common.geometry.point.Point2D_Double
import com.common.image.focusType.ImageFocusMPO
import com.common.image.myImage.MyImage
import com.common.image.occultation.OccConfiguration
import com.common.image.occultation.SourceOccultation._
import com.common.image.occultation.gnuplot.GnuPlotScript
import com.common.image.occultation.occSource.{OccSource, OccSourceEvolution}
import com.common.jpl.Spice
import com.common.logger.MyLogger
import com.common.math.MyMath
import com.common.stat.StatDescriptive
import com.common.util.file.MyFile
import com.common.util.path.Path
import com.common.util.time.Time
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import java.time.format.DateTimeFormatter
//=============================================================================
//=============================================================================
object OccImage extends MyLogger {
  //---------------------------------------------------------------------------
  final val sep = "\t"
  //---------------------------------------------------------------------------
  final val PARTIAL_RESULT_DIR                = "partial_result"
  final val PARTIAL_RESULT_OCC_IMAGE_CSV      = "occ_image.csv"
  final val PARTIAL_RESULT_OCC_SOURCE_SEQ_CSV = "occ_source_seq.csv"
  //---------------------------------------------------------------------------
  final val COL_NAME_IMAGE_NAME                    = "image_name"
  private final val COL_NAME_X_PIX_MAX             = "x_pix_max"
  private final val COL_NAME_Y_PIX_MAX             = "y_pix_max"
  private final val COL_NAME_PIX_SIZE              = "pix_size"
  private final val COL_NAME_NOISE_TIDE            = "noise_tide"
  private final val COL_NAME_LOCATION              = "location"
  final val COL_NAME_OBSERVING_TIME_MID_POINT_JD   = "observing_time_mid_point_jd"
  final val COL_NAME_OBSERVING_TIME                = "observing_time"
  private final val COL_NAME_EXPOSURE_TIME         = "exposure_time"
  private final val COL_NAME_HAS_WCS               = "has_wcs"
  private final val COL_NAME_AFT_0                 = "aft_0"
  private final val COL_NAME_AFT_1                 = "aft_1"
  private final val COL_NAME_AFT_2                 = "aft_2"
  private final val COL_NAME_AFT_3                 = "aft_3"
  private final val COL_NAME_AFT_4                 = "aft_4"
  private final val COL_NAME_AFT_5                 = "aft_5"
  private final val COL_NAME_FOCUS_ID              = "focus_id"
  private final val COL_NAME_FOCUS_POSITION        = "focus_position"
  private final val COL_NAME_FOCUS_FLUX_PER_SECOND = "focus_flux_per_second"

  private final val COL_NAME_BASIC_SEQ = Seq(
    COL_NAME_IMAGE_NAME
    , COL_NAME_X_PIX_MAX
    , COL_NAME_Y_PIX_MAX
    , COL_NAME_PIX_SIZE
    , COL_NAME_NOISE_TIDE
    , COL_NAME_LOCATION
    , COL_NAME_OBSERVING_TIME_MID_POINT_JD
    , COL_NAME_OBSERVING_TIME
    , COL_NAME_EXPOSURE_TIME
    , COL_NAME_HAS_WCS
  )
  private final val COL_NAME_SEQ =
    COL_NAME_BASIC_SEQ ++ Seq(
      OccSourceMatch.COL_NAME_REF_IMAGE_NAME
    , COL_NAME_AFT_0
    , COL_NAME_AFT_1
    , COL_NAME_AFT_2
    , COL_NAME_AFT_3
    , COL_NAME_AFT_4
    , COL_NAME_AFT_5
    , COL_NAME_FOCUS_ID
    , COL_NAME_FOCUS_POSITION
    , COL_NAME_FOCUS_FLUX_PER_SECOND
  )
  //---------------------------------------------------------------------------
  private final val timeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS")
  //---------------------------------------------------------------------------
  def apply(img: MyImage): OccImage = {
    val observingTime =  img.getObservingTime()
    val occImg = OccImage(
        path = img.name
      , img.xMax
      , img.yMax
      , img.getNoiseTide
      , img.getTelescope()
      , observingTimeMidPointJD = Time.toJulian(img.getObservingDateMidPoint)
      , observingTime = observingTime.format(timeFormatter)
      , img.getExposureTime()
      , wcs = if (img.hasWcs()) Some(img.getWcs()) else None
    )
    occImg
  }

  //---------------------------------------------------------------------------
  def load(csvName: String): OccImage = {
    val csv = CsvRead(csvName)
    csv.read()
    val colNameSeq = OccImage.COL_NAME_BASIC_SEQ
    csv.getRowSeq.map { row =>
      val path = row.getString(colNameSeq(0))
      val wcs  =
        if (row.getInt(colNameSeq(9)) == 1) Some(MyImage(path).getWcs())
        else None

      new OccImage(
          path                    = path
        , xPixMax                 = row.getInt(colNameSeq(1))
        , yPixMax                 = row.getInt(colNameSeq(2))
        , noiseTide               = row.getDouble(colNameSeq(4))
        , location                = row.getString(colNameSeq(5))
        , observingTimeMidPointJD = row.getDouble(colNameSeq(6))
        , observingTime           = row.getString(colNameSeq(7))
        , exposureTime            = row.getDouble(colNameSeq(8))
        , wcs                     = wcs
      )
    }.head
  }
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String = "") =
    classOf[OccSource].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  def saveSumary(csvName: String
                 , occImageSeq:Array[OccImage]
                 , sep: String = sep)= {
    val bw = new BufferedWriter(new FileWriter(new File(csvName)))
    bw.write(OccImage.COL_NAME_SEQ.mkString(sep)+ "\n")
    occImageSeq
      .sortWith(_.observingTimeMidPointJD < _.observingTimeMidPointJD)
      .foreach { occImage =>
        bw.write(occImage.getAsCsvLine(sep) + "\n")
    }
    bw.close()
  }
  //---------------------------------------------------------------------------
  def findEvolutionSeq(occImageSeq: Array[OccImage]
                       , outputDir: String
                       , occConf: OccConfiguration): Array[OccSourceEvolution] = {
    info(s"Calculating the common sources on: ${occImageSeq.size} images")
    //get only the matched sources in the first image
    val sortedImageSeq = occImageSeq.sortWith(_.observingTime < _.observingTime)
    val refImage = sortedImageSeq.head
    val refSourceSeq = refImage.sourceSeq.sortWith(_.pixSize > _.pixSize).take(occConf.MAX_SOURCES_FIRST_IMAGE)
    val notMatchedRefSourceDir = s"$outputDir/$SOURCE_SOURCE_NOT_MATCHED_DIR/"
    val remainImageSeq = sortedImageSeq.drop(1) //avoid the very first image that is used as reference

    info(s"Using as reference image:'${refImage.getShortName()}' with: ${refSourceSeq.size} matched sources")
    val occEvolutionSeq = refSourceSeq.sortWith (_.id < _.id).flatMap { refSource =>
      val evolutionSeq = OccSourceEvolution.find(
          remainImageSeq
        , refImage
        , refSource
        , notMatchedRefSourceDir
        , occConf)
      if (evolutionSeq.isEmpty) None
      else Some(OccSourceEvolution(refSource, evolutionSeq))
    }
    info(s"Found: ${occEvolutionSeq.size} common sources in: ${occImageSeq.size} images")
    occEvolutionSeq.foreach { sourceEvolution => sourceEvolution.saveAsCsv(outputDir) }
    occEvolutionSeq
  }
  //---------------------------------------------------------------------------
  def findFocusInImage(focusName: String
                       , occImage: OccImage
                       , occConf: OccConfiguration
                       , spkVersion: Option[String] = None): Option[OccSource] = {
    //-------------------------------------------------------------------------
    val maxAllowedPixDistance = occConf.MAX_ALLOWED_PIX_DISTANCE_MATCH
    //-------------------------------------------------------------------------
    def getSource(focusPos: Point2D_Double): Option[OccSource] = {

      val (found, (source, _)) = occImage.findClosestSourceByPixPos(focusPos
                                                                    , maxAllowedPixDistance)
      if (!found) {
        error(s"Can not find the focus:'$focusName' in the reference image '${occImage.getShortName}' at pixel position:'${source.centroid.getFormattedShortString()}'")
        return None
      }
      Some(source)
    }
    //-------------------------------------------------------------------------
    if (occImage.sourceSeq.size == 0) {
      error(s"Image:'${occImage.getShortName}' has not valid sources")
      return None
    }

    if (occConf.NO_REFERENCE_SOURCES) {
      return Some(occImage.sourceSeq.sortWith(_.pixSize > _.pixSize).head)
    }

    //focus pix set in the configuration
    val focusPix = occConf.FOCUS_PIX
    if (focusPix.isDefined) {
      if (occConf.FOCUS_PIX_MATCH_CLOSEST_SOURCE) return getSource(focusPix.get)
      else { //no source required, only the pixel value
        val pixValue = MyImage(occImage.path).get(Math.round(focusPix.get.x).toInt
          , Math.round(focusPix.get.y).toInt)
        return Some(OccSource(focusPix.get, pixValue, occImage))
      }
    }

    //get focus position by (ra,dec)
    val img = occImage.getImage
    if (img == null) {
      error(s"Error loading the image:'${img.getRawName()}'")
      return None
    }

    if (!img.getSimpleFits().hasWcs() &&
      focusPix.isEmpty) {
      error(s"Error, can not locate the focus. The image:'${img.getRawName()}' has no WCS and none focus pixel is provided")
      return None
   }

    val imageFocus = ImageFocusMPO.build(focusName, source = null).getOrElse {
      error(s"Error, can not find the Spice kernel for focus name: '$focusName'")
      return None
    }

    val estPos = getEstPos(img
                          , imageFocus
                          , spkVersion).getOrElse{
      error(s"Error, error locating the MPO in the image: '$focusName'")
      return None
    }

    getSource(img.skyToPix(estPos))
  }
  //---------------------------------------------------------------------------
  private def getEstPos(img: MyImage
                       , imageFocus:ImageFocusMPO
                       , spkVersion: Option[String] = None): Option[Point2D_Double]= {


    //calculate the position of the focus using Spice kernel
    Spice.init()
    val estPos = imageFocus.getEstPosition(img)
    Spice.close()

    if (estPos.isDefined) Some(estPos.get._1)
    else None

  }
  //---------------------------------------------------------------------------
  def calculateFocusNormalizedFlux(occImageSeq: Array[OccImage]
                                   , focus: OccSource
                                   , sourceEvolutionSeq: Array[OccSourceEvolution]) = {

    //filter the source evolution of the focus
    val remainSourceEvolutionSeq = sourceEvolutionSeq.filterNot(_.sourceSeq.head.id == focus.id)

    //set common sources avoiding the focus evolution
    val imageCount = occImageSeq.size
    for (i <- 0 until imageCount) {
      val commonSourceSeq = remainSourceEvolutionSeq map { _(i)}
      occImageSeq(i).setCommonSource(commonSourceSeq)
    }

    //calculate relative flux to focus in the common sources in each image
    occImageSeq.foreach { _.calculateRelativeFluxWithCommonSourceSeq() }

    //calculate relative flux in the source evolution
    remainSourceEvolutionSeq.map { remainSourceEvolution =>
      val medianRelativeFluxSourceEvolution = remainSourceEvolution.calculateMedianRelativeFlux()
      val medianRelativeFluxSNRSourceEvolution = remainSourceEvolution.calculateMedianRelativeFluxSNR()
      remainSourceEvolution.sourceSeq map { occSource =>
        occSource.setRelativeFluxPerSecond(occSource.getRelativeFluxPerSecond / medianRelativeFluxSourceEvolution)
        occSource.setRelativeFluxSNR(MyMath.getDivisionOfSNR_lineal(occSource.getRelativeFluxSNR(),medianRelativeFluxSNRSourceEvolution))
      }
    }

    // update focus flux
    occImageSeq.foreach { occImage =>
      //update the the normalized flux and SNR
      val relativeFluxSeq = occImage.commonSourceSeq map (_.getRelativeFluxPerSecond)
      val relativeFluxSNRSeq = occImage.commonSourceSeq map (_.getRelativeFluxSNR())

      val newFlux = StatDescriptive.getWithDouble(relativeFluxSeq.filter(!_.isNaN)).median
      val newFluxSNR = StatDescriptive.getWithDouble(relativeFluxSNRSeq.filter(!_.isNaN)).median
      val focus = occImage.getFocus()

      focus.setRelativeFluxSeq(relativeFluxSeq)
      focus.setRelativeFluxPerSecond(newFlux)

      focus.setRelativeFluxSNR(newFluxSNR)
    }
  }
  //---------------------------------------------------------------------------
  def generatePlot(focusName: String
                   , occImageSeq: Array[OccImage]
                   , notFocusSourceEvolution: Array[OccSourceEvolution]
                   , medianStdevFluxRelative: String
                   , location: String
                   , timeRange: String
                   , apertureInfo: String
                   , outputDir: String) = {

    val csvFileName = s"$outputDir/${focusName}_occultation.csv"
    val gnuplotFileName = s"$outputDir/${focusName}_occultation.gnuplot"
    val bw = new BufferedWriter(new FileWriter(new File(csvFileName)))

    val headerSeq = Seq(
        OccImage.COL_NAME_IMAGE_NAME
      , OccImage.COL_NAME_OBSERVING_TIME_MID_POINT_JD
      , OccImage.COL_NAME_OBSERVING_TIME
      , OccImage.COL_NAME_FOCUS_FLUX_PER_SECOND
      , "focus_flux_per_second_SNR"
      , "focus_position.x"
      , "focus_position.y"
      , OccImage.COL_NAME_PIX_SIZE
    )

    bw.write(headerSeq.mkString(sep) + "\n")
    occImageSeq.foreach { occImg =>
      val focus = occImg.getFocus()
      bw.write(occImg.getShortName + sep +
        occImg.observingTimeMidPointJD + sep +
        occImg.observingTime + sep +
        focus.getRelativeFluxPerSecond  + sep +
        focus.getRelativeFluxSNR()  + sep +
        focus.centroid.x  + sep +
        focus.centroid.y + sep +
        focus.pixSize + sep +
        "\n"
      )
    }
    bw.close()
    GnuPlotScript.plotOccultation(focusName
                                  , occImageSeq.size
                                  , notFocusSourceEvolution.size
                                  , medianStdevFluxRelative
                                  , if (location.isEmpty) "unknown" else location
                                  , timeRange
                                  , apertureInfo
                                  , gnuplotFileName
                                  , Path.getCurrentPath() + csvFileName)

    gnuplotFileName
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.common.image.occultation.occImage.OccImage._
case class OccImage(path: String
                    , xPixMax: Int
                    , yPixMax: Int
                    , noiseTide: Double
                    , location: String
                    , observingTimeMidPointJD: Double
                    , observingTime:String
                    , exposureTime: Double
                    , wcs: Option[WCS] = None) {
  //---------------------------------------------------------------------------
  private var sourceSeq = Array[OccSource]()
  //---------------------------------------------------------------------------
  private val sourceKdTree = K2d_TreeDouble()
  private val sourceMap = scala.collection.mutable.Map[Int, OccSource]()
  //---------------------------------------------------------------------------
  private var refImage: OccImage = null
  //---------------------------------------------------------------------------
  private val sourceMatchMap    = scala.collection.mutable.Map[Int,OccSourceMatch]()  //(source id, match)
  private val refSourceMatchMap = scala.collection.mutable.Map[Int,OccSourceMatch]()  //(ref source id, match)
  //---------------------------------------------------------------------------
  private var focus:OccSource = null
  private var commonSourceSeq:Array[OccSource] = null
  //---------------------------------------------------------------------------
  //aft to transform a position from refeerence image to the source image
  private var aft: AffineTransformation2D = AFFINE_TRANSFORMATION_2D_UNIT
  //---------------------------------------------------------------------------
  def getShortName() = MyFile.getFileNameNoPathNoExtension(path)
  //---------------------------------------------------------------------------
  def getBySourceID(id: Int) = sourceMap(id)
  //---------------------------------------------------------------------------
  def containsSourceID(id: Int) = sourceMap.contains(id)
  //---------------------------------------------------------------------------
  def getRefOccImage() = refImage
  //---------------------------------------------------------------------------
  def setRefOccImage(img:OccImage) = refImage = img
  //---------------------------------------------------------------------------
  def getAft() = aft
  //---------------------------------------------------------------------------
  def composeRightAft(newAft: AffineTransformation2D) = {
    aft = aft.compose(newAft)
    aft
  }
  //---------------------------------------------------------------------------
  def composeLeftAft(newAft: AffineTransformation2D) = {
    aft = newAft.compose(aft)
    aft
  }
  //---------------------------------------------------------------------------
  def getSourceCount() = sourceSeq.size
  //---------------------------------------------------------------------------
  //(found,(source,distance))
  def findClosestSource(source: OccSource
                        , maxAllowedPixDistance: Double) =
    findClosestSourceByPixPos(source.centroid
                             , maxAllowedPixDistance)
  //---------------------------------------------------------------------------
  //(found,(source,distance))
  def findClosestSourceByPixPos(pixPos: Point2D_Double
                                , maxAllowedPixDistance: Double) = {
    val closestSource = sourceMap(sourceKdTree.getKNN(pixPos.toSequence()).head.v)
    val dist = closestSource.centroid.getDistance(pixPos)
    if (dist <= maxAllowedPixDistance) (true, (closestSource, dist))
    else (false, (closestSource, dist))
  }
  //---------------------------------------------------------------------------
  def getSourceMatch(sourceID: Int) = {
    if (sourceMatchMap.contains(sourceID)) Some(sourceMatchMap(sourceID))
    else None
  }
  //---------------------------------------------------------------------------
  def getRefSourceMatch(refSourceID: Int) = {
    if (refSourceMatchMap.contains(refSourceID)) Some(refSourceMatchMap(refSourceID))
    else None
  }
  //---------------------------------------------------------------------------
  def getSourceMatchCount = sourceMatchMap.size
  //---------------------------------------------------------------------------
  def getSourceSeq(idSeq: Seq[Int]) =  idSeq map (sourceMap(_))
  //---------------------------------------------------------------------------
  def getSourceSeq(id:Int) = sourceSeq.filter(_.id == id).head
  //---------------------------------------------------------------------------
  def getSourceSeq() = sourceSeq
  //---------------------------------------------------------------------------
  def setSourceSeq(seq: Array[OccSource]) = {
    sourceSeq = seq

    val pairSeq = sourceSeq.map { occSource =>
      sourceMap(occSource.id) = occSource
      (aft(occSource.centroid).toSequence(), occSource.id)  //apply otheraAft
    }
    sourceKdTree.addSeq(pairSeq)
    (sourceKdTree,sourceMap)
  }
  //---------------------------------------------------------------------------
  def getAsCsvLine(sep: String
                   , useShortName: Boolean  = true) = {
    val focusID = if (focus == null || focus.id == -1) "None" else focus.id
    (if (useShortName) getShortName else path) + sep +
    xPixMax + sep +
    yPixMax + sep +
    (if (focus == null) "None" else focus.pixSize) + sep +
    noiseTide + sep +
    location + sep +
    observingTimeMidPointJD + sep +
    observingTime + sep +
    exposureTime + sep +
    (if (wcs.isDefined) "1" else "0") + sep +
    (if (refImage  == null) "None" else refImage.getShortName) + sep +
    aft.getAsCsvLine(sep) + sep +
    focusID + sep +
    (if (focus == null) "None" else focus.centroid.getFormattedShortString()) + sep +
    (if (focus == null) "None" else focus.getRelativeFluxPerSecond)
  }
  //---------------------------------------------------------------------------
  def getFocus() = focus
  //---------------------------------------------------------------------------
  def setFocus(_focus: OccSource) = focus = _focus
  //---------------------------------------------------------------------------
  def setFocusBiggerSource() = {
    focus = sourceSeq.sortWith( _.pixSize > _.pixSize).head
    focus.setRelativeFluxPerSecond( focus.fluxPerSecond)
    focus.setRelativeFluxSNR( focus.fluxPersSecondSNR)
  }
  //---------------------------------------------------------------------------
  def setCommonSource(_commonSourceSeq:Array[OccSource]) =
    commonSourceSeq = _commonSourceSeq
  //---------------------------------------------------------------------------
  def calculateRelativeFluxWithCommonSourceSeq() =
    commonSourceSeq map { s =>
      s.setRelativeFluxPerSecond(focus.fluxPerSecond / s.fluxPerSecond)
      s.setRelativeFluxSNR(MyMath.getDivisionOfSNR_lineal(focus.fluxPersSecondSNR,s.fluxPersSecondSNR))
    }
  //---------------------------------------------------------------------------
  //between A and a transformation
  def getOverlappingPercentage(atf: AffineTransformation2D): Double = {
    val (_, _, width, height) = MyImage.aftOnDimensions(atf, xPixMax, yPixMax)
    getOverlappingPercentage(width, height)
  }
  //---------------------------------------------------------------------------
  def getOverlappingPercentage(width: Double, height: Double): Double =
    ((width * height) / (xPixMax * yPixMax).toFloat) * 100f
  //---------------------------------------------------------------------------
  def sortByFluxPerSecond(maxCount: Int = -1) =
    sourceSeq
      .sortWith(_.fluxPerSecond > _.fluxPerSecond)
      .take(maxCount)
  //---------------------------------------------------------------------------
  def getCommonSourceCount = {
    if (commonSourceSeq == null) 0
    else commonSourceSeq.size
  }
  //---------------------------------------------------------------------------
  def findSourceMatchMap(maxAllowedPixDistance: Double): Unit = {

    if (sourceSeq.isEmpty  || refImage.sourceSeq.isEmpty) return
    refImage.getSourceSeq().foreach { refSource =>
        val (valid, (source, dist)) = findClosestSourceByPixPos(aft(refSource.centroid)
                                                                   , maxAllowedPixDistance)
        val sourceMatch = OccSourceMatch(
            this
          , refImage
          , source
          , refSource
          , dist
          , valid)
        sourceMatchMap(source.id) = sourceMatch
        refSourceMatchMap(refSource.id) = sourceMatch
      }
  }

  //---------------------------------------------------------------------------
  def getImage = MyImage(path)
  //---------------------------------------------------------------------------
  def hasWcs  = wcs.isDefined
  //----------------------------------------------------------------------------
  def savePartialResult(focusName:String) = {
    val dir = s"$DEFAULT_OUTPUT_DIR/$PARTIAL_RESULT_DIR/$focusName/$getShortName"

    //occ image
    Path.ensureDirectoryExist(dir)
    var bw = new BufferedWriter(new FileWriter(new File(s"$dir/$PARTIAL_RESULT_OCC_IMAGE_CSV")))
    bw.write(OccImage.COL_NAME_SEQ.mkString(sep) + "\n")
    bw.write(getAsCsvLine(sep, useShortName = false) + "\n")
    bw.close()

    //occ sources
    bw = new BufferedWriter(new FileWriter(new File(s"$dir/$PARTIAL_RESULT_OCC_SOURCE_SEQ_CSV")))
    bw.write(OccSource.COL_NAME_SEQ.mkString(sep) + "\n")
    sourceSeq.foreach { source =>
      bw.write(source.getAsCsvLine(sep) + "\n")
    }
    bw.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file OccImage.scala
//=============================================================================
