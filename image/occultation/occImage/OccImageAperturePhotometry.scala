/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Apr/2023
 * Time:  13h:51m
 * Description: None
 */
//=============================================================================
package com.common.image.occultation.occImage
//=============================================================================
import com.common.configuration.MyConf
import com.common.dataType.pixelDataType.PixelDataType
import com.common.database.mongoDB.database.MatchedImageSource
import com.common.database.mongoDB.database.gaia.{GaiaPhotometricSource, GaiaSourceReduced}
import com.common.geometry.point.Point2D_Double
import com.common.geometry.segment.Segment1D
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.estimator.centroid.Centroid
import com.common.image.estimator.flux.ApertureTrait
import com.common.image.estimator.fwhm.Fwhm
import com.common.image.mask.Mask
import com.common.image.myImage.MyImage
import com.common.image.occultation.OccConfiguration
import com.common.logger.MyLogger
import com.common.util.path.Path
import com.common.image.occultation.SourceOccultation._
import com.common.image.occultation.occSource.OccSource
import com.common.image.region.Region
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import scala.collection.mutable.ArrayBuffer
import java.awt.{Color, Font}
//=============================================================================
//=============================================================================
object OccImageAperturePhotometry extends MyLogger{
  //---------------------------------------------------------------------------
  private val sep = "\t"
  //---------------------------------------------------------------------------
  private final val IMAGE_MASK_PARENT_KEY =  "Occultation.sourceDetection.mask"
  //---------------------------------------------------------------------------
  def calculateAperturePhotometry(img: MyImage
                                  , outputDir: String
                                  , occConf: OccConfiguration
                                  , verbose: Boolean) = {

    if(verbose) info(s"Image: '${img.getRawName()}' calculating flux per second")

    //conf parameters
    val sigmaMultiplier   = occConf.SIGMA_MULTIPLIER
    val roundSources      = occConf.ROUND_SOURCES
    val minAperture       = occConf.MIN_APERTURE
    val anularAperture    = occConf.ANULAR_APERTURE
    val maxAperture       = occConf.MAX_APERTURE
    val sourceMinPixCount = occConf.SOURCE_MIN_PIX_COUNT
    val sourceMaxPixCount = occConf.SOURCE_MAX_PIX_COUNT
    val sourceMaxPixValue = occConf.MAX_PIX_VALUE

    //detect sources
    val background    = img.getBackground(verbose)
    val backgroundRMS = img.getBackgroundRMS(verbose)
    val noiseTide     = background + (backgroundRMS * sigmaMultiplier)

    if (roundSources != 0) if(verbose) info(s"Image: '${img.getRawName()}' rounding sources with neighbours: $roundSources pixel allowed:(min,max): ($sourceMinPixCount,$sourceMaxPixCount)")
    val maskSeq = Mask.parseMask(occConf.conf
                                 , IMAGE_MASK_PARENT_KEY
                                 , img.getDimension())

    if(verbose) info(s"Image: '${img.getRawName}' sigma multiplier: $sigmaMultiplier background:${f"$background%.3f"} background RMS:${f"$backgroundRMS%.3f"} noise tide:${f"$noiseTide%.3f"}")
    if(verbose) info(s"Image: '${img.getRawName}' mask: '${if (maskSeq == null || maskSeq.isEmpty) "Mask none" else maskSeq.mkString("[", ",", "]")}'")
    val region = img.findSourceSeq(noiseTide, maskSeq, Some((sourceMinPixCount, sourceMaxPixCount)))
    val _regionSourceSeq = region.toSegment2D_seq()
    val regionSourceSeq = _regionSourceSeq.filter( _.getMaxValue() < sourceMaxPixValue)

    val droppedSaturatedSourceCount = _regionSourceSeq.size - regionSourceSeq.size
    if (droppedSaturatedSourceCount > 0)
      warning(s"Image: '${img.getRawName()}' dropped: $droppedSaturatedSourceCount sources due to saturation. Max allowed pixel: '${occConf.MAX_PIX_VALUE}'")

    if(verbose) info(s"Image: '${img.getRawName()}' found: ${regionSourceSeq.size} sources before rounding")

    //round sources
    val imageSourceSeq =
      if (roundSources == 0) regionSourceSeq
      else {
        val newSourceSeq = regionSourceSeq flatMap (_.round(noiseTide, roundSources))
        newSourceSeq filter { source =>
          (source.getPixCount() >= sourceMinPixCount
          && !source.getData().forall(_ == PixelDataType.PIXEL_ZERO_VALUE))
        }
      }
    if(verbose) info(s"Image: '${img.getRawName()}' found: ${imageSourceSeq.size} sources after rounding: $roundSources with pixel count between:[$sourceMinPixCount,$sourceMaxPixCount]")

    //calculate precise centroid
    if (occConf.PRECISE_CENTROIDS) {
      Fwhm.calculate(img,imageSourceSeq, verbose)
      if(verbose) info(s"Image: '${img.getRawName()}' image fwhm average: ${f"${img.getFwhmAverage}%.3f"} pix")
      Centroid.calculateCentroiWithFallBack(imageSourceSeq
                                            , img
                                            , MyConf.c.getStringSeq("SourceCentroid.fallbackAlgorithmSeq")
                                            , verbose)
    }

    //build fake GAIA match source
    val sourceSeq = imageSourceSeq map { s =>
      MatchedImageSource(s, GaiaPhotometricSource(GaiaSourceReduced(s)))
    }

    if(verbose) info(s"Image: minAperture:$minAperture (pix) anular aperture:$anularAperture (pix) max aperture:$maxAperture (pix)")

    //aperture photometry
    ApertureTrait.calculate(
      img
      , sourceSeq
      , minAperture
      , anularAperture
      , maxAperture
      , apertureCoef = (true,-1,-1,-1)  //fixed aperture
      , noiseTide = noiseTide
      , sourceMaxPix = sourceMaxPixCount)

    //update the region
    val newRegion = Region()
    newRegion.addToStorage(imageSourceSeq)
    img.setRegion(newRegion)

    //build the occultation image
    val occImage = OccImage(img)

    //save sources
    val occSourceSeq = saveSourceSeq(img, sourceSeq, outputDir)

    //set the sources to the occultation image
    occImage.setSourceSeq(occSourceSeq.toArray)

    occImage
  }
  //---------------------------------------------------------------------------
  private def saveSourceSeq(img: MyImage
                            , sourceMatchedSeq: Array[MatchedImageSource]
                            , outputDir: String
                            , renderSourceSeq: Boolean = true) = {

    val dir = Path.resetDirectory(s"$outputDir/${img.getRawName()}")
    Path.resetDirectory(s"$dir")
    Path.resetDirectory(s"$dir/$CSV_IMAGE_INVALID_SOURCE_DIR")

    val csvSourceValidFile   = s"$dir/${img.getRawName()}.csv"
    val csvSourceInValidFile = s"$dir/$CSV_IMAGE_INVALID_SOURCE_DIR/${img.getRawName()}.csv"
    val pngSourceValidFile   = s"$dir/${img.getRawName()}.png"
    val pngSourceInValidFile = s"$dir/$CSV_IMAGE_INVALID_SOURCE_DIR/${img.getRawName()}.png"

    val validSourceSeq = ArrayBuffer[Segment2D]()
    val validOccSourceSeq = ArrayBuffer[OccSource]()
    val inValidSourceSeq = ArrayBuffer[Segment2D]()

    val bwValid = new BufferedWriter(new FileWriter(new File(csvSourceValidFile)))
    val bwInValid = new BufferedWriter(new FileWriter(new File(csvSourceInValidFile)))
    Seq(bwValid, bwInValid).foreach { bw =>
      bw.write(OccSource.COL_NAME_SEQ.mkString(sep) + "\n")
    }
    val occImage = OccImage(img)
    sourceMatchedSeq.sortWith(_.imageSource.id < _.imageSource.id).foreach { source =>
      val centroid = source.imageSource.getCentroid()
      val bw =
        if (centroid.x == -1
          || centroid.y == -1
          || source.catalogSource.asInstanceOf[GaiaPhotometricSource].getFluxPerSecond() == -1) {
          inValidSourceSeq += source.imageSource
          bwInValid
        }
        else {
          validSourceSeq += source.imageSource
          validOccSourceSeq += OccSource(source, occImage)
          bwValid
        }
      bw.write(OccSource(source, occImage).getAsCsvLine(sep) + "\n")
    }
    bwValid.close()
    bwInValid.close()

    //render the sources
    val region = img.getRegion
    if (renderSourceSeq && region != null) {
      (Seq(validSourceSeq, inValidSourceSeq) zip
        Seq(pngSourceValidFile, pngSourceInValidFile))
        .foreach { case (sourceSeq, pngFileName) =>
          region.synthesizeImageWithColor(
            pngFileName
            , sourceSeq.toArray
            , imageDimension = Some(img.getDimension())
            , font = Some(new Font(Font.MONOSPACED, Font.PLAIN, 10))
            , fontColor = Color.ORANGE)
        }
    }

    if (inValidSourceSeq.isEmpty) Path.deleteDirectoryIfExist(s"$dir/$CSV_IMAGE_INVALID_SOURCE_DIR")
    validOccSourceSeq
  }
  //---------------------------------------------------------------------------
  def calculateAperturePhotometryOnPixPos(occImage: OccImage
                                          , centroid: Point2D_Double
                                          , occConf: OccConfiguration
                                          , verbose: Boolean) = {
    //conf parameters
    val sigmaMultiplier = occConf.SIGMA_MULTIPLIER
    val minAperture       = occConf.MIN_APERTURE
    val anularAperture    = occConf.ANULAR_APERTURE
    val maxAperture       = occConf.MAX_APERTURE
    val sourceMinPixCount = occConf.SOURCE_MIN_PIX_COUNT
    val sourceMaxPixCount = occConf.SOURCE_MAX_PIX_COUNT

    val img = occImage.getImage

    //noise tide
    val background = img.getBackground(verbose)
    val backgroundRMS = img.getBackgroundRMS(verbose)
    val noiseTide = background + (backgroundRMS * sigmaMultiplier)

    val source = Segment2D(Array[Array[Segment1D]]())
    source.setCentroidRaw(centroid)
    val sourceSeq = Array(MatchedImageSource(source, GaiaPhotometricSource(GaiaSourceReduced(source))))

    //aperture photometry
    ApertureTrait.calculate(
      img
      , sourceSeq
      , minAperture
      , anularAperture
      , maxAperture
      , apertureCoef = (true,-1,-1,-1)  //fixed aperture
      , noiseTide = noiseTide
      , sourceMaxPixCount
      , allowNegativeFlux = true)

    val newSource = sourceSeq.head
    val occImageFocus = OccSource(
        id = -1
      , centroid
      , newSource.imageSource.getPixCount()
      , fluxPerSecond = newSource.catalogSource.asInstanceOf[GaiaPhotometricSource].getFluxPerSecond()
      , fluxPersSecondSNR = newSource.catalogSource.asInstanceOf[GaiaPhotometricSource].getFluxSNR()
      , occImage)
    occImageFocus
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file OccImageAperturePhotometry.scala
//=============================================================================
