/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  06/May/2023
 * Time:  09h:46m
 * Description: None
 */
//=============================================================================
package com.common.image.occultation.occImage
//=============================================================================
import com.common.image.occultation.occSource.OccSource
//=============================================================================
//=============================================================================
object OccSourceMatch {
  //---------------------------------------------------------------------------
  final val COL_NAME_SOURCE_IMAGE_NAME           = "source_image"
  final val COL_NAME_REF_IMAGE_NAME              = "ref_image"
  final val COL_NAME_REF_SOURCE_ID               = "ref_source_id"

  final val COL_NAME_CLOSEST_SOURCE_ID           = "closest_source_id"
  final val COL_NAME_DIST_TO_REF_SOURCE          = "distance_to_ref_source(pix)"

  final val COL_NAME_CLOSEST_SOURCE_CENTROID     = "closest_source_centroid"
  final val COL_NAME_REF_SOURCE_CENTROID         = "ref_source_centroid"

  final val COL_NAME_CLOSEST_SOURCE_FLUX_PER_SECOND = "closest_source_flux_per_second"
  final val COL_NAME_REF_SOURCE_FLUX_PER_SECOND     = "ref_source_flux_per_second"

  final val COL_NAME_CLOSEST_SOURCE_PIX_SIZE     = "closest_source_pix_size"
  final val COL_NAME_REF_SOURCE_PIX_SIZE         = "ref_source_pix_size"

  final val COL_NAME_AFT_REF_SOURCE_CENTROID     = "aft_ref_source_centroid"

  final val COL_NAME_SEQ = Seq(
      COL_NAME_SOURCE_IMAGE_NAME
    , COL_NAME_REF_IMAGE_NAME
    , COL_NAME_REF_SOURCE_ID
    , COL_NAME_CLOSEST_SOURCE_ID
    , COL_NAME_DIST_TO_REF_SOURCE

    , COL_NAME_CLOSEST_SOURCE_CENTROID
    , COL_NAME_REF_SOURCE_CENTROID

    , COL_NAME_CLOSEST_SOURCE_FLUX_PER_SECOND
    , COL_NAME_REF_SOURCE_FLUX_PER_SECOND

    , COL_NAME_CLOSEST_SOURCE_PIX_SIZE
    , COL_NAME_REF_SOURCE_PIX_SIZE

    , COL_NAME_AFT_REF_SOURCE_CENTROID
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
case class OccSourceMatch(sourceImage: OccImage
                          , refImage: OccImage
                          , closestSource: OccSource
                          , refSource: OccSource
                          , distance: Double
                          , valid: Boolean) {
  //---------------------------------------------------------------------------
  def getAsCsvLine(sep: String) =
    sourceImage.getShortName + sep +
    refImage.getShortName + sep +
    refSource.id + sep +
    closestSource.id + sep +
    s"${f"$distance%.3f"}" + sep +
    closestSource.centroid.getFormattedShortString() + sep +
    refSource.centroid.getFormattedShortString() + sep +
    closestSource.fluxPerSecond + sep +
    refSource.fluxPerSecond + sep +
    closestSource.pixSize + sep +
    refSource.pixSize + sep +
    sourceImage.getAft()(refSource.centroid).getFormattedShortString()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file OccSourceMatch.scala
//=============================================================================
