/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  09/May/2023
 * Time:  09h:35m
 * Description: None
 */
//=============================================================================
package com.common.image.occultation
//=============================================================================
import com.common.configuration.{MyConf, MyConfWithDefault}
import com.common.geometry.point.Point2D_Double
import com.common.image.registration.Registration
import com.common.logger.MyLogger
//=============================================================================
//=============================================================================
object OccConfiguration {
  //---------------------------------------------------------------------------
  private val DEFAULT_CONFIGURATION = "input/occultation/default/occultation.conf"
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.common.image.occultation.OccConfiguration._
case class OccConfiguration(configutationPath: Option[String]) extends MyLogger {
  //---------------------------------------------------------------------------
  private val defaultConf =  MyConf(DEFAULT_CONFIGURATION)
  //---------------------------------------------------------------------------
  val conf =
    if (configutationPath.isEmpty)
      MyConfWithDefault(MyConf.c.getString("Occultation.configurationFile")
                       , defaultConf)
    else
      MyConfWithDefault(configutationPath.get
                        , defaultConf)

  //---------------------------------------------------------------------------
  //focus pix
  private final val FOCUS_PIX_KEY = "Occultation.focusPix"
  private final val FOCUS_PIX_X = s"$FOCUS_PIX_KEY.x"
  private final val FOCUS_PIX_Y = s"$FOCUS_PIX_KEY.y"
  val FOCUS_PIX = {
    if (!conf.existKey(FOCUS_PIX_X))  None
    else
      Some(Point2D_Double(conf.getFloat(FOCUS_PIX_X)
                        , conf.getFloat(FOCUS_PIX_Y)))
  }
  //---------------------------------------------------------------------------
  private val MATCH_CLOSEST_SOURCE_KEY = FOCUS_PIX_KEY + ".matchClosestSource"
  val FOCUS_PIX_MATCH_CLOSEST_SOURCE =
    if (conf.existKey(MATCH_CLOSEST_SOURCE_KEY)) conf.getInt(MATCH_CLOSEST_SOURCE_KEY) == 1
    else false

  //---------------------------------------------------------------------------
  private final val NO_REFERENCE_SOURCES_KEY = s"$FOCUS_PIX_KEY.noReferenceSources"
  val NO_REFERENCE_SOURCES = if (!conf.existKey(NO_REFERENCE_SOURCES_KEY))  false
                             else true
  //---------------------------------------------------------------------------
  final val LOAD_PREVIOUS_RESULTS = conf.getInt(s"Occultation.loadPartialResults") == 1
  //---------------------------------------------------------------------------
  //source dectection
  final val SIGMA_MULTIPLIER                                 = conf.getDouble("Occultation.sourceDetection.sigmaMultiplier")
  final val MAX_ALLOWED_PIX_DISTANCE_MATCH                   = conf.getInt("Occultation.sourceDetection.maxAllowedPixDistanceMatch")

  final val ROUND_SOURCES                                    = conf.getInt("Occultation.sourceDetection.roundSources")
  final val SOURCE_MIN_PIX_COUNT                             = conf.getInt("Occultation.sourceDetection.sourceMinPixCount")
  final val SOURCE_MAX_PIX_COUNT                             = conf.getInt("Occultation.sourceDetection.sourceMaxPixCount")
  final val PRECISE_CENTROIDS                                = conf.getInt("Occultation.sourceDetection.preciseCentroids") == 1
  final val MAX_PIX_VALUE                                    = conf.getInt("Occultation.sourceDetection.maxPixValueAllowed")
  final val MAX_SOURCES_FIRST_IMAGE                          = conf.getInt("Occultation.sourceDetection.maxConsideredSourcesInFirstImage")
  //---------------------------------------------------------------------------
  //aperture photometry
  final val MIN_APERTURE    = conf.getDouble("Occultation.aperturePhotometry.minAperture")
  final val ANULAR_APERTURE = conf.getDouble("Occultation.aperturePhotometry.anularAperture")
  final val MAX_APERTURE    = conf.getDouble("Occultation.aperturePhotometry.maxAperture")
  //---------------------------------------------------------------------------
  //registration
  final val REGISTRATION_ALGORITHM_CLOSEST_MAX_PIX        = MAX_ALLOWED_PIX_DISTANCE_MATCH
  final val REGISTRATION_ALGORITHM_CLOSEST_USE_MEDIAN     = conf.getInt("Occultation.registration.closestSourceAlgorithm.useResidualMedianForAFT") == 1
  final val REGISTRATION_ALGORITHM_CLOSEST_SIGMA_CLIPPING = conf.getDouble("Occultation.registration.closestSourceAlgorithm.residualMedianSigmaClipping")
  final val REGISTRATION_ALGORITHM_CLOSEST_PARAM_SEQ = (
      REGISTRATION_ALGORITHM_CLOSEST_MAX_PIX
    , REGISTRATION_ALGORITHM_CLOSEST_USE_MEDIAN
    , REGISTRATION_ALGORITHM_CLOSEST_SIGMA_CLIPPING)

  final val REGISTRATION_ALGORITHM_SEQ = conf.getIntSeq("Occultation.registration.algorithmSeq") map (Registration.getAlgorithm(_))
  //---------------------------------------------------------------------------
  //common source filtering
  final val MAX_ALLOWED_NORMALIZED_STD_DEV = conf.getDouble("Occultation.commonSourceFiltering.maxAllowedNormalizedStdev")
  //---------------------------------------------------------------------------
  //croppy
  final val CROPPY_X_SIZE                       = conf.getInt("Occultation.croppy.croppyX_PixSize")
  final val CROPPY_Y_SIZE                       = conf.getInt("Occultation.croppy.croppyY_PixSize")
  final val CROPPY_SOURCE_RECTANGLE_HULL_X_SIZE = conf.getInt("Occultation.croppy.rectagleY_PixSize")
  final val CROPPY_SOURCE_RECTANGLE_HULL_Y_SIZE = conf.getInt("Occultation.croppy.rectagleY_PixSize")

  final val BUILD_GIF = conf.getInt("Occultation.gif.build_gif") == 1
  final val GIF_SPEED_MS = conf.getInt("Occultation.gif.speed_ms")
  //-----------------------------------------------------------------------
  //manual tracking
  private final val MANUAL_TRACKING                        = "Occultation.sourceDetection.manualTrackig"
  private final val MANUAL_TRACKING_SKIP_IMAGES            = s"$MANUAL_TRACKING.skipImages"
  private final val MANUAL_TRACKING_SKIP_IMAGE_RANGE       = s"$MANUAL_TRACKING.skipImageRange"
  private final val MANUAL_TRACKING_MATCH_SOURCE           = s"$MANUAL_TRACKING.matchSource"
  private final val MANUAL_TRACKING_MATCH_SOURCE_FIXED_ID  = s"$MANUAL_TRACKING.matchSourceFixedID"
  //---------------------------------------------------------------------------
  getManualTracking()
  getSkipImageRange()
  getMatchedSourceFixedMapID()
  //---------------------------------------------------------------------------
  private def getManualTracking(): Unit = {
    if (!conf.existKey(MANUAL_TRACKING)) return
    val skipImageSeq = conf.getStringSeq(MANUAL_TRACKING_SKIP_IMAGES)
    skipImageSeq map (s => ManualTracking.skipImageMap(s) = s)

    val matchSourceSeq = conf.getStringSeq(MANUAL_TRACKING_MATCH_SOURCE)
    matchSourceSeq map { s =>
      warning(s"Parsing manual tracking line:'$s'")
      val valueSeq = s
        .replaceAll("\t", " ")
        .replaceAll(" +", " ")
        .split(" ")

      val imageName = valueSeq(0)
      val refImageName = valueSeq(1)
      val refSourceID = valueSeq(2).toInt
      val sourceID = valueSeq(3).toInt
      ManualTracking.add(imageName
        , refImageName
        , sourceID
        , refSourceID)
    }
  }
  //---------------------------------------------------------------------------
  private def getMatchedSourceFixedMapID(): Unit = {
    if (!conf.existKey(MANUAL_TRACKING)) return
    val skipImageRange = conf.getStringSeq(MANUAL_TRACKING_MATCH_SOURCE_FIXED_ID)
    skipImageRange.foreach { s =>
      val valueSeq = s.replaceAll("\t", " ")
        .replaceAll(" +", " ")
        .split(" ")
      ManualTracking.sourceFixedIdMap(valueSeq(0).trim.toInt) = valueSeq(1).trim.toInt
    }
  }
  //---------------------------------------------------------------------------
  private def getSkipImageRange(): Unit = {
    if (!conf.existKey(MANUAL_TRACKING)) return
    val skipImageRange = conf.getStringSeq(MANUAL_TRACKING_SKIP_IMAGE_RANGE)
    skipImageRange.foreach { s=>
      val valueSeq = s.replaceAll("\t", " ")
        .replaceAll(" +", " ")
        .split(" ")
      ManualTracking.skipImageRangeSeq += ((valueSeq(0),valueSeq(1)))
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file OccConfiguration.scala
//=============================================================================
