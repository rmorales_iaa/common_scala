/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  29/Mar/2023
 * Time:  21h:42m
 * Description: None
 */
//=============================================================================
package com.common.image.occultation
//=============================================================================
import com.common.configuration.MyConf
import com.common.csv.{CsvRead, CsvRow}
import com.common.geometry.point.{Point2D, Point2D_Double}
import com.common.geometry.rectangle.Rectangle
import com.common.hardware.cpu.CPU
import com.common.image.myImage.{MyImage, ObservingTime}
import com.common.image.occultation.occImage.{OccImage, OccImageAperturePhotometry, OccImageFindSourceMatch}
import com.common.image.occultation.occSource.{OccSource, OccSourceEvolution}
import com.common.image.util.UtilImage
import com.common.logger.MyLogger
import com.common.stat.StatDescriptive
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
//=============================================================================
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.ConcurrentHashMap
import scala.collection.JavaConverters.collectionAsScalaIterableConverter
import scala.collection.JavaConverters.mapAsScalaConcurrentMapConverter
//=============================================================================
//=============================================================================
object SourceOccultation extends MyLogger {
  //---------------------------------------------------------------------------
  final val DEFAULT_OUTPUT_DIR               = "output/results/occultation/"
  //---------------------------------------------------------------------------
  final val IMAGE_SOURCE_DETECTION_DIR       = "0_source_detection"
  final val SOURCE_EVOLUTION_DIR             = "1_source_evolution"
  final val FOCUS_CROPPY_SEQ_DIR             = "2_focus_croppies"
  //---------------------------------------------------------------------------
  final val FOCUS_FITS_CROPPY_SEQ_DIR        = s"$FOCUS_CROPPY_SEQ_DIR/fits"
  final val FOCUS_PNG_CROPPY_SEQ_DIR         = s"$FOCUS_CROPPY_SEQ_DIR/png"
  //---------------------------------------------------------------------------
  final val CSV_SUMMARY_CSV                  = s"summary.csv"
  //---------------------------------------------------------------------------
  final val CSV_IMAGE_INVALID_SOURCE_DIR     = s"invalid_source"
  //---------------------------------------------------------------------------
  final val SOURCE_EVOLUTION_STATS_CSV       = "source_evolution_stats.csv"
  //---------------------------------------------------------------------------
  final val SOURCE_SOURCE_NOT_MATCHED_DIR    = "source_not_matched"
  final val SOURCE_DISCARDED_DIR             = "source_discarded"
  //---------------------------------------------------------------------------
  //the the list of images
  def calculate(focusName: String
                , inputDir:String
                , configurationPath: Option[String]
                , verbose: Boolean): Boolean = {

    //load configuration
    val occConf = OccConfiguration(configurationPath)

    //load the images and find its sources
    val outputDir = Path.resetDirectory(s"$DEFAULT_OUTPUT_DIR/$focusName")
    val occImageSeq = findSourceInImageDir(focusName, inputDir, occConf, verbose)

    //find focus in the very first image
    info("Finding focus in the very first image")
    val focusSource = OccImage.findFocusInImage(
      focusName
      , occImageSeq.head
      , occConf).getOrElse {
      error(s"Can not find focus position in the very first image")
      return false
    }
    occImageSeq.head.setFocus(focusSource)
    warning(s"Image:'${occImageSeq.head.getShortName()}' found focus with id:{${focusSource.id}} at position: ${focusSource.centroid.getFormattedShortString()} pix size: ${focusSource.pixSize} and flux per second: ${f"${focusSource.fluxPerSecond}%.3f"}" )

    //get common sources and calculate its evolution
    info("Getting common sources and calculate its evolution")
    if (occImageSeq.isEmpty) return error(s"The input directory:'$inputDir' has only:${occImageSeq.size} valid images")
    val sourceEvolutionDir = s"$outputDir/$SOURCE_EVOLUTION_DIR/"
    var sourceEvolutionSeq = OccSourceEvolution.find(occImageSeq
                                                     , inputDir
                                                     , sourceEvolutionDir
                                                     , occConf)
    var originalSourceEvolutionSeq = sourceEvolutionSeq

    if (sourceEvolutionSeq.isEmpty) { //save summary of occ images
      OccImage.saveSumary(s"$outputDir/$CSV_SUMMARY_CSV", occImageSeq)
      return error(s"There is no common sources in the images")
    }

    if (!occConf.NO_REFERENCE_SOURCES) {
        //show common sources after filtering
        var remainSourceEvolution = sourceEvolutionSeq.filter(_.sourceSeq.head.id != focusSource.id)
        warning(s"Before source evolution filtering, using: ${remainSourceEvolution.size} common sources as reference (without focus)")

        remainSourceEvolution =
          OccSourceEvolution.filterSeq(remainSourceEvolution
            , Path.resetDirectory(s"$sourceEvolutionDir/$SOURCE_DISCARDED_DIR/")
            , occConf)

        if (remainSourceEvolution.isEmpty) { //save summary of occ images
          OccImage.saveSumary(s"$outputDir/$CSV_SUMMARY_CSV", occImageSeq)
          return error(s"There is no common sources after filtering")
        }
        //show common sources after filtering
        warning(s"After filtering, using: ${remainSourceEvolution.size} common sources as reference (without focus)")

       //calculate focus evolution
       info("Calculating evolution of focus")
       sourceEvolutionSeq = OccSourceEvolution.calculateFocusEvolution(occImageSeq
                                                                       , remainSourceEvolution
                                                                       , occConf
                                                                       , verbose)

       //calculate normalized flux
       OccImage.calculateFocusNormalizedFlux(occImageSeq, focusSource, sourceEvolutionSeq)
    }
    else {
      occImageSeq.map( _.setFocusBiggerSource()) //fix focus
      originalSourceEvolutionSeq = Array()
    }

    //save source evolution
    info("Saving source evolution")
    OccSourceEvolution.saveSeq(focusName
                               , focusSource
                               , (originalSourceEvolutionSeq ++ sourceEvolutionSeq).toSet.toArray
                               , sourceEvolutionDir)

    //save summary of occ images
    OccImage.saveSumary(s"$outputDir/$CSV_SUMMARY_CSV", occImageSeq)

    //generate plot
    info("Generating plot")
    val focusSourceEvolution = sourceEvolutionSeq.filter(_.sourceSeq.head.id == focusSource.id).head
    val notFocusSourceEvolution = sourceEvolutionSeq.filterNot(_.sourceSeq.head.id == focusSource.id)
    val medianStdevFluxRelative = StatDescriptive.getWithDouble(notFocusSourceEvolution map (_.calculateStdevRelativeFlux)).median
    val apertureInfo = s"min: ${occConf.MIN_APERTURE} pix  anular: ${occConf.ANULAR_APERTURE} pix max: ${occConf.MAX_APERTURE} pix"
    val gnuplotFileName = OccImage.generatePlot(
      focusName
      , occImageSeq
      , notFocusSourceEvolution
      , s"${f"$medianStdevFluxRelative%.3f"}"
      , focusSourceEvolution.getLocation
      , focusSourceEvolution.getTimeRange
      , apertureInfo
      , outputDir)

    //summary
    warning(s"Reference image:'${focusSourceEvolution.refSource.occImage.getShortName}' " +
           s"focus on source: {${focusSourceEvolution.refSource.id}} at position: ${focusSourceEvolution.refSource.centroid.getFormattedShortString()}")
    warning(s"Processed: ${occImageSeq.size} images with: ${occImageSeq.head.getCommonSourceCount} common sources")
    warning(s"Generated gnuplot file: '${Path.getCurrentPath()}$gnuplotFileName'")

    //generate gif
    if (occConf.BUILD_GIF) {
      info("Generating gif")
     buildCroppyAndGif(focusName
                      , occConf.SIGMA_MULTIPLIER
                      , Some((occConf.SOURCE_MIN_PIX_COUNT, occConf.SOURCE_MAX_PIX_COUNT))
                      , inputDir
                      , outputDir
                      , occConf)
    }
    else info("Gif creation disabled in the configuration file")

    true
  }
  //---------------------------------------------------------------------------
  private def findSourceInImageDir(focusName: String
                                   , inputDir: String
                                   , occConf: OccConfiguration
                                   , verbose:Boolean): Array[OccImage] = {
    info(s"Loading all images of the directory: '$inputDir'")
    var fileNameSeq = Path.getSortedFileList(inputDir, MyConf.c.getStringSeq("Common.fitsFileExtension"))

    //filter images
    fileNameSeq = ManualTracking.filterImageSeq(fileNameSeq)

    if (fileNameSeq.isEmpty) {
      error(s"There is no input images in the directory: '$inputDir'")
      return Array()
    }

    //prepare the result directories
    val outputDir = s"$DEFAULT_OUTPUT_DIR/$focusName"
    info("Preparing directories")
    prepareResultDirSeq(dropPreviousResults = true, outputDir)

    //get the list of images and sort by time
    val loadPreviosResultDir = s"$DEFAULT_OUTPUT_DIR/${OccImage.PARTIAL_RESULT_DIR}/$focusName"
    var loadPreviousResultsFlag = false
    if (occConf.LOAD_PREVIOUS_RESULTS) {
      if (!Path.directoryExist(loadPreviosResultDir)) {
        warning(s"Can not load previous results on object: '$focusName' becasue directory '$loadPreviosResultDir' does not exist")
      }
      else loadPreviousResultsFlag = true
    }

    var occImageSeq =
      if (loadPreviousResultsFlag)
        loadPreviousResults(loadPreviosResultDir
                            , verbose)
      else {
         info("Calculating aperture photometry")
         calculateAperturePhotometry(focusName
                                     , (fileNameSeq map (_.getAbsolutePath)).toArray
                                     , outputDir
                                     , occConf
                                     , verbose)
      }

    //filter occImages by range
    info("Filtering images")
    occImageSeq = ManualTracking.filterRangeOccImage(occImageSeq)
    if(occImageSeq.isEmpty) {
      error(s"There is no images in the directory: '$inputDir'")
      return Array()
    }
    if (occImageSeq.size == 1) {
      error(s"There is just one image in the directory: '$inputDir', expecting at least 2")
      return Array()
    }

    //match sources
    info("Matching sources")
    OccImageFindSourceMatch.calculate(occImageSeq, occConf, verbose)

    occImageSeq
  }
  //---------------------------------------------------------------------------
  private def prepareResultDirSeq(dropPreviousResults: Boolean
                                  , outputDir: String) =
    if (dropPreviousResults)
      Path.resetDirectory(s"$outputDir/")
    else
      Path.createDirectoryIfNotExist(s"$outputDir/")
  //---------------------------------------------------------------------------
  def calculateAperturePhotometry(focusName: String
                                  , pathSeq: Array[String]
                                  , outputDir: String
                                  , occConf: OccConfiguration
                                  , verbose: Boolean) = {

    val occQueue = new ConcurrentLinkedQueue[OccImage]()
    new CalculatePhotometry(focusName
                            , pathSeq
                            , occQueue
                            ,s"$outputDir/$IMAGE_SOURCE_DETECTION_DIR/"
                            , occConf
                            , verbose)

    occQueue.asScala.toArray.sortWith(_.observingTimeMidPointJD < _.observingTimeMidPointJD) //sorted by julian date
  }
  //---------------------------------------------------------------------------
  def loadPreviousResults(inputDir: String
                          , verbose: Boolean) = {
    val occQueue = new ConcurrentLinkedQueue[OccImage]()
    new LoadPreviousResult((Path.getSubDirectoryList(inputDir)map (_.getAbsolutePath)).toArray
                            , occQueue
                            , verbose)
    occQueue.asScala.toArray.sortWith(_.observingTimeMidPointJD < _.observingTimeMidPointJD) //sorted by julian date
  }
  //---------------------------------------------------------------------------
  private class LoadPreviousResult(pathSeq: Array[String]
                                    , occImageQueue: ConcurrentLinkedQueue[OccImage]
                                    , verbose: Boolean)
    extends ParallelTask[String](
      pathSeq
      , CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 100
      , verbose = verbose) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(inputDir: String) = {
      val occImage  = OccImage.load(s"$inputDir/${OccImage.PARTIAL_RESULT_OCC_IMAGE_CSV}")
      val occSource = OccSource.load(s"$inputDir/${OccImage.PARTIAL_RESULT_OCC_SOURCE_SEQ_CSV}"
                                      , occImage)
      occImage.setSourceSeq(occSource)
      occImageQueue.add(occImage)
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private class CalculatePhotometry(focusName: String
                                    , pathSeq: Array[String]
                                    , occImageQueue: ConcurrentLinkedQueue[OccImage]
                                    , outputDir: String
                                    , occConf: OccConfiguration
                                    , verbose: Boolean)
    extends ParallelTask[String](
      pathSeq
      , CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 100) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(path: String) = {
      val img = MyImage(path)
      val observingTime = ObservingTime.obtain(img)
      if (observingTime.isEmpty)
        warning(s"Image: '${img.getRawName()}' ignored because can not obtaing the observing time")
      else {
        val occImage = OccImageAperturePhotometry.calculateAperturePhotometry(img,outputDir, occConf, verbose)
        occImage.savePartialResult(focusName)
        occImageQueue.add(occImage)
      }
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private class BuildGif(rowSeq: Array[CsvRow]
                          , imagePathSeq: Array[String]
                          , sigmaMultiplier: Double
                          , rectangleSize: Point2D
                          , sourceDetectionSizeRestriction: Option[(Int, Int)]
                          , oDirFits: String
                          , oDirPng: String
                          , subImageMap: ConcurrentHashMap[Double,(String,Long)]
                          , occConf:OccConfiguration
                          , verbose: Boolean  = false)
    extends ParallelTask[CsvRow](
      rowSeq
      , CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 100
      , verbose = verbose) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(row: CsvRow) = {
      //load row values
      val imageRawName = row.getString(OccImage.COL_NAME_IMAGE_NAME)
      val centroid = Point2D_Double(row.getDouble(OccSource.COL_NAME_CENTROID_X)
        , row.getDouble(OccSource.COL_NAME_CENTROID_Y)).toPoint2D()
      val imagePath = imagePathSeq.find(_.contains(imageRawName)).head
      val img = MyImage(imagePath)
      val julianDate = row.getDouble(OccImage.COL_NAME_OBSERVING_TIME_MID_POINT_JD)
      val source_flux_per_second = Math.round(row.getDouble(OccSource.COL_NAME_FLUX_PER_SECOND))

      val noiseTide = img.getBackground(verbose) + (img.getBackgroundRMS(verbose) * sigmaMultiplier)
      val rectangleHull = Rectangle(centroid - rectangleSize, centroid + rectangleSize)
      img.saveCroppy(
        Point2D_Double(centroid)
        , outputFileNameFits = s"$oDirFits/$imageRawName.fits"
        , outputFileNamePng = s"$oDirPng/$imageRawName.png"
        , croppyXpixSize = occConf.CROPPY_X_SIZE
        , croppyYpixSize = occConf.CROPPY_Y_SIZE
        , rectangleHull
        , rectangleOffset = Point2D.POINT_ZERO
        , noiseTide
        , sigmaMultiplier
        , sourceDetectionSizeRestriction)

      subImageMap.put(julianDate,(img.getRawName(), source_flux_per_second))
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def buildCroppyAndGif(focusName: String
                        , sigmaMultiplier: Double
                        , sourceDetectionSizeRestriction: Option[(Int, Int)] = None
                        , inputDir: String
                        , outputDir: String
                        , occConf:OccConfiguration): Boolean = {
    //--------------------------------------------------------------------------
    info("Generating croppy images and gif")
    //--------------------------------------------------------------------------
    def findFocusCommonOccSourceDir(): Option[String] = {
      Path.getSortedSubDirectoryList(s"$outputDir/$SOURCE_EVOLUTION_DIR").foreach { path=>
        if (path.getAbsolutePath.contains("_focus")) return Some(path.getAbsolutePath) }
      None
    }
    //--------------------------------------------------------------------------
    //locate dir where the focus common source is strored
    val csvFileName = findFocusCommonOccSourceDir().getOrElse{
      return error(s"Error can not find the focus common dir for: '$focusName'")
    } +  s"/${OccSourceEvolution.SOURCE_CSV}"

    //get all image path
    val imagePathSeq = Path.getSortedFileListRecursive(inputDir, MyConf.c.getStringSeq("Common.fitsFileExtension"))
      .map(_.getAbsolutePath)
      .toArray

    //load the csv and create the cropped fits
    val oDirFits = Path.resetDirectory(s"$outputDir/$FOCUS_FITS_CROPPY_SEQ_DIR")
    val oDirPng = Path.resetDirectory(s"$outputDir/$FOCUS_PNG_CROPPY_SEQ_DIR")
    val rectangleSize = Point2D(occConf.CROPPY_SOURCE_RECTANGLE_HULL_X_SIZE, occConf.CROPPY_SOURCE_RECTANGLE_HULL_Y_SIZE)
    val csv = CsvRead(csvFileName, itemDivider = OccImage.sep)
    csv.read()

    val subImageMap = new ConcurrentHashMap[Double, (String, Long)]()
    new BuildGif(csv.getRowSeq
      , imagePathSeq
      , sigmaMultiplier
      , rectangleSize
      , sourceDetectionSizeRestriction
      , oDirFits
      , oDirPng
      , subImageMap
      , occConf
      , verbose  = false)

    //sort and build label map
    val labelMap = scala.collection.mutable.Map[String,String]()
    val subImageSeq = subImageMap.asScala.toSeq
    subImageSeq.sortWith(_._1 < _._1).zipWithIndex.map { case (t,i)=>
      labelMap(t._2._1) = s"${f"$i%04d"}:${t._2._2}"
    }

    val imageSeqName = subImageSeq.sortWith(_._1 < _._1) map {t=> t._2._1}

    UtilImage.buildGif(
        Path.getCurrentPath() + oDirFits
      , outputGifName = s"$outputDir/${focusName}_occultation_fits.gif"
      , fileExtension = ".fits"
      , useZscale = true
      , occConf.GIF_SPEED_MS
      , title = s"$focusName"
      , imageSeqName = imageSeqName
      , labelMap = labelMap.toMap)

    true
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SourceOccultation.scala
//=============================================================================
