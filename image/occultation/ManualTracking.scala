/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/May/2023
 * Time:  12h:04m
 * Description: None
 */
//=============================================================================
package com.common.image.occultation
//=============================================================================
import com.common.image.occultation.occImage.OccImage
import com.common.logger.MyLogger
import com.common.util.path.Path

import scala.collection.mutable.ArrayBuffer
//=============================================================================
import java.io.File
import scala.collection.mutable
//=============================================================================
//=============================================================================
object ManualTracking extends MyLogger {
  //---------------------------------------------------------------------------
  val skipImageMap = mutable.Map[String,String]()
  //---------------------------------------------------------------------------
  val skipImageRangeSeq = ArrayBuffer[(String,String)]()
  //---------------------------------------------------------------------------
  val sourceFixedIdMap = mutable.Map[Int,Int]()
  //---------------------------------------------------------------------------
  //map(image,map(refImage,map(ref_source_ID,source_ID))
  private val matchSourceMap =
    mutable.Map[String   //image
               , mutable.Map[String  //refImage
                             ,mutable.Map[Int  //ref_source_ID
                                          ,Int]]]() //source_ID
  //---------------------------------------------------------------------------
  def add(imageName:String
         , refImageName: String
         , sourceID: Int
         , refSourceID: Int) = {

    if(!matchSourceMap.contains(imageName))
      matchSourceMap(imageName) =
        mutable.Map[String //refImage
                  , mutable.Map[Int //ref_source_ID
                               ,Int]]() //source_ID
    val imageMap = matchSourceMap(imageName)
    if (!imageMap.contains(refImageName)) imageMap(refImageName) = mutable.Map[Int //ref_source_ID
                                                                    , Int] () //source_ID
    imageMap(refImageName)(refSourceID) = sourceID
  }
  //---------------------------------------------------------------------------
  def getSourceMatchMap(imageName: String
                       , refImageName: String): Option[mutable.Map[Int, Int]] = {
    if (matchSourceMap.isEmpty) return None
    if (!matchSourceMap.contains(imageName)) return None
    val imageMap = matchSourceMap(imageName)
    if (!imageMap.contains(refImageName)) return None
    Some(imageMap(refImageName))
  }
  //---------------------------------------------------------------------------
  def getSourceFixedIdMap(): Option[mutable.Map[Int, Int]] = {
    if (sourceFixedIdMap.isEmpty) return None
    Some(sourceFixedIdMap)
  }
  //---------------------------------------------------------------------------
  def filterImageSeq(fileNameSeq: List[File]) = {
    if (ManualTracking.skipImageMap.isEmpty) fileNameSeq
    else {
      ManualTracking.skipImageMap.keys.foreach { s =>
        warning(s"Manual tracking. Avoiding image:'$s'")
      }
      //single image
      fileNameSeq filter (f =>
        !ManualTracking.skipImageMap.contains(Path.getOnlyFilenameNoExtension(f.getAbsolutePath)))
    }
  }
  //---------------------------------------------------------------------------
  def filterRangeOccImage(occImageSeq: Array[OccImage]): Array[OccImage] = {

    if (ManualTracking.skipImageRangeSeq.isEmpty) return occImageSeq

    var filteredOccImageSeq = occImageSeq
    ManualTracking.skipImageRangeSeq.foreach { case (minImageName,maxImageName) =>

      warning(s"Applyig the skipImageRange:'($minImageName,$maxImageName)'")

      val minOccImage = filteredOccImageSeq.filter( _.getShortName == minImageName )
      val maxOccImage = filteredOccImageSeq.filter( _.getShortName == maxImageName )
      if (minOccImage.isEmpty)
        warning(s"Can not find the image:'$minImageName', so ignoring the skipImageRange:'($minImageName,$maxImageName)'")
      if (maxOccImage.isEmpty)
        warning(s"Can not find the image:'$maxImageName', so ignoring the skipImageRange:'($minImageName,$maxImageName)'")

      if (!minOccImage.isEmpty && !maxOccImage.isEmpty) {
        val minTime = minOccImage.head.observingTimeMidPointJD
        val maxTime = maxOccImage.head.observingTimeMidPointJD
        filteredOccImageSeq = filteredOccImageSeq.filter(occImage =>
          (occImage.observingTimeMidPointJD < minTime) ||
          (occImage.observingTimeMidPointJD > maxTime)
        )
      }
    }
    filteredOccImageSeq
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ManualTracking.scala
//=============================================================================
