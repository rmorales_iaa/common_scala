/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  01/Apr/2023
 * Time:  10h:25m
 * Description: None
 */
//=============================================================================
package com.common.image.astrometry
//=============================================================================
import com.common.configuration.MyConf
import BuildInfo.BuildInfo
import com.common.fits.simpleFits.SimpleFits.{KEY_M2_RUN_DATE, KEY_M2_VERSION}
import com.common.hardware.cpu.CPU
import com.common.image.myImage.MyImage
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
import com.common.util.time.Time
//=============================================================================
//=============================================================================
object Align extends MyLogger {
  //---------------------------------------------------------------------------
  private final val ACTION_ALIGN_FLIP_V            = "flip_v"
  private final val ACTION_ALIGN_FLIP_H            = "flip_h"

  private final val ACTION_ALIGN_ROTATE_90         = "rot+90"
  private final val ACTION_ALIGN_ROTATE_MINUS_90   = "rot-90"

  private final val ACTION_ALIGN_ROTATE_180         = "rot+180"
  private final val ACTION_ALIGN_ROTATE_MINUS_180   = "rot-180"

  private final val ACTION_ALIGN_ROTATE_270         = "rot+270"
  private final val ACTION_ALIGN_ROTATE_MINUS_270   = "rot-270"

  private val ACTION_SEQ = Seq (
      ACTION_ALIGN_FLIP_V
    , ACTION_ALIGN_FLIP_H
    , ACTION_ALIGN_ROTATE_90
    , ACTION_ALIGN_ROTATE_MINUS_90
    , ACTION_ALIGN_ROTATE_180
    , ACTION_ALIGN_ROTATE_MINUS_180
    , ACTION_ALIGN_ROTATE_270
    , ACTION_ALIGN_ROTATE_MINUS_270)
  //---------------------------------------------------------------------------
  private class ParallelProcessing(pathSeq: Array[String]
                                   , outputDir: String
                                   , action:Option[String]) extends ParallelTask[String](
    pathSeq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(path: String) = {
      alignImage(path,outputDir,action)
    }
    //-----------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private def isCloseToRefValue(v: Double, refValue: Double, margin: Double) = {
    Math.abs(v - refValue) < margin
  }
  //---------------------------------------------------------------------------
  private def approxClosestAngle(angle: Double, margin: Double = 10): Option[Double] = {
    Seq(0, 90, 180, 270, -90, -180, -270) foreach { refValue =>
      if (isCloseToRefValue(angle, refValue, margin)) return Some(refValue)
    }
    None
  }
  //---------------------------------------------------------------------------
  private def processAction(img: MyImage
                            , outputDir:String
                            , action: String) : Boolean = {

    val fileName = img.getRawName()
    if (!ACTION_SEQ.contains(action.trim.toLowerCase())) {
      error(s"Image:'${fileName}' action:'$action' unknown")
      return false
    }
    info(s"Image:'${img.getRawName()} performing action:'$action'")
    val newImage = action match {
      case ACTION_ALIGN_FLIP_V            => img.verticalFlip()
      case ACTION_ALIGN_FLIP_H            => img.horizontalFlip()

      case ACTION_ALIGN_ROTATE_90         => img.rotation90(clockwise = true)
      case ACTION_ALIGN_ROTATE_MINUS_90   => img.rotation90(clockwise = false)

      case ACTION_ALIGN_ROTATE_180        => img.rotation180(clockwise = true)
      case ACTION_ALIGN_ROTATE_MINUS_180  => img.rotation180(clockwise = false)

      case ACTION_ALIGN_ROTATE_270        => img.rotation270(clockwise = true)
      case ACTION_ALIGN_ROTATE_MINUS_270  => img.rotation270(clockwise = false)
    }
    newImage.saveAsFits(s"$outputDir/$fileName"
    , buildInfo = Array((KEY_M2_VERSION, s"'${BuildInfo.version.trim}'")
                       , (KEY_M2_RUN_DATE, s"'${Time.getISO_DateTimeStamp}'")))
    true
  }
  //---------------------------------------------------------------------------
  private def alignImage(path:String
                         , outputDir:String
                         , action:Option[String]): Boolean = {
    var img = MyImage(path)
    val wcs = img.getWcs()
    val fileName = Path.getOnlyFilename(path)

    if (action.isDefined)
      return processAction(img,outputDir,action.get)

    val horizontalFlip = wcs.imflip == 1
    val rotAngle = -wcs.orientation  //rotation angle to obtain the aligment with celestial coordinates

    val appRotAngle = approxClosestAngle(rotAngle).getOrElse {
      info(s"Imagen: '$fileName' was not modified")
      return false
    }
    if (appRotAngle == 0) {
      info(s"Imagen: '$fileName' was not modified. Rotation angle:$rotAngle")
      return false
    }

    if (horizontalFlip) img = img.verticalFlip()
    val clockwise = appRotAngle > 0
    img = Math.abs(appRotAngle) match {
      case 90  => img.rotation90(clockwise,invertY = !clockwise)
      case 180 => img.rotation180(clockwise)
      case 270 => img.rotation270(clockwise)
    }

    info(s"Imagen: '$fileName' has been rotated: $appRotAngle degrees")
    img.saveAsFits(s"$outputDir/$fileName"
                   , buildInfo = Array((KEY_M2_VERSION, s"'${BuildInfo.version.trim}'")
                                    , (KEY_M2_RUN_DATE, s"'${Time.getISO_DateTimeStamp}'")))
    true
  }
  //---------------------------------------------------------------------------
  def processDirectory(inputDir:String, outputDir:String, action:Option[String]): Boolean = {
    val pathSeq = Path.getSortedFileList(inputDir, MyConf.c.getStringSeq("Common.fitsFileExtension"))
      .map (_.getAbsolutePath)
      .toArray
    if (pathSeq.isEmpty) return error(s"There is no input images in the directory: '$inputDir'")
    else new ParallelProcessing(pathSeq, Path.resetDirectory(outputDir), action)
    true
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file Align.scala
//=============================================================================
