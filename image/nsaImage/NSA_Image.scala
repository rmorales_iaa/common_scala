/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  24/May/2024
 * Time:  05h:03m
 * Description: single image FITS (just one HDU storing data block of 2 dimensions)
 * stored as non sequential array. It can store up to 4 TiB of data
 * The padding data at the end of FITS file is also stored
 */
package com.common.image.nsaImage
//=============================================================================
import com.common.configuration.MyConf
import com.common.dataType.array._
import com.common.fits.standard.ItemSize
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.dataType.DataType._
import com.common.fits.standard.fits.{Fits, FitsLoad}
import com.common.fits.standard.structure.hdu.primary.Primary
import com.common.image.nsaImage.source.{Source, SubImageSourceDetection}
import com.common.geometry.point.{Point2D, Point2D_Double}
import com.common.logger.MyLogger
//=============================================================================
import scala.reflect.ClassTag
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object NSA_Image extends MyLogger with Serializable {
  //---------------------------------------------------------------------------
  private final val BLOCK_BYTE_SIZE = ItemSize.BLOCK_BYTE_SIZE
  //---------------------------------------------------------------------------
  private final val BACKGROUND_SUB_IMAGE_X_SIZE = MyConf.c.getInt("Background.meshX_PixSize")
  private final val BACKGROUND_SUB_IMAGE_Y_SIZE = MyConf.c.getInt("Background.meshX_PixSize")
  //---------------------------------------------------------------------------
  private final val SOURCE_DETECTION_SUB_IMAGE_X_SIZE = MyConf.c.getInt("SourceDetection.subImageX_PixSize")
  private final val SOURCE_DETECTION_SUB_IMAGE_Y_SIZE = MyConf.c.getInt("SourceDetection.subImageY_PixSize")
  //---------------------------------------------------------------------------
  private final val SOURCE_DETECTION_SIGMA_MULTIPLIER = MyConf.c.getDouble("SourceDetection.sigmaMultiplier")
  //---------------------------------------------------------------------------
  private def apply[ITEM_DATA_TYPE: ClassTag: Numeric]
                   (nsaImage: NSA_Image[ITEM_DATA_TYPE]
                   , dataBlockSeq: Array[DataBlock[ITEM_DATA_TYPE]]): NSA_Image[_] =
    NSA_Image(nsaImage.name, nsaImage, dataBlockSeq, nsaImage.getBitPix)
  //---------------------------------------------------------------------------
  private def apply[ITEM_DATA_TYPE: ClassTag : Numeric]
                   (name: String
                    , nsaImage: NSA_Image[ITEM_DATA_TYPE]
                    , dataBlockSeq: Array[DataBlock[ITEM_DATA_TYPE]]
                    , bitPix: Int): NSA_Image[_] =
    NSA_Image(name, nsaImage.header, dataBlockSeq, bitPix)
  //---------------------------------------------------------------------------
  private def apply[ITEM_DATA_TYPE: ClassTag : Numeric]
                   (name: String
                    , header: RecordMap
                    , dataBlockSeq: Array[DataBlock[ITEM_DATA_TYPE]]
                    , bitPix: Int): NSA_Image[_] = {

    bitPix.toString match {
      case DATA_TYPE_ID_UINT_8 =>
        val nsa = NSA_Byte(dataBlockSeq.asInstanceOf[Array[DataBlock[NSA_Byte.ITEM_DATA_TYPE]]], BLOCK_BYTE_SIZE)
        NSA_Image[NSA_Byte.ITEM_DATA_TYPE](name, nsa, header, minPixValue = -1, maxPixValue = -1)

      case DATA_TYPE_ID_INT_16 =>
        val nsa = NSA_Short(dataBlockSeq.asInstanceOf[Array[DataBlock[NSA_Short.ITEM_DATA_TYPE]]], BLOCK_BYTE_SIZE)
        NSA_Image[NSA_Short.ITEM_DATA_TYPE](name, nsa, header, minPixValue = -1, maxPixValue = -1)

      case DATA_TYPE_ID_INT_32 =>
        val nsa = NSA_Int(dataBlockSeq.asInstanceOf[Array[DataBlock[NSA_Int.ITEM_DATA_TYPE]]], BLOCK_BYTE_SIZE)
        NSA_Image[NSA_Int.ITEM_DATA_TYPE](name, nsa, header, minPixValue = -1, maxPixValue = -1)

      case DATA_TYPE_ID_INT_64 =>
        val nsa = NSA_Long(dataBlockSeq.asInstanceOf[Array[DataBlock[NSA_Long.ITEM_DATA_TYPE]]], BLOCK_BYTE_SIZE)
        NSA_Image[NSA_Long.ITEM_DATA_TYPE](name, nsa, header, minPixValue = -1, maxPixValue = -1)

      case DATA_TYPE_ID_FLOAT_32 =>
        val nsa = NSA_Float(dataBlockSeq.asInstanceOf[Array[DataBlock[NSA_Float.ITEM_DATA_TYPE]]], BLOCK_BYTE_SIZE)
        NSA_Image[NSA_Float.ITEM_DATA_TYPE](name, nsa, header, minPixValue = -1, maxPixValue = -1)

      case DATA_TYPE_ID_FLOAT_64 =>
        val nsa = NSA_Double(dataBlockSeq.asInstanceOf[Array[DataBlock[NSA_Double.ITEM_DATA_TYPE]]], BLOCK_BYTE_SIZE)
        NSA_Image[NSA_Double.ITEM_DATA_TYPE](name, nsa, header, minPixValue = -1, maxPixValue = -1)
    }
  }
  //---------------------------------------------------------------------------
  private def fitDataType(mM:Point2D_Double): NonSequentialArray[_] =
    fitDataType(mM.x, mM.y)
  //---------------------------------------------------------------------------
  private def fitDataType(min: Double
                          , max: Double): NonSequentialArray[_] = {
    (min, max) match {
      case _ if min >= Byte.MinValue && max <= Byte.MaxValue =>
        NSA_Byte.build(BLOCK_BYTE_SIZE)

      case _ if min >= Short.MinValue && max <= Short.MaxValue =>
        NSA_Short.build(BLOCK_BYTE_SIZE)

      case _ if min >= Int.MinValue && max <= Int.MaxValue =>
        NSA_Int.build(BLOCK_BYTE_SIZE)

      case _ if min >= Long.MinValue && max <= Long.MaxValue =>
        NSA_Long.build(BLOCK_BYTE_SIZE)

      case _ if min >= Float.MinValue && max <= Float.MaxValue =>
        NSA_Float.build(BLOCK_BYTE_SIZE)

      case _ => NSA_Double.build(BLOCK_BYTE_SIZE)
    }
  }
  //---------------------------------------------------------------------------
  private def load(name:String
                   , primaryHDU: Primary
                   , m: Double = -1
                   , M: Double = -1): Option[NSA_Image[_]] = {
    val dataBlockByteSeq = primaryHDU.dataBlockSeq.map( _.byteSeq)
    val header = primaryHDU.recordMap

    val nsaFits = primaryHDU.dataType match {
      case DATA_TYPE_U_INT_8  =>
        NSA_Image[NSA_Byte.ITEM_DATA_TYPE](name, NSA_Byte(dataBlockByteSeq, BLOCK_BYTE_SIZE), header, m, M)

      case DATA_TYPE_INT_16   =>
        NSA_Image[NSA_Short.ITEM_DATA_TYPE](name, NSA_Short(dataBlockByteSeq, BLOCK_BYTE_SIZE), header, m, M)

      case DATA_TYPE_INT_32   =>
        NSA_Image[NSA_Int.ITEM_DATA_TYPE](name, NSA_Int(dataBlockByteSeq, BLOCK_BYTE_SIZE), header, m, M)

      case DATA_TYPE_INT_64   =>
        NSA_Image[NSA_Long.ITEM_DATA_TYPE](name, NSA_Long(dataBlockByteSeq, BLOCK_BYTE_SIZE), header, m, M)

      case DATA_TYPE_FLOAT_32 =>
        NSA_Image[NSA_Float.ITEM_DATA_TYPE](name, NSA_Float(dataBlockByteSeq, BLOCK_BYTE_SIZE), header, m, M)

      case DATA_TYPE_FLOAT_64 =>
        NSA_Image[NSA_Double.ITEM_DATA_TYPE](name, NSA_Double(dataBlockByteSeq, BLOCK_BYTE_SIZE), header, m, M)

      case _ =>
        error(s"Data type: '${primaryHDU.dataType.name}' not supported")
        return None
    }
    Some(nsaFits)
  }
  //---------------------------------------------------------------------------
  //load standard FITS
  def load(fits: Fits): Option[NSA_Image[_]] = {

    //check if FITS is SIF
    if (!fits.isSIF()) {
      error(s"Can only process SIF (single image FITS) files. File: '${fits.name}'")
      return None
    }
    val primaryHDU = fits.getPrimaryHdu()

    //load file as non sequential array in the native data type before applying any transformation
    val nsaFits = load(fits.name, primaryHDU).getOrElse {
      error(s"Can not load the file as an 'non sequential array'. File: '${fits.name}'")
      return None
    }

    //get the min and max pix values int the original FITS data type and applying the scale
    //the get the most appropriate type to be used
    val targetType = fitDataType(nsaFits.getMinMaxScaled)

    val result = targetType match {
      case _: NSA_Byte =>
        nsaFits.applyLinearTransformation(fits.name, nsaFits.header, targetType.asInstanceOf[NSA_Byte])

      case _: NSA_Short =>
        nsaFits.applyLinearTransformation(fits.name, nsaFits.header, targetType.asInstanceOf[NSA_Short])

      case _: NSA_Int =>
        nsaFits.applyLinearTransformation(fits.name, nsaFits.header, targetType.asInstanceOf[NSA_Int])

      case _: NSA_Long =>
        nsaFits.applyLinearTransformation(fits.name, nsaFits.header, targetType.asInstanceOf[NSA_Long])

      case _: NSA_Float =>
        nsaFits.applyLinearTransformation(fits.name, nsaFits.header, targetType.asInstanceOf[NSA_Float])

      case _: NSA_Double =>
        nsaFits.applyLinearTransformation(fits.name, nsaFits.header, targetType.asInstanceOf[NSA_Double])
    }
    Some(result)
  }
  //---------------------------------------------------------------------------
  def localFileLoad(fitsFilename: String
                    , readDataBlockFlag: Boolean = true
                    , printDisconfFlag: Boolean = false
                    , ignoreDisconf: Boolean = true): Option[NSA_Image[_]] = {
    val fits = FitsLoad.load(fitsFilename
                             , readDataBlockFlag
                             , printDisconfFlag
                             , ignoreDisconf).getOrElse(return None)
    load(fits)
  }
  //---------------------------------------------------------------------------
  def getBackgroundAndRMS(backGroundAndRmsSeq: Array[(Double, Double)]): (Double, Double) = {
    val (estimatedBackground, estimatedBackgroundRMS) =
      if (backGroundAndRmsSeq.size % 2 == 1) backGroundAndRmsSeq(backGroundAndRmsSeq.size / 2)
      else {
        val posRight = backGroundAndRmsSeq.size / 2
        val posLeft = posRight - 1
        ((backGroundAndRmsSeq(posRight)._1 + backGroundAndRmsSeq(posLeft)._1) / 2.0d
          , (backGroundAndRmsSeq(posRight)._2 + backGroundAndRmsSeq(posLeft)._2) / 2.0d)
      }
    (estimatedBackground, estimatedBackgroundRMS)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import NSA_Image._
case class NSA_Image[ITEM_DATA_TYPE: ClassTag: Numeric]
                    (name: String
                    , dataBlock: NonSequentialArray[ITEM_DATA_TYPE]
                    , header: RecordMap
                    , minPixValue: Double
                    , maxPixValue: Double) extends MyLogger {
  //---------------------------------------------------------------------------
  val classTag: ClassTag[ITEM_DATA_TYPE] = implicitly[ClassTag[ITEM_DATA_TYPE]]
  val numeric: Numeric[ITEM_DATA_TYPE] = implicitly[Numeric[ITEM_DATA_TYPE]]
  //---------------------------------------------------------------------------
  def getDimensions = Point2D(getX_PixCount, getY_PixCount)
  //---------------------------------------------------------------------------
  def getPixCount = getX_PixCount * getY_PixCount
  //---------------------------------------------------------------------------
  def getX_PixCount = header.getAxisSeq().head.toInt
  //---------------------------------------------------------------------------
  def getY_PixCount = header.getAxisSeq().last.toInt
  //---------------------------------------------------------------------------
  def getBitPix = header.getBitPix().toInt
  //---------------------------------------------------------------------------
  def getScale = header.getBscale()
  //---------------------------------------------------------------------------
  def getZero = header.getBzero()
  //---------------------------------------------------------------------------
  def getMinMaxPix = dataBlock.getMinMax()
  //---------------------------------------------------------------------------
  def getMinMaxScaled = {
    val (minNoScaled, maxNoScaled) =  dataBlock.getMinMax()
    val bScale = getScale
    val bZero = getZero

    val m = dataBlock.toDoubleImpl(minNoScaled)
    val M = dataBlock.toDoubleImpl(maxNoScaled)
    Point2D_Double(m * bScale + bZero, M * bScale + bZero)
  }
  //---------------------------------------------------------------------------
  def applyLinearTransformation[T: ClassTag: Numeric]
  (name: String
   , header: RecordMap
   , targetType: NonSequentialArray[T])
  : NSA_Image[T] = {
    val targetItemByteSize = targetType.itemByteSize
    val bScale = header.getBscale()
    val bZero = header.getBzero()
    val scaledDataBlock = dataBlock.applyLinearTransformation(bScale, bZero, targetType)
    val expectedBlockPerDataBlock = BLOCK_BYTE_SIZE / targetItemByteSize
    val actualBlockPerDataBlock = scaledDataBlock.head.itemSeq.length

    val redistributesDataBLockSeq =
      if (expectedBlockPerDataBlock != actualBlockPerDataBlock)
        DataBlock.redistribute(scaledDataBlock, expectedBlockPerDataBlock, targetItemByteSize)
      else scaledDataBlock

    NSA_Image[T](name, header, redistributesDataBLockSeq, targetItemByteSize * 8)
      .asInstanceOf[NSA_Image[T]]
  }
  //---------------------------------------------------------------------------
  def getImage(min: Point2D
               , max: Point2D): Option[NSA_Image[ITEM_DATA_TYPE]] = {
    val subImageDataBlock = dataBlock.getRectangle(getX_PixCount
                                                   , getY_PixCount
                                                   , min.x
                                                   , min.y
                                                   , max.x
                                                   , max.y).getOrElse(return None)
     Some(NSA_Image(name
                    , this
                    , subImageDataBlock
                    , getStorageSizeInBits).asInstanceOf[NSA_Image[ITEM_DATA_TYPE]])
  }
  //---------------------------------------------------------------------------
  def collect = dataBlock.collect()
  //---------------------------------------------------------------------------
  def getSubImagePartition(subWidth: Int, subHeight: Int)
                           : Array[NSA_SubImage[ITEM_DATA_TYPE]] = {

    val imageWidth = getX_PixCount
    val imageHeight = getY_PixCount
    info(s"Input image: ${imageWidth}x$imageHeight pixels. Creating the a sequence of sub-images ${subWidth}x$subHeight pixels")

    val axisPartitionSeq = Point2D.getAxisPartitionSeq(
      imageWidth
      , imageHeight
      , subWidth
      , subHeight)

    info(s"Creating ${axisPartitionSeq.length} sub-images of ${subWidth}x$subHeight pixels")

    axisPartitionSeq map { case (minPos, maxPos) =>
      NSA_SubImage(getImage(minPos,maxPos).get.dataBlock
                   , minPos
                   , maxPos)
    }
  }
  //---------------------------------------------------------------------------
  //this is the bit per pixel used in memory storage, not the one specified in
  // FITS file definition. It is the result of linear transformation
  // (BSCALE and BZERO) and data type fitting
  def getStorageSizeInBits() = {
    dataBlock match {
      case  _:NSA_Byte   => NSA_Byte.ITEM_DATA_TYPE_BYTE_SIZE * 8
      case  _:NSA_Short  => NSA_Short.ITEM_DATA_TYPE_BYTE_SIZE * 8
      case  _:NSA_Int    => NSA_Int.ITEM_DATA_TYPE_BYTE_SIZE * 8
      case  _:NSA_Long   => NSA_Long.ITEM_DATA_TYPE_BYTE_SIZE * 8
      case  _:NSA_Float  => NSA_Float.ITEM_DATA_TYPE_BYTE_SIZE * 8
      case  _:NSA_Double => NSA_Double.ITEM_DATA_TYPE_BYTE_SIZE * 8
    }
  }
  //---------------------------------------------------------------------------
  private def getBackgroundAndRmsMap() = {
    //-------------------------------------------------------------------------
    def subImageGetEstBackgroundAndRms(subImage: NSA_SubImage[ITEM_DATA_TYPE]) =
      (subImage.minPos, subImage.estBackgroundAndRms())
    //-------------------------------------------------------------------------
    val subImageSeq = getSubImagePartition(BACKGROUND_SUB_IMAGE_X_SIZE
                                           , BACKGROUND_SUB_IMAGE_Y_SIZE)
    val r = NSA_SubImage.parallelProcessing(
        subImageSeq
      , subImageGetEstBackgroundAndRms
      , Some(s"->estimating background and RMS of sub-images of size: '${BACKGROUND_SUB_IMAGE_X_SIZE}x$BACKGROUND_SUB_IMAGE_Y_SIZE' pixels"))

    r.toMap
  }
  //---------------------------------------------------------------------------
  // The size of each sub-image (sourceSizeRestriction)
  // must be greater than the maximum source present in the image. If this is not
  // the case, it is possible that small parts of the source that cross the sub-image boundaries
  // will be detected as new sources, instead of being recognized as parts of the main source.
  def getSourceSeq(providedGlobalEstDetectionLevel: Option[Double] = None
                   , mainImageDimension: Point2D
                   , sourceSizeRestriction: Option[(Int,Int)]
                   , providedSubImageSize: Option[Point2D] = None
                   , verbose: Boolean)
                  : Array[Source[ITEM_DATA_TYPE]] = {
    //-------------------------------------------------------------------------
    val subImagePixSize = if (providedSubImageSize.isDefined)
                            Point2D (providedSubImageSize.get.x
                                     , providedSubImageSize.get.y)
                          else Point2D(SOURCE_DETECTION_SUB_IMAGE_X_SIZE
                                       , SOURCE_DETECTION_SUB_IMAGE_Y_SIZE)
    val subImageSeq = getSubImagePartition(subImagePixSize.x
                                           , subImagePixSize.y)
    //-------------------------------------------------------------------------
    def subImageGetSourceSeq(subImage: NSA_SubImage[ITEM_DATA_TYPE])
                             : Array[Source[ITEM_DATA_TYPE]] = {
      val compiledSourceSeq = ArrayBuffer[Source[ITEM_DATA_TYPE]]()
      SubImageSourceDetection.findSourceSeq(subImage
                                            , compiledSourceSeq
                                            , verbose = verbose)

      compiledSourceSeq.toArray
    }
    //-------------------------------------------------------------------------
    //calculate background and rms in each sub image. Then use those values to obtain
    // a global background and rms and final calculate the global estimation of detection level

    val globalEstDetectionLevel =
      providedGlobalEstDetectionLevel.getOrElse {
        val backgroundAndRmsMap = getBackgroundAndRmsMap()
        val (globalEstimatedBackground, globalEstimatedBackgroundRMS) = getBackgroundAndRMS(backgroundAndRmsMap.values.toArray)
        globalEstimatedBackground + (globalEstimatedBackgroundRMS * SOURCE_DETECTION_SIGMA_MULTIPLIER)
    }

    //adding the global estimation of detection level to each subImage
    subImageSeq.foreach(subImage=>subImage.setEstDetectionLevel(globalEstDetectionLevel))

    val compiledSourceSeq = NSA_SubImage.parallelProcessing(subImageSeq
                                                            , subImageGetSourceSeq
                                                            , Some(s"->finding sources in sub-image with size: '${SOURCE_DETECTION_SUB_IMAGE_X_SIZE}x$SOURCE_DETECTION_SUB_IMAGE_X_SIZE' pixels")
                                                            , verbose = verbose)
                                                            .flatten

    warning(s"Image: '$name' source detection level:" + s"'${f"$globalEstDetectionLevel%10.3f"}' compiled source count before merging: ${compiledSourceSeq.length}")

    SubImageSourceDetection.mergeSourceSeq(compiledSourceSeq
                                           , mainImageDimension
                                           , subImagePixSize
                                           , sourceSizeRestriction
                                           , verbose)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file NSA_Image.scala
//=============================================================================