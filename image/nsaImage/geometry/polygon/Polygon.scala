/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Jun/2024
 * Time:  13h:33m
 * Description: Closed generic polygon
 */
package com.common.image.nsaImage.geometry.polygon
//=============================================================================
import com.common.image.nsaImage.geometry.point.{Point2D, Point2D_Double}
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
import scala.reflect.ClassTag
import java.util.concurrent.atomic.AtomicLong
//=============================================================================
//=============================================================================
object Polygon {
  //---------------------------------------------------------------------------
  private val id = new AtomicLong(-1)
  //---------------------------------------------------------------------------
  private def getNewID: Long = id.addAndGet(1)
  //---------------------------------------------------------------------------
  //https://jamesadam.me/2013/11/21/grahams-scan-in-scala/
  def convexHullWithGrahamScan(pointSeq: List[Point2D[Double]]): List[Point2D[Double]] = {
    if (pointSeq.size < 3) return pointSeq
    //-------------------------------------------------------------------------
    //Scan the List of coordinates and find our vertices
    def scan(theCoords: List[Point2D[Double]]): List[Point2D[Double]] = theCoords match {
      case xs if xs.isEmpty => List()
      case xs if xs.size == 2 => theCoords
      case x :: y :: z :: xs if orientation(x, y, z) > 0 => x :: scan(y :: z :: xs)
      case x :: y :: z :: xs => scan(x :: z :: xs)
    }
    //-------------------------------------------------------------------------
    //find the coordinate with the lowest y
    val origin = pointSeq.minBy(_.y)

    //sort the rest of the points according to their polar angle (the angle between the line
    //defined by the origin and the current point, and the x-axis)
    val coordList = origin :: pointSeq.filterNot(_ == origin)
      .sortBy{point => Math.atan2(point.y - origin.y, point.x - origin.x)}
    //do the graham scan
    scan(coordList)
  }
  //---------------------------------------------------------------------------
  def buildWithFixedPointCount(pointSeq: Array[Point2D[Double]]
                               , pointCount: Int
                               , keepPolygonWithLessVertex: Boolean = false) =
    pointSeq.sliding(pointCount).flatMap { seq =>

      val vertexSeq = convexHullWithGrahamScan(seq.toList)
        .toArray
        .dropRight(1) //sort the vertex to obtain a convex hull and remove the last point that is equal to the first one

      if (vertexSeq.length != (pointCount - 1)) {
        if (keepPolygonWithLessVertex) Some(Polygon[Double](Point2D.sortSmallFirst(vertexSeq)))
        else None
      }
      else Some(Polygon[Double](Point2D.sortSmallFirst(vertexSeq)))
    }
  //---------------------------------------------------------------------------
  def buildWithDoubleNoSort(vertexSeq: Array[(Double, Double)]): Polygon[Double] =
    Polygon[Double]( vertexSeq map (v => Point2D[Double](v._1, v._2)) )
  //---------------------------------------------------------------------------
  def buildWithDouble(vertexSeq: Array[(Double,Double)]): Polygon[Double] =
    Polygon[Double](
      Point2D.sortSmallFirst(vertexSeq map (v=> Point2D[Double](v._1,v._2)))
    )
  //---------------------------------------------------------------------------
  def buildWithDoublePoint(vertexSeq: Array[Point2D[Double]]): Polygon[Double] =
    Polygon[Double](Point2D.sortSmallFirst(vertexSeq))
  //---------------------------------------------------------------------------
  def buildWithShort(vertexSeq: Array[(Short, Short)]): Polygon[Short] =
    Polygon[Short](
      Point2D.sortSmallFirst(vertexSeq map (v=> Point2D[Short](v._1,v._2)))
    )
  //---------------------------------------------------------------------------
  def buildWithInt(vertexSeq: Array[(Int, Int)]): Polygon[Int] =
    Polygon[Int](
      Point2D.sortSmallFirst(vertexSeq map (v=> Point2D[Int](v._1,v._2)))
    )
  //---------------------------------------------------------------------------
  private def segmentsIntersect[T: ClassTag: Numeric]
                               (p1: Point2D[T]
                                , p2: Point2D[T]
                                , q1: Point2D[T]
                                , q2: Point2D[T]): Boolean = {

    val orientation1 = orientation(p1, p2, q1)
    val orientation2 = orientation(p1, p2, q2)
    val orientation3 = orientation(q1, q2, p1)
    val orientation4 = orientation(q1, q2, p2)

    // General case
    if (orientation1 != orientation2 && orientation3 != orientation4) true
    else {
      // Special cases
      if (orientation1 == 0 && onSegment(p1, q1, p2)) return true
      if (orientation2 == 0 && onSegment(p1, q2, p2)) return true
      if (orientation3 == 0 && onSegment(q1, p1, q2)) return true
      if (orientation4 == 0 && onSegment(q1, p2, q2)) return true
      false
    }
  }
  //---------------------------------------------------------------------------
  // Helper function to calculate orientation of ordered triplet (p, q, r)
  private def orientation[T: ClassTag: Numeric]
                         (p: Point2D[T]
                          , q: Point2D[T]
                          , r: Point2D[T]): Int = {
    val numeric = implicitly[Numeric[T]]
    import numeric._
    val value = (toDouble(q.y) - toDouble(p.y)) *
                (toDouble(r.x) - toDouble(q.x)) -
                (toDouble(q.x) - toDouble(p.x)) *
                (toDouble(r.y) - toDouble(q.y))
    if (value == 0) 0 // Collinear
    else if (value > 0) 1 // Clockwise
    else 2 // Counterclockwise
  }
  //---------------------------------------------------------------------------
  // Helper function to check if point q lies on line segment pr
  private def onSegment[T: ClassTag: Numeric]
                        (p: Point2D[T]
                         , q: Point2D[T]
                         , r: Point2D[T]): Boolean = {
    val numeric = implicitly[Numeric[T]]

    numeric.lteq(q.x,numeric.max(p.x, r.x)) &&
    numeric.gteq(q.x,numeric.max(p.x, r.x)) &&
    numeric.lteq(q.y,numeric.max(p.y, r.y)) &&
    numeric.gteq(q.y,numeric.max(p.y, r.y))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//assuming last vertex in the same as first one, so it must be ignored
import Polygon._
case class Polygon[T: ClassTag: Numeric]
                    (vertexSeq : Array[Point2D[T]]
                    , id: Long = Polygon.getNewID) {
  //----------------------------------------------------------------------------
  assert(vertexSeq.length > 2, "The number of vertex must be grater than 2")
  //----------------------------------------------------------------------------
  // Numeric operations for type T
  private val numeric = implicitly[Numeric[T]]
  import numeric._
  //----------------------------------------------------------------------------
  // Number of vertices in the polygon
  private val vertexCount: Int = vertexSeq.length
  private val vertexPath = vertexSeq.sliding(2,1).toArray ++
                           Array(Array(vertexSeq.last, vertexSeq.head))
  //----------------------------------------------------------------------------
  val propeties = Properties(InvariantToGeometricTransformation(id
                                                               , internalAngleSeq)
                            , area
                            , perimeter
                            , sideLengthSeq
                            , momentOfInertia
                            , centroid)
  //----------------------------------------------------------------------------
  def getInvariantToGeometricTransformations = propeties.itgt
  //----------------------------------------------------------------------------
  def getArea = propeties.area
  //----------------------------------------------------------------------------
  def getPerimeter = propeties.perimeter

  //----------------------------------------------------------------------------
  def getSideLengthSeq = propeties.sideLengthSeq

  //----------------------------------------------------------------------------
  def getInternalAngleSeq = propeties.itgt

  //----------------------------------------------------------------------------
  def getMomentOfInertia = propeties.momentOfInertia

  //----------------------------------------------------------------------------
  def getCentroid = propeties.centroid
  //----------------------------------------------------------------------------
  //invariant: translations, rotations, and scale
  private def area: Double = {
    var areaSum: Double = 0
    for (pair <-vertexPath)
      areaSum += pair.head.crossProduct(pair.last)
    Math.abs(areaSum) / 2
  }
  //----------------------------------------------------------------------------
  private def perimeter: Double = sideLengthSeq.sum
  //---------------------------------------------------------------------------
  private def sideLengthSeq: Array[Double] =
    for (pair <- vertexPath) yield
      pair.head.getDistance(pair.last)
  //---------------------------------------------------------------------------
  //invariant regarding geometric transformations
  //angles in degrees
  private def internalAngleSeq: Array[Double] = {
    val angleSeq = new Array[Double](vertexCount)
    for (i <- 0 until vertexCount) {
      val j = (i + 1) % vertexCount
      val k = (i + 2) % vertexCount

      val side1 = vertexSeq(i)
      val side2 = vertexSeq(j)
      val side3 = vertexSeq(k)

      val vector1x = side1.x - side2.x
      val vector1y = side1.y - side2.y
      val vector2x = side3.x - side2.x
      val vector2y = side3.y - side2.y

      val dotProduct = toDouble(vector1x) * toDouble(vector2x) +
                        toDouble(vector1y) * toDouble(vector2y)
      val magnitude1 = Math.sqrt(Math.pow(vector1x.toDouble, 2) + Math.pow(vector1y.toDouble, 2))
      val magnitude2 = Math.sqrt(Math.pow(vector2x.toDouble, 2) + Math.pow(vector2y.toDouble, 2))

      angleSeq(j) = Math.toDegrees(Math.acos(dotProduct / (magnitude1 * magnitude2)))
    }
    angleSeq.sorted
  }
  //----------------------------------------------------------------------------
  private def momentOfInertia: Double = {
    var inertiaSum: Double = 0
    val centroidPoint = centroid
    for (pair <- vertexPath) {
      val crossProduct = pair.head.crossProduct(pair.last)
      val term1 = Math.pow(centroidPoint.x - toDouble(pair.last.x), 2)
      val term2 = Math.pow(centroidPoint.y - toDouble(pair.last.y), 2)
      inertiaSum += crossProduct * (term1 + term2)
    }
    inertiaSum / 12
  }
  //----------------------------------------------------------------------------
  private def centroid: Point2D_Double = {
    var cx: Double = 0
    var cy: Double = 0
    var areaSum: Double = 0

    for (pair <- vertexPath) {
      val crossProduct = pair.head.crossProduct(pair.last)
      areaSum += crossProduct
      cx += (toDouble(pair.head.x) + toDouble(pair.last.x)) * crossProduct
      cy += (toDouble(pair.head.y) + toDouble(pair.last.y)) * crossProduct
    }

    val areaFactor = areaSum * 3
    Point2D_Double(cx / areaFactor, cy / areaFactor)
  }
  //----------------------------------------------------------------------------
  def sidesDoNotCross: Boolean = {
    val n = vertexCount
    // Iterate through all pairs of edges (i, j) where i != j
    for (i <- 0 until n) {
      val iNext = (i + 1) % n
      for (j <- i + 1 until n) {
        val jNext = (j + 1) % n
        // Check if edge (i, iNext) intersects with edge (j, jNext)
        if (segmentsIntersect(vertexSeq(i)
                             , vertexSeq(iNext)
                             , vertexSeq(j)
                             , vertexSeq(jNext)))
          return false // Intersection found
      }
    }
    true // No intersections found
  }
  //---------------------------------------------------------------------------
  def sidesCross: Boolean = !sidesDoNotCross
  //---------------------------------------------------------------------------
  def saveAsCsv(filename: String) = {
    val bw = new BufferedWriter(new FileWriter(new File(filename)))
    bw.write("x,y\n")
    Point2D.sortSmallFirst(vertexSeq)
      .foreach{ p=>
      bw.write(toDouble(p.x) +  "," +  toDouble(p.y) + "\n")
    }
    bw.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Polygon.scala
//=============================================================================