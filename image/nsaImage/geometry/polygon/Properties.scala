/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Jun/2024
 * Time:  14h:41m
 * Description: None
 */
package com.common.image.nsaImage.geometry.polygon
//=============================================================================
import com.common.image.nsaImage.geometry.point.Point2D_Double
//=============================================================================
//=============================================================================
case class Properties(itgt: InvariantToGeometricTransformation
                      , area: Double
                      , perimeter: Double
                      , sideLengthSeq: Array[Double]
                      , momentOfInertia: Double
                      , centroid: Point2D_Double)
//=============================================================================
//=============================================================================
case class InvariantToGeometricTransformation(id: Long
                                              , internalAngleSeq: Array[Double] //invariant: translations, rotations, and scale
                                             ) {
  //---------------------------------------------------------------------------
  def getAsArray() = internalAngleSeq
  //---------------------------------------------------------------------------
  def getAsArrayLength() = internalAngleSeq.length
  //---------------------------------------------------------------------------
  def isEqualTo(other: InvariantToGeometricTransformation
                 , minDifftoBeEqual:Double) = {
    if (internalAngleSeq.length != other.internalAngleSeq.length) false
    else internalAngleSeq.zip(other.internalAngleSeq)
         .forall(p=> Math.abs(p._1 - p._2) <= minDifftoBeEqual)
  }

  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Invariant.scala
//=============================================================================