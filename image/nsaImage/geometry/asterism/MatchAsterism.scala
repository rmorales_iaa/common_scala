/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Jun/2024
 * Time:  17h:49m
 * Description: None
 */
package com.common.image.nsaImage.geometry.asterism
//=============================================================================
import com.common.geometry.affineTransformation.AffineTransformation2D
import com.common.geometry.point.Point2D_Double
import com.common.image.nsaImage.geometry.point.Point2D
import com.common.image.nsaImage.geometry.polygon.{InvariantToGeometricTransformation, Polygon}
import net.robowiki.knn.implementations.Rednaxela3rdGenTreeKNNSearch
import net.robowiki.knn.util.KNNEntry
//=============================================================================
import scala.util.Random
//=============================================================================
//=============================================================================
object MatchAsterism {
  //---------------------------------------------------------------------------
  private type ITGT = InvariantToGeometricTransformation
  type KnnTreeType = Rednaxela3rdGenTreeKNNSearch
  //---------------------------------------------------------------------------
  private final val MIN_ASTERISM_POLYGON_ORDER = 3
  //---------------------------------------------------------------------------
  private def getPointKnnTree(pointSeq: Array[Point2D[Double]]) = {
    val knnTree = new KnnTreeType(2) //point 2D
    pointSeq.zipWithIndex.foreach { case (point, id) =>
      knnTree.addDataPoint(new KNNEntry(id.toString, point.toArray()))
    }
    knnTree
  }
  //---------------------------------------------------------------------------
  private def getInvariantKnnTreeAndMap(pointSeq: Array[Point2D[Double]]
                                        , maxAsterismPolygonOrder: Int
                                        , buildTree: Boolean) = {
    val knnTree = new KnnTreeType(maxAsterismPolygonOrder - 1) //number of invariants
    val polygonMap = Polygon.buildWithFixedPointCount(pointSeq, maxAsterismPolygonOrder).map { polygon =>
      if (buildTree) {
        val invariant = polygon.getInvariantToGeometricTransformations.getAsArray()
        knnTree.addDataPoint(new KNNEntry(polygon.id.toString, invariant))
      }
      (polygon.id, polygon)
    }.toMap
    (polygonMap, knnTree)
  }

  //---------------------------------------------------------------------------
  def getTransformation(aft: AffineTransformation2D
                        , p: Point2D[Double]) = {
    val coeffSeq = aft.coeffSeq
    Point2D_Double((coeffSeq(0) * p.x) + (coeffSeq(1) * p.y) + coeffSeq(2)
                 , (coeffSeq(3) * p.x) + (coeffSeq(4) * p.y) + coeffSeq(5))
  }
}
//=============================================================================
//return the affine transformation to transform a point in sample point sequence
// to be converted into a point in catalog point
import MatchAsterism._
case class MatchAsterism(catalogPointSeq: Array[Point2D[Double]]
                         , samplePointSeq: Array[Point2D[Double]]
                         , catalogTestPointSeq: Array[Point2D[Double]]
                         , sampleTestPointSeq: Array[Point2D[Double]]
                         , _maxAsterismPolygonOrder: Int
                         , minAsterismPolygonOrder: Int = MatchAsterism.MIN_ASTERISM_POLYGON_ORDER
                         , AFT_SamplePointTestToApply: Int = 3
                         , AFT_SamplePointTestMinPassPercentage: Double = 70
                         , AFT_SamplePointTestMinAllowedPointDistance: Double = 1.5) {

  private val maxAsterismPolygonOrder = Math.max(_maxAsterismPolygonOrder, minAsterismPolygonOrder)

  //build the trees used for test int the affine transformation test
  private val catalogTreePointTest = getPointKnnTree(catalogTestPointSeq)
  private val random = new Random() //used to test random sample points transformation

  //used in the AFT_SamplePointTest
  private var minPixDistanceFound = Double.MaxValue
  private var aftSamplePointTestPassPercentage = Double.MaxValue
  //--------------------------------------------------------------------------
  def calculate() : (Option[AffineTransformation2D],Double, Double) = {
    //iterate thru all asterism order
    for (asterismPolygonOrder <- maxAsterismPolygonOrder to minAsterismPolygonOrder by -1) {
      val aft = matchAsterism(asterismPolygonOrder+1)
      if (aft.isDefined) return (aft,minPixDistanceFound,aftSamplePointTestPassPercentage) //one aft found that pass the tests
    }

    (None,minPixDistanceFound,aftSamplePointTestPassPercentage)
  }
  //--------------------------------------------------------------------------
  private def matchAsterism(asterismPolygonOrder: Int): Option[AffineTransformation2D] = {

    //build the polygons with the required asterism order for catalog point sequence
    val (invariantCatalogPolyMap, invariantCatalogKnnTree) =
      getInvariantKnnTreeAndMap(catalogPointSeq, asterismPolygonOrder, true)
    if (invariantCatalogPolyMap.isEmpty) return None

    //build the polygons with the required asterism order for sample point sequence
    val (invariantSamplePolyMap, _) =
      getInvariantKnnTreeAndMap(samplePointSeq, asterismPolygonOrder, false)
    if (invariantSamplePolyMap.isEmpty) return None

    //find the closest invariant of sample in catalog using kntree,
    //build the affine transformation and test the geometric transformation
    invariantSamplePolyMap.values.toArray.foreach { samplePoly =>

      val sampleTargetLocation = samplePoly.getInvariantToGeometricTransformations.getAsArray()

      //find the closest invariant of sample in catalog using kntree
      val catalogCandidateLocationSeq =
        invariantCatalogKnnTree.getNearestNeighbors(sampleTargetLocation, 1) //get the nearest invariant in the catalog

      //build the affine transformation and test the geometric transformation
      if (!catalogCandidateLocationSeq.isEmpty) {
        val catalogPoly = invariantCatalogPolyMap(catalogCandidateLocationSeq.head.getValue.toLong)

        //calculate the affine transformation
        val aft = AffineTransformation2D.umeyana2D(
            samplePoly.vertexSeq map (p => p.toArray())
          , catalogPoly.vertexSeq map (p => p.toArray())
        )
        if (aft.isDefined && passAFT_SamplePointTest(aft.get, asterismPolygonOrder)) return aft
      }
    }
    None
  }

  //---------------------------------------------------------------------------
  //Select random points form sample point sequence and check if they pass the
  //affine transformation test
  private def passAFT_SamplePointTest(aft: AffineTransformation2D, asterismPolygonOrder: Int): Boolean = {
    var passPointCount = 0L
    var noPassPointCount = 0L
    val samplePointTotalCount = sampleTestPointSeq.length.toDouble
    val noPassAFT_TestMinPercentage  = 100 - AFT_SamplePointTestMinPassPercentage
    //--------------------------------------------------------------------------
    //Get the percentage of sample points that when is applied the affine transformation,
    // exits a "close" point in the catalog
    //If this percentage is more than the expected 'passAFT_TestMinPercentage' then return true
    //else return false
    //--------------------------------------------------------------------------
    def pointSequencePassAFT_Test(seq: Array[Point2D[Double]]) : Boolean = {
      seq.foreach { point =>
        val samplePointTransformed = getTransformation(aft, point)
        val pointMatch = catalogTreePointTest.getNearestNeighbors(samplePointTransformed.toArray(), 1)
        if (!pointMatch.isEmpty)
          minPixDistanceFound = Math.min(minPixDistanceFound, pointMatch.head.getDistance)

        if (pointMatch.isEmpty || pointMatch.head.getDistance > AFT_SamplePointTestMinAllowedPointDistance) {
          noPassPointCount += 1
          if ((noPassPointCount / samplePointTotalCount * 100) >= noPassAFT_TestMinPercentage)
            return false
        }
        else {
          passPointCount += 1
          aftSamplePointTestPassPercentage = passPointCount / samplePointTotalCount * 100
          if (aftSamplePointTestPassPercentage >= AFT_SamplePointTestMinPassPercentage)
            return true
        }
      }
      false
    }
    //--------------------------------------------------------------------------
    for(_<-0 until  AFT_SamplePointTestToApply)
      if (!pointSequencePassAFT_Test(random.shuffle(sampleTestPointSeq.toSeq).toArray)) return false
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file MatchAsterism.scala
//=============================================================================