/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  01/Jun/2024
 * Time:  13h:08m
 * Description: None
 */
package com.common.image.nsaImage.geometry.point
//=============================================================================
//=============================================================================
object Point2D_Short {
  //---------------------------------------------------------------------------
  final val POINT_ZERO =    new Point2D_Short(0, 0)
  final val POINT_ONE =     new Point2D_Short(1, 1)
  final val POINT_INVALID = new Point2D_Short(-1, -1)
  //---------------------------------------------------------------------------
  def apply(x: Short, y: Short) = new Point2D_Short(x, y)
  //---------------------------------------------------------------------------
}
//=============================================================================
class Point2D_Short(x: Short, y:Short) extends Point2D[Short](x,y)
//=============================================================================
//End of file Point2D_Short.scala
//=============================================================================