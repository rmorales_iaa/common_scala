/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  01/Jun/2024
 * Time:  12h:19m
 * Description: None
 */
package com.common.image.nsaImage.geometry.point
//=============================================================================
import scala.reflect.ClassTag
//=============================================================================
//=============================================================================
object Point2D {
  //---------------------------------------------------------------------------
  def apply[T](p: Point2D[T], x: T, y: T)
              (implicit ct: ClassTag[T]
                       , numeric: Numeric[T]): Point2D[T] =
    Point2D[T](numeric.plus(p.x, x), numeric.plus(p.y, y))
  //---------------------------------------------------------------------------
  def apply[T](p: Point2D[T], v: T)
              (implicit ct: ClassTag[T]
               , numeric: Numeric[T]): Point2D[T] =
    Point2D[T](numeric.plus(p.x, v), numeric.plus(p.y, v))
  //---------------------------------------------------------------------------
  def toSeq[T](seq : Seq[Point2D[T]])
              (implicit ct: ClassTag[T]) =
    (seq flatMap (p=> Seq(p.x,p.y))).toArray
  //---------------------------------------------------------------------------
  def getMin[T](seq: Seq[Point2D[T]])
               (implicit ct: ClassTag[T]
                        , numeric: Numeric[T]): Point2D[T] = {
    var minX = seq(0).x
    var minY = seq(0).y
    seq drop 1 foreach { p =>
      minX = numeric.min(minX, p.x)
      minY = numeric.min(minY, p.y)
    }
    Point2D[T](minX, minY)
  }
  //---------------------------------------------------------------------------
  def getMax[T](seq: Seq[Point2D[T]])
               (implicit ct: ClassTag[T]
                , numeric: Numeric[T]): Point2D[T] = {
    var maxX = seq(0).x
    var maxY = seq(0).y
    seq drop 1 foreach { p =>
      maxX = numeric.max(maxX, p.x)
      maxY = numeric.max(maxY, p.y)
    }
    Point2D[T](maxX, maxY)
  }
  //---------------------------------------------------------------------------
  implicit def pointOrdering[T: Ordering]: Ordering[Point2D[T]] =
    new Ordering[Point2D[T]] {
    def compare(p1: Point2D[T], p2: Point2D[T]): Int = {
      val ordering = implicitly[Ordering[T]]
      val xComparison = ordering.compare(p1.x, p2.x)
      if (xComparison == 0) ordering.compare(p1.y, p2.y) else xComparison
    }
  }
  //---------------------------------------------------------------------------
  def sortSmallFirst[T: ClassTag : Numeric]
                   (pointSeq: Array[Point2D[T]])
      : Array[Point2D[T]] =
    pointSeq.sortWith{_.compare(_) < 0}
  //---------------------------------------------------------------------------
  def sortBiggerFirst[T: ClassTag : Numeric]
                     (pointSeq: Array[Point2D[T]])
  : Array[Point2D[T]] =
    pointSeq.sortWith { _.compare(_) > 0 }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Point2D[T: ClassTag: Numeric](x:T, y: T) {
  //--------------------------------------------------------------------------
  val numeric = implicitly[Numeric[T]]
  import numeric._
  //--------------------------------------------------------------------------
  def equals(other: Point2D[T]) =
    numeric.equals(x,other.x) &&
    numeric.equals(x,other.x)
  //--------------------------------------------------------------------------
  def compare(other: Point2D[T]): Int = {
    val ordering = implicitly[Ordering[T]]
    val xComparison = ordering.compare(x, other.x)
    if (xComparison == 0) ordering.compare(y, other.y) else xComparison
  }
  //---------------------------------------------------------------------------
  def + (p: Point2D[T]) = Point2D[T](x + p.x, y + p.y)
  //--------------------------------------------------------------------------
  def + (v:T) = Point2D[T](numeric.plus(x,v), numeric.plus(y,v))
  //--------------------------------------------------------------------------
  def - (p: Point2D[T]) = Point2D[T](numeric.minus(x,p.x), numeric.minus(y,p.y))
  //--------------------------------------------------------------------------
  def - (v:T) = Point2D[T](numeric.minus(x,v), numeric.minus(y,v))
  //--------------------------------------------------------------------------
  def * (v: T) = Point2D[T](numeric.times(x,v), numeric.times(y,v))
  //--------------------------------------------------------------------------
  def * (p: Point2D[T]) = Point2D[T](numeric.times(x,p.x), numeric.times(y,p.y))
  //--------------------------------------------------------------------------
  def abs() = Point2D[T](numeric.abs(x), numeric.abs(y))
  //--------------------------------------------------------------------------
  def != (p: Point2D[T]) = !(this == p)
  //--------------------------------------------------------------------------
  def < (p: Point2D[T]) = getDistance() < p.getDistance()
  //--------------------------------------------------------------------------
  def <= (p: Point2D[T]) = getDistance() <= p.getDistance()
  //--------------------------------------------------------------------------
  def > (p: Point2D[T]) = getDistance() > p.getDistance()
  //--------------------------------------------------------------------------
  def >= (p: Point2D[T]) = getDistance() >= p.getDistance()
  //--------------------------------------------------------------------------
  def add(v: T = numeric.one) = Point2D[T](x+v, y+v)
  //--------------------------------------------------------------------------
  def addX(v: T = numeric.one) = Point2D[T](x+v, y)
  //--------------------------------------------------------------------------
  def addY(v: T = numeric.one) = Point2D[T](x, y+v)
  //--------------------------------------------------------------------------
   def subAbs(p: Point2D[T]): Point2D[T] =
     Point2D(numeric.abs(x - p.x), numeric.abs(y - p.y))
  //--------------------------------------------------------------------------
  def subX(v: T = numeric.one) = Point2D[T](x-v, y)
  //--------------------------------------------------------------------------
  def subY(v: T = numeric.one) = Point2D[T](x, y-v)
  //--------------------------------------------------------------------------
  def subtractMinZero(p: Point2D[T]): Point2D[T] =
    Point2D(numeric.max(numeric.zero, x - p.x)
          , numeric.max(numeric.zero, y - p.y))
  //--------------------------------------------------------------------------
  def inverse = Point2D[T](-x, -y)
  //--------------------------------------------------------------------------
  def inverseX = Point2D[T](-x, y)
  //--------------------------------------------------------------------------
  def inverseY = Point2D[T](x, -y)
  //--------------------------------------------------------------------------
  def reverse = Point2D[T](y, x)
  //--------------------------------------------------------------------------
  def someNegative: Boolean = (x < numeric.zero) || (y < numeric.zero)
  //--------------------------------------------------------------------------
  def allPositive: Boolean = !someNegative
  //---------------------------------------------------------------------------
  def isClosedAs(p: Point2D[T], pixelDistance: T = numeric.one): Boolean =
    (numeric.abs(x - p.x) <= pixelDistance) && (numeric.abs(y - p.y) <= pixelDistance)
  //---------------------------------------------------------------------------
  def toSequence() = Seq(x,y)
  //---------------------------------------------------------------------------
  def toArray() = Array(x,y)
  //---------------------------------------------------------------------------
  def toArrayDouble() = Array(x,y)
  //---------------------------------------------------------------------------
  def getDistance(): Double =
    Math.sqrt((numeric.toDouble(x) * numeric.toDouble(x)) +
              (numeric.toDouble(y) * numeric.toDouble(y)))
  //---------------------------------------------------------------------------
  def toDouble() = Point2D[Double](numeric.toDouble(x),numeric.toDouble(y))
  //---------------------------------------------------------------------------
  def getDistance(p: Point2D[T]): Double = {
    val a = numeric.toDouble(x) - numeric.toDouble(p.x)
    val b = numeric.toDouble(y) - numeric.toDouble(p.y)
    Math.sqrt((a * a) + (b * b))
  }
  //---------------------------------------------------------------------------
  def crossProduct(p: Point2D[T]): Double =
    numeric.toDouble(x) * numeric.toDouble(p.y) -
    numeric.toDouble(p.x) * numeric.toDouble(y)
  //---------------------------------------------------------------------------
  def getAngle(p: Point2D[T]): Double = Math.atan2(numeric.toDouble(p.y) - numeric.toDouble(y)
                                                 , numeric.toDouble(p.x) - numeric.toDouble(x))
  //---------------------------------------------------------------------------
  def getSlope(p: Point2D[T]): Double = (numeric.toDouble(p.y) - numeric.toDouble(y)) /
                                        (numeric.toDouble(p.x) - numeric.toDouble(x))  //tangent of the angle
  //---------------------------------------------------------------------------
  override def toString = s"($x,$y)"
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
  def getDistanceToLine(p1: Point2D[T], p2: Point2D[T]): Double = {
    val dx = numeric.toDouble(p2.x) - numeric.toDouble(p1.x)
    val dy = numeric.toDouble(p2.y) - numeric.toDouble(p1.y)
    Math.abs(((dy * numeric.toDouble(x)) - (dx * numeric.toDouble(y)) +
              (numeric.toDouble(p2.x) * numeric.toDouble(p1.y)) - (numeric.toDouble(p2.y) * numeric.toDouble(p1.x))) /
              Math.sqrt((dy * dy) + (dx * dx)))
  }
  //---------------------------------------------------------------------------
  //https://github.com/caente/convex-hull/blob/master/src/main/scala/com/miguel/GrahamScanScala.scala
  def goesLeft(p1: Point2D[T], p2: Point2D[T]): Boolean =
    numeric.toDouble((p2.x - p1.x) * (y - p1.y) - (p2.y - p1.y) * (x - p1.x)) > 0
  //---------------------------------------------------------------------------
  def magnitude(): Double =
    Math.sqrt((numeric.toDouble(x) * numeric.toDouble(x)) + (numeric.toDouble(y) * numeric.toDouble(y)))
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Point2D.scala
//=============================================================================