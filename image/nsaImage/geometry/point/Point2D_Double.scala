/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  01/Jun/2024
 * Time:  13h:08m
 * Description: None
 */
package com.common.image.nsaImage.geometry.point
//=============================================================================
//=============================================================================
object Point2D_Double {
  //---------------------------------------------------------------------------
  final val DOUBLE_COMPARISION_MAX_ALLOWED_MARGIN = 10e-10
  //---------------------------------------------------------------------------
  final val POINT_ZERO =    new Point2D_Double(0d, 0d)
  final val POINT_ONE =     new Point2D_Double(1d, 1d)
  final val POINT_INVALID = new Point2D_Double(-1, -1)
  final val POINT_NAN =     new Point2D_Double(Double.NaN, Double.NaN)
  //---------------------------------------------------------------------------
  def apply(x: Double, y:Double) = new Point2D_Double(x,y)
  //---------------------------------------------------------------------------
}
//=============================================================================
import Point2D_Double._
class Point2D_Double(x: Double, y:Double) extends Point2D[Double](x,y) {
  //--------------------------------------------------------------------------
  override def equals(p: Point2D[Double]) =
    (Math.abs(x - p.x) < DOUBLE_COMPARISION_MAX_ALLOWED_MARGIN) &&
    (Math.abs(y - p.y) < DOUBLE_COMPARISION_MAX_ALLOWED_MARGIN)
  //--------------------------------------------------------------------------
  override def compare(other: Point2D[Double]): Int = {
    val xComparison = {
      val abs = Math.abs(x - other.x)
      if (abs <= DOUBLE_COMPARISION_MAX_ALLOWED_MARGIN) 0
      else x.compare(other.x)
    }
    if (xComparison == 0) y.compare(other.y) else xComparison
  }
  //--------------------------------------------------------------------------
}
//=============================================================================
//End of file Point2D_Double.scala
//=============================================================================