/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  25/May/2024
 * Time:  01h:00m
 * Description: None
 */
package com.common.image.nsaImage
//=============================================================================
import com.common.dataType.array._
import com.common.fits.standard.ItemSize.BLOCK_BYTE_SIZE
import com.common.geometry.point.Point2D
import com.common.hardware.cpu.CPU
import com.common.stat.SimpleStatDescriptive
import com.common.util.parallelTask.ParallelTask
//=============================================================================
import scala.reflect.ClassTag
import scala.collection.JavaConverters.collectionAsScalaIterableConverter
import java.util.concurrent.ConcurrentLinkedQueue
//=============================================================================
//=============================================================================
object NSA_SubImage {
  //---------------------------------------------------------------------------
  //pretty print
  private final val PRETTY_PRINT_VALUE_PAD_SIZE = 5
  private final val PRETTY_PRINT_VALUE_CELL_SEPARATOR = "|"
  private final val PRETTY_PRINT_ROW_HEADER = "->|"
  private final val PRETTY_PRINT_ROW_VALUE_PAD_SIZE = 4
  private final val PRETTY_PRINT_COL_VALUE_PAD_SIZE = 4
  //---------------------------------------------------------------------------
  def apply[T: ClassTag : Numeric]
            (data: Array[T]
             , minPos: Point2D
             , maxPos: Point2D): NSA_SubImage[_] = {

    data.head match {
      case _: Byte =>
        NSA_SubImage[NSA_Byte.ITEM_DATA_TYPE](NSA_Byte(data.asInstanceOf[Array[Byte]], BLOCK_BYTE_SIZE), minPos, maxPos)

      case _: Short =>
        NSA_SubImage[NSA_Short.ITEM_DATA_TYPE](NSA_Short(data.asInstanceOf[Array[Short]], BLOCK_BYTE_SIZE), minPos, maxPos)

      case _: Int =>
        NSA_SubImage[NSA_Int.ITEM_DATA_TYPE](NSA_Int(data.asInstanceOf[Array[Int]], BLOCK_BYTE_SIZE), minPos, maxPos)

      case _: Long =>
        NSA_SubImage[NSA_Long.ITEM_DATA_TYPE](NSA_Long(data.asInstanceOf[Array[Long]], BLOCK_BYTE_SIZE), minPos, maxPos)

      case _ : Float =>
        NSA_SubImage[NSA_Float.ITEM_DATA_TYPE](NSA_Float(data.asInstanceOf[Array[Float]], BLOCK_BYTE_SIZE), minPos, maxPos)

      case _: Double =>
        NSA_SubImage[NSA_Double.ITEM_DATA_TYPE](NSA_Double(data.asInstanceOf[Array[Double]], BLOCK_BYTE_SIZE), minPos, maxPos)
    }
  }
  //---------------------------------------------------------------------------
  def parallelProcessing[SUB_T, QT: ClassTag]
  (subImageSeq: Array[NSA_SubImage[SUB_T]]
   , transf: (NSA_SubImage[SUB_T]) => QT
   , message: Option[String] = None
   , verbose: Boolean = false)
  : Array[QT] = {
    //---------------------------------------------------------------------------
    val queue = new ConcurrentLinkedQueue[QT]()
    new ProcessParallelDataBlock(subImageSeq)
    //---------------------------------------------------------------------------
    class ProcessParallelDataBlock
    (dataBlockSeq: Array[NSA_SubImage[SUB_T]])
      extends ParallelTask[NSA_SubImage[SUB_T]](
        dataBlockSeq
        , CPU.getCoreCount()
        , isItemProcessingThreadSafe = true
        , randomStartMaxMsWait = 100
        , verbose = verbose
        , message = message) {
      //-------------------------------------------------------------------------
      def userProcessSingleItem(dataBLock: NSA_SubImage[SUB_T]): Unit =
        queue.add(transf(dataBLock))
      //-------------------------------------------------------------------------
    }
    queue.asScala.toArray
    //---------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import NSA_SubImage._
case class NSA_SubImage[ITEM_DATA_TYPE: ClassTag: Numeric]
                         (dataBlock: NonSequentialArray[ITEM_DATA_TYPE]
                          , minPos: Point2D
                          , maxPos: Point2D) {
  //--------------------------------------------------------------------------
  private val xMaxIndex = getX_PixCount - 1
  private val yMaxIndex = getY_PixCount - 1
  //--------------------------------------------------------------------------
  private val minPosIndexAbso = offset
  private val maxPosIndexAbso = Point2D(xMaxIndex, yMaxIndex) + offset
  //--------------------------------------------------------------------------
  private var estDetectionLevel: Double = -1
  //--------------------------------------------------------------------------
  def getEstDetectionLevel = estDetectionLevel
  //--------------------------------------------------------------------------
  def setEstDetectionLevel(v: Double) = estDetectionLevel = v
  //--------------------------------------------------------------------------
  def getID = minPos.toString
  //--------------------------------------------------------------------------
  def getHash: Int = {
    val dataBlockHash = dataBlock.hashCode()
    val minPosHash = minPos.hashCode()
    val maxPosHash = maxPos.hashCode()
    31 * (31 * dataBlockHash + minPosHash) + maxPosHash
  }
  //--------------------------------------------------------------------------
  def offset = minPos
  //--------------------------------------------------------------------------
  def getX_PixCount = maxPos.x - minPos.x + 1
  //--------------------------------------------------------------------------
  def getY_PixCount = maxPos.y - minPos.y + 1
  //--------------------------------------------------------------------------
  def contains(p: Point2D) =
    (p.x >= minPos.x && p.x <= maxPos.x) &&
    (p.y >= minPos.y && p.y <= maxPos.y)

  //---------------------------------------------------------------------------
  def isIn(x: Int, y: Int): Boolean = (x >= 0 && x <= xMaxIndex) && (y >= 0 && y <= yMaxIndex)
  //---------------------------------------------------------------------------
  def isIn(p: Point2D): Boolean = isIn(p.x, p.y)
  //---------------------------------------------------------------------------
  def isInAbs(p: Point2D) = (p.x >= (0 + offset.x) && p.x <= (xMaxIndex + offset.x)) &&
    (p.y >= (0 + offset.y) && p.y <= (yMaxIndex + offset.y))
  //---------------------------------------------------------------------------
  def isInBorder(x: Int, y: Int) = (x == 0) || (y == 0) || (x == xMaxIndex) || (y == yMaxIndex)
  //---------------------------------------------------------------------------
  def isAtBorderAbso(x: Int, y: Int) =
    (x == minPosIndexAbso.x) || (y == minPosIndexAbso.y) ||
    (x == maxPosIndexAbso.x) || (y == maxPosIndexAbso.y)
  //--------------------------------------------------------------------------
  def estBackgroundAndRms() = {
    val stats = getStats(isPoblation = false)
    val backGround =
      if (((stats.average - stats.median) / stats.stdDev) > 0.3) stats.median
      else (2.5 * stats.median) - (1.5 * stats.average)
    (backGround, stats.rms)
  }
  //--w------------------------------------------------------------------------
  def getStats(isPoblation: Boolean = false) : SimpleStatDescriptive = {
    val data = dataBlock.collectAsDouble()

    val n:Long = data.size
    val average = data.sum / n.toDouble
    val variance = data.map(v => Math.pow(average - v, 2)).sum / (if (isPoblation) n else if (n == 1) 1 else n - 1)
    val stdDev = Math.sqrt(variance)

    val sortedSeq = data.sortWith(_ < _)
    val median = if (data.size % 2 == 1) sortedSeq(sortedSeq.size / 2)
    else {
      val posRight = data.size / 2
      val posLeft = posRight - 1
      (sortedSeq(posRight) + sortedSeq(posLeft)) / 2.0d
    }
    val (lower, upper) = data.sorted.splitAt(n.toInt / 2)
    if (data.size % 2 == 0) (lower.last + upper.head) / 2 else upper.head

    // Calcular el RMS
    val rms = Math.sqrt(data.map(v => (v - median) *  (v - median) ).sum / n.toDouble)

    SimpleStatDescriptive(
      n
      , average
      , variance
      , stdDev
      , median
      , sortedSeq.head
      , sortedSeq.last
      , rms
    )
  }
  //---------------------------------------------------------------------------
  def prettyPrint(reverse: Boolean = true,
                   o: Point2D = offset,
                   convertToInt: Boolean = true
                 ): Unit = {
    //-------------------------------------------------------------------------
    val data = dataBlock.dataBlockSeq.flatMap(_.itemSeq)
    val xMax = maxPos.x - minPos.x + 1
    val yMax = maxPos.y - minPos.y + 1

    //---------------------------------------------------------------------------
    def prettyPrintRow(row: IndexedSeq[ITEM_DATA_TYPE],
                       pixValuePadSize: Int,
                       pixValueSeparator: String,
                       convertToInt: Boolean): Unit = {
      row foreach { v =>
        val vAsString = if (convertToInt) Math.round(v.asInstanceOf[Number].doubleValue()).toString else v.toString
        val padSize = pixValuePadSize - vAsString.length
        if (padSize > 0) print(" " * padSize)
        print(vAsString + pixValueSeparator)
      }
    }
    //-------------------------------------------------------------------------
    def printColIndicator(xOffset: Int = 0): Unit = {
      print(" " * (PRETTY_PRINT_ROW_HEADER.length + PRETTY_PRINT_ROW_VALUE_PAD_SIZE - 1))
      print("|")
      for (i <- 0 until xMax) {
        val colIndex = s"%0${PRETTY_PRINT_COL_VALUE_PAD_SIZE}d".format(i + xOffset)
        val padSize = (PRETTY_PRINT_VALUE_PAD_SIZE - colIndex.length) / 2
        print(" " * padSize)
        print(colIndex)
        print(" " * (padSize + 1))
        print("|")
      }
    }
    //-------------------------------------------------------------------------
    // pix values with pad + separator +  row column indicator
    val maxRowCharSize = PRETTY_PRINT_ROW_HEADER.length + PRETTY_PRINT_ROW_VALUE_PAD_SIZE + (xMax * (PRETTY_PRINT_VALUE_PAD_SIZE + 1))

    //start line
    println(s"----------- NSA_SubImage: '${this.hashCode()}' with offset: $o starts -----------")
    if (o.y > 0) println("yAbs")
    print("_" * maxRowCharSize)
    if (o.y > 0) print("  yRel")
    println

    val seq = if (reverse) data.grouped(xMax).toList.reverseIterator.zipWithIndex else data.grouped(xMax).toList.zipWithIndex
    seq foreach { case (row, index) =>

      //print row star
      val rowIndex = s"%0${PRETTY_PRINT_ROW_VALUE_PAD_SIZE}d".format(yMax + o.y - index - 1)
      print(rowIndex + "->|")

      //print the row
      prettyPrintRow(row
        , PRETTY_PRINT_VALUE_PAD_SIZE
        , PRETTY_PRINT_VALUE_CELL_SEPARATOR
        , convertToInt)

      if (o.y > 0) print("  " + s"%0${PRETTY_PRINT_ROW_VALUE_PAD_SIZE}d".format(yMax - index - 1))

      //print row separator
      println
    }

    //end of row
    for (_ <- 0 until maxRowCharSize)
      print(0x203e.toChar)
    println

    //offset column indicator
    if (o.x > 0) {
      printColIndicator()
      print("xRel")
      println
    }

    //column indicator
    printColIndicator(o.x)
    if (o.x > 0) print("xAbs")
    println
    println
    println(s"----------- NSA_SubImage: '${this.hashCode()}' with offset: $o ends -------------")
  }
 //--------------------------------------------------------------------------
}
//=============================================================================
//End of file NSA_SubImage.scala
//=============================================================================