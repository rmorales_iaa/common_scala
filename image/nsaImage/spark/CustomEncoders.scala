/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/May/2024
 * Time:  10h:50m
 * Description: None
 */
package com.common.image.nsaImage.spark
//=============================================================================
import com.common.dataType.array._
import com.common.image.nsaImage.NSA_SubImage
import org.apache.spark.sql.{Encoder, Encoders}
//=============================================================================
import scala.reflect.ClassTag
//=============================================================================
//=============================================================================
object CustomEncoders {
  //---------------------------------------------------------------------------
  //DataBlock data types
  implicit def nsaByteEncoder: Encoder[NSA_Byte] = Encoders.kryo[NSA_Byte]
  //---------------------------------------------------------------------------
  implicit def nsaShortEncoder: Encoder[NSA_Short] = Encoders.kryo[NSA_Short]
  //---------------------------------------------------------------------------
  //implicit def nsaIntEncoder: Encoder[NSA_Int] = Encoders.kryo[NSA_Int]
  //---------------------------------------------------------------------------
  implicit val nsaSubImageIntEncoder: Encoder[NSA_Int] = {
    //-------------------------------------------------------------------------
    implicit val nsaEncoder              : Encoder[NonSequentialArray[NSA_Int]]  = Encoders.kryo[NonSequentialArray[NSA_Int]]
    //-------------------------------------------------------------------------
    implicit val nsaDataBlockEncoder     : Encoder[DataBlock[NSA_Int]]           = Encoders.kryo[DataBlock[NSA_Int]]
    implicit val nsaArrayDataBlockEncoder: Encoder[Array[DataBlock[NSA_Int]]]    = Encoders.kryo[Array[DataBlock[NSA_Int]]]
    //-------------------------------------------------------------------------
    implicit val nsaSubImageEncoder      : Encoder[NSA_SubImage[NSA_Int]]        = Encoders.kryo[NSA_SubImage[NSA_Int]]
    implicit val nsaSubImageArrayEncoder : Encoder[Array[NSA_SubImage[NSA_Int]]] = Encoders.kryo[Array[NSA_SubImage[NSA_Int]]]
    //-------------------------------------------------------------------------
    Encoders.kryo[NSA_Int]
  }
  //---------------------------------------------------------------------------
  implicit def nsaLongEncoder: Encoder[NSA_Long] = Encoders.kryo[NSA_Long]
  //---------------------------------------------------------------------------
  implicit def nsaFloatEncoder: Encoder[NSA_Float] = Encoders.kryo[NSA_Float]
  //---------------------------------------------------------------------------
  implicit def nsaDoubleEncoder: Encoder[NSA_Double] = Encoders.kryo[NSA_Double]
  //---------------------------------------------------------------------------
  //SubImage data types
  implicit def nsaSubImageByteEncoder: Encoder[NSA_SubImage[NSA_Byte]] = Encoders.kryo[NSA_SubImage[NSA_Byte]]

  //---------------------------------------------------------------------------
  implicit def nsaSubImageShortEncoder: Encoder[NSA_SubImage[NSA_Short]] = Encoders.kryo[NSA_SubImage[NSA_Short]]

  //---------------------------------------------------------------------------
  implicit def nsaSubImageLongEncoder: Encoder[NSA_SubImage[NSA_Long]] = Encoders.kryo[NSA_SubImage[NSA_Long]]

  //---------------------------------------------------------------------------
  implicit def nsaSubImageFloatEncoder: Encoder[NSA_SubImage[NSA_Float]] = Encoders.kryo[NSA_SubImage[NSA_Float]]

  //---------------------------------------------------------------------------
  implicit def nsaSubImageDoubleEncoder: Encoder[NSA_SubImage[NSA_Double]] = Encoders.kryo[NSA_SubImage[NSA_Double]]

  //---------------------------------------------------------------------------
  implicit def nonSequentialArrayEncoder[ITEM_DATA_TYPE: ClassTag : Numeric](implicit e: Encoder[ITEM_DATA_TYPE]): Encoder[ITEM_DATA_TYPE] = Encoders.kryo[ITEM_DATA_TYPE]
  //---------------------------------------------------------------------------
  implicit def nsaSubImageEncoder[ITEM_DATA_TYPE: ClassTag : Numeric]: Encoder[NSA_SubImage[ITEM_DATA_TYPE]] = Encoders.kryo[NSA_SubImage[ITEM_DATA_TYPE]]
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CustomEncoders.scala
//=============================================================================