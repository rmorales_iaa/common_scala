/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/May/2024
 * Time:  18h:04m
 * Description: None
 */
package com.common.image.nsaImage.spark
//=============================================================================
import com.common.image.nsaImage.spark.ProcessNSA_ImageDataSet.getDataset
import com.common.image.nsaImage.NSA_SubImage
import com.common.logger.MyLogger
import com.common.spark.MySpark
import org.apache.spark.sql.Dataset
import org.apache.spark.storage.StorageLevel
//=============================================================================
import scala.reflect.ClassTag
//=============================================================================
//=============================================================================
object NSA_ImageCalculateSourceDetectionLevel extends MyLogger with Serializable {
  //---------------------------------------------------------------------------
  private def processSubImage[ITEM_DATA_TYPE: ClassTag : Numeric]
                              (subImage: NSA_SubImage[ITEM_DATA_TYPE]): (Double, Double) = {
    subImage.estBackgroundAndRms()
  }
  //---------------------------------------------------------------------------

  def run(spark: MySpark
          , filePath: String
          , subWidth: Int
          , subHeight: Int)
          : Array[(Double, Double)] = {

    getDataset(spark, filePath, subWidth, subHeight) match {
      case Some((nsaImage, ds)) =>
        type ITEM_DATA_TYPE = nsaImage.classTag.type
        implicit val numericImpl: Numeric[ITEM_DATA_TYPE] =
          nsaImage.dataBlock.numericImpl.asInstanceOf[Numeric[ITEM_DATA_TYPE]]

        import spark.sparkSession.implicits._
        import CustomEncoders._

        // Process the dataset
        ds.asInstanceOf[Dataset[NSA_SubImage[ITEM_DATA_TYPE]]]
          .persist(StorageLevel.MEMORY_AND_DISK)
          .mapPartitions { partition =>
            partition.map { subImage => processSubImage(subImage) }
          }.collect()

      case None =>
        warning(s"Failed to load dataset from file: '$filePath'")
        Array.empty[(Double, Double)]
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file NSA_ImageCalculateSourceDetectionLevel.scala
//=============================================================================