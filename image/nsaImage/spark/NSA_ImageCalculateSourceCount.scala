/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/May/2024
 * Time:  18h:04m
 * Description: None
 */
package com.common.image.nsaImage.spark
//=============================================================================
import com.common.geometry.point.Point2D
import com.common.image.nsaImage.spark.ProcessNSA_ImageDataSet.getDataset
import com.common.image.nsaImage.NSA_SubImage
import com.common.image.nsaImage.source.{Source, SubImageSourceDetection}
import com.common.logger.MyLogger
import com.common.spark.MySpark
import com.common.util.file.MyFile
import com.sparkFits.sparkProcess.ProcessFITS.addResultToDatabase
//=============================================================================
import scala.reflect.ClassTag
import scala.collection.mutable.ArrayBuffer
import org.apache.spark.sql.Dataset
import org.apache.spark.storage.StorageLevel
//=============================================================================
//=============================================================================
object NSA_ImageCalculateSourceCount extends MyLogger with Serializable {
  //---------------------------------------------------------------------------
  private def processSubImage[ITEM_DATA_TYPE: ClassTag : Numeric]
  (fileName:String
   , subImage: NSA_SubImage[ITEM_DATA_TYPE]
   , sourceDetectionLevel: Double
  ): Long = {
    val startMs = System.currentTimeMillis

    subImage.setEstDetectionLevel(sourceDetectionLevel)
    val compiledSourceSeq = ArrayBuffer[Source[ITEM_DATA_TYPE]]()
    val sourceSeq = SubImageSourceDetection.findSourceSeq[ITEM_DATA_TYPE](
        subImage
      , compiledSourceSeq
    )
    if (sourceSeq.length > 0) {
      val serializedSourceSeq = sourceSeq map { source =>
        source.serialization() +
          s"&${subImage.getHash}" +
          s"&${source.getExtraInfo}"
      }
      addResultToDatabase(
        fileName
        , subImage.getHash.toString
        , serializedSourceSeq
        , startMs)
    }
    sourceSeq.length
  }
  //---------------------------------------------------------------------------
  def processSubImageSeq[ITEM_DATA_TYPE: ClassTag : Numeric]
                        (spark: MySpark
                         , ds: Dataset[NSA_SubImage[ITEM_DATA_TYPE]]
                         , imageName: String
                         , sourceDetectionLevel: Double): Long = {

    import spark.sparkSession.implicits._

    ds
      .persist(StorageLevel.MEMORY_AND_DISK)
      .mapPartitions { partition =>
        partition.map { subImage =>
          processSubImage(imageName, subImage, sourceDetectionLevel)
        }
      }.collect()
      .sum
  }
  //---------------------------------------------------------------------------
  def run(spark: MySpark
          , filePath: String
          , subWidth: Int
          , subHeight: Int
          , sourceDetectionLevel: Double): (Long,Point2D) = {

    val pathSeq = filePath.split("/")
    val imageName = MyFile.removeFileExtension(pathSeq.last)

    getDataset(spark, filePath, subWidth, subHeight) match {
      case None =>
        warning(s"Failed to load dataset from file: '$imageName'")
        (0L, Point2D.POINT_ZERO)

      case Some((nsaImage, ds)) =>

        //type ITEM_DATA_TYPE = nsaImage.classTag.type
        // "It does not work in scala 2.12 in this case. It reports an exception 'java.lang.ArrayStoreException' in 'buildSegmentSeq' mapping the values of the array"

        nsaImage.classTag match {

          //-------------------------------------------------------------------
          case ct if ct == ClassTag.Byte =>
            (processSubImageSeq(spark
                                , ds.asInstanceOf[Dataset[NSA_SubImage[Byte]]]
                                , imageName
                                , sourceDetectionLevel)
              , nsaImage.getDimensions)
          //-------------------------------------------------------------------
          case ct if ct == ClassTag.Short =>
            (processSubImageSeq(spark
                                , ds.asInstanceOf[Dataset[NSA_SubImage[Short]]]
                                , imageName
                                , sourceDetectionLevel)
              , nsaImage.getDimensions)

          //-------------------------------------------------------------------
          case ct if ct == ClassTag.Int =>
            (processSubImageSeq(spark
                                , ds.asInstanceOf[Dataset[NSA_SubImage[Int]]]
                                , imageName
                                , sourceDetectionLevel)
              , nsaImage.getDimensions)

          //-------------------------------------------------------------------
          case ct if ct == ClassTag.Long =>
            (processSubImageSeq(spark
                                , ds.asInstanceOf[Dataset[NSA_SubImage[Long]]]
                                , imageName
                                , sourceDetectionLevel)
              , nsaImage.getDimensions)

          //-------------------------------------------------------------------
          case ct if ct == ClassTag.Float =>
            (processSubImageSeq(spark
                                , ds.asInstanceOf[Dataset[NSA_SubImage[Float]]]
                                , imageName
                                , sourceDetectionLevel)
              , nsaImage.getDimensions)

          //-------------------------------------------------------------------
          case ct if ct == ClassTag.Double =>
            (processSubImageSeq(spark
                                , ds.asInstanceOf[Dataset[NSA_SubImage[Double]]]
                                , imageName
                                , sourceDetectionLevel)
              , nsaImage.getDimensions)
        }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file NSA_ImageCalculateSourceDetectionLevel.scala
//=============================================================================