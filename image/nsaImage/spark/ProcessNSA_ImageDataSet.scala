/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  25/May/2024
 * Time:  23h:49m
 * Description: None
 */
package com.common.image.nsaImage.spark
//=============================================================================
import com.common.configuration.MyConf
import com.common.image.nsaImage.{NSA_Image, NSA_SubImage}
import com.common.logger.MyLogger
import com.common.spark.MySpark
import com.sparkFits.sparkProcess.ProcessFITS.loadHDFS_Fits
//=============================================================================
import org.apache.spark.sql.Dataset
import scala.reflect.ClassTag
//=============================================================================
//=============================================================================
object ProcessNSA_ImageDataSet extends Serializable with MyLogger {
  //---------------------------------------------------------------------------
  def getSubImageDatasetRepartitioned[ITEM_DATA_TYPE: ClassTag: Numeric]
                                     (ds: Dataset[NSA_SubImage[ITEM_DATA_TYPE]]):
                                     Dataset[NSA_SubImage[ITEM_DATA_TYPE]] = {

    val sparkConf = MyConf(MyConf.c.getString("Spark.conf"))
    val partitionCount = sparkConf.getIntWithDefaultValue("Spark.computationCluster.remote.partitionCount", -1)

    val repartitionedDS =
      if (partitionCount == -1) {
        info("SPARK will decide the number of partitions")
        ds
      }
      else {
        info(s"Requested user repartition: '$partitionCount'")
        ds.repartition(partitionCount)
      }
    repartitionedDS
  }

  //---------------------------------------------------------------------------
  def getDataset(spark: MySpark
                 , filePath: String
                 , subWidth: Int
                 , subHeight: Int): Option[(NSA_Image[_],Dataset[NSA_SubImage[_]])] = {

    info(s"Loading the file: '$filePath'")
    val fits = loadHDFS_Fits(spark.myHadoop.get.fs, filePath, readDataBlockFlag = true)
      .getOrElse(return None)

    import spark.sparkSession.implicits._
    import CustomEncoders._

    val nsaImage: NSA_Image[_] = NSA_Image.load(fits).getOrElse(return None)
    val subImageSeq = nsaImage.getSubImagePartition(subWidth, subHeight)

    //'subImageSeq' and 'nsaFits' share the same instantiation type
    //so get the type of instantiation from 'nsaFits'
    type ITEM_DATA_TYPE = nsaImage.classTag.type
    implicit val numericImpl: Numeric[ITEM_DATA_TYPE] =
      nsaImage.dataBlock.numericImpl.asInstanceOf[Numeric[ITEM_DATA_TYPE]]

    val ds = getSubImageDatasetRepartitioned(subImageSeq.asInstanceOf[Array[NSA_SubImage[ITEM_DATA_TYPE]]].toSeq.toDS())
    warning(s"Processing dataset with partition count: '${ds.rdd.getNumPartitions}'")
    Some((nsaImage, ds.asInstanceOf[Dataset[NSA_SubImage[_]]]))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ProcessNSA_ImageDataSet.scala
//=============================================================================