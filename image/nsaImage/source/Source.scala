//=============================================================================
package com.common.image.nsaImage.source
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import java.util.concurrent.atomic.AtomicInteger
import scala.collection.mutable.ArrayBuffer
import scala.reflect.ClassTag
//=============================================================================
import com.common.configuration.MyConf
import com.common.image.nsaImage.source.Interval.IntervalDataType
import com.common.geometry.point.{Point2D, Point2D_Double}
import com.common.geometry.rectangle.Rectangle
import com.common.image.nsaImage.NSA_SubImage
import com.common.logger.MyLogger
//=============================================================================
//=============================================================================
object Source extends MyLogger {
  //---------------------------------------------------------------------------
  val MIN_SOURCE_FOR_ONE_STEP_MERGING = MyConf.c.getInt("SourceDetection.minSourceForOneStepMerging")
  //---------------------------------------------------------------------------
  //serialization
  private val SERIALIZATION_MULTIPLE_SEGMENT_1D_DIVIDER = "!"
  private val SERIALIZATION_MULTIPLE_SEGMENT_2D_DIVIDER = "%"
  private val SERIALIZATION_ROW_DIVIDER = "#"
  //---------------------------------------------------------------------------
  //pretty print
  private final val PRETTY_PRINT_VALUE_PAD_SIZE = 5
  private final val PRETTY_PRINT_VALUE_CELL_SEPARATOR = "|"
  private final val PRETTY_PRINT_ROW_HEADER = "->|"
  private final val PRETTY_PRINT_ROW_VALUE_PAD_SIZE = 4
  private final val PRETTY_PRINT_COL_VALUE_PAD_SIZE = 4
  //---------------------------------------------------------------------------
  final val EIGHT_CONNECTIVITY_OFFSET_SEQ  = List(
    Point2D(0, 1)   //Top (N)
    , Point2D(0, -1)  //Bottom (S)
    , Point2D(-1, 0)  //Left (W)
    , Point2D(1, 0)   //Right (E)
    , Point2D(-1, 1)  //Top-left (NW)
    , Point2D(1, 1)   //Top-right (NE)
    , Point2D(-1, -1) //Bottom-left (SW)
    , Point2D(1, -1)  //Bottom-right (SE)
  )
  //---------------------------------------------------------------------------
  private val idGenerator = new AtomicInteger(-1)
  //---------------------------------------------------------------------------
  def getNewID: Int = idGenerator.addAndGet(1)
  //---------------------------------------------------------------------------
  def initalizeID(v: Int) = idGenerator.set(v)
  //---------------------------------------------------------------------------
  def deserialization(encodedDataType: String, s: String): Source[_] = {
    val split = s.split(SERIALIZATION_MULTIPLE_SEGMENT_2D_DIVIDER)
    val rowSeq = split(0).split(SERIALIZATION_ROW_DIVIDER) map (
      _.split(SERIALIZATION_MULTIPLE_SEGMENT_1D_DIVIDER) map { s =>
        Segment.deserialization(encodedDataType, s)
      })
    val offsetSeq = split(1).split(",")
    val offset = Point2D(offsetSeq(0).toInt, offsetSeq(1).toInt)
    val id = split(2).toInt

    encodedDataType match {
      case "B" => Source[Byte](rowSeq.asInstanceOf[Array[Array[Segment[Byte]]]], offset, id)
      case "S" => Source[Short](rowSeq.asInstanceOf[Array[Array[Segment[Short]]]], offset, id)
      case "I" => Source[Int](rowSeq.asInstanceOf[Array[Array[Segment[Int]]]], offset, id)
      case "L" => Source[Long](rowSeq.asInstanceOf[Array[Array[Segment[Long]]]], offset, id)
      case "F" => Source[Float](rowSeq.asInstanceOf[Array[Array[Segment[Float]]]], offset, id)
      case "D" => Source[Double](rowSeq.asInstanceOf[Array[Array[Segment[Double]]]], offset, id)
    }
  }
  //---------------------------------------------------------------------------
  def saveAsCsv[T](tsvFilename: String
                   , seq: Array[Source[T]]
                   , sep: String = "\t") = {
    val header = Seq("id", "centroid_x", "centroid_y", "flux", "offset", "pixCount", "extraInfo")
    val bw = new BufferedWriter(new FileWriter(new File(tsvFilename), false)) //open for append
    bw.write(header.mkString(sep) + "\n")
    seq.foreach { source =>
      bw.write(s"${source.id}$sep${source.getCentroid().x}$sep${source.getCentroid().y}$sep${source.getFlux()}$sep${source.offset}$sep${source.getPixCount()}$sep${source.extraInfo}\n")
    }
    bw.close
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import Source._
case class Source[T :ClassTag :Numeric]
(var rowSeq: Array[Array[Segment[T]]]
 , var offset : Point2D = Point2D.POINT_ZERO
 , var id: Int = Source.getNewID) {
  //---------------------------------------------------------------------------
  val numeric: Numeric[T] = implicitly[Numeric[T]]
  type RowSeq = Array[Array[Segment[T]]]
  //---------------------------------------------------------------------------
  //calculated values

  //centroid
  private var centroid  = Point2D_Double.POINT_NAN //origin at (1,1)

  //flux
  private var flux: Double = -1

  //additional info
  private var extraInfo: Int = -1

  //axis
  private var xAxis: Segment[T] = null
  private var yAxis: Segment[T] = null
  private var xAxisAbso: Segment[T] = null
  private var yAxisAbso: Segment[T] = null

  //surround rectangle
  private var surroundRectangleAbso : Rectangle = null
  //---------------------------------------------------------------------------
  override def toString() = s"{$id} o:$offset s:$getPixCount"
  //---------------------------------------------------------------------------
  def getPixCount() = rowSeq.flatten.map(_.getNonZeroValueCount).sum
  //---------------------------------------------------------------------------
  def getFlux(force: Boolean = false): Double = {
    if (flux == -1 || force) flux = rowSeq.flatten.map(_.getFlux).sum
    flux
  }
  //---------------------------------------------------------------------------
  def apply(i: Int) = rowSeq(i)
  //---------------------------------------------------------------------------
  def getY_Axis(): Segment[T] = {
    if (yAxis == null)
      yAxis = Segment[T](0, rowSeq.length - 1, Array())
    yAxis
  }

  //---------------------------------------------------------------------------
  def getX_Axis(): Segment[T] = {
    if (xAxis == null) {
      val r = (for (row <- rowSeq) yield
        for (seg <- row) yield
          (seg.s, seg.e)).flatten
      val (sSeq, eSeq) = r.unzip
      xAxis = Segment[T](sSeq.min, eSeq.max, Array())
    }
    xAxis
  }
  //---------------------------------------------------------------------------
  def getY_AxisAbso() = {
    if (yAxisAbso == null) {
      val seg = getY_Axis
      yAxisAbso = Segment[T](seg.s + offset.y, seg.e + offset.y, seg.d)
    }
    yAxisAbso
  }
  //---------------------------------------------------------------------------
  def getX_AxisAbso()= {
    if (xAxisAbso == null) {
      val seg = getX_Axis
      xAxisAbso = Segment[T](seg.s + offset.x, seg.e + offset.x, seg.d)
    }
    xAxisAbso
  }
  //---------------------------------------------------------------------------
  def hasExtraInfo = extraInfo != -1
  //---------------------------------------------------------------------------
  def getExtraInfo = extraInfo
  //---------------------------------------------------------------------------
  def setExtraInfo(o: Int) = extraInfo = o
  //---------------------------------------------------------------------------
  def getPosSeq(): Array[(IntervalDataType, IntervalDataType)] =
    getPosSeq(false) map (t => (t._1, t._2))
  //---------------------------------------------------------------------------
  def getPosSeqWithValues() = getPosSeq(false)
  //---------------------------------------------------------------------------
  def getAbsolutePosSeq() = getPosSeq(true) map (t => (t._1, t._2))
  //---------------------------------------------------------------------------
  private def getPosSeq(absolutePos: Boolean)
  : Array[(IntervalDataType, IntervalDataType, T)] = {
    val o = if (absolutePos) offset else Point2D.POINT_ZERO
    rowSeq.zipWithIndex.flatMap { case (row, yPos) => row.map { s =>
      s.getPosSeqWithValues().map { case(xPos,data) => (o.x + xPos
        , o.y + yPos
        , data) }
    }}.flatten
  }
  //---------------------------------------------------------------------------
  def getAbsolutePosSeqWithValues() = getPosSeq(true)

  //---------------------------------------------------------------------------
  def getCentroid(): Point2D_Double = centroid
  //---------------------------------------------------------------------------
  def serializationData() =
    (rowSeq map (_.map(_.serialization).mkString(SERIALIZATION_MULTIPLE_SEGMENT_1D_DIVIDER)))
      .mkString(SERIALIZATION_ROW_DIVIDER)
  //---------------------------------------------------------------------------
  def serialization() = {
    val dataTypeEncoded = numeric.getClass.getName.split("\\$").last.head
    dataTypeEncoded + SERIALIZATION_MULTIPLE_SEGMENT_2D_DIVIDER +
      serializationData + SERIALIZATION_MULTIPLE_SEGMENT_2D_DIVIDER +
      s"${offset.x},${offset.y}" + SERIALIZATION_MULTIPLE_SEGMENT_2D_DIVIDER +
      s"$id"
  }
  //---------------------------------------------------------------------------
  def setOffset(o: Point2D) = offset = o
  //---------------------------------------------------------------------------
  def getRowCount() = rowSeq.length
  //---------------------------------------------------------------------------
  def calculateCentroidByCentreOfMasses() = {
    val m00 = getFlux()
    var m10 = 0d
    var m01 = 0d
    rowSeq.zipWithIndex flatMap { case (row, y) =>
      row.map { s =>
        val xPosSeq = s.getPosSeq map (_.toDouble)
        val yPosSeq = Array.fill[Double](s.elementCount)(y)
        m10 += s.multiplyAndSum(xPosSeq)
        m01 += s.multiplyAndSum(yPosSeq)
      }
    }
    centroid = (
      Point2D_Double(m10 / m00, m01 / m00)
        + Point2D_Double(offset)
        + Point2D_Double.POINT_ONE) //reference in (1,1)
  }

  //---------------------------------------------------------------------------
  //get surround rectangle in absolute coordinates
  def getRectangleAbso() = {
    if (surroundRectangleAbso == null) {
      val xAxis = getX_Axis()
      val yAxis = getY_Axis()
      surroundRectangleAbso = Rectangle(Point2D(offset.x, offset.y),
                                       Point2D(xAxis.e + offset.x, yAxis.e + offset.y))
    }
    surroundRectangleAbso
  }
  //---------------------------------------------------------------------------
  def rectangleContiguousOrIntersect(other: Source[T]): Boolean = {
    val rec = getRectangleAbso()
    val otherRec = other.getRectangleAbso()
    rec.isContiguousX_Axis(otherRec) || rec.isContiguousY_Axis(otherRec) ||
      rec.intersects(otherRec)
  }
  //---------------------------------------------------------------------------
  def is_8_Connected(other: Source[T]): Boolean = {
    //--------------------------------------------------------------------------
    val otherOffset = other.offset
    //--------------------------------------------------------------------------
    def isConnected(thisSegment: Segment[T]
                    , otherSegment: Segment[T]
                    , thisRowY: Int
                    , otherRowY: Int): Boolean = {
      thisSegment.getPosSeq map { x =>
        val thisPosition = Point2D(x, thisRowY) + offset
        otherSegment.getPosSeq map { x =>
          val otherPosition = Point2D(x, otherRowY) + otherOffset
          EIGHT_CONNECTIVITY_OFFSET_SEQ.foreach { connectivityOffset =>
            if (thisPosition == (otherPosition + connectivityOffset)) return true
          }
        }
      }
      false
    }
    //--------------------------------------------------------------------------
    for ((thisRow,thisRowY) <- rowSeq.zipWithIndex) {
      thisRow.foreach{ thisSegment=>
        for ((otherRow,otherRowY) <- other.rowSeq.zipWithIndex) {
          otherRow.foreach{ otherSegment=>
            if (isConnected(thisSegment, otherSegment, thisRowY, otherRowY)) return true
          }
        }
      }
    }
    false
  }

  //-------------------------------------------------------------------------
  def merge(otherSeq: Source[T]): Array[Source[T]] =
    this.merge(Array(otherSeq))
  //-------------------------------------------------------------------------
  def merge(otherSeq: Array[Source[T]]): Array[Source[T]] = {

    //calculate the max and min position of combined source
    val xAxis = getX_AxisAbso()
    val yAxis = getY_AxisAbso()

    var xMin = xAxis.s
    var xMax = xAxis.e

    var yMin = yAxis.s
    var yMax = yAxis.e

    otherSeq.foreach { other =>
      val xAxis = other.getX_AxisAbso()
      val yAxis = other.getY_AxisAbso()
      xMin = Math.min(xMin, xAxis.s)
      xMax = Math.max(xMax, xAxis.e)

      yMin = Math.min(yMin, yAxis.s)
      yMax = Math.max(yMax, yAxis.e)
    }

    //build a subImage with all sources
    val xRange = xMax - xMin + 1
    val yRange = yMax - yMin + 1
    val mergedSourceSeq = ArrayBuffer.fill(xRange * yRange)(numeric.zero)
    val mergedOffset = Point2D(xMin, yMin)
    (otherSeq :+ this).foreach { source =>
      source.getPosSeq(absolutePos = false).foreach { case (x, y, pix) =>
        val ofsset = source.offset - mergedOffset
        val pos = ((y + ofsset.y) * xRange) + (x + ofsset.x)
        mergedSourceSeq(pos) = pix
      }
    }

    val newSubImage = NSA_SubImage[T](
      mergedSourceSeq.toArray
      , mergedOffset
      , Point2D(xMax, yMax))

    SourceDetection(noiseTide = 0 //all sources
                    , sourceSizeRestriction = None) //No source restrictions
                   .findSourceSeq(newSubImage.asInstanceOf[NSA_SubImage[T]])
  }
  //---------------------------------------------------------------------------
  def prettyPrint(reverse: Boolean = true, o: Point2D = offset): Unit = {
    //-------------------------------------------------------------------------
    val xAxis = getX_Axis
    //-------------------------------------------------------------------------
    def printColIndicator(xAxis: Segment[T], xOffset: Int = 0): Unit = {
      print("|")

      for (i <- 0 until xAxis.range) {
        val colIndex = s"%0${PRETTY_PRINT_COL_VALUE_PAD_SIZE}d".format(i + xOffset)
        val padSize = (PRETTY_PRINT_VALUE_PAD_SIZE - colIndex.length) / 2
        print(" " * padSize)
        print(colIndex)
        print(" " * (padSize + 1))
        print("|")
      }
    }

    //-------------------------------------------------------------------------
    def printRow(row: Array[Segment[T]], rowIndex: Int): Unit = {
      //fill the line with empty values
      val lineSeq = ArrayBuffer.fill(xAxis.range)((" " * PRETTY_PRINT_VALUE_PAD_SIZE) + PRETTY_PRINT_VALUE_CELL_SEPARATOR)
      row foreach { case seg =>
        for (x <- seg.s to seg.e) {
          val pos = x - seg.s
          val pixValue = seg(pos)(numeric)
          lineSeq(x - xAxis.s) =  s"%${PRETTY_PRINT_ROW_VALUE_PAD_SIZE + 1}d".format(pixValue) + PRETTY_PRINT_VALUE_CELL_SEPARATOR
        }
      }
      println(lineSeq.mkString("") + "  " + s"%0${PRETTY_PRINT_ROW_VALUE_PAD_SIZE}d".format(rowIndex))
    }
    //-------------------------------------------------------------------------
    println(s"---------- Source with m2_id: $id starts ----------")

    val maxRowCharSize = PRETTY_PRINT_ROW_HEADER.length + PRETTY_PRINT_ROW_VALUE_PAD_SIZE +
      (xAxis.range * (PRETTY_PRINT_VALUE_PAD_SIZE + 1))

    println(s"Source: m2_id: $id centroid: ${centroid.getFormattedShortString()} With offset: $o  pix count: $getPixCount segment count: $getRowCount")
    println("")
    println("yAbs")
    print("_" * maxRowCharSize)
    print("  yRel")
    println
    val seq = if (reverse) rowSeq.reverseIterator else rowSeq.iterator
    seq.zipWithIndex foreach { case (row, index) =>
      val rowBaseIndex = getRowCount - index - 1
      print(s"%0${PRETTY_PRINT_ROW_VALUE_PAD_SIZE}d".format(rowBaseIndex + o.y) + "->|")
      if (reverse) printRow(row, getRowCount - index - 1)
      else printRow(row, index)
    }

    //end line
    for (_ <- 0 until maxRowCharSize)
      print(0x203e.toChar)
    println

    //column indicator
    print("xAbs")
    print(" " * (PRETTY_PRINT_ROW_HEADER.length - 1))
    printColIndicator(xAxis, o.x + xAxis.s)

    println("")
    print("xRel")
    print(" " * (PRETTY_PRINT_ROW_HEADER.length - 1))
    printColIndicator(xAxis)

    println
    println
    println(s"---------- Source with m2_id: $id ends ----------")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Source.scala
//=============================================================================
