/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Jun/2024
 * Time:  09h:48m
 * Description: None
 */
package com.common.image.nsaImage.source
//=============================================================================
import com.common.geometry.point.Point2D
import com.common.hardware.cpu.CPU
import com.common.image.nsaImage.source.Source.MIN_SOURCE_FOR_ONE_STEP_MERGING
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
//=============================================================================
import scala.collection.mutable.ArrayBuffer
import java.util.concurrent.{ConcurrentHashMap, ConcurrentLinkedQueue}
import scala.collection.JavaConverters.{asScalaSetConverter, collectionAsScalaIterableConverter}
//=============================================================================
//=============================================================================
object SourceMerge extends MyLogger {
  //-------------------------------------------------------------------------
  private final val SURROUND_RECTANGLE_OFFSET_SEQ = Seq(
       Point2D(0,0)  //current pos
     , Point2D(1,0)  //right
     , Point2D(0,1)  //top
     , Point2D(1,1)  //top right
  )
  //---------------------------------------------------------------------------
  private class PopulateRectangleMap[T](seq: Array[Source[T]]
                                       , rectanglePixSize: Point2D
                                       , rectangleMap: ConcurrentHashMap[Point2D, ArrayBuffer[Source[T]]]
                                       , checkSourceDim: Option [Point2D] = None)
    extends ParallelTask[Source[T]](
    seq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100
    , verbose = false ) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(source: Source[T]) = {
      val rectangleOffset = source.offset / rectanglePixSize
      val exists = rectangleMap.get(rectangleOffset) != null
      if (!exists) rectangleMap.put(rectangleOffset,ArrayBuffer[Source[T]]())
      rectangleMap.get(rectangleOffset) += source

      checkSourceDim.foreach { subImageSize =>
        val sourceDim = new Point2D(source.getX_Axis().range
                                    , source.getY_Axis().range)
        if (sourceDim.x > subImageSize.x ||
            sourceDim.y > subImageSize.y)
          warning(s"Source:'${source.id}' with dimensions:'$sourceDim' is greater than the sub-image dimension:'$subImageSize'. Inaccurate results can be produced")
      }
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private def mergingInOneStep[T](sourceSeq: Array[Source[T]]
                                 , sourceSizeRestriction: Option[(Int,Int)]
                                  , checkSourceDim: Option [Point2D] = None)
                                : Array[Source[T]] =
    sourceSeq
      .head
      .merge(sourceSeq.drop(1)).flatMap { source =>
        sourceSizeRestriction match {
          case None => Some(source)
          case Some((minSize, maxSize)) =>
            val sourcePixCount = source.getPixCount()
            if (sourcePixCount >= minSize &&
                sourcePixCount <= maxSize) {
              checkSourceDim.foreach { subImageSize =>
                val sourceDim = new Point2D(source.getX_Axis().range
                  , source.getY_Axis().range)
                if (sourceDim.x > subImageSize.x ||
                  sourceDim.y > subImageSize.y)
                  warning(s"Source:'${source.id}' with dimensions:'$sourceDim' is greater than the sub-image dimension:'$subImageSize'. Inaccurate results can be produced")
              }
              Some(source)
            }
            else None
      }
    }
  //---------------------------------------------------------------------------
  private class ProcessBigRectangle[T](bigRectanglePosSeq: Array[Point2D]
                                       , rectanglePixSize: Point2D
                                       , rectangleMap: ConcurrentHashMap[Point2D, ArrayBuffer[Source[T]]]
                                       , remainSourceSeq: ConcurrentLinkedQueue[Source[T]]
                                       , mergedSourceSeq: ConcurrentLinkedQueue[Source[T]]
                                       , sourceSizeRestriction: Option[(Int,Int)])
    extends ParallelTask[Point2D](
      bigRectanglePosSeq
      ,  CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 100
      , verbose = false) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(currentRectanglePos: Point2D) ={

      val sourceSeq = SURROUND_RECTANGLE_OFFSET_SEQ.flatMap { recOffset =>
        val requiredRecPos = currentRectanglePos + recOffset
        val exists = rectangleMap.get(requiredRecPos) != null
        if (exists) Some(rectangleMap.get(requiredRecPos))
        else None
      }.flatten.toArray

      //check the size of the sources to merge to do it in one step
      if (sourceSeq.length <= MIN_SOURCE_FOR_ONE_STEP_MERGING) {
        info(s"Currente rectangle pos: $currentRectanglePos. Adding sources in one step. Source count to merge:'${sourceSeq.length}'. Min source count for one step merging:'$MIN_SOURCE_FOR_ONE_STEP_MERGING'")
        mergingInOneStep(sourceSeq,sourceSizeRestriction).foreach { source =>
          mergedSourceSeq.add(source)
        }
      }
      else {
        //merge the sources of the big rectangle
        val bigRectangleMergedSourceSeq = sourceSeq.size match {
          case 0 => Array[Source[T]]()
          case 1 => sourceSeq
          case _ => sourceSeq.head.merge(sourceSeq.drop(1))
        }

        val bigRecMin = currentRectanglePos * rectanglePixSize
        val bigRecMax = bigRecMin + (rectanglePixSize * 2) - Point2D.POINT_ONE

        //classify the sources, getting the sources that are at
        // the border of the big rectangle because the will be processed again
        bigRectangleMergedSourceSeq.foreach { source =>
          val rec = source.getRectangleAbso()
          val isAtBorder =
            (rec.min.x <= bigRecMin.x) || (rec.min.y <= bigRecMin.y) || //some sources cross the rectangle definition
              (rec.max.x >= bigRecMax.x) || (rec.max.y >= bigRecMax.y)
          if (isAtBorder) remainSourceSeq.add(source)
          else {
            if (sourceSizeRestriction.isEmpty)
              mergedSourceSeq.add(source)
            else { //not in the border of big rectangle
              val sourcePixCount = source.getPixCount()
              val passSourceSizeRestriction =
                (sourcePixCount >= sourceSizeRestriction.get._1) &&
                  (sourcePixCount <= sourceSizeRestriction.get._2)
              if (passSourceSizeRestriction) mergedSourceSeq.add(source)
            }
          }
        }
      }
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def run[T](sourceSeq: Array[Source[T]]
             , mainImageDimension: Point2D
             , _rectanglePixSize: Point2D
             , sourceSizeRestriction: Option[(Int,Int)]
             , verbose: Boolean): Array[Source[T]] =  {

    Source.initalizeID(Int.MinValue)  //avoid duplicates

    //check the size of the sources to merge: to do it in one step
    if (sourceSeq.length <= MIN_SOURCE_FOR_ONE_STEP_MERGING) {
      info(s"Adding sources in one step. Source count to merge:'${sourceSeq.length}'. Min source count for one step merging:'$MIN_SOURCE_FOR_ONE_STEP_MERGING'")
      return mergingInOneStep(sourceSeq, sourceSizeRestriction, checkSourceDim = Some(_rectanglePixSize))
    }

    val remainSourceSeq = new ConcurrentLinkedQueue[Source[T]]()
    val mergedSourceSeq = new ConcurrentLinkedQueue[Source[T]]()

    //store the sources in its rectangle definition
    var rectanglePixSize = _rectanglePixSize
    val rectangleMap = new ConcurrentHashMap[Point2D, ArrayBuffer[Source[T]]]()
    new PopulateRectangleMap(sourceSeq, rectanglePixSize, rectangleMap, checkSourceDim = Some(rectanglePixSize))
    if (rectangleMap.size == 1) return sourceSeq.head.merge(sourceSeq.drop(1))
    var rectanglePosSeq = rectangleMap.keySet().asScala.toSeq

    //iterate over the rectangles grouping them in each iteration: doubling the size of the rectangle
    var continueProcessing = true
    var iteration = 0
    while(continueProcessing) {

      val (minPosRec, maxPosRec) = Point2D.getMinMax(rectanglePosSeq)

      //group the rectangles in big rectangles
      val bigRectanglePosSeq = (for {
        recPosY <- minPosRec.y to maxPosRec.y by 2
        recPosX <- minPosRec.x to maxPosRec.x by 2
      } yield Point2D(recPosX, recPosY)).toArray

      //detect sources in big rectangle
      remainSourceSeq.clear()
      new ProcessBigRectangle(bigRectanglePosSeq
                              , rectanglePixSize
                              , rectangleMap
                              , remainSourceSeq
                              , mergedSourceSeq
                              , sourceSizeRestriction)

      //check results of detection
      if(remainSourceSeq.isEmpty) continueProcessing = false
      else { //iterate with a bigger rectangle
        if (verbose) info(s"Remain source count:${remainSourceSeq.size()}")
        val _remainSourceSeq = remainSourceSeq.asScala.toArray

        //double the rectangle size
        rectanglePixSize = rectanglePixSize * 2
        rectangleMap.clear()
        new PopulateRectangleMap(_remainSourceSeq, rectanglePixSize, rectangleMap)
        if (rectangleMap.size == 1) {
          _remainSourceSeq.head.merge(_remainSourceSeq.drop(1)).foreach { source =>
            mergedSourceSeq.add(source)
          }
          continueProcessing = false
        }
        else rectanglePosSeq = rectangleMap.keySet().asScala.toSeq
      }
      if (verbose) {
        mergedSourceSeq.asScala.foreach(_.calculateCentroidByCentreOfMasses())
        val paddedIteration = f"${iteration}%04d"
        val fileName = s"output/merged_$paddedIteration.png"
        info(s"Generated file:'$fileName'")
        SourceSynthesization.run(fileName
          , mergedSourceSeq.asScala.toArray
          , xSize = mainImageDimension.x
          , ySize = mainImageDimension.y
          , font = None
          , grid = Some(rectanglePixSize)
        )
      }
      iteration += 1
    }
    //add the remains sources
    remainSourceSeq.asScala.toArray.foreach { mergedSourceSeq.add(_) }

    mergedSourceSeq.asScala.toArray
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file SourceMerge.scala
//=============================================================================