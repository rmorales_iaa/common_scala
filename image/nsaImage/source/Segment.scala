//=============================================================================
package com.common.image.nsaImage.source
//=============================================================================
import com.common.image.nsaImage.source.Interval.IntervalDataType
//=============================================================================
import scala.collection.mutable.ArrayBuffer
import scala.reflect.ClassTag
//=============================================================================
//=============================================================================
object Segment {
  //---------------------------------------------------------------------------
  private val SERIALIZATION_DIVIDER = ":"
  private val SERIALIZATION_DATA_DIVIDER = ","
  //---------------------------------------------------------------------------
  def deserialization(encodedDataType: String, line: String): Segment[_] = {
    val split = line.split(SERIALIZATION_DIVIDER)
    val dataStr = split(2).split(SERIALIZATION_DATA_DIVIDER)
    val s = Interval.fromString(split(0))
    val e = Interval.fromString(split(1))

    encodedDataType match {
      case "B" => Segment[Byte](s, e, dataStr.map (_.toByte))
      case "S" => Segment[Short](s, e, dataStr.map (_.toShort))
      case "I" => Segment[Int](s, e, dataStr.map (_.toInt))
      case "L" => Segment[Long](s, e, dataStr.map (_.toLong))
      case "F" => Segment[Float](s, e, dataStr.map (_.toFloat))
      case "D" => Segment[Double](s, e, dataStr.map (_.toDouble))
    }
  }
  //---------------------------------------------------------------------------
  def apply[T: ClassTag : Numeric]
           (s: Segment[T]
            , o: IntervalDataType)
           : Segment[T] =
           Segment[T](s.s + o
                      , s.e + o
                      , s.d)
  //---------------------------------------------------------------------------
  def apply[T: ClassTag : Numeric]
            (p: IntervalDataType)
            : Segment[T] = Segment[T](p, p, Array())
  //---------------------------------------------------------------------------
  def apply[T: ClassTag : Numeric]
           (d: Array[T])
           : Segment[T] =
    Segment[T](0
               , d.length - 1
               , d)
  //---------------------------------------------------------------------------
  def getMax[T :ClassTag :Numeric](seq: Array[Segment[T]]) = {
    var minS = Int.MaxValue
    var maxE = Int.MinValue
    seq foreach { s =>
      minS = Math.min(minS,s.s)
      maxE = Math.max(maxE,s.e)
    }
    new Segment[T](minS, maxE, Array())
  }
  //---------------------------------------------------------------------------
  //return the sequence of contiguous segments regarding the the one at index starIndex'
  def getFirstContiguousSeq[T](seq: Array[Segment[T]], starIndex : Int = 0) : Array[Segment[T]] = {
    val contiguousSeq = ArrayBuffer[Segment[T]]()
    var lastContiguousIndex = -1
    for(x<- starIndex until seq.length-1)
      if (seq(x) isContiguous seq(x+1)) {
        contiguousSeq += seq(x)  
        lastContiguousIndex = x+1
      }
      else {
        if (lastContiguousIndex != -1)  contiguousSeq += seq(lastContiguousIndex)
        return contiguousSeq.toArray
      }
    if (lastContiguousIndex != -1)  contiguousSeq += seq(lastContiguousIndex)
    contiguousSeq.toArray
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.common.image.nsaImage.source.Segment._
case class Segment[T :ClassTag :Numeric]
                  (s : IntervalDataType
                   , e : IntervalDataType
                   , d: Array[T] = Array()) extends Interval {
  //---------------------------------------------------------------------------
  private var flag = 0
  //---------------------------------------------------------------------------
  if(d.length > 0)
    require((e - s + 1) == d.length, s"Range of segment: ${e-s+1} must match with data size: ${d.length}")
  //---------------------------------------------------------------------------
  def apply(x:Int)
           (numeric: Numeric[T]): T =
    if (d.isEmpty) numeric.zero else d(x)
  //---------------------------------------------------------------------------
  def getFlux(implicit numeric: Numeric[T]):Double =
    numeric.toDouble(d.sum(numeric))
  //---------------------------------------------------------------------------
  def getFlag = flag
  //---------------------------------------------------------------------------
  def size() = e-s+1
  //---------------------------------------------------------------------------
  def setFlag(f: Int) = flag = f
  //---------------------------------------------------------------------------
  override def equals(obj: Any): Boolean = obj match {
    case seg: Segment[T] =>
      this.s == seg.s && this.e == seg.e && (this.d sameElements seg.d)
    case _ => false
  }
  //---------------------------------------------------------------------------
  def !=(seg: Segment[T])  = !(this == seg)
  //---------------------------------------------------------------------------
  def - (o: IntervalDataType) =
    Segment[T]((s - o)
             , (e - o)
             , d)
  //---------------------------------------------------------------------------
  def findPos(v: T) = d.indexOf(v)

  //---------------------------------------------------------------------------
  def add(itv: Segment[T]): Array[Segment[T]] = {
    if (isContiguous(itv)) {
      if(this <= itv) Array(Segment[T](s, itv.e, d ++ itv.d))
      else Array(Segment[T](itv.s, e, itv.d ++ d))
    }
    else Array(this,itv)
  }
  //---------------------------------------------------------------------------
  def addIfContiguous(p: IntervalDataType
                      , v: T): Option[Segment[T]] = {
    if ((e + 1) == p)   //x is just on the right
      Some(Segment[T](s, p, d :+ v))
    else if ((p + 1) == s)
      Some(Segment[T](p, e, v +: d))  //x is just on the left
    else
      None
  }
  //---------------------------------------------------------------------------
  def addIfContiguous(p: IntervalDataType)
                     : Option[Segment[T]] = {
    if ((e + 1) == p) Some(Segment(s, p, d)) //x is just on the right
    else if ((p + 1) == s) Some(Segment(p, e, d)) //x is just on the left
    else None
  }

  //---------------------------------------------------------------------------
  def getIntersectionNoData(other: Segment[T]): Option[Segment[T]] = {
    val its = getIntersectionInterval(other)
    if (its.isEmpty) None
    else Some(Segment[T](its.get._1,its.get._2,Array()))
  }
  //---------------------------------------------------------------------------
  def getIntersection(seg: Segment[T]
                      , storeData: Boolean = false
                      , keepFlag: Boolean = false
                      , keepIDInputFlag: Boolean = false): Option[Segment[T]] = {
    if (!(this intersects seg)) return None
    val start_max = if (s >  seg.s)  s else seg.s
    val end_min = if (e <  seg.e)  e else seg.e
    if (end_min < start_max) None
    else {
      val newD =  if(!storeData) Array[T]() else d.slice(start_max - s, (end_min + 1) - s)
      val newFlag = if (keepFlag) flag else if (keepIDInputFlag) seg.flag else 0
      val r = Segment(start_max, end_min, newD)
      r.setFlag(newFlag)
      Some(r)
    }
  }
  //---------------------------------------------------------------------------
  def prettyPrint(pixValuePadSize: Int
                  , pixValueSeparator : String): Unit = {
    d foreach { v =>
      val padSize = pixValuePadSize - v.toString.size
      if (padSize > 0)  print("0" * padSize)
      print(v + pixValueSeparator)
    }
  }
  //---------------------------------------------------------------------------
  override def toString = super.toString + s"=>{${d.mkString(",")}}"
  //---------------------------------------------------------------------------
  def serialization() = {
    s"$s" + Segment.SERIALIZATION_DIVIDER +
      s"$e" + Segment.SERIALIZATION_DIVIDER +
      d.mkString(SERIALIZATION_DATA_DIVIDER) + Segment.SERIALIZATION_DIVIDER
  }
  //---------------------------------------------------------------------------
  def multiplyAndSum(scale: Array[Double])
                    (implicit numeric: Numeric[T]): Double =
    (d zip scale).map { case (value, factor) => numeric.toDouble(value) * factor }.sum
  //---------------------------------------------------------------------------
  def getPosSeq: Array[IntervalDataType] =
    (s to e).map(i => i).toArray
  //---------------------------------------------------------------------------
  def getPosSeqWithValues(): Array[(IntervalDataType,T)] =
    (s to e).map(i => (i, d(i - s))).toArray
  //---------------------------------------------------------------------------
  def getNonZeroValueCount
      (implicit numeric: Numeric[T])
      = d.filter( _ != numeric.zero).size
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================