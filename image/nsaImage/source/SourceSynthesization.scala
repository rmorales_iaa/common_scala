/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  01/Jun/2024 
 * Time:  16h:10m
 * Description: None
 */
package com.common.image.nsaImage.source
//=============================================================================
import com.common.geometry.point.{Point2D, Point2D_Double}
import com.common.math.regression.Regression
//=============================================================================
import java.awt.image.BufferedImage
import java.awt.{Color, Font}
import java.io.File
import javax.imageio.ImageIO
//=============================================================================
//=============================================================================
object SourceSynthesization {
  //---------------------------------------------------------------------------
  //https://www.w3schools.com/colors/colors_picker.asp
  //https://www.rapidtables.com/web/color/RGB_Color.html
  //https://html-color.codes/image
  val COLOR_PALETTE = IndexedSeq(
    0xff0000, 0x008B8B, 0x7FFFD4, 0xffbf00, 0xffff00, 0xbfff00, 0x80ff00, 0x40ff00,
    0x00ff80, 0x00ffbf, 0x00bfff, 0x0080ff, 0x0040ff, 0x0000ff, 0x4000ff, 0x8000ff,
    0xbf00ff, 0xff00ff, 0xff0080, 0xff0040, 0x800000, 0x808000, 0x008080, 0x000080,
    0xFF7F50, 0xCD5C5C, 0xB8860B, 0x90EE90, 0x3CB371, 0xff4000, 0xff8000, 0x00BFFF,
    0x808000, 0xbdb76b, 0x9acd32, 0x6b8e23, 0xe3f988, 0xd1e189, 0xa4c639, 0x8db600,
    0xe8f48c, 0xffff66, 0xfada5e, 0xdfff00, 0xeeff1b, 0xffd800, 0xe6e200, 0xeeff1b,
    0xffc0cb, 0xff69b4, 0xdb7093, 0xff1493, 0xffe4e1, 0xfddde6, 0xffb6c1, 0xf28dcd,
    0xffa07a, 0xff4500, 0xdc143c, 0xfd5e53, 0xfe6f5e, 0xe97451, 0xff355e, 0xfe2712,
    // Complementary colors
    0x00ffff, 0xff7474, 0x80002b, 0x0040ff, 0x0000ff, 0x4000ff, 0x7fff00, 0xbf00ff,
    0xff007f, 0xff0040, 0xff40ff, 0xff7fff, 0xffbfff, 0xbfff00, 0x7fff00,
    0x40ff00, 0x00ff00, 0x00ff7f, 0x00ffbf, 0x7fff7f, 0x7f7f7f, 0xff7f7f,
    0x0080af, 0x32a3a3, 0x4775f4, 0x6f116f, 0xc34d8c, 0x00bfff, 0x0080ff, 0xff40ff,
    0x7f7f7f, 0x4247ff, 0x6532cd, 0x9471dc, 0x1c07b1, 0x2e1e76, 0x5b3e96, 0x7249ff,
    0x17007a, 0x000099, 0x025aa3, 0x3200ff, 0x1100e4, 0x0028ff, 0x0001df, 0x0028e4,
    0x003f34, 0x00964b, 0x2f8f6c, 0x00eb6c, 0x001f1e, 0x020021, 0x00493e, 0x0d7233,
    0x005f85, 0x00ba9f, 0x23ebc3, 0x02a1b3, 0x01909f, 0x0080a6, 0x00caeb, 0x01d3ef
  )
  //---------------------------------------------------------------------------
  private final val COLOR_RGB_WHITE = 0xFFFFFF
  //---------------------------------------------------------------------------
  def run[T]
         (pngImageName: String
          , sourceSeq: Array[Source[T]]
          , xSize: Int
          , ySize: Int
          , blackAndWhite: Boolean = false
          , userPixColor: Option[Int] = None //set to None for color random color in each source
          , centroidColor: Option[Int] = None //set to None when no centroid must be show
          , font: Option[Font] = Some(new Font(Font.MONOSPACED, Font.PLAIN, 10)) //Set to 'None' for no text on source
          , fontColor: Color = Color.YELLOW
          , fontOffsetX: Int = 0
          , fontOffsetY: Int = 1
          , dict: scala.collection.mutable.Map[Long, String] = scala.collection.mutable.Map[Long, String]()
          , differentColorSource: scala.collection.mutable.Map[Long, Color] = scala.collection.mutable.Map[Long, Color]()
          , arrowDiffMap: Map[Int, Point2D] = Map[Int, Point2D]()
          , pixelTransformation: Option[(Option[Seq[Double]], Option[Seq[Double]])] = None
          , inverYAxis: Boolean = true
          , additionalOffset: Point2D = Point2D.POINT_ZERO
          , grid: Option[Point2D] = None)
    : Unit = {

    if (sourceSeq.isEmpty) return
    //---------------------------------------------------------------------------
    val bmpImage = new BufferedImage(xSize, ySize, BufferedImage.TYPE_INT_RGB)
    val g2d = bmpImage.createGraphics()
    val bmpMinX = bmpImage.getMinX
    val bmpMinY = bmpImage.getMinY
    val bmpWidth = bmpImage.getWidth
    val bmpHeight = bmpImage.getHeight
    //---------------------------------------------------------------------------
    val numeric = sourceSeq.head.numeric
    val pixZero = numeric.zero
    //---------------------------------------------------------------------------
    def plotPixel(x: Int, y: Int, v: Int) = {

      if (x >= bmpMinX &&
        x < (bmpMinX + bmpWidth) &&
        y >= bmpMinY &&
        y < (bmpMinY + bmpHeight))
        bmpImage.setRGB(x, y, v)
    }

    //---------------------------------------------------------------------------
    def getColor(source: Source[T], sourceIndex: Int) = {
      if (differentColorSource.contains(source.id)) {
        val c = differentColorSource(source.id)
        g2d.setColor(c)
        c.getRGB
      }
      else {
        g2d.setColor(fontColor)
        if (blackAndWhite) COLOR_RGB_WHITE
        else if (userPixColor.isDefined) userPixColor.get else COLOR_PALETTE(sourceIndex % COLOR_PALETTE.length)
      }
    }

    //---------------------------------------------------------------------------
    def applyTransformationX(x: Int) =
      if (pixelTransformation.isEmpty || pixelTransformation.get._1.isEmpty) x
      else Math.round(Regression.evaluatePolynomy(pixelTransformation.get._1.get, x)).toInt

    //---------------------------------------------------------------------------
    def applyTransformationY(y: Int) =
      if (pixelTransformation.isEmpty || pixelTransformation.get._2.isEmpty) y
      else Math.round(Regression.evaluatePolynomy(pixelTransformation.get._2.get, y)).toInt
    //---------------------------------------------------------------------------
    def drawGrid() = {
      // Draw grid if specified
      grid.foreach { case Point2D(gridX, gridY) =>
        g2d.setColor(Color.GRAY)
        for (x <- 0 until xSize by gridX) {
          g2d.drawLine(x, 0, x, ySize)
        }
        for (y <- 0 until ySize by gridY) {
          g2d.drawLine(0, y, xSize, y)
        }
      }
    }
    //---------------------------------------------------------------------------
    //https://stackoverflow.com/questions/2027613/how-to-draw-a-directed-arrow-line-in-java
    def drawArrowLine(x1: Int, y1: Int, x2: Int, y2: Int, d: Int, h: Int) = {
      val dx = x2 - x1
      val dy = y2 - y1
      val D = Math.sqrt(dx * dx + dy * dy)
      var xm: Double = D - d
      var xn: Double = xm
      var ym: Double = h
      var yn: Double = -h
      var x = .0d
      val sin: Double = dy / D
      val cos: Double = dx / D

      x = xm * cos - ym * sin + x1
      ym = xm * sin + ym * cos + y1
      xm = x

      x = xn * cos - yn * sin + x1
      yn = xn * sin + yn * cos + y1
      xn = x

      val xpoints = Array(applyTransformationX(x2), applyTransformationX(xm.toInt), applyTransformationX(xn.toInt))
      val ypoints = Array(applyTransformationY(y2), applyTransformationY(ym.toInt), applyTransformationY(yn.toInt))

      g2d.drawLine(applyTransformationX(x1), applyTransformationY(y1), applyTransformationX(x2), applyTransformationY(y2))
      g2d.fillPolygon(xpoints, ypoints, 3)
    }
    //---------------------------------------------------------------------------
    if (font.isDefined) {
      g2d.setFont(font.get)
      g2d.setColor(fontColor)
    }

    //init bmp
    for (x <- 0 until xSize;
         y <- 0 until ySize)
      bmpImage.setRGB(x, y, numeric.toInt(pixZero))

    // Draw grid if specified
    if (grid.isDefined) drawGrid()

    //render all pix of the source
    sourceSeq.zipWithIndex.foreach { case (source, sourceIndex) =>

      val offset = source.offset + additionalOffset

      //render all pixels of the source
      for ((row, index) <- source.rowSeq.zipWithIndex) {
        for (seg1D <- row) {
          val y = index
          for (x <- seg1D.s to seg1D.e) {
            if (!seg1D(x - seg1D.s)(numeric).equals(pixZero)) {
              val finalX = x + offset.x - 1 //bmp origin is in (0,0)
              var finalY = y + offset.y - 1 //bmp origin is in (0,0)
              finalY = if (inverYAxis) ySize - finalY else finalY
              val pixColor = getColor(source, sourceIndex)
              plotPixel(finalX, finalY, pixColor)
            }
          }
        }
      }

      //centroid
      if (centroidColor.isDefined) {
        val centroid = source.getCentroid - Point2D_Double.POINT_ONE //bmp origin is in (0,0)
        val finalX = Math.round(centroid.x).toInt
        val finalY = if (inverYAxis) (ySize - Math.round(centroid.y) - 1).toInt else (Math.round(centroid.y) - 1).toInt
        plotPixel(finalX, finalY, centroidColor.get)

        //arrow diff
        if (arrowDiffMap.size > 0) {
          val endPoint = arrowDiffMap(source.id)
          val endPointFinalX = endPoint.x
          val endPointFinalY = if (inverYAxis) ySize - endPoint.y - 1 else endPoint.y - 1
          drawArrowLine(finalX, finalY, endPointFinalX, endPointFinalY, 8, 8)
        }
      }

      //font
      if (font.isDefined) {
        val centroid = source.getCentroid() - Point2D_Double.POINT_ONE //bmp origin is in (0,0)
        val finalX = Math.round(centroid.x)
        val finalY = if (inverYAxis) ySize - Math.round(centroid.y) else Math.round(centroid.y)
        val message = source.id.toString + (if (dict.contains(source.id)) "->" + dict(source.id) else "")
        g2d.drawString(message, finalX.toInt + fontOffsetX, (finalY - fontOffsetY).toInt)
      }
    }

    val fileName = if (pngImageName.endsWith(".png")) pngImageName else pngImageName + ".png"
    ImageIO.write(bmpImage, "png", new File(fileName))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SourceSynthesization.scala
//=============================================================================