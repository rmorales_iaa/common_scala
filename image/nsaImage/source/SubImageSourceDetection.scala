
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  20/Oct/2023
 * Time:  12h:52m
 * Description: None
 */
//=============================================================================
package com.common.image.nsaImage.source
//=============================================================================
import com.common.geometry.point.Point2D
import com.common.image.nsaImage.NSA_SubImage
import com.common.logger.MyLogger
//=============================================================================
import scala.collection.mutable.ArrayBuffer
import scala.reflect.ClassTag
//=============================================================================
//=============================================================================
object SubImageSourceDetection extends MyLogger {
  //-------------------------------------------------------------------------
  def findSourceSeq[T: ClassTag:  Numeric]
                    (subImage: NSA_SubImage[T]
                     , compiledSourceSeq: ArrayBuffer[Source[T]]
                     , verbose: Boolean = false): Array[Source[T]] = {
    val noiseTide = subImage.getEstDetectionLevel

    val sourceSeq = SourceDetection(noiseTide
                                    , sourceSizeRestriction = None  //none source restriction applied to avoid dump small partial detections
                                    ).findSourceSeq(subImage)

    if (verbose)
        info(s"SubImage '${subImage.getID}' min pos: '${subImage.minPos}' max pos: '${subImage.maxPos}' before apply restrictions, found source count: '${sourceSeq.length}'")

    sourceSeq.foreach { source =>
      val sourceX_Axis = source.getX_AxisAbso()
      val sourceY_Axis = source.getY_AxisAbso()

      //check if source is at the border
      if (subImage.isAtBorderAbso(sourceX_Axis.s, sourceY_Axis.s) ||
        subImage.isAtBorderAbso(sourceX_Axis.e, sourceY_Axis.e)) { //avoid source restriction check
        source.setExtraInfo(1)
        compiledSourceSeq += source
      }
      else compiledSourceSeq += source
    }
    compiledSourceSeq.toArray
  }
  //-------------------------------------------------------------------------
  def mergeSourceSeq[T: ClassTag:  Numeric]
                     (_compiledSourceSeq: Array[Source[T]]
                     , mainImageDimension: Point2D
                     , subImagePixSize: Point2D
                     , sourceSizeRestriction: Option[(Int,Int)]
                     , verbose: Boolean = false)
                     : Array[Source[T]] = {

    //compiled sources
    val compiledSourceSeq = ArrayBuffer[Source[T]]()

    //split by sources detected at border
    val sourceSeqAtBorder    = ArrayBuffer[Source[T]]()
    val sourceSeqNotAtBorder = ArrayBuffer[Source[T]]()

    _compiledSourceSeq.foreach { source =>
      if (source.hasExtraInfo) sourceSeqAtBorder += source
      else sourceSeqNotAtBorder += source
    }
    //add to final result the sources not at the border
    compiledSourceSeq ++= sourceSeqNotAtBorder

    //add to final result the merged sources at the border
    compiledSourceSeq ++= SourceMerge.run(sourceSeqAtBorder.toArray
                                          , mainImageDimension
                                          , subImagePixSize
                                          , sourceSizeRestriction
                                          , verbose)

    //apply the restrictions (it was disabled up to now)
    val r =
      if (sourceSizeRestriction.isEmpty) compiledSourceSeq
      else compiledSourceSeq.filter { source =>
        val sourcePixCount = source.getPixCount()
        (sourcePixCount >= sourceSizeRestriction.get._1) &&
        (sourcePixCount <= sourceSizeRestriction.get._2)
      }

    //calculate centroids
    r.foreach(_.calculateCentroidByCentreOfMasses())

    r.toArray
  }
  //---------------------------------------------------------------------------
  private def sourcePassRestrictions
             [T: ClassTag:  Numeric]
             (source: Source[T]
             , sourceSizeRestriction: (Int, Int)): Boolean = {
    val sourcePixCount = source.getPixCount()
    (sourcePixCount >= sourceSizeRestriction._1) &&
    (sourcePixCount <= sourceSizeRestriction._2)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SubImageSourceDetection.scala
//=============================================================================