/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  19/Feb/2021
  * Time:  11h:07m
  * Description:
 *      To get Lat-Log-Alt of a telescope use: 'find_orb' tool: 'mpc_code'
 *      To get XYZ position use 'Telescope.getRectangularCoordinateSeq'
 *      To genereate telescope bsp:
 *        - modify properly the  file: 'input/spice/kernels/spk/observatories/observatory_def' to append the NEW telescope
 *            SITES
 *            TELESCOPE_NAME_IDCODE (append 1 to get a NEW index)
 *            TELESCOPE_NAME_XYX
 *        - create a file the the name of telescope in: 'input/telescope' filling properly:
 *            observatoryCode
 *              spkObservatoryCode (same as TELESCOPE_NAME_IDCODE)
 *            latLongAlt
 *        - use 'spice' 'pipoint' tool (adapt the paths to the proper location)
 *           rm /home/rafa/proyecto/m2/input/spice/kernels/spk/observatories/observatory.bsp
 *          /home/rafa/images/tools/spice/cspice/exe/pinpoint -def /home/rafa/proyecto/m2/input/spice/kernels/spk/observatories/observatory_def -spk /home/rafa/proyecto/m2/input/spice/kernels/spk/observatories/observatory.bsp
 */
//=============================================================================
package com.common.image.telescope
//=============================================================================
//=============================================================================
import com.common.configuration.{MyConf, MyConfTrait, MyConfWithDefault}
import com.common.coordinate.conversion.Conversion
import com.common.fits.simpleFits.SimpleFits
import com.common.image.filter.Filter.PATTERN_ALWAYS_FAIL
import com.common.image.telescope.TelescopeMatcher.getTelescope
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.path.Path
//=============================================================================
import scala.util.matching.Regex
//=============================================================================
object Telescope extends MyLogger {
  //---------------------------------------------------------------------------
  val UNKNOWN_TELESCOPE = "UNKNOWN"
  val IGNORED_TELESCOPE_NAME = "IGNORED"
  //---------------------------------------------------------------------------
  private final val CONF_INSTRUMENT_ROOT = "Instrument.pixelScale"
  //---------------------------------------------------------------------------
  //higher priority means go first
  private val PRIORITY_100 =   100
  private val PRIORITY_5 =       5
  private val PRIORITY_NONE =    0
  //---------------------------------------------------------------------------
  private val CONFIGURATION_SPK_OBSERVATORY_CODE = "Location.spkObservatoryCode"
  private val CONFIGURATION_OBSERVATORY_CODE = "Location.observatoryCode"
  private val defaultConf = MyConf("input/telescope/default/default.conf", verbose = false)
  private final val DEFAULT_PIXEL_SCALE = defaultConf.getDouble(s"$CONF_INSTRUMENT_ROOT.default")
  //---------------------------------------------------------------------------
  private val PATTERN_ALWAYS_FAIL = "(?!)".stripMargin.replaceAll("\n","").r  //always fail match
  //---------------------------------------------------------------------------
  private final val DEFAULT_TELESCOPE_CODE    = "J86"     //OSN
  final val DEFAULT_TELESCOPE_SPK_CODE        =  "399800"  //OSN
  private final val DEFAULT_TELESCOPE_LAT_LON =  List(37.064136d, 356.6153d)  //OSN
  //---------------------------------------------------------------------------
  private val codeCacheMap       = scala.collection.mutable.Map[String, String]()
  private val spkCacheMap        = scala.collection.mutable.Map[String, String]()
  private val latLonAltCacheMap  = scala.collection.mutable.Map[String, List[Double]]()
  private val configurationMap   = scala.collection.mutable.Map[String, MyConfWithDefault]()
  //---------------------------------------------------------------------------
  def getTelescopeDir = Path.ensureEndWithFileSeparator(MyConf.c.getString("Telescope.dir"))
  //---------------------------------------------------------------------------
  def getNormalizedTelescopeName(fits: SimpleFits) : String =
    getNormalizedTelescopeName(getTelescope(fits))
  //---------------------------------------------------------------------------
  def getNormalizedTelescopeName(s: String) : String =
    s.toLowerCase.replaceAll("\\.","_").replaceAll("'","")
  //---------------------------------------------------------------------------
  def getConfigurationFile(fits: SimpleFits) : MyConfTrait = {
    val telescopeName =  getTelescope(fits)
    getConfigurationFile(telescopeName)
  }
  //---------------------------------------------------------------------------
  def existConfigurationFile(name: String) = {
    val telescopeConfFileName = (getTelescopeDir + getNormalizedTelescopeName(name) + ".conf").toLowerCase()
    MyFile.fileExist(telescopeConfFileName)
  }
  //---------------------------------------------------------------------------
  def getConfigurationFile(name: String): MyConfTrait = {
    val telescopeConfFileName = (getTelescopeDir + getNormalizedTelescopeName(name) + ".conf").toLowerCase()

    if (configurationMap.contains(telescopeConfFileName)) return configurationMap(telescopeConfFileName)
    if(!MyFile.fileExist(telescopeConfFileName)) {
      warning(s"Can not find the telescope input configuration file:'$telescopeConfFileName'. Using default configuration file: '${defaultConf.confFileName}'")
      return defaultConf
    }
    val c = MyConfWithDefault(telescopeConfFileName, defaultConf, verbose = false)
    configurationMap(telescopeConfFileName) = c
    c
  }
  //---------------------------------------------------------------------------
  def isKnown(telescopeName: String): Boolean =
    if (telescopeName.isEmpty) false
    else {
      val name = getNormalizedTelescopeName(telescopeName)
      codeCacheMap.contains(name)
    }
  //---------------------------------------------------------------------------
  def isKnown(fits:SimpleFits): Boolean =
    isKnown(getTelescope(fits))

  //---------------------------------------------------------------------------
  def getOfficialCode(fits: SimpleFits): String = {
    val telescopeName = getTelescope(fits)
    if (telescopeName.isEmpty) "NONE"
    else getOfficialCode(telescopeName)
  }
  //---------------------------------------------------------------------------
  def getOfficialCode(telescopeName:String): String = {
    if (telescopeName.isEmpty) DEFAULT_TELESCOPE_CODE //by default OSN
    else {
      val name = getNormalizedTelescopeName(telescopeName)
      if (codeCacheMap.contains(name)) return codeCacheMap(name)
      val telescopeConf = getConfigurationFile(name)
      if (telescopeConf == null) DEFAULT_TELESCOPE_CODE
      else {
        val r = telescopeConf.getStringWithDefaultValue(CONFIGURATION_OBSERVATORY_CODE, DEFAULT_TELESCOPE_CODE)
        codeCacheMap(name) = r
        r
      }
    }
  }
  //---------------------------------------------------------------------------
  def getSpkId(fits: SimpleFits) : String = {
    val telescopeName = getTelescope(fits)
    if (telescopeName.isEmpty) "NONE"
    else getSpkId(telescopeName)
  }
  //---------------------------------------------------------------------------
  def getSpkId(telescopeName: String) : String = {
    val name = getNormalizedTelescopeName(telescopeName)
    if (spkCacheMap.contains(name)) return spkCacheMap(name)
    val telescopeConf = getConfigurationFile(name)

    if (telescopeConf == null) {
      error(s"Telescope : '$telescopeName' has not a configuration file. Using default telescope configuration")
      spkCacheMap(name) = DEFAULT_TELESCOPE_SPK_CODE
      DEFAULT_TELESCOPE_SPK_CODE
    }
    else {
      val r = telescopeConf.getStringWithDefaultValue(CONFIGURATION_SPK_OBSERVATORY_CODE, DEFAULT_TELESCOPE_SPK_CODE)
      spkCacheMap(name) = r
      r
    }
  }
  //---------------------------------------------------------------------------
  def getLatLonAlt(fits: SimpleFits) : List[Double] = getLatLonAlt(getTelescope(fits))
  //---------------------------------------------------------------------------
  def getLatLonAlt(telescopeName: String) : List[Double] = {
    val name = getNormalizedTelescopeName(telescopeName)
    if (latLonAltCacheMap.contains(name)) return latLonAltCacheMap(name)
    val telescopeConf = getConfigurationFile(name)
    val key = "Location.latLongAlt"
    val r =
      if (!telescopeConf.existKey(key)) {
        warning(s"Configuration key '$key' in telescope: '$telescopeName' was not found. Using default latitude, longitude and altitude")
        DEFAULT_TELESCOPE_LAT_LON
      }
      else telescopeConf.getDoubleSeq(key)
    latLonAltCacheMap(name) = r
    r
  }

  //---------------------------------------------------------------------------
  //(code,name,lat,lon,alt)
  def parseEntryFindOrb(line: String) = {
   //0  1    2          3            4        5         6            7
   //Pl Code Longitude  Latitude     Altitude rho_cos   rho_sin_phi  region
   val seq = line.trim.trim.replaceAll(" +", " ").split(" ")
   val region = seq.drop(7).mkString(" ")
   (seq(1),region,seq(3),seq(2),seq(4))
  }
  //---------------------------------------------------------------------------
  //mainly used to create the SPK of an observatory.
  //(longitide,latitude,altitutde) can be obtained from "find_orb" (https://github.com/Bill-Gray/find_orb) utility mpc_obs
  //See input/spice/kernels/spk/observatories/observatory_def
  //(x,y,z)
  def getRectangularCoordinateSeq(longitude: Double, latitude: Double, altitude: Double) = {
    if (latitude <= 90 && latitude >= -90)
      Conversion.latitudinalToRectangularWgs84(latitude,longitude,altitude)
    else {
      error(s"Error, Latitude is in [90,-90], but current latitude is: '$latitude'. Latitude is above the equator (N), and negative latitude is below the equator (S)" )
      (-1d,-1d,-1d)
    }
  }
  //===========================================================================
  def getPixScale(telescopeName: String
                  , instrumentName: String): Double = {

    val confFilename = s"input/telescope/$telescopeName.conf".toLowerCase

    if (!MyFile.fileExist(confFilename)) {
      error(s"PixelScale. Unknown telescope: '$confFilename' Using default pixel scale: '$DEFAULT_PIXEL_SCALE'")
      DEFAULT_PIXEL_SCALE
    }
    else {
      val conf = MyConf(confFilename)
      val instrumentKey = s"$CONF_INSTRUMENT_ROOT.${instrumentName.toLowerCase}"
      if (conf.existKey(instrumentKey))
        conf.getDouble(instrumentKey)
      else {
        warning(s"PixelScale. Telescope:'$telescopeName' with configuration file:' $confFilename' has no entry for instrument:'$instrumentName'. Using default pixel scale for this telescope")
        conf.getDouble(s"$CONF_INSTRUMENT_ROOT.default")
      }

    }

  }
  //===========================================================================
}
//=============================================================================
import com.common.image.telescope.Telescope._
sealed trait Telescope {
  //---------------------------------------------------------------------------
  val name: String
  val telescopePattern: Regex
  val pathPattern: Regex
  //---------------------------------------------------------------------------
  //priority: TelescopeValue, objectValue, alternativeTelescopeValue, path
  def tryToMatch(telescopeValue: String, path: String): (Boolean,Int) = {
    if (tryToMatchTelescopeValue(telescopeValue)) return (true,PRIORITY_100)
    if (tryToMatchPathValue(path)) return (true,PRIORITY_5)
    (false,PRIORITY_NONE)
  }
  //---------------------------------------------------------------------------
  private def tryToMatchTelescopeValue(v: String) =
    v.trim.toLowerCase.trim match {
      case telescopePattern(_*) => true
      case _ => false
    }
  //---------------------------------------------------------------------------
  private def tryToMatchPathValue(v: String) =
    v.toLowerCase.trim match {
      case pathPattern(_*) => true
      case _ => false
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class TelescopeIgnored() extends Telescope {
  //---------------------------------------------------------------------------
  val name = IGNORED_TELESCOPE_NAME
  val telescopePattern =
    """.*test.*|
      |.*check.*|
      |.*trash.*|
      |.*basura.*|
      |.*error.*""".stripMargin.replaceAll("\n","").r

  val pathPattern   = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
sealed case class TelescopeMultipleMatch(nameSeq: String) extends Telescope() {
  //---------------------------------------------------------------------------
  val name = "MULTIPLE_FILTER_MATCH_" + nameSeq
  val telescopePattern = PATTERN_ALWAYS_FAIL
  val pathPattern   = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class TelescopeUnknown() extends Telescope() {
  //---------------------------------------------------------------------------
  val name             = UNKNOWN_TELESCOPE
  val telescopePattern = PATTERN_ALWAYS_FAIL
  val pathPattern      = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class OSN_90() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "OSN_90"

  val telescopePattern =
    """.*osn.?90.*|
      |.*t90.?osn*""".stripMargin.replaceAll("\n", "").r
  val pathPattern = telescopePattern
  //---------------------------------------------------------------------------

}
//=============================================================================
sealed case class OSN_1_5() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "OSN_1.5"
  val telescopePattern =
    """1\.5m|
      |.*osn.*|
      |.*t150?.*|
      |.*1.*nevada.*|
      |.*nevada.*1.*""".stripMargin.replaceAll("\n","").r
  val pathPattern = telescopePattern
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class CAHA_1_23() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "CAHA_1.23"
  val telescopePattern =
    """ca.?1.*|
      |.*1\.23.*|
      |.*caha.?1.*""".stripMargin.replaceAll("\n","").r
  val pathPattern = telescopePattern
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class CAHA_2_2() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "CAHA_2.2"
  val telescopePattern =
    """2\.2m|
      |.*caha.?2.*|
      |.*ca.?2.*""".stripMargin.replaceAll("\n","").r
  val pathPattern = telescopePattern
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class CAHA_3_50() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "CAHA_3_5"
  val telescopePattern =
    """.*ca.?3.*|
      |.*caha.?3.*""".stripMargin.replaceAll("\n","").r
  val pathPattern = telescopePattern
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class LaHita_77() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "LaHita77"
  val telescopePattern =
    """.*la.?hita.*|
      |t77""".stripMargin.replaceAll("\n","").r
  val pathPattern =telescopePattern
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class ASH() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "ASH"
  val telescopePattern =
    """.*ash.*""".stripMargin.replaceAll("\n","").r
  val pathPattern =telescopePattern
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class ASH2() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "ASH2"
  val telescopePattern =
    """.*ash2.*|
      |asa""".stripMargin.replaceAll("\n","").r
  val pathPattern =telescopePattern
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class TCS() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "TCS"
  val telescopePattern =
    """.*carlos.*|
      |.*sanchez.*|
      |tcs""".stripMargin.replaceAll("\n","").r
  val pathPattern =telescopePattern
  //---------------------------------------------------------------------------
}
//=============================================================================
//Korea Microlensing Telescope Network
sealed case class KMTNet() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "KMTNet"
  val telescopePattern =
    """.*kmtnet.*|
      |.*kasi.*|
      |kasi""".stripMargin.replaceAll("\n","").r
  val pathPattern =telescopePattern
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class Casleo_215() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "CASLEO_215"
  val telescopePattern =
    """.*casleo.*""".stripMargin.replaceAll("\n","").r
  val pathPattern = telescopePattern
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class PicduMidi() extends Telescope {
  //---------------------------------------------------------------------------
  //https://es.wikipedia.org/wiki/Pic_du_Midi_de_Bigorre
  val name = "PIC_DU_MIDI"
  val telescopePattern =
    """.*t1m.*""".stripMargin.replaceAll("\n","").r
  val pathPattern = telescopePattern
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed abstract class TelescopeSimpleNameFamily(subType: String) extends Telescope {
  //---------------------------------------------------------------------------
  val name = subType.toUpperCase
  val telescopePattern = s""".*$subType.*""".stripMargin.replaceAll("\n","").r
  val pathPattern = telescopePattern
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class TelescopeNTT()  extends TelescopeSimpleNameFamily("ntt")
sealed case class TelescopeLiverpool()  extends TelescopeSimpleNameFamily("liverpool")
sealed case class TelescopeIAC_80()  extends TelescopeSimpleNameFamily("iac80")
sealed case class TelescopeNot()  extends TelescopeSimpleNameFamily("not")
sealed case class TelescopeWHT()  extends TelescopeSimpleNameFamily("wht")
sealed case class TelescopeTng()  extends TelescopeSimpleNameFamily("tng")
sealed case class TelescopeKot()  extends TelescopeSimpleNameFamily("kot")
sealed case class TelescopeLot()  extends TelescopeSimpleNameFamily("lot")
//=============================================================================
sealed case class Javalambre() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "OAJ-T80"
  val telescopePattern =
    """.*oaj.*""".stripMargin.replaceAll("\n","").r
  val pathPattern =  s""".*javalambre.*""".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class EsoMPI_2_2() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "ESO_MPI_2_2"
  val telescopePattern =
    """mpi.?2.?2*""".stripMargin.replaceAll("\n","").r
  val pathPattern =  PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class Soar() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "SOAR"
  val telescopePattern =
    """soar.*""".stripMargin.replaceAll("\n","").r
  val pathPattern =  PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class VLT() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "VLT"
  val telescopePattern =
    """vlt.*""".stripMargin.replaceAll("\n","").r
  val pathPattern =  PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
//Canada France Hawaii Telescope
sealed case class CFHT() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "CFHT"
  val telescopePattern =
    """cfht.*""".stripMargin.replaceAll("\n","").r
  val pathPattern =  PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}

//=============================================================================
//Cerro Tololo Inter-American Observatory
sealed case class CTIO_90() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "CTIO_90"
  val telescopePattern =
    """ctio.?0.*""".stripMargin.replaceAll("\n","").r
  val pathPattern =  PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class CTIO_1() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "CTIO_1"
  val telescopePattern =
    """ct1m.*""".stripMargin.replaceAll("\n","").r
  val pathPattern =  PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
sealed case class CTIO_4() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "CTIO_4"
  val telescopePattern =
    """ctio-4.*""".stripMargin.replaceAll("\n","").r
  val pathPattern =  PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
//Lowell Discovery Telescope
sealed case class Loneos() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "LONEOS"
  val telescopePattern =
    """lowell observatory""".stripMargin.replaceAll("\n","").r
  val pathPattern =  PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
//Mauna Kea-UH/Tholen NEO Follow-Up (2.24-m)
sealed case class MaunaKeaUH() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "MAUNA_KEA_UH"
  val telescopePattern = """2\.2m uh""".stripMargin.replaceAll("\n","").r
  val pathPattern =  PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
//SARA Observatory, Kitt Peak
sealed case class SARA() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "SARA"
  val telescopePattern = """sara-orm""".stripMargin.replaceAll("\n","").r
  val pathPattern = """.*sara.*""".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
//Odessa-Mayaki
sealed case class Odessa() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "ODESSA"
  val telescopePattern =
    """omt-800""".stripMargin.replaceAll("\n","").r
  val pathPattern =  PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
//Cordoba-Bosque Alegre
sealed case class BosqueAlegre() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "BOSQUE_ALEGRE"
  val telescopePattern =
    """.* eaba.* |
      |bosque.*alegre.*""".stripMargin.replaceAll("\n","").r
  val pathPattern =  PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
//Palomar Mountain
sealed case class PalomarMountain() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "PALOMAR_MOUNTAIN"
  val telescopePattern =
    """palomar.*""".stripMargin.replaceAll("\n","").r
  val pathPattern =  PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
//SPECULOOS-South Observatory, Paranal
sealed case class Speculoos() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "Speculoos"
  val telescopePattern =
    """.*speculoos.*""".stripMargin.replaceAll("\n","").r
  val pathPattern = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
//Las Cumbres Observatory Global Telescope (LCOGT)
//https://lco.global/observatory/status/
sealed abstract class LCO(subType: String,id:String) extends Telescope {
  //---------------------------------------------------------------------------
  val name = "lco_" + subType.toLowerCase
  val telescopePattern = s"""$id""".stripMargin.replaceAll("\n", "").r
  val pathPattern =PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
sealed case class LCO_siding_spring_1() extends LCO("siding_spring_1","0m4-03")
sealed case class LCO_haleakala_3() extends LCO("haleakala_3","0m4-04")
sealed case class LCO_siding_spring_2() extends LCO("siding_spring_2","0m4-05")
sealed case class LCO_haleakala_2() extends LCO("haleakala_2","0m4-06")
sealed case class LCO_sutherland_aqawan_a() extends LCO("sutherland_aqawan_a","0m4-07")
sealed case class LCO_cerro_tololo_aqawan_a() extends LCO("cerro_tololo_aqawan_a","0m4-09")
sealed case class LCO_tenerife_aqawan_a_2() extends LCO("tenerife_aqawan_a_2","0m4-10")
sealed case class LCO_mcdonald_1() extends LCO("mcdonald_1","0m4-11")
sealed case class LCO_cerro_tololo_aqawan_b() extends LCO("cerro_tololo_aqawan_b","0m4-12")
sealed case class LCO_tenerife_aqawan_a_1() extends LCO("tenerife_aqawan_a_1","0m4-14")
sealed case class LCO_tenerife_b() extends LCO("tenerife_b","1m0-01")
sealed case class LCO_cerro_pachon() extends LCO("cerro_pachon","1m0-02")
sealed case class LCO_siding_spring_b() extends LCO("siding_spring_b","1m0-03")
sealed case class LCO_cerro_tololo_c() extends LCO("cerro_tololo_c","1m0-04")
sealed case class LCO_cerro_tololo_a() extends LCO("cerro_tololo_a","1m0-05")
sealed case class LCO_mcdonald_b() extends LCO("mcdonald_b","1m0-06")
sealed case class LCO_mcdonald() extends LCO("mcdonald","1m0-08")
sealed case class LCO_cerro_tololo_b() extends LCO("cerro_tololo_b","1m0-09")
sealed case class LCO_sutherland_a() extends LCO("sutherland_a","1m0-10")
sealed case class LCO_siding_spring_a() extends LCO("siding_spring_a","1m0-11")
sealed case class LCO_sutherland_c() extends LCO("sutherland_c","1m0-12")
sealed case class LCO_sutherland_b() extends LCO("sutherland_b","1m0-13")
sealed case class LCO_tenerife_a() extends LCO("tenerife_a","1m0-14")

//-----------------------------------------------------------------------------
// Gran Telescopio Canarias, Roque de los Muchachos
sealed case class GTC() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "GTC"
  val telescopePattern =
    """gtc.*""".stripMargin.replaceAll("\n","").r
  val pathPattern = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}

//=============================================================================
sealed case class Botorrita() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "Botorrita"
  val telescopePattern = PATTERN_ALWAYS_FAIL
  val pathPattern =  """.*botorrita.*""".stripMargin.replaceAll("\n","").r

  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class Linhaceira() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "Linhaceira"
  val telescopePattern = PATTERN_ALWAYS_FAIL
  val pathPattern =  """.*linhaceira.*""".stripMargin.replaceAll("\n","").r

  //---------------------------------------------------------------------------
}
//=============================================================================
//OBSERVING STATION: Podkoren, Slovenia, NW Slovenia
//  Longitude: 13.76138888
//  Latitude: 46.49805555
//  Altitude (m): 920.0
sealed case class Podkoren() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "Podkoren"
  val telescopePattern = PATTERN_ALWAYS_FAIL
  val pathPattern =  """.*podkoren.*""".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class ESO_VST() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "ESO_VST"
  val telescopePattern =
    """.*eso-vst.*""".stripMargin.replaceAll("\n","").r
  val pathPattern =  PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}

//=============================================================================
//INT (La palma)
sealed case class IntLaPalma() extends Telescope {
  //---------------------------------------------------------------------------
  val name = "int"
  val telescopePattern =
    """int|
      |ing .*""".stripMargin.replaceAll("\n", "").r
  val pathPattern = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
 //=============================================================================
//=============================================================================
//End of file Telescope.scala
//=============================================================================
