/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  19/Feb/2021
  * Time:  11h:07m
  * Description: None
  */
//=============================================================================
package com.common.image.telescope
//=============================================================================
//=============================================================================
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.simpleFits.SimpleFits.{KEY_OBSERVATORY, KEY_ORIGIN, KEY_TELESCOPE}
import com.common.logger.MyLogger
//=============================================================================
import java.io.{File, PrintWriter}
//=============================================================================
object TelescopeMatcher extends MyLogger {
  //---------------------------------------------------------------------------
  final val FIELD_DIVIDER = "\t"
  //---------------------------------------------------------------------------
  private val telescopeSeq : Array[Telescope] = Array (

      TelescopeIgnored()

    , OSN_90()
    , OSN_1_5()

    , CAHA_1_23()
    , CAHA_3_50()
    , CAHA_2_2()

    , LaHita_77()

    , ASH2()
    , ASH()

    , KMTNet()
    , TCS()

    , TelescopeLiverpool()
    , TelescopeNTT()
    , TelescopeIAC_80()
    , TelescopeNot()
    , TelescopeWHT()

    , TelescopeTng()
    , TelescopeKot()
    , TelescopeLot()

    , Casleo_215()

    , Javalambre()
    , EsoMPI_2_2()
    , Soar()

    , VLT()

    , CFHT()

    , CTIO_90()
    , CTIO_1()
    , CTIO_4()

    , Loneos()
    , MaunaKeaUH()
    , SARA()
    , Odessa()

    , BosqueAlegre()

    , PalomarMountain()

    , Speculoos()

    , LCO_siding_spring_1()
    , LCO_haleakala_3()
    , LCO_siding_spring_2()
    , LCO_haleakala_2()
    , LCO_sutherland_aqawan_a()
    , LCO_cerro_tololo_aqawan_a()
    , LCO_tenerife_aqawan_a_2()
    , LCO_mcdonald_1()
    , LCO_cerro_tololo_aqawan_b()
    , LCO_tenerife_aqawan_a_1()
    , LCO_tenerife_b()
    , LCO_cerro_pachon()
    , LCO_siding_spring_b()
    , LCO_cerro_tololo_c()
    , LCO_cerro_tololo_a()
    , LCO_mcdonald_b()
    , LCO_mcdonald()
    , LCO_cerro_tololo_b()
    , LCO_sutherland_a()
    , LCO_siding_spring_a()
    , LCO_sutherland_c()
    , LCO_sutherland_b()
    , LCO_tenerife_a()

    , GTC()

    , Botorrita()
    , Linhaceira()

    , Podkoren()

    , ESO_VST()

    , IntLaPalma()

    , PicduMidi() //always the last one
  )
  //---------------------------------------------------------------------------
  private def tryToMatchFirstTelescope(telescopeValue: String, path: String) : Telescope =
    tryToMatchAllTelescope(telescopeValue, path).head
  //---------------------------------------------------------------------------
  //match and sort by priority
  private def tryToMatchAllTelescope(telescopeValue: String, path: String) : Array[Telescope] = {
    val r = telescopeSeq flatMap { telescope=>
      val matchResult = telescope.tryToMatch(telescopeValue, path)
      if (matchResult._1) Some((telescope,matchResult._2)) else None}
    if (r.isEmpty) Array(TelescopeUnknown())
    else r.sortWith(_._2 > _._2) map ( _._1)
  }
  //---------------------------------------------------------------------------
  //(telescope, path)
  def tryToMatch(seq: Array[(String,String)]
                 , matchLog: Option[String] = None
                 , unknownMatchLog: Option[String] = None
                 , multipleMatchLog: Option[String] = None
                 , allowMultipleMatch : Boolean = false) : Array[Telescope] = {
    var matchedCount : Long = 0
    var unknownMatchCount : Long = 0
    var multipleMatchCount : Long = 0
    val logFileMatch = if (matchLog.isDefined) new PrintWriter(new File(matchLog.get)) else null
    val logFileUnknown = if (unknownMatchLog.isDefined) new PrintWriter(new File(unknownMatchLog.get)) else null
    val logFileMultipleMatch = if (allowMultipleMatch && multipleMatchLog.isDefined) new PrintWriter(new File(multipleMatchLog.get)) else null
    if (matchLog.isDefined) {
      val s = s"#om_telescope${FIELD_DIVIDER}telescope${FIELD_DIVIDER}path\n"
      logFileMatch.write(s)
      logFileUnknown.write(s)
      if (allowMultipleMatch) logFileMultipleMatch.write(s)
    }

    //get telescope
    val r = seq.map { case (telescopeValue, path) =>
      matchedCount += 1
      val telescope =
        if (allowMultipleMatch){
          val telescopeSeq = TelescopeMatcher.tryToMatchAllTelescope(telescopeValue, path)
          if (telescopeSeq.length > 1) TelescopeMultipleMatch((telescopeSeq map (_.name)) mkString("{",",","}"))
          else telescopeSeq.head
        }
        else TelescopeMatcher.tryToMatchFirstTelescope(telescopeValue, path)

      //save in logs
      if (matchLog.isDefined) {
        val logLine = s"${telescope.name}$FIELD_DIVIDER$telescopeValue$FIELD_DIVIDER$path\n"
        logFileMatch.write(logLine)
        if (matchLog.isDefined) {
          if (telescope.isInstanceOf[TelescopeUnknown]) {
            logFileUnknown.write(logLine)
            unknownMatchCount += 1
          }
          else {
            if (telescope.isInstanceOf[TelescopeMultipleMatch]) {
              logFileMultipleMatch.write(logLine)
              multipleMatchCount += 1
            }
          }
        }
      }
      telescope
    }
    if (matchLog.isDefined) {
      val total = matchedCount.toFloat
      info(s"Matched telescope count          : $matchedCount -> ${(matchedCount/total) * 100}%")
      info(s"Unknown telescope count          : $unknownMatchCount -> ${(unknownMatchCount/total) * 100}%")
      if (allowMultipleMatch) info(s"Multiple match telescope count   : $multipleMatchCount -> ${(multipleMatchCount/total) * 100}%")

      if (logFileMatch != null) logFileMatch.close()
      if (logFileUnknown != null) logFileUnknown.close()
      if (allowMultipleMatch && (logFileMultipleMatch != null)) logFileMultipleMatch.close()
    }
    r
  }
  //---------------------------------------------------------------------------
  def getTelescope(fits: SimpleFits) : String = {
    var r = getTelescope(fits.getStringValueOrEmptyNoQuotation(KEY_TELESCOPE), fits.fileName)
    if (r == Telescope.UNKNOWN_TELESCOPE) {
      r = getTelescope(fits.getStringValueOrEmptyNoQuotation(KEY_OBSERVATORY), fits.fileName)
      if (r == Telescope.UNKNOWN_TELESCOPE)
        r = getTelescope(fits.getStringValueOrEmptyNoQuotation(KEY_ORIGIN), fits.fileName)
    }
    r
  }
  //---------------------------------------------------------------------------
  def getTelescope(t:String, path:String) =
    TelescopeMatcher.tryToMatch( Array((t, path))).head.name
  //---------------------------------------------------------------------------
  def getTelescope(t:String) =
    TelescopeMatcher.tryToMatch( Array((t, "")))
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file TelescopeMatcher.scala
//=============================================================================
