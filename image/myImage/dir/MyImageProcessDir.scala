/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  04/Jul/2024
 * Time:  16h:40m
 * Description: None
 */
package com.common.image.myImage.dir
//=============================================================================
import com.common.configuration.MyConf
import com.common.dataType.pixelDataType.PixelDataType
import com.common.dataType.pixelDataType.PixelDataType.PIXEL_DATA_TYPE
import com.common.fits.simpleFits.SimpleFits.{error, info}
import com.common.geometry.matrix.matrix2D.Matrix2D
import com.common.geometry.segment.segment2D.Segment2D
import com.common.hardware.cpu.CPU
import com.common.image.astrometry.GaiaCatalog
import com.common.image.estimator.centroid.Centroid
import com.common.image.myImage.MyImage
import com.common.image.telescope.Telescope
import com.common.stat.StatDescriptive
import com.common.util.file.MyFile
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
import com.common.util.util.Util
//=============================================================================
import java.awt.Font
//=============================================================================
//=============================================================================
object MyImageDir {
  //---------------------------------------------------------------------------
  private final val SEXTRACTOR_BACKGROUND_SCRIPT_PATH = MyConf.c.getString("Astromatic.sextracor_background.scriptPath")
  private final val SEXTRACTOR_BACKGROUND_SCRIPT_NAME = MyConf.c.getString("Astromatic.sextracor_background.scriptName")
  //---------------------------------------------------------------------------
  def detectSourceSeq(inputDirectory: String
                      , outputDirectory: String
                      , centroidFallbackAlgorithmSeq: List[String]
                      , roundSources: Int
                      , gaiaSourceMinAllowedDistance: Option[Double] = None
                      , noiseTideMap: Option[Map[String, Double]] = None
                      , synthetizeImageNameMap: Option[Map[String, String]] = None
                      , synthetizeDir: Option[String] = None
                      , validateSourceSeq: Option[(String,Segment2D) => Boolean] = None
                      , resetOutputDir: Boolean = true) = {

    //-------------------------------------------------------------------------
    def filterSourceSeq(img: MyImage
                        , sourceSeq: Array[Segment2D]
                        , sourceMinPixCount: Int
                        , noiseTide: Double): Array[Segment2D] = {

      var remainSourceSeq =
        if (roundSources == 0) sourceSeq
        else
          sourceSeq.flatMap (_.round(noiseTide, roundSources)).filter { source =>
            (source.getPixCount() >= sourceMinPixCount
              && !source.getData().forall(_ == PixelDataType.PIXEL_ZERO_VALUE))
          }

      //calculate precise centroid
      Centroid.calculateCentroiWithFallBack(
        remainSourceSeq
        , img
        , centroidFallbackAlgorithmSeq
        , verbose = false)

      //set centroid ra dec
      img.setCentroidRaDec(remainSourceSeq)

      if (validateSourceSeq.isDefined)
        remainSourceSeq = remainSourceSeq.filter(source => validateSourceSeq.get(img.name, source))

      if (gaiaSourceMinAllowedDistance.isDefined) {

        val knnTree = GaiaCatalog.getCatalogAsKnnTree(img)
        val relevantPos = img.getRaDecRelevantPos()

        remainSourceSeq = remainSourceSeq.filter { source =>
          val r = knnTree.getNearestNeighbors(source.getCentroidRaDec().toArray(), 1)
          if (r.head.getDistance < gaiaSourceMinAllowedDistance.get) false
          else true
        }
      }

      remainSourceSeq
    }
    //-------------------------------------------------------------------------
    class SourceDetection(imageNameSeq: Array[String])
      extends ParallelTask[String](
        imageNameSeq
        , CPU.getCoreCount()
        , isItemProcessingThreadSafe = true
        , randomStartMaxMsWait = 100
        , verbose = true
        , message = Some("detecting sources")) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(imageName: String): Unit = {
        val img = MyImage(imageName)
        val csvFileName = outputDirectory + "/" + Path.getOnlyFilenameNoExtension(imageName) + ".tsv"
        val rawImageName = Path.getOnlyFilenameNoExtension(imageName)

        val c = Telescope.getConfigurationFile(img.getSimpleFits())
        val noiseTide =
          if (noiseTideMap.isDefined && noiseTideMap.get.contains(rawImageName))
            noiseTideMap.get(rawImageName)
          else {
            val sigmaMultiplier = c.getFloat("SourceDetection.image.sigmaMultiplier")
            val background = img.getBackground()
            val backgroundRMS = img.getBackgroundRMS()
            background + (backgroundRMS * sigmaMultiplier)
          }

        val sourceMinPixCount = c.getInt("SourceDetection.source.minPixCount")
        val sourceMaxPixCount = c.getInt("SourceDetection.source.maxPixCount")
        val rawName = img.getRawName()

        //no mask to avoid miss sources in the borders
        val sourceSeq = filterSourceSeq(
          img
          , img.findSourceSeq(noiseTide
                            , sourceSizeRestriction = Some((sourceMinPixCount, sourceMaxPixCount)))
            .toSegment2D_seq()
          , sourceMinPixCount
          , noiseTide)

        //synthetize
        if (synthetizeDir.isDefined) {
          warning(s"$rawName -> noise tide:'$noiseTide'")
          MyImage.synthesizeImage(s"${synthetizeDir.get}/$rawName"
            , sourceSeq
            , img.getDimension()
            , font = Some(new Font(Font.MONOSPACED, Font.PLAIN, 8))
            , inverYAxis = true)
        }

        if (synthetizeImageNameMap.isDefined) {
          if (synthetizeImageNameMap.get.contains(rawName)) {
            MyImage.synthesizeImage(s"${synthetizeImageNameMap.get(rawName)}/$rawName"
              , sourceSeq
              , img.getDimension()
              , font = Some(new Font(Font.MONOSPACED, Font.PLAIN, 10))
              , inverYAxis = false)
          }
        }
        Segment2D.saveAsCsvWithData(sourceSeq
                                   , csvFileName)
      }
      //-------------------------------------------------------------------------
    }
    info(s"Detecting the sources of the images at directory:'$inputDirectory'")
    if (resetOutputDir) Path.resetDirectory(outputDirectory)
    if (synthetizeDir.isDefined) Path.resetDirectory(synthetizeDir.get)

    val imageNameSeq = Path.getSortedFileList(inputDirectory
      , MyConf.c.getStringSeq("Common.fitsFileExtension"))
      .map(_.getAbsolutePath)
      .sortWith(_ < _)


    new SourceDetection(imageNameSeq.toArray)
  }

  //---------------------------------------------------------------------------
  def stack(inputDirectory: String
            , outputImageName: String) = {

    info(s"Stacking the images at directory:'$inputDirectory'")
    MyFile.deleteFileIfExist(outputImageName)

    //get the sequence of images to processu
    val imageNameSeq = Path.getSortedFileList(inputDirectory
      , MyConf.c.getStringSeq("Common.fitsFileExtension"))
      .map(_.getAbsolutePath)
      .sortWith(_ < _)

    val firstImage = MyImage(imageNameSeq.head)
    var stackedPixelSeq = firstImage.data
    val maxAllowedPixValue = firstImage.maxValidPixelValue
    imageNameSeq
      .drop(1)
      .zipWithIndex
      .foreach { case (imageName, i) =>
        info(s"Stacking image ${i + 2}/${imageNameSeq.length}")
        val img = MyImage(imageName)
        stackedPixelSeq = (stackedPixelSeq zip img.data).map { case (pixelStacked, newPixel) =>
          Math.min(maxAllowedPixValue, pixelStacked + newPixel)
        }
      }
    info(s"Saving stacked image:'$outputImageName'")
    val result = MyImage(Matrix2D(firstImage.xMax
      , firstImage.yMax
      , stackedPixelSeq)
      , firstImage.getSimpleFits())
    result.saveAsFits(outputImageName)
    result
  }
  //---------------------------------------------------------------------------
  def getMedian(inputDirectory: String
                , outputImageName: String): MyImage = {

    info(s"Calculating the median of images at directory:'$inputDirectory'")
    MyFile.deleteFileIfExist(outputImageName)

    val imageNameSeq = Path.getSortedFileList(inputDirectory
      , MyConf.c.getStringSeq("Common.fitsFileExtension"))
      .map(_.getAbsolutePath)
      .sortWith(_ < _)
    if (imageNameSeq.length < 2) {
      error(s"Not enough images:'${imageNameSeq.length}'"); return null
    }

    val imageSeq = MyImage(imageNameSeq.toArray, checkWcs = false)
    val firstImage = imageSeq.head

    val pixCount = imageSeq.head.getPixCount
    val medianPixSeq = for (i <- 0 until pixCount) yield {
      val pixSeq = for (img <- imageSeq) yield img.data(i)
      StatDescriptive.getWithDouble(pixSeq).median
    }
    val medianImage = MyImage(Matrix2D(firstImage.xMax
      , firstImage.yMax
      , medianPixSeq.toArray)
      , firstImage.getSimpleFits())

    info(s"Saving median image:'$outputImageName'")
    medianImage.saveAsFits(outputImageName)
    medianImage
  }
  //---------------------------------------------------------------------------
  private def parallelProcessing(inputDir: String
                                 , outputDir: String
                                 , imageProcessor: MyImage => MyImage
                                 , resetOutputDir: Boolean
                                 , processMessage: String
                                ): Unit = {
    info(s"Processing images at directory:'$inputDir' with message: $processMessage")
    val oDir = if (resetOutputDir) Path.resetDirectory(outputDir) else outputDir

    val imageNameSeq = Path.getSortedFileList(inputDir
      , MyConf.c.getStringSeq("Common.fitsFileExtension"))
      .map(_.getAbsolutePath)
      .sorted
    //--------------------------------------------------------------------------
    class ImageProcessor(imageNameSeq: Array[String])
      extends ParallelTask[String](
        imageNameSeq
        , CPU.getCoreCount()
        , isItemProcessingThreadSafe = true
        , randomStartMaxMsWait = 100
        , verbose = true
        , message = Some(processMessage)) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(imageName: String): Unit = {
        val img = MyImage(imageName)
        val processedImage = imageProcessor(img)
        processedImage.saveAsFits(s"$oDir/${img.getRawName()}")
      }
      //-----------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    new ImageProcessor(imageNameSeq.toArray)
  }
  //---------------------------------------------------------------------------
  def subtractImage(inputDir: String
                    , imageToBeSubtracted: MyImage
                    , subtractedDir: String
                    , resetSubtractedDir: Boolean = true
                    , inverseSubtraction: Boolean = false): Unit = {
    val imageProcessor: MyImage => MyImage = img => {
      val maxAllowedPixValue = img.maxValidPixelValue
      val minAllowedPixValue = img.minValidPixelValue
      val subtractedPixelSeq =
        if (inverseSubtraction)
          (img.data zip imageToBeSubtracted.data).map { case (leftPix, rightPix) =>
            Math.min(maxAllowedPixValue, Math.max(minAllowedPixValue, rightPix - leftPix))}
        else
          (img.data zip imageToBeSubtracted.data).map { case (leftPix, rightPix) =>
            Math.min(maxAllowedPixValue, Math.max(minAllowedPixValue, leftPix - rightPix))}

      MyImage(Matrix2D(img.xMax
        , img.yMax
        , subtractedPixelSeq)
        , img.getSimpleFits())
    }

    parallelProcessing(inputDir
      , subtractedDir
      , imageProcessor
      , resetSubtractedDir
      , "subtracting image")
  }
  //---------------------------------------------------------------------------
  def subtractBackgroundFromDirectory(inputDir: String
                                      , outputDir: String
                                      , resetOutputDir: Boolean = true): Unit = {
    val imageProcessor: MyImage => MyImage = img => {
      val background = img.getBackground()
      val minAllowedPixValue = img.minValidPixelValue
      val subtractedPixelSeq = img.data.map(pix => Math.max(minAllowedPixValue, pix - background))
      MyImage(Matrix2D(img.xMax
        , img.yMax
        , subtractedPixelSeq)
        , img.getSimpleFits())
    }
    parallelProcessing(
      inputDir
      , outputDir
      , imageProcessor
      , resetOutputDir
      , "subtracting background from image")
  }
  //-----------------------------------------------------------------------
  def subtractValueFromMap(inputDir: String
                           , outputDir: String
                           , map: Map[String, PIXEL_DATA_TYPE]
                           , resetOutputDir: Boolean = true): Unit = {
    info(s"Subtracting background of all images of the images at directory:'$inputDir'")
    val oDir = if (resetOutputDir) Path.resetDirectory(outputDir) else outputDir

    val imageNameSeq = Path.getSortedFileList(inputDir
      , MyConf.c.getStringSeq("Common.fitsFileExtension"))
      .map(_.getAbsolutePath)
      .sortWith(_ < _)

    //-----------------------------------------------------------------------
    class ImageSubtract(imageNameSeq: Array[String])
      extends ParallelTask[String](
        imageNameSeq
        , CPU.getCoreCount()
        , isItemProcessingThreadSafe = true
        , randomStartMaxMsWait = 100
        , verbose = true
        , message = Some("subtracting background from image")) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(imageName: String): Unit = {
        val img = MyImage(imageName)
        val minAllowedPixValue = img.minValidPixelValue
        val subtractedPixelSeq = img.data.map { case pix =>
          Math.max(minAllowedPixValue, pix - map(img.getRawName()))
        }
        MyImage(Matrix2D(img.xMax
          , img.yMax
          , subtractedPixelSeq)
          , img.getSimpleFits())
          .saveAsFits(oDir + "/" + img.getRawName())
      }
    }
    if (!imageNameSeq.isEmpty)
      new ImageSubtract(imageNameSeq.toArray)
  }
  //---------------------------------------------------------------------------
  def subtractImageMap(inputDir: String
                       , outputDir: String
                       , resetOutputDir: Boolean = true): Unit = {
    info(s"Subtracting background of all images of the images at directory:'$inputDir'")
    val oDir = if (resetOutputDir) Path.resetDirectory(outputDir) else outputDir

    val imageNameSeq = Path.getSortedFileList(inputDir
      , MyConf.c.getStringSeq("Common.fitsFileExtension"))
      .map(_.getAbsolutePath)
      .sortWith(_ < _)
    //-----------------------------------------------------------------------
    class ImageSubtract(imageNameSeq: Array[String])
      extends ParallelTask[String](
        imageNameSeq
        , CPU.getCoreCount()
        , isItemProcessingThreadSafe = true
        , randomStartMaxMsWait = 100
        , verbose = true
        , message = Some("subtracting background map from image")) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(imageName: String): Unit = {
        val img = MyImage(imageName)

        //calculate the background map
        val prefix = if (imageName.startsWith("/")) "" else Path.getCurrentPath()
        val scriptPath = if(SEXTRACTOR_BACKGROUND_SCRIPT_PATH.startsWith("/")) SEXTRACTOR_BACKGROUND_SCRIPT_PATH else s"${Path.getCurrentPath()}/$SEXTRACTOR_BACKGROUND_SCRIPT_PATH"
        val backgroundFilename = s"$scriptPath/background_${Path.getOnlyFilenameNoExtension(imageName)}.fits"
        val command = s"$scriptPath/$SEXTRACTOR_BACKGROUND_SCRIPT_NAME $scriptPath '$prefix/$imageName' '$backgroundFilename'"
        Util.runShellSimple(command)

        //subtract the background map
        val imgBackground = MyImage(backgroundFilename)
        val minAllowedPixValue = img.minValidPixelValue
        val subtractedPixelSeq = (img.data zip imgBackground.data) map { case (pix, pixBackground) =>
          Math.max(minAllowedPixValue, pix - pixBackground)
        }
        MyImage(Matrix2D(img.xMax
          , img.yMax
          , subtractedPixelSeq)
          , img.getSimpleFits())
          .saveAsFits(oDir + "/" + img.getRawName())

        //delete the background file
        MyFile.deleteFile(backgroundFilename)
      }
    }
    if (!imageNameSeq.isEmpty)
      new ImageSubtract(imageNameSeq.toArray)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MyImageProcessDir.scala
//=============================================================================