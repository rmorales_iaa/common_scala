/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  04/may/2019
  * Time:  21h:35m
  * Description: None
  */
//=============================================================================
package com.common.image.myImage
//=============================================================================
import com.common.image.telescope.TelescopeMatcher

import java.awt.image.BufferedImage
import java.awt.{Color, Font}
import java.io.{File, FileOutputStream}
import java.nio.file.{Files, Paths}
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import javax.imageio.ImageIO
import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}
//=============================================================================
import BuildInfo.BuildInfo
import com.common.configuration.MyConf
import com.common.coordinate.conversion.Conversion
import com.common.dataType.pixelDataType.PixelDataType
import com.common.dataType.pixelDataType.PixelDataType.{PIXEL_DATA_TYPE, PIXEL_INVALID_VALUE, PIXEL_ZERO_VALUE}
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.simpleFits.SimpleFits._
import com.common.fits.wcs.WCS
import com.common.geometry.affineTransformation.AffineTransformation2D
import com.common.geometry.affineTransformation.AffineTransformation2D.AFFINE_TRANSFORMATION_2D_UNIT
import com.common.geometry.circle.CircleFloat
import com.common.geometry.grid.GridCircleOverlap
import com.common.geometry.matrix.matrix2D.Matrix2D
import com.common.geometry.point.{Point2D, Point2D_Double, Point2D_Float}
import com.common.geometry.rectangle.{Rectangle, RectangleDouble}
import com.common.geometry.segment.Segment1D
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.estimator.background.{Sextractor, Simple}
import com.common.image.mask.Mask
import com.common.image.region.Region
import com.common.image.telescope.Telescope
import com.common.image.util.UtilImage
import com.common.plot.bmpImage.BmpImagePlot
import com.common.stat.{SimpleStatDescriptive, StatDescriptive}
import com.common.util.file.MyFile
import com.common.util.path.Path
import com.common.util.time.Time
//=============================================================================
//=============================================================================
object MyImage {
  //---------------------------------------------------------------------------
  //the value fwhm of a fitted source must be around n-times the affine_transformation gaussian fit FWHM
  //It it is not true, the fit is discarded and alternative fit is calculated
  final val SIMPLE_FWHM_DISCARD_CRITERIA = 3
  //---------------------------------------------------------------------------
  private val localDateTimeFormatterWithMillis = DateTimeFormatter.ofPattern("yyyy_MM_dd'T'HH_mm_ss_SSS")
  //---------------------------------------------------------------------------
  def apply(img: MyImage, o : Point2D) = new MyImage(img.xMax, img.yMax, img.data, o, img.name, userFits = Some(img.getSimpleFits()))
  //---------------------------------------------------------------------------
  def apply(m: Matrix2D, fits: SimpleFits) = new MyImage(m.xMax, m.yMax, m.data, m.offset, m.name, userFits = Some(fits))
  //---------------------------------------------------------------------------
  def apply(f: File) : MyImage = MyImage(f.getAbsolutePath)
  //---------------------------------------------------------------------------
  def apply(name: String, o : Point2D = Point2D.POINT_ZERO) : MyImage = {
    if (!MyFile.fileExist(name)) {
      error(s"Image: '$name' does not exist")
      return null
    }
    val byteSeq = Files.readAllBytes(Paths.get(name))
    val fits = SimpleFits(byteSeq)
    if(!fits.isValid()) {
      error(s"Image: '$name' .Invalid FITS")
      return null
    }

    val pixelByteSeq = byteSeq.drop(fits.imageDataAbsoluteOffset).take(fits.getImageDataByteSize.toInt)
    val pixelSeq = PixelDataType.getPixelSeq(pixelByteSeq, fits.bitPix)
    val scaledPixSeq = PixelDataType.scalePixelSeq(pixelSeq, fits.bScale, fits.bZero)
    fits.fileName = name
    new MyImage(
      fits.width.toInt
      , fits.height.toInt
      , scaledPixSeq
      , o
      , name = name
      , userFits = Some(fits)
    )
  }

  //---------------------------------------------------------------------------
  def apply(pixelByteSeq: Array[Byte]
            , naxis_1: Int
            , naxis_2: Int
            , bitPix: Int
            , bScale: Double
            , bZero: Double): MyImage = {

    val pixelSeq = PixelDataType.getPixelSeq(pixelByteSeq, bitPix)
    val scaledPixSeq = PixelDataType.scalePixelSeq(pixelSeq, bScale, bZero)
    val fits = SimpleFits()

    //build minimal FITS header
    fits.updateRecord(KEY_SIMPLE, "T")
    fits.updateRecord(KEY_BIT_PIX,bitPix.toString)
    fits.updateRecord(KEY_NAXIS, "2")
    fits.updateRecord(KEY_NAXIS_1, naxis_1.toString)
    fits.updateRecord(KEY_NAXIS_2, naxis_2.toString)
    fits.updateRecord(KEY_BSCALE,bScale.toString)
    fits.updateRecord(KEY_BZERO,bZero.toString)

    fits.bitPix = bitPix.toByte
    fits.bScale = bScale
    fits.bZero = bZero

    new MyImage(
      naxis_1
      , naxis_2: Int
      , scaledPixSeq
      , Point2D.POINT_ZERO
      , name = "none"
      , userFits = Some(fits)
    )
  }
  //---------------------------------------------------------------------------
  def apply(seq: Array[String], checkWcs : Boolean) : Array[MyImage] =  {
    seq flatMap { s =>
      var img: MyImage = null
      Try{ img = MyImage(s, Point2D.POINT_ZERO) }
      match {
        case Success( _ ) =>
          if (img == null || !img.isValid) {
            error(s"Error loading FITS file '$s'. Ignoring it")
            None
          }
          else
            if (checkWcs && !img.getSimpleFits.hasWcs()) {
              error(s"The FITS file '$s' has no wcs")
              None
            }
            else Some(img)

        case Failure(e) =>
          MyImage(s, Point2D.POINT_ZERO)
          error(e.getMessage + s". Error loading file '$s'."  + e.toString)
          None
      }
    }
  }
  //---------------------------------------------------------------------------
  def getStringID(s: String) = {
    val name =  MyFile.getFileNameNoPathNoExtension(Path.getOnlyFilename(s))
    name.split("-")(0)
  }
  //---------------------------------------------------------------------------
  def getIdSeq(dir: String, extension : String = ".fit")  : Array[String] =
    (Path.getSortedFileList(Path.ensureEndWithFileSeparator(dir),extension) map
      {f=> getStringID(Path.getOnlyFilenameNoExtension(f.getAbsolutePath))}).distinct.toArray
  //---------------------------------------------------------------------------
  def getMosaicID(s: String) = getStringID(s)
  //---------------------------------------------------------------------------
  def joinImageFromPartition(imgSeq: Array[Array[MyImage]]) = {
    val img = imgSeq.head.head
    Matrix2D.joinImageFromPartition(imgSeq.asInstanceOf[Array[Array[Matrix2D]]]
      , img.xMax
      , img.yMax)
  }
  //---------------------------------------------------------------------------
  def createFitsHeader(name: String
                       , bitPix: Int
                       , bScale: Double
                       , bZero: Double
                       , xAxis: Int
                       , yAxis: Int
                       , recordSeq: IndexedSeq[(String, String)]
                       , commentSeq: scala.collection.mutable.Map[String, String] = scala.collection.mutable.Map[String, String]()) = {
    //-------------------------------------------------------------------------
    def fixedFormatString(k: String, v: String, comment: String) =
      (k.padTo(8, ' ') + "= " + f"$v%20s" + (if (comment.isEmpty) "" else " /" + comment)).padTo(80, ' ')
    //-------------------------------------------------------------------------
    def fixedFormatValue(k: String, v: Int, comment: String) = fixedFormatString(k, f"$v%20d", comment)
    //-------------------------------------------------------------------------
    val recordCount = (7 + 1) + recordSeq.size //fixed header + recordSeq
    val maxRecordPerHeader = HDU_BLOCK_BYTE_SIZE / HDU_RECORD_BYTE_SIZE
    val headerCount = recordCount / maxRecordPerHeader + (if ((recordCount % maxRecordPerHeader.toFloat) > 0.0f) 1 else 0)
    val totalRecordCount = headerCount * maxRecordPerHeader

    val primaryHDU = Array.fill(totalRecordCount) (" " * HDU_RECORD_BYTE_SIZE) //2880 byte size for the primary PrimaryHDU
    val f = new FileOutputStream(name)

    Try {
      primaryHDU(0) = "SIMPLE  =                    T /file does conform to FITS standard".padTo(80, ' ')
      primaryHDU(1) = fixedFormatValue(KEY_BIT_PIX,bitPix, "number of bits per data pixel")
      primaryHDU(2) = fixedFormatValue(KEY_NAXIS,2,"number of data axes")
      primaryHDU(3) = fixedFormatValue(KEY_NAXIS_1,xAxis,"length of data axis 1 (x)")
      primaryHDU(4) = fixedFormatValue(KEY_NAXIS_2,yAxis,"length of data axis 2 (y)")
      primaryHDU(5) = fixedFormatString(KEY_BSCALE,bScale.toString,"linear transformation of pixels: scale value")
      primaryHDU(6) = fixedFormatString(KEY_BZERO,bZero.toString,"linear transformation of pixels: constant value")

      for (i <- 0 until recordSeq.size) {
        val key = recordSeq(i)._1
        val value = recordSeq(i)._2.replaceAll("''","'")
        val comment = if (commentSeq.contains(key)) commentSeq(key) else ""
        val record =
          (key.padTo(8, ' ') +
            "= " +
            value +
            (if (comment.isEmpty) "" else " /" + comment)
            ).padTo(HDU_RECORD_BYTE_SIZE,' ')

        primaryHDU(7 + i) = if (record.length > HDU_RECORD_BYTE_SIZE) record.take(HDU_RECORD_BYTE_SIZE) else record
      }
      primaryHDU(7 + recordSeq.size) = "END".padTo(HDU_RECORD_BYTE_SIZE,' ')
      var byteCount = 0
      primaryHDU.foreach{s =>
        byteCount += s.length
        f.write(s.getBytes,0,HDU_RECORD_BYTE_SIZE)
      }
      val bytesInLastBlock = byteCount % HDU_BLOCK_BYTE_SIZE
      if (bytesInLastBlock > 0) {
        val padSize = HDU_BLOCK_BYTE_SIZE - bytesInLastBlock
        val pad = " " * padSize
        f.write(pad.getBytes)
      }
      f.close
    }
    match {
      case Success(_) => true
      case Failure(ex) =>
        f.close
        error(s"Error writing FITS file: '$name' " + ex.toString)
        false
    }
  }
  //---------------------------------------------------------------------------
  def save(bi: BufferedImage, name: String, format: String = "png"): Unit =
    ImageIO.write(bi, format, new File(name + "." + format))

  //---------------------------------------------------------------------------
  def loadImageSeq(seqName: Array[String]) = seqName map (s=> MyImage(s))
  //---------------------------------------------------------------------------
  def loadImageDir(dirName: String) = {
    val fileSeq = Path.getSortedFileListWithExtension(dirName, MyConf.c.getStringSeq("Common.fitsFileExtension"))
      .map(_.getAbsolutePath)
      .sorted
      .toArray
    loadImageSeq(fileSeq)
  }
  //---------------------------------------------------------------------------
  //(finalMinRa, finalMaxRa, finalMinDec, finalMaxDec)
  def getRaDecRangeWithImageSeq(imageSeq: Array[MyImage]
                                , raMargin: Double = 0.1d
                                , decMargin: Double = 0.1d) : Option[(Double,Double,Double,Double)] = {

    var minRa: Double = Double.MaxValue
    var minDec: Double = Double.MaxValue
    var maxRa: Double = Double.MinValue
    var maxDec: Double = Double.MinValue
    var imageCount : Long = 0

    imageSeq foreach { img =>
      val fits = img.getSimpleFits
      if (fits.hasWcs) {
        val r = img.getRaDecRelevantPos()
        info(s"Image: '${img.getRawName}' {ra[${r(0)},${r(1)}] dec[${r(2)},${r(3)}]} => " +
          s"{ra[${Conversion.DD_to_HMS(r(0))},${Conversion.DD_to_HMS(r(1))}] dec[${Conversion.DD_to_DMS(r(2))},${Conversion.DD_to_DMS(r(3))}]} ")
        minRa = Math.min(minRa,r(0))
        maxRa = Math.max(maxRa,r(1))
        minDec = Math.min(minDec,r(2))
        maxDec = Math.max(maxDec,r(3))
        imageCount += 1
      }
      else error(s"Image: '${img.getRawName()}' has no world coordiante system values (WCS) in its header")
    }

    val finalMinRa = minRa  - raMargin
    val finalMaxRa = maxRa  + raMargin

    val finalMinDec = minDec - decMargin
    val finalMaxDec = maxDec + decMargin

    info(s"\tminRa:  $minRa}")
    info(s"\tmaxRa:  $maxRa}")
    info(s"\tminDec: $minDec}")
    info(s"\tmaxDec: $maxDec")

    info(s"\taugmented minRa:  $finalMinRa}")
    info(s"\taugmented maxRa:  $finalMaxRa}")
    info(s"\taugmented minDec: $finalMinDec}")
    info(s"\taugmented maxDec: $finalMaxDec")

    info(s"Ra-Dec range. Files processed: $imageCount")

    Some((finalMinRa,finalMaxRa,finalMinDec,finalMaxDec))
  }
  //---------------------------------------------------------------------------
  //get the median values
  def getBackgroundAndRMS(backGroundAndRmsSeq: Array[(Double, Double)]): (Double, Double) = {
    val (estimatedBackground, estimatedBackgroundRMS) =
      if (backGroundAndRmsSeq.size % 2 == 1) backGroundAndRmsSeq(backGroundAndRmsSeq.size / 2)
      else {
        val posRight = backGroundAndRmsSeq.size / 2
        val posLeft = posRight - 1
        ((backGroundAndRmsSeq(posRight)._1 + backGroundAndRmsSeq(posLeft)._1) / 2.0d
          , (backGroundAndRmsSeq(posRight)._2 + backGroundAndRmsSeq(posLeft)._2) / 2.0d)
      }
    (estimatedBackground, estimatedBackgroundRMS)
  }
  //---------------------------------------------------------------------------
  def synthesizeImage(n: String
                      , sourceSeq: Array[Segment2D]
                      , imageDimension: Point2D
                      , format: String = "png"
                      , saveImage: Boolean = true
                      , blackAndWhite: Boolean = false
                      , userPixColor: Option[Int] = None //set to None for color random color in each source
                      , centroidColor : Option[Int] = None //set to None when no centroid must be show
                      , font: Option[Font] = None //Set to 'None' for no text on source. In other case, a valid alternative is: Some(new Font(Font.MONOSPACED, Font.PLAIN, 10)
                      , fontColor: Color = Color.YELLOW
                      , fontOffsetY: Int = 1
                      , pixZero: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE
                      , dict: scala.collection.mutable.Map[Long, String] = scala.collection.mutable.Map[Long, String]()
                      , differentColorSource: scala.collection.mutable.Map[Long, Color] = scala.collection.mutable.Map[Long, Color]()
                      , arrowDiffMap: Map[Int, Point2D] = Map[Int, Point2D]()
                      , pixelTransformation: Option[(Option[Seq[Double]],Option[Seq[Double]])] = None
                      , inverYAxis: Boolean = true
                      , additionalOffset: Point2D = Point2D.POINT_ZERO)
  : BufferedImage = {
    val dim = imageDimension
    val bi = Segment2D.synthesizeImageWithColor(
        sourceSeq
      , dim.x
      , dim.y
      , blackAndWhite = blackAndWhite
      , pixZero = pixZero
      , userPixColor = userPixColor
      , centroidColor = centroidColor
      , font = font
      , fontColor = fontColor
      , fontOffsetY = fontOffsetY
      , dict
      , differentColorSource
      , arrowDiffMap
      , pixelTransformation
      , inverYAxis
      , additionalOffset)

    val fileName = if (n.endsWith("." + format)) n else n + "." + format
    if (saveImage) ImageIO.write(bi, format, new File(fileName))
    bi
  }

  //---------------------------------------------------------------------------
  //(minPos,maxPos,width,height)
  def aftOnDimensions(atf: AffineTransformation2D
                      , xPixMax: Int
                      , yPixMax: Int) = {
    val min = atf(Point2D_Double(0, 0))
    val max = atf(Point2D_Double(xPixMax - 1, yPixMax - 1))
    val (x1, x2) = (Math.round(min.x), Math.round(max.x))
    val (y1, y2) = (Math.round(min.y), Math.round(max.y))

    val xAxisNew = Segment1D(Math.min(x1, x2).toInt, Math.max(x1, x2).toInt)
    val yAxisNew = Segment1D(Math.min(y1, y2).toInt, Math.max(y1, y2).toInt)

    val x = Segment1D(0, xPixMax - 1) getIntersection xAxisNew
    val y = Segment1D(0, yPixMax - 1) getIntersection yAxisNew

    val width = if (x.isDefined) x.get.range else 0
    val height = if (y.isDefined) y.get.range else 0

    (min, max, width, height)
  }
  //---------------------------------------------------------------------------
  //(timeStamp, telescope, filter, xPixSize, yPixSize, exposureTime, originalImageName, instrument, objectName)
  def getMetadaFromImageName(imageName: String) =  {
    val itemSeq = MyFile.getFileNameNoPathNoExtension(imageName).split("#")
    val timeStamp = Time.parseLocalDateTime(itemSeq(1), localDateTimeFormatterWithMillis).get

    val pixSizeItem   = itemSeq(2).split("x")
    val filter    = itemSeq(3)
    val telescope = itemSeq(4)
    val exposureTime = itemSeq(5).replaceAll("s","").toDouble
    val (originalImageName,instrument,objectName) =
      if (itemSeq == 8) (itemSeq(6), itemSeq(7), itemSeq(8))
      else ("UNKNOWN","UNKNOWN","UNKNOWN")
    (timeStamp
      , telescope
      , filter
      , pixSizeItem.head.toInt  //pix size x
      , pixSizeItem.last.toInt  //pix size y
      , exposureTime
      , originalImageName
      , instrument
      , objectName
    )
  }
  //---------------------------------------------------------------------------
  def getMetadaFromImageNameWithPath(filePath:String) =  {
    val imageName = MyFile.getFileNameNoPathNoExtension(filePath)
    if (imageName.startsWith("science_")) {
      val img = MyImage(filePath)
      (img.getObservingTime()
        , img.getTelescope()
        , img.getFilter()
        , img.xMax
        , img.yMax
        , img.getExposureTime().toDouble
        , imageName
        , img.getInstrument()
        , img.getObject()
      )
    }
    else getMetadaFromImageName(imageName)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.common.image.myImage.MyImage._
class MyImage(xMax : Int //range [0,xMax-1]
              , yMax : Int //range [0,yMax-1]
              , data: Array[PIXEL_DATA_TYPE]
              , offset: Point2D = Point2D.POINT_ZERO
              , name: String= "None"
              , userFits: Option[SimpleFits] = None) extends Matrix2D(xMax, yMax, data, offset, name) {
  //---------------------------------------------------------------------------
  protected val fits: SimpleFits = userFits.getOrElse(null)
  assert(fits != null,s"Error: image:'$name' has not FITS")
  final val bitPix: Int = fits.bitPix
  bScale =  fits.bScale
  bZero = fits.bZero

  //not considering scale
  final val (minPixelStorage,maxPixelStorage) = PixelDataType.getMinMaxPixelValue(bitPix)

  //considering scale
  final val minValidPixelValue: Double = (minPixelStorage * bScale) + bZero
  final val maxValidPixelValue: Double = (maxPixelStorage * bScale) + bZero
  //---------------------------------------------------------------------------
  final val robustStatMinValue: PIXEL_DATA_TYPE = minValidPixelValue
  final val robustStatMaxValue: PIXEL_DATA_TYPE = maxValidPixelValue
  protected var stats: SimpleStatDescriptive = null
  protected var robustStats: SimpleStatDescriptive = null
  //---------------------------------------------------------------------------
  private var aft: AffineTransformation2D = AFFINE_TRANSFORMATION_2D_UNIT
  //---------------------------------------------------------------------------
  protected var wcs: WCS = null
  protected var region: Region = null

  protected var background: PIXEL_DATA_TYPE = PIXEL_INVALID_VALUE
  protected var backgroundRMS: PIXEL_DATA_TYPE = PIXEL_INVALID_VALUE

  protected var noiseTide: PIXEL_DATA_TYPE = PIXEL_INVALID_VALUE
  protected var fwhmAverage = -1d //median of the fwhm brightest sources
  protected var maskSeq: Array[Mask] = null

  //--------------------------------------------------------------------------
  def getAFT = aft
  //--------------------------------------------------------------------------
  def setAft(a: AffineTransformation2D) = aft = a
  //---------------------------------------------------------------------------
  def composeRightAft(newAft: AffineTransformation2D) = {
    aft = aft.compose(newAft)
    aft
  }
  //---------------------------------------------------------------------------
  def composeLeftAft(newAft: AffineTransformation2D) = {
    aft = newAft.compose(aft)
    aft
  }
  //--------------------------------------------------------------------------
  def getStringID(s: String = name) = MyImage.getStringID(s)

  //---------------------------------------------------------------------------
  def setBackground(b: PIXEL_DATA_TYPE) = background = b

  //---------------------------------------------------------------------------
  def setBackgroundRMS(b: PIXEL_DATA_TYPE) = backgroundRMS = b

  //---------------------------------------------------------------------------
  def setFwhmAverage(b: Double, algorithmName: String, verbose: Boolean) = {
    if (verbose) info(s"Image: '${getRawName()}' fwhm '" + f"$b%.3f" + s"' using algorithm: '$algorithmName'")
    fwhmAverage = b
  }
  //---------------------------------------------------------------------------
  //https://photutils.readthedocs.io/en/stable/background.html
  //split the image into a mesh and use a background estimator on each cell of the mesh
  //The result value is the median of the values calculated for each cell
  def estimateGlobalBackgroundAndRMS(xPixSizeMesh: Int = MyConf.c.getInt("Background.m2_Algorithm.meshSizeX")
                                    , yPixSizeMesh: Int = MyConf.c.getInt("Background.m2_Algorithm.meshSizeY")): (Double, Double) = {
    val fits = getSimpleFits
    val partitionSeq = getPartitionSeq(xPixSizeMesh, yPixSizeMesh)

    //estimate the background and get median
    val backGroundAndRmsSeq = partitionSeq.map(m => Simple.estimateBackgroundAndRms(MyImage(m, fits).data))
    val (estimatedBackground,estimatedBackgroundRMS) = getBackgroundAndRMS(backGroundAndRmsSeq)
    setBackground(estimatedBackground)
    setBackgroundRMS(estimatedBackgroundRMS)
    (estimatedBackground,estimatedBackgroundRMS)
  }
  //---------------------------------------------------------------------------
  def getNoiseTide(sigmaMultiplier: Double
                   , verbose:Boolean): PIXEL_DATA_TYPE =
    getBackground(verbose) + (getBackgroundRMS(verbose) * sigmaMultiplier)
  //---------------------------------------------------------------------------
  def getPixCount = getItemCount

  //---------------------------------------------------------------------------
  def getWcs() = {
    if (wcs == null) wcs = WCS(getSimpleFits())
    wcs
  }
  //---------------------------------------------------------------------------
  def setWcs(_wcs: WCS) = wcs = _wcs

  //---------------------------------------------------------------------------
  def hasWcs() = getSimpleFits().hasWcs()

  //--------------------------------------------------------------------------
  def setRegion(r: Region) = region = r

  //--------------------------------------------------------------------------
  def getRegion = region

  //--------------------------------------------------------------------------
  def getSimpleFits(): SimpleFits = {
    if (fits != null) fits
    else {
      if (data.isEmpty || name.isEmpty || name == "None") null
      else {
        val newFits = SimpleFits(name)
        if (!newFits.isValid()) null
        else newFits
      }
    }
  }

  //---------------------------------------------------------------------------
  def getCentralSubImage(halfPercentage: Int = 25) = { //the final percentage will be the double around the mid point
    val mid = Point2D(xMax / 2, yMax / 2)
    val radiusAsPoint = Point2D(Math.round(xMax * (halfPercentage * 0.01)).toInt
      , Math.round(yMax * (halfPercentage * 0.01)).toInt)
    getSubMatrix(mid - radiusAsPoint, mid + radiusAsPoint)
  }

  //---------------------------------------------------------------------------
  def findSourceSeq(noiseTide: PIXEL_DATA_TYPE
                    , maskSeq: Array[Mask] = Array[Mask]()
                    , sourceSizeRestriction: Option[(Int, Int)] = None //(minSourcePixCount,maxSourcePixCount)
                    , useGloblalId: Boolean = false
                   ): Region = {
    region = super.findSource(noiseTide
                              , maskSeq
                              , sourceSizeRestriction
                              , useGloblalId)
    this.noiseTide = noiseTide
    region
  }
  //---------------------------------------------------------------------------
  //between A and B transformation
  def getOverlappingPercentage(atf: AffineTransformation2D): Double = {
    val (_, width, height) = applyAffineTransformationOnDimensions(atf)
    getOverlappingPercentage(width, height)
  }

  //---------------------------------------------------------------------------
  def getOverlappingPercentage(width: Float, height: Float): Double =
    ((width * height) / getPixCount.toFloat) * 100f
  //--------------------------------------------------------------------------
  def pixToSky(pix: Point2D_Double): Point2D_Double = pixToSkySeq(Array(pix)).head

  //--------------------------------------------------------------------------
  def pixToSkySeq(pixSeq: Array[Point2D_Double]): Array[Point2D_Double] = {
    val _wcs = getWcs
    pixSeq map { p => _wcs.pixToSky(p) }
  }

  //--------------------------------------------------------------------------
  def skyToPix(p: Point2D_Double
               , positionOffset: Point2D_Double = Point2D_Double.POINT_ZERO
              ): Point2D_Double = skyToPixSeq(p.toArray(), positionOffset).head

  //--------------------------------------------------------------------------
  def skyToPixSeq(raDecSeq: Array[Double]
                  , positionOffset: Point2D_Double = Point2D_Double.POINT_ZERO
                 ): Array[Point2D_Double] = {

    val fits = getSimpleFits()
    if (!fits.hasWcs) {
      error(s"The fits image ${fits.fileName} must have WCS (world ccordiante system) keywords to get 'sky2xy'")
      return Array[Point2D_Double]()
    }
    val _wcs = getWcs()
    raDecSeq.grouped(2).map { t =>
      val r = _wcs.skyToPix(Point2D_Double(t(0), t(1)))
      if (r == Point2D_Double.POINT_INVALID) Point2D_Double.POINT_INVALID
      else r - positionOffset
    }.toArray
  }

  //--------------------------------------------------------------------------
  def getDateObs = getSimpleFits.getStringValue(SimpleFits.KEY_DATEOBS).get.replace("'", "")

  //---------------------------------------------------------------------------
  // result    index
  //-----------------
  // raMin     0
  // raMax     1
  // decMin    2
  // decMax    3
  // raCenter  4
  // decCenter 5
  def getRaDecRelevantPos() = {
    val pixPosSeq = Array(Point2D_Double.POINT_ONE, Point2D_Double(xMax, yMax)
                        , Point2D_Double(xMax / 2, yMax / 2))
    val r = pixToSkySeq(pixPosSeq)
    val raMin = Math.min(r(0).x, r(1).x)
    val raMax = Math.max(r(0).x, r(1).x)
    val decMin = Math.min(r(0).y, r(1).y)
    val decMax = Math.max(r(0).y, r(1).y)
    val radecCenter = r(2)
    Array(
        raMin             //0
      , raMax             //1
      , decMin            //2
      , decMax            //3
      , radecCenter.x     //4
      , radecCenter.y)    //5
  }
  //---------------------------------------------------------------------------
  def getRaDecRectangle() = {
    val pos = getRaDecRelevantPos()
    RectangleDouble(pos(0)
                    , pos(2)
                    , pos(1)
                    , pos(3))
  }
  //---------------------------------------------------------------------------
  def getRawName() = Path.getOnlyFilenameNoExtension(name)

  //---------------------------------------------------------------------------
  def getParentDir() = Path.getParentPath(name)

  //---------------------------------------------------------------------------
  def getRawNameWithExtension() = Path.getOnlyFilename(name)

  //---------------------------------------------------------------------------
  def getFileExtension() = MyFile.getFileExtension(name)

  //---------------------------------------------------------------------------
  def getSimpleStats(): SimpleStatDescriptive = {
    if (stats == null) {
      val stat = StatDescriptive.getWithDouble(data)
      stats = SimpleStatDescriptive(stat.count
                                    , stat.average
                                    , stat.variance
                                    , stat.stdDev
                                    , stat.median
                                    , stat.min
                                    , stat.max
                                    , stat.rms)
    }
    stats
  }

  //---------------------------------------------------------------------------
  def getRobustStats(maxValue: PIXEL_DATA_TYPE = robustStatMaxValue): SimpleStatDescriptive = {
    if (robustStats == null) {
      val robustData = data.filter { v => (v > robustStatMinValue) && (v < maxValue) }
      val stats = StatDescriptive.getWithDouble(robustData)
      robustStats = SimpleStatDescriptive(stats.count
                                          , stats.average
                                          , stats.variance
                                          , stats.stdDev
                                          , stats.median
                                          , stats.min
                                          , stats.max
                                          , stats.rms)
    }
    robustStats
  }

  //---------------------------------------------------------------------------
  def getBackground(verbose:Boolean = false) = {
    if (background == PIXEL_INVALID_VALUE) {
      if (MyConf.c.getBoolean("Background.useSextractor"))
        Sextractor.estimateBackgroundAndFWHM(this,verbose) //it updates the fwhm and background
      else {
        val r = estimateGlobalBackgroundAndRMS()
        background = r._1
        backgroundRMS = r._2
      }
    }
    background
  }

  //---------------------------------------------------------------------------
  def getBackgroundRaw() = {
    if (background == PIXEL_INVALID_VALUE) {
      val r = estimateGlobalBackgroundAndRMS()
      background = r._1
      backgroundRMS = r._2
    }
    background
  }
  //---------------------------------------------------------------------------
  def getBackgroundRMS(verbose: Boolean = false) = {
    getBackground(verbose)
    backgroundRMS
  }
  //---------------------------------------------------------------------------
  //an aperture is just a circle around a point
  def getAperturePosSeq(centre: Point2D_Float, radius: Float, o: Point2D = Point2D.POINT_ZERO) = {
    val c = CircleFloat(centre, radius)
    val max = c.getMax.toPoint2D
    val min = c.getMin.toPoint2D
    val r = for (y <- min.y to max.y;
                 x <- min.x to max.x)
    yield if (centre.getDistance(Point2D_Float(x, y)) <= radius) Some(Point2D(x, y) + o) else None

    r.flatten.toArray
  }

  //---------------------------------------------------------------------------
  def getNoiseTide() = noiseTide
  //---------------------------------------------------------------------------
  def setNoiseTide(v: Double) = noiseTide = v
  //---------------------------------------------------------------------------
  def getSourceDetectionFromConfiguration() = {
    val c = Telescope.getConfigurationFile(getSimpleFits())
    val sigmaMultiplier = c.getFloat("SourceDetection.image.sigmaMultiplier")
    val sourceMinPixCount = c.getInt("SourceDetection.source.minPixCount")
    val sourceMaxPixCount = c.getInt("SourceDetection.source.maxPixCount")
    val background = getBackground()
    val backgroundRMS = getBackgroundRMS()
    val noiseTide = background + (backgroundRMS * sigmaMultiplier)
    setBackground(background)
    setBackgroundRMS(backgroundRMS)
    setNoiseTide(noiseTide)
    (background
      , backgroundRMS
      , noiseTide
      , sourceMinPixCount
      , sourceMaxPixCount)
  }
  //---------------------------------------------------------------------------
  def getSourceSizeRestrictionFromConfiguration() = {
    val c = Telescope.getConfigurationFile(getSimpleFits())
    val sourceMinPixCount = c.getInt("SourceDetection.source.minPixCount")
    val sourceMaxPixCount = c.getInt("SourceDetection.source.maxPixCount")
    (sourceMinPixCount, sourceMaxPixCount)
  }
  //---------------------------------------------------------------------------
  def isValid() = {
    if (xMax == 0 && yMax == 0) false
    else getSimpleFits.isValid()
  }

  //---------------------------------------------------------------------------
  //get the flux intersecting a circle and a grid (pixels image). Ported from 'python photutils.circular_overlap_grid'
  def getFluxWithApertureByCircleGridIntersection(centroid: Point2D_Double
                                                  , circleRadius: Double
                                                  , paddingValue: PIXEL_DATA_TYPE): Double = {
    val x_delta = circleRadius
    val y_delta = circleRadius
    val xRange = new Point2D_Double(centroid.x - x_delta, centroid.x + x_delta)
    val yRange = new Point2D_Double(centroid.y - y_delta, centroid.y + y_delta)

    //bounding box
    val xRangeBoundingBox = Point2D(Math.floor(xRange.x + 0.5).toInt, Math.ceil(xRange.y + 0.5).toInt)
    val yRangeBoundingBox = Point2D(Math.floor(yRange.x + 0.5).toInt, Math.ceil(yRange.y + 0.5).toInt)

    //edges
    val xMin = xRangeBoundingBox.x - 0.5 - centroid.x
    val xMax = xRangeBoundingBox.y - 0.5 - centroid.x
    val yMin = yRangeBoundingBox.x - 0.5 - centroid.y
    val yMax = yRangeBoundingBox.y - 0.5 - centroid.y

    val maskSize = (xRangeBoundingBox.y - xRangeBoundingBox.x
                  , yRangeBoundingBox.y - yRangeBoundingBox.x)
    val weightMatrix = GridCircleOverlap.calculate(
      xMin
      , xMax
      , yMin
      , yMax
      , maskSize._1
      , maskSize._2
      , circleRadius
      , use_exact = true
      , subpixels = 1)

    //calculate pix mask, get sub image dimensions and reshape the weights to fit with sub-image
    val (weightMatrixReshaped, sliceLarge, pixMask) = GridCircleOverlap.reshape(
        xRangeBoundingBox.x
      , xRangeBoundingBox.y
      , yRangeBoundingBox.x
      , yRangeBoundingBox.y
      , getDimension()
      , weightMatrix)

    //get sub image
    val yRangeSlice = sliceLarge._1
    val xRangeSlice = sliceLarge._2

    var subImg: Matrix2D = null
    Try {
      subImg = getSubMatrixWithPadding(
        Point2D(Math.max(0, xRangeSlice.x)
          , Math.max(0, yRangeSlice.x)) //min
        , Point2D(Math.min(xMaxIndex, xRangeSlice.y - 1)
          , Math.min(yMaxIndex, yRangeSlice.y - 1)) //max
        , paddingValue)
    }
    match {
      case Success(_) =>
      case Failure(_) => return Double.MinValue
    }

    //apply weight to sub image pixels avoiding the bad ones defined in the 'pixMask'
    (((weightMatrixReshaped zip subImg.data) zip pixMask) flatMap { t =>
      val pixWeighted = t._1._1 * t._1._2
      if (t._2 == true && pixWeighted != 0)
        Some(pixWeighted)
      else None
    }).sum
  }

  //---------------------------------------------------------------------------
  def getFwhmAverage() = fwhmAverage

  //---------------------------------------------------------------------------
  def getMaskSeq() = {
    if (maskSeq == null) maskSeq = Mask.get(getSimpleFits)
    maskSeq
  }
  //---------------------------------------------------------------------------
  def getElapsedProcessingTimeinDays() = {

    val date = getSimpleFits.getStringValueOrEmptyNoQuotation(KEY_M2_RUN_DATE)
    if (date.isEmpty) 0
    else {
      val processingTime = LocalDateTime.parse(date, com.common.util.time.Time.logTimeFormatter)
      val now = LocalDateTime.now()
      ChronoUnit.DAYS.between(processingTime, now)
    }
  }
  //---------------------------------------------------------------------------
  def getExposureTime() = {
    val fits = getSimpleFits
    if (fits.existKey(KEY_OM_EXPTIME))
      fits.getStringValueOrEmptyNoQuotation(KEY_OM_EXPTIME).toFloat
    else ExposureTime.obtain(fits).get
  }
  //---------------------------------------------------------------------------
  def getTelescope() = {
    val fits = getSimpleFits
    if (fits.existKey(KEY_OM_TELESCOPE))
      fits.getStringValueOrEmptyNoQuotation(KEY_OM_TELESCOPE)
    else TelescopeMatcher.getTelescope(fits)
  }
  //---------------------------------------------------------------------------
  def getObservingTime() = {
    val fits = getSimpleFits
    if (fits.existKey(KEY_OM_DATEOBS))
      LocalDateTime.parse(getSimpleFits.getStringValueOrEmptyNoQuotation(KEY_OM_DATEOBS)
        , DateTimeFormatter.ISO_LOCAL_DATE_TIME)
    else ObservingTime.obtain(fits).get
  }
  //---------------------------------------------------------------------------
  def getFilter() = {
    val fits = getSimpleFits
    if (fits.existKey(KEY_OM_FILTER))
      fits.getStringValueOrEmptyNoQuotation(KEY_OM_FILTER)
    else "UNKNOWN"
  }
  //---------------------------------------------------------------------------
  def getObservingDateMidPoint() = {
    val observingTime = getObservingTime()
    observingTime
      .plusNanos(Time.secondsToNanos(getExposureTime() / 2d))
  }

  //---------------------------------------------------------------------------
  def getInstrument() = {
    val fits = getSimpleFits
    if (fits.existKey(KEY_OM_INSTRUMENT))
      fits.getStringValueOrEmptyNoQuotation(KEY_OM_INSTRUMENT)
    else "UNKNOWN"
  }

  //---------------------------------------------------------------------------
  def getObject() = {
    val fits = getSimpleFits
    if (fits.existKey(KEY_OM_OBJECT))
      fits.getStringValueOrEmptyNoQuotation(KEY_OM_OBJECT)
    else "UNKNOWN"
  }
  //---------------------------------------------------------------------------
  def getPixScaleX() = fits.getKeyDoubleValueOrDefault(KEY_OM_PIX_SCALE_X, -1) //ra

  //---------------------------------------------------------------------------
  def getPixScaleY() = fits.getKeyDoubleValueOrDefault(KEY_OM_PIX_SCALE_Y, -1) //dec

  //---------------------------------------------------------------------------
  def getRaPixInDecimalDegree(pix: Double) = Conversion.pixToDecimalDegree(pix, getPixScaleX)

  //---------------------------------------------------------------------------
  def getDecPixInInDecimalDegree(pix: Double) = Conversion.pixToDecimalDegree(pix, getPixScaleY)

  //---------------------------------------------------------------------------
  def getFovX() = fits.getKeyDoubleValueOrDefault(KEY_OM_FOV_X, -1) //ra in arcmin

  def getFovX_InMas() = getFovX * Conversion.ARCMIN_TO_MAS

  def getFovX_InDeg() = Conversion.arcoMinToDecimalDegree(getFovX)

  //---------------------------------------------------------------------------
  def getFovY() = fits.getKeyDoubleValueOrDefault(KEY_OM_FOV_Y, -1) //dec in arcmin

  def getFovY_InMas() = getFovY * Conversion.ARCMIN_TO_MAS

  def getFovY_InDeg() = Conversion.arcoMinToDecimalDegree(getFovY)

  //---------------------------------------------------------------------------
  def setCentroidRaDec(sourceSeq: Array[Segment2D], offset: Point2D = Point2D.POINT_ZERO): Unit = {
    val centerSeq = sourceSeq map (s => s.getCentroid + Point2D_Double(offset.x, offset.y))
    val raDecSeq = pixToSkySeq(centerSeq)
    if (raDecSeq.isEmpty) return
    //assign the ra dec centroid calculation
    sourceSeq.zipWithIndex foreach { case (s, i) =>
      s.setCentroidRaDec(raDecSeq(i))
    }
  }

  //---------------------------------------------------------------------------
  //(raStats,decStats,xStats,yStats)
  def getResidualStats(pixPosSeq: Array[Point2D_Double]
                       , raDecSeq: Array[Point2D_Double]) = {
    //---------------------------------------------------------------
    val wcs = getWcs()
    var diffSeqPrev = (pixPosSeq zip raDecSeq) map { case (pix, raDec) =>
      wcs.pixToSky(pix).getResidualMas(raDec)
    }
    val raStats = StatDescriptive.getWithDouble(diffSeqPrev map (_.x))
    val decStats = StatDescriptive.getWithDouble(diffSeqPrev map (_.y))

    diffSeqPrev = (pixPosSeq zip raDecSeq) map { case (pix, raDec) =>
      wcs.skyToPix(raDec) - pix
    }
    val xStats = StatDescriptive.getWithDouble(diffSeqPrev map (_.x))
    val yStats = StatDescriptive.getWithDouble(diffSeqPrev map (_.y))

    (raStats, decStats, xStats, yStats)
  }

  //---------------------------------------------------------------------------
  override def verticalFlip() = {
    val m = super.verticalFlip()
    fits.updateRecord(KEY_NAXIS_1, m.xMax.toString)
    fits.updateRecord(KEY_NAXIS_2, m.yMax.toString)
    new MyImage(
      xMax
      , yMax
      , m.data
      , offset
      , m.name
      , userFits = Some(fits))
  }

  //---------------------------------------------------------------------------
  override def horizontalFlip() = {
    val m = super.horizontalFlip()
    new MyImage(
      xMax
      , yMax
      , m.data
      , offset
      , m.name
      , userFits = Some(fits))
  }

  //---------------------------------------------------------------------------
  override def rotation90(clockwise: Boolean, invertY: Boolean = false) = {
    val m = super.rotation90(clockwise, invertY)
    fits.updateRecord(KEY_NAXIS_1, m.xMax.toString)
    fits.updateRecord(KEY_NAXIS_2, m.yMax.toString)
    new MyImage(
      m.xMax
      , m.yMax
      , m.data
      , offset
      , m.name
      , userFits = Some(fits))
  }

  //---------------------------------------------------------------------------
  override def rotation180(clockwise: Boolean) = {
    val m = super.rotation180(clockwise)
    fits.updateRecord(KEY_NAXIS_1, m.xMax.toString)
    fits.updateRecord(KEY_NAXIS_2, m.yMax.toString)
    new MyImage(
      m.xMax
      , m.yMax
      , m.data
      , offset
      , m.name
      , userFits = Some(fits))
  }

  //---------------------------------------------------------------------------
  override def rotation270(clockwise: Boolean) = {
    val m = super.rotation270(clockwise)
    fits.updateRecord(KEY_NAXIS_1, m.xMax.toString)
    fits.updateRecord(KEY_NAXIS_2, m.yMax.toString)
    new MyImage(
      m.xMax
      , m.yMax
      , m.data
      , offset
      , m.name
      , userFits = Some(fits))
  }

  //---------------------------------------------------------------------------
  def createGif(gifName: String
                , noiseTideMin: Int
                , noiseTideMax: Int
                , noiseTideStep: Int
                , blackAndWhite: Boolean = false
                , userPixColor: Option[Int] = Some(0xFFFFFF) //set to None for color random color in each source
                , gifSpeed: Int = 150) //set to None of no centroid must be calculated
  = {
    val start = System.currentTimeMillis()
    val n = Path.getOnlyFilenameNoExtension(name)
    info(s"Creating gif for image '$n' from noise tide: $noiseTideMin to $noiseTideMax with step: $noiseTideStep")
    val gifItemSeq = for (noiseTide <- noiseTideMax to noiseTideMin by -noiseTideStep) yield {
      info(s"Generating gif item sequence for image '$n' and noise tide: $noiseTide/$noiseTideMin")
      findSource(noiseTide)
        .synthesizeImageWithColor(n
          , saveImage = false
          , blackAndWhite = blackAndWhite
          , userPixColor = userPixColor)
    }
    info(s"Creating gif for image '$n'")
    UtilImage.saveGIF(gifName, gifItemSeq, gifSpeed)
    info(s"Created gif for image '$n' from noise tide: $noiseTideMin to $noiseTideMax with step: $noiseTideStep elapsed time: ${System.currentTimeMillis() - start} ms")
  }

  //---------------------------------------------------------------------------
  def normalizeHistogram(): MyImage =
    MyImage(super.normalizeHistogram(maxValidPixelValue), getSimpleFits())
  //---------------------------------------------------------------------------
  override def normalize(sigmaScale: Double): MyImage =
    MyImage(super.normalize(sigmaScale), getSimpleFits())
  //---------------------------------------------------------------------------
  def createGifImageSeq(dir: String
                        , noiseTideMin: Int
                        , noiseTideMax: Int
                        , noiseTideStep: Int
                        , blackAndWhite: Boolean = false
                        , userPixColor: Option[Int] = Some(0xFFFFFF) //set to None for color random color in each source
                       )
  = {
    val n = Path.getOnlyFilenameNoExtension(name)
    val d = Path.resetDirectory(dir)
    info(s"Creating gif for image '$n' from noise tide: $noiseTideMin to $noiseTideMax with step: $noiseTideStep")
    for (noiseTide <- noiseTideMax to noiseTideMin by -noiseTideStep) {
      info(s"Generating gif item sequence for image '$n' and noise tide: $noiseTide/$noiseTideMin")
      findSource(noiseTide).synthesizeImageWithColor(d + n + "_" + f"$noiseTide%05d"
        , blackAndWhite = blackAndWhite
        , userPixColor = userPixColor)
    }
  }
  //-------------------------------------------------------------------------
  //keep the data without BZERO,BSCALE transforamtion
  def saveAsFitsRaw(fileName: String
                    , userFitsHeader: Array[(String, String)] = Array()
                    , data: Array[Byte] = Files.readAllBytes(Paths.get(name))) = {
    val fitsHeader = ArrayBuffer(getSimpleFits.getKeyValueMap.toSeq: _*)
    fitsHeader ++= userFitsHeader
    saveAsFits(fileName
      , fitsHeader.toArray
      , rawData = data.drop(getSimpleFits.imageDataAbsoluteOffset)
      , applyTransformation = false
      , buildInfo = Array((KEY_M2_VERSION, s"'${BuildInfo.version.trim}'")
                       , (KEY_M2_RUN_DATE, s"'${Time.getISO_DateTimeStamp}'")))
  }
  //---------------------------------------------------------------------------
  def saveAsFits(_name: String
                 , userFitsRecordSeq: Array[(String, String)] = getSimpleFits.getKeyValueMap.toArray
                 , _data: Array[PIXEL_DATA_TYPE] = data
                 , _bitPix: Int    = bitPix
                 , _bScale: Double = bScale
                 , _bZero: Double  = bZero
                 , applyTransformation: Boolean = true
                 , rawData: Array[Byte] = Array[Byte]()
                 , userFitsRecordCommentSeq: scala.collection.mutable.Map[String, String] = getSimpleFits().getKeyValueCommentMap
                 , buildInfo: Array[(String,String)] = Array()
                ): Boolean = {
    //remove basic info to avoid duplications
    val filteredUserFitsRecordSeq =
      (userFitsRecordSeq filter (t => !SimpleFits.FITS_MIN_INFO_RECORD_SEQ.contains(t._1))) ++ buildInfo

    val name = if (_name.endsWith(".fits") | _name.endsWith(".fit") | _name.endsWith(".fts")) _name else _name + ".fits"

    if (!MyImage.createFitsHeader(name, _bitPix, _bScale, _bZero, xMax, yMax, filteredUserFitsRecordSeq, userFitsRecordCommentSeq)) return false
    val f = new FileOutputStream(name, true) //append
    Try {
      val byteSeq = {
        if (applyTransformation) {
          val scaledPixelSeq =
            PixelDataType.scalePixelSeq(_data
                                        , _bScale
                                        , _bZero
                                        , isForward = false)
              .take(xMax * yMax) //inverse transformation when write. FITS standard sectionName 4.4.2.5
          PixelDataType.getPixelByteSeq(scaledPixelSeq, _bitPix)
        }
        else {
          if (!rawData.isEmpty) rawData.take(xMax * yMax * Math.abs(_bitPix / 8))
          else rawData
        }
      }
      if (!byteSeq.isEmpty) {
        f.write(byteSeq)
        f.write(Array.fill(SimpleFits.HDU_BLOCK_BYTE_SIZE - (byteSeq.size % SimpleFits.HDU_BLOCK_BYTE_SIZE))(0.byteValue))
      }
      f.close
    }
    match {
      case Success(_) => true
      case Failure(ex) => f.close
        error(s"Error writing FITS file: '$name' " + ex.toString)
        false
    }
  }

  //---------------------------------------------------------------------------
  def saveCroppy(centralPos: Point2D_Double
                 , outputFileNameFits: String
                 , outputFileNamePng: String
                 , croppyXpixSize: Int
                 , croppyYpixSize: Int
                 , rectangleHull: Rectangle
                 , rectangleOffset: Point2D = Point2D.POINT_ZERO
                 , noideTide: PIXEL_DATA_TYPE
                 , sigmaMultiplier: Double
                 , sourceDetectionSizeRestriction: Option[(Int, Int)] = None): Unit = {
    val croopyCentroid = centralPos.toPoint2D()
    val croppyHalfSize = Point2D(croppyXpixSize / 2, croppyYpixSize / 2)
    val subImage = MyImage(getSubMatrixWithPadding(croopyCentroid - croppyHalfSize, croopyCentroid + croppyHalfSize),getSimpleFits())
    val min = rectangleHull.min - rectangleOffset - subImage.offset
    val max = rectangleHull.max + rectangleOffset - subImage.offset
    val pixSeq = ArrayBuffer[PIXEL_DATA_TYPE]()
    pixSeq ++= subImage.data

    //png and fits order is important, in other case the rectangle is detected as source
    //png
    val pngImage = MyImage(Matrix2D(subImage.xMax, subImage.yMax, pixSeq.toArray, offset = subImage.offset), getSimpleFits())
    val croppyRegion = pngImage.findSource(noideTide, sourceSizeRestriction = sourceDetectionSizeRestriction)
    croppyRegion.synthesizeImageWithColor(outputFileNamePng
      , imageDimension = Some(pngImage.getDimension())
      , additionalOffset = pngImage.offset.inverse)
    val savedPng = BmpImagePlot(0, 0, imageName = Some(outputFileNamePng))
    val minRecPng = min - Point2D.POINT_ONE //bmp origin is (0,0)
    val maxRecPng = max - Point2D.POINT_ONE //bmp origin is (0,0)
    savedPng.rectangle(Rectangle(minRecPng, maxRecPng), borderColor = Some(Color.WHITE), solid = false)
    savedPng.save(outputFileNamePng)

    //fits
    //draw rectangle
    for (x <- min.x to max.x) {
      if (subImage.isIn(Point2D(x, min.y))) pixSeq((min.y * subImage.xMax) + x) = maxValidPixelValue
      if (subImage.isIn(Point2D(x, max.y))) pixSeq((max.y * subImage.xMax) + x) = maxValidPixelValue
    }
    for (y <- min.y to max.y) {
      if (subImage.isIn(Point2D(min.x, y))) pixSeq((y * subImage.xMax) + min.x) = maxValidPixelValue
      if (subImage.isIn(Point2D(max.x, y))) pixSeq((y * subImage.xMax) + max.x) = maxValidPixelValue
    }
    //prepare header
    val userFitsRecordSeq = Array(
      ("BACKGROU", background.toString)
      , ("BACK_RMS", backgroundRMS.toString)
      , ("SIGMA_MU", sigmaMultiplier.toString)
      , ("NOISE_TI", noideTide.toString)
      , ("RA_DEC  ", centralPos.toString)
      , ("PIX_POS ", (centralPos - Point2D_Double(subImage.offset)).toPoint2D().toString)
    )
    //create the image
    val fitsCroppy = MyImage(Matrix2D(subImage.xMax, subImage.yMax, pixSeq.toArray, offset = subImage.offset), getSimpleFits())
    fitsCroppy.saveAsFits(outputFileNameFits
      , userFitsRecordSeq = userFitsRecordSeq
      , userFitsRecordCommentSeq = scala.collection.mutable.Map[String, String]()
      , _bScale = bScale
      , _bZero = bZero
      , buildInfo = Array((KEY_M2_VERSION, s"'${BuildInfo.version.trim}'")
                        , (KEY_M2_RUN_DATE, s"'${Time.getISO_DateTimeStamp}'")))
  }

  //---------------------------------------------------------------------------
  def getAftOnDimensions() =
    aftOnDimensions(aft, xMax, yMax)
  //---------------------------------------------------------------------------
  def getAftRectangle() = {
    val (min, max, _, _) = aftOnDimensions(aft, xMax, yMax)
     RectangleDouble(Point2D_Double(Math.max(0,min.x),Math.min(yMaxIndex,Math.max(0, -min.y)))
                    , Point2D_Double(Math.max(0,max.x),Math.min(yMaxIndex, max.y)))
  }
  //---------------------------------------------------------------------------
  def getAftOverlapping() = {
    val (min, max, width, height) = aftOnDimensions(aft, xMax, yMax)
    (min.x, min.y
     , max.x, max.y
     , width, height
     , ((width * height) / (xMax * xMax).toFloat) * 100f)
  }

  //---------------------------------------------------------------------------
  def stack(other: MyImage) = {
    val maxAllowedPixValue = maxValidPixelValue
    val stackedPixelSeq = (data zip other.data).map { case (leftPix, rightPix) =>
      Math.min(maxAllowedPixValue, leftPix + rightPix)
    }
    MyImage(Matrix2D(xMax
      , yMax
      , stackedPixelSeq)
      , getSimpleFits())
  }
  //---------------------------------------------------------------------------
  def subtract(other:MyImage) = {
    val maxAllowedPixValue = maxValidPixelValue
    val minAllowedPixValue = minValidPixelValue
    val subtractedPixelSeq = (data zip other.data).map { case (leftPix, rightPix) =>
      Math.min(maxAllowedPixValue, Math.max(minAllowedPixValue, leftPix - rightPix))
    }
    MyImage(Matrix2D(xMax
      , yMax
      , subtractedPixelSeq)
      , getSimpleFits())
  }
  //---------------------------------------------------------------------------
  def hasWCS_fit() =
    getSimpleFits.existKey(KEY_M2_WCS_FIT)

  //---------------------------------------------------------------------------
  //(northIsUp,eastIsRight)
  def getOrientation() = {
    val r = getRaDecRelevantPos()
    val raMin  = r(0)
    val raMax  = r(1)
    val decMin = r(2)
    val decMax = r(3)
    (decMin >= decMax, raMin >= raMax)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file MyImage.scala
//=============================================================================