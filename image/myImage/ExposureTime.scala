/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Apr/2023
 * Time:  16h:28m
 * Description: None
 */
//=============================================================================
package com.common.image.myImage
//=============================================================================
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.simpleFits.SimpleFits.{KEY_EXPOSURE, KEY_EXPTIME, KEY_EXP_TIME}
import com.common.util.util.Util
//=============================================================================
//=============================================================================
object ExposureTime {
  //---------------------------------------------------------------------------
  final val DEFAULT_EXPOSURE_TIME  = 1f //in seconds
  //---------------------------------------------------------------------------
  def obtain(imagePath: String): Option[Float] = {
    val img = MyImage(imagePath)
    obtain(img.getSimpleFits())
  }
  //---------------------------------------------------------------------------
  def obtain(fits: SimpleFits): Option[Float] = {
    val expTime = fits.getStringValueOrEmptyNoQuotation(KEY_EXPTIME)
    val exposure = fits.getStringValueOrEmptyNoQuotation(KEY_EXPOSURE)
    val exp_time = fits.getStringValueOrEmptyNoQuotation(KEY_EXP_TIME)
    obtain(expTime, exposure, exp_time)
  }
  //---------------------------------------------------------------------------
  def obtain(expTime: String, exposure: String, exp_time: String): Option[Float] = {
    val s =
      (if (!expTime.isEmpty && expTime != "NULL") expTime
      else if (!exposure.isEmpty && exposure != "NULL") exposure
      else if (!exp_time.isEmpty && exp_time != "NULL") exp_time
      else return Some(DEFAULT_EXPOSURE_TIME)).toLowerCase()

    if (s.endsWith("ms")) {
      val newS = s.replace("ms","")
      if (Util.isFloat(newS)) return Some(newS.toFloat / 1000)
      else return None
    }
    else {
      if (s.endsWith("s")) {
        val newS = s.replace("s", "")
        if (Util.isFloat(newS)) return Some(newS.toFloat)
        else return None
      }
    }
    Some(s.replaceAll("'", "").toFloat)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ExposureTime.scala
//=============================================================================
