/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Apr/2023
 * Time:  16h:22m
 * Description: None
 */
//=============================================================================
package com.common.image.myImage
//=============================================================================
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.simpleFits.SimpleFits._
import com.common.util.time.Time
//=============================================================================
import java.time.format.DateTimeFormatter
import java.time.LocalDateTime
//=============================================================================
//=============================================================================
object ObservingTime {
  //---------------------------------------------------------------------------
  private final val COMPLETE_TIME_STAMP_ISO_8601_LENGTH_MS_3 = 23 //yyyy-MM-ddTHH:mm:ss.xyz

  private final val PARTIAL_DATE_ISO_8601_LENGTH = 10 //yyyy-MM-dd
  private final val PARTIAL_TIME_ISO_8601_LENGTH_SHORT = 8 //HH:mm:ss
  private final val PARTIAL_TIME_ISO_8601_MS_LENGTH_SHORT   = 12 //HH:mm:ss.mss
  private final val PARTIAL_TIME_ISO_8601_MS_LENGTH_SHORT_2 = 11 //HH:mm:ss.ms

  private final val SHORT_DATE_FORMATTER_1 = DateTimeFormatter.ofPattern("dd/MD/yy")
  private final val SHORT_DATE_FORMATTER_2 = DateTimeFormatter.ofPattern("yyyy/MM/dd")
  private final val SHORT_DATE_FORMATTER_3 = DateTimeFormatter.ofPattern("MM/dd/yyyy")

  private final val INVERSE_DATE_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss.SSS")
  //---------------------------------------------------------------------------
  def obtain(imagePath: String): Option[LocalDateTime] = {
    val img = MyImage(imagePath)
    obtain(img.getSimpleFits())
  }
  //---------------------------------------------------------------------------
  def obtain(img: MyImage): Option[LocalDateTime] =
    obtain(img.getSimpleFits())
  //---------------------------------------------------------------------------
  def obtain(fits: SimpleFits): Option[LocalDateTime] = {
    val dateObs = fits.getStringValueOrEmptyNoQuotation(KEY_DATEOBS)
    val timeObs = fits.getStringValueOrEmptyNoQuotation(KEY_TIMEOBS)
    val date = fits.getStringValueOrEmptyNoQuotation(KEY_DATE)
    val time = fits.getStringValueOrEmptyNoQuotation(KEY_TIME)

    val utStart = fits.getStringValueOrEmptyNoQuotation(KEY_UTSTART)
    val ut = fits.getStringValueOrEmptyNoQuotation(KEY_UT)
    val utObs = fits.getStringValueOrEmptyNoQuotation(KEY_UTOBS)

    val utcStart = fits.getStringValueOrEmptyNoQuotation(KEY_UTCSTART)
    val utcObs = fits.getStringValueOrEmptyNoQuotation(KEY_UTC_OBS)
    val utcEnd = fits.getStringValueOrEmptyNoQuotation(KEY_UTCEND)

    val timStamp = fits.getStringValueOrEmptyNoQuotation(KEY_TIMESTMP)

    obtain(dateObs
      , timeObs
      , date
      , time
      , utStart
      , ut
      , utObs
      , utcStart
      , utcObs
      , utcEnd
      , timStamp)

  }
  //---------------------------------------------------------------------------
  def obtain(_dateObs: String
             , _timeObs: String
             , _date: String
             , _time: String
             , _utStart: String
             , _ut: String
             , _utObs: String
             , _utcStart: String
             , _utcObs: String
             , _utcEnd: String
             , _timeStamp:String = null): Option[LocalDateTime] = {
    //-------------------------------------------------------------------------
    def getFirstValidStringValue(seq: Seq[String]): String = {
      if (seq.isEmpty) ""
      else {
        for (a <- seq)
          if (!a.isEmpty && a != "NULL") return a
      }
      ""
    }

    //-------------------------------------------------------------------------
    def getTimeValue(st: String) = {
      if (st.isEmpty || (st == "NULL")) ""
      else {
        val s = st.replaceAll("'", "")
        if (s.isEmpty || (s == "NULL")) ""
        else {
          val length = s.length
          if (length > COMPLETE_TIME_STAMP_ISO_8601_LENGTH_MS_3) s.take(COMPLETE_TIME_STAMP_ISO_8601_LENGTH_MS_3) //do not care about time zone
          else if (length < PARTIAL_TIME_ISO_8601_LENGTH_SHORT) ""
          else s
        }
      }
    }

    //-------------------------------------------------------------------------
    def getAdditionalTimeSource() = {
      val r = getFirstValidStringValue(Seq(_ut, _utObs, _utcObs, _utStart, _utcStart, _utcEnd))
      if (r.isEmpty) ""
      else {
        val seq = r.split(":")
        if (seq.length < 3) ""
        else {
          val hour = if (seq(0).length != 2) "0" + seq(0) else seq(0)
          val minute = if (seq(1).length != 2) "0" + seq(1) else seq(1)
          val (second, fractionSecond) = {
            if (seq(2).contains(".")) {
              val sMs = seq(2).split("\\.")
              val s = sMs(0)
              val ms = sMs(1)
              val finalMS =
                if (ms.length == 1) "00" + ms
                else if (ms.length == 2) "0" + ms
                else ms.take(3)
              (if (s.length != 2) "0" + s else s, finalMS)
            }
            else {
              val s = seq(2).replaceAll("\\.", "")
              (if (s.length != 2) "0" + s else s, ".000")
            }
          }
          hour + ":" + minute + ":" + (if (second == "60") "59" else second) + "." + fractionSecond
        }
      }
    }

    //-------------------------------------------------------------------------
    def getDateConversion(s: String) = {
      if (s.contains("/")) {
        val d1 = Time.parseLocalDate(s, SHORT_DATE_FORMATTER_1)
        if (d1.isDefined) d1.get.format(DateTimeFormatter.ISO_LOCAL_DATE)
        else {
          val d2 = Time.parseLocalDate(s, SHORT_DATE_FORMATTER_2)
          if (d2.isDefined) d2.get.format(DateTimeFormatter.ISO_LOCAL_DATE)
          else {
            val d3 = Time.parseLocalDate(s, SHORT_DATE_FORMATTER_3)
            if (d3.isDefined) d3.get.format(DateTimeFormatter.ISO_LOCAL_DATE)
            else ""
          }
        }
      }
      else s
    }

    //--------------------------------------------------------------------------
    val dateObs = getDateConversion(getTimeValue(_dateObs))
    val timeObs = getTimeValue(_timeObs)
    val date = getDateConversion(getTimeValue(_date))
    val time = getTimeValue(_time)
    val additionalTimeSource = getAdditionalTimeSource

    var r = Time.parseLocalDateTime(dateObs)
    if (r.isDefined) return r
    else {
      val r2 = Time.parseLocalDateTime(dateObs + "T" + timeObs)
      if (r2.isDefined) return r2
    }

    r = Time.parseLocalDateTime(date)
    if (r.isDefined) return r

    //inverse date
    if (_timeStamp != null && !_timeStamp.trim.isEmpty)
      return Time.parseLocalDateTime(_timeStamp.trim, INVERSE_DATE_FORMATTER)

    //just local date with no time ?
    if (timeObs.isEmpty && time.isEmpty && additionalTimeSource.isEmpty) {
      val justDate = Time.parseLocalDate(date)
      if (justDate.isEmpty) return None
      else return Time.parseLocalDateTime(dateObs + "T00:00:00")
    }

    (dateObs.length, timeObs.length, date.length, time.length, additionalTimeSource.length) match {

      case (_, _, PARTIAL_DATE_ISO_8601_LENGTH, PARTIAL_TIME_ISO_8601_LENGTH_SHORT, _)    => Time.parseLocalDateTime(date + "T" + time)
      case (_, _, PARTIAL_DATE_ISO_8601_LENGTH, PARTIAL_TIME_ISO_8601_MS_LENGTH_SHORT, _) => Time.parseLocalDateTime(date + "T" + time)

      case (PARTIAL_DATE_ISO_8601_LENGTH, PARTIAL_TIME_ISO_8601_MS_LENGTH_SHORT, _, _, _)   => Time.parseLocalDateTime(dateObs + "T" + timeObs)
      case (PARTIAL_DATE_ISO_8601_LENGTH, PARTIAL_TIME_ISO_8601_LENGTH_SHORT, _, _, _)      => Time.parseLocalDateTime(dateObs + "T" + timeObs)
      case (PARTIAL_DATE_ISO_8601_LENGTH, PARTIAL_TIME_ISO_8601_MS_LENGTH_SHORT_2, _, _, _) => Time.parseLocalDateTime(dateObs + "T" + timeObs)

      case (PARTIAL_DATE_ISO_8601_LENGTH, _, _, PARTIAL_TIME_ISO_8601_LENGTH_SHORT, _)    => Time.parseLocalDateTime(dateObs + "T" + time)
      case (PARTIAL_DATE_ISO_8601_LENGTH, _, _, PARTIAL_TIME_ISO_8601_MS_LENGTH_SHORT, _) => Time.parseLocalDateTime(dateObs + "T" + time)

      case (PARTIAL_DATE_ISO_8601_LENGTH, _, _, _, PARTIAL_TIME_ISO_8601_MS_LENGTH_SHORT) => Time.parseLocalDateTime(dateObs + "T" + additionalTimeSource)
      case (_, _, PARTIAL_DATE_ISO_8601_LENGTH, _, PARTIAL_TIME_ISO_8601_MS_LENGTH_SHORT) => Time.parseLocalDateTime(date + "T" + additionalTimeSource)

      case _ => None
    }
    //--------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ObservingTime.scala
//=============================================================================
