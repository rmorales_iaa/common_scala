/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  19/Feb/2021
  * Time:  11h:07m
  * Description: None
  */
//=============================================================================
package com.common.image.filter
//=============================================================================
//=============================================================================
import scala.util.matching.Regex
//=============================================================================
object Filter {
  //===========================================================================
  val IGNORED_FILTER_NAME = "IGNORED"
  val UNKNOWN_FILTER_NAME = "UNKNOWN"
  //===========================================================================
  //higher priority means go first
  val PRIORITY_100 =   100
  val PRIORITY_75 =     75
  val PRIORITY_25 =     25
  val PRIORITY_15 =     15
  val PRIORITY_1  =      1
  val PRIORITY_NONE =    0
  //===========================================================================
  val PATTERN_ALWAYS_FAIL = "(?!)".stripMargin.replaceAll("\n","").r  //always fail match
  //===========================================================================
}
//=============================================================================
import Filter._
sealed trait Filter {
  //---------------------------------------------------------------------------
  val name: String
  val filterPattern: Regex
  val objectPattern: Regex
  val alternativePattern: Regex
  val pathPattern: Regex
  val mpcFilterID: String = "B"  //minor planet center band (aka filter) ID . 'B' means not identified
  var keepLetterCase: Boolean = false //use lower case
  //---------------------------------------------------------------------------
  //priority: filterValue, alternativeFilterValue, objectValue, path
  def tryToMatch(filterValue: String
                 , objectValue: String
                 , alternativeFilterValue: String
                 , path: String): (Boolean,Int) = {
    if (tryToMatchFilterValue(filterValue)) return (true,PRIORITY_100)
    if (tryToMatchAlternativeFilterValue(alternativeFilterValue)) return (true,PRIORITY_75)
    if (tryToMatchPathValue(path)) return (true,PRIORITY_15)
    if (tryToMatchObjectValue(objectValue)) return (true, PRIORITY_1)
    (false,PRIORITY_NONE)
  }
  //---------------------------------------------------------------------------
  private def tryToMatchFilterValue(v: String) = {
    (if (keepLetterCase) v else v.toLowerCase).trim match {
      case filterPattern(_*) => true
      case _ => false
    }
  }
  //---------------------------------------------------------------------------
  private def tryToMatchObjectValue(v: String) =
    (if (keepLetterCase) v else v.toLowerCase).trim match {
      case objectPattern(_*) => true
      case _ => false
    }
  //---------------------------------------------------------------------------
  private def tryToMatchAlternativeFilterValue(v: String) =
    (if (keepLetterCase) v else v.toLowerCase).trim match {
      case alternativePattern(_*) => true
      case _ => false
    }
  //---------------------------------------------------------------------------
  private def tryToMatchPathValue(v: String) =
    (if (keepLetterCase) v else v.toLowerCase).trim match {
      case pathPattern(_*) => true
      case _ => false
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class FilterMultipleMatch(nameSeq: String) extends Filter() {
  //---------------------------------------------------------------------------
  val name = "MULTIPLE_FILTER_MATCH_" + nameSeq
  val filterPattern = PATTERN_ALWAYS_FAIL
  val objectPattern = PATTERN_ALWAYS_FAIL
  val alternativePattern = PATTERN_ALWAYS_FAIL
  val pathPattern   = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class FilterUnknown() extends Filter() {
  //---------------------------------------------------------------------------
  val name = UNKNOWN_FILTER_NAME
  val filterPattern =
    """.*unknown.*""".stripMargin.replaceAll("\n","").r
  val objectPattern = filterPattern
  val alternativePattern = filterPattern
  val pathPattern   = filterPattern
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class FilterIgnored() extends Filter {
  //---------------------------------------------------------------------------
  val name = IGNORED_FILTER_NAME

  val filterPattern =
    """gg495|
      |hbw|
      |hbn|
      |.*ft.?mask.*|
      |nii""".stripMargin.replaceAll("\n","").r

  val objectPattern      = filterPattern
  val alternativePattern = objectPattern
  val pathPattern        = filterPattern
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed abstract class FilterJohnsonFamily(subType: String, _keepLetterCase: Boolean) extends Filter {
  //---------------------------------------------------------------------------
  val name = "JOHNSON_" + subType.toUpperCase
  val filterPattern =
    s"""${subType.toUpperCase()}|
        |.*john.$subType.*|
        |.*johnson.$subType.*|
        |.*johnson$subType.*|
        |.*john.$subType.*|
        |.*johnson.$subType.*|
        |.*johnson$subType.*|
        |.*johnson_$subType.*|
        |.*cous.*$subType.*|
        |.*Cous.*$subType.*|
        |$subType.john.*|
        |$subType.John.*|
        |dome.ff.$subType.*|
        |.*dome.$subType.*|
        |dome.ff.$subType.*|
        |.*dome.$subType.*|
        |.*filter$subType.*|
        |.*flat$subType.*|
        |.*flats$subType.*|
        |.*flat *$subType.*|
        |.*flat *sky $subType.*|
        |.*flat\\-$subType.*|
        |.*flat_$subType.*|
        |.*filter$subType.*|
        |.*flat$subType.*|
        |.*flats$subType.*|
        |.*flat *$subType.*|,
        |.*flat *sky $subType.*|
        |.*flat\\-$subType.*|
        |.*flat_$subType.*|
        |.*[[flat]].* $subType.*|
        |.*flat_sky $subType.*|
        |${subType.toUpperCase()}.#.*|
        |.*$subType.?[0123456789]+|
        |.*$subType.?[0123456789]+.*|
        |.*-$subType|
        |.*_$subType [0123456789]+|
        |.*_$subType[0123456789]+|
        |.*_$subType.*|
        |.*\\-$subType..*|
        |.*standar[0123456789]+ $subType.*|
        |.*\\($subType\\).*|
        |.*\\([0123456789]+\\) $subType.*|
        |[0123456789]+.$subType.*|
        |ison.$subType.*|
        |.*Skyflat. *$subType.*|
        |.*skyflat. *$subType.*""".stripMargin.replaceAll("\n","").r

  val objectPattern = filterPattern
  val alternativePattern = filterPattern
  val pathPattern =
    s""".*flat[0123456789]+s$subType.*|
        |.*flat[0123456789]+s$subType.*|
        |.*filter_$subType.*|
        |.*[0123456789]+$subType.f.*""".stripMargin.replaceAll("\n","").r

  keepLetterCase = _keepLetterCase
  override val mpcFilterID: String = subType
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class JohnsonU() extends FilterJohnsonFamily("u",true)
sealed case class JohnsonB() extends FilterJohnsonFamily("b",true)
sealed case class JohnsonV() extends FilterJohnsonFamily("v",true)
sealed case class JohnsonR() extends FilterJohnsonFamily("r",true)
sealed case class JohnsonI() extends FilterJohnsonFamily("i",true)

sealed case class Johnson_u() extends FilterJohnsonFamily("u",false)
sealed case class Johnson_b() extends FilterJohnsonFamily("b",false)
sealed case class Johnson_v() extends FilterJohnsonFamily("v",false)
sealed case class Johnson_r() extends FilterJohnsonFamily("r",false)
sealed case class Johnson_i() extends FilterJohnsonFamily("i",false)

//=============================================================================
sealed case class FilterOpen() extends Filter {
  //---------------------------------------------------------------------------
  val name = "open"

  val filterPattern =
    """.*clear.*|
      |.*cle.*|
      |.*_clr.*|
      |.*free.*|
      |.*empty.*|
      |.*none.*|
      |.*abierto.*|
      |.*blank.*|
      |.*libre.*|
      |.*open.*|
      |.*no_filter.*|
      |.*nofilter.*|
      |.*nofilt.*|
      |.*sin.filtro.*|
      |no|
      |clr|
      |c|
      |.*air.*|
      |.*aire.*|
      |.*without_filter.*|
      |.*sin filtro.*""".stripMargin.replaceAll("\n","").r

  val objectPattern = filterPattern
  val alternativePattern = objectPattern
  val pathPattern   = """.*clear.*|
                        |.*filter_clear.*|
                        |.*filter_none.*|
                        |.*filter_empty.*|
                        |.*filter_free.*|
                        |.*flatc.*|
                        |.*flatsc.*|
                        |.*nofilt.*|
                        |.*[0123456789]+clea.*\.f.*|
                        |.*cle\.f.*""".stripMargin.replaceAll("\n","").r

  override val mpcFilterID: String = "R"
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed abstract class FilterSdssFamily(subType: String) extends Filter {
  //---------------------------------------------------------------------------
  val name = "sdss_" + subType
  val filterPattern = s"""sdss.?$subType.*|
                     |${subType}p|
                     |.*$subType.?sdss*""".stripMargin.replaceAll("\n","").r

  val objectPattern = filterPattern
  val alternativePattern = filterPattern
  val pathPattern = filterPattern
  override val mpcFilterID: String = subType
}
//=============================================================================
sealed case class FilterSdssU() extends FilterSdssFamily("i")
sealed case class FilterSdssG() extends FilterSdssFamily("g")
sealed case class FilterSdssR() extends FilterSdssFamily("r")
sealed case class FilterSdssI() extends FilterSdssFamily("i")
sealed case class FilterSdssZ() extends FilterSdssFamily("z")
//=============================================================================
sealed case class FilterLuminance() extends Filter {
  //---------------------------------------------------------------------------
  val name = "luminance"

  val filterPattern =
    """.*luminance.*|
      |l""".stripMargin.replaceAll("\n","").r

  val objectPattern =
    """.*luminance.*|
      |l|
      |.*_lum [0123456789]+|
      |.*dome lum""".stripMargin.replaceAll("\n","").r

  val alternativePattern = objectPattern
  val pathPattern   = filterPattern

  override val mpcFilterID: String = "R"
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class FilterH_Alfa() extends Filter {
  //---------------------------------------------------------------------------
  val name = "h_alfa"

  val filterPattern =
    """ha#.*|
      |ha|
      |halfa|
      |halpha|
      |ha[0123456789]+|
      |hFilterR[0123456789]+""".stripMargin.replaceAll("\n","").r

  val objectPattern = filterPattern
  val alternativePattern = objectPattern
  val pathPattern   = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed abstract class FilterPhotographicFamily(subType: String) extends Filter {
  //---------------------------------------------------------------------------
  val name = "photo_" + subType

  val filterPattern =
    s"""$subType|
       |.*photo.$subType.*""".stripMargin.replaceAll("\n","").r

  val objectPattern = PATTERN_ALWAYS_FAIL
  val alternativePattern = PATTERN_ALWAYS_FAIL
  val pathPattern = PATTERN_ALWAYS_FAIL
}
//=============================================================================
sealed case class FilterPhotographicRed()   extends FilterPhotographicFamily("red")
sealed case class FilterPhotographicGreen() extends FilterPhotographicFamily("green")
sealed case class FilterPhotographicBlue()  extends FilterPhotographicFamily("blue")
//=============================================================================
sealed case class Filter02() extends Filter {
  //---------------------------------------------------------------------------
  val name = "H02"

  val filterPattern =
    """h02-ha.*""".stripMargin.replaceAll("\n","").r

  val objectPattern = PATTERN_ALWAYS_FAIL
  val alternativePattern = PATTERN_ALWAYS_FAIL
  val pathPattern   = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class FilterDiffuser() extends Filter {
  //---------------------------------------------------------------------------
  val name = "diffuser"
  val filterPattern =
    """.*difusor.*""".stripMargin.replaceAll("\n","").r
  val objectPattern = PATTERN_ALWAYS_FAIL
  val alternativePattern = PATTERN_ALWAYS_FAIL
  val pathPattern   = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class FilterSpectralLine() extends Filter {
  //---------------------------------------------------------------------------
  val name = "spectral_line"
  val filterPattern =
    """[0123456789]+/[0123456789]+""".stripMargin.replaceAll("\n","").r
  val objectPattern = PATTERN_ALWAYS_FAIL
  val alternativePattern = filterPattern
  val pathPattern   = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class FilterIR_Block() extends Filter {
  //---------------------------------------------------------------------------
  val name = "IR_BLOCK"
  val filterPattern = PATTERN_ALWAYS_FAIL
  val objectPattern = """.*ir.?dataBlock.*""".stripMargin.replaceAll("\n","").r
  val alternativePattern = objectPattern
  val pathPattern   = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class FilterKG() extends Filter {
  //---------------------------------------------------------------------------
  val name = "KG"
  val filterPattern = """kg""".stripMargin.replaceAll("\n","").r
  val objectPattern = """.*kg1.*""".stripMargin.replaceAll("\n","").r
  val alternativePattern = objectPattern
  val pathPattern   = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed abstract class FilterBesselFamily(subType: String) extends Filter {
  //---------------------------------------------------------------------------
  val name = "BESSEL_" + subType.toUpperCase
  val filterPattern = PATTERN_ALWAYS_FAIL
  val objectPattern =
    s""".*bessel.?$subType.*|
       |.*bessel..$subType.*|
       |$subType.*_bes.*""".stripMargin.replaceAll("\n","").r
  val alternativePattern = objectPattern
  val pathPattern = objectPattern
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class FilterBesselU() extends FilterBesselFamily("i")
sealed case class FilterBesselB() extends FilterBesselFamily("b")
sealed case class FilterBesselV() extends FilterBesselFamily("v")
sealed case class FilterBesselR() extends FilterBesselFamily("r")
sealed case class FilterBesselI() extends FilterBesselFamily("i")
//=============================================================================
sealed abstract class FilterCahaFamily(subType: String) extends Filter {
  //---------------------------------------------------------------------------
  val name = "CAHA_" + subType.toUpperCase
  val filterPattern = (subType + s"""_.""".stripMargin.replaceAll("\n","")).r
  val objectPattern = PATTERN_ALWAYS_FAIL
  val alternativePattern = (subType + s"""/ir.*""".stripMargin.replaceAll("\n","")).r
  val pathPattern = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
sealed case class FilterCahaNIR() extends FilterCahaFamily("nir")
sealed case class FilterCahaR()   extends FilterCahaFamily("r")
sealed case class FilterCahaB()   extends FilterCahaFamily("b")
sealed case class FilterCahaUV()  extends FilterCahaFamily("uv")

sealed case class FilterCahaNIR_2() extends Filter {
  //---------------------------------------------------------------------------
  val name = "CAHA_NIR"
  val filterPattern = """nir""".stripMargin.replaceAll("\n","").r
  val objectPattern = PATTERN_ALWAYS_FAIL
  val alternativePattern = PATTERN_ALWAYS_FAIL
  val pathPattern   = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
//William Herschel Telescope
sealed abstract class FilterWHT_L(subType: String) extends Filter {
  //---------------------------------------------------------------------------
  val name = "WHT_L_" + subType.toUpperCase
  val filterPattern =
    s"""l$subType |
        |wht.l.$subType"""
    .stripMargin.replaceAll("\n","").r
  val objectPattern = PATTERN_ALWAYS_FAIL
  val alternativePattern = PATTERN_ALWAYS_FAIL
  val pathPattern = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
sealed case class FilterWHT_L1() extends FilterWHT_L("1")
sealed case class FilterWHT_L2() extends FilterWHT_L("2")
sealed case class FilterWHT_L3() extends FilterWHT_L("3")
sealed case class FilterWHT_L4() extends FilterWHT_L("4")

//=============================================================================
//sloan
sealed abstract class Sloan(subType: String, _keepLetterCase: Boolean) extends Filter {
  //---------------------------------------------------------------------------
  val name = "sloan_" + subType.toLowerCase
  val filterPattern =
    s"""${subType.toLowerCase()}|
       |${subType.toLowerCase()}#.*|
       |.*sloan_$subType.*|
       |${subType.toLowerCase()}\\..*"""
    .stripMargin.replaceAll("\n","").r
  val objectPattern = filterPattern
  val alternativePattern = filterPattern
  val pathPattern = filterPattern
  keepLetterCase = _keepLetterCase
  //---------------------------------------------------------------------------
}
sealed case class SloanU() extends Sloan("u", true)
sealed case class SloanG() extends Sloan("g", true)
sealed case class SloanR() extends Sloan("r", true)
sealed case class SloanI() extends Sloan("i", true)
sealed case class SloanZ() extends Sloan("z", true)

sealed case class Sloan_u() extends Sloan("u", false)
sealed case class Sloan_g() extends Sloan("g", false)
sealed case class Sloan_r() extends Sloan("r", false)
sealed case class Sloan_i() extends Sloan("i", false)
sealed case class Sloan_z() extends Sloan("z", false)
//=============================================================================
sealed case class FilterJ() extends Filter {
  //---------------------------------------------------------------------------
  val name = "FILTER_J"
  val filterPattern = """j""".stripMargin.replaceAll("\n","").r
  val objectPattern = filterPattern
  val alternativePattern = filterPattern
  val pathPattern   = filterPattern
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class FilterK() extends Filter {
  //---------------------------------------------------------------------------
  val name = "FILTER_K"
  val filterPattern = """k""".stripMargin.replaceAll("\n","").r
  val objectPattern = filterPattern
  val alternativePattern = filterPattern
  val pathPattern   = filterPattern
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class FilterLiverpool_OG() extends Filter {
  //---------------------------------------------------------------------------
  val name = "Liverpool_og515+kg5"
  val filterPattern = """og515\+kg5""".stripMargin.replaceAll("\n","").r
  val objectPattern = PATTERN_ALWAYS_FAIL
  val alternativePattern = """og515\+kg5""".stripMargin.replaceAll("\n","").r
  val pathPattern   = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class FilterLiverpool_IR720() extends Filter {
  //---------------------------------------------------------------------------
  val name = "Liverpool_IR_720"
  val filterPattern = PATTERN_ALWAYS_FAIL
  val objectPattern = PATTERN_ALWAYS_FAIL
  val alternativePattern = """ir720""".stripMargin.replaceAll("\n","").r
  val pathPattern   = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class FrameTransfer() extends Filter {
  //---------------------------------------------------------------------------
  val name = "Frame_transfer"
  val filterPattern =
    """.*ff.*|
      |frame.?transfer|
      |.*ft.*|
      |.*mask.*""".stripMargin.replaceAll("\n", "").r

  val objectPattern = PATTERN_ALWAYS_FAIL
  val alternativePattern = filterPattern
  val pathPattern = PATTERN_ALWAYS_FAIL
}
//=============================================================================
sealed case class EsoMPI_2_2_BB_RC() extends Filter {
    //---------------------------------------------------------------------------
    val name = "EsoMPI_2_2_BB_RC"
    val filterPattern =
      """.*bb#rc.*""".stripMargin.replaceAll("\n","").r
    val objectPattern = PATTERN_ALWAYS_FAIL
    val alternativePattern = filterPattern
    val pathPattern   = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class ColourBalance() extends Filter {
  //---------------------------------------------------------------------------
  val name = "ColourBalance"
  val filterPattern = PATTERN_ALWAYS_FAIL
  val objectPattern = PATTERN_ALWAYS_FAIL
  val alternativePattern = """cb""".stripMargin.replaceAll("\n","").r
  val pathPattern   = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class Diaphragm() extends Filter {
  //---------------------------------------------------------------------------
  val name = "Diaphragm"
  val filterPattern      = """dia""".stripMargin.replaceAll("\n","").r
  val objectPattern      = PATTERN_ALWAYS_FAIL
  val alternativePattern =  filterPattern
  val pathPattern        = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class VR() extends Filter {
  //---------------------------------------------------------------------------
  val name = "VR"
  val filterPattern      = """vr.*""".stripMargin.replaceAll("\n","").r
  val objectPattern      = PATTERN_ALWAYS_FAIL
  val alternativePattern = filterPattern
  val pathPattern        = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}

//=============================================================================
//CAHA 2.2 (PANIC)
sealed case class KS() extends Filter {
  //---------------------------------------------------------------------------
  val name = "KS"
  val filterPattern      = """ks.*""".stripMargin.replaceAll("\n","").r
  val objectPattern      = PATTERN_ALWAYS_FAIL
  val alternativePattern = filterPattern
  val pathPattern        = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Filter.scala
//=============================================================================
