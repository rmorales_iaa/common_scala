/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  19/Feb/2021
  * Time:  11h:07m
  * Description: None
  */
//=============================================================================
package com.common.image.filter
//=============================================================================
//=============================================================================
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.simpleFits.SimpleFits._
import com.common.logger.MyLogger
import com.common.image.filter.Filter.UNKNOWN_FILTER_NAME
//=============================================================================
import java.io.{File,PrintWriter}
//=============================================================================
object FilterMatcher extends MyLogger {
  //---------------------------------------------------------------------------
  final val FIELD_DIVIDER = "\t"
  //---------------------------------------------------------------------------
  final val ALTERNATIVE_KEY_FILTER_SEQ =  Seq (
     "INSFLNAM"
    , "FLT_ID"
    , "ALFLTNM"
    , "WFFBAND"
    , "FILTER01"
    , "INSFILTE"
    , "IRFILTER"
    , "FILTER1"
    , "FILTER2"
    , "FILTER3"
    , "FILTER4"
    , "PFMFBAND"
    , "FILTNAME"
    , "WFFBAND"
    , KEY_FILTER_HIERARCHY) ++ HIERARCHY_FILTER_KEY_SEQ

  //---------------------------------------------------------------------------
  private val filterSeq : Array[Filter] = Array (

      FilterIgnored()

    , FilterOpen()

    , FilterIR_Block()
    , FilterKG()

    , FilterLuminance()

    , FilterPhotographicRed()
    , FilterPhotographicGreen()
    , FilterPhotographicBlue()

    , FilterBesselR()
    , FilterBesselV()
    , FilterBesselI()
    , FilterBesselU()
    , FilterBesselB()

    , FilterSdssR()
    , FilterSdssI()
    , FilterSdssZ()
    , FilterSdssU()
    , FilterSdssG()

    , FilterH_Alfa()

    , Filter02()

    , FilterDiffuser()

    , FilterSpectralLine()

    , FilterCahaNIR()
    , FilterCahaR()
    , FilterCahaB()
    , FilterCahaUV()
    , FilterCahaNIR_2()

    , FilterJ()

    , FilterK()

    , FilterWHT_L1()
    , FilterWHT_L2()
    , FilterWHT_L3()
    , FilterWHT_L4()

    , JohnsonR()
    , JohnsonV()
    , JohnsonI()
    , JohnsonB()
    , JohnsonU()

    , SloanU()
    , SloanG()
    , SloanR()
    , SloanI()
    , SloanZ()

    , FilterLiverpool_OG()
    , FilterLiverpool_IR720()

    , FrameTransfer()

    , EsoMPI_2_2_BB_RC()

    , ColourBalance()
    , Diaphragm()

    , VR()
    , KS()

    , Johnson_u()
    , Johnson_b()
    , Johnson_v()
    , Johnson_r()
    , Johnson_i()

    , Sloan_u()
    , Sloan_g()
    , Sloan_r()
    , Sloan_i()
    , Sloan_z()

    , FilterUnknown()
    )
  //---------------------------------------------------------------------------
  private def tryToMatchFirstFilter(filterValue: String, objectValue: String, alternativeValue: String, path: String) : Filter =
    tryToMatchAllFilter(filterValue, objectValue, alternativeValue, path).head
  //---------------------------------------------------------------------------
  //match and sort by priority
  private def tryToMatchAllFilter(filterValue: String, objectValue: String, alternativeValue: String, path: String) : Array[Filter] = {
    val r = filterSeq flatMap { filter=>
      val matchResult = filter.tryToMatch(filterValue, objectValue, alternativeValue, path)
      if (matchResult._1) Some((filter,matchResult._2)) else None}
    if (r.isEmpty) Array(FilterUnknown())
    else r.sortWith(_._2 > _._2) map ( _._1)
  }
  //---------------------------------------------------------------------------
  //(filter, object, alternativeFilterValue, path)
  def tryToMatch(seq: Array[(String,String,String,String)]
                 , matchLog: Option[String] = None
                 , unknownMatchLog: Option[String] = None
                 , multipleMatchLog: Option[String] = None
                 , allowMultipleMatch : Boolean = false) : Array[Filter] = {
    var matchedCount : Long = 0
    var unknownMatchCount : Long = 0
    var multipleMatchCount : Long = 0
    val logFileMatch = if (matchLog.isDefined) new PrintWriter(new File(matchLog.get)) else null
    val logFileUnknown = if (unknownMatchLog.isDefined) new PrintWriter(new File(unknownMatchLog.get)) else null
    val logFileMultipleMatch = if (allowMultipleMatch && multipleMatchLog.isDefined) new PrintWriter(new File(multipleMatchLog.get)) else null
    if (matchLog.isDefined) {
      val s = s"#om_filter${FIELD_DIVIDER}filter${FIELD_DIVIDER}object${FIELD_DIVIDER}alternativeFilterValue${FIELD_DIVIDER}path\n"
      logFileMatch.write(s)
      logFileUnknown.write(s)
      if (allowMultipleMatch) logFileMultipleMatch.write(s)
    }

    //get filter
    val r = seq.map { case (filterValue, objectValue, alternativeValue, path) =>
      matchedCount += 1
      val filter =
        if (allowMultipleMatch){
          val filterSeq = FilterMatcher.tryToMatchAllFilter(filterValue, objectValue, alternativeValue, path)
          if (filterSeq.contains(FilterIgnored())) FilterIgnored()
          else {
            if (filterSeq.length > 1) FilterMultipleMatch((filterSeq map (_.name)) mkString("{","," ,"}"))
            else {  //'La Hita' observatory
              val f = filterSeq.head
              if (f.name != UNKNOWN_FILTER_NAME) f
              else if(path.toLowerCase.contains("hita")) FilterOpen() else f
            }
          }
        }
        else {
          val f = FilterMatcher.tryToMatchFirstFilter(filterValue, objectValue, alternativeValue, path)
            //'La Hita' observatory
            if (f.name != UNKNOWN_FILTER_NAME) f
            else if(path.toLowerCase.contains("hita")) FilterOpen() else f
        }

      //save in logs
      if (matchLog.isDefined) {
        val logLine = s"${filter.name}$FIELD_DIVIDER$filterValue$FIELD_DIVIDER$objectValue$FIELD_DIVIDER$alternativeValue${FIELD_DIVIDER}$path\n"
        logFileMatch.write(logLine)
        if (matchLog.isDefined) {
          if (filter.isInstanceOf[FilterUnknown]) {
            logFileUnknown.write(logLine)
            unknownMatchCount += 1
          }
          else {
            if (filter.isInstanceOf[FilterMultipleMatch]) {
              logFileMultipleMatch.write(logLine)
              multipleMatchCount += 1
            }
          }
        }
      }
      filter
    }
    if (matchLog.isDefined) {
      val total = matchedCount.toFloat
      info(s"Matched filter count          : $matchedCount -> ${(matchedCount/total) * 100}%")
      info(s"Unknown filter count          : $unknownMatchCount -> ${(unknownMatchCount/total) * 100}%")
      if (allowMultipleMatch) info(s"Multiple match filter count   : $multipleMatchCount -> ${(multipleMatchCount/total) * 100}%")

      if (logFileMatch != null) logFileMatch.close()
      if (logFileUnknown != null) logFileUnknown.close()
      if (allowMultipleMatch && (logFileMultipleMatch != null)) logFileMultipleMatch.close()
    }
    r
  }

  //-------------------------------------------------------------------------
  def getAlternateFilterValue(fits: SimpleFits): String = {
    ALTERNATIVE_KEY_FILTER_SEQ.foreach { key=>
      val v = fits.getStringValueOrEmptyNoQuotation(key).trim
      if(!v.isEmpty) return v
    }
    ""
  }
  //---------------------------------------------------------------------------
  def getObservingFilterRaw(fits: SimpleFits) =
    FilterMatcher.tryToMatch(
      Array((
          fits.getStringValueOrEmptyNoQuotation(KEY_FILTER).trim
        , fits.getStringValueOrEmptyNoQuotation(KEY_OBJECT).trim
        ,  getAlternateFilterValue(fits)
        , fits.fileName))
    )
  //---------------------------------------------------------------------------
  def getObservingFilterRaw(filterName:String) =
    FilterMatcher.tryToMatch(
      Array((
          filterName.trim
        , ""
        , ""
        , ""))
    )
  //---------------------------------------------------------------------------
  def getObservingFilter(fits: SimpleFits
                         , telescope:String
                         , instrument: String) = {
    val r = FilterMatchException.tryToMatchGTC_Telescope(fits,telescope,instrument)
    if (r.isEmpty) getObservingFilterRaw(fits).head.name
    else r.get
  }
  //---------------------------------------------------------------------------
  def getObservingFilter(fits: SimpleFits
                         , f: String
                         , obj: String
                         , alt: String
                         , telescope: String
                         , instrument: String
                         , path: String) = {

    val r = FilterMatchException.tryToMatchGTC_Telescope(fits,telescope,instrument)
    if (!r.isEmpty) r.get
    else {
      FilterMatcher.tryToMatch(
        Array((f
          , obj
          , alt
          , path))
      ).head.name
    }
  }
  //---------------------------------------------------------------------------
  def getObservingFilter(f: String
                         , obj: String
                         , alt: String
                         , path: String) =
      FilterMatcher.tryToMatch(
        Array((f
          , obj
          , alt
          , path))
      ).head.name

  //---------------------------------------------------------------------------
  def getObservingFilter(fiterName: String
                         , obj: String
                         , alternativeFilterName: String
                         , path: String
                         , telescope: String
                         , instrument: String
                         , filterValueSeq: Seq[String]) = {
    val r = FilterMatchException.tryToMatch(telescope, instrument, filterValueSeq.map(_.trim))
    if (!r.isEmpty) r.get
    else
      FilterMatcher.tryToMatch(
        Array((fiterName.trim
               , obj
               , alternativeFilterName.trim
               , path))
      ).head.name
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FilterMatcher.scala
//=============================================================================
