/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Feb/2021
 * Time:  11h:07m
 * Description: Use when the nominal 'FilterMatcher' is not applied
 */
//=============================================================================
package com.common.image.filter
//=============================================================================
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.simpleFits.SimpleFits.FILTER_NUMBERED_NAME_SEQ
import com.common.image.instrument.Osiris
import com.common.image.telescope.GTC
//=============================================================================
//=============================================================================
object FilterMatchException {
  //---------------------------------------------------------------------------
  private final val GTC_TELESCOPE_NAME = GTC().name.toLowerCase()
  private final val OSIRIS_INSTRUMENT_NAME = Osiris().name.toLowerCase()
  //---------------------------------------------------------------------------
  private def GTC_MatchFilter(filterValueSeq: Seq[String]): Option[String]  = {
    filterValueSeq flatMap { filterValue =>
      val s = filterValue.toLowerCase()
      if (s.toLowerCase() == "open") None else
      return Some(FilterMatcher.getObservingFilterRaw(s).head.name)
    }
    None
  }

  //---------------------------------------------------------------------------
  private def GTC_getFilterSeq(fits:SimpleFits): Seq[String] = {
    FILTER_NUMBERED_NAME_SEQ flatMap { filterName =>
      val s = fits.getStringValueOrEmptyNoQuotation(filterName)
      if (s.toLowerCase() == "open") None else
        Some(FilterMatcher.getObservingFilterRaw(s).head.name)
    }
  }
  //---------------------------------------------------------------------------
  def tryToMatch(telescope: String
                 , instrument: String
                 , filterValueSeq: Seq[String]): Option[String] = {
    val gtcName = GTC().name.toLowerCase()
    val osirisInstrumentName = Osiris().name
    telescope match {
      case telescope => if ((telescope.toLowerCase() == gtcName) &&
                           (instrument.toLowerCase() == osirisInstrumentName)) GTC_MatchFilter(filterValueSeq)
                        else None
      case _         => None
    }
  }

  //---------------------------------------------------------------------------
  def tryToMatchGTC_Telescope(fits: SimpleFits
                              , telescope: String
                              , instrument: String): Option[String] = {

    telescope match {
      case telescope => if ((telescope.toLowerCase() == GTC_TELESCOPE_NAME) &&
                           (instrument.toLowerCase() == OSIRIS_INSTRUMENT_NAME)) GTC_MatchFilter(GTC_getFilterSeq(fits))
      else None
      case _ => None
    }
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file FilterMatchException.scala
//=============================================================================
