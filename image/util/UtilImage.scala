package com.common.image.util
//=============================================================================
import com.common.configuration.MyConf
import com.common.hardware.cpu.CPU
import com.common.image.myImage.MyImage
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
import com.common.util.time.Time
//=============================================================================
import java.awt.image.BufferedImage
import java.io.{File, PrintWriter}
import gif.AnimatedGifEncoder
import java.awt.Font
import java.awt.Color
import scala.collection.concurrent.TrieMap
import javax.imageio.ImageIO
import sys.process._
import scala.io.Source
//=============================================================================
//=============================================================================
object UtilImage extends MyLogger {
  //-------------------------------------------------------------------------
  //astromImage macro circle
  private val xOvalPixelSize = 1
  private val yOvalPixelSize = 1

  //ImageJ first valid pixel starts at (1,1), so append an offset. Also Y=1 is in top left, so subtract the X and Y dimension
  private val xOvalPixelSizeCorrection = -1
  private val yOvalPixelSizeCorrection = -1+1 //append one pixel due to drawing oval calculate
  //-------------------------------------------------------------------------
  def buildMacro(rootBaseDirectory : String
                 , pixPos: Seq[(Float, Float)]
                 , imageHeightPixel: Int
                 , prefix: String = ""
                 , suffix: String = ""
                 , colorName: String  = "green"
                 , xOvalPixelSize : Int = 1
                 , yOvalPixelSize : Int = 1) : Unit = {

    Path.ensureDirectoryExist(rootBaseDirectory)
    val dir = Path.ensureEndWithFileSeparator(rootBaseDirectory)

    val macroFileName = dir + prefix + suffix  + ".ijm"
    val file = new PrintWriter(new File(macroFileName))

    info(s"Building a macro for ImageJ with circles around ${pixPos.size} detected sources: '" + macroFileName + "'")

    //configuration
    file.write(s"""run("Colors...", "foreground=black background=white selection=$colorName");""" + "\n")

    //circles
    pixPos.sorted.foreach { x =>
      val xPos =  x._1 + xOvalPixelSizeCorrection
      val yPos = imageHeightPixel - x._2 + yOvalPixelSizeCorrection

      file.write("makeOval(" +
          xPos + ", " +
          yPos + ", " +
          xOvalPixelSize + ", " +
          yOvalPixelSize +
          ");" +
          """roiManager("Add");""" + "\n")
    }

    //final activities
    finalActivities(file)

    file.close
    //-------------------------------------------------------------------------
  }

  //-------------------------------------------------------------------------
  private def writeOval(file: PrintWriter, x: Int, y: Int, colorName: String) =
    file.write("makeOval(" +
      x + ", " +
      y + ", " +
      xOvalPixelSize + ", " +
      yOvalPixelSize +
      ");" +
      s"""roiManager("Add", "$colorName");""" + "\n")
  //-------------------------------------------------------------------------
  private def writeRectangle(file: PrintWriter
                             , x: Int
                             , y: Int
                             , xSize: Int
                             , ySize: Int
                             , colorName: String): Unit =
    file.write("makeRectangle(" +
      x + ", " +
      y + ", " +
      xSize +
      ", " +
      ySize +
      ");" +
      s"""roiManager("Add", "$colorName");""" + "\n")

  //-------------------------------------------------------------------------
  private def writeOval(file: PrintWriter, seq: IndexedSeq[(Int,Int)], imageHeightPixel: Int, colorName: String, comment: String ): Unit = {

    file.write(s"//$comment\n")
    seq foreach { case p =>
      writeOval(file, p._1 + xOvalPixelSizeCorrection, imageHeightPixel - p._2 + yOvalPixelSizeCorrection, colorName)}
    file.write("\n")
  }
  //-------------------------------------------------------------------------
  private def writeRectangle(file: PrintWriter
                             , minPos: IndexedSeq[(Int,Int)]
                             , maxPos: IndexedSeq[(Int,Int)]
                             , imageHeightPixel: Int
                             , colorName: String
                             , comment: String ) : Unit = {

    file.write(s"//$comment\n")

    minPos.zipWithIndex.foreach { case (_, i) =>
      val min = minPos(i)
      val max = maxPos(i)

      val upperLeftX_Pos = min._1 - 1
      val upperLeftY_Pos = imageHeightPixel - max._2

      val xSize = max._1 - min._1 + 1
      val ySize = max._2 - min._2 + 1

      writeRectangle(file, upperLeftX_Pos, upperLeftY_Pos, xSize, ySize, colorName)
    }
    file.write("\n")
  }
  //-------------------------------------------------------------------------
  private def finalActivities(file: PrintWriter) : Unit = {

    //final actions
    file.write("""roiManager("Show All");

//Close ROI manager window
setBatchMode(true);
selectWindow("ROI Manager");
run("Close");
setBatchMode(false);

//End of file""" + "\n")

  }
  //-------------------------------------------------------------------------
  def buildMacroSourceRectangle(rootBaseDirectory : String
                                , geometricalCenterPos: IndexedSeq[(Int,Int)]
                                , peakPos: IndexedSeq[(Int,Int)]
                                , minPos: IndexedSeq[(Int,Int)]
                                , maxPos: IndexedSeq[(Int,Int)]
                                , imageHeightPixel: Int
                                , prefix: String = ""
                                , suffix: String = "") : Unit = {

    Path.ensureDirectoryExist(rootBaseDirectory)
    val dir = Path.ensureEndWithFileSeparator(rootBaseDirectory)

    val macroFileName = dir + prefix + suffix  + ".ijm"
    val file = new PrintWriter(new File(macroFileName))

    info(s"Building a macro for ImageJ with ${minPos.size} rectangles: '" + macroFileName + "'")

    //surround rectangles
    writeRectangle(file, minPos, maxPos, imageHeightPixel, "green", "Surround rectangles")

    //geometrical center
    writeOval(file, geometricalCenterPos, imageHeightPixel, "blue", "Geometrical centers")

    //peak center
    writeOval(file, peakPos, imageHeightPixel, "red", "Peak centers")

    //final activities
    finalActivities(file)

    //end
    file.close
    //-------------------------------------------------------------------------
  }

  //-------------------------------------------------------------------------
  def saveGaussianRectangleMacro(rootBaseDirectory : String
                                 , geometricalCenterPos: IndexedSeq[(Int,Int)]
                                 , peakPos: IndexedSeq[(Int,Int)]
                                 , minPos: IndexedSeq[(Int,Int)]
                                 , maxPos: IndexedSeq[(Int,Int)]
                                 , slicePeakPos: IndexedSeq[(Int,Int)]
                                 , sliceMinPos: IndexedSeq[(Int,Int)]
                                 , sliceMaxPos: IndexedSeq[(Int,Int)]
                                 , imageHeightPixel: Int
                                 , prefix: String = ""
                                 , suffix: String = "") : Unit = {

    Path.ensureDirectoryExist(rootBaseDirectory)
    val dir = Path.ensureEndWithFileSeparator(rootBaseDirectory)

    val macroFileName = dir + prefix + suffix  + ".ijm"
    val file = new PrintWriter(new File(macroFileName))

    info(s"Building a macro for ImageJ with ${minPos.size} rectangles: '" + macroFileName + "'")

    //slice rectangle
    writeRectangle(file, sliceMinPos, sliceMaxPos, imageHeightPixel, "orange", "Slice surround rectangles")

    //slice peak pos
    writeOval(file, slicePeakPos, imageHeightPixel, "magenta", "Slice peak centers")

    //surround rectangles
    writeRectangle(file, minPos, maxPos, imageHeightPixel, "green", "Surround rectangles" )

    //geometrical center
    writeOval(file, geometricalCenterPos, imageHeightPixel, "blue",  "Geometrical centers")

    //peak center
    writeOval(file, peakPos, imageHeightPixel, "red", "Peak centers")

    //final activities
    finalActivities(file)

    file.close
    //-------------------------------------------------------------------------
  }
  //-------------------------------------------------------------------------
  def saveGIF(name: String, bmpSeq: IndexedSeq[BufferedImage], gifSpeed: Int): Unit = {
    val gif = new AnimatedGifEncoder
    gif.start(name)
    gif.setDelay( gifSpeed)
    bmpSeq map ( gif.addFrame(_) )
    gif.finish
  }

  //-------------------------------------------------------------------------
  def buildGif(dir: String
               , outputGifName: String
               , fileExtension: String = ".fits"
               , useZscale: Boolean = true
               , speedMs: Int = 250
               , title: String = ""
               , titleX_Position: Int = 1
               , titleY_Position: Int = 8
               , imageSeqName: Seq[String]
               , labelMap: Map[String,String] = Map[String,String]()
               , labelX_Position: Int = 1
               , labelY_Position: Int = 2
               , labelColor: Color = Color.GREEN
               , labelFont: Font = new Font(Font.MONOSPACED, Font.PLAIN, 8)
               , verbose: Boolean = false
              ): Unit = {

    val gif = new AnimatedGifEncoder
    gif.start(outputGifName)
    gif.setDelay(speedMs)
    val isInputPng = fileExtension == ".png"
    imageSeqName.foreach { imageName =>
      val imagePath = s"$dir/$imageName$fileExtension"
      if (verbose) info(s"Loading image: '$imagePath'")

      val bi =
        if (isInputPng) ImageIO.read(new File(imagePath))
        else {
          val img = MyImage(imagePath)
          img.convertToBufferedImage(useZscale)
         }

      val g2d = bi.createGraphics

      //draw title
      g2d.setColor(Color.YELLOW)
      g2d.setFont(labelFont)
      if (!title.isEmpty) g2d.drawString(title
                                         , titleX_Position
                                         , titleY_Position)

      //draw label
      if (isInputPng) g2d.setColor(Color.white)
      else g2d.setColor(labelColor)
      if (!labelMap.isEmpty) g2d.drawString(labelMap(imageName)
                                            , labelX_Position
                                            , bi.getHeight - labelY_Position)

      //add frame
      gif.addFrame(bi)

      //dispose grpahics
      g2d.dispose()
    }
    gif.finish
  }
  //---------------------------------------------------------------------------
  def rename(inDir: String, outDir: String) = {
    //-------------------------------------------------------------------------
    val imageMap = new TrieMap[String, Double]()
    //-------------------------------------------------------------------------
    class ProcessImageDir(fileNameSeq: Array[String]) extends ParallelTask[String](
      fileNameSeq
      , CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 100) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(fileName: String) = {
        val img = MyImage(fileName)
        imageMap(fileName) = Time.toJulian(img.getObservingTime())
      }
      //-----------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    val fileNameSeq = Path.getSortedFileListRecursive(inDir, MyConf.c.getStringSeq("Common.fitsFileExtension"))
      .map(_.getAbsolutePath).toArray
    new ProcessImageDir(fileNameSeq)

    val oDir = Path.resetDirectory(outDir)
    imageMap.toArray.sortWith(_._2 < _._2).zipWithIndex.foreach { case ((oldFileName, _),i) =>
      val newName = f"$i%04d"
      s"cp $oldFileName $oDir/$newName.fits".!
    }
  }

  //---------------------------------------------------------------------------
  def createGifFromFile(inputFile:String
                         , outputGifName: String
                         , speedMs: Int = 250) = {

    val bufferedSource = Source.fromFile(inputFile)

    val gif = new AnimatedGifEncoder
    gif.start(outputGifName)
    gif.setDelay(speedMs)

    for (imagePath <- bufferedSource.getLines) {
      gif.addFrame(ImageIO.read(new File(imagePath)))
    }
    bufferedSource.close
    gif.finish()
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file UtilImage.scala
//=============================================================================