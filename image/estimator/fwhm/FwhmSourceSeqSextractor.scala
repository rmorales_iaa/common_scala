/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  13/Jan/2023
 * Time:  17h:42m
 * Description: None
 */
//=============================================================================
package com.common.image.estimator.fwhm
//=============================================================================
import com.common.configuration.MyConf
import com.common.dataType.tree.kdTree.K2d_TreeDouble
import com.common.geometry.point.Point2D_Double
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.estimator.bellShape2D.simple.SimpleBellShape2D
import com.common.image.myImage.MyImage
import com.common.util.file.MyFile
import com.common.util.path.Path
import com.common.util.util.Util
//=============================================================================
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
//=============================================================================
//=============================================================================
object FwhmSourceSeqSextractor {
  //---------------------------------------------------------------------------
  private val ASTROMATIC_SCRIPT_PATH = Path.ensureEndWithFileSeparator(Path.getCurrentPath()) +
                                       MyConf.c.getString("Astromatic.sextracor_psfex.scriptPath")
  private val ASTROMATIC_SCRIPT_NAME = MyConf.c.getString("Astromatic.sextracor_psfex.scriptName")
  //---------------------------------------------------------------------------
  private def matchAndAssignFwhm(imageSourceSeq: Array[Segment2D]
                                 , sextractorSourceSeq: ArrayBuffer[FwhmSourceSeqSextractor]) = {
    //---------------------------------------------------------------------------
    var sourceMap: Map[Int, Segment2D] = null
    var kdTree: K2d_TreeDouble = null
    //---------------------------------------------------------------------------
    def buildMapAndKdTree() = {

      kdTree = K2d_TreeDouble()
      sourceMap = (imageSourceSeq map { case source =>
        source.setBellShapeFit(SimpleBellShape2D(-1)) //by default is a not valid fwhm
        (source.id, source)
      }).toMap

      val seq = imageSourceSeq.map { s =>
        val centroid =  s.getCentroid()
        (centroid.toSequence(), s.id)
      }
      kdTree.addSeq(seq)
    }
    //---------------------------------------------------------------------------
    buildMapAndKdTree

    //match and assign the fwhm
    sextractorSourceSeq.foreach { sextractorSource =>
      val centroidSextractorSource = sextractorSource.pixPos
      kdTree.getKNN(Seq(centroidSextractorSource.x, centroidSextractorSource.y), requestedK = 1).foreach { t =>
        val source = sourceMap(t.v)
         val distancPix = source.getCentroid().getDistance(centroidSextractorSource)
        if (distancPix < 1)
          source.setBellShapeFit(SimpleBellShape2D(sextractorSource.fwhm))
      }
    }
    //---------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //run sextractor and the pfsex to get the fwhm
  def estimate(image: MyImage
              , imageSourceSeq: Array[Segment2D]
              ): Unit= {

    val sextractorSourceSeq = ArrayBuffer[FwhmSourceSeqSextractor]()

    //call the script and get the results
    //sextractor is a modified version that generates a file with the Background and BackgroundRMS
    val command = s"$ASTROMATIC_SCRIPT_PATH/$ASTROMATIC_SCRIPT_NAME $ASTROMATIC_SCRIPT_PATH '${image.name}' ${image.id}.cat sextractor_ascii.config"
    Util.runShellSimple(command)

    val catName = ASTROMATIC_SCRIPT_PATH + s"${image.id}.cat"
    if (!MyFile.fileExist(catName)) return

    //parse the catalog and build the result
    val bufferedSource = Source.fromFile(catName)
    for (line <- bufferedSource.getLines) {
      val s = line.trim
      if (s.startsWith("#")) None
      else {
        val seq = s.replaceAll(" +", " ").split(" ")
        sextractorSourceSeq += FwhmSourceSeqSextractor(
          Point2D_Double(seq(4).toDouble
                        ,seq(5).toDouble)
        , seq(7).toDouble)
      }
    }
    bufferedSource.close

    //match and assign the fwhm, cleaning the sextractor sources loaded
    matchAndAssignFwhm(imageSourceSeq, sextractorSourceSeq)
    sextractorSourceSeq.clear()

    //delete the files generated
    Util.runShellSimple(s"rm -f $ASTROMATIC_SCRIPT_PATH/${image.id}.*")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class FwhmSourceSeqSextractor(pixPos:Point2D_Double, fwhm: Double)
//=============================================================================
//End of file FwhSourceSeqSextractor.scala
//=============================================================================
