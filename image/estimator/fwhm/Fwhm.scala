/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Feb/2024
 * Time:  22h:57m
 * Description: None
 */
package com.common.image.estimator.fwhm
//=============================================================================
import com.common.configuration.MyConf
import com.common.geometry.point.Point2D
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.estimator.bellShape2D.BellShape2D_Fit
import com.common.image.myImage.MyImage
import com.common.logger.MyLogger
import com.common.stat.StatDescriptive
import estimator.fwhm.FwhmSourceExtraction
//=============================================================================
//=============================================================================
object Fwhm extends MyLogger {
  //---------------------------------------------------------------------------
  //fwhm estimate algorithm
  private val FWHM_IMAGE_ALGORITHM = MyConf.c.getString("FWHM.image.algorithm")
  private val FWHM_IMAGE_FALLBACK_ALGORITHM = MyConf.c.getString("FWHM.image.fallback")
  //---------------------------------------------------------------------------
  private final val FWHM_IMAGE_ALGORITHM_PSFEX       = "psfex" //calculated by default when image backgroud is calculated
  private final val FWHM_IMAGE_ALGORITHM_SEXTRACTOR  = "sextractor"
  private final val FWHM_IMAGE_ALGORITHM_M2          = "m2"
  private final val FWHM_IMAGE_ALGORITHM_SIMPLE      = "simple"
  //---------------------------------------------------------------------------
  private final val FWHM_IMAGE_ALGORITHM_M2_SIGMASCALE = MyConf.c.getDouble("FWHM.algorithm.m2.sigmaScale")
  //---------------------------------------------------------------------------
  private final val BELL_SHAPE_ESTIMATION_ALGORITHM = BellShape2D_Fit.getAlgorithmCode(MyConf.c.getInt("BellShapeFit.fitAlgorithm"))
  private final val BELL_SHAPE_SAMPLING_REGION = Point2D(MyConf.c.getInt("BellShapeFit.sampligRegionX"), MyConf.c.getInt("BellShapeFit.sampligRegionY"))
  //---------------------------------------------------------------------------
  def calculate(img: MyImage
                , sourceSeq: Array[Segment2D]
                , verbose: Boolean) = {
    //-------------------------------------------------------------------------
    def updateAverageFwhm(algorithmName: String) = {
      val fwhmSeq = sourceSeq
        .map(_.getFwhm())
        .filter(fwhm => fwhm > 0 && fwhm != Double.NaN)
      val calculatedFWHM = StatDescriptive.getWithDouble(
        StatDescriptive.sigmaClippingDouble(fwhmSeq, sigmaScale = FWHM_IMAGE_ALGORITHM_M2_SIGMASCALE)).median
      img.setFwhmAverage(calculatedFWHM, algorithmName, verbose)
    }
    //-------------------------------------------------------------------------
    FWHM_IMAGE_ALGORITHM match {
      case FWHM_IMAGE_ALGORITHM_PSFEX => //calculated by default when image backgroud is calculated

      case FWHM_IMAGE_ALGORITHM_SEXTRACTOR =>
        FwhmSourceSeqSextractor.estimate(img, sourceSeq)
        updateAverageFwhm(FWHM_IMAGE_ALGORITHM_SEXTRACTOR)

      case FWHM_IMAGE_ALGORITHM_M2 =>
        FwhmSourceExtraction.estimate(img, sourceSeq, BELL_SHAPE_SAMPLING_REGION, BELL_SHAPE_ESTIMATION_ALGORITHM)
        updateAverageFwhm(s"$FWHM_IMAGE_ALGORITHM_M2:$BELL_SHAPE_ESTIMATION_ALGORITHM")

      case FWHM_IMAGE_ALGORITHM_SIMPLE =>
        FwhmSimpleEstimation.estimate(sourceSeq)
        updateAverageFwhm(FWHM_IMAGE_ALGORITHM_SIMPLE)
    }

    val imageFwhm = img.getFwhmAverage()
    if (imageFwhm <= 0) {
      if (verbose) warning(s"Image: '${img.getRawName()}' has invalid image fwhm estimated by algorithm:'$FWHM_IMAGE_ALGORITHM' Using alternative algorithm estimate:'$FWHM_IMAGE_FALLBACK_ALGORITHM'")

      FWHM_IMAGE_FALLBACK_ALGORITHM match {

        case FWHM_IMAGE_ALGORITHM_PSFEX => //calculated by default when image backgroud is calculated

        case FWHM_IMAGE_ALGORITHM_SEXTRACTOR =>
          if (FWHM_IMAGE_ALGORITHM != FWHM_IMAGE_FALLBACK_ALGORITHM) {
            FwhmSourceSeqSextractor.estimate(img, sourceSeq)
            updateAverageFwhm(FWHM_IMAGE_ALGORITHM_SEXTRACTOR)
          }
          else img.setFwhmAverage(0, FWHM_IMAGE_ALGORITHM, verbose)

        case FWHM_IMAGE_ALGORITHM_M2 =>
          if (FWHM_IMAGE_ALGORITHM != FWHM_IMAGE_FALLBACK_ALGORITHM) {
            FwhmSourceExtraction.estimate(img, sourceSeq, BELL_SHAPE_SAMPLING_REGION, BELL_SHAPE_ESTIMATION_ALGORITHM)
            updateAverageFwhm(s"$FWHM_IMAGE_ALGORITHM_M2:$BELL_SHAPE_ESTIMATION_ALGORITHM")
          }
          else img.setFwhmAverage(0, FWHM_IMAGE_ALGORITHM, verbose)

        case FWHM_IMAGE_ALGORITHM_SIMPLE =>
          if (FWHM_IMAGE_ALGORITHM != FWHM_IMAGE_FALLBACK_ALGORITHM) {
            FwhmSimpleEstimation.estimate(sourceSeq)
            updateAverageFwhm(FWHM_IMAGE_ALGORITHM_SIMPLE)
          }
          else img.setFwhmAverage(0, FWHM_IMAGE_ALGORITHM, verbose)
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Fwhm.scala
//=============================================================================