/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Feb/2024
 * Time:  22h:48m
 * Description: None
 */
package com.common.image.estimator.fwhm
//=============================================================================
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.estimator.bellShape2D.simple.SimpleBellShape2D
//=============================================================================
//=============================================================================
object FwhmSimpleEstimation {
  //---------------------------------------------------------------------------
  def estimate(imageSourceSeq: Array[Segment2D]): Unit = {
    imageSourceSeq.foreach { source=>
      val fwhm = source.calculateSimpleFwhmEstimation()
      source.setBellShapeFit(SimpleBellShape2D(fwhm))
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FwhmSimpleEstimation.scala
//=============================================================================