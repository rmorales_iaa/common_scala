/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  14/Feb/2024
 * Time:  11h:17m
 * Description: None
 */
package estimator.fwhm

import com.common.geometry.matrix.matrix2D.Matrix2D.fitGaussian2D
import com.common.geometry.point.Point2D
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.estimator.bellShape2D.BellShape2D_Fit.{BELL_SHAPE_2D_FIT_ALGORITHM, MOFFAT_ELLIPTICAL_ROTATED_FIT}
import com.common.image.myImage.MyImage
//=============================================================================
//=============================================================================
object FwhmSourceExtraction {
  //---------------------------------------------------------------------------
  def estimate(img: MyImage
               , sourceSeq: Array[Segment2D]
               , samplingRegion: Point2D = Point2D(0, 0) //extra pix size around the source
               , gaussian2D_EstimationAlgorithm: BELL_SHAPE_2D_FIT_ALGORITHM = MOFFAT_ELLIPTICAL_ROTATED_FIT): Unit =
    fitGaussian2D(img, sourceSeq, samplingRegion, gaussian2D_EstimationAlgorithm).zipWithIndex.foreach { case (gaussian, i) =>
      if (gaussian.isDefined) {
        val source = sourceSeq(i)
        val fwhm = gaussian.get.fwhm
        if (source.isValidFWHM(fwhm))
          source.setBellShapeFit(gaussian.get)
      }
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FwhmSourceExtraction.scala
//=============================================================================