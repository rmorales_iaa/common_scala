/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Jan/2024
 * Time:  14h:05m
 * Description: None
 */
package com.common.image.estimator.flux
//=============================================================================
import com.common.configuration.MyConf
import com.common.dataType.pixelDataType.PixelDataType.{PIXEL_DATA_TYPE, PIXEL_ZERO_VALUE}
import com.common.database.mongoDB.database.MatchedImageSource
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.estimator.flux.ApertureTrait.APERTURE_ALGORITHM
import com.common.image.myImage.MyImage
import com.common.logger.MyLogger
//=============================================================================
//=============================================================================
object ApertureTrait {
  //---------------------------------------------------------------------------
  final val APERTURE_ALGORITHM_CLASSIC_ID                          = 1
  final val APERTURE_ALGORITHM_BY_SOURCE_EXTRACTION_ID             = 2
  final val APERTURE_ALGORITHM_BY_MEDIAN_PIX_ANULAR_CIRCLE_ID      = 3
  //---------------------------------------------------------------------------
  sealed trait APERTURE_ALGORITHM { val name: String; val id: Int }
  //---------------------------------------------------------------------------
  //high precission algorithms
  case object APERTURE_ALGORITHM_CLASSIC                     extends APERTURE_ALGORITHM { val name = "APERTURE_ALGORITHM_CLASSIC"; val id=APERTURE_ALGORITHM_CLASSIC_ID}
  case object APERTURE_ALGORITHM_BY_SOURCE_EXTRACTION        extends APERTURE_ALGORITHM { val name = "APERTURE_ALGORITHM_BY_SOURCE_EXTRACTION"; val id=APERTURE_ALGORITHM_BY_SOURCE_EXTRACTION_ID}
  case object APERTURE_ALGORITHM_BY_MEDIAN_PIX_ANULAR_CIRCLE extends APERTURE_ALGORITHM { val name = "APERTURE_ALGORITHM_BY_MEDIAN_PIX_ANULAR_CIRCLE"; val id=APERTURE_ALGORITHM_BY_MEDIAN_PIX_ANULAR_CIRCLE_ID}
  //---------------------------------------------------------------------------
  private val minPixelInPercentage = MyConf.c.getInt("Photometry.sourceMinPixelInPercentage")
  private val stretchPercentage = MyConf.c.getFloat("Photometry.stretchPercentage")
  private val maxAllowedSourceSizeProportion = MyConf.c.getFloat("Photometry.maxAllowedSourceSizeProportion")
  private val maxAlloedSourceProportionInverse = 1 / maxAllowedSourceSizeProportion
  //---------------------------------------------------------------------------
  def getApertureRadio(source: Segment2D
                       , minApertureRadio: Double
                       , annularApertureRadio: Double
                       , maxApertureRadio: Double
                       , apertureCoef: (Boolean, Double, Double, Double, Double)) = {
    val (isFixedAperture, imageFWHM, minApertureCoef, annularApertureCoef, maxApertureCoef) = apertureCoef

    if (isFixedAperture) (minApertureRadio, annularApertureRadio, maxApertureRadio)
    else {
      var fwhm = source.getFwhm()
      if (fwhm == -1) fwhm = imageFWHM
      val _minApertureRadio     = fwhm * minApertureCoef
      val _annularApertureRadio = _minApertureRadio + annularApertureCoef
      val _maxApertureRadio     = _annularApertureRadio + maxApertureCoef
      (_minApertureRadio, _annularApertureRadio, _maxApertureRadio)
    }
  }
  //---------------------------------------------------------------------------
  def hasValidMorphology(source: Segment2D): Boolean = {
    if (source.getData().isEmpty) return true
    val m = source.toMatrix2D()
    val s = m.toSegment2D_ByDetection().get
    val mo = s.getMorphology()
    if (mo.hasUniqueCol) return false
    if (mo.hasUniqueRow) return false
    if (mo.pixelInPercentage < minPixelInPercentage) return false
    if (mo.sizeProportion > maxAllowedSourceSizeProportion || mo.sizeProportion < maxAlloedSourceProportionInverse) false
    else true
  }

  //---------------------------------------------------------------------------
  def isValidAfterStretching(source: Segment2D) = {
    val m = source.toMatrix2D()
    //append this percentage to the min value and detect sources again. This is a affine_transformation way to detect local min and also blended sources
    val min = m.data.filter(_ > PIXEL_ZERO_VALUE).sorted.min
    val newNoiseTide = min + (min * (stretchPercentage * 0.01))
    m.findSource(newNoiseTide).toSegment2D_seq().length < 2 //if 2 or more sources are found, then original source is blended
  }
  //---------------------------------------------------------------------------
  def calculate(img: MyImage
                , matchedImageSourceSeq: Array[MatchedImageSource]
                , minApertureRadio: Double
                , annularApertureRadio: Double
                , maxApertureRadio: Double
                , apertureCoef:(Boolean,Double,Double,Double)
                , noiseTide: PIXEL_DATA_TYPE
                , sourceMaxPix: Int
                , isMPO: Boolean = false
                , useStretchingHeuristic: Boolean = false
                , allowNegativeFlux: Boolean = false
                , verbose: Boolean = false
               ): String = {

    val algorithmID = MyConf.c.getInt("Photometry.algorithmID")
    algorithmID match {
      case APERTURE_ALGORITHM_CLASSIC.id =>
        ApertureClassic().calculate(
          img
          , matchedImageSourceSeq
          , minApertureRadio
          , annularApertureRadio
          , maxApertureRadio
          , isMPO
          , useStretchingHeuristic
          , allowNegativeFlux
          , noiseTide
          , sourceMaxPix
          , verbose)

      case APERTURE_ALGORITHM_BY_SOURCE_EXTRACTION.id =>
        ApertureBySourceExtraction().calculate(
          img
          , matchedImageSourceSeq
          , minApertureRadio
          , annularApertureRadio
          , maxApertureRadio
          , isMPO
          , useStretchingHeuristic
          , allowNegativeFlux
          , noiseTide
          , sourceMaxPix
          , verbose)

      case APERTURE_ALGORITHM_BY_MEDIAN_PIX_ANULAR_CIRCLE.id =>
        ApertureByMedianPixAnularCircle().calculate(
          img
          , matchedImageSourceSeq
          , minApertureRadio
          , annularApertureRadio
          , maxApertureRadio
          , isMPO
          , useStretchingHeuristic
          , allowNegativeFlux
          , noiseTide
          , sourceMaxPix
          , verbose)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
trait ApertureTrait extends MyLogger {
  //---------------------------------------------------------------------------
  val algorithmName: APERTURE_ALGORITHM
  //---------------------------------------------------------------------------
  def calculate(img: MyImage
                , matchedImageSourceSeq: Array[MatchedImageSource]
                , minApertureRadio: Double
                , annularApertureRadio: Double
                , maxApertureRadio: Double
                , isMPO: Boolean = false
                , useStretchingHeuristic: Boolean = false
                , allowNegativeFlux: Boolean = false
                , noiseTide: PIXEL_DATA_TYPE
                , sourceMaxPix: Int
                , verbose: Boolean = false): String
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Aperture.scala
//=============================================================================