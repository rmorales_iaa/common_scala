/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Jan/2024
 * Time:  14h:04m
 * Description: None
 */
package com.common.image.estimator.flux
//=============================================================================
import com.common.dataType.pixelDataType.PixelDataType.PIXEL_DATA_TYPE
import com.common.database.mongoDB.database.MatchedImageSource
import com.common.database.mongoDB.database.gaia.GaiaPhotometricSource
import com.common.geometry.point.Point2D_Double
import com.common.image.estimator.flux.ApertureTrait._
import com.common.image.myImage.MyImage
//=============================================================================
case class ApertureClassic() extends ApertureTrait {
  //---------------------------------------------------------------------------
  val algorithmName = APERTURE_ALGORITHM_CLASSIC
  //---------------------------------------------------------------------------
  def calculate(img: MyImage
                , matchedImageSourceSeq: Array[MatchedImageSource]
                , minApertureRadio: Double
                , annularApertureRadio: Double
                , maxApertureRadio: Double
                , isMPO: Boolean = false
                , useStretchingHeuristic: Boolean = false
                , allowNegativeFlux: Boolean = false
                , noiseTide: PIXEL_DATA_TYPE
                , sourceMaxPix: Int
                , verbose: Boolean = false): String = {

    if (verbose) info(s"Image: '${img.getRawName()}' calculating flux on: ${matchedImageSourceSeq.size} sources using apertures")
    val exposureTime = img.getExposureTime()
    val imageBackground = img.getBackground(verbose)

    matchedImageSourceSeq.foreach { matchedSource =>
      val source = matchedSource.imageSource
      val centroid = source.getCentroid() - Point2D_Double.POINT_ONE //grid intersection matrix need offset (0,0)
      val minApertureArea = Math.PI * minApertureRadio * minApertureRadio
      val backgroundSamplingArea = Math.PI * ((maxApertureRadio * maxApertureRadio) - (annularApertureRadio * annularApertureRadio)) //difference of two circles: max - anular
      val validMorphology = hasValidMorphology(source)
      val validAfterStretching =
        if (!useStretchingHeuristic) true
        else isValidAfterStretching(source)

      if (centroid.x >= 0 && centroid.y >= 0) {
        //special actions in case of mpo
        if (isMPO) {
          if (!validMorphology) return " Invalid source morphology"
          if (!validAfterStretching) return " Source is blended to other/s sources according to stretching algorithm"
        }
        if (img != null && validMorphology && validAfterStretching) {

          val minApertureFlux = img.getFluxWithApertureByCircleGridIntersection(centroid, minApertureRadio, paddingValue = imageBackground)
          val annularApertureFlux = img.getFluxWithApertureByCircleGridIntersection(centroid, annularApertureRadio, paddingValue = imageBackground)
          val maxApertureFlux = img.getFluxWithApertureByCircleGridIntersection(centroid, maxApertureRadio, paddingValue = imageBackground)
          val estFluxPerUnitArea = (maxApertureFlux - annularApertureFlux) / backgroundSamplingArea
          val estLocalBackgroundInMinAperture = estFluxPerUnitArea * minApertureArea

          //avoid invalid calculations (usually because the borders)
          if (minApertureFlux > 0 &&
            annularApertureFlux > 0 &&
            maxApertureFlux > 0 &&
            estFluxPerUnitArea > 0) {
            val flux = minApertureFlux - estLocalBackgroundInMinAperture //Also known as ADU: Analog to digital unit
            val snr = flux / Math.sqrt(flux + estLocalBackgroundInMinAperture) //signal to noise ratio
            val fluxPerSecond = flux / exposureTime
            if (fluxPerSecond > 0 || allowNegativeFlux)
              matchedSource
                .catalogSource
                .asInstanceOf[GaiaPhotometricSource]
                .setCalculatedInfo(fluxPerSecond
                  , snr = if (allowNegativeFlux && flux < 0) 0 else snr
                  , estFluxPerUnitArea)
          }
        }
      }
    }
    ""
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//=============================================================================
//End of file ApertureClassic.scala
//=============================================================================