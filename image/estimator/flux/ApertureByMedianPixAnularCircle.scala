/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Jan/2024
 * Time:  14h:04m
 * Description: None
 */
package com.common.image.estimator.flux
//=============================================================================
import com.common.dataType.pixelDataType.PixelDataType.PIXEL_DATA_TYPE
import com.common.database.mongoDB.database.MatchedImageSource
import com.common.database.mongoDB.database.gaia.GaiaPhotometricSource
import com.common.geometry.circle.CircleDouble
import com.common.geometry.point.{Point2D_Double}
import com.common.image.estimator.flux.ApertureTrait._
import com.common.image.myImage.MyImage
import com.common.stat.StatDescriptive
//=============================================================================
object ApertureByMedianPixAnularCircle {
  //---------------------------------------------------------------------------
  def estimatedBackground(img: MyImage
                          , centroid: Point2D_Double
                          , annularApertureRadio: Double
                          , maxApertureRadio: Double) = {

    val annularCircle = CircleDouble(centroid,annularApertureRadio)
    val maxCircle = CircleDouble(centroid,maxApertureRadio)

    val maxPos = maxCircle.getMax().toPoint2D()
    val minPos = maxCircle.getMin().toPoint2D()

    val minX = minPos.x
    val minY = minPos.y

    val maxX = maxPos.x
    val maxY = maxPos.y

    val pixSeq = (for (x<-minX to maxX;
                       y<-minY to maxY) yield {
      val pos = Point2D_Double(x,y)
      if (!annularCircle.isIn(pos) && maxCircle.isIn(pos)) Some(img(pos.toPoint2D()))
      else None
    }).flatten.toArray

    StatDescriptive.getWithDouble(pixSeq).median
  }
  //---------------------------------------------------------------------------
}
case class ApertureByMedianPixAnularCircle() extends ApertureTrait {
  //---------------------------------------------------------------------------
  val algorithmName: APERTURE_ALGORITHM = APERTURE_ALGORITHM_BY_MEDIAN_PIX_ANULAR_CIRCLE
  //---------------------------------------------------------------------------
  def calculate(img: MyImage
                , matchedImageSourceSeq: Array[MatchedImageSource]
                , minApertureRadio: Double
                , annularApertureRadio: Double
                , maxApertureRadio: Double
                , isMPO: Boolean = false
                , useStretchingHeuristic: Boolean = false
                , allowNegativeFlux: Boolean = false
                , noiseTide: PIXEL_DATA_TYPE
                , sourceMaxPix: Int
                , verbose: Boolean = false): String = {

    if (verbose) info(s"Image:'${img.getRawName()}' calculating flux on: ${matchedImageSourceSeq.size} sources using apertures")
    val exposureTime = img.getExposureTime()
    matchedImageSourceSeq.foreach { matchedSource =>
      val source = matchedSource.imageSource
      val centroid = source.getCentroid() - Point2D_Double.POINT_ONE //grid intersection matrix need offset (0,0)
      val minApertureArea = Math.PI * minApertureRadio * minApertureRadio
      val validMorphology = hasValidMorphology(source)
      val validAfterStretching =
        if (!useStretchingHeuristic) true
        else isValidAfterStretching(source)

      if (centroid.x >= 0 && centroid.y >= 0) {

        //special actions in case of mpo
        if (isMPO) {
          if (!validMorphology) return " Invalid source morphology"
          if (!validAfterStretching) return " Source is blended to other/s sources according to stretching algorithm"
        }

        if (img != null && validMorphology && validAfterStretching) {
          val estimatedBackground = ApertureByMedianPixAnularCircle.estimatedBackground(
            img
            , centroid
            , annularApertureRadio
            , maxApertureRadio)
          //avoid invalid calculations (usually because the borders)
          if (estimatedBackground < 0) None
          else {
            val minApertureFlux = img.getFluxWithApertureByCircleGridIntersection(centroid, minApertureRadio, img.getBackground(verbose))
            val flux = minApertureFlux - (estimatedBackground * minApertureArea) //Also known as ADU: Analog to digital unit
            val snr = flux / Math.sqrt(flux + (estimatedBackground * minApertureArea)) //signal to noise ratio
            val fluxPerSecond = flux / exposureTime
            if (fluxPerSecond > 0 || allowNegativeFlux) //Possible error estimating the background
              matchedSource
                .catalogSource
                .asInstanceOf[GaiaPhotometricSource]
                .setCalculatedInfo(fluxPerSecond
                  , snr = if (allowNegativeFlux && flux < 0) 0 else snr
                  , estimatedBackground)
          }
        }
      }
      else None
    }
    ""
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//=============================================================================
//End of file ApertureByMedianPixAnularCircle.scala
//=============================================================================