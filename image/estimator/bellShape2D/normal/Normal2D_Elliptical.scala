/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Nov/2020
 * Time:  09h:42m
 * Description: it calculates a elliptical gaussain PSF fitting
 */
//=============================================================================
package com.common.image.estimator.bellShape2D.normal
//=============================================================================
import org.apache.commons.math3.analysis.{MultivariateMatrixFunction, MultivariateVectorFunction}
import org.apache.commons.math3.fitting.leastsquares.{LeastSquaresBuilder, LevenbergMarquardtOptimizer}
import org.apache.commons.math3.special.Erf.erf
import java.lang.Math._
//=============================================================================
import com.common.geometry.matrix.matrix2D.Matrix2D
import com.common.geometry.point.Point2D_Double
import com.common.math.MyMath
import com.common.image.estimator.bellShape2D.BellShape2D_Fit
import com.common.image.estimator.bellShape2D.simple.Gaussian2D_Simple
//=============================================================================
object Normal2D_Elliptical {
  //---------------------------------------------------------------------------
  //https://stats.stackexchange.com/questions/198362/how-to-define-a-2d-gaussian-using-1d-variance-of-component-gaussians
  //Asumming that x and y are independients. In other case use the bivariate distribution
  //    https://en.wikipedia.org/wiki/Multivariate_normal_distribution
  // Assuming that (x0,y0) = (meanX, meanY)
  // f(x,y,B,x_0,y_0,s_x,s_y):= B + ((1/(2*%pi*s_x*s_y))*exp(-((x-x_0)^2/(2*s_x^2))-((y-y_0)^2/(2*s_y^2))));
  //where B = background
  ///Hint: use an computational algebra package as 'wxmaxima'  to evaluate the function and list the jacobian
  // Based on :
  // https://pixinsight.com/doc/tools/DynamicPSF/DynamicPSF.html
  //---------------------------------------------------------------------------
  //evaluates the circular gaussian
  def evaluateFunction(x: Double,y: Double, parameterSeq: Array[Double]) = {
    val B  = parameterSeq(0) //background
    val x0 = parameterSeq(1) //meanX = centroid x
    val y0 = parameterSeq(2) //meanY = centroid y
    val sx = parameterSeq(3) //std_devX
    val sy = parameterSeq(4) //std_devY

    //precalculated values
    val xC = x-x0
    val yC = y-y0
    val xC2 = xC * xC
    val yC2 = yC * yC

    val sx2 = sx * sx
    val sy2 = sy * sy

    val e =  exp (-(xC2 / (2 * sx2)) - (yC2 / (2 * sy2)))

    //evaluate
    B + (1 / (2 * PI * sx * sy) ) * e
  }
  //---------------------------------------------------------------------------
  //evaluates the jacobian of a circular gaussian
  def evaluateJacobian(x: Double, y: Double, parameterSeq : Array[Double]) = {
    val x0 = parameterSeq(1) //meanX = centroid x
    val y0 = parameterSeq(2) //meanY = centroid y
    val sx = parameterSeq(3) //std_devX
    val sy = parameterSeq(4) //std_devY

    //precalculated values
    val xC = x-x0
    val yC = y-y0
    val xC2 = xC * xC
    val yC2 = yC * yC

    val sx2 = sx * sx
    val sx3 = sx2 * sx
    val sx4 = sx3 * sx

    val sy2 = sy * sy
    val sy3 = sy2 * sy
    val sy4 = sy3 * sy

    val e =  exp (-(xC2 / (2d * sx2)) - (yC2 / (2d * sy2)))
    val twoPi = 2 * PI

    //process the array of partial derivates
    Array( 1 //df(x,y)/background
        , (xC * e) / (twoPi * sx3 * sy) //df(x,y)/meanX
        , (yC * e) / (twoPi * sx * sy3) //df(x,y)/meanX
        , ((xC2 * e) / (twoPi * sx4 * sy)) - (e / (twoPi * sx2 * sy)) //df(x,y)/dstdDevX
        , ((yC2 * e) / (twoPi * sx * sy4)) - (e / (twoPi * sx * sy2))//df(x,y)/dstdDevY
      )
  }
  //---------------------------------------------------------------------------
  private def fitter(m: Matrix2D, maxEvaluations: Int = 1000, maxIterations: Int = 100) : Option[Array[Double]] = {
    val gaussian = Gaussian2D_Simple.fit(m)
    val initalParameterGuess = Array(   //set to 1 if no initial guessing
        gaussian.background
      , gaussian.centroid.x  //mean X
      , gaussian.centroid.y  //mean Y
      , gaussian.stdX
      , gaussian.stdY
    )
    //-------------------------------------------------------------------------
    //model function
    class FunctionModel extends MultivariateVectorFunction {
      @throws[IllegalArgumentException]
      def value(paremeterSeq: Array[Double]) = {
        val r  = (for(y<- 0 until m.yMax;x<-0 until m.xMax) yield evaluateFunction(x, y, paremeterSeq)).toArray
        r
      }
    }
    //-------------------------------------------------------------------------
    //model function jacobian
    class JacobianOfFunctionModel extends MultivariateMatrixFunction {
      @throws[IllegalArgumentException]
      def value(paremeterSeq: Array[Double]) = {
        val r = (for(y<- 0 until m.yMax;x<-0 until m.xMax) yield evaluateJacobian(x, y, paremeterSeq)).toArray
        r
      }
    }
    //-------------------------------------------------------------------------
    val lsb = new LeastSquaresBuilder

    //set model distribution.function and its jacobian
    lsb.model(new FunctionModel, new JacobianOfFunctionModel)
      .target(m.data map (_.toDouble))
      .start(initalParameterGuess)
      .maxEvaluations(maxEvaluations) //set upper limit of evaluation time
      .maxIterations(maxIterations)   //set upper limit of iteration time
    try {
      val optimizer = new LevenbergMarquardtOptimizer().optimize(lsb.build)
      Some(optimizer.getPoint.toArray)
    }
    catch {
      case _: Exception =>  None
    }
  }
  //---------------------------------------------------------------------------
  //the image must be centred (approximately) on the source to be used
  def fit(m: Matrix2D) : Option[Normal2D_Elliptical] = {
    val r = fitter(m).getOrElse(return None)

    val B  = r(0) //background
    val x_0 = r(1) //meanX = centroid x
    val y_0 = r(2) //meanY = centroid y
    val sx = r(3) //std_devX
    val sy = r(4) //std_devY
    val centroid = Point2D_Double(x_0,y_0)

    Some(new Normal2D_Elliptical(
       B
      , centroid
      , Point2D_Double(m.offset)
      , sx
      , sy
    ))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Normal2D_Elliptical(background : Double
                               , centroid: Point2D_Double
                               , offset : Point2D_Double
                               , stdX: Double
                               , stdY: Double
                               , fwhmX: Double
                               , fwhmY: Double)  extends BellShape2D_Fit {
  //---------------------------------------------------------------------------
  def this(background : Double
           , centroid: Point2D_Double
           , offset : Point2D_Double
           , stdX: Double
           , stdY: Double) = this(background
    , centroid
    , offset
    , stdX
    , stdY
    , MyMath.FWHM_COEFFICIENT * stdX
    , MyMath.FWHM_COEFFICIENT * stdY)
  //---------------------------------------------------------------------------
  val amplitude = 1 / (2 * PI * stdX * stdY)
  //---------------------------------------------------------------------------
  def at(x: Double, y:Double) =
    Normal2D_Elliptical.evaluateFunction(x,y,Array(background, centroid.x, centroid.y, stdX, stdY))
  //---------------------------------------------------------------------------
  override def integrate(mx:Double, Mx: Double, my:Double, My: Double) : Double = {
    val sx = stdX
    val sy = stdY
    val x0 = centroid.x
    val y0 = centroid.y
    val B = background

    (-(PI*sx*sy*erf((sqrt(2)*x0-sqrt(2)*Mx)/(2*sx))-
       PI*sx*sy*erf((sqrt(2)*x0-sqrt(2)*mx)/(2*sx)))*
       erf((sqrt(2)*y0-sqrt(2)*my)/(2*sy))+
       (PI*sx*sy*erf((sqrt(2)*x0-sqrt(2)*Mx)/(2*sx))-
         PI*sx*sy*erf((sqrt(2)*x0-sqrt(2)*mx)/(2*sx)))*
         erf((sqrt(2)*y0-sqrt(2)*My)/(2*sy))-
      (4*PI*B*Mx-4*PI*B*mx)*my*sx*sy+(4*PI*B*Mx*My-4*PI*B*My*mx)*sx*sy)/(4*PI*sx*sy)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Normal2D_Elliptical.scala
//=============================================================================
