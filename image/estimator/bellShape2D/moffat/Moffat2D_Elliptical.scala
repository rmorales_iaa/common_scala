/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Nov/2020
 * Time:  09h:42m
 * Description: it calculates a elliptical Moffat PSF fitting
 * Based on :
 *   http://www.aspylib.com/doc/aspylib_fitting.html
 *   https://pixinsight.com/doc/tools/DynamicPSF/DynamicPSF.html
 *   http://openafox.com/science/peak-function-derivations.html
 */
//=============================================================================
package com.common.image.estimator.bellShape2D.moffat
//=============================================================================
import com.common.geometry.matrix.matrix2D.Matrix2D
import com.common.geometry.point.Point2D_Double
import com.common.image.estimator.bellShape2D.BellShape2D_Fit
import com.common.image.estimator.bellShape2D.simple.Gaussian2D_Simple
import math._
//=============================================================================
import org.apache.commons.math3.analysis.{MultivariateMatrixFunction, MultivariateVectorFunction}
import org.apache.commons.math3.fitting.leastsquares.{LeastSquaresBuilder, LevenbergMarquardtOptimizer}
//=============================================================================
//=============================================================================
object Moffat2D_Elliptical {
  //---------------------------------------------------------------------------
  //Based on: https://pixinsight.com/doc/tools/DynamicPSF/DynamicPSF.html
  //M(x,y,G,A,x_0,y_0,B,c_x,c_y):= G + ( A / (1+((x-x_0)^2 /c_x^2)+((y-y_0)^2 /c_y^2))^B);
  //where A = Amplitude, B = background, cx = core in x dimension and cy = core in y dimension
  ///Hint: use an computational algebra package as 'wxmaxima'  to evaluate the function and list the jacobian
  //---------------------------------------------------------------------------
  //evaluates the function
  def evaluateFunction(x: Double, y: Double, parameterSeq: Array[Double]) = {
    val A   = parameterSeq(0) //A = height = Amplitude
    val G   = parameterSeq(1) //background
    val x_0 = parameterSeq(2) //centroid X
    val y_0 = parameterSeq(3) //centroid Y
    val cx  = parameterSeq(4) //coreX
    val cy  = parameterSeq(5) //coreY
    val B   = parameterSeq(6) //B = shape of the fitting function

    //precalculated values
    val xC = x - x_0
    val xC2 = xC * xC

    val yC = y - y_0
    val yC2 = yC * yC

    val cX2 = cx * cx
    val cY2 = cy * cy

    val t = 1+(xC2/cX2)+(yC2/cY2)
    val powB = pow(t, B)
    G+(A/powB)
  }
  //---------------------------------------------------------------------------
  //evaluates the jacobian
  def evaluateJacobian(x: Double, y: Double, parameterSeq : Array[Double]) = {
    //-------------------------------------------------------------------------
    val A   = parameterSeq(0) //A = height = Amplitude
    val G   = parameterSeq(1) //background
    val x_0 = parameterSeq(2) //centroid X
    val y_0 = parameterSeq(3) //centroid Y
    val cx  = parameterSeq(4) //coreX
    val cy  = parameterSeq(5) //coreY
    val B   = parameterSeq(6) //B = shape of the fitting function

    //precalculated values
    val xC = x-x_0
    val xC2 = xC * xC

    val yC = y-y_0
    val yC2 = yC * yC

    val cX2 = cx * cx
    val cY2 = cy * cy

    val cX3 = cX2 * cx
    val cY3 = cX2 * cy
    
    val t = 1+(xC2/cX2)+(yC2/cY2)
    val powB = pow(t, B)
    val powB_1 = pow(t, -B-1)

    val twoAB = 2 * A * B

    //process the array of partial derivates
    Array(
      1/powB                               //dG(x,y)/dA
      , 1                                  //dG(x,y)/dG
      , (twoAB*xC*powB_1)/cX2              //dG(x,y)/dx_0
      , (twoAB*powB_1*yC)/cY2              //dG(x,y)/dy_0
      , (twoAB*xC2*powB_1)/cX3             //dG(x,y)/dsx
      , (twoAB*powB_1*yC2)/cY3             //dG(x,y)/dsy
      ,  -(A*log(t))/powB                  //dG(x,y)/dB
    )
  }
  //---------------------------------------------------------------------------
  private def fitter(m: Matrix2D, maxEvaluations: Int = 1000, maxIterations: Int = 100) : Option[Array[Double]] = {
    //-------------------------------------------------------------------------
    //perform an initial estimate of the parameters
    //0               1                2                3              4         5             6
    //gaussianHeight, gaussianStdDevX, gaussianStdDevY, gaussianWidth, centroid, gaussianFwhm, backGround)
    val gaussian = Gaussian2D_Simple.fit(m)

    val initalParameterGuess = Array(   //set to 1 if no initial guessing
        gaussian.amplitude    //0
      , gaussian.background   //1
      , gaussian.centroid.x   //2
      , gaussian.centroid.y   //3
      , gaussian.stdX         //4  (core x)
      , gaussian.stdY         //5  (core y)
      , 5                     //6 (B = shape of the fitting function)
    )
    //-------------------------------------------------------------------------
    //model function
    class FunctionModel extends MultivariateVectorFunction {
      @throws[IllegalArgumentException]
      def value(paremeterSeq: Array[Double]) = {
        val r  = (for(y<- 0 until m.yMax;x<-0 until m.xMax) yield evaluateFunction(x, y, paremeterSeq)).toArray
        r
      }
    }
    //-------------------------------------------------------------------------
    //model function jacobian
    class JacobianOfFunctionModel extends MultivariateMatrixFunction {
      @throws[IllegalArgumentException]
      def value(paremeterSeq: Array[Double]) = {
        val r = (for(y<- 0 until m.yMax;x<-0 until m.xMax) yield evaluateJacobian(x, y, paremeterSeq)).toArray
        r
      }
    }
    //-------------------------------------------------------------------------
    val lsb = new LeastSquaresBuilder

    //set model distribution.function and its jacobian
    lsb.model(new FunctionModel, new JacobianOfFunctionModel)
      .target(m.data map (_.toDouble))
      .start(initalParameterGuess)
      .maxEvaluations(maxEvaluations) //set upper limit of evaluation time
      .maxIterations(maxIterations)   //set upper limit of iteration time
    try {
      val optimizer = new LevenbergMarquardtOptimizer().optimize(lsb.build)
      Some(optimizer.getPoint.toArray)
    }
    catch {
      case _: Exception =>  None
    }
  }
  //---------------------------------------------------------------------------
  //the image must be centred (approximately) on the source to be used
  def fit(m: Matrix2D) : Option[Moffat2D_Elliptical] = {
    val r = fitter(m).getOrElse(return None)
    val A   = r(0) //A = height = Amplitude
    val G   = r(1) //background
    val x_0 = r(2) //centroid X
    val y_0 = r(3) //centroid Y
    val cx  = r(4) //core X
    val cy  = r(5) //core Y
    val B   = r(6) //B = shape of the fitting function
    val centroid = Point2D_Double(x_0,y_0)

    Some(new Moffat2D_Elliptical(A
                        , G
                        , centroid
                        , Point2D_Double(m.offset)
                        , cx
                        , cy
                        , B))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Moffat2D_Elliptical(amplitude: Double
                               , background : Double
                               , centroid: Point2D_Double
                               , offset : Point2D_Double
                               , coreX: Double
                               , coreY: Double
                               , stdX: Double
                               , stdY: Double
                               , fwhmX: Double
                               , fwhmY: Double
                               , B: Double) extends BellShape2D_Fit {
  //---------------------------------------------------------------------------
  def this(amplitude: Double
           , background : Double
           , centroid: Point2D_Double
           , offset : Point2D_Double
           , coreX: Double
           , coreY: Double
           , B: Double) = this(amplitude
    , background
    , centroid
    , offset
    , coreX
    , coreY
    , stdX = Math.sqrt(coreX)  //estimated.Is not a result from a equation
    , stdY = Math.sqrt(coreY)  //estimated.Is not a result from a equation
    , 2 * Math.sqrt(Math.pow(2,1d/B)-1) * coreX
    , 2 * Math.sqrt(Math.pow(2,1d/B)-1) * coreY
    , B)
  //---------------------------------------------------------------------------
  def at(x: Double, y:Double) =
    Moffat2D_Elliptical.evaluateFunction(x,y, Array(amplitude, background, centroid.x, centroid.y, stdX, stdY, B))
  //---------------------------------------------------------------------------
  override def getString(prefix: String ="\t") = {
    super.getString() +
      s"$prefix fit core x              : " + coreX + "\n" +
      s"$prefix fit core y              : " + coreY + "\n" +
      s"$prefix fit B (power)           : " + B + "\n"
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MoffatElliptical.scala
//=============================================================================
