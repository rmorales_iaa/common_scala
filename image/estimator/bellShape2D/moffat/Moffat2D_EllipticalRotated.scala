/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Nov/2020
 * Time:  09h:42m
 * Description: it calculates a elliptical Moffat PSF fitting
 * Based on :
 *   http://www.aspylib.com/doc/aspylib_fitting.html
 *   https://pixinsight.com/doc/tools/DynamicPSF/DynamicPSF.html
 *   http://openafox.com/science/peak-function-derivations.html
 */
//=============================================================================
package com.common.image.estimator.bellShape2D.moffat
//=============================================================================
import com.common.geometry.matrix.matrix2D.Matrix2D
import com.common.geometry.point.Point2D_Double
import com.common.image.estimator.bellShape2D.BellShape2D_Fit
import com.common.image.estimator.bellShape2D.simple.Gaussian2D_Simple
//=============================================================================
import org.apache.commons.math3.analysis.{MultivariateMatrixFunction, MultivariateVectorFunction}
import org.apache.commons.math3.fitting.leastsquares.{LeastSquaresBuilder, LevenbergMarquardtOptimizer}
//=============================================================================
//=============================================================================
object Moffat2D_EllipticalRotated {
  //---------------------------------------------------------------------------
  //We append a background parameter to the original functiona and use a non linear iterative optimization solver 'LevenbergMarquardtOptimizer'
  //     M(x,y,G,A,x_0,y_0,B,cx,cy):= G + ( A / (1+((x-x_0)^2 /cx^2)+((y-y_0)^2 /cy^2))^B);
  //where A = Amplitude, B = background, cx = core in x dimension and cy = core in y dimension
  ///Hint: use an computational algebra package as 'wxmaxima'  to evaluate the function and list the jacobian
  //---------------------------------------------------------------------------
  //evaluates the function
  def evaluateFunction(x: Double, y: Double, parameterSeq: Array[Double]) = {
    val A   = parameterSeq(0) //A = height = Amplitude
    val G   = parameterSeq(1) //background
    val x_0 = parameterSeq(2) //centroid X
    val y_0 = parameterSeq(3) //centroid Y
    val sx  = parameterSeq(4) //core x
    val sy  = parameterSeq(5) //core y
    val B   = parameterSeq(6) //B = shape of the fitting function
    val T   = parameterSeq(7) //phi (rotation angle)

    //precalculated values
    val sinT = Math.sin(T)
    val cosT = Math.cos(T)

    val wx = x * cosT + y * sinT - x_0
    val wx2 = wx * wx

    val wy = (-x)* sinT + y * cosT - y_0
    val wy2 = wy * wy

    val cX2 = sx * sx
    val cY2 = sy * sy

    //calculate
    G + (A/Math.pow(1+(wx2/cX2)+(wy2/cY2),B))
  }
  //---------------------------------------------------------------------------
  //evaluates the jacobian
  def evaluateJacobian(x: Double, y: Double, parameterSeq : Array[Double]) = {
    //-------------------------------------------------------------------------
    val A   = parameterSeq(0) //A = height = Amplitude
    val G   = parameterSeq(1) //background
    val x_0 = parameterSeq(2) //centroid X
    val y_0 = parameterSeq(3) //centroid Y
    val cx  = parameterSeq(4) //coreX
    val cy  = parameterSeq(5) //coreY
    val B   = parameterSeq(6) //B = shape of the fitting function
    val T   = parameterSeq(7) //phi (rotation angle)

    //precalculated values
    val sinT = Math.sin(T)
    val cosT = Math.cos(T)

    val sx2 = cx * cx
    val sy2 = cy * cy

    val wy = -y_0 + cosT * y - sinT * x
    val wy2 = wy * wy

    val wx = sinT * y - x_0 + cosT * x
    val wx2 = wx * wx

    val W = (wy2 / sy2) + ( wx2 / sx2) + 1

    val WB = Math.pow(W,B)
    val WB_1 = Math.pow(W,-B-1)

    //calculate variables
    val dT    = -A * B * WB_1 * (((2 * (-sinT * y - cosT * x) * (-y_0 + cosT * y - sinT * x)) / sy2) +
                                 ((2 * (cosT * y - sinT * x ) * (sinT * y - x_0 + cosT * x)) / sx2))

    //process the array of partial derivates
    Array( 1 / WB                               //dG(x,y)/dA
      , 1                                       //dG(x,y)/dG
      , (2 * A * B * wx * WB_1) / sx2           //dG(x,y)/dx_0
      , (2 * A * B * wy * WB_1) / sy2           //dG(x,y)/dy_0
      , (2 * A * B * wx2 * WB_1) / (sx2 * cx)   //dG(x,y)/dsx
      , (2 * A * B * wy2 * WB_1) / (sy2 * cy)   //dG(x,y)/dsy
      , -((A * Math.log(W)) / WB)               //dG(x,y)/dB
      , dT                                      //dG(x,y)/dT
    )
  }
  //---------------------------------------------------------------------------
  private def fitter(m: Matrix2D, maxEvaluations: Int = 1000, maxIterations: Int = 100) : Option[Array[Double]] = {
    //-------------------------------------------------------------------------
    //perform an initial estimate of the parameters
    //0               1                2                3              4         5             6
    //gaussianHeight, gaussianStdDevX, gaussianStdDevY, gaussianWidth, centroid, gaussianFwhm, backGround)
    val gaussian = Gaussian2D_Simple.fit(m)

    val initalParameterGuess = Array(   //set to 1 if no initial guessing
        gaussian.amplitude    //0
      , gaussian.background   //1
      , gaussian.centroid.x   //2
      , gaussian.centroid.y   //3
      , gaussian.stdX         //4  (core x)
      , gaussian.stdY         //5  (core y)
      , 5                     //6 (B = shape of the fitting function)
      , Math.toRadians(0)     //rotation angle
    )
    //-------------------------------------------------------------------------
    //model function
    class FunctionModel extends MultivariateVectorFunction {
      @throws[IllegalArgumentException]
      def value(paremeterSeq: Array[Double]) = {
        val r  = (for(y<- 0 until m.yMax;x<-0 until m.xMax) yield evaluateFunction(x, y, paremeterSeq)).toArray
        r
      }
    }
    //-------------------------------------------------------------------------
    //model function jacobian
    class JacobianOfFunctionModel extends MultivariateMatrixFunction {
      @throws[IllegalArgumentException]
      def value(paremeterSeq: Array[Double]) = {
        val r = (for(y<- 0 until m.yMax;x<-0 until m.xMax) yield evaluateJacobian(x, y, paremeterSeq)).toArray
        r
      }
    }
    //-------------------------------------------------------------------------
    val lsb = new LeastSquaresBuilder

    //set model distribution.function and its jacobian
    lsb.model(new FunctionModel, new JacobianOfFunctionModel)
      .target(m.data map (_.toDouble))
      .start(initalParameterGuess)
      .maxEvaluations(maxEvaluations) //set upper limit of evaluation time
      .maxIterations(maxIterations)   //set upper limit of iteration time
    try {
      val optimizer = new LevenbergMarquardtOptimizer().optimize(lsb.build)
      Some(optimizer.getPoint.toArray)
    }
    catch {
      case _: Exception =>  None
    }
  }
  //---------------------------------------------------------------------------
  //the image must be centred (approximately) on the source to be used
  def fit(m: Matrix2D) : Option[Moffat2D_EllipticalRotated] = {
    val r = fitter(m).getOrElse(return None)
    val A   = r(0) //A = height = Amplitude
    val G   = r(1) //background
    val x_0 = r(2) //centroid X
    val y_0 = r(3) //centroid Y
    val cx  = r(4) //core x
    val cy  = r(5) //core y
    val B   = r(6) //B = shape of the fitting function
    val T   = r(7) //phi (rotation angle)
    val centroid = Point2D_Double(x_0,y_0)

    Some(new Moffat2D_EllipticalRotated(A
      , G
      , centroid
      , Point2D_Double(m.offset)
      , cx
      , cy
      , B
      , Math.toDegrees(T)))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Moffat2D_EllipticalRotated(amplitude: Double
                                      , background : Double
                                      , centroid: Point2D_Double
                                      , offset : Point2D_Double
                                      , coreX: Double
                                      , coreY: Double
                                      , stdX: Double
                                      , stdY: Double
                                      , fwhmX: Double
                                      , fwhmY: Double
                                      , B: Double
                                      , angle: Double) extends BellShape2D_Fit {
  //---------------------------------------------------------------------------
  def this(amplitude: Double
           , background : Double
           , centroid: Point2D_Double
           , offset : Point2D_Double
           , coreX: Double
           , coreY: Double
           , B: Double
           , angle: Double) = this(amplitude
    , background
    , centroid
    , offset
    , coreX
    , coreY
    , stdX = Math.sqrt(coreX)  //estimated.Is not a result from a equation
    , stdY = Math.sqrt(coreY)  //estimated.Is not a result from a equation
    , 2 * Math.sqrt(Math.pow(2,1d/B)-1) * coreX
    , 2 * Math.sqrt(Math.pow(2,1d/B)-1) * coreY
    , B
    , angle)
  //---------------------------------------------------------------------------
  def at(x: Double, y:Double) =
    Moffat2D_EllipticalRotated.evaluateFunction(x,y, Array(amplitude, background, centroid.x, centroid.y, stdX, stdY, B, Math.toRadians(angle)))
  //---------------------------------------------------------------------------
  override def getString(prefix: String ="\t") = {
    super.getString() +
      s"$prefix fit core x              : " + coreX + "\n" +
      s"$prefix fit core y              : " + coreY + "\n" +
      s"$prefix fit B (power)           : " + B + "\n" +
      s"$prefix fit angle               : " + angle + "\n"
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MoffatEllipticalRotated.scala
//=============================================================================
