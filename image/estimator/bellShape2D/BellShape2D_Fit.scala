/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Apr/2021
 * Time:  14h:36m
 * Description: None
 */
//=============================================================================
package com.common.image.estimator.bellShape2D
//=============================================================================
import scala.collection.mutable.ArrayBuffer
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
import com.common.geometry.point.{Point2D_Double}
import com.common.util.path.Path
//=============================================================================
//=============================================================================
object BellShape2D_Fit {
  //---------------------------------------------------------------------------
  sealed trait BELL_SHAPE_2D_FIT_ALGORITHM
  //---------------------------------------------------------------------------
  case object GAUSSIAN_2D_SIMPLE_FIT                      extends BELL_SHAPE_2D_FIT_ALGORITHM
  case object GAUSSIAN_2D_SIMPLE_CENTRAL_ROW_COL_FIT      extends BELL_SHAPE_2D_FIT_ALGORITHM

  case object GAUSSIAN_2D_ELLIPTICAL_FIT          extends BELL_SHAPE_2D_FIT_ALGORITHM
  case object GAUSSIAN_2D_ELLIPTICAL_ROTATED_FIT  extends BELL_SHAPE_2D_FIT_ALGORITHM

  case object NORMAL_2D_ELLIPTICAL                extends BELL_SHAPE_2D_FIT_ALGORITHM
  case object NORMAL_2D_ELLIPTICAL_ROTATED_FIT    extends BELL_SHAPE_2D_FIT_ALGORITHM

  case object MOFFAT_ELLIPTICAL_2D_FIT            extends BELL_SHAPE_2D_FIT_ALGORITHM
  case object MOFFAT_ELLIPTICAL_ROTATED_FIT       extends BELL_SHAPE_2D_FIT_ALGORITHM

  //---------------------------------------------------------------------------
  def getAlgorithmCode(i: Int) =
    i match {
      case 0   => GAUSSIAN_2D_SIMPLE_FIT
      case 1   => GAUSSIAN_2D_SIMPLE_CENTRAL_ROW_COL_FIT

      case 2   => GAUSSIAN_2D_ELLIPTICAL_FIT
      case 3   => GAUSSIAN_2D_ELLIPTICAL_ROTATED_FIT

      case 10  => NORMAL_2D_ELLIPTICAL
      case 11  => NORMAL_2D_ELLIPTICAL_ROTATED_FIT

      case 20  => MOFFAT_ELLIPTICAL_2D_FIT
      case 21  => MOFFAT_ELLIPTICAL_ROTATED_FIT
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
trait BellShape2D_Fit {
  val amplitude: Double
  val background : Double
  val stdX: Double
  val stdY:Double
  val fwhmX:Double
  val fwhmY:Double
  val centroid: Point2D_Double
  val offset: Point2D_Double
  val fwhm = (fwhmX + fwhmY) / 2
  //---------------------------------------------------------------------------
  def at(x: Double, y:Double) : Double
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Simpson%27s_rule
  //https://math.stackexchange.com/questions/2121611/simpson-rule-for-double-integral
  def integrate(minX:Double, maxX: Double, minY:Double, maxY: Double) : Double = {
    val ba = (maxX+minX) / 2
    val dc = (maxY+minY) / 2
    ((16 * at(ba,dc)) + 4 * at(ba,maxY) + 4 * at(ba,minY) + 4 * at(maxX,dc) + at(maxX,maxY) +
      at(maxX,minY) + 4 * at(minX,dc) + at(minX,maxY) + at(minX,minY)) * ( ((maxX-minX) * (maxY-minY))/36)
  }
  //---------------------------------------------------------------------------
  //https://observablehq.com/@jobleonard/gaussian-kernel-calculater
  def getDiscreteKernel(xSize: Double
                        , ySize: Double
                        , stepX: Double = 1d
                        , stepY: Double = 1d
                        , normalize: Boolean = false
                        , o: Point2D_Double = offset) = {
    val endX = centroid.x + xSize
    val startX = centroid.x - xSize

    val endY = centroid.y + ySize
    val startY = centroid.y - ySize

    var x = startX
    var y = startY
    val kernel = ArrayBuffer[ArrayBuffer[Double]]()
    val kernelAxisX = ArrayBuffer[Double]()
    val kernelAxisY = ArrayBuffer[Double]()
    var sum = 0d
    var firstRow = true

    while (y <= endY) {
      val xCoeff = ArrayBuffer[Double]()
      kernel.append(xCoeff)
      x = startX
      while (x <= endX) {
        val c = at(x,y)
        xCoeff.append(c)
        sum += c
        if (firstRow) kernelAxisX += x + o.x
        x += stepX
      }
      if (firstRow) { //append the last one in x axis
        kernelAxisX += x + o.x
        firstRow = false
      }
      kernelAxisY += y + o.y
      y += stepY
    }
    kernelAxisY += y + o.y //append the last one in y axis

    //normalize
    if (normalize){
      val normalizeFactor = 1d / sum
      val yAxis = kernel.length
      for (y <-0 until yAxis;x <-0 until kernel(y).length)
        kernel(y)(x) = kernel(y)(x) * normalizeFactor
    }
    val k = kernel.toArray map (_.toArray)
    (k, kernelAxisX,kernelAxisY)
  }
  //---------------------------------------------------------------------------
  def toGnuplot3D(kernel: Array[Array[Double]], csv: String, o: Point2D_Double = Point2D_Double.POINT_ZERO) = {

    val csvFileName = if (csv.endsWith(".csv")) csv else csv + ".csv"

    //write data
    val bw = new BufferedWriter(new FileWriter(new File(csvFileName)))
    bw.write("x\ty\tz\n")

    val yAxis = kernel.length
    for (y <-0 until yAxis;x <- 0 until kernel(y).length)
      bw.write(s"${Math.round(x + o.x)}\t${Math.round(y + o.y)}\t${kernel(y)(x)}\n")
    bw.close

    val gnuCommandSeq =
      s"""A = ${amplitude};
         |x_0 = ${centroid.x};
         |y_0 = ${centroid.y};
         |s_x = $stdX;
         |s_y = $stdY;
         |B = $background;
         |
         |set xlabel "x pix"
         |set ylabel "y pix"
         |set zlabel "z intensity"
         |
         |set hidden3d
         |set dgrid3d 20,20
         |set xtics 1
         |splot "${Path.getOnlyFilename(csvFileName)}" i 1:2:3 with lines
         |pause -1
         |""".stripMargin

    //write data
    val gnuPlotFileName = csvFileName.replaceAll(".csv", ".gnuplot")
    val bwGnu = new BufferedWriter(new FileWriter(new File(gnuPlotFileName)))
    bwGnu.write(gnuCommandSeq + "\n")
    bwGnu.close
  }
  //---------------------------------------------------------------------------
  def getString(prefix: String ="\t") = {
    s"$prefix fit amplitude           : " + amplitude + "\n" +
    s"$prefix fit background          : " + background + "\n" +
    s"$prefix fit stdevX              : " + stdX + "\n" +
    s"$prefix fit stdevY              : " + stdY + "\n" +
    s"$prefix fit fwhmX               : " + fwhmX + "\n" +
    s"$prefix fit fwhmY               : " + fwhmY + "\n" +
    s"$prefix fit fwhm                : " + fwhm + "\n" +
    s"$prefix fit centroid            : " + centroid  + "\n" +
    s"$prefix fit offset              : " + offset + "\n"
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file BellShape2D_Estimator.scala
//=============================================================================
