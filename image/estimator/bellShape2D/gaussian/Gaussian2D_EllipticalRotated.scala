/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Nov/2020
 * Time:  09h:42m
 * Description: it calculates a elliptical gaussain PSF fitting
 */
//=============================================================================
package com.common.image.estimator.bellShape2D.gaussian
//=============================================================================
import com.common.image.estimator.bellShape2D.BellShape2D_Fit
import com.common.image.estimator.bellShape2D.simple.Gaussian2D_Simple
import org.apache.commons.math3.analysis.{MultivariateMatrixFunction, MultivariateVectorFunction}
import org.apache.commons.math3.fitting.leastsquares.{LeastSquaresBuilder, LevenbergMarquardtOptimizer}
//=============================================================================
import com.common.geometry.matrix.matrix2D.Matrix2D
import com.common.geometry.point.Point2D_Double
import com.common.math.MyMath
//=============================================================================
object Gaussian2D_EllipticalRotated {
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Gaussian_function sectionName Estimation of parameters
  //We append a background parameter to the original functional and use a non linear iterative optimization solver 'LevenbergMarquardtOptimizer'
  //    f(x,y,A,B,x_0,y_0,s_x,s_y,T):=A*exp(-(((x*cos(T)+y*sin(T))-x_0)^2/(2*s_x^2)+((-x*sin(T)+y*cos(T))-y_0)^2/(2*s_y^2)))+B;
  //where A = Amplitude, B = background and T is the rotate angle
  ///Hint: use an computational algebra package as 'wxmaxima'  to evaluate the function and list the jacobian
  // Based on :
  // https://pixinsight.com/doc/tools/DynamicPSF/DynamicPSF.html
  //---------------------------------------------------------------------------
  //evaluates the circular gaussian
  private def evaluateFunction(_x: Double, _y: Double, parameterSeq: Array[Double]) = {
    val A   = parameterSeq(0) //A = height = Amplitude
    val B   = parameterSeq(1) //background
    val x_0 = parameterSeq(2) //centroidX
    val y_0 = parameterSeq(3) //centroidY
    val s_x = parameterSeq(4) //std_devX
    val s_y = parameterSeq(5) //std_devY
    val T   = parameterSeq(6) //phi (rotation angle)

    //precalculated values
    val x =  _x * Math.cos(T) + _y * Math.sin(T)
    val y = -_x * Math.sin(T) + _y * Math.cos(T)
    val xC2 = (x-x_0) * (x-x_0)
    val yC2 = (y-y_0) * (y-y_0)
    val sX2 = s_x * s_x
    val sY2 = s_y * s_y
    val sX2_2 = sX2 * 2
    val sY2_2 = sY2 * 2
    val e = Math.exp(-(yC2/sY2_2) - (xC2/sX2_2))

    //evaluate
    A * e + B
  }
  //---------------------------------------------------------------------------
  //evaluates the jacobian of a circular gaussian
  def evaluateJacobian(x: Double, y: Double, parameterSeq : Array[Double]) = {
    //-------------------------------------------------------------------------
    val A   = parameterSeq(0) //A = height = Amplitude
    val x_0 = parameterSeq(2) //centroidX
    val y_0 = parameterSeq(3) //centroidY
    val s_x = parameterSeq(4) //std_devX
    val s_y = parameterSeq(5) //std_devY
    val T   = parameterSeq(6) //phi (rotation angle)

    //precalculated values
    val sX2 = s_x * s_x
    val sY2 = s_y * s_y
    val sX2_2 = sX2 * 2
    val sY2_2 = sY2 * 2

    val sinT = Math.sin(T)
    val cosT = Math.cos(T)

    val wy = -y_0 + cosT * y - sinT * x
    val wy2 = (wy * wy) / sY2_2

    val wx = sinT * y - x_0 + cosT * x
    val wx2 = (wx * wx) / sX2_2

    val e = Math.exp(-wy2 - wx2)

    //process the array of partial derivates
    Array(e                                                                                   //df(x,y)/dA
      , 1                                                                                     //df(x,y)/dB
      , (A * (sinT * y - x_0 + cosT * x) * e) / sX2                                           //df(x,y)/dcentroidX
      , (A * (-y_0 + cosT * y - sinT * x) * e) / sY2                                          //df(x,y)/dcentroidY
      , (A * (sinT * y - x_0 + cosT * x) * (sinT * y - x_0 + cosT * x) * e) / (sX2 * s_x)     //df(x,y)/dstdDevX
      , (A * (-y_0 + cosT * y - sinT * x) * (-y_0 + cosT * y - sinT * x) * e) / (sY2 * s_y)   //df(x,y)/dstdDevY
      , A * ( -(((-sinT * y - cosT * x) * (-y_0 + cosT * y - sinT * x)) / sY2 )               //df(x,y)/T
              -(((cosT * y - sinT * x) * (sinT * y - x_0 + cosT * x))/ sX2) ) * e
    )
  }
  //---------------------------------------------------------------------------
  private def fitter(m: Matrix2D, maxEvaluations: Int = 1000, maxIterations: Int = 100) : Option[Array[Double]] = {
    //-------------------------------------------------------------------------
    //perform an initial estimate of the parameters
    val gaussian = Gaussian2D_Simple.fit(m)
    val initalParameterGuess = Array(   //set to 1 if no initial guessing
      gaussian.amplitude
      , gaussian.background
      , gaussian.centroid.x
      , gaussian.centroid.y
      , gaussian.stdX
      , gaussian.stdY
      , Math.toRadians(0) //phi (rotation angle)
    )
    //-------------------------------------------------------------------------
    //model function
    class FunctionModel extends MultivariateVectorFunction {
      @throws[IllegalArgumentException]
      def value(paremeterSeq: Array[Double]) = {
        val r  = (for(y<- 0 until m.yMax;x<-0 until m.xMax) yield evaluateFunction(x, y, paremeterSeq)).toArray
        r
      }
    }
    //-------------------------------------------------------------------------
    //model function jacobian
    class JacobianOfFunctionModel extends MultivariateMatrixFunction {
      @throws[IllegalArgumentException]
      def value(paremeterSeq: Array[Double]) = {
        val r = (for(y<- 0 until m.yMax;x<-0 until m.xMax) yield evaluateJacobian(x, y, paremeterSeq)).toArray
        r
      }
    }
    //-------------------------------------------------------------------------
    val lsb = new LeastSquaresBuilder

    //set model distribution.function and its jacobian
    lsb.model(new FunctionModel, new JacobianOfFunctionModel)
      .target(m.data map (_.toDouble))
      .start(initalParameterGuess)
      .maxEvaluations(maxEvaluations) //set upper limit of evaluation time
      .maxIterations(maxIterations)   //set upper limit of iteration time
    try {
      val optimizer = new LevenbergMarquardtOptimizer().optimize(lsb.build)
      Some(optimizer.getPoint.toArray)
    }
    catch {
      case _: Exception =>  None
    }
  }
  //---------------------------------------------------------------------------
  //the image must be centred (approximately) on the source to be used
  def fit(m: Matrix2D) : Option[Gaussian2D_EllipticalRotated] = {
    val r = fitter(m).getOrElse(return None)

    val A   = r(0) //A = height = Amplitude
    val B   = r(1) //background
    val x_0 = r(2) //centroidX
    val y_0 = r(3) //centroidY
    val s_x = r(4) //std_devX
    val s_y = r(5) //std_devY
    val T   = r(6) //phi (rotation angle)
    val centroid = Point2D_Double(x_0,y_0)

    Some(new Gaussian2D_EllipticalRotated(A
      , B
      , centroid
      , Point2D_Double(m.offset)
      , s_x
      , s_y
      , Math.toDegrees(T)))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Gaussian2D_EllipticalRotated(amplitude: Double
                                        , background : Double
                                        , centroid: Point2D_Double
                                        , offset : Point2D_Double
                                        , stdX: Double
                                        , stdY: Double
                                        , fwhmX: Double
                                        , fwhmY: Double
                                        , angle: Double)  extends BellShape2D_Fit {
  //---------------------------------------------------------------------------
  def this(amplitude: Double
           , background : Double
           , centroid: Point2D_Double
           , offset : Point2D_Double
           , stdX: Double
           , stdY: Double
           , angle: Double) = this(amplitude
    , background
    , centroid
    , offset
    , stdX
    , stdY
    , MyMath.FWHM_COEFFICIENT * stdX
    , MyMath.FWHM_COEFFICIENT * stdY
    , angle)
  //---------------------------------------------------------------------------
  def at(x: Double, y:Double) =
    Gaussian2D_EllipticalRotated.evaluateFunction(x,y,Array(amplitude, background, centroid.x, centroid.y, stdX, stdY, Math.toRadians(angle)))
  //---------------------------------------------------------------------------
  override def getString(prefix: String ="\t") = {
    super.getString() +
    s"$prefix fit angle               : " + angle + "\n"
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Gaussian2D_EllipticalRotated.scala
//=============================================================================
