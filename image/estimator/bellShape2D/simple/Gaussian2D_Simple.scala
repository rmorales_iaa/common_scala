/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
package com.common.image.estimator.bellShape2D.simple
//=============================================================================
import com.common.geometry.matrix.matrix2D.Matrix2D
import com.common.geometry.point.Point2D_Double
import com.common.image.estimator.bellShape2D.gaussian.Gaussian2D_Elliptical
//=============================================================================
object Gaussian2D_Simple {
  //---------------------------------------------------------------------------
  val FWHM_COEFFICIENT = 2 * Math.sqrt(2 * Math.log(2))
  //---------------------------------------------------------------------------
  //(height, std_devX, std_devY, width, central poitnt, fwhm, backGround)
  //The image must be centred (approximately) on the source to be used
  //It estimates roughly the gaussian parameters of a gaussian distribution that fits on data
  //Adapted from Aspylib.scala

  def fit(m: Matrix2D, userFwhm: Option[Double] = None) = {
    val (_, max, median, _, _) = m.getStats()
    val backGround = median
    val gaussianHeight = max - median
    val centroid = m.toSegment2D().getCentroid()
    val filterValue = backGround + (gaussianHeight / 2d)
    val gaussianFwhm = userFwhm.getOrElse(Math.sqrt(m.data.map(v => if (v > filterValue) 1 else 0).sum))
    val gaussianStdDev = gaussianFwhm / FWHM_COEFFICIENT

    new Gaussian2D_Elliptical(
        gaussianHeight
      , backGround
      , centroid
      , Point2D_Double(m.offset)
      , gaussianStdDev
      , gaussianStdDev)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Gausiian2D_Simple.scala
//=============================================================================
