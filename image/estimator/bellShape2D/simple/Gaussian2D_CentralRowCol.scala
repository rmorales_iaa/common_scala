/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.common.image.estimator.bellShape2D.simple
//=============================================================================
import com.common.geometry.matrix.matrix2D.Matrix2D
import com.common.geometry.point.Point2D_Double
import com.common.math.distribution.gaussian.guassian1D.Gaussian1D
import com.common.image.estimator.bellShape2D.gaussian.Gaussian2D_Elliptical
//=============================================================================
object Gaussian2D_CentralRowCol {
  //---------------------------------------------------------------------------
  //it calculates the central row and col and fit a guaussian 1-d in both directions
  //(gaussianHeight, gaussianStdDevX, gaussianStdDevY, gaussianWidth, centroids, gaussianFwhm, backGround)
  def fit(m: Matrix2D) = {
    //-------------------------------------------------------------------------
    def fitGaussian(seq: Array[Double]) = {
      val m = seq.min
      val centralRowPos = seq.zipWithIndex.map { case (v, i) => Point2D_Double(i, v - m) }
      Gaussian1D.fit(centralRowPos)._1
    }

    //-------------------------------------------------------------------------
    val centralRowIndex = m.yMax / 2
    val centralColIndex = m.xMax / 2

    val centralRow = if ((m.yMax % 2) == 0) m.getAverageRow(centralRowIndex, centralRowIndex + 1).toArray else m.getAsRowSeq()(centralRowIndex) map (_.toDouble)
    val centralCol = if ((m.xMax % 2) == 0) m.getAverageCol(centralColIndex, centralColIndex + 1).toArray else m.getAsColSeq()(centralColIndex) map (_.toDouble)

    val centralRowGaussian = fitGaussian(centralRow)
    val centralColGaussian = fitGaussian(centralCol)
    val gaussianHeight = (centralRowGaussian.getHeight + centralColGaussian.getHeight) / 2.0
    val gaussianStdDevX = centralRowGaussian.getWidth
    val gaussianStdDevY = centralColGaussian.getWidth
    val centroid = {
      val source = m.toSegment2D()
      source.getCentroid()
    }
    val backGround = m.getStats()._3

    new Gaussian2D_Elliptical(gaussianHeight
      , backGround
      , centroid
      , Point2D_Double(m.offset)
      , gaussianStdDevX
      , gaussianStdDevY)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Gaussian2D_CentralRowCol.scala
//=============================================================================
