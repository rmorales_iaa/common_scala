/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  13/Jan/2023
 * Time:  18h:32m
 * Description: None
 */
//=============================================================================
package com.common.image.estimator.bellShape2D.simple
//=============================================================================}
import com.common.geometry.point.Point2D_Double
import com.common.image.estimator.bellShape2D.BellShape2D_Fit
//=============================================================================}
//=============================================================================
case class SimpleBellShape2D(override val fwhm: Double) extends BellShape2D_Fit {
  val amplitude = 0
  val background= 0
  val stdX= 0
  val stdY= 0
  val fwhmX = fwhm
  val fwhmY = fwhm
  val centroid = Point2D_Double.POINT_ZERO
  val offset = Point2D_Double.POINT_ZERO
  //---------------------------------------------------------------------------
  def at(x: Double, y:Double) : Double = 0
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SimpleBellShape2D.scala
//=============================================================================
