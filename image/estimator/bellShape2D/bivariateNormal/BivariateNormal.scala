/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Nov/2020
 * Time:  09h:42m
 * Description: https://en.wikipedia.org/wiki/Multivariate_normal_distribution#Bivariate_case
 * f(x,y,rho,x_0,y_0,s_x,s_y):=1/(2*%pi*s_x*s_y*sqrt(1-rho^2))*exp((-1)/(2*(1-rho^2))*((x-x_0)^2/s_x^2-(2*rho*(x-x_0)*(y-y_0))/(s_x*s_y)+(y-y_0)^2/s_y^2))
 */
//=============================================================================
package com.common.image.estimator.bellShape2D.bivariateNormal
//=============================================================================
import com.common.image.estimator.bellShape2D.BellShape2D_Fit
import com.common.image.estimator.bellShape2D.simple.Gaussian2D_Simple
import org.apache.commons.math3.analysis.{MultivariateMatrixFunction, MultivariateVectorFunction}
import org.apache.commons.math3.fitting.leastsquares.{LeastSquaresBuilder, LevenbergMarquardtOptimizer}
//=============================================================================
import com.common.geometry.matrix.matrix2D.Matrix2D
import com.common.geometry.point.Point2D_Double
import com.common.constant.astronomy.AstronomicalUnit.PRECISE_PI
import scala.math._
//=============================================================================
//=============================================================================
object BivariateNormal {
  //---------------------------------------------------------------------------
  //evaluates the circular gaussian
  def evaluateFunction(x: Double, y: Double, parameterSeq: Array[Double]) = {
    val rho = parameterSeq(0) //correlation
    val x_0 = parameterSeq(1) //centroidX
    val y_0 = parameterSeq(2) //centroidY
    val s_x = parameterSeq(3) //std_devX
    val s_y = parameterSeq(4) //std_devY

    val r =  BivariateNormal(
         rho
       , Point2D_Double(x_0,y_0)
       , s_x
       , s_y).at(x, y)
    r
  }

  //---------------------------------------------------------------------------
  //evaluates the jacobian of a circular gaussian
  def evaluateJacobian(x: Double, y: Double, parameterSeq: Array[Double]) = {
    //-------------------------------------------------------------------------
    val rho = parameterSeq(0) //correlation
    val x_0 = parameterSeq(1) //centroidX
    val y_0 = parameterSeq(2) //centroidY
    val s_x = parameterSeq(3) //std_devX
    val s_y = parameterSeq(4) //std_devY

    //precalculated values
    val dx = x - x_0
    val dy = y - y_0
    val sx2 = s_x * s_x
    val sy2 = s_y * s_y
    val rho2 = rho * rho
    val oneMinusRho2 = 1 - rho2

    // Precompute common terms
    val dxdy = dx * dy
    val dx2 = dx * dx
    val dy2 = dy * dy
    val sxsy = s_x * s_y

    // Exponent term
    val exponentNumerator = -2 * rho * dxdy / sxsy + dy2 / sy2 + dx2 / sx2
    val exponentDenominator = 2 * oneMinusRho2
    val exponent = exp(-exponentNumerator / exponentDenominator)

    // Common factor in denominators
    val commonDenominator = 2 * PRECISE_PI * sxsy * sqrt(oneMinusRho2)
    val commonDenominatorRho = commonDenominator * pow(oneMinusRho2, 1.5)

    // Calculation of `rho_dif`
    val rho_dif = (
      ((dxdy / (oneMinusRho2 * sxsy) - rho * exponentNumerator / (oneMinusRho2 * oneMinusRho2)) * exponent) / commonDenominator +
        (rho * exponent) / commonDenominatorRho
      )

    // Calculation of `x_0_dif`
    val x_0_dif = -((2 * rho * dy / sxsy - 2 * dx / sx2) * exponent) / (4 * commonDenominatorRho)

    // Calculation of `y_0_dif`
    val y_0_dif = -((2 * rho * dx / sxsy - 2 * dy / sy2) * exponent) / (4 * commonDenominatorRho)

    // Calculation of `s_x_dif`
    val s_x_dif = -(
      ((2 * rho * dxdy / (sx2 * s_y) - 2 * dx2 / (sx2 * s_x)) * exponent) / (4 * commonDenominatorRho) +
        exponent / (2 * commonDenominator * s_x)
      )

    // Calculation of `s_y_dif`
    val s_y_dif = -(
      ((2 * rho * dxdy / (s_x * sy2) - 2 * dy2 / (sy2 * s_y)) * exponent) / (4 * commonDenominatorRho) +
        exponent / (2 * commonDenominator * s_y)
      )

    //process the array of partial derivates
    Array(
        rho_dif //df(x,y)/rho
      , x_0_dif //df(x,y)/dcentroidX
      , y_0_dif //df(x,y)/dcentroidY
      , s_x_dif //df(x,y)/dstdDevX
      , s_y_dif //df(x,y)/dstdDevY
    )
  }

  //---------------------------------------------------------------------------
  private def fitter(m: Matrix2D, maxEvaluations: Int = 1000, maxIterations: Int = 100): Option[Array[Double]] = {
    //-------------------------------------------------------------------------
    //perform an initial estimate of the parameters
    val gaussian = Gaussian2D_Simple.fit(m)
    val initalParameterGuess = Array( //set to 1 if no initial guessing
        0 //rho
      , gaussian.centroid.x
      , gaussian.centroid.y
      , gaussian.stdX
      , gaussian.stdY
    )
    //-------------------------------------------------------------------------
    //model function
    class FunctionModel extends MultivariateVectorFunction {
      @throws[IllegalArgumentException]
      def value(paremeterSeq: Array[Double]) = {
        val r = (for (y <- 0 until m.yMax; x <- 0 until m.xMax) yield evaluateFunction(x, y, paremeterSeq)).toArray
        r
      }
    }
    //-------------------------------------------------------------------------
    //model function jacobian
    class JacobianOfFunctionModel extends MultivariateMatrixFunction {
      @throws[IllegalArgumentException]
      def value(paremeterSeq: Array[Double]) = {
        val r = (for (y <- 0 until m.yMax; x <- 0 until m.xMax) yield evaluateJacobian(x, y, paremeterSeq)).toArray
        r
      }
    }
    //-------------------------------------------------------------------------
    val lsb = new LeastSquaresBuilder

    //set model distribution.function and its jacobian
    lsb.model(new FunctionModel, new JacobianOfFunctionModel)
      .target(m.data)
      .start(initalParameterGuess)
      .maxEvaluations(maxEvaluations) //set upper limit of evaluation time
      .maxIterations(maxIterations) //set upper limit of iteration time
    try {
      val optimizer = new LevenbergMarquardtOptimizer().optimize(lsb.build)
      Some(optimizer.getPoint.toArray)
    }
    catch {
      case _: Exception => None
    }
  }

  //---------------------------------------------------------------------------
  //the image must be centred (approximately) on the source to be used
  def fit(m: Matrix2D): Option[BivariateNormal] = {
    val r = fitter(m).getOrElse(return None)

    val rho = r(0) //correlation
    val x_0 = r(1) //centroidX
    val y_0 = r(2) //centroidY
    val s_x = r(3) //std_devX
    val s_y = r(4) //std_devY

    Some(BivariateNormal(
        rho
      , Point2D_Double(x_0, y_0)
      , s_x
      , s_y))
  }
  //=============================================================================
  case class BivariateNormal(rho: Double
                             , centroid: Point2D_Double
                             , stdX: Double
                             , stdY: Double) extends BellShape2D_Fit {
    //---------------------------------------------------------------------------
    override val amplitude: Double = 0
    override val background: Double = 0
    override val fwhmX: Double = 0
    override val fwhmY: Double = 0
    override val offset: Point2D_Double = Point2D_Double.POINT_ZERO
    private val pdf = com.common.math.distribution.BivariateNormal(
      centroid.x
      , centroid.y
      , stdX
      , stdY
      , rho
    )
    //---------------------------------------------------------------------------
    def at(x: Double, y: Double) = pdf.eval(x,y)
    //---------------------------------------------------------------------------
    //integrate(integrate(f(x,y,A,B,x_0,y_0,s_x,s_y), x,mx,Mx), y,my,My)
    override def integrate(mx: Double, Mx: Double, my: Double, My: Double) =
      0d
    //---------------------------------------------------------------------------
  }
  //=============================================================================
}
//=============================================================================
//End of file BivariateNormal.scala
//=============================================================================
