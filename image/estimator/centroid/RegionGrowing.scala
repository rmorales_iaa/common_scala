/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Apr/2023
 * Time:  17h:54m
 * Description: None
 */
//=============================================================================
package com.common.image.estimator.centroid
//=============================================================================
import com.common.geometry.point.{Point2D, Point2D_Double}
import com.common.geometry.rectangle.Rectangle
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.estimator.centroid.Centroid.SOURCE_CENTROID_ALGORITHM_REGION_GROWING
//=============================================================================
/**
 * BitMap class to track visited pixels
 */
private class BitMap(width: Int, height: Int, minX: Int, minY: Int) {
  //---------------------------------------------------------------------------
  private val bytesPerRow = (width + 7) / 8
  private val data = new Array[Byte](height * bytesPerRow)
  private val rowPtrs = Array.tabulate(height)(i => i * bytesPerRow)
  //---------------------------------------------------------------------------
  def isMarked(x: Int, y: Int): Boolean = {
    val relX = x - minX
    val relY = y - minY

    if (relY >= height || relX >= width) true
    else {
      val byteOffset = relX >> 3
      val bitMask = 1 << (relX & 7)
      val idx = rowPtrs(relY) + byteOffset
      (data(idx) & bitMask) != 0
    }
  }
  //---------------------------------------------------------------------------
  def mark(x: Int, y: Int): Unit = {
    val relX = x - minX
    val relY = y - minY

    if (relY < height && relX < width) {
      val byteOffset = relX >> 3
      val bitMask = 1 << (relX & 7)
      val idx = rowPtrs(relY) + byteOffset
      data(idx) = (data(idx) | bitMask).toByte
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
/**
 * Case class to hold region growing centroids data
 */
private case class CentroidData(var pixelCount: Int = 0,
                                var sumIntensity: Double = 0.0,
                                var sumXIntensity: Double = 0.0,
                                var sumYIntensity: Double = 0.0,
                                var sumX: Int = 0,
                                var sumY: Int = 0)
//=============================================================================
/**
 * Implementation of the growing region centroid calculation algorithm
 */
object RegionGrowingCentroid {
  //---------------------------------------------------------------------------
  private val MaxStackSize = 65535
  //---------------------------------------------------------------------------
  def calculateCentroidRegionGrowing(source: Segment2D): Unit = {
    val calculatedCentroid = getSourceCentroid(source)
    if (!calculatedCentroid.oneComponentIsNan && !calculatedCentroid.oneComponentIsInfinite) {
      source.setLowResolutionCentroid(false)
      source.setCentroidRaw(calculatedCentroid + Point2D_Double.POINT_ONE)
      source.setCentroidAlgorithm(SOURCE_CENTROID_ALGORITHM_REGION_GROWING)
    }
  }
  //---------------------------------------------------------------------------
  private def sourceBoundingBox(source: Segment2D): Option[Rectangle] = {

    if (source.rowSeq.isEmpty || source.rowSeq.forall(_.isEmpty)) {
      None
    }
    else  {
      val xAxisBounds = source.getX_AxisAbsolute()
      val yAxisBounds = source.getY_AxisAbsolute()
      Some(Rectangle(
        Point2D(xAxisBounds.s, yAxisBounds.s),
        Point2D(xAxisBounds.e, yAxisBounds.e)
      ))
    }

  }
  //---------------------------------------------------------------------------
  private def isPointInCircle(cx: Double, cy: Double, radius: Double, x: Double, y: Double): Boolean = {
    val dx1 = x - cx - 0.5
    val dy1 = y - cy - 0.5
    val dx2 = x - cx + 0.5
    val dy2 = y - cy + 0.5

    val r2 = radius * radius
    val corners = Array(
      r2 - (dx1 * dx1 + dy1 * dy1),
      r2 - (dx2 * dx2 + dy1 * dy1),
      r2 - (dx2 * dx2 + dy2 * dy2),
      r2 - (dx1 * dx1 + dy2 * dy2)
    )

    corners.exists(_ > 0.0)
  }
  //---------------------------------------------------------------------------
  private def getSourceCentroid(source: Segment2D): Point2D_Double = {
    val bbox = sourceBoundingBox(source)
    if (bbox.isEmpty) return Point2D_Double.POINT_NAN

    // Find initial centroid and radius based on source extent
    val minX = bbox.get.min.x
    val minY = bbox.get.min.y
    val maxX = bbox.get.max.x
    val maxY = bbox.get.max.y

    val width = maxX - minX + 8
    val height = maxY - minY + 8

    val initialX = (minX + maxX).toDouble / 2.0
    val initialY = (minY + maxY).toDouble / 2.0
    val radius = Math.ceil(Math.max(maxX - minX, maxY - minY).toDouble / 2.0)

    // Create bitmap for visited pixels
    val bitmap = new BitMap(width, height, minX - 4, minY - 4)

    // Stack for region growing (x, y)
    val stack = new scala.collection.mutable.Stack[(Int, Int)]()
    val data = CentroidData()

    // Start with initial point
    stack.push((initialX.toInt, initialY.toInt))
    bitmap.mark(initialX.toInt, initialY.toInt)

    while (stack.nonEmpty && stack.size < MaxStackSize) {
      val (absX, absY) = stack.pop()

      // Convert absolute coordinates to source's relative coordinates
      val relX = absX - source.offset.x
      val relY = absY - source.offset.y

      // Get pixel value using relative coordinates
      val intensity = source(relX, relY).getOrElse(0.0)

      if (intensity > 0.0) {  // Changed condition to match Rust version

        // Update running sums using absolute coordinates
        data.pixelCount += 1
        data.sumIntensity += intensity
        data.sumXIntensity += absX * intensity
        data.sumYIntensity += absY * intensity
        data.sumX += absX
        data.sumY += absY

        // Check neighbors in the same order as Rust
        val neighbors = Array((0, -1), (0, 1), (-1, 0), (1, 0))
        neighbors.foreach { case (dx, dy) =>
          val nx = absX + dx
          val ny = absY + dy

          if (!bitmap.isMarked(nx, ny) &&
            isPointInCircle(initialX, initialY, radius, nx.toDouble, ny.toDouble)) {
            stack.push((nx, ny))
            bitmap.mark(nx, ny)
          }
        }
      }
    }

    // Calculate final centroid
    if (data.pixelCount > 0 && data.sumIntensity > 0.0) {
      new Point2D_Double(
        data.sumXIntensity / data.sumIntensity,
        data.sumYIntensity / data.sumIntensity
      )
    } else {
      Point2D_Double.POINT_NAN
    }
  }
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
//=============================================================================
//End of file RegionGroing.scala
//=============================================================================
