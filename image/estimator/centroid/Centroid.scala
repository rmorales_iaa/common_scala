/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Apr/2023
 * Time:  17h:54m
 * Description: None
 */
//=============================================================================
package com.common.image.estimator.centroid
//=============================================================================
import com.common.dataType.tree.kdTree.K2d_TreeDouble
import com.common.geometry.point.Point2D_Double
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.estimator.bellShape2D.bivariateNormal.BivariateNormal
import com.common.image.estimator.bellShape2D.moffat.{Moffat2D_Elliptical, Moffat2D_EllipticalRotated}
import com.common.image.estimator.bellShape2D.normal.{Normal2D_Elliptical, Normal2D_EllipticalRotated}
import com.common.image.estimator.centroid.RegionGrowingCentroid._
import com.common.image.estimator.centroid.daophot.{CNTRD, GCNTRD}
import com.common.image.myImage.MyImage
import com.common.math.MyMath
//=============================================================================
import scala.util.{Failure, Success, Try}
//=============================================================================
object Centroid {
  //---------------------------------------------------------------------------
  private final val SOURCE_CENTROID_ALGORITHM_NAME_CNTRD  = "cntrd"
  private final val SOURCE_CENTROID_ALGORITHM_NAME_GCNTRD = "gcntrd"

  private final val SOURCE_CENTROID_ALGORITHM_NAME_NORMAl2D_ELLIPTICAL         = "normal2D_Elliptical"
  private final val SOURCE_CENTROID_ALGORITHM_NAME_NORMAl2D_ELLIPTICAL_ROTATED = "normal2D_EllipticalRotated"

  private final val SOURCE_CENTROID_ALGORITHM_NAME_MOFFAT2D_ELLIPTICAL         = "moffat2D_Elliptical"
  private final val SOURCE_CENTROID_ALGORITHM_NAME_MOFFAT2D_ELLIPTICAL_ROTATED = "moffat2D_EllipticalRotated"

  private final val SOURCE_CENTROID_ALGORITHM_NAME_CENTRE_OF_MASSES = "centreOfMasses"

  private final val SOURCE_CENTROID_ALGORITHM_NAME_BIVARIATE_NORMAL = "bivariateNormal"

  private final val SOURCE_CENTROID_ALGORITHM_NAME_POLYGON          = "polygon"
  private final val SOURCE_CENTROID_ALGORITHM_NAME_REGION_GROWING   = "region_growing"

  private final val SOURCE_CENTROID_ALGORITHM_NAME_NONE             = "none"
  //---------------------------------------------------------------------------
  sealed trait SOURCE_CENTROID_ALGORITHM{val name:String}
  //---------------------------------------------------------------------------
  //high precission algorithms
  case object SOURCE_CENTROID_ALGORITHM_DAOPHOT_CNTRD    extends SOURCE_CENTROID_ALGORITHM {val name = SOURCE_CENTROID_ALGORITHM_NAME_GCNTRD}
  case object SOURCE_CENTROID_ALGORITHM_DAOPHOT_GCNTRD   extends SOURCE_CENTROID_ALGORITHM {val name = SOURCE_CENTROID_ALGORITHM_NAME_GCNTRD}
  //---------------------------------------------------------------------------
  //low precission algorithms
  case object SOURCE_CENTROID_ALGORITHM_CENTRE_OF_MASSES extends SOURCE_CENTROID_ALGORITHM {val name = SOURCE_CENTROID_ALGORITHM_NAME_CENTRE_OF_MASSES}

  case object SOURCE_NORMAL_2D_ELLIPTICAL extends SOURCE_CENTROID_ALGORITHM {val name = SOURCE_CENTROID_ALGORITHM_NAME_NORMAl2D_ELLIPTICAL}
  case object SOURCE_NORMAL_2D_ELLIPTICAL_ROTATED extends SOURCE_CENTROID_ALGORITHM {val name = SOURCE_CENTROID_ALGORITHM_NAME_NORMAl2D_ELLIPTICAL_ROTATED}

  case object SOURCE_MOFFAT2D_ELLIPTICAL extends SOURCE_CENTROID_ALGORITHM {val name = SOURCE_CENTROID_ALGORITHM_NAME_MOFFAT2D_ELLIPTICAL}
  case object SOURCE_MOFFAT2D_ELLIPTICAL_ROTATED extends SOURCE_CENTROID_ALGORITHM {val name = SOURCE_CENTROID_ALGORITHM_NAME_MOFFAT2D_ELLIPTICAL_ROTATED}

  case object SOURCE_BIVARIATE_NORMAL extends SOURCE_CENTROID_ALGORITHM {val name = SOURCE_CENTROID_ALGORITHM_NAME_BIVARIATE_NORMAL}

  case object SOURCE_CENTROID_ALGORITHM_CENTROID_POLYGON extends SOURCE_CENTROID_ALGORITHM {val name = SOURCE_CENTROID_ALGORITHM_NAME_POLYGON}

  case object SOURCE_CENTROID_ALGORITHM_REGION_GROWING extends SOURCE_CENTROID_ALGORITHM {val name = SOURCE_CENTROID_ALGORITHM_NAME_REGION_GROWING}

  case object SOURCE_CENTROID_ALGORITHM_CENTROID_NONE extends SOURCE_CENTROID_ALGORITHM {val name = SOURCE_CENTROID_ALGORITHM_NAME_NONE}

  //---------------------------------------------------------------------------
  //apply algorithms and stops when one of them succeed
  private final val SOURCE_CENTROID_ALGORITHM_SEQ = Seq(SOURCE_CENTROID_ALGORITHM_DAOPHOT_CNTRD
    , SOURCE_CENTROID_ALGORITHM_DAOPHOT_GCNTRD
    , SOURCE_CENTROID_ALGORITHM_CENTRE_OF_MASSES
    , SOURCE_NORMAL_2D_ELLIPTICAL
    , SOURCE_NORMAL_2D_ELLIPTICAL_ROTATED
    , SOURCE_MOFFAT2D_ELLIPTICAL
    , SOURCE_MOFFAT2D_ELLIPTICAL_ROTATED
    , SOURCE_BIVARIATE_NORMAL
    , SOURCE_CENTROID_ALGORITHM_REGION_GROWING)
  //---------------------------------------------------------------------------
  private def getAlgorithm(algorithmName: String) : SOURCE_CENTROID_ALGORITHM =
    algorithmName match {
      case SOURCE_CENTROID_ALGORITHM_NAME_CNTRD                       => SOURCE_CENTROID_ALGORITHM_DAOPHOT_CNTRD
      case SOURCE_CENTROID_ALGORITHM_NAME_GCNTRD                      => SOURCE_CENTROID_ALGORITHM_DAOPHOT_GCNTRD
      case SOURCE_CENTROID_ALGORITHM_NAME_CENTRE_OF_MASSES            => SOURCE_CENTROID_ALGORITHM_CENTRE_OF_MASSES

      case SOURCE_CENTROID_ALGORITHM_NAME_NORMAl2D_ELLIPTICAL         => SOURCE_NORMAL_2D_ELLIPTICAL
      case SOURCE_CENTROID_ALGORITHM_NAME_NORMAl2D_ELLIPTICAL_ROTATED => SOURCE_NORMAL_2D_ELLIPTICAL_ROTATED

      case SOURCE_CENTROID_ALGORITHM_NAME_MOFFAT2D_ELLIPTICAL         => SOURCE_MOFFAT2D_ELLIPTICAL
      case SOURCE_CENTROID_ALGORITHM_NAME_MOFFAT2D_ELLIPTICAL_ROTATED => SOURCE_MOFFAT2D_ELLIPTICAL_ROTATED

      case SOURCE_CENTROID_ALGORITHM_NAME_BIVARIATE_NORMAL            => SOURCE_BIVARIATE_NORMAL

      case SOURCE_CENTROID_ALGORITHM_NAME_REGION_GROWING              => SOURCE_CENTROID_ALGORITHM_REGION_GROWING

      case _                                                          => SOURCE_CENTROID_ALGORITHM_CENTRE_OF_MASSES
    }
  //---------------------------------------------------------------------------
  def calculateCentroiWithFallBack(sourceSeq: Array[Segment2D]
                                   , img: MyImage = null
                                   , centroidAlgorithmSeq: List[String]
                                   , verbose:Boolean): Unit = {
    //-------------------------------------------------------------------------
    def calculateFirstValidCentroid(source: Segment2D
                                    , img: MyImage = null
                                    , centroidAlgorithmSeq: Seq[SOURCE_CENTROID_ALGORITHM] = SOURCE_CENTROID_ALGORITHM_SEQ): Unit = {

      centroidAlgorithmSeq.foreach { centroidAlgorithm =>
        calculateCentroidByAlgorithm(source, centroidAlgorithm, img)
        if (!source.contains(source.getCentroid().toPoint2D())) {
          calculateCentroidByAlgorithm(source,centroidAlgorithm = SOURCE_CENTROID_ALGORITHM_CENTRE_OF_MASSES)
          return
        }
        else if (source.getCentroidAlgorithm() == centroidAlgorithm) return
      }
      calculateCentroidByCentreOfMasses(source) //final backup
    }
    //-------------------------------------------------------------------------
    val algorithmSeq = centroidAlgorithmSeq.map { getAlgorithm(_) }
    sourceSeq.foreach { source => calculateFirstValidCentroid(source, img, algorithmSeq) }
  }
  //---------------------------------------------------------------------------
  def calculateCentroidByAlgorithm(source: Segment2D
                                   , centroidAlgorithm: SOURCE_CENTROID_ALGORITHM = SOURCE_CENTROID_ALGORITHM_CENTRE_OF_MASSES
                                   , img: MyImage = null
                                   , verbose: Boolean = false): Unit = {
    Try {
      centroidAlgorithm match {
        case SOURCE_CENTROID_ALGORITHM_DAOPHOT_CNTRD    => calculateCentroidByCNTRD(source, img)
        case SOURCE_CENTROID_ALGORITHM_DAOPHOT_GCNTRD   => calculateCentroidByGCNTRD(source, img)
        case SOURCE_CENTROID_ALGORITHM_CENTRE_OF_MASSES => calculateCentroidByCentreOfMasses(source)

        case SOURCE_NORMAL_2D_ELLIPTICAL                 => calculateCentroidByNormal2D_Elliptical(source, img, verbose)
        case SOURCE_NORMAL_2D_ELLIPTICAL_ROTATED         => calculateCentroidByNormal2D_EllipticalRotated(source, img, verbose)

        case SOURCE_MOFFAT2D_ELLIPTICAL                 => calculateCentroidByMoffat2D_Elliptical(source, img, verbose)
        case SOURCE_MOFFAT2D_ELLIPTICAL_ROTATED         => calculateCentroidByMoffat2D_EllipticalRotated(source, img, verbose)

        case SOURCE_BIVARIATE_NORMAL                    => calculateCentroidByBivariateNormal(source, img, verbose)

        case SOURCE_CENTROID_ALGORITHM_REGION_GROWING   => calculateCentroidRegionGrowing(source)

        case SOURCE_CENTROID_ALGORITHM_CENTROID_POLYGON => calculateCentroidByPolygon(source)
      }
    }
    match {
      case Success(_) =>
      case Failure(e) =>
        if (verbose) println(e.getMessage)
        source.setCentroidAlgorithm(SOURCE_CENTROID_ALGORITHM_CENTROID_NONE)
    }
  }
  //---------------------------------------------------------------------------
  private  def calculateCentroidByCNTRD(source: Segment2D, img: MyImage) =
    CNTRD.calculateCentroid(img, source)
  //---------------------------------------------------------------------------
  private def calculateCentroidByGCNTRD(source: Segment2D,img: MyImage) =
    GCNTRD.calculateCentroid(img, source)
  //---------------------------------------------------------------------------
  private def calculateCentroidByNormal2D_Elliptical(source: Segment2D
                                                     , img: MyImage
                                                     , verbose: Boolean) = {
    val gaussian = Normal2D_Elliptical.fit(source.toMatrix2D(img.getBackground(verbose)))
    if (gaussian.isDefined) {
      source.setCentroidRaw(gaussian.get.centroid) //reference in (1,1)
      source.setLowResolutionCentroid(false)
      source.setCentroidAlgorithm(SOURCE_NORMAL_2D_ELLIPTICAL)
    }
  }
  //---------------------------------------------------------------------------
  private def calculateCentroidByNormal2D_EllipticalRotated(source: Segment2D
                                                            , img: MyImage
                                                            , verbose: Boolean) = {
    val gaussian = Normal2D_EllipticalRotated.fit(source.toMatrix2D(img.getBackground(verbose)))
    if (gaussian.isDefined) {
      source.setCentroidRaw(gaussian.get.centroid) //reference in (1,1)
      source.setLowResolutionCentroid(false)
      source.setCentroidAlgorithm(SOURCE_NORMAL_2D_ELLIPTICAL_ROTATED)
    }
  }

  //---------------------------------------------------------------------------
  private def calculateCentroidByMoffat2D_Elliptical(source: Segment2D
                                                     , img: MyImage
                                                     , verbose: Boolean) = {
    val gaussian = Moffat2D_Elliptical.fit(source.toMatrix2D(img.getBackground(verbose)))
    if (gaussian.isDefined) {
      source.setCentroidRaw(gaussian.get.centroid) //reference in (1,1)
      source.setLowResolutionCentroid(false)
      source.setCentroidAlgorithm(SOURCE_MOFFAT2D_ELLIPTICAL)
    }
  }

  //---------------------------------------------------------------------------
  private def calculateCentroidByMoffat2D_EllipticalRotated(source: Segment2D
                                                            , img: MyImage
                                                            , verbose: Boolean) = {
    val gaussian = Moffat2D_EllipticalRotated.fit(source.toMatrix2D(img.getBackground(verbose)))
    if (gaussian.isDefined) {
      source.setCentroidRaw(gaussian.get.centroid)
      source.setLowResolutionCentroid(false)
      source.setCentroidAlgorithm(SOURCE_MOFFAT2D_ELLIPTICAL_ROTATED)
    }
  }

  //---------------------------------------------------------------------------
  private def calculateCentroidByBivariateNormal(source: Segment2D
                                                 , img: MyImage
                                                 , verbose: Boolean) = {
    val gaussian = BivariateNormal.fit(source.toMatrix2D(img.getBackground(verbose)))
    if (gaussian.isDefined) {
      source.setCentroidRaw(gaussian.get.centroid)
      source.setLowResolutionCentroid(false)
      source.setCentroidAlgorithm(SOURCE_BIVARIATE_NORMAL)
    }
  }
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Image_moment
  //centre of masses or centroid is: (m10 / m00,m01 / m00)
  //where m00 = area = flux
  def calculateCentroidByCentreOfMasses(source: Segment2D) = {
    val m00 = source.getFlux()
    var m10 = 0d
    var m01 = 0d
    source.rowSeq.zipWithIndex flatMap { case (row, y) =>
      row.map { s =>
        val xPosSeq = s.getPosSeq map (_.toDouble)
        val yPosSeq = Array.fill[Double](s.elementCount)(y)
        m10 += s.multiplyAndSum(xPosSeq)
        m01 += s.multiplyAndSum(yPosSeq)
      }
    }
    source.setCentroidRaw(
      Point2D_Double(m10 / m00, m01 / m00)
        + Point2D_Double(source.offset)
        + Point2D_Double.POINT_ONE)  //reference in (1,1)
    source.setCentroidAlgorithm(SOURCE_CENTROID_ALGORITHM_CENTRE_OF_MASSES)
  }

  //---------------------------------------------------------------------------
  //Points are numbered in clockwise order: first all Segment1D starts, later all Segment1D ends
  //Please note that the absolute position is regarded to its container definition (usually a Region)
  //so to get the real the absolute position, the offset of the container definition must be added
  def calculateCentroidByPolygon(source: Segment2D) = {
    val vertexStartsEndsSeq = source.rowSeq.zipWithIndex flatMap { case (row, i) => row.map { s =>
      (Array(s.s.toDouble, i.toDouble)
        , Array(s.e.toDouble, i.toDouble))}
    }
    val (starts, ends) = vertexStartsEndsSeq.unzip
    val c = MyMath.getPolygonCentroid(starts ++ ends.reverse)

    source.setCentroidRaw(
      Point2D_Double(c._1, c._2)
        + Point2D_Double(source.offset)
        + Point2D_Double.POINT_ONE) //reference in (1,1)
    source.setCentroidAlgorithm(SOURCE_CENTROID_ALGORITHM_CENTROID_POLYGON)
  }
  //---------------------------------------------------------------------------
  def setCentroidBySextractor(img: MyImage
                              , sourceSeq: Array[Segment2D]
                              , sextractorCsvCatalog: Option[String] = None) = {
    val sextractorCentroidSeq =
      if (sextractorCsvCatalog.isDefined) com.common.image.estimator.sextractor.Sextractor.loadCentroidFromCsv(sextractorCsvCatalog.get)
      else com.common.image.estimator.sextractor.Sextractor.getCentroidSeq(img)

    //append the current centroid to kdree
    val k2d_Tree = K2d_TreeDouble(s"Centroids of ${img.getRawName()}")
    val map = scala.collection.mutable.Map[Int, Segment2D]()
    sourceSeq foreach (s => map(s.id) = s)
    val centroidSeq = sourceSeq map (seg2D => (seg2D.getCentroid(), seg2D.id))
    k2d_Tree.addSeq(centroidSeq map (t => (t._1.toSequence, t._2)))

    //set centroids to nan
    sourceSeq.foreach { s =>
      s.setCentroidRaw(Point2D_Double.POINT_NAN)
      s.setCentroidRaDec(Point2D_Double.POINT_NAN)
    }

    //update centroids
    sextractorCentroidSeq foreach { centroid =>
      val closestSource = k2d_Tree.getKNN(centroid.toSequence())
      val source = map(closestSource.head.v)
      val distance = source.getCentroid().getDistance(centroid)
      if (distance < 1) {
        source.setCentroidRaw(centroid)
        source.setCentroidRaDec(img.pixToSky(centroid))
      }
    }
    sourceSeq.filter(source => !source.getCentroid().x.isNaN && !source.getCentroid().y.isNaN)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Centroid.scala
//=============================================================================
