/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  11/Oct/2021
 * Time:  13h:07m
 * Description:
 * expected enabled columns and order in 'default.param'

  X_IMAGE                  Object position along x                                   [pixel]
  Y_IMAGE                  Object position along y                                   [pixel]
  XMIN_IMAGE               Minimum x-coordinate among detected pixels                [pixel]
  XMAX_IMAGE               Maximum x-coordinate among detected pixels                [pixel]
  YMIN_IMAGE               Minimum y-coordinate among detected pixels                [pixel]
  YMAX_IMAGE               Maximum y-coordinate among detected pixels                [pixel]
  FLUX_ISO                 Isophotal flux                                            [count]
  FLAGS                    Extraction flags
 **/
//=============================================================================
package com.common.image.estimator.sextractor
//=============================================================================
import com.common.geometry.point.{Point2D_Double}
import com.common.image.myImage.MyImage
import com.common.util.file.MyFile
import com.common.util.path.Path
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import scala.io.Source
import sys.process._
//=============================================================================
//=============================================================================
object Sextractor {
  //---------------------------------------------------------------------------
  private val RUN_DIR = Path.ensureEndWithFileSeparator("/home/rafa/proyecto/m2/input/sextractor/")
  private val RUN_SEX = RUN_DIR + "sex"
  private val TEMPLATE_CONFIG = RUN_DIR + "default.sex"
  //---------------------------------------------------------------------------
  private def createConfigurationFile(imageName: String) = {
    val outputFilename = RUN_DIR + imageName +  ".sex"
    val catFilename = RUN_DIR + imageName +  ".cat"
    val bw = new BufferedWriter(new FileWriter(new File(outputFilename)))
    val bufferedSource = Source.fromFile(TEMPLATE_CONFIG)
    for (line <- bufferedSource.getLines) {
      if (line.startsWith("CATALOG_NAME")) bw.write("CATALOG_NAME " + catFilename)
      else {
        if (line.startsWith("PARAMETERS_NAME")) bw.write("PARAMETERS_NAME " + RUN_DIR +  "sextractor_centroid.param")
        else
          if (line.startsWith("FILTER_NAME")) bw.write("FILTER_NAME " + RUN_DIR +  "default.conv")
          else bw.write(line)
      }
      bw.write("\n")
    }
    bufferedSource.close
    bw.close()
    (outputFilename,catFilename)
  }
  //---------------------------------------------------------------------------
  def loadCentroidFromCsv(catFilename: String) = {
    val bufferedSource = Source.fromFile(catFilename)
    val r = bufferedSource.getLines.toArray.flatMap { line =>
      if(!line.startsWith("#")) {
        val seq = line.trim.split(" ").filter( !_.isEmpty)
        Some(Point2D_Double(seq(0).toDouble, seq(1).toDouble))
      }
      else None
    }
    bufferedSource.close
    r
  }
  //---------------------------------------------------------------------------
  def getCentroidSeq(img: MyImage) = {
    val r = createConfigurationFile(img.id.toString)

    val command = RUN_SEX + " " + img.name + " -c " + r._1
    command.!!

    val posSeq = loadCentroidFromCsv(r._2)

    MyFile.deleteFileIfExist(r._1)
    MyFile.deleteFileIfExist(r._2)
    MyFile.deleteFileIfExist(r._2 + ".deleteme")
    posSeq
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Sextractor.scala
//=============================================================================
