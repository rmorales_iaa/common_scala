/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  29/Oct/2020
 * Time:  10h:36m
 * Description: Code ported from https://github.com/djones1040/PythonPhot/tree/master/PythonPhot
 * The original algorithm 'cntrd' was originally written in: https://idlastro.gsfc.nasa.gov/ftp/pro/idlphot/cntrd.pro
 */
//=============================================================================
package com.common.image.estimator.centroid.daophot
//=============================================================================
//=============================================================================
import com.common.geometry.segment.segment2D.Segment2D
import com.common.dataType.pixelDataType.PixelDataType.PIXEL_DATA_TYPE
import com.common.geometry.point.Point2D_Double
import com.common.image.myImage.MyImage
import com.common.image.estimator.centroid.Centroid.{SOURCE_CENTROID_ALGORITHM_DAOPHOT_CNTRD}
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
//Compute the centroid of a paper using a derivative search.
/* Maximum pixel within distance from input pixel X, Y  determined
from FHWM is found and used as the center of a square, within
which the centroid is computed as the value (XCEN,YCEN) at which
the derivatives of the partial sums of the input image over (y,x)
with respect to (x,y) = 0.  In order to minimize contamination from
neighboring stars a weighting factor W is defined as unity in
center, 0.5 at end, and linear in between */
object CNTRD {
  //---------------------------------------------------------------------------
  //fwhm must be calculated before this call
  def calculateCentroid(img: MyImage
                        , s:Segment2D
                        , extendbox: Int =  0
                        , keepcenter: Boolean = false) =
    calculate(img, Array(s), extendbox, keepcenter)
  //---------------------------------------------------------------------------
  //fwhm must be calculated before this call
  private def calculate(img: MyImage
                        , sourceSeq: Array[Segment2D]
                        , extendbox: Int = 0
                        , keepcenter: Boolean = false): Unit= {
    sourceSeq.foreach { source =>
      val estimatedFwhm = if (source.getFwhm() > 0) source.getFwhm() else img.getFwhmAverage()
      val calculatedCentroid = getCentroidSingleSource(
          img
        , Array(source.getCentroid() - Point2D_Double.POINT_ONE) //centroid with orign (0,0)
        , estimatedFwhm
        , extendbox
        , keepcenter).head
      if (!calculatedCentroid.oneComponentIsNan && !calculatedCentroid.oneComponentIsInfinite) {
        source.setLowResolutionCentroid(false)
        source.setCentroidRaw(calculatedCentroid + Point2D_Double.POINT_ONE) //back to origin (1,1)
        source.setCentroidAlgorithm(SOURCE_CENTROID_ALGORITHM_DAOPHOT_CNTRD)
      }
    }
  }
  //---------------------------------------------------------------------------
  private def getCentroidSingleSource(
      img: MyImage
    , posEstimatedSeq: Array[Point2D_Double]
    , fwhm : Double //Full width at half maximum
    , extendbox: Int =  0
    , keepcenter: Boolean = false) : Array[Point2D_Double] = {
    //--------------------------------------------------------------------------
    val xImagePixSize = img.xMax
    val yImagePixSize = img.yMax
    val nhalf = Math.max(2, 0.637 * fwhm).toInt
    val boxWidth = 2 * nhalf + 1 // Width of box to be used to compute centroid
    val nnhalfbig = nhalf + extendbox
    val result = ArrayBuffer[Point2D_Double]()
    //--------------------------------------------------------------------------
    def findMaxPosition(x: Int, y:Int) : Option[(Int,Int)] = {
      val r =
        if (keepcenter) Some((x,y))
        else {
          //check is pos is at the border
          if ((x < nnhalfbig || (x + nnhalfbig) > xImagePixSize -1) ||
             (y < nnhalfbig || (y + nnhalfbig) > yImagePixSize -1)) return None
          else {
            val subImage = MyImage(img.getSubMatrix(x - nnhalfbig, y - nnhalfbig, x + nnhalfbig, y + nnhalfbig), img.getSimpleFits())
            val (_, _, _, pixMaxPosSeq) = subImage.findMinMax()
            val (idx,idy) = {
              if (pixMaxPosSeq.length > 1) (pixMaxPosSeq.map(_.x).sum/pixMaxPosSeq.length, Math.round( pixMaxPosSeq.map(_.y).sum.toFloat/pixMaxPosSeq.length))
              else (pixMaxPosSeq(0).x, pixMaxPosSeq(0).y)
            }
            Some((x - (nhalf + extendbox) + idx, y - (nhalf + extendbox) + idy))
          }
        }
      // check if the location of the max pix is at the border
      val xmax = r.get._1
      val ymax = r.get._2
      if ((xmax < nhalf) || ((xmax + nhalf) > xImagePixSize-1) ||
          (ymax < nhalf) || ((ymax + nhalf) > yImagePixSize-1)) None
      else Some((xmax, ymax))
    }
    //--------------------------------------------------------------------------
    def computeCentroid(
      data: Array[Array[PIXEL_DATA_TYPE]]
      , weight: Array[Double]
      , dd: Array[Double]
      , ddPowerWeightedSum : Double
      , dimensionSize: Int
      , isX_Axis : Boolean) : Option[Double] = {

     if (data.isEmpty) return None
     val sumDerivate =
       if (isX_Axis) {
         (for (x <- 0 until data(0).length) yield {
           var sum = 0d
           for (y <- 0 until data.length) sum += data(y)(x)
           sum
         }).toArray
       }
       else data map (_.sum.toDouble)

      val weightedData = (weight,sumDerivate).zipped.map(_ * _)
      val weightedDD_Data = (dd,weightedData).zipped.map(_ * _)

      val weightedDataSum = weightedDD_Data.sum
      val weightedDD_DataSum = weightedData.sum

      if (weightedDataSum >=0) return None //Reject if X derivative not decreasing
      val pos = ddPowerWeightedSum * weightedDD_DataSum / (weight.sum * weightedDataSum)
      if (Math.abs(pos) > nhalf) return None // # Reject if centroid outside box

      Some(dimensionSize - pos)
    }
    //--------------------------------------------------------------------------
    posEstimatedSeq foreach  { pos=>
      val x = Math.round(pos.x).toInt
      val y = Math.round(pos.y).toInt

      //find the position of the max value
      val pMax = findMaxPosition(x,y)
      if (pMax.isEmpty) result += Point2D_Double.POINT_NAN  //None result for this estimated center
      else{
        val xmax = pMax.get._1
        val ymax = pMax.get._2

        //subImage used to compute centroid which center is the max value
        val subImage = img.getSubMatrix(Math.round(xmax-nhalf), Math.round(ymax-nhalf), Math.round(xmax+nhalf), Math.round(ymax+nhalf))
        val dd = (for (i <- 0 until boxWidth-1) yield i + 0.5d - nhalf).toArray
        val weight = dd.map{ di=> 1d - (0.5 * (Math.abs(di) - 0.5d) / (nhalf - 0.5d))}.toArray // Weighting factor W unity in center, 0.5 at end, and linear in between
        val rowSeq = subImage.getAsRowSeq

        val derivateX = rowSeq.drop(1).dropRight(1).map { row=>
          val r = row.drop(1) :+ row.head  //move first pixel to the end and subtract to get derivative
          (r,row).zipped.map(_ - _).dropRight(1) .toArray //remove borders
        }

        val derivateY = (rowSeq.drop(1) :+ rowSeq.head).zipWithIndex.map { case (row,i) => //move first row to the end and subtract to get derivative
          (row,rowSeq(i)).zipped.map(_ - _).drop(1).dropRight(1).toArray //remove borders
        }.dropRight(1) //remove borders

        val ddPower2 = (dd,dd).zipped.map(_ * _)
        val ddPowerWeightedSum = (ddPower2,weight).zipped.map(_ * _).sum

        val centreX = computeCentroid(derivateX, weight, dd, ddPowerWeightedSum, xmax, isX_Axis = true)
        val centreY = computeCentroid(derivateY, weight, dd, ddPowerWeightedSum, ymax, isX_Axis = false)

        if (centreX.isEmpty || centreY.isEmpty) result += Point2D_Double.POINT_NAN
        else result += new Point2D_Double(centreX.get,centreY.get)
      }
    }
    result.toArray
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CNTRD.scala
//=============================================================================
