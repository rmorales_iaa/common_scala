/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  29/Oct/2020
 * Time:  10h:36m
 * Description: Code ported from : https://idlastro.gsfc.nasa.gov/ftp/pro/idlphot/gcntrd.pro
 *  Compute the stellar centroid by Gaussian fits to marginal X,Y, sums
 */
//=============================================================================
package com.common.image.estimator.centroid.daophot
//=============================================================================
import com.common.dataType.pixelDataType.PixelDataType.PIXEL_DATA_TYPE
import com.common.geometry.point.Point2D_Double
import com.common.image.myImage.MyImage
import com.common.geometry.point.Point2D
import com.common.geometry.segment.segment2D.Segment2D
import com.common.geometry.matrix.matrix2D.Matrix2D
import com.common.image.estimator.centroid.Centroid.{SOURCE_CENTROID_ALGORITHM_DAOPHOT_GCNTRD}
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
/* Unless /KEEPCENTER is set, a small area around the initial X,Y is
  convolved with a Gaussian kernel, and the maximum pixel is found.
  This pixel is used as the  center of a square, within
  which the centroid is computed as the Gaussian least-squares fit
   to the  marginal sums in the X and Y directions */
object GCNTRD {
  //---------------------------------------------------------------------------
  private val MAX_BOX = 13
  //---------------------------------------------------------------------------
  def calculateCentroid(img: MyImage //fwhm must be calculated before this call
                        , s: Segment2D
                        , keepcenter: Boolean = false) =
    calculateCentroidSeq(img, Array(s), keepcenter)
  //---------------------------------------------------------------------------
  //fwhm must be calculated before this call
  private def calculateCentroidSeq(img: MyImage
                                  , sourceSeq: Array[Segment2D]
                                  , keepcenter: Boolean = false): Unit= {
    sourceSeq.foreach { source =>
      val estimatedFwhm = if (source.getFwhm() > 0) source.getFwhm() else img.getFwhmAverage()
      val calculatedCentroid = getCentroidSingleSource(img
                                                       , Array(source.getCentroid() - Point2D_Double.POINT_ONE)  //centroid with orign (0,0)
                                                       , estimatedFwhm
                                                       , keepcenter).head
      if (!calculatedCentroid.oneComponentIsNan && !calculatedCentroid.oneComponentIsInfinite) {
        source.setLowResolutionCentroid(false)
        source.setCentroidRaw(calculatedCentroid + Point2D_Double.POINT_ONE) //back to origin (1,1)
        source.setCentroidAlgorithm(SOURCE_CENTROID_ALGORITHM_DAOPHOT_GCNTRD)
      }
    }
  }
  //---------------------------------------------------------------------------
  private def getCentroidSingleSource (
    img: MyImage
    , posEstimatedSeq: Array[Point2D_Double]
    , fwhm : Double //Full width at half maximum
    , keepcenter: Boolean = false) : Array[Point2D_Double] = {
    //--------------------------------------------------------------------------
    val xImagePixSize = img.xMax
    val yImagePixSize = img.yMax
    val radius = Math.max(2.001d, 0.637d * fwhm)
    val radsq = radius * radius
    val sigsq = ( fwhm/2.35482 ) * ( fwhm/2.35482 )
    val nhalf = Math.min(radius.toInt, (MAX_BOX-1)/2)
    val nbox = 2 * nhalf + 1 	//of pixels in side of convolution box
    val result = ArrayBuffer[Point2D_Double]()
    //--------------------------------------------------------------------------
    def convolve2D(m: Matrix2D, kernel: ArrayBuffer[ArrayBuffer[Double]]) = {
      //adpated from: https://github.com/PacktPublishing/Java-Machine-Learning-for-Computer-Vision/blob/master/EdgeDetection/src/main/java/ramo/klevis/ml/Convolution.java
      //------------------------------------------------------------------------
      val kernelHeight = kernel.length
      val kernelWidth = kernel(0).length
      val smallWidth = m.xMax - kernelWidth + 1
      val smallHeight = m.yMax - kernelHeight + 1
      val top  = kernelHeight / 2
      val left = kernelWidth / 2
      val convolvedMatrix = ArrayBuffer.fill[Double](m.xMax, m.yMax)(0d)
      //------------------------------------------------------------------------
      for (y<- 0 until smallHeight; x<- 0 until smallWidth) {
        var v = 0d
        for (h<- 0 until kernelHeight;w<- 0 until kernelWidth)
          v += m(x + w, y + h) * kernel(h)(w)
        convolvedMatrix(y + top)(x + left) = v
      }
      convolvedMatrix
    }
    //--------------------------------------------------------------------------
    def findMaxPosition(x: Int, y:Int,convolutionedKernel: Option[ArrayBuffer[ArrayBuffer[Double]]]) : Option[(Int,Int)] = {

      if (keepcenter) return Some((x,y))

      //check is pos is at the border
      if ((x < nhalf || (x + nhalf) > xImagePixSize - 1) ||
          (y < nhalf || (y + nhalf) > yImagePixSize - 1)) None
      else {
        //get the realted subMatrix
        val xMin = Math.max(x - nbox, 0)
        val xMax = Math.min(x + nbox, xImagePixSize - 1)
        val yMin = Math.max(y - nbox, 0)
        val yMax = Math.min(y + nbox, yImagePixSize - 1)
        val subMatrix = MyImage(img.getSubMatrix(xMin, yMin, xMax, yMax), img.getSimpleFits())

        //convolve the subMatrix with the kernel
        val convolvedMatrix = convolve2D(subMatrix, convolutionedKernel.get)

        //avoid borders in the convolution
        val axisSize = nhalf * 2 + 1
        val offset = nbox - nhalf
        val convolvedMatrixNoBorder = ArrayBuffer.fill[Double](axisSize, axisSize)(0d)
        for (h <- 0 until axisSize; w <- 0 until axisSize)
          convolvedMatrixNoBorder(h)(w) = convolvedMatrix(offset + h)(offset + w)

        //find max
        var max = convolvedMatrixNoBorder(0)(0)
        for (h <- 0 until axisSize; w <- 0 until axisSize) yield
          if (convolvedMatrixNoBorder(h)(w) > max) max = convolvedMatrixNoBorder(h)(w)

        val pixMaxPosSeq = (for (h <- 0 until axisSize; w <- 0 until axisSize) yield
          if (convolvedMatrixNoBorder(h)(w) == max) Some(Point2D(w, h)) else None).flatten

        val (idx, idy) =
          if (pixMaxPosSeq.length > 1) (pixMaxPosSeq.map(_.x).sum / pixMaxPosSeq.length, Math.round(Int.int2float(pixMaxPosSeq.map(_.y).sum / pixMaxPosSeq.length)))
          else (pixMaxPosSeq(0).x, pixMaxPosSeq(0).y)

        // check if the location of the max pix is at the border
        val xmax = x- nhalf + idx
        val ymax = y- nhalf + idy
        if ((xmax < nhalf) || ((xmax + nhalf) > xImagePixSize-1) ||
          (ymax < nhalf) || ((ymax + nhalf) > yImagePixSize-1)) None
        else Some((xmax, ymax))
      }
    }
    //--------------------------------------------------------------------------
    def computeCentroid(
      subMatrix: Array[Array[PIXEL_DATA_TYPE]]
      , kernel: Array[Array[Double]]
      , weightedMatrix: Array[Array[Double]]
      , wt: Array[Double]
      , maxPos : Int
      , isX_Axis : Boolean) : Option[Double] = {

      if (subMatrix.isEmpty) return None
      //------------------------------------------------------------------------
      val wSubMatrix =
        for (y <- 0 until subMatrix.length) yield
          for(x <- 0 until subMatrix(0).length) yield
            subMatrix(y)(x) * weightedMatrix(y)(x)

      val wKernel =
        for (y <- 0 until kernel.length) yield
          for(x <- 0 until kernel(0).length) yield
            kernel(y)(x) * weightedMatrix(y)(x)

      val sd =
        if (isX_Axis) {
          (for (x <- 0 until wSubMatrix(0).length) yield {  //sum all cols
            var sum = 0d
            for (y <- 0 until wSubMatrix.length) sum += wSubMatrix(y)(x)
            sum
          }).toArray
        }
        else (wSubMatrix map (_.sum)).toArray //sum all rows

      val sg =
        if (isX_Axis) {
          (for (x <- 0 until wKernel(0).length) yield {  //sum all cols
            var sum = 0d
            for (y <- 0 until wKernel.length) sum += wKernel(y)(x)
            sum
          }).toArray
        }
        else (wKernel map (_.sum)).toArray //sum all rows

      val sumgSimple = (wt,sg).zipped.map( _ * _ )
      val sumgSquared = (sumgSimple,sg).zipped.map( _ * _ )
      val sumg = sumgSimple.sum
      val sumgsq = sumgSquared.sum

      val sumgd = (sumgSimple,sd).zipped.map( _ * _ ).sum
      val sumd = (wt,sd).zipped.map( _ * _ ).sum
      val p = wt.sum

      val _vec  = (for(v<- 0 until nbox) yield nhalf - v).toArray
      val dgd_ = (sg,_vec).zipped.map( _ * _ )
      val wdgd_Simple = (wt,dgd_).zipped.map( _ * _ )
      val dgd_Squared = (dgd_,dgd_).zipped.map( _ * _ )

      val sdgd_s = (wt,dgd_Squared).zipped.map( _ * _ ).sum
      val sdgd_ = wdgd_Simple.sum
      val sddgd_ = (wdgd_Simple,sd).zipped.map( _ * _ ).sum
      val sgdgd_ = (wdgd_Simple,sg).zipped.map( _ * _ ).sum

      val h_ = (sumgd - sumg*sumd/p) / (sumgsq - (sumg*sumg) / p)
      if (h_ <= 0) None  //Can not fit the gaussian
      else {
        val skyLevel = (sumd - h_ * sumg) / p
        val d_ = (sgdgd_ - (sddgd_ - sdgd_ * (h_ * sumg + skyLevel * p))) / (h_ * sdgd_s / sigsq)
        if (Math.abs(d_) >= nhalf) None //Too far from initial guess
        else Some(maxPos + d_)
      }
    }
    //--------------------------------------------------------------------------
    def createGaussianKernelAndMask() = {
      val row = (for (x<- 0 until nbox) yield ((x - nhalf) * (x - nhalf)).toDouble).toArray
      val kernel = ArrayBuffer.fill[Double](nbox, nbox)(0d)
      for (x<- 0 until nbox) kernel(nhalf)(x) += row(x)
      for (i<- 1 to nhalf) {
         val newRow = row map ( _ + (i * i))
         for (x<- 0 until nbox) kernel(nhalf-i)(x) = newRow(x)
         for (x<- 0 until nbox) kernel(nhalf+i)(x) = newRow(x)
      }
      val mask = kernel.map( _.map (v=> if (v >= radsq) 0 else 1))
      val maskNonZeroPixPosSeq = mask.flatten.zipWithIndex.flatMap {case(v,i)=> if (v > 0) Some(i) else None}
      for (y<- 0 until nbox;x<- 0 until nbox) kernel(y)(x) = Math.exp(-0.5d * kernel(y)(x) / sigsq)
      (kernel.toArray.map(_.toArray),mask,maskNonZeroPixPosSeq.toArray)
    }
    //--------------------------------------------------------------------------
    def createWeightedMatrix() = {
      val weightedRow = (for (x<- 0 until nbox) yield nhalf - Math.abs(x - nhalf) + 1d).toArray
      val weightedMatrixX = for (_<- 0 until nbox) yield weightedRow
      val weightedMatrixY = ArrayBuffer.fill[Double](nbox, nbox)(0d)
      for (y<- 0 until nbox;x<- 0 until nbox) weightedMatrixY(y)(x) = weightedMatrixX(x)(y) //transpose
      (weightedRow, weightedMatrixX.toArray,weightedMatrixY.toArray.map(_.toArray))
    }
    //--------------------------------------------------------------------------
    def getConvolutionedKernel(
        kernel: Array[Array[Double]]
      , mask: ArrayBuffer[ArrayBuffer[Int]]
      , maskNonZeroPixPosSeq: Array[Int]) = {

      if (keepcenter) None
      else { //precompute convolution kernel
        val convolutionedKernel = ArrayBuffer.fill[Double](nbox, nbox)(0d)
        var sum = 0d
        var sumSquare = 0d
        for (y <- 0 until nbox; x <- 0 until nbox) {
          val v = kernel(y)(x) * mask(y)(x)
          sum += v
          sumSquare += (v * v)
          convolutionedKernel(y)(x) = v
        }
        val maskNonZeroPixPosSeqLength = maskNonZeroPixPosSeq.length
        sumSquare = sumSquare - ((sum * sum) / maskNonZeroPixPosSeqLength)
        sum = sum / maskNonZeroPixPosSeqLength
        var i = 0
        for (y <- 0 until nbox; x <- 0 until nbox) {
          if (maskNonZeroPixPosSeq.contains(i))
            convolutionedKernel(y)(x) = (convolutionedKernel(y)(x) - sum) / sumSquare
          i +=1
        }
        Some(convolutionedKernel)
      }
    }
    //--------------------------------------------------------------------------
    val (kernel,mask,maskNonZeroPixPosSeq) = createGaussianKernelAndMask
    val (weightedRow, weightedMatrixX,weightedMatrixY) = createWeightedMatrix
    val convolutionedKernel = getConvolutionedKernel(kernel, mask, maskNonZeroPixPosSeq)

    posEstimatedSeq map { pos=>
      val x = Math.round(pos.x).toInt
      val y = Math.round(pos.y).toInt

      //find the position of the max value
      val pMax = findMaxPosition(x,y, convolutionedKernel)
      if (pMax.isEmpty) result += Point2D_Double.POINT_NAN//None result for this estimated center
      else{
        val xmax = pMax.get._1
        val ymax = pMax.get._2

        //subMatrix used to compute centroid which center is the max value
        val subMatrix = img.getSubMatrix(xmax-nhalf, ymax-nhalf, xmax+nhalf, ymax+nhalf).toRawMatrix
        val centreX = computeCentroid(subMatrix, kernel, weightedMatrixY, weightedRow, xmax, isX_Axis = true)
        val centreY = computeCentroid(subMatrix, kernel, weightedMatrixX, weightedRow, ymax, isX_Axis = false)

        if (centreX.isEmpty || centreY.isEmpty) result += Point2D_Double.POINT_NAN
        else result += new Point2D_Double(centreX.get,centreY.get)
      }
    }
    result.toArray
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file GCNTRD.scala
//=============================================================================
