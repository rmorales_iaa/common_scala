/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Dec/2021
 * Time:  15h:28m
 * Description: None
 */
//=============================================================================
package com.common.image.estimator.ellipse
//=============================================================================
import com.common.geometry.point.Point2D_Double
//=============================================================================
//=============================================================================
case class Ellipse(centre: Point2D_Double, majorAxis: Double, minorAxis: Double, angleDeg: Double) {
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Ellipse
  val area = Math.PI * majorAxis * minorAxis
  val eccentricity =  {
    val c = minorAxis / majorAxis
    Math.sqrt(1d - (c * c))
  }
  //---------------------------------------------------------------------------
  def getArea() = Math.PI * majorAxis * minorAxis
  //---------------------------------------------------------------------------
}
//=============================================================================
object EllipseFit {
  //---------------------------------------------------------------------------
  sealed trait ELLIPSE_FIT_FIT_ALGORITHM
  //---------------------------------------------------------------------------
  case object ELLIPSE_DIRECT_FIT        extends ELLIPSE_FIT_FIT_ALGORITHM
  case object ELLIPSE_MY_FIT            extends ELLIPSE_FIT_FIT_ALGORITHM
  case object ELLIPSE_CGAL_FIT          extends ELLIPSE_FIT_FIT_ALGORITHM
  //---------------------------------------------------------------------------
  def fit(pointSeq: Array[Point2D_Double]
          , algorithm: ELLIPSE_FIT_FIT_ALGORITHM) : Option[Ellipse] = {

    val pointSeqSeq = pointSeq map (p=> Array(p.x,p.y))
    algorithm match {
      case ELLIPSE_DIRECT_FIT     => Some(EllipseDirectFit.fit(pointSeqSeq))
      case ELLIPSE_MY_FIT         => MyEllipseFit(pointSeq).fit
      case ELLIPSE_CGAL_FIT       => EllipseCgalFit.fit(pointSeqSeq.flatten)
    }
  }
  //---------------------------------------------------------------------------
  //https://github.com/mdoube/BoneJ/blob/master/src/org/doube/geometry/FitEllipse.java
  def varToDimensions(ellipse: Array[Double]): Array[Double] = {
    val a = ellipse(0)
    val b = ellipse(1) / 2
    val c = ellipse(2)
    val d = ellipse(3) / 2
    val f = ellipse(4) / 2
    val g = ellipse(5)
    // centre
    val cX = (c * d - b * f) / (b * b - a * c)
    val cY = (a * f - b * d) / (b * b - a * c)
    // semiaxis length
    val af = 2 * (a * f * f + c * d * d + g * b * b - 2 * b * d * f - a * c * g)
    val aL = Math.sqrt(af / ((b * b - a * c) * (Math.sqrt((a - c) * (a - c) + 4 * b * b) - (a + c))))
    val bL = Math.sqrt(af / ((b * b - a * c) * (-Math.sqrt((a - c) * (a - c) + 4 * b * b) - (a + c))))
    var phi = 0d
    if (b == 0) {
      if (a <= c) phi = 0
      else
        if (a > c) phi = Math.PI / 2;
    }
    else {
      if (a < c) phi = Math.atan(2 * b / (a - c)) / 2;
      else
        if (a > c) phi = Math.atan(2 * b / (a - c)) / 2 + Math.PI / 2;
    }
    val dimensions = Array(cX, cY, aL, bL, phi)
    dimensions
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file EllipseFit.scala
//=============================================================================
