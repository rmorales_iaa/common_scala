/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Dec/2021
 * Time:  11h:43m
 * Description: None
 */
//=============================================================================
package com.common.image.estimator.ellipse
//=============================================================================
import Jama.Matrix
import com.common.geometry.point.Point2D_Double
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
object EllipseDirectFit {
  //---------------------------------------------------------------------------
  private def getCentroid2D(points: Array[Array[Double]]): Array[Double] = {
    val centroid = new Array[Double](2)
    var sumX = 0d
    var sumY = 0d
    val nPoints = points.length
    for (n <- 0 until nPoints) {
      sumX += points(n)(0)
      sumY += points(n)(1)
    }
    centroid(0) = sumX / nPoints
    centroid(1) = sumY / nPoints
    centroid
  }
  //----------------------------------------------------------------------------
  //https://github.com/mdoube/BoneJ/blob/master/src/org/doube/geometry/FitEllipse.java
  def fit(points: Array[Array[Double]]): Ellipse = {
    val nPoints = points.length
    val centroid = getCentroid2D(points)
    val xC = centroid(0)
    val yC = centroid(1)
    val d1 = ArrayBuffer[ArrayBuffer[Double]]()
    for (i <- 0 until nPoints) {
      val xixC = points(i)(0) - xC
      val yiyC = points(i)(1) - yC
      d1 += ArrayBuffer(xixC * xixC, xixC * yiyC,yiyC * yiyC)
    }
    val D1 = new Matrix((d1 map (_.toArray)).toArray)

    val d2 = ArrayBuffer[ArrayBuffer[Double]]()
    for (i <- 0 until nPoints)
      d2 += ArrayBuffer(points(i)(0) - xC, points(i)(1) - yC, 1d)

    val D2 = new Matrix((d2 map (_.toArray)).toArray)
    val S1 = D1.transpose.times(D1)
    val S2 = D1.transpose.times(D2)
    val S3 = D2.transpose.times(D2)
    val T = S3.inverse.times(-(1)).times(S2.transpose)
    val M = S1.plus(S2.times(T))
    val m = M.getArray
    val n = Array(Array(m(2)(0) / 2, m(2)(1) / 2, m(2)(2) / 2), Array(-m(1)(0), -m(1)(1), -m(1)(2)), Array(m(0)(0) / 2, m(0)(1) / 2, m(0)(2) / 2))
    val N = new Matrix(n)
    val E = N.eig
    val eVec = E.getV
    val R1 = eVec.getMatrix(0, 0, 0, 2)
    val R2 = eVec.getMatrix(1, 1, 0, 2)
    val R3 = eVec.getMatrix(2, 2, 0, 2)
    val cond = R1.times(4).arrayTimes(R3).minus(R2.arrayTimes(R2))
    var f = 0
    for (i <- 0 until 3) {
      if (cond.get(0, i) > 0) {
        f = i
        //break //todo: break is not supported
      }
    }
    val A1 = eVec.getMatrix(0, 2, f, f)
    var A = new Matrix(6, 1)
    A.setMatrix(0, 2, 0, 0, A1)
    A.setMatrix(3, 5, 0, 0, T.times(A1))
    val a = A.getColumnPackedCopy
    val a4 = a(3) - 2 * a(0) * xC - a(1) * yC
    val a5 = a(4) - 2 * a(2) * yC - a(1) * xC
    val a6 = a(5) + a(0) * xC * xC + a(2) * yC * yC + a(1) * xC * yC - a(3) * xC - a(4) * yC
    A.set(3, 0, a4)
    A.set(4, 0, a5)
    A.set(5, 0, a6)
    A = A.times(1 / A.normF)
    val r = A.getColumnPackedCopy
    val p = EllipseFit.varToDimensions(r)
    Ellipse(Point2D_Double(p(0),p(1)), p(2), p(3), Math.toDegrees(p(4)))
  }
}
//=============================================================================
//End of file EllipseFitDirect.scala
//=============================================================================
