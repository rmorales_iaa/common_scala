/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.common.image.estimator.ellipse
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.logger.MyLogger
//=============================================================================
import org.apache.commons.math3.analysis.{MultivariateMatrixFunction, MultivariateVectorFunction}
import org.apache.commons.math3.fitting.leastsquares.{LeastSquaresBuilder, LevenbergMarquardtOptimizer}
import Math._
//---------------------------------------------------------------------------
//f(x,y,x_0,y_0,A,B,T):=(x*cos(T)+y*sin(T)-x_0)^2/A^2+((-x)*sin(T)+y*cos(T)-y_0)^2/B^ = 1
case class MyEllipseFit(samplePointSeq: Array[Point2D_Double]
                        , maxEvaluations: Int = 1000
                        , maxIterations: Int = 100) extends MyLogger {
  //-------------------------------------------------------------------------
  //evaluate the function using the sample sequence with the NEW parameters
  private def function: MultivariateVectorFunction = new MultivariateVectorFunction() {
    //-----------------------------------------------------------------------
    override def value(parameterSeq: Array[Double]) : Array[Double] = {
      val x_0 = parameterSeq(0)
      val y_0 = parameterSeq(1)
      val A   = parameterSeq(2)
      val B   = parameterSeq(3)
      val T   = parameterSeq(4)

      val A2 = A * A
      val B2 = B * B
      val cosT = cos(T)
      val sinT = sin(T)

      samplePointSeq.map { p =>
        val ta = (p.x - x_0) * cosT + (p.y - y_0) * sinT
        val tb = (p.x - x_0) * sinT - (p.y - y_0) * cosT
        ((ta * ta) / A2) + ((tb * tb) / B2)
      }
    }
    //-----------------------------------------------------------------------
  }
  //-------------------------------------------------------------------------
  //evaluate the jacobian of the function using the sample sequence and the NEW parameters
  private def jacobian: MultivariateMatrixFunction = new MultivariateMatrixFunction() {
    override def value(parameterSeq: Array[Double]): Array[Array[Double]] = {
      val x_0 = parameterSeq(0)
      val y_0 = parameterSeq(1)
      val A   = parameterSeq(2)
      val B   = parameterSeq(3)
      val T   = parameterSeq(4)

      val A2 = A * A
      val B2 = B * B
      val A3 = A2 * A
      val B3 = B2 * B
      val cosT = cos(T)
      val sinT = sin(T)

      samplePointSeq.map { p =>
        val ta = sinT * (p.y - y_0) + cosT * (p.x - x_0)
        val tb = sinT * (p.x - x_0) - cosT * (p.y - y_0)

        Array(
           ((-2 * cosT * ta) / A2) - ((2 * sinT * tb) / B2)  //df(x,y)/dx_0
          , ((2 * cosT * tb) / B2) - ((2 * sinT * ta) / A2)  //df(x,y)/dy_0
          , (-2 * (ta * ta)) / A3                            //df(x,y)/dA
          , (-2 * (tb * tb)) / B3                            //df(x,y)/dB
          , ((2 * (cosT * (p.y - y_0) - sinT * (p.x-x_0)) * (sinT * (p. y - y_0) + cosT* (p.x-x_0))) / A2) +
            ( (2 * ta * tb)/ B2)                             //df(x,y)/dT
        )
      }
    }
  }
  //-------------------------------------------------------------------------
  private def getInitialParameterGuess = {
    val initialEllipse = EllipseDirectFit.fit(samplePointSeq map (_.toArray()))
    Array(
        initialEllipse.centre.x            //x_0
      , initialEllipse.centre.y            //y_0
      , initialEllipse.majorAxis           //A
      , initialEllipse.minorAxis           //B
      , toRadians(initialEllipse.angleDeg) //T
    )
  }
  //-------------------------------------------------------------------------
  def getTarget() =  Array.fill(samplePointSeq.length) (1d)   //see equation definition
  //-------------------------------------------------------------------------
  def fit() = {
    try {
      val lsb = new LeastSquaresBuilder()
        .model(function, jacobian)
        .target(getTarget())
        .start(getInitialParameterGuess)
        .maxEvaluations(maxEvaluations) //set upper limit of evaluation time
        .maxIterations(maxIterations)   //set upper limit of iteration time
      val optimizer = new LevenbergMarquardtOptimizer().optimize(lsb.build)
      val r = optimizer.getPoint.toArray
      if (r(0).isNaN || r(1).isNaN || r(2).isNaN || r(3).isNaN) None
      else Some(Ellipse(Point2D_Double(r(0),r(1)), r(2), r(3), toDegrees(r(4))))
    }
    catch {
      case e: Exception => None
    }
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file MyEllipseFit.scala
//=============================================================================
