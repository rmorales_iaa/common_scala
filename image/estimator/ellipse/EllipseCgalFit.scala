/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.common.image.estimator.ellipse
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.geometry.utils.Cgal
//=============================================================================
//https://scicomp.stackexchange.com/questions/40494/how-do-i-find-the-minimum-area-ellipse-that-encloses-a-set-of-points
object EllipseCgalFit {
  //---------------------------------------------------------------------------
  def fit(pointSeq: Array[Double]) = {
    val fit  = Array.fill[Double](5)(-1d)
    Cgal.ellipseFit(pointSeq, pointSeq.length/2, fit)
    if (fit.forall( _ == -1)) None
    else {
      val M = Math.max(fit(0), fit(1))
      val m = Math.min(fit(0), fit(1))
      Some(Ellipse(Point2D_Double(fit(2), fit(3)), M, m, fit(4)))
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file EllipseCgalFit.scala
//=============================================================================
