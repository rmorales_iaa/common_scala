/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  29/Aug/2024
 * Time:  10h:38m
 * Description: None
 */
package com.common.estimator.skyPosition
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.spiceSPK.SpiceSPK_DB
import com.common.geometry.point.Point2D_Double
import com.common.image.focusType.ImageFocusMPO
import com.common.image.myImage.MyImage
import com.common.image.telescope.Telescope
import com.common.orbitIntegrator.celestialBody.CelestialBody
import com.common.jpl.Spice._
import com.common.logger.MyLogger
//=============================================================================
import java.time.LocalDateTime
//=============================================================================
//=============================================================================
object EstimatedSkyPosition extends MyLogger {
  //---------------------------------------------------------------------------
  private final val ESTIMATED_SKY_POSITION_LOCAL_JPL_SPICE         = "LOCAL_JPL_SPICE"
  private final val ESTIMATED_SKY_POSITION_REMOTE_JPL_SPICE        = "REMOTE_JPL_SPICE"
  private final val ESTIMATED_SKY_POSITION_LOCAL_ORBIT_INTEGRATOR  = "LOCAL_ORBIT_INTEGRATOR"
  //---------------------------------------------------------------------------
  private final val ESTIMATED_SKY_POSITION_ALGORITHM_SEQ = MyConf.c.getStringSeq("Astrometry.estimatedSkyPositionAlgorithm.algorithmSequence")
  private final val JPL_SPICE_REFERENCE_FRAME            = MyConf.c.getString("Astrometry.estimatedSkyPositionAlgorithm.jplSpiceReferenceFrame")
  private final val JPL_SPICE_ABERRATION_CORRECTION      = MyConf.c.getString("Astrometry.estimatedSkyPositionAlgorithm.jplSpiceAberrationCorrection")
  //---------------------------------------------------------------------------
  def getPosWithFallBack(mpo: ImageFocusMPO
                         , observatoryOfficialCode: String
                         , observerSpkID: String
                         , timeStamp: String
                         , referenceFrame: String =  JPL_SPICE_REFERENCE_FRAME
                         , aberrationCorrection: String = JPL_SPICE_ABERRATION_CORRECTION
                         , verbose: Boolean = true): Point2D_Double = {

    val composedObjectName = mpo.composedName
    val targetSpkID = mpo.spiceSpk.spkID.toString
    val objectSpkPath = mpo.getSpkFilePath()
    if(!loadKernel(objectSpkPath,verbose = verbose)) return Point2D_Double.POINT_NAN

    ESTIMATED_SKY_POSITION_ALGORITHM_SEQ.foreach {
      //----------------------------------------------------------------------
      case ESTIMATED_SKY_POSITION_LOCAL_JPL_SPICE =>
        val r = getEphemerisAndPhaseAngle(
          targetSpkID
          , observerSpkID
          , timeStamp
          , referenceFrame
          , aberrationCorrection
          , calculatePhaseAngle = false
          , verbose)
        val estPos = Point2D_Double(r._1, r._2)
        if (estPos != Point2D_Double.POINT_INVALID) return estPos
        else
           if (verbose) error(s"Error getting the estimated sky position for algorithm:'$ESTIMATED_SKY_POSITION_LOCAL_JPL_SPICE' MPO:'${mpo.composedName}' observatory:'$observatoryOfficialCode' time stamp:'$timeStamp'")
      //----------------------------------------------------------------------
      case ESTIMATED_SKY_POSITION_REMOTE_JPL_SPICE =>
        val db = SpiceSPK_DB(collectionName = targetSpkID)
        val lastOrbit = db.getVersionSeq().head

        val estPos = getPosByJplWebQuery(
          objectName = targetSpkID
          , normalizedName = composedObjectName
          , orbit = lastOrbit
          , observatoryCode = observatoryOfficialCode
          , timeSeq = timeStamp)

        db.close()
        if (estPos != Point2D_Double.POINT_INVALID) return estPos
        else
          if (verbose) error(s"Error getting the estimated sky position for algorithm:'$ESTIMATED_SKY_POSITION_REMOTE_JPL_SPICE' MPO:'${mpo.composedName}' observatory:'$observatoryOfficialCode' time stamp:'$timeStamp'")

      //----------------------------------------------------------------------
      case ESTIMATED_SKY_POSITION_LOCAL_ORBIT_INTEGRATOR =>

        val r = CelestialBody.computeLocation(
            mpcID = mpo.spiceSpk.mpcID.toInt
          , timeStamp = LocalDateTime.parse(timeStamp)
          , observatoryOfficialCode)
        if (r.isDefined) return Point2D_Double(r.get)
        else
          if (verbose) error(s"Error getting the estimated sky position for algorithm:'$ESTIMATED_SKY_POSITION_LOCAL_ORBIT_INTEGRATOR' MPO:'${mpo.composedName}' observatory:'$observatoryOfficialCode' time stamp:'$timeStamp'")
      //----------------------------------------------------------------------
    }
    if (verbose) error(s"Error getting the estimated sky position using algorithms:'${ESTIMATED_SKY_POSITION_ALGORITHM_SEQ.mkString("{",",","}")}' MPO:'${mpo.composedName}' observatory:'$observatoryOfficialCode' time stamp:'$timeStamp'")
    Point2D_Double.POINT_INVALID
  }
  //---------------------------------------------------------------------------
  def getPosAtImageWithFallBack(mpo: ImageFocusMPO
                               , img: MyImage
                               , referenceFrame: String =  JPL_SPICE_REFERENCE_FRAME
                               , aberrationCorrection: String = JPL_SPICE_ABERRATION_CORRECTION
                               , verbose: Boolean = false): Point2D_Double = {
    val fits = img.getSimpleFits()
    img.getTelescope()
    getPosWithFallBack(
        mpo
      , Telescope.getOfficialCode(img.getTelescope())
      , Telescope.getSpkId(fits)
      , img.getObservingDateMidPoint().toString
      , referenceFrame
      , aberrationCorrection
      , verbose)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file EstimatedSkyPosition.scala
//=============================================================================