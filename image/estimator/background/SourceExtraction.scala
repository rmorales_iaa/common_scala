/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/Oct/2020
 * Time:  12h:21m
 * Description: None
 */
//=============================================================================
package com.common.image.estimator.background
//=============================================================================
import com.common.dataType.pixelDataType.PixelDataType.PIXEL_DATA_TYPE
import com.common.geometry.point.{Point2D_Double}
import com.common.image.myImage.MyImage
//=============================================================================
//=============================================================================
object SourceExtraction {
  //---------------------------------------------------------------------------
  def estimatedBackground(img: MyImage
                          , subImageCentre: Point2D_Double
                          , radius: Double
                          , noiseTide: PIXEL_DATA_TYPE
                          , sourceMaxPix: Int
                          , debug: Boolean = false): Double = {
    //-------------------------------------------------------------------------
    //get a square around the centre
    val squareSize = Point2D_Double(radius, radius)
    val subImage = img.getSubMatrixWithPadding((subImageCentre - squareSize).toPoint2D()
                                             , (subImageCentre + squareSize).toPoint2D())
    var subImageFlux = 0D
    subImage.data.foreach( subImageFlux += _ )
    if(debug) subImage.prettyPrint()

    //detect all sources above the noise tide
    val region = subImage.findSource(noiseTide, sourceSizeRestriction = Some((1,sourceMaxPix)))
    val sourceSeq = region.toSegment2D_seq()

    if(debug) sourceSeq.head.prettyPrint()

    //calculate the flux of all sources
    val sourceSeqFlux = sourceSeq.map(_.getFlux()).sum

    //calculate the pix size of all sources
    val sourceSeqPixCount = sourceSeq.map(_.getPixCount()).sum

    //calculate the effective pix count
    val backgroundPixCount = subImage.getPixSize - sourceSeqPixCount

    //calculate the effective flux
    val backgroundFlux = subImageFlux - sourceSeqFlux

    if (backgroundPixCount == 0 || backgroundFlux == 0) -1d
    else backgroundFlux / backgroundPixCount
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SourceExtraction.scala
//=============================================================================
