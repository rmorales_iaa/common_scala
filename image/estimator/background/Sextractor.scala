package com.common.image.estimator.background
//=============================================================================
import com.common.configuration.MyConf
import com.common.image.myImage.MyImage
import com.common.logger.MyLogger
import com.common.util.path.Path
import com.common.util.util.Util
//=============================================================================
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.UUID
import scala.io.Source
//=============================================================================
//=============================================================================
object Sextractor extends MyLogger {
  //---------------------------------------------------------------------------
  private val ASTROMATIC_PATH = Path.ensureEndWithFileSeparator(Path.getCurrentPath()) +
                                    MyConf.c.getString("Astromatic.sextracor_psfex.scriptPath")
  private val ASTROMATIC_SCRIPT_NAME = MyConf.c.getString("Astromatic.sextracor_psfex.scriptName")
  //---------------------------------------------------------------------------
  private def generateUniqueName(prefix: String = ""): String = {
    val timestamp = LocalDateTime.now()
    val formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS")
    val formattedTimestamp = timestamp.format(formatter)
    val uuid = UUID.randomUUID().toString.split("-").last
    s"$prefix$formattedTimestamp-rand-$uuid"
  }
  //---------------------------------------------------------------------------
  //run sextractor and pfsex to get the fwhm
  def estimateBackgroundAndFWHM(image: MyImage, verbose: Boolean) = {

    //call the script and get the results
    //sextractor is a modified version that generates a file with the Background and BackgroundRMS
    val imageUniqueID = generateUniqueName()

    val command = s"$ASTROMATIC_PATH/$ASTROMATIC_SCRIPT_NAME $ASTROMATIC_PATH '${image.name}' $imageUniqueID.cat sextractor_fits.config"

    Util.runShellSimple(command)

    //read the sextractor output
    val sextractorOutput = s"$ASTROMATIC_PATH/$imageUniqueID.cat.deleteme"
    var bufferedSource = Source.fromFile(sextractorOutput)
    var lineSeq = bufferedSource.getLines.toArray
    if (lineSeq.isEmpty) error(s"Image: '${image.getRawName()}' Error getting background from sextractor. Commmand: '$command'")
    lineSeq.foreach { line =>
      val split = line.split(" ")
      image.setBackground(split(0).toDouble)
      image.setBackgroundRMS(split(1).toDouble)
    }
    bufferedSource.close

    //read the psfex output
    //psfex is a modified version that generates a file with the Moffat psf
    val psfOutput = s"$ASTROMATIC_PATH/$imageUniqueID.moffat.fwhm"
    bufferedSource = Source.fromFile(psfOutput)
    lineSeq = bufferedSource.getLines.toArray
    if (lineSeq.isEmpty) warning(s"Image: '${image.getRawName()}' Error getting pfsex average. Commmand: '$command'")
    lineSeq.foreach { line =>
      val s = line.replaceAll("</TD>", "").split("<TD>")
      if (!s.isEmpty) {
        val fwhmPsf = s(2).trim.toDouble //take the Moffat averge
        image.setFwhmAverage(fwhmPsf, "psfex", verbose)
      }
    }
    bufferedSource.close

    //delete the files generated
    Util.runShellSimple(s"rm $ASTROMATIC_PATH/$imageUniqueID.*")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file SextractorBackgroundEstimator.scala
//=============================================================================