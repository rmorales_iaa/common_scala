/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  18/Aug/2020
 * Time:  13h:30m
 * Description: None
 */
//=============================================================================
package com.common.image.estimator.background
//=============================================================================
import com.common.dataType.pixelDataType.PixelDataType.PIXEL_DATA_TYPE
import com.common.stat.{SimpleStatDescriptive, StatDescriptive}
//=============================================================================
//=============================================================================
object Simple {
  //---------------------------------------------------------------------------
  //https://photutils.readthedocs.io/en/stable/background.html
  //https://photutils.readthedocs.io/en/stable/api/photutils.background.SExtractorBackground.html#photutils.background.SExtractorBackground
  //sigmaClipping = (iteration,sigmaScale)
  def estimateBackgroundAndRms(data: Array[PIXEL_DATA_TYPE]) = {
    val stats = StatDescriptive.getWithDouble(StatDescriptive.sigmaClippingMedianDouble(data))
    estimateBackgroundAndRmsWithStats(stats)
  }
  //---------------------------------------------------------------------------
  def estimateBackgroundAndRmsWithStats(stats: SimpleStatDescriptive) = {
    val backGround =
      if (((stats.average - stats.median) / stats.stdDev) > 0.3) stats.median
      else (2.5 * stats.median) - (1.5 * stats.average)
    (backGround, stats.rms)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file StandardRms.scala
//=============================================================================
