/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  25/Jan/2021
 * Time:  11h:31m
 * Description: None
 */
//=============================================================================
package com.common.image.estimator.background
//=============================================================================
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.myImage.MyImage
import com.common.image.region.Region
//=============================================================================
//=============================================================================
object VoronoiArea {
  //---------------------------------------------------------------------------
  def estimatedBackground(img: MyImage, region: Region, segment: Segment2D) = {}

  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file VoronoiArea.scala
//=============================================================================
