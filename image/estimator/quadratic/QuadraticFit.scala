/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Dec/2021
 * Time:  09h:52m
 * Description: None
 */
//=============================================================================
package com.common.estimator.quadratic
//=============================================================================
import org.apache.commons.math3.analysis.{MultivariateMatrixFunction, MultivariateVectorFunction}
import org.apache.commons.math3.fitting.leastsquares.{LeastSquaresBuilder, LevenbergMarquardtOptimizer}
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
//https://commons.apache.org/proper/commons-math/userguide/optimization.html
//Function: f(x) = ax2 + bx + c = (ax + b) * x + c
//It optimizes f(x) using a set of samples to list the parameters: u,b,c  that minimizes the error of that samples
//---------------------------------------------------------------------------
case class QuadraticFit(maxEvaluations: Int = 1000, maxIterations: Int = 100){
  //-------------------------------------------------------------------------
  private val xSamplePointSeq = ArrayBuffer[Double]()
  private val ySamplePointSeq = ArrayBuffer[Double]()
  //-------------------------------------------------------------------------
  def addSamplePoint(x: Double, y: Double) : Unit = {
    xSamplePointSeq += x
    ySamplePointSeq += y
  }
  //-------------------------------------------------------------------------
  //evaluate the function using the sample sequence with the NEW parameters
  def function: MultivariateVectorFunction = new MultivariateVectorFunction() {
    override def value(parameterSeq: Array[Double]) : Array[Double] = {
      val a = parameterSeq(0)
      val b = parameterSeq(1)
      val c = parameterSeq(2)
      (xSamplePointSeq map { x=> (a * x + b) * x + c}).toArray  //f(x) = ax2 + bx + c = (ax + b) * x + c
    }
  }
  //-------------------------------------------------------------------------
  //evaluate the jacobian of the function using the sample sequence and the NEW parameters
  def jacobian: MultivariateMatrixFunction = new MultivariateMatrixFunction() {
    override def value(parameterSeq: Array[Double]): Array[Array[Double]] = {
      (xSamplePointSeq map { x=>
        Array(
          x * x             // d(ax2 + bx + c)/da = x * x
          , x                // d(ax2 + bx + c)/imageDB = x
          , 1)               // d(ax2 + bx + c)/imageDB = 1
      }).toArray
    }
  }
  //-------------------------------------------------------------------------
  //weight = 1 for all xSamplePointSeq
  def getWeigthSeq() = Array.fill[Double](xSamplePointSeq.length)(1d)
  //-------------------------------------------------------------------------
  def getYSamplePointSeq() = ySamplePointSeq.toArray
  //-------------------------------------------------------------------------
  def getInitialParameterGuess = Array(
      1d  //u
    , 1d  //b
    , 1d  //c
  )
  //-------------------------------------------------------------------------
  def solve() = {
    try {
      val lsb = new LeastSquaresBuilder()
        .model(function, jacobian)
        .target(getYSamplePointSeq)
        .start(getInitialParameterGuess)
        .maxEvaluations(maxEvaluations) //set upper limit of evaluation time
        .maxIterations(maxIterations)   //set upper limit of iteration time
      val optimizer = new LevenbergMarquardtOptimizer().optimize(lsb.build)
      val optimalValues = optimizer.getPoint.toArray
      println("A: " + optimalValues(0))
      println("B: " + optimalValues(1))
      println("C: " + optimalValues(2))
      Some(optimizer.getPoint.toArray)
    }
    catch {
      case _: Exception =>  None
    }
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file QuadraticFit.scala
//=============================================================================
