/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  14/Apr/2021
 * Time:  20h:15m
 * Description: Image mask used when detecting sources
 */
//=============================================================================
package com.common.image.mask
//=============================================================================
import com.common.configuration.{MyConfTrait, MyConfWithDefault}
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.simpleFits.SimpleFits.KEY_OM_TELESCOPE
import com.common.geometry.circle.CircleDouble
import com.common.geometry.point.{Point2D, Point2D_Double}
import com.common.geometry.rectangle.Rectangle
import com.common.image.telescope.Telescope
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object Mask {
  //---------------------------------------------------------------------------
  //mask type
  private final val CONF_KEY_MASK            = "SourceDetection.image.mask"
  private final val MASK_CIRCLE              = "circle"
  private final val MASK_RECTANGLE           = "rectangle"
  private final val VALUEA_AS_PERCENTAGE_KEY = "valuesArPercentages"
  private final val POSITIVE_AREA            = "positiveArea"
  //---------------------------------------------------------------------------
  private val confMap = scala.collection.mutable.Map[String, MyConfWithDefault]()
  //---------------------------------------------------------------------------
  private def parseRectangle(conf: MyConfTrait
                             , parentConfigKey : String
                             , dimension: Point2D) : Array[Mask] = {
    if (!conf.existKey(parentConfigKey)) return Array[Mask]()
    val valueAsPercentage = conf.getIntWithDefaultValue(s"$parentConfigKey.$VALUEA_AS_PERCENTAGE_KEY",0) == 1

    var left = conf.getIntWithDefaultValue(s"$parentConfigKey.$MASK_RECTANGLE.xLeftBorderPix", 0)
    var bottom = conf.getIntWithDefaultValue(s"$parentConfigKey.$MASK_RECTANGLE.yBottomBorderPix", 0)
    var right = conf.getIntWithDefaultValue(s"$parentConfigKey.$MASK_RECTANGLE.xRightBorderPix", 0)
    var top = conf.getIntWithDefaultValue(s"$parentConfigKey.$MASK_RECTANGLE.yTopBorderPix", 0)
    val positiveArea = conf.getIntWithDefaultValue(s"$parentConfigKey.$MASK_RECTANGLE.$POSITIVE_AREA", 1) == 1

    if (valueAsPercentage) {
      left   = Math.round(dimension.x * (left / 100d)).toInt
      bottom = Math.round(dimension.y * (bottom / 100d)).toInt
      right  = dimension.x - Math.round(dimension.x * (right / 100d)).toInt
      top    = dimension.y - Math.round(dimension.y * (top / 100d)).toInt
    }
    else {
      right = dimension.x - right
      top = dimension.y - top
    }
    Array(MaskRentangle(
        left
      , bottom
      , right
      , top
      , dimension
      , positiveArea))
  }
  //---------------------------------------------------------------------------
  private def parseCircle(conf: MyConfTrait, parentConfigKey : String
                          , dimension: Point2D) : Array[Mask] = {
    //------------------------------------------------------------------------
    if (!conf.existKey(parentConfigKey)) return Array[Mask]()
    //------------------------------------------------------------------------
    def getSingleMask(key: String) : Option[Mask] =  {
      val discriminant = Discriminant(conf, key, dimension)
      val positiveArea = conf.getIntWithDefaultValue(s"$parentConfigKey.$POSITIVE_AREA", 1) == 1
      if (discriminant.isInstanceOf[DiscriminantNone]) None
      else {
        val radius = conf.getDoubleWithDefaultValue(s"$key.$MASK_CIRCLE.radius", 1d)
        Some(MaskCircle(
              CircleDouble(Point2D_Double(dimension) / 2
                           , radius)
                           , dimension
                           , positiveArea))
      }
    }
    //------------------------------------------------------------------------
    var currentCircle = 1
    val r = ArrayBuffer[Mask]()
    var key = s"$parentConfigKey.$MASK_CIRCLE.c_$currentCircle"

    while(conf.existKey(key)) {
      val newMask = getSingleMask(key)
      if (newMask.isDefined) r += newMask.get
      currentCircle += 1
      key = s"$parentConfigKey.$MASK_CIRCLE.c_$currentCircle"
    }
    r.toArray
  }
  //---------------------------------------------------------------------------
  def get(fits: SimpleFits) : Array[Mask] = {
    if (fits == null) return Array(MaskNone())
    val fitsTelescopeName = fits.getStringValueOrEmpty(KEY_OM_TELESCOPE)
    val imageDimension = fits.getImageDimension()
    if (fitsTelescopeName.isEmpty) return Array(MaskNone())
    val telescopeName = Telescope.getNormalizedTelescopeName(fitsTelescopeName)
    val telescopeConf =
      if (confMap.contains(telescopeName)) confMap(telescopeName).asInstanceOf[MyConfWithDefault]
      else {
        val conf = Telescope.getConfigurationFile(telescopeName).asInstanceOf[MyConfWithDefault]
        confMap(telescopeName) = conf
        conf
    }
    if (!telescopeConf.existKey(CONF_KEY_MASK)) return Array(MaskNone())
    parseMask(telescopeConf, CONF_KEY_MASK, Point2D(imageDimension))
  }
  //---------------------------------------------------------------------------
  def parseMask(conf: MyConfTrait
                , parentConfigKey: String
                , imageDimension: Point2D) : Array[Mask] = {
    val r = parseRectangle(conf, s"$parentConfigKey", imageDimension) ++
               parseCircle(conf, s"$parentConfigKey", imageDimension)
    if (r.isEmpty) Array(MaskNone()) else r
  }
  //---------------------------------------------------------------------------
  def isInForbiddenArea(maskSeq : Array[Mask], x: Double, y: Double) : Boolean = {//is in any forbiden areas?
    maskSeq.foreach { mask=>
      if (mask.isInForbiddenArea(Point2D_Double(x,y))) return true }
    false
  }

  //---------------------------------------------------------------------------
  def isInForbiddenArea(maskSeq: Array[Mask], pos: Point2D): Boolean = { //is in any forbiden areas?
    val posDouble = Point2D_Double(pos)
    maskSeq.foreach { mask =>
      if (mask.isInForbiddenArea(posDouble)) return true
    }
    false
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//A mask is defined as forbiden area. Thi area is defined using a geometric figure and
// a flag ('positiveArea') that indicates whether the area delimited of the figure is forbiden or not
//In other words, the figure describe the forbiden using its own area or its inverse
//When 'positiveArea' the area described by the figure indicated the valid zone allowed, so the forbiden area id the inverse
//When it is false, the area described by the figure and the forbidden area is the same
trait Mask {
  //---------------------------------------------------------------------------
  val imageDimension: Point2D
  val positiveArea: Boolean
  //---------------------------------------------------------------------------
  def isInForbiddenArea(pos: Point2D_Double) : Boolean
  //---------------------------------------------------------------------------
  def toString() : String
  //---------------------------------------------------------------------------
  protected def getCommonString() = s" positive area: '$positiveArea' apply when image dimension is:" + imageDimension
  //---------------------------------------------------------------------------
}
//=============================================================================
case class MaskNone() extends Mask {
  val imageDimension  = Point2D.POINT_ZERO
  val positiveArea = false
  //---------------------------------------------------------------------------
  def isInForbiddenArea(pos: Point2D_Double)  = false
  //---------------------------------------------------------------------------
  override def toString() = "MaskNone"
  //---------------------------------------------------------------------------
}
//=============================================================================
case class MaskCircle(circle: CircleDouble
                      , imageDimension: Point2D
                      , positiveArea: Boolean
                     ) extends Mask {
  //---------------------------------------------------------------------------
  def isInForbiddenArea(pos: Point2D_Double) = {
    val in = circle.isIn(pos)
    if (positiveArea) !in
    else in
  }
  //---------------------------------------------------------------------------
  override def toString() = "MaskCircle: " + circle.toString + getCommonString()
  //---------------------------------------------------------------------------
}
//=============================================================================
case class MaskRentangle(xLeftBorderPix : Int
                         , yBottomBorderPix: Int
                         , xRightBorderPix : Int
                         , yTopBorderPix: Int
                         , imageDimension: Point2D
                         , positiveArea: Boolean) extends Mask {
  //---------------------------------------------------------------------------
  val rec = Rectangle(Point2D(xLeftBorderPix,yBottomBorderPix)
                    , Point2D(xRightBorderPix,yTopBorderPix))
  //---------------------------------------------------------------------------
  def isInForbiddenArea(pos: Point2D_Double) = {
    val in = rec.isIn(pos.toPoint2D())
    if (positiveArea) !in
    else in
  }
  //---------------------------------------------------------------------------
  override def toString() = "MaskRentangle: " + s"LeftBottom($xLeftBorderPix,$yBottomBorderPix) RightTop($xRightBorderPix,$yTopBorderPix)" + getCommonString()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Mask.scala
//=============================================================================
