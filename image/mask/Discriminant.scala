/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  27/May/2021
 * Time:  09h:01m
 * Description: None
 */
//=============================================================================
package com.common.image.mask
//=============================================================================
import com.common.configuration.{MyConfTrait}
import com.common.geometry.point.Point2D
import com.common.logger.MyLogger
//=============================================================================
trait Discriminant {
  //---------------------------------------------------------------------------
  def pass(disc: String) : Boolean
  //---------------------------------------------------------------------------
  def toString() : String
  //---------------------------------------------------------------------------
}
//=============================================================================
object Discriminant extends MyLogger{
  //---------------------------------------------------------------------------
  //discriminantByDimension
  private final val DISCRIMINAT_BY_DIMENSION = "activateWhenDimension"
  //---------------------------------------------------------------------------
  def apply(conf: MyConfTrait, parentConfigKey : String, dimension: Point2D) : Discriminant = {
    val discriminantByDimension = conf.getIfExistKey(s"$parentConfigKey.$DISCRIMINAT_BY_DIMENSION")
    if (discriminantByDimension.isEmpty)  return DiscriminantPassAll()
    val expectedDimension = dimension.x + "X" + dimension.y
    if (discriminantByDimension.get.toLowerCase == expectedDimension.toLowerCase) {
      info(s"Found a valid mask discriminant: '${discriminantByDimension.get}'")
      DiscriminantByDimension(dimension)
    }
    else
      DiscriminantNone()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class DiscriminantNone() extends Discriminant {
  //---------------------------------------------------------------------------
  def pass(disc: String) = true
  //---------------------------------------------------------------------------
  override def toString() = "DiscriminantNone"
  //---------------------------------------------------------------------------
}
//=============================================================================
case class DiscriminantPassAll() extends Discriminant {
  //---------------------------------------------------------------------------
  def pass(disc: String) = true
  //---------------------------------------------------------------------------
  override def toString() = "DiscriminantPassAll"
  //---------------------------------------------------------------------------
}
//=============================================================================
case class DiscriminantByDimension(imageDimension: Point2D) extends Discriminant {
  //---------------------------------------------------------------------------
  def pass(disc: String) = disc == (imageDimension.x + "X" + imageDimension.y)
  //---------------------------------------------------------------------------
  override def toString() = s"DiscriminantByDimension: ${imageDimension.x + "X" + imageDimension.y}"
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Discriminant.scala
//=============================================================================
