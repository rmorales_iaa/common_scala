/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  15/Jul/2024
 * Time:  10h:54m
 * Description: None
 */
package com.common.image.mpo
//=============================================================================
import com.common.configuration.MyConf
import com.common.coordinate.conversion.Conversion
import com.common.image.estimator.centroid.Centroid
import com.common.image.focusType.ImageFocusMPO
import com.common.image.mpo.asteroidChecker.AsteroidChecker
import com.common.image.myImage.MyImage
import com.common.jpl.Spice
import com.common.logger.MyLogger
//=============================================================================
//=============================================================================
object MpoLocation extends MyLogger {
  //---------------------------------------------------------------------------
  def locate(imageName: String
             , mpo: Option[ImageFocusMPO]
             , printReport: Boolean = true
             , initSpiceLibrary: Boolean = true)  = {

    if (initSpiceLibrary) Spice.init(verbose = false)
    val img = MyImage(imageName)
    val r = if (mpo.isDefined) locateKnownTno(img, mpo.get)
            else locateAllKnownMposInBlindMode(img, printReport)
    if (initSpiceLibrary) Spice.close()
    r
  }
  //---------------------------------------------------------------------------
  private def locateKnownTno(img: MyImage
                             , mpo: ImageFocusMPO) = {
    mpo.getEstPosition(img, splitInLinesDebugInfo = true, verbose = true)
    Array[ImageFocusMPO]()
  }
  //---------------------------------------------------------------------------
  private def locateAllKnownMposInBlindMode(img: MyImage
                                            , printReport: Boolean = true) = {

    //find all sources
    val (_, _, noiseTide, sourceMinPixCount, sourceMaxPixCount) = img.getSourceDetectionFromConfiguration()
    val sourceSeq = img.findSourceSeq(noiseTide
      , sourceSizeRestriction = Some(sourceMinPixCount, sourceMaxPixCount))
      .toSegment2D_seq()

    Centroid.calculateCentroiWithFallBack(
      sourceSeq
      , img
      , MyConf.c.getStringSeq("SourceCentroid.fallbackAlgorithmSeq")
      , verbose = false)

    img.setCentroidRaDec(sourceSeq)
    val mpoSeq = AsteroidChecker.query(img, sourceSeq, initSpiceLibrary = false)

    if (printReport) {
      info(s"Found:'${mpoSeq.size}' know MPO's at image:'${img.getRawName()}'")
      mpoSeq.zipWithIndex.foreach { case (mpo, i) =>
        val source = mpo._2.source
        val centroidRaDec = source.getCentroidRaDec()
        val ra_hms = Conversion.DD_to_HMS_WithDivider(centroidRaDec.x, divider = ":")
        val dec_dms = Conversion.DD_to_DMS_WithDivider(centroidRaDec.y, divider = ":")
        println(s"--- MPO ${i + 1}/${mpoSeq.size} starts ---")
        println(s"\t Spice kernel in db :'${mpo._2.storedInDB}'")
        println(s"\t mpo                :'${mpo._2.composedName}'")
        println(s"\t mpc ID             :'${mpo._2.spiceSpk.mpcID}'")
        println(s"\t spk ID             :'${mpo._2.spiceSpk.spkID}'")
        println(s"\t main designation   :'${mpo._2.spiceSpk.mainDesignation}'")
        println(s"\t alt. designations  :'${mpo._2.spiceSpk.alternativeDesignation}'")
        println(s"\t orbit version      :'${mpo._2.spiceSpk.version}'")
        println(s"\t (ra,dec) dec deg   :${source.getCentroidRaDec()}")
        println(s"\t (ra,dec) (hms,dms) :($ra_hms,$dec_dms)")
        println(s"\t image pixel coord  :${source.getCentroid()}")
        println(s"--- MPO ${i + 1}/${mpoSeq.size}  ends  ---")
      }
    }
    (mpoSeq map (_._2)).toArray
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MpoLocation.scala
//=============================================================================