/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  15/May/2020
 * Time:  03h:22m
 * Description: https://www.minorplanetcenter.net/iau/info/OpticalObs.html    (Section MINOR PLANETS)
   Columns     Format   Use
   1 -  5        A5     Packed minor planet number
   6 - 12        A7     Packed provisional designation, or a temporary designation (https://minorplanetcenter.net/iau/info/PackedDes.html)
   13            A1     Discovery asterisk: new (or unidentified) objects should contain `*'
   14            A1     Note 1: standard codes (https://minorplanetcenter.net/iau/info/ObsNote.html)
   15            A1     Note 2: how the observation was made (https://minorplanetcenter.net//iau/info/OpticalObs.html)
   16 - 32              Date of observation. Format:   "YYYY MM DD.dddddd"
   33 - 44              Observed RA (J2000.0). Format: "HH MM SS.ddd"
   45 - 56              Observed Decl. (J2000.0). Format:  "DD MM SS.dd"
   57 - 65       9X     Must be blank
   66 - 71    F5.2,A1   Observed magnitude and band (or nuclear/total flag for comets)
   72 - 77       6X     Must be blank
   78 - 80       A3     Observatory code

          1        2         3         4         5         6         7         8
123456789|123456789|123456789|123456789|123456789|123456789|123456789|1234567890|
12345678901234567890123456789012345678901234567890123456789012345678901234567890
    Example1  C1997 10 13.74589 00 37 45.24 +03 53 36.5                       J9
candidate_0   C2019 05 04.02985117 42 50.219-25 30 35.98                     808
empty_search  C2019 05 04.02985117 42 52.219-25 31 35.98                     808
D6108         C2018 05 17.02384314 10 49.041+17 12 22.45                     493
m2_809        C2020 07 17.09560118 48 58.956-06 09 36.98                     J86
       D6472  C2020 03 05.23753413 09 35.948+23 41 27.01         17.52V      J86
//-----------------------------------------------------------------------------
Web search:
https://www.projectpluto.com/cgicheck.htm
https://minorplanetcenter.net//cgi-bin/checkmp.cgi
http://vo.imcce.fr/webservices/skybot/?forms
https://mpo.lowell.edu/astfinder/
 */
//=============================================================================
package com.common.image.mpo.asteroidChecker
//=============================================================================
import com.common.math.Base62
import com.common.util.string.MyString.{leftPadding, rightPadding}
import com.common.util.time.Time
//=============================================================================
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import scala.math.BigDecimal.RoundingMode
//=============================================================================
//=============================================================================
object MinorPlanetCenter {
  //-------------------------------------------------------------------------
  final val DISCOVERED_ASTERISK_UNIDENTIFIED             = "*"  //https://minorplanetcenter.net//iau/info/OpticalObs.html
  final val STANDARD_CODE_IMAGE_TRACKED_ON_OBJECT_MOTION = "m"  //see https://minorplanetcenter.net/iau/info/ObsNote.html
  final val OBSERVING_TYPE_CCD                           = "c"  //see https://minorplanetcenter.net//iau/info/OpticalObs.html
  //-------------------------------------------------------------------------
  private final val FRACTIONAL_DAY_DECIMAL_PLACES = 6
  private val base62 = Base62()
  //-------------------------------------------------------------------------
  //https://www.minorplanetcenter.net/iau/info/PackedDes.html#perm
  //https://en.wikipedia.org/wiki/Provisional_designation_in_astronomy#Packed_designation
  //Columns: 1 - 5
  private def getPacketNumberPermanent(mpoID: Long) = {
    if (mpoID < 100000) rightPadding(mpoID.toString, 5, "0")
    else if (mpoID > 619999) {
      val diff = mpoID - 620000
      "~" + rightPadding(base62.encode(diff), 4, "0")
    }
    else {
      val mod = mpoID % 10000
      val div = mpoID / 10000
      base62.encode(div) + rightPadding(mod.toString, 4, "0")
    }
  }
  //-------------------------------------------------------------------------
  //https://www.minorplanetcenter.net/iau/info/OpticalObs.html   (Section MINOR PLANETS)
  //"YYYY MM DD.dddddd", with the decimal day of observation normally being given to a precision of 0.00001 days.
  //So, it is a precision of: 864ms
  private def getObservingDateFormatted(s: String) = {
    val ldt = LocalDateTime.parse(s, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
    val year = ldt.getYear
    val month = leftPadding(ldt.getMonthValue.toString, 2, "0")
    val day = ldt.getDayOfMonth
    val hour = ldt.getHour.toDouble
    val minute = ldt.getMinute.toDouble
    val second = ldt.getSecond.toDouble + (ldt.getNano.toDouble / Time.NANO_SECONDS_IN_ONE_SECOND)
    val totalS =
      (hour * Time.SECONDS_IN_ONE_HOUR) +
        (minute * Time.SECONDS_IN_ONE_MINUTE) +
        second
    val dayString = if (day < 10) "0" + day.toString else day.toString
    val fractionDays = totalS / Time.SECONDS_IN_ONE_DAY
    val fractionDaysString = BigDecimal(fractionDays).setScale(FRACTIONAL_DAY_DECIMAL_PLACES, RoundingMode.HALF_UP)
      .toString
      .drop(1) //remove first zero

    year + " " +
      month + " " +
      dayString +
      fractionDaysString
  }
  //---------------------------------------------------------------------------
  private def getFormatedRA(ra:String) = {
    val seq = ra.split(" ")
    val h = leftPadding(seq(0),2,"0")
    val m = leftPadding(seq(1),2,"0")
    val rounded = BigDecimal(seq(2)).setScale(3, BigDecimal.RoundingMode.HALF_UP)
                 .toDouble
    val s = f"$rounded%06.3f"
    Seq(h,m,s).mkString(" ")
  }
  //---------------------------------------------------------------------------
  private def getFormatedDEC(dec: String) = {
    val seq = dec.split(" ")

    val sign = dec.head match {
      case '+' => "+"
      case '-' => "-"
      case _   => "+"
    }

    val d = if(dec.head == '+' || dec.head == '-')
      leftPadding(seq(0).drop(1), 2, "0") //avoid format sign
    else
      leftPadding(seq(0), 2, "0")

    val m = leftPadding(seq(1), 2, "0")
    val rounded = BigDecimal(seq(2)).setScale(2, BigDecimal.RoundingMode.HALF_UP)
      .toDouble
    val s = f"$rounded%05.2f"
    val fixPrefix = if (d.head == '+' || d.head == '-') "" else sign
    Seq(fixPrefix + d,m,s).mkString(" ")
  }
  //---------------------------------------------------------------------------
  def createQueryUnknownObjectSeq(
       discoveryNumber: Int = 0
     , programCode: String = STANDARD_CODE_IMAGE_TRACKED_ON_OBJECT_MOTION
     , observingType: String = OBSERVING_TYPE_CCD
     , observingDate: String
     , raDecMap: Map[Int,(String, String)] //ra,dec
     , observatoryCode: String
   ) = {

   raDecMap.map { case (sourceID,raDec) =>
      val provisionalDesignation = ProvisionalDesignation.getDesignation(LocalDateTime.parse(observingDate), discoveryNumber).get
     (sourceID , createQueryLine(
        provisionalDesignation = provisionalDesignation
        , programCode = programCode
        , observingType = observingType
        , observingDate = observingDate
        , ra = getFormatedRA(raDec._1)
        , dec = getFormatedDEC(raDec._2)
        , observatoryCode = observatoryCode
      ))
    }
  }
  //---------------------------------------------------------------------------
  def createQueryKnownObject(mpcID: String
                             , programCode: String = STANDARD_CODE_IMAGE_TRACKED_ON_OBJECT_MOTION
                             , observingType: String = OBSERVING_TYPE_CCD
                             , observingDate: String
                             , ra: String
                             , dec: String
                             , magnitudeAndBand: String = "".padTo(6, ' ')
                             , observatoryCode: String): String =
    createQueryLine(
        packedMinorPlanetNumber = PackedMPC_Number.encode(mpcID.toInt)
      , discoveryAsterisk = " "
      , programCode = programCode
      , observingType = observingType
      , observingDate  = observingDate
      , ra = ra
      , dec = dec
      , magnitudeAndBand = magnitudeAndBand
      , observatoryCode = observatoryCode
    )
  //---------------------------------------------------------------------------
  private def createQueryLine(packedMinorPlanetNumber: String = "".padTo(5, '0')
                              , provisionalDesignation: String= "".padTo(7, ' ')
                              , discoveryAsterisk: String = DISCOVERED_ASTERISK_UNIDENTIFIED
                              , programCode: String = STANDARD_CODE_IMAGE_TRACKED_ON_OBJECT_MOTION
                              , observingType: String = OBSERVING_TYPE_CCD
                              , observingDate: String
                              , ra: String
                              , dec: String
                              , magnitudeAndBand: String = "".padTo(6, ' ')
                              , observatoryCode: String): String = {
    packedMinorPlanetNumber +                  //1-5  Packed minor planet number
    provisionalDesignation +                   //6-12  Packed provisional designation
    discoveryAsterisk +                        //13 Discovery asterisk
    programCode +                              //14 program code that produces the observing
    observingType +                            //15 source of the observing
    getObservingDateFormatted(observingDate) + //16-32 date of observation
    getFormatedRA(ra) +                        //33-44 observed right ascension(J2000.0)
    getFormatedDEC(dec) +                      //45-56 observed declination (J2000.0)
    "".padTo(9, ' ') +                         //57-65 blank
    magnitudeAndBand +                         //66-71 observed magnitude and band (or nuclear/total flag for comets)
    "".padTo(6, ' ') +                         //72-77 blank
    f"$observatoryCode%3s"                     //78-80 observatory code
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MinorPlanetCenter.scala
//=============================================================================
