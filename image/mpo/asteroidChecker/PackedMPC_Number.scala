/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  22/Jul/2024
 * Time:  14h:33m
 * Description: None
 */
package com.common.image.mpo.asteroidChecker
//=============================================================================
import com.common.math.Base62
//=============================================================================
//=============================================================================
object PackedMPC_Number {
  //---------------------------------------------------------------------------
  private val base62 = Base62()
  //---------------------------------------------------------------------------
  def encode(mpcID: Int): String = {
    if (mpcID < 100000)  // Zero-padded to five digits
      f"$mpcID%05d"
    else
      if (mpcID < 620000) {
      // Encode the number in base-62
      // Calculate the number and remainder
      val division = mpcID / 10000
      val remainder = mpcID % 10000

      // Convert division part to base-62 letter
      val divisionChar = if (division < 10) {
        (division + '0').toChar
      } else if (division < 36) {
        ('A' + (division - 10)).toChar
      } else {
        ('a' + (division - 36)).toChar
      }

      // Zero-padded remainder part to four digits
      val remainderStr = f"$remainder%04d"

      s"$divisionChar$remainderStr"
    } else {
      val value = mpcID - 620000
      val base62String = base62.encode(value).padTo(4, '0') // Ensure it is 4 characters long
      s"~$base62String"
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file PackedMPC_Number.scala
//=============================================================================