/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  24/Jul/2024
 * Time:  12h:21m
 * Description: None
 */
package com.common.image.mpo.asteroidChecker
//=============================================================================
import com.common.coordinate.conversion.Conversion
import com.common.geometry.point.Point2D_Double
import com.common.geometry.segment.segment2D.Segment2D
//=============================================================================
//=============================================================================
object AsteroidCheckerObject {
  //---------------------------------------------------------------------------
  def apply(line: String
            , source: Segment2D): AsteroidCheckerObject = {
    val mpcID = line.trim.take(20).trim.split(" ").head.replaceAll("-","")
    val seq = line.drop(20).trim.replaceAll(" +", " ").split(" ")
    val raDistArcSec = seq(0).toDouble
    val decDistArcSec = seq(1).toDouble
    val distArcSec = seq(2).toDouble
    val magnitude = seq(3).toDouble
    val motionArcSecPerHour = seq(4).toDouble

    AsteroidCheckerObject(
        mpcID
      , raDistArcSec
      , decDistArcSec
      , distArcSec
      , magnitude
      , motionArcSecPerHour
      , source)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class AsteroidCheckerObject(mpcID: String
                                 , raDistArcSec: Double
                                 , decDistArcSec: Double
                                 , distArcSec: Double
                                 , magnitude: Double
                                 , motionArcSecPerHour: Double
                                 , source: Segment2D) {
  //---------------------------------------------------------------------------
  val estRaDecPos = getEstRaDec()
  //---------------------------------------------------------------------------
  private def getEstRaDec() = {
    val centroidRaDec = source.getCentroidRaDec()
    Point2D_Double(Conversion.addToRa(centroidRaDec.x, raDistArcSec)
                   , Conversion.addToDec(centroidRaDec.y, decDistArcSec))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file AsteroidCheckerObject.scala
//=============================================================================