/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  22/Jul/2024
 * Time:  13h:24m
 * Description: Implementation of provisional designation described at:
 *   https://minorplanetcenter.net/iau/info/DesDoc.html
 */
package com.common.image.mpo.asteroidChecker
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
import java.time.LocalDateTime
//=============================================================================
//=============================================================================
object ProvisionalDesignation extends MyLogger{
  //---------------------------------------------------------------------------
  case class MonthCoding(month:Int, startDay:Int, endDay: Int)  //start and end day included
  //---------------------------------------------------------------------------
  private final val HALF_MONTH_CODING = Map(
      "A" -> MonthCoding(1, 1, 15)
    , "B" -> MonthCoding(1, 16, 31)
    , "C" -> MonthCoding(2, 1, 15)
    , "D" -> MonthCoding(2, 16, 29)
    , "E" -> MonthCoding(3, 1, 15)
    , "F" -> MonthCoding(3, 16, 31)
    , "G" -> MonthCoding(4, 1, 15)
    , "H" -> MonthCoding(4, 16, 30)
    , "J" -> MonthCoding(5, 1, 15)
    , "K" -> MonthCoding(5, 16, 31)
    , "L" -> MonthCoding(6, 1, 15)
    , "M" -> MonthCoding(6, 16, 30)
    , "N" -> MonthCoding(7, 1, 15)
    , "O" -> MonthCoding(7, 16, 31)
    , "P" -> MonthCoding(8, 1, 15)
    , "Q" -> MonthCoding(8, 16, 31)
    , "R" -> MonthCoding(9, 1, 15)
    , "S" -> MonthCoding(9, 16, 30)
    , "T" -> MonthCoding(10, 1, 15)
    , "U" -> MonthCoding(10, 16, 31)
    , "V" -> MonthCoding(11, 1, 15)
    , "W" -> MonthCoding(11, 16, 30)
    , "X" -> MonthCoding(12, 1, 15)
    , "Y" -> MonthCoding(12, 16, 31)
  )
  //---------------------------------------------------------------------------
  private final val DISCOVER_ORDER = Map(
      "A" -> 1
    , "B" -> 2
    , "C" -> 3
    , "D" -> 4
    , "E" -> 5
    , "F" -> 6
    , "G" -> 7
    , "H" -> 8
    , "J" -> 9
    , "K" -> 10
    , "L" -> 11
    , "M" -> 12
    , "N" -> 13
    , "O" -> 14
    , "P" -> 15
    , "Q" -> 16
    , "R" -> 17
    , "S" -> 17
    , "T" -> 19
    , "U" -> 20
    , "V" -> 21
    , "W" -> 22
    , "X" -> 23
    , "Y" -> 24
    , "Z" -> 25
  )
  //---------------------------------------------------------------------------
  def getDesignation(date: LocalDateTime
                     , discoveryNumber: Int): Option[String] = {
    val year = date.getYear
    val month = date.getMonthValue
    val day = date.getDayOfMonth

    // Find the corresponding half-month code
    val halfMonthCode = HALF_MONTH_CODING.find { case (_, coding) =>
      coding.month == month && day >= coding.startDay && day <= coding.endDay
    }.map(_._1).getOrElse{error(s"Invalid date:'${date.toString}'");return None}

    // Determine the order letter
    val discoverNumberIndex = (discoveryNumber % 25) + 1
    val orderLetter = DISCOVER_ORDER.find { case (_, orderIndex) =>
      discoverNumberIndex == orderIndex
    }.map(_._1).getOrElse{error(s"Invalid discovery number:'$discoveryNumber'");return None}

    //get the cycle number
    val cycleNumber = (discoveryNumber - 1) / 25

    if (cycleNumber == 0) Some(s"$year $halfMonthCode$orderLetter")
    else Some(s"$year $halfMonthCode$orderLetter$cycleNumber")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ProvisionalDesignation.scala
//=============================================================================