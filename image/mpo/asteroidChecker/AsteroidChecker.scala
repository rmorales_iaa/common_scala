/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/May/2020
 * Time:  07h:38m
 * Description: Wrapper for astchecker
 *  https://github.com/Bill-Gray/lunar/blob/master/astcheck.cpp
 *  https://github.com/Bill-Gray/lunar
 * MPC format string
 *  https://minorplanetcenter.net//iau/info/OpticalObs.html
 * Web search:
 *   http://vo.imcce.fr/webservices/skybot/?forms    (Search objects)
 *   https://ssd.jpl.nasa.gov/horizons.cgi?s_target=1#top
 *   https://www.projectpluto.com/cgicheck.htm
 *   https://minorplanetcenter.net//cgi-bin/checkmp.cgi
 *   https://mpo.lowell.edu/astfinder/
 *
 * General info:
 *   https://www.projectpluto.com/tools.htm
 */
//=============================================================================
package com.common.image.mpo.asteroidChecker
//=============================================================================
//=============================================================================
import com.common.configuration.MyConf
import com.common.coordinate.conversion.Conversion
import com.common.geometry.segment.segment2D.Segment2D
import com.common.hardware.cpu.CPU
import com.common.image.focusType.ImageFocusMPO
import com.common.image.myImage.MyImage
import com.common.image.telescope.Telescope
import com.common.jpl.Spice
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
import com.common.util.util.Util
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import scala.collection.JavaConverters.mapAsScalaMapConverter
import scala.io.Source
import java.util.concurrent.ConcurrentHashMap
//=============================================================================
//=============================================================================
object AsteroidChecker extends MyLogger {
  //---------------------------------------------------------------------------
  //false positive: mpo checker
  //---------------------------------------------------------------------------
  private final val ASTEROID_CHECKER_QUERY_DIR                      = Path.ensureEndWithFileSeparator(MyConf.c.getString("AsteroidChecker.queryDir"))
  private final val ASTEROID_CHECKER_SCRIPT_DIR                     = Path.ensureEndWithFileSeparator(MyConf.c.getString("AsteroidChecker.scriptDir"))
  private final val ASTEROID_CHECKER_SCRIPT                         = MyConf.c.getString("AsteroidChecker.script")
  private final val ASTEROID_CHECKER_SOF_DIR                        = Path.ensureEndWithFileSeparator(ASTEROID_CHECKER_SCRIPT_DIR) + "sof/"
  private final val ASTEROID_CHECKER_QUERY_RADIUS_ARCOSEC           = MyConf.c.getFloat("AsteroidChecker.queryRadius")
  private final val ASTEROID_CHECKER_MAX_ALLOWED_ARCOSEC_DISTANCE   = MyConf.c.getDouble("AsteroidChecker.maxAllowedImageSourceMatchArcoSecDistance")
  //---------------------------------------------------------------------------
  Path.ensureDirectoryExist(ASTEROID_CHECKER_QUERY_DIR)
  private val queryDir = Path.ensureEndWithFileSeparator(ASTEROID_CHECKER_QUERY_DIR)
  //---------------------------------------------------------------------------
  private def createQueryFile(queryFileName: String, content: String) = {
    val bw = new BufferedWriter(new FileWriter(new File(queryFileName)))
    bw.write(content)
    bw.close()
  }
  //---------------------------------------------------------------------------
  private def parseAnswer(img: MyImage
                          , resultFileName: String
                          , sourceMap : Map[Int,Segment2D]
                          , mpoResultMap:ConcurrentHashMap[Int,(ImageFocusMPO,Double)]): Unit = {
    //-------------------------------------------------------------------------
    Source.fromFile(resultFileName).getLines.zipWithIndex.foreach { case (line,i) =>
      if (i > 5) { //results start at line 5
        if (line.startsWith("The apparent motion ")) return
        //no result
        val source = sourceMap(Path.getOnlyFilenameNoExtension(resultFileName).toInt)
        val asteroidCheckerObject = AsteroidCheckerObject(line, source)
        val mpo = ImageFocusMPO.build(asteroidCheckerObject.mpcID
                                      , source
                                      , allowSPK_NotStoredInDB = true).getOrElse(return)
        val estRaDecPos =
          if (mpo.storedInDB) {
            val r = mpo.getEstPosition(img, verbose = false)
            if (!r.isDefined) asteroidCheckerObject.estRaDecPos
            else r.get._1
          }
          else asteroidCheckerObject.estRaDecPos

        val distArcoSec = (estRaDecPos.getResidualMas(source.getCentroidRaDec()) / 1000).getDistance()

        if (distArcoSec < ASTEROID_CHECKER_MAX_ALLOWED_ARCOSEC_DISTANCE) {
          //store the for the same MPO, the closest source
          val isStored = mpoResultMap.contains(source.id)
          if (!isStored ||
               (isStored &&
               mpoResultMap.get(source.id)._2 < distArcoSec))
            mpoResultMap.put(source.id, (mpo,distArcoSec))
        }
      }
    }
  }
  //---------------------------------------------------------------------------
  private def getSofFileName(timeStamp: String): String = {
    val t = LocalDateTime.parse(timeStamp,DateTimeFormatter.ISO_LOCAL_DATE_TIME)
    val year = t.getYear
    val month = "%02d".format(t.getMonthValue)

    t.getDayOfMonth match {
      case day if day <= (1 + 7) => year + month + "01.sof"
      case day if day <= (15 + 7) => year + month + "15.sof"
      case _ =>  //take the closest sof on next month
        val newT = t.plusMonths(1)
        newT.getYear + "%02d".format(newT.getMonthValue) + "01.sof"
    }
  }
  //---------------------------------------------------------------------------
  private class ProcessMPC_Seq(img: MyImage
                               , sourceMap: Map[Int,Segment2D]
                               , pairSeq: Array[(Int,String)]
                               , msofFile: String
                               , outDir: String
                               , mpoResultMap:ConcurrentHashMap[Int,(ImageFocusMPO,Double)])
    extends ParallelTask[(Int,String)](
    pairSeq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100
    , message = Some("finding known MPO's in the image")) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(pair: (Int,String)) = {
      val sourceID = pair._1
      val mpcQuery = pair._2
      val absoluteQueryPath = s"${Path.getCurrentPath}/$outDir/"
      val inputQueryFileName =  absoluteQueryPath + s"$sourceID.mpc"
      val outputAnswerFileName = absoluteQueryPath + s"$sourceID.result"

      createQueryFile(inputQueryFileName,mpcQuery)

      Util.runShellWaitUntilTerminate(ASTEROID_CHECKER_SCRIPT_DIR + ASTEROID_CHECKER_SCRIPT
                                      , Seq(s"$ASTEROID_CHECKER_SCRIPT_DIR $inputQueryFileName $ASTEROID_CHECKER_QUERY_RADIUS_ARCOSEC $msofFile $outputAnswerFileName"))

      //parseInput result
      parseAnswer(img
                   , outputAnswerFileName
                   , sourceMap
                   , mpoResultMap)
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def query(img: MyImage
            , _seg2D_seq : Array[Segment2D]
            , initSpiceLibrary: Boolean = true): Map[Int,ImageFocusMPO] = {
    info(s"The image '${img.getRawName()}' finding all known MPOs using astrochecker (blind mode)")
    val fits = img.getSimpleFits()
    val timeStamp = img.getObservingDateMidPoint().toString
    if (timeStamp.isEmpty) {
      warning(s"The image '${img.getRawName()}' has not the record key 'KEY_OM_DATEOBS'. Can not run the mpo checker")
      return Map[Int,ImageFocusMPO]()
    }

    //filter NaN pos
    val notNaNCentroidPos = _seg2D_seq
      .zipWithIndex.flatMap { case (p,i) =>
      if (p.getCentroid().oneComponentIsNan() ||
          p.getCentroidRaDec().oneComponentIsNan()) None
      else Some(i)
    }
    val seg2D_seq = notNaNCentroidPos.map{i=> _seg2D_seq(i)}
    if(seg2D_seq.length == 0) {
      warning("Error in AsteroidChecker:check. The number of valid centroids not NaN to calculate is zero")
      return Map[Int,ImageFocusMPO]()
    }
    //preparing call to astrochecker
    val raDecMap = seg2D_seq.map { source =>
      val t = img.pixToSky(source.getCentroid())
      val ra = Conversion.DD_to_HMS_WithDivider(t.x)
      val dec = Conversion.DD_to_DMS_WithDivider(t.y, threeDecimalPlaces = false)
      source.id -> ((ra, dec))
    }.toMap

    val msofFile = ASTEROID_CHECKER_SOF_DIR + getSofFileName(timeStamp)
    if (!MyFile.fileExist(msofFile)) {
      error(s"AsteroidChecker msof file does not exist:'$msofFile'")
      return Map[Int,ImageFocusMPO]()
    }
    val queryMap = MinorPlanetCenter.createQueryUnknownObjectSeq(
        raDecMap = raDecMap
      , observingDate  = timeStamp
      , observatoryCode = Telescope.getOfficialCode(fits)
    )

    val outDir = Path.resetDirectory(s"$queryDir/${img.getRawName()}")
    val mpoResultMap = new ConcurrentHashMap[Int,(ImageFocusMPO,Double)]()
    val sourceMap = (seg2D_seq map { source => (source.id, source) }).toMap

    if (initSpiceLibrary) Spice.init()
    new ProcessMPC_Seq(img
                       , sourceMap
                       , (queryMap map (t=> (t._1,t._2))).toArray
                       , msofFile
                       , outDir
                       , mpoResultMap)
    if (initSpiceLibrary) Spice.close()

    Path.deleteDirectoryIfExist(outDir)
    mpoResultMap.asScala.toMap.map { case (k,v) =>
      k->v._1
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file AsteroidChecker.scala
//=============================================================================
