/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  25/Mar/2024
 * Time:  17h:01m
 * Description: None
 */
package com.common.image.web
//=============================================================================
import com.common.database.mongoDB.spiceSPK.SpiceSPK_DB
import com.common.image.astrometry.query.AstrometryQuery
import com.common.image.focusType.ImageFocusMPO
import com.common.jpl.Spice
import com.common.logger.MyLogger
import com.common.util.path.Path
//=============================================================================
import java.io.{File, FileWriter}
import java.io.BufferedWriter
//=============================================================================
object QueryAstrometry extends MyLogger {
  //---------------------------------------------------------------------------
  def query(imageFocus: ImageFocusMPO
           , outputDir: String
           , linkImages: Boolean): Unit = {

    Path.ensureDirectoryExist(outputDir)
    val o = outputDir + "/"
    val resultFileName = s"$o/${AstrometryQuery.RESULT_IMAGE_FILENAME}"

    //generate the list of images without restriction
    Spice.init()

    val vr = QueryVR(imageFocus.composedName
      , ImageFocusMPO.build(imageFocus.spiceSpk.spkID.toString)
      , outputDir = "").query(simpleMode = true)

    val query = AstrometryQuery(
      Seq(imageFocus)
      , vrSeq = Seq(vr)
      , remoteHost = None
      , noImages = true
      , userStartDate = None
      , userEndDate = None
      , userTelescope = None
      , userFilter = None
      , userInstrument = None
      , userExposureTime = None
      , userImageDir = None
      , userOutputDir = Some(outputDir)
      , linkImages
    )
    Spice.close()

    //get kernel report
    val spkID = imageFocus.spiceSpk.spkID.toString
    val kernelReport = o + spkID +  ".info"
    val bwKernelReport = new BufferedWriter(new FileWriter(new File(kernelReport)))
    SpiceSPK_DB.writeReport(spkID, query.getImageCount, bwKernelReport)
    bwKernelReport.close()

    info(s"Generated file: '$kernelReport'")
    info(s"Generated file: '$resultFileName'")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file QueryAstrometry.scala
//=============================================================================