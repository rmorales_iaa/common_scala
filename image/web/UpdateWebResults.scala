/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  13/Mar/2024
 * Time:  13h:35m
 * Description: None
 */
package com.common.image.web
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.spiceSPK.SpiceSPK_DB
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.simpleFits.SimpleFits._
import com.common.geometry.point.Point2D_Double
import com.common.geometry.rectangle.RectangleDouble
import com.common.hardware.cpu.CPU
import com.common.image.astrometry.fov.Fov
import com.common.image.astrometry.query.AstrometryQuery
import com.common.estimator.skyPosition.EstimatedSkyPosition
import com.common.image.focusType.ImageFocusMPO
import com.common.image.focusType.ImageFocusTrait.getComposedObjectName
import com.common.image.myImage.MyImage
import com.common.image.telescope.Telescope
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
import com.common.util.time.Time
//=============================================================================
import java.time.LocalDateTime
import java.util.concurrent.{ConcurrentHashMap, ConcurrentLinkedQueue}
import scala.collection.JavaConverters.{collectionAsScalaIterableConverter, mapAsScalaConcurrentMapConverter}
import scala.collection.mutable.ArrayBuffer
import scala.sys.process._
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
object UpdateWebResults {
  //---------------------------------------------------------------------------
  private final val fitsExtension = MyConf.c.getStringSeq("Common.fitsFileExtension")
  //---------------------------------------------------------------------------
  private final val DEFAULT_TMP_DIRECTORY               = "output/tmp/web_update_results/"
  //---------------------------------------------------------------------------
  val VALID_STARTING_YEAR_REGEX = """^(20[0-4][0-9]|2100).*""".r
  //---------------------------------------------------------------------------
  private def getFov(fits: SimpleFits, relevantPos: Array[Double], name: String, id: Int) = {
    val minRa  = relevantPos(0)
    val maxRa  = relevantPos(1)
    val minDec = relevantPos(2)
    val maxDec = relevantPos(3)
    val raRange = maxRa - minRa
    val decRange = maxDec - minDec
    val rec = RectangleDouble(Point2D_Double(minRa, minDec), Point2D_Double(maxRa, maxDec))

    Fov(id
      , name
      , rec
      , LocalDateTime.parse(fits.getStringValueOrEmptyNoQuotation(KEY_OM_DATEOBS))
      , fits.getStringValueOrEmptyNoQuotation(KEY_OM_TELESCOPE)
      , fits.getStringValueOrEmptyNoQuotation(KEY_OM_FILTER)
      , fits.getStringValueOrEmptyNoQuotation(KEY_OM_INSTRUMENT)
      , fits.getStringValueOrEmptyNoQuotation(KEY_OM_EXPTIME).replaceAll("s","").toDouble
      , fits.getStringValueOrEmptyNoQuotation(KEY_NAXIS_1).toInt
      , fits.getStringValueOrEmptyNoQuotation(KEY_NAXIS_2).toInt
      , raRange
      , decRange
      , fits.getStringValueOrEmptyNoQuotation(KEY_OM_PIX_SCALE_X).toDouble
      , fits.getStringValueOrEmptyNoQuotation(KEY_OM_PIX_SCALE_Y).toDouble
      , objectName = fits.getStringValueOrEmptyNoQuotation(KEY_OM_OBJECT)
    )
  }
  //---------------------------------------------------------------------------
  private class ProcessImageDir(fileNameSeq: Array[String]
                               , imageNotProcessed:ConcurrentLinkedQueue[Fov]) extends ParallelTask[String](
    fileNameSeq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(fileName: String) = {
      val image = MyImage(fileName)
      val fits = image.getSimpleFits()
      val imageHasBeenProcessed = fits
        .getStringValue(KEY_M2_RUN_DATE)
        .isDefined
      if (!imageHasBeenProcessed) {
        imageNotProcessed.add(getFov(fits,image.getRaDecRelevantPos(), image.name, image.id))
      }
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private class ProcessFovSeq(spiceKernelSeq: Array[ImageFocusMPO]
                             , fovSeq:Array[Fov]
                             , solvedFovMap: ConcurrentHashMap[String, Boolean]
                             , solvedImageFocusMap: ConcurrentHashMap[String, ImageFocusMPO]
                             , resultMap: ConcurrentHashMap[String, ArrayBuffer[String]]) extends ParallelTask[ImageFocusMPO](
    spiceKernelSeq
      , CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , verbose = false) {
    //----------------------------------------------------------------------
    def userProcessSingleItem(imageFocus: ImageFocusMPO): Unit = {
      info(s"Processing: ${fovSeq.length} FOVs of '${imageFocus.composedName}'")
      fovSeq.foreach { fov =>
        if (!solvedFovMap.contains(fov.imageName)) {
          val composedObjectName = imageFocus.composedName
          val midPointTime = fov.observingDate.plusNanos(Time.secondsToNanos( fov.expTime.toDouble / 2d))
          val raDec = EstimatedSkyPosition.getPosWithFallBack(
            imageFocus
            , Telescope.getOfficialCode(fov.telescope)
            , Telescope.getSpkId(fov.telescope)
            , midPointTime.toString)

          if (raDec.x >= fov.rec.min.x && raDec.x <= fov.rec.max.x &&
              raDec.y >= fov.rec.min.y && raDec.y <= fov.rec.max.y) {

            //add to solved maps
            solvedFovMap.put(fov.imageName, true)
            solvedImageFocusMap.put(composedObjectName,imageFocus)

            //add to result map
            val imageSeq = if (resultMap.contains(composedObjectName)) resultMap.get(composedObjectName)
            else ArrayBuffer[String]()
            imageSeq += fov.imageName
            resultMap.put(composedObjectName, imageSeq)
          }
        }
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.common.image.web.UpdateWebResults._
case class UpdateWebResults(inputDir: String) extends MyLogger {
  //---------------------------------------------------------------------------
  private val fovSeq = getImageNotProcessedSeq()
  private val resultMap = new ConcurrentHashMap[String, ArrayBuffer[String]]()
  private val solvedFovMap = new ConcurrentHashMap[String, Boolean]()
  private val solvedImageFocusMap = new ConcurrentHashMap[String, ImageFocusMPO]()
  private val vrCachedMap = scala.collection.mutable.Map[String, Double]()
  //---------------------------------------------------------------------------
  if (fovSeq.isEmpty)
    warning(s"There is no valid images to process in directory: '$inputDir'")
  else {
    info(s"Found: ${fovSeq.size} images to process")
    Path.resetDirectory(DEFAULT_TMP_DIRECTORY)

    //get the initial kernel seq obtained from object name present in the image header
    val initialKernelSeq = getKernelSeq()

    new ProcessFovSeq(
        initialKernelSeq
      , fovSeq
      , solvedFovMap
      , solvedImageFocusMap
      , resultMap)

    if (solvedFovMap.size() < fovSeq.size) {
      //get latest SPICE SPK
      val spiceKernelSeq = SpiceSPK_DB().findAll()

      //remove the kernels already used and process the remain ones
      val initialKernelIdSeq = initialKernelSeq map (_.spiceSpk.spkID)
      val remainSpiceKernelSeq = spiceKernelSeq.filter { kernel => !initialKernelIdSeq.contains(kernel.spkID) }

      new ProcessFovSeq(
        (remainSpiceKernelSeq map {k=> ImageFocusMPO(k.mainDesignation
                                                   , getComposedObjectName(k.mainDesignation,spkID = Some(k.spkID))
                                                   , k)}).toArray
        , fovSeq
        , solvedFovMap
        , solvedImageFocusMap
        , resultMap)
    }
    else info("All MPO's associated with the images has been found")

    //process MPO sequence
    processMpoSeq()

    Path.deleteDirectory(DEFAULT_TMP_DIRECTORY)
  }

  Path.deleteDirectory(DEFAULT_TMP_DIRECTORY)
  //---------------------------------------------------------------------------
  private def processMpoSeq() = {
    info(s"Processing MPOs")
    val dirSeq = resultMap.asScala.map {case (mpoComposedName,imageSeq) =>
       info(s"Building a directory with ${imageSeq.length} images of MPO '$mpoComposedName' ")
       val dir = Path.resetDirectory(s"$DEFAULT_TMP_DIRECTORY/$mpoComposedName")
       imageSeq.foreach { imageName =>
         s"ln -s $imageName $dir".!!
       }
      (mpoComposedName,dir)
    }.toArray

    dirSeq.foreach { case (mpoComposedName, dir) =>

      if (!vrCachedMap.contains(mpoComposedName)) {
        val vrValue = QueryVR(mpoComposedName
          , ImageFocusMPO.build(mpoComposedName.split("_")(1))
          , outputDir = "").query(simpleMode = true)
        vrCachedMap(mpoComposedName) = vrValue
      }
      val vr = vrCachedMap(mpoComposedName)

      val scriptName = AstrometryQuery.generateScript(
          solvedImageFocusMap.get(mpoComposedName)
        , vr
        , Some(dir)
        , DEFAULT_TMP_DIRECTORY)

      Try {
        val command = s"$scriptName"
        info(s"Command: '$command'")
        val processBuilder = sys.process.Process(Seq("bash", "-c", command))
        val result = processBuilder.!!
        info(result)
      }
      match {
        case Success(_) =>
          warning(s"Script '$scriptName' ends successfuly")
        case Failure(ex) =>
          error(s"Error processing script '$scriptName'\n" + ex.toString)
      }
    }
  }

  //---------------------------------------------------------------------------
  private def getImageNotProcessedSeq() = {
    info(s"Getting images to be processed")
    val imageSeq = Path.getSortedFileList(inputDir, fitsExtension)
      .map {
        _.getAbsolutePath
      }.toArray
    val imageNotProcessed = new ConcurrentLinkedQueue[Fov]()
    new ProcessImageDir(imageSeq,imageNotProcessed)
    imageNotProcessed.asScala.toArray
  }
  //---------------------------------------------------------------------------
  def getKernelSeq() = {
    info(s"Getting SPICE kernels from images")

    fovSeq.flatMap { fov =>
      val objectName = fov.objectName
      if (!fov.objectName.isEmpty && !fov.objectName.isBlank) {
        val focus = ImageFocusMPO.build(objectName, verbose = false)
        if (focus.isDefined) Some(focus)
        else {
          objectName match {
            case VALID_STARTING_YEAR_REGEX(_) =>
              val focus = ImageFocusMPO.build(objectName.take(4) + "_" + objectName.drop(4), verbose = false)
              if (focus.isDefined) Some(focus)
              else None
            case _ => None
          }
        }
      }
      else None
    }.flatten
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file UpdateWebResults.scala
//=============================================================================