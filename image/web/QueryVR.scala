/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  25/Mar/2024
 * Time:  17h:00m
 * Description: None
 */
package com.common.image.web
//=============================================================================
import com.common.database.mongoDB.database.vr.{VR_DB, VR_Entry}
import com.common.database.mongoDB.spiceSPK.SpiceSPK_DB
import com.common.image.focusType.{ImageFocusMPO, ImageFocusTrait}
import com.common.jpl.horizons.Horizons
import com.common.logger.MyLogger
import com.common.util.path.Path

import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
//=============================================================================
object QueryVR {
  //---------------------------------------------------------------------------
  private final val RESULT_FILENAME        = "vr.csv"
  //---------------------------------------------------------------------------
  private final val SIMPLE_MODE_DEFAULT_VR = 0.5d
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.common.image.web.QueryVR._
case class QueryVR(objectName: String
                   , imageFocus: Option[ImageFocusMPO]
                   , outputDir: String = "") extends MyLogger {
  //---------------------------------------------------------------------------
  private val vrDB = VR_DB()
  private val horizons = Horizons()
  //---------------------------------------------------------------------------
  def queryLcdbMostRecentEntry(): Double = {
    val vrEntrySeq = {
      if (imageFocus.isDefined) getVR_EntrySetWithComposedNameNoErrorMessage(imageFocus.get.composedName)
      else getVR_EntryWithNameNoErrorMessage()
    }.getOrElse(return SIMPLE_MODE_DEFAULT_VR)

    val lcdbEntrySeq = vrEntrySeq.filter { entry=>
      entry.databaseName ==   "lcdb_mpc_color_index"
    }
    if (lcdbEntrySeq.isEmpty) SIMPLE_MODE_DEFAULT_VR
    else {
      val sortByYear = lcdbEntrySeq.map( entry=>  (entry.vr,entry.reference.split(" ").last)).sortWith(_._2 > _._2)
      sortByYear.head._1
    }
  }

  //---------------------------------------------------------------------------
  def query(simpleMode: Boolean = false): Double = {
    //-------------------------------------------------------------------------
    def create_output_file(vrEntrySeq:Seq[VR_Entry]) = {
      Path.ensureDirectoryExist(outputDir)
      val o = outputDir + "/"
      val resultFileName = s"$o/$RESULT_FILENAME"

      val bw = new BufferedWriter(new FileWriter(new File(resultFileName)))
      bw.write(VR_Entry.CSV_HEADER + "\n")

      vrEntrySeq.foreach { vrEntry =>
        bw.write(vrEntry.toCsv() + "\n")
      }

      bw.close()
      info(s"Generated file: '$resultFileName'")
    }
    //-------------------------------------------------------------------------
    val vrEntrySeq =
      {if (imageFocus.isDefined) getVR_EntrySetWithComposedName(imageFocus.get.composedName)
      else {
        getVR_EntryWithName()
      }}.getOrElse {
        if (!simpleMode) create_output_file(Seq(VR_Entry("NOT_FOUND_IN_DB"
                                                        , SIMPLE_MODE_DEFAULT_VR
                                                        , 0d
                                                        , "NONE")))
        return SIMPLE_MODE_DEFAULT_VR
      }

    if (simpleMode) {
      if (vrEntrySeq.isEmpty) return SIMPLE_MODE_DEFAULT_VR
      else return vrEntrySeq.head.vr
    }
    create_output_file(vrEntrySeq)
    vrEntrySeq.head.vr
  }
  //---------------------------------------------------------------------------
  private def getVR_EntryWithName(): Option[Seq[VR_Entry]] = {
    val spkID = SpiceSPK_DB.getSpkID(objectName)
    if (spkID.isEmpty) None
    else {
      val doc = horizons.db.getBySpkId(spkID.get).head
      getVR_EntrySetWithComposedName(ImageFocusTrait.getComposedObjectName(doc("name").asString().getValue
                                     , spkID = spkID))
    }
  }
  //---------------------------------------------------------------------------
  private def getVR_EntryWithNameNoErrorMessage(): Option[Seq[VR_Entry]] = {
    val spkID = SpiceSPK_DB.getSpkID(objectName)
    if (spkID.isEmpty) None
    else {
      val doc = horizons.db.getBySpkId(spkID.get).head
      getVR_EntrySetWithComposedNameNoErrorMessage(ImageFocusTrait.getComposedObjectName(doc("name").asString().getValue
        , spkID = spkID))
    }
  }
  //---------------------------------------------------------------------------
  private def getVR_EntrySetWithComposedName(composedName: String): Option[Seq[VR_Entry]] = {
    val vrEntry = vrDB.findByComposedName(composedName)
    if (vrEntry.isEmpty) None
    else Some(vrEntry.head.entrySeq.filter(!_.vr.isNaN))
  }

  //---------------------------------------------------------------------------
  private def getVR_EntrySetWithComposedNameNoErrorMessage(composedName: String): Option[Seq[VR_Entry]] = {
    val vrEntry = vrDB.findByComposedName(composedName)
    if (vrEntry.isEmpty)None
    else Some(vrEntry.head.entrySeq.filter(!_.vr.isNaN))
  }
  //---------------------------------------------------------------------------
  def close() = {
    if (vrDB != null) vrDB.close()
    if (horizons != null) horizons.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file QueryVR.scala
//=============================================================================