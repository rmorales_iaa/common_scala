/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  19/Feb/2021
  * Time:  11h:07m
  * Description: None
  */
//=============================================================================
//=============================================================================
package com.common.image.instrument
//=============================================================================
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.simpleFits.SimpleFits.{KEY_EXTVER, KEY_EXTNAME,KEY_INSTRUM, KEY_INSTRUME}
import com.common.logger.MyLogger
//=============================================================================
object InstrumentMatcher extends MyLogger {
  //---------------------------------------------------------------------------
  final val FIELD_DIVIDER = "\t"
  //---------------------------------------------------------------------------
  private val instrumentSeq : Array[Instrument] = Array (

    Dlr()
    , Site()
    , Busca()
    , Cafos()
    , Mosca()
    , Laica()
    , Panic()
    , Io()
    , Rise()
    , RatCam()
    , Ccd()
    , Andor()
    , Roper()
    , LRS()
    , Prime()
    , JavalambreT80_Cam()
    , EFOSC()
    , SI600()
    , SBIG()
    , Tcp()
    , QHY()
    , DZ936()

    , WFI_Chip_1()
    , WFI_Chip_2()
    , WFI_Chip_3()
    , WFI_Chip_4()
    , WFI_Chip_5()
    , WFI_Chip_6()
    , WFI_Chip_7()
    , WFI_Chip_8()

    , SOI_Chip_1()
    , SOI_Chip_2()
    , SOI_Chip_3()
    , SOI_Chip_4()

    , Fors2_Chip_1()
    , Fors2_Chip_2()

    , MegaPrimeChip_0()
    , MegaPrimeChip_1()
    , MegaPrimeChip_2()
    , MegaPrimeChip_3()
    , MegaPrimeChip_4()
    , MegaPrimeChip_5()
    , MegaPrimeChip_6()
    , MegaPrimeChip_7()
    , MegaPrimeChip_8()
    , MegaPrimeChip_9()
    , MegaPrimeChip_10()
    , MegaPrimeChip_11()
    , MegaPrimeChip_12()
    , MegaPrimeChip_13()
    , MegaPrimeChip_14()
    , MegaPrimeChip_15()
    , MegaPrimeChip_16()
    , MegaPrimeChip_17()
    , MegaPrimeChip_18()
    , MegaPrimeChip_19()
    , MegaPrimeChip_20()
    , MegaPrimeChip_21()
    , MegaPrimeChip_22()
    , MegaPrimeChip_23()
    , MegaPrimeChip_24()
    , MegaPrimeChip_25()
    , MegaPrimeChip_26()
    , MegaPrimeChip_27()
    , MegaPrimeChip_28()
    , MegaPrimeChip_29()
    , MegaPrimeChip_30()
    , MegaPrimeChip_31()
    , MegaPrimeChip_32()
    , MegaPrimeChip_33()
    , MegaPrimeChip_34()
    , MegaPrimeChip_35()

    , Decam19()
    , Decam20()

    , Y4KCam()
    , Tek2K_3()
    , Cfccd()

    , UH_tektronik_2048()

    , Microline_9000()

    , ApogeeUSB_Net()

    , Direct_ccd()

    , GoodManSpectro()

    , Speculoos_1()
    , Speculoos_2()
    , Speculoos_3()
    , Speculoos_4()

    , LCO_SinistroFA_01()
    , LCO_SinistroFA_03()
    , LCO_SinistroFA_05()
    , LCO_SinistroFA_06()
    , LCO_SinistroFA_07()
    , LCO_SinistroFA_11()
    , LCO_SinistroFA_12()
    , LCO_SinistroFA_15()
    , LCO_SinistroFA_20()
    , LCO_NRES_AGU_AK14()
    , LCO_NRES_AGU_AK06()
    , LCO_SBIG_23()
    , LCO_SBIG_24()
    , LCO_SBIG_83()
    , LCO_SBIG_85()
    , LCO_sbig_55()
    , LCO_SBIG_92()
    , LCO_SBIG_95()
    , LCO_FA_04()
    , LCO_FLIP_05()
    , LCO_FLIP_06()
    , LCO_FLIP_07()
    , LCO_FLIP_08()
    , LCO_FLIP_09()
    , LCO_FLIP_10()
    , LCO_FLIP_11()
    , LCO_FLIP_12()
    , LCO_FLIP_13()
    , LCO_FLIP_14()
    , LCO_FLIP_15()
    , LCO_FLIP_16()

    , Osiris()

    , OmegaCam()

    , ZwoAsi_6200()
    , ZwoAsi_461()
    , ZwoAsi_174()

    , AsiCamera_1()
    , Asi461()

    , Alfosc()

    , Wfc()

    , Kmts()

    , Ultrcam()

    , InstrumentUnknown()
  )
  //---------------------------------------------------------------------------
  private def tryToMatch(value: String): Instrument = {
    val r = instrumentSeq flatMap { instrument=>
      val matchResult = instrument.tryToMatch(value)
      if (matchResult._1) Some((instrument,matchResult._2)) else None}
    if (r.isEmpty) InstrumentUnknown()
    else (r.sortWith(_._2 > _._2) map ( _._1)).head
  }

  //---------------------------------------------------------------------------
  //(Instrument, path)
  def tryToMatchSeq(seq: Array[String]) : Array[Instrument] = {
    var matchedCount : Long = 0
    seq.map { instrumentValue =>
      matchedCount += 1
      tryToMatch(instrumentValue)
    }
  }

  //---------------------------------------------------------------------------
  private def getInstrumentValue(fits: SimpleFits): String = {
    val a = fits.getStringValueOrEmptyNoQuotation(KEY_INSTRUME)
    if (!a.isEmpty) return a
    val b = fits.getStringValueOrEmptyNoQuotation(KEY_INSTRUM)
    if (!b.isEmpty)  b
    else ""
  }
  //---------------------------------------------------------------------------
  def getCubeID(fits: SimpleFits): String = {
    val cubeSliceName = fits.getStringValueOrEmptyNoQuotation(KEY_EXTNAME).replaceAll("#","_")
    val cubeSliceVersion = fits.getStringValueOrEmptyNoQuotation(KEY_EXTVER)
    getCubeID(cubeSliceName, cubeSliceVersion)
  }
  //---------------------------------------------------------------------------
  def getCubeID(cubeSliceName: String, cubeSliceVersion: String): String = {
    val cubeSliceID =
      if (cubeSliceName.isEmpty) cubeSliceVersion
      else cubeSliceName + (if (cubeSliceVersion.isEmpty) "" else s"_$cubeSliceVersion")
    if (cubeSliceID.isEmpty) "" else s"_$cubeSliceID"
  }
  //---------------------------------------------------------------------------
  def getInstrument(fits: SimpleFits): String = {
    val r = InstrumentMatcher.tryToMatch(getInstrumentValue(fits)).name
    r + getCubeID(fits)
  }
  //---------------------------------------------------------------------------
  def getInstrument(s: String): String = InstrumentMatcher.tryToMatch(s).name
  //---------------------------------------------------------------------------
  def getInstrument(a: String
                    , b: String
                    , extName:String
                    , extVer: String): String = {
    val r =
      if (!a.isEmpty)  a
      else
        if (!b.isEmpty)  b
        else ""

    getInstrument(r) +  getCubeID(extName,extVer)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file InstrumentMatcher.scala
//=============================================================================
