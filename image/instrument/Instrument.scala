/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  19/Feb/2021
  * Time:  11h:07m
  * Description:
 *      To get Lat-Log-Alt of a Instrument use: 'find_orb' tool: 'mpc_code'
 *      To get XYZ position use 'Telescope.getRectangularCoordinateSeq'
 *      To genereate Instrument bsp:
 *        - modify properly the  file: 'input/spice/kernels/spk/observatories/observatory_def' to append the NEW Instrument
 *            SITES
 *            Instrument_NAME_IDCODE (append 1 to the previous entry to get a NEW index)
 *            Instrument_NAME_XYX
 *        - create a file the the name of Instrument in: 'input/Instrument' filling properly:
 *            observatoryCode
 *              spkObservatoryCode (same as Instrument_NAME_IDCODE)
 *            latLongAlt (find_orb/mpc_code)
 *        - use 'spice' 'pipoint' tool (adapt the paths to the proper location)
 *           rm /home/rafa/proyecto/m2/input/spice/kernels/spk/observatories/observatory.bsp
 *          /home/rafa/images/tools/spice/cspice/exe/pinpoint -def /home/rafa/proyecto/m2/input/spice/kernels/spk/observatories/observatory_def -spk /home/rafa/proyecto/m2/input/spice/kernels/spk/observatories/observatory.bsp
 */
//=============================================================================
//=============================================================================
package com.common.image.instrument
//=============================================================================
import com.common.image.filter.Filter.PATTERN_ALWAYS_FAIL
import com.common.logger.MyLogger
//=============================================================================
import scala.util.matching.Regex
//=============================================================================
object Instrument extends MyLogger {
  //===========================================================================
  val UNKNOWN_INSTRUMENT = "UNKNOWN"
  //===========================================================================
  //higher priority means go first
  val PRIORITY_100 =   100
  val PRIORITY_NONE =    0
  //===========================================================================
  val PATTERN_ALWAYS_FAIL = "(?!)".stripMargin.replaceAll("\n", "").r //always fail match
  //---------------------------------------------------------------------------
  //=============================================================================
}
//=============================================================================
import Instrument._
sealed trait Instrument {
  //---------------------------------------------------------------------------
  val name: String
  val instrumentPattern: Regex
  //---------------------------------------------------------------------------
  //priority: filterValue, alternativeFilterValue, objectValue, path
  def tryToMatch(s: String): (Boolean,Int) = {
    if (tryToMatchInstrumentValue(s)) return (true,PRIORITY_100)
    (false,PRIORITY_NONE)
  }
  //---------------------------------------------------------------------------
  private def tryToMatchInstrumentValue(v: String) = {
    v.toLowerCase.trim match {
      case instrumentPattern(_*) => true
      case _ => false
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================

//=============================================================================
sealed case class InstrumentUnknown() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "UNKNOWN"
  val instrumentPattern = PATTERN_ALWAYS_FAIL
  //---------------------------------------------------------------------------
}
sealed case class Dlr() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "dlr"
  val instrumentPattern = "dlr.*".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class Site() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "site"
  val instrumentPattern = "site.*".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class Busca() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "busca"
  val instrumentPattern = "busca.*".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class Cafos() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "cafos"
  val instrumentPattern =
    """cafos.*|
      |nn.*""".stripMargin.replaceAll("\n", "").r
}
//=============================================================================
sealed case class Mosca() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "mosca"
  val instrumentPattern = "mosca.*".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class Laica() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "laica"
  val instrumentPattern = "laica.*".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class Panic() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "panic"
  val instrumentPattern = "panic.*".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class Io() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "io"
  val instrumentPattern = "io.*".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class Rise() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "rise"
  val instrumentPattern = "rise.*".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class RatCam() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "ratcam"
  val instrumentPattern = "ratcam.*".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class Ccd() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "ccd"
  val instrumentPattern = "ccd.*".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class Andor() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "andor"
  val instrumentPattern = "andor.*".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class Roper() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "roper"
  val instrumentPattern = "roper.*".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//=============================================================================
//dolores
sealed case class LRS() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "lrs"
  val instrumentPattern = "lrs.*".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class Prime() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "prime"
  val instrumentPattern = "prime.*".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//=============================================================================
//javalambre
sealed case class JavalambreT80_Cam() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "t80cam"
  val instrumentPattern = ".*t80cam.*".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class EFOSC() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "efosc"
  val instrumentPattern = ".*efosc.*".stripMargin.replaceAll("\n","").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class SI600() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "si600"
  val instrumentPattern =
    """si600.*|
      |camelot.*""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}

//=============================================================================
sealed case class Tcp() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "tcp"
  val instrumentPattern =
    """tcp.*""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class SBIG() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "sbig"
  val instrumentPattern =
    """sbig.*""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class QHY() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "qhy"
  val instrumentPattern =
    """qhy.*""".stripMargin.replaceAll("\n", "").r
}
//=============================================================================
//Pic du midi
sealed case class DZ936() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "DZ936"
  val instrumentPattern =
    """dz936.*""".stripMargin.replaceAll("\n", "").r
}
//=============================================================================
//ESO MPI 2.2
sealed abstract class WFI(subType: String) extends Instrument {
  //---------------------------------------------------------------------------
  val name = s"wfi_chip_$subType"
  val instrumentPattern =
    s"""wfi_chip_$subType*""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
sealed case class WFI_Chip_1() extends WFI("1")
sealed case class WFI_Chip_2() extends WFI("2")
sealed case class WFI_Chip_3() extends WFI("3")
sealed case class WFI_Chip_4() extends WFI("4")
sealed case class WFI_Chip_5() extends WFI("5")
sealed case class WFI_Chip_6() extends WFI("6")
sealed case class WFI_Chip_7() extends WFI("7")
sealed case class WFI_Chip_8() extends WFI("8")
//=============================================================================
//SOAR SOI
sealed abstract class SOI(subType: String) extends Instrument {
  //---------------------------------------------------------------------------
  val name = s"soi_chip_$subType"
  val instrumentPattern =
    s"""soi_im$subType*""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
sealed case class SOI_Chip_1() extends SOI("1")
sealed case class SOI_Chip_2() extends SOI("2")
sealed case class SOI_Chip_3() extends SOI("3")
sealed case class SOI_Chip_4() extends SOI("4")

//=============================================================================
//VLT
sealed abstract class Fors2(subType: String) extends Instrument {
  //---------------------------------------------------------------------------
  val name = s"fors2_chip_$subType"
  val instrumentPattern =
    s"""fors2_chip$subType*""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
sealed case class Fors2_Chip_1() extends Fors2("1")
sealed case class Fors2_Chip_2() extends Fors2("2")
//=============================================================================
//VLT
sealed abstract class MegaPrime(subType: String) extends Instrument {
  //---------------------------------------------------------------------------
  val name = s"megaprime_chip_$subType"
  val instrumentPattern =
    s"""megaprime_ccd$subType*""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
sealed case class MegaPrimeChip_0() extends MegaPrime("00")
sealed case class MegaPrimeChip_1() extends MegaPrime("01")
sealed case class MegaPrimeChip_2() extends MegaPrime("02")
sealed case class MegaPrimeChip_3() extends MegaPrime("03")
sealed case class MegaPrimeChip_4() extends MegaPrime("04")
sealed case class MegaPrimeChip_5() extends MegaPrime("05")
sealed case class MegaPrimeChip_6() extends MegaPrime("06")
sealed case class MegaPrimeChip_7() extends MegaPrime("07")
sealed case class MegaPrimeChip_8() extends MegaPrime("08")
sealed case class MegaPrimeChip_9() extends MegaPrime("09")
sealed case class MegaPrimeChip_10() extends MegaPrime("10")
sealed case class MegaPrimeChip_11() extends MegaPrime("11")
sealed case class MegaPrimeChip_12() extends MegaPrime("12")
sealed case class MegaPrimeChip_13() extends MegaPrime("13")
sealed case class MegaPrimeChip_14() extends MegaPrime("14")
sealed case class MegaPrimeChip_15() extends MegaPrime("15")
sealed case class MegaPrimeChip_16() extends MegaPrime("16")
sealed case class MegaPrimeChip_17() extends MegaPrime("17")
sealed case class MegaPrimeChip_18() extends MegaPrime("18")
sealed case class MegaPrimeChip_19() extends MegaPrime("19")
sealed case class MegaPrimeChip_20() extends MegaPrime("20")
sealed case class MegaPrimeChip_21() extends MegaPrime("21")
sealed case class MegaPrimeChip_22() extends MegaPrime("22")
sealed case class MegaPrimeChip_23() extends MegaPrime("23")
sealed case class MegaPrimeChip_24() extends MegaPrime("24")
sealed case class MegaPrimeChip_25() extends MegaPrime("25")
sealed case class MegaPrimeChip_26() extends MegaPrime("26")
sealed case class MegaPrimeChip_27() extends MegaPrime("27")
sealed case class MegaPrimeChip_28() extends MegaPrime("28")
sealed case class MegaPrimeChip_29() extends MegaPrime("29")
sealed case class MegaPrimeChip_30() extends MegaPrime("30")
sealed case class MegaPrimeChip_31() extends MegaPrime("31")
sealed case class MegaPrimeChip_32() extends MegaPrime("32")
sealed case class MegaPrimeChip_33() extends MegaPrime("33")
sealed case class MegaPrimeChip_34() extends MegaPrime("34")
sealed case class MegaPrimeChip_35() extends MegaPrime("35")
//=============================================================================
//CTIO_1
sealed case class Y4KCam() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "Y4KCam"
  val instrumentPattern =
    """y4kcam.*""".stripMargin.replaceAll("\n", "").r
}
//=============================================================================
//CTIO_09
sealed case class Tek2K_3() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "Tek2K_3"
  val instrumentPattern =
    """tek2K_3.*""".stripMargin.replaceAll("\n", "").r
}
//=============================================================================
//CTIO_09
sealed case class Cfccd() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "cfccd"
  val instrumentPattern =
    """cfccd.*""".stripMargin.replaceAll("\n", "").r
}
//=============================================================================
//CTIO 4m
sealed abstract class Decam(subType: String) extends Instrument {
  //---------------------------------------------------------------------------
  val name = s"decam_chip_$subType"
  val instrumentPattern =
    s"""decam_$subType.*|
       |decam_$subType.*""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
sealed case class Decam19() extends Decam("s19")
sealed case class Decam20() extends Decam("n20")
//=============================================================================
//UH Tektronix 2048 CCD (Mauna Kea-UH)
sealed case class UH_tektronik_2048() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "uh_tektronix_2048"
  val instrumentPattern =
    """uh tektronix 2048 ccd""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
//=============================================================================
//MicroLine 9000 (Odessa-Mayaki)
sealed case class Microline_9000() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "microline_9000"
  val instrumentPattern =
    """microline 9000""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
//=============================================================================
//Apogee USB/Net (Bosque alegre)
sealed case class ApogeeUSB_Net() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "apogee_usb_net"
  val instrumentPattern =
    """apogee usb/net""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
//=============================================================================
//Direct CCF (Casleo)
sealed case class Direct_ccd() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "direct_ccd"
  val instrumentPattern =
    """direct ccd""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
//=============================================================================
//Goodman spectro (SOAR)
sealed case class GoodManSpectro() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "goodman_spectro"
  val instrumentPattern =
    """goodman spectro""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
//=============================================================================
//SPECULOOS-South Observatory, Paranal
sealed abstract class Speculoos(subType: String) extends Instrument {
  //---------------------------------------------------------------------------
  val name = s"speculoos_$subType"
  val instrumentPattern =
    s"""speculoos$subType""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
sealed case class Speculoos_1() extends Speculoos("1")
sealed case class Speculoos_2() extends Speculoos("2")
sealed case class Speculoos_3() extends Speculoos("3")
sealed case class Speculoos_4() extends Speculoos("4")

//=============================================================================
//Las Cumbres Observatory Global Telescope (LCOGT)
sealed abstract class LCO(subType: String) extends Instrument {
  //---------------------------------------------------------------------------
  val name = s"lco_$subType"
  val instrumentPattern =
    s"""$subType""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}

sealed case class LCO_SinistroFA_01() extends LCO("fa01")
sealed case class LCO_SinistroFA_03() extends LCO("fa03")
sealed case class LCO_SinistroFA_05() extends LCO("fa05")
sealed case class LCO_SinistroFA_06() extends LCO("fa05")
sealed case class LCO_SinistroFA_07() extends LCO("fa07")
sealed case class LCO_SinistroFA_11() extends LCO("fa11")
sealed case class LCO_SinistroFA_12() extends LCO("fa12")
sealed case class LCO_SinistroFA_15() extends LCO("fa15")
sealed case class LCO_SinistroFA_20() extends LCO("fa20")

sealed case class LCO_NRES_AGU_AK14() extends LCO("nres-agu-ak14")
sealed case class LCO_NRES_AGU_AK06() extends LCO("nres-agu-ak06")

sealed case class LCO_SBIG_23() extends LCO("kb23")
sealed case class LCO_SBIG_24() extends LCO("kb24")
sealed case class LCO_SBIG_83() extends LCO("kb83")
sealed case class LCO_SBIG_85() extends LCO("kb85")
sealed case class LCO_sbig_55() extends LCO("kb55")
sealed case class LCO_SBIG_92() extends LCO("kb92")
sealed case class LCO_SBIG_95() extends LCO("kb95")

sealed case class LCO_FA_04() extends LCO("fa04")

sealed case class LCO_FLIP_05() extends LCO("ff05")
sealed case class LCO_FLIP_06() extends LCO("ff06")
sealed case class LCO_FLIP_07() extends LCO("ff07")
sealed case class LCO_FLIP_08() extends LCO("ff08")
sealed case class LCO_FLIP_09() extends LCO("ff09")
sealed case class LCO_FLIP_10() extends LCO("ff10")
sealed case class LCO_FLIP_11() extends LCO("ff11")
sealed case class LCO_FLIP_12() extends LCO("ff12")
sealed case class LCO_FLIP_13() extends LCO("ff13")
sealed case class LCO_FLIP_14() extends LCO("ff14")
sealed case class LCO_FLIP_15() extends LCO("ff15")
sealed case class LCO_FLIP_16() extends LCO("ff16")
//=============================================================================
//Osiris, GTC
sealed case class Osiris() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "osiris"
  val instrumentPattern =
    """osiris""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
//=============================================================================
//OmegaCam,ESO VST
sealed case class OmegaCam() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "omegacam"
  val instrumentPattern =
    """omegacam""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}

//=============================================================================
//ZWO, Calar Alto 2.2
sealed case class ZwoAsi_6200() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "zwo_asi_6200"
  val instrumentPattern =
    """.*zwo.*asi6200.*""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
//ZWO, Calar Alto 1.23
sealed case class ZwoAsi_461() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "zwo_asi_461"
  val instrumentPattern =
    """.*zwo.*asi461.*""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
//=============================================================================
//monsky test cameras at La Sagra.
sealed case class ZwoAsi_174() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "zwo_asi_174"
  val instrumentPattern =
    """.*zwo.*asi174.*""".stripMargin.replaceAll("\n", "").r
}
//=============================================================================
//ASI Camera 1, Calar Alto 1.3
sealed case class AsiCamera_1() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "asi_camera_1"
  val instrumentPattern =
    """.*asi.*camera.*(1).*""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
//=============================================================================
//ASI 461MM Pro, Calar Alto 1.3
sealed case class Asi461() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "asi_461"
  val instrumentPattern =
    """.*asi461.*""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
//=============================================================================
//Alfosc, NOT (La palma)
sealed case class Alfosc() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "alfosc"
  val instrumentPattern =
    """alfosc.*""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}

//=============================================================================
//WFC (INT wide-field camera), INT (La palma)
sealed case class Wfc() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "wfc"
  val instrumentPattern =
    """wfc""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
//=============================================================================
//KMTS (CTIO)
sealed case class Kmts() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "kmts"
  val instrumentPattern =
    """kmts.*""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class Ultrcam() extends Instrument() {
  //---------------------------------------------------------------------------
  val name = "ultracam"
  val instrumentPattern =
    """ultracam""".stripMargin.replaceAll("\n", "").r
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Instrument.scala
//=============================================================================
