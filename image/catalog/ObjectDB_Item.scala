/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Jan/2023
 * Time:  10h:35m
 * Description: None
 */
//=============================================================================
package com.common.image.catalog
//=============================================================================
import com.common.csv.{CsvItem, CsvRow}
import com.common.image.photometry.absolute.photometricModel.PhotometricModelDB_Item
import org.mongodb.scala.bson.codecs.Macros._
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
//=============================================================================
//=============================================================================
object ObjectDB_Item {
  //---------------------------------------------------------------------------
  //https://www.jannikarndt.de/blog/2017/08/writing_case_classes_to_mongodb_in_scala/
  private val customCodecs = fromProviders(classOf[ObjectDB_Item]
                                           , classOf[SourceDB_Item]
                                           , classOf[PhotometricModelDB_Item])

  val codecRegistry = fromRegistries(customCodecs
                                     , DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  final val COL_NAME_ID                   = "_id"
  final val COL_NAME_SOURCE_SEQ           = "source_seq"
  final val COL_NAME_PHOTOMETRY_MODEL_SEQ = "photometry_model_seq"
  //---------------------------------------------------------------------------
  final val COL_NAME_MIN_APERTURE_DATABASE     = "min_aperture"
  final val COL_NAME_ANNULAR_APERTURE_DATABASE = "annular_aperture"
  final val COL_NAME_MAX_APERTURE_DATABASE     = "max_aperture"
  //---------------------------------------------------------------------------
  final val COL_NAME_IMAGE_NAME           = "image_name"
  final val COL_NAME_OBSERVING_TIME       = "observing_time"
  final val COL_NAME_FILTER               = "filter"
  final val COL_NAME_TELESCOPE            = "telescope"
  final val COL_NAME_INSTRUMENT           = "instrument"
  final val COL_NAME_EXPOSURE_TIME        = "exposure_time(s)"

  final val COL_NAME_JULIAN_DATE          = "julian_date"
  final val COL_NAME_OBSERVING_NIGHT      = "observing_night"

  final val COL_NAME_BACKGROUND           = "background(counts)"
  final val COL_NAME_NOISE_TIDE           = "noise_tide(counts)"
  final val COL_NAME_FWHM                 = "fwhm(pix)"

  final val COL_NAME_X_PIX_SIZE           = "x_pix_size"
  final val COL_NAME_Y_PIX_SIZE           = "y_pix_size"
  final val COL_NAME_RA_MIN               = "ra_min(dec_deg)"
  final val COL_NAME_RA_MAX               = "ra_max(dec_deg)"
  final val COL_NAME_DEC_MIN              = "dec_min(dec_deg)"
  final val COL_NAME_DEC_MAX              = "dec_max(dec_deg)"
  final val COL_NAME_RA_FOV               = "ra_fov(arcmin)"
  final val COL_NAME_DEC_FOV              = "dec_fov(arcmin)"
  final val COL_NAME_X_PIX_SCALE          = "x_pix_scale(arcosec/pixel)"
  final val COL_NAME_Y_PIX_SCALE          = "y_pix_scale(arcosec/pixel)"

  final val COL_NAME_M2_WCS_FITTING_FLAGS = "m2_wcs_fitting_flags"
  final val COL_NAME_OM_FLAGS             = "om_flags"

  final val COL_NAME_SOURCE_COUNT         = "source_count"
  final val COL_NAME_GAIA_SOURCE_COUNT    = "gaia_source_count"
  final val COL_NAME_RA_QUALITY           = "ra_std_dev_gaia(mas)"
  final val COL_NAME_DEC_QUALITY          = "dec_std_dev_gaia(mas)"

  final val COL_NAME_MIN_APERTURE         = "min_aperture(pix)"
  final val COL_NAME_ANNULAR_APERTURE     = "annular_aperture(pix)"
  final val COL_NAME_MAX_APERTURE         = "max_aperture(pix)"

  //---------------------------------------------------------------------------
  private val columnSeq = Seq(
    COL_NAME_IMAGE_NAME
    , COL_NAME_OBSERVING_TIME
    , COL_NAME_FILTER
    , COL_NAME_TELESCOPE
    , COL_NAME_EXPOSURE_TIME

    , COL_NAME_JULIAN_DATE
    , COL_NAME_OBSERVING_NIGHT

    , COL_NAME_BACKGROUND
    , COL_NAME_NOISE_TIDE
    , COL_NAME_FWHM

    , COL_NAME_X_PIX_SIZE
    , COL_NAME_Y_PIX_SIZE
    , COL_NAME_RA_MIN
    , COL_NAME_RA_MAX
    , COL_NAME_DEC_MIN
    , COL_NAME_DEC_MAX
    , COL_NAME_RA_FOV
    , COL_NAME_DEC_FOV
    , COL_NAME_X_PIX_SCALE
    , COL_NAME_Y_PIX_SCALE

    , COL_NAME_M2_WCS_FITTING_FLAGS
    , COL_NAME_OM_FLAGS

    , COL_NAME_SOURCE_COUNT
    , COL_NAME_GAIA_SOURCE_COUNT
    , COL_NAME_RA_QUALITY
    , COL_NAME_DEC_QUALITY

    , COL_NAME_MIN_APERTURE
    , COL_NAME_ANNULAR_APERTURE
    , COL_NAME_MAX_APERTURE
  )
  //---------------------------------------------------------------------------
  //fake item used in light curve rotational period estimate
  def apply(imageName: String
            , filter: String
            , telescope:String
            , julian_date_mid_point: Double): ObjectDB_Item = {
    ObjectDB_Item(_id = imageName
      , observing_time = ""
      , filter
      , telescope = telescope
      , instrument = ""
      , exposure_time = 0
      , julian_date_mid_point
      , observing_night = ""
      , background = 0
      , noise_tide = 0
      , fwhm = 0
      , x_pix_size = 0
      , y_pix_size = 0
      , ra_min = 0
      , ra_max = 0
      , dec_min = 0
      , dec_max = 0
      , ra_fov = 0
      , dec_fov = 0
      , x_pix_scale = 0
      , y_pix_scale = 0
      , m2_wcs_fitting_flags = ""
      , om_flags = ""
      , source_count = 0
      , gaia_source_count = 0
      , ra_std_dev_gaia = 0
      , dec_std_dev_gaia = 0
      , min_aperture = 0
      , annular_aperture = 0
      , max_aperture = 0
      , source_seq = Seq[SourceDB_Item]()
      , photometry_model_seq = Seq[PhotometricModelDB_Item]()
    )
  }
  //---------------------------------------------------------------------------
   def normalizeColNameForDatabase(s: String) = {
    val posOpenParenthesis = s.indexOf("(")
    if (posOpenParenthesis == -1) s else s.take(posOpenParenthesis)
  }
  //---------------------------------------------------------------------------
  def apply(row: CsvRow): ObjectDB_Item =
    ObjectDB_Item(
        _id                   = row.getString(COL_NAME_IMAGE_NAME)
      , observing_time        = row.getString(COL_NAME_OBSERVING_TIME)
      , filter                = row.getString(COL_NAME_FILTER)
      , telescope             = row.getString(COL_NAME_TELESCOPE)
      , instrument            = row.getString(COL_NAME_INSTRUMENT)
      , exposure_time         = row.getDouble(COL_NAME_EXPOSURE_TIME)
      , julian_date_mid_point = row.getDouble(COL_NAME_JULIAN_DATE)
      , observing_night       = row.getString(COL_NAME_OBSERVING_NIGHT)
      , background            = row.getDouble(COL_NAME_BACKGROUND)
      , noise_tide            = row.getDouble(COL_NAME_NOISE_TIDE)
      , fwhm                  = row.getDouble(COL_NAME_FWHM)
      , x_pix_size            = row.getInt(COL_NAME_X_PIX_SIZE)
      , y_pix_size            = row.getInt(COL_NAME_Y_PIX_SIZE)
      , ra_min                = row.getDouble(COL_NAME_RA_MIN)
      , ra_max                = row.getDouble(COL_NAME_RA_MAX)
      , dec_min               = row.getDouble(COL_NAME_DEC_MIN)
      , dec_max               = row.getDouble(COL_NAME_DEC_MAX)
      , ra_fov                = row.getDouble(COL_NAME_RA_FOV)
      , dec_fov               = row.getDouble(COL_NAME_DEC_FOV)
      , x_pix_scale           = row.getDouble(COL_NAME_X_PIX_SCALE)
      , y_pix_scale           = row.getDouble(COL_NAME_Y_PIX_SCALE)
      , m2_wcs_fitting_flags  = row.getString(COL_NAME_M2_WCS_FITTING_FLAGS)
      , om_flags              = row.getString(COL_NAME_OM_FLAGS)
      , source_count          = row.getInt(COL_NAME_SOURCE_COUNT)
      , gaia_source_count     = row.getInt(COL_NAME_GAIA_SOURCE_COUNT)
      , ra_std_dev_gaia       = row.getDouble(COL_NAME_RA_QUALITY)
      , dec_std_dev_gaia      = row.getDouble(COL_NAME_DEC_QUALITY)
      , min_aperture          = row.getDouble(COL_NAME_MIN_APERTURE)
      , annular_aperture      = row.getDouble(COL_NAME_ANNULAR_APERTURE)
      , max_aperture          = row.getDouble(COL_NAME_MAX_APERTURE)
    )
  //---------------------------------------------------------------------------
  final val headerRow = {
    val row = CsvRow()
    columnSeq foreach (row + CsvItem(_))
    row
  }
  //---------------------------------------------------------------------------
  final val headerNameToDB_ColName= {
    classOf[ObjectDB_Item].getDeclaredFields.map { f =>
       f.setAccessible(true)
       val headerName = f.getName()
       val dbColName = headerRow.getColName(headerName).getOrElse("None")
       f.setAccessible(false)
       (headerName,dbColName)
      }.toMap
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import ObjectDB_Item._
case class ObjectDB_Item(_id: String //image name
                         , observing_time: String
                         , filter: String
                         , telescope: String
                         , instrument: String
                         , exposure_time: Double //s
                         , julian_date_mid_point: Double //julia date light time corrected
                         , observing_night: String
                         , background: Double //counts
                         , noise_tide: Double //counts
                         , fwhm: Double //pix
                         , x_pix_size: Int
                         , y_pix_size: Int
                         , ra_min: Double
                         , ra_max: Double
                         , dec_min: Double
                         , dec_max: Double
                         , ra_fov: Double //arcmin
                         , dec_fov: Double //arcmin
                         , x_pix_scale: Double //arcosec/pixel
                         , y_pix_scale: Double //arcosec/pixel
                         , m2_wcs_fitting_flags: String
                         , om_flags: String
                         , source_count: Int
                         , gaia_source_count: Int
                         , ra_std_dev_gaia: Double //mas
                         , dec_std_dev_gaia: Double //mas
                         , min_aperture: Double //pix
                         , annular_aperture: Double //pix
                         , max_aperture: Double //pix
                         , source_seq: Seq[SourceDB_Item] = Seq[SourceDB_Item]()
                         , photometry_model_seq: Seq[PhotometricModelDB_Item]= Seq[PhotometricModelDB_Item]()) {
  //---------------------------------------------------------------------------
  def toRow() = {
    val nameValueMap = this.getClass.getDeclaredFields.flatMap { f =>
      f.setAccessible(true)
      val headerName = f.getName()
      val r = if (headerName == COL_NAME_SOURCE_SEQ) None
      else {
        val s =
          if (headerName == COL_NAME_ID) COL_NAME_IMAGE_NAME
          else headerNameToDB_ColName(headerName)
        Some((s,f.get(this).toString))
      }
      f.setAccessible(false)
      r
    }.toMap
    CsvRow(nameValueMap)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ObjectDB_Item.scala
//=============================================================================
