/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  11/Jan/2023
 * Time:  13h:12m
 * Description: None
 */
//=============================================================================
package com.common.image.catalog
//=============================================================================
import com.common.csv.{CsvItem, CsvRow}
import com.common.geometry.point.Point2D_Double
import com.common.logger.MyLogger
import org.mongodb.scala.Document
//=============================================================================
//=============================================================================
object SourceDB_Item extends MyLogger {
  //---------------------------------------------------------------------------
  final val COL_NAME_M2_ID                = "m2_id"
  final val COL_NAME_FOCUS_ID             = "focus_id"
  final val COL_NAME_GAIA_ID              = "gaia_id"
  final val COL_NAME_X_PIX                = "x_pix"  //first x pix is 1
  final val COL_NAME_Y_PIX                = "y_pix"  //first y pix is 1
  final val COL_NAME_RA_DEC_DEG           = "ra(dec_deg)"
  final val COL_NAME_DEC_DEC_DEG          = "dec(dec_deg)"
  final val COL_NAME_RA_HMS               = "ra(hms)"
  final val COL_NAME_DEC_DMS              = "dec(dms)"
  final val COL_NAME_PIX_COUNT            = "pix_count"
  final val COL_NAME_FWHM                 = "fwhm(pix)"

  final val COL_NAME_PERIMETER            = "perimeter"
  final val COL_NAME_ECCENTRICITY         = "eccentricity"
  final val COL_NAME_PIX_IN_PERCENTAGE    = "pix_in_percentage"

  final val COL_NAME_FLUX_PER_SECOND      = "flux_per_second(counts/s)"
  final val COL_NAME_SNR                  = "snr"
  final val COL_NAME_ESTIMATED_BACKGROUND = "estimated_background"
  //---------------------------------------------------------------------------
  private val columnSeq = Seq(
      COL_NAME_M2_ID
    , COL_NAME_FOCUS_ID
    , COL_NAME_GAIA_ID
    , COL_NAME_X_PIX
    , COL_NAME_Y_PIX
    , COL_NAME_RA_DEC_DEG
    , COL_NAME_DEC_DEC_DEG
    , COL_NAME_RA_HMS
    , COL_NAME_DEC_DMS
    , COL_NAME_PIX_COUNT
    , COL_NAME_FWHM
    , COL_NAME_PERIMETER
    , COL_NAME_ECCENTRICITY
    , COL_NAME_PIX_IN_PERCENTAGE
    , COL_NAME_FLUX_PER_SECOND
    , COL_NAME_SNR
    , COL_NAME_ESTIMATED_BACKGROUND
  )
  //---------------------------------------------------------------------------
  final val headerRow = {
    val row = CsvRow()
    columnSeq foreach (row + CsvItem(_))
    row
  }
  //---------------------------------------------------------------------------
  final val headerNameToDB_ColName = {
    classOf[SourceDB_Item].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val headerName = f.getName()
      val dbColName = headerName match {
        case "ra_hms"  => COL_NAME_RA_HMS
        case "dec_dms" => COL_NAME_DEC_DMS
        case _         =>  headerRow.getColName(headerName).getOrElse("None")
      }

      f.setAccessible(false)
      (headerName, dbColName)
    }.toMap
  }
  //---------------------------------------------------------------------------
  def apply(row: CsvRow): SourceDB_Item =
    SourceDB_Item(
        m2_id                = row.getInt(COL_NAME_M2_ID)
      , focus_id             = row.getStringOrDefault(COL_NAME_FOCUS_ID,"")
      , gaia_id              = row.getLongOrDefault(COL_NAME_GAIA_ID,-1)
      , x_pix                = row.getDouble(COL_NAME_X_PIX)
      , y_pix                = row.getDouble(COL_NAME_Y_PIX)
      , ra                   = row.getDouble(COL_NAME_RA_DEC_DEG)
      , dec                  = row.getDouble(COL_NAME_DEC_DEC_DEG)
      , ra_hms               = row.getString(COL_NAME_RA_HMS)
      , dec_dms              = row.getString(COL_NAME_DEC_DMS)
      , pix_count            = row.getLong(COL_NAME_PIX_COUNT)
      , fwhm                 = row.getDouble(COL_NAME_FWHM)
      , perimeter            = row.getDouble(COL_NAME_PERIMETER)
      , eccentricity         = row.getDouble(COL_NAME_ECCENTRICITY)
      , pix_in_percentage    = row.getDouble(COL_NAME_PIX_IN_PERCENTAGE)
      , flux_per_second      = row.getDouble(COL_NAME_FLUX_PER_SECOND)
      , snr                  = row.getDouble(COL_NAME_SNR)
      , estimated_background = row.getDouble(COL_NAME_ESTIMATED_BACKGROUND)
    )

  //---------------------------------------------------------------------------
  def apply(doc: Document): SourceDB_Item = {
    SourceDB_Item(
        doc(normalizeColNameForDatabase(COL_NAME_M2_ID)).asInt32().getValue
      , doc(normalizeColNameForDatabase(COL_NAME_FOCUS_ID)).asString().getValue
      , doc(normalizeColNameForDatabase(COL_NAME_GAIA_ID)).asInt64().getValue
      , doc(normalizeColNameForDatabase(COL_NAME_X_PIX)).asDouble().getValue
      , doc(normalizeColNameForDatabase(COL_NAME_Y_PIX)).asDouble().getValue
      , doc(normalizeColNameForDatabase(COL_NAME_RA_DEC_DEG)).asDouble().getValue
      , doc(normalizeColNameForDatabase(COL_NAME_DEC_DEC_DEG)).asDouble().getValue
      , doc(normalizeColNameForDatabase(COL_NAME_RA_HMS)).asString().getValue
      , doc(normalizeColNameForDatabase(COL_NAME_DEC_DMS)).asString().getValue
      , doc(normalizeColNameForDatabase(COL_NAME_PIX_COUNT)).asInt64().getValue.toShort
      , doc(normalizeColNameForDatabase(COL_NAME_FWHM)).asDouble().getValue
      , doc(normalizeColNameForDatabase(COL_NAME_PERIMETER)).asDouble().getValue
      , doc(normalizeColNameForDatabase(COL_NAME_ECCENTRICITY)).asDouble().getValue
      , doc(normalizeColNameForDatabase(COL_NAME_PIX_IN_PERCENTAGE)).asDouble().getValue
      , doc(normalizeColNameForDatabase(COL_NAME_FLUX_PER_SECOND)).asDouble().getValue
      , doc(normalizeColNameForDatabase(COL_NAME_SNR)).asDouble().getValue
      , doc(normalizeColNameForDatabase(COL_NAME_ESTIMATED_BACKGROUND)).asDouble().getValue
    )
  }
  //---------------------------------------------------------------------------
  def getQualifiedName(prefix: String) = (headerNameToDB_ColName map (s=> s"$prefix." + s._1)).toSeq
  //---------------------------------------------------------------------------
  def normalizeColNameForDatabase(s: String) = {
    val posOpenParenthesis = s.indexOf("(")
    val r = if (posOpenParenthesis == -1) s else s.take(posOpenParenthesis)
    if (s.contains("(hms)")) r + "_hms"
    else
      if (s.contains("(dms)")) r + "_dms"
      else r
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import SourceDB_Item._
case class SourceDB_Item(m2_id: Int
                         , focus_id: String
                         , gaia_id: Long
                         , x_pix: Double
                         , y_pix: Double
                         , ra: Double //decimal degree
                         , dec: Double //decimal degree
                         , ra_hms: String
                         , dec_dms: String
                         , pix_count: Long
                         , fwhm: Double
                         , perimeter: Double
                         , eccentricity: Double
                         , pix_in_percentage: Double
                         , flux_per_second: Double
                         , snr: Double
                         , estimated_background: Double) {
  //---------------------------------------------------------------------------
  def getRaDec = Point2D_Double(ra,dec)
  //---------------------------------------------------------------------------
  def toRow() = {
    val nameValueMap = this.getClass.getDeclaredFields.map { f =>
      f.setAccessible(true)
      val headerName = f.getName()
      val r = (headerNameToDB_ColName(headerName), f.get(this).toString)
      f.setAccessible(false)
      r
    }.toMap
    CsvRow(nameValueMap)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file SourceDB_Item.scala
//=============================================================================