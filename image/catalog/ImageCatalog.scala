/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  11/Jan/2023
 * Time:  13h:12m
 * Description: None
 */
//=============================================================================
package com.common.image.catalog
//=============================================================================
import com.common.csv.{CsvRow, CsvWrite}
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.geometry.point.Point2D_Double
import com.common.hardware.cpu.CPU
import com.common.image.catalog.ObjectDB_Item._
import com.common.image.photometry.absolute.PolynomialFit
import com.common.image.photometry.absolute.photometricModel._
import com.common.util.file.MyFile
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path

import java.util.concurrent.ConcurrentHashMap
import scala.collection.JavaConverters.mapAsScalaConcurrentMapConverter
//=============================================================================
import org.mongodb.scala.bson.BsonDocument
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
import org.mongodb.scala.model.Aggregates.{filter, project, unwind}
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Projections.{excludeId, fields, include}
import org.mongodb.scala.model.Updates._
import scala.util.{Failure, Success, Try}
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object ImageCatalog {
  //---------------------------------------------------------------------------
  private final val DATABASE_NAME   = "mpo_image_catalog"
  //---------------------------------------------------------------------------
  private final val NONE_FOCUS_OBJECT = "none"
  //---------------------------------------------------------------------------
  //Map(imageName,Seq[(image_source,image_focus)])
  type ImageSourceFocusMap = Map[String,
                                 Seq[(SourceDB_Item, ImageFocusDB_Item)]]

  //Map(imageFilter,Seq(imageName))
  type ImageByFilterMap = Map[String,ArrayBuffer[String]]

  //Map(imageFilter,Map(catalog,Map(catalogFilter,polynomial fit)))
  type ImagePhotometricMap = Map[String,Map[String, Map[String, PolynomialFit]]]
  //---------------------------------------------------------------------------
}
//=============================================================================
import ImageCatalog._
case class ImageCatalog(collectionName: String) extends MongoDB {
  //-------------------------------------------------------------------------
  val databaseName = DATABASE_NAME
  //-------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = initWithCodeRegister(ObjectDB_Item.codecRegistry)
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  val collWithRegistry: MongoCollection[ObjectDB_Item] = database.getCollection(collectionName)
  connected = true
  //-------------------------------------------------------------------------
  def add(row: CsvRow): Unit = {
    val entry = ObjectDB_Item(row)
    val imageName = entry._id
    if (exist(imageName))
      synchronized(collWithRegistry.replaceOne(equal(COL_NAME_ID, imageName),entry).headResult)
    else
      synchronized(collWithRegistry.insertOne(entry).headResult)
  }
  //-------------------------------------------------------------------------
  def updateSourceSeq(sourceSeq: Seq[SourceDB_Item], imageName: String): Unit =
    synchronized(collWithRegistry.updateOne(equal(COL_NAME_ID, imageName)
                                , set(COL_NAME_SOURCE_SEQ, sourceSeq)).headResult)
  //-------------------------------------------------------------------------
  def updatePhotometryModelSeq(itemSeq: Seq[PhotometricModelDB_Item], imageName: String): Unit =
    synchronized(collWithRegistry.updateOne(equal(COL_NAME_ID, imageName)
      , set(COL_NAME_PHOTOMETRY_MODEL_SEQ, itemSeq)).headResult)
  //-------------------------------------------------------------------------
  def imageHasPhotometryModel(imageName: String) = {
    val docSeq =
      synchronized(collection
        .find(equal(ObjectDB_Item.COL_NAME_ID, imageName))
        .projection(
          fields(include(COL_NAME_PHOTOMETRY_MODEL_SEQ)
                 , excludeId()))
        .results())
    if (docSeq.isEmpty) false
    else !docSeq.head(ObjectDB_Item.COL_NAME_PHOTOMETRY_MODEL_SEQ).asArray().isEmpty
  }
  //-------------------------------------------------------------------------
  def addAstrometricInfo(imageName:String): Unit = {
    synchronized(collection
      .find(equal(ObjectDB_Item.COL_NAME_ID, imageName))
      .projection(include(ObjectDB_Item.COL_NAME_ID
                         , ObjectDB_Item.COL_NAME_FILTER
                         , ObjectDB_Item.COL_NAME_TELESCOPE
                         , ObjectDB_Item.COL_NAME_MIN_APERTURE_DATABASE
                         , ObjectDB_Item.COL_NAME_ANNULAR_APERTURE_DATABASE
                         , ObjectDB_Item.COL_NAME_MAX_APERTURE_DATABASE
                  ))
      .results()
      .map { doc =>
        ObjectPhotInfo.addToStorage(
          ObjectPhotInfo(
            doc(ObjectDB_Item.COL_NAME_ID).asString().getValue
            , PhotometricFilter(doc(ObjectDB_Item.COL_NAME_FILTER).asString().getValue)
            , doc(ObjectDB_Item.COL_NAME_TELESCOPE).asString().getValue
            , doc(ObjectDB_Item.COL_NAME_MIN_APERTURE_DATABASE).asDouble().getValue
            , doc(ObjectDB_Item.COL_NAME_ANNULAR_APERTURE_DATABASE).asDouble().getValue
            , doc(ObjectDB_Item.COL_NAME_MAX_APERTURE_DATABASE).asDouble().getValue)
        )
      })
  }
  //-------------------------------------------------------------------------
  def getNameSeq(limitResult: Option[Int] = None) =
    synchronized(collection
      .find(Document())
      .projection(include(ObjectDB_Item.COL_NAME_ID))
      .limit(limitResult.getOrElse(0))
      .results()
      .map { doc =>
        doc(ObjectDB_Item.COL_NAME_ID).asString().getValue
      })
  //-------------------------------------------------------------------------
  //(name,filter,telescope)
  def getFilterAndTelescopeMap(limitResult: Option[Int] = None) =
    synchronized(collection
      .find(Document())
      .projection(include(ObjectDB_Item.COL_NAME_ID, ObjectDB_Item.COL_NAME_FILTER, ObjectDB_Item.COL_NAME_TELESCOPE))
      .limit(limitResult.getOrElse(0))
      .results()
      .map { doc =>
        (doc(ObjectDB_Item.COL_NAME_ID).asString().getValue
          , (doc(ObjectDB_Item.COL_NAME_FILTER).asString().getValue
            ,doc(ObjectDB_Item.COL_NAME_TELESCOPE).asString().getValue))
      }.toMap)
  //-------------------------------------------------------------------------
  def getNameSeqGroupByFilterMap(imageNameMap: Map[String,String]) = {
    val seq =
      synchronized(collection
        .find(Document())
        .projection(include(ObjectDB_Item.COL_NAME_ID, ObjectDB_Item.COL_NAME_FILTER))
        .results())
        .flatMap { doc =>
          val imageName = doc(ObjectDB_Item.COL_NAME_ID).asString().getValue
          if (!imageNameMap.contains(imageName)) None
          else
            Some((imageName
                  , doc(ObjectDB_Item.COL_NAME_FILTER).asString().getValue))
       }

    val filterSeq = (seq map (_._2)).toSet
    filterSeq.map { filter => (filter,  seq filter (_._2 == filter) map (_._1)) }.toMap
  }
  //-------------------------------------------------------------------------
  def get(imageName: String) =
    synchronized(collWithRegistry
      .find(equal(ObjectDB_Item.COL_NAME_ID, imageName))
      .results())
      .head
  //-------------------------------------------------------------------------
  //(telescope, exposure time, observing time, filter)
  def getBasicInfo(imageName: String) = {

    val doc =
      synchronized(
        collection
          .find(equal(ObjectDB_Item.COL_NAME_ID, imageName))
          .projection(include(ObjectDB_Item.COL_NAME_TELESCOPE
                             , ObjectDB_Item.COL_NAME_OBSERVING_TIME
                             , normalizeColNameForDatabase(ObjectDB_Item.COL_NAME_EXPOSURE_TIME)
                             , ObjectDB_Item.COL_NAME_FILTER
                             ))
          .results().head)

     ( doc(ObjectDB_Item.COL_NAME_TELESCOPE).asString().getValue
     , doc(normalizeColNameForDatabase(ObjectDB_Item.COL_NAME_OBSERVING_TIME)).asString().getValue
     , doc(normalizeColNameForDatabase(ObjectDB_Item.COL_NAME_EXPOSURE_TIME)).asDouble().getValue
     , doc(normalizeColNameForDatabase(ObjectDB_Item.COL_NAME_FILTER)).asString().getValue
     )
  }
  //-------------------------------------------------------------------------
  def getTelescope(imageName: String) = {
    val doc =
      synchronized(
        collection
          .find(equal(ObjectDB_Item.COL_NAME_ID, imageName))
          .projection(fields(include(ObjectDB_Item.COL_NAME_TELESCOPE)
                      , excludeId()))
          .results().head)
    doc(ObjectDB_Item.COL_NAME_TELESCOPE).asString().getValue
  }
  //-------------------------------------------------------------------------
  def getFilter(imageName: String) = {
    val doc =
      synchronized(collection
                     .find(equal(ObjectDB_Item.COL_NAME_ID, imageName))
                     .projection(fields(include(ObjectDB_Item.COL_NAME_FILTER)
                                , excludeId()))
                     .results().head)
    doc(ObjectDB_Item.COL_NAME_FILTER).asString().getValue
  }

  //-------------------------------------------------------------------------
  def getFocusSourceSeq(imageName: String) = {

    val filterStatement = equal(ObjectDB_Item.COL_NAME_ID, imageName)
    val qualifiedMPO_ColNameSeq = SourceDB_Item.getQualifiedName(ObjectDB_Item.COL_NAME_SOURCE_SEQ)
    val mpcID_QualifedName = ObjectDB_Item.COL_NAME_SOURCE_SEQ + "." + SourceDB_Item.COL_NAME_FOCUS_ID

    val projectionStatementBeforeUnwind =
      fields(include(ObjectDB_Item.COL_NAME_SOURCE_SEQ), excludeId())

    val projectionStatementAfterUnwind =
      fields(include(qualifiedMPO_ColNameSeq: _*))

    synchronized(collection.aggregate(
      List(
        filter(filterStatement)
        , project(projectionStatementBeforeUnwind)
        , unwind("$" + ObjectDB_Item.COL_NAME_SOURCE_SEQ)
        , filter(notEqual(mpcID_QualifedName, NONE_FOCUS_OBJECT)) //it is an mpo
        , project(projectionStatementAfterUnwind)
      ))
      .results())
      .map { doc =>
        SourceDB_Item(doc.last._2.asInstanceOf[BsonDocument])
      }.distinct
  }
  //-------------------------------------------------------------------------
  def getSourceSeq(imageName: String): Array[SourceDB_Item] =
    synchronized(collWithRegistry
      .find(equal(ObjectDB_Item.COL_NAME_ID, imageName))
      .results()
      .map(_.source_seq).flatten.toArray)
  //-------------------------------------------------------------------------
  def generateCsvImageMetadata(csvFileName: String) = {
    val csvFile = CsvWrite(csvFileName, ObjectDB_Item.headerRow, appendWrite = MyFile.fileExist(csvFileName))
    val imageCatalogSeq = collWithRegistry.find(Document()).results()
    imageCatalogSeq map { imageCatalog => csvFile.append(imageCatalog.toRow())}
    csvFile.close()
  }
  //-------------------------------------------------------------------------
  def generateCsvSourceSeq(outputDir: String): Unit = {
    val oDir = Path.resetDirectory(outputDir)
    val imageNameSeq = getNameSeq()
    imageNameSeq.foreach { imageName =>
      val csvFileName = oDir + imageName +  ".csv"
      val csvFile = CsvWrite(csvFileName
                            , SourceDB_Item.headerRow
                            , appendWrite = MyFile.fileExist(csvFileName))
      val sourceSeq = getSourceSeq(imageName)
      sourceSeq.foreach { source => csvFile.append(source.toRow()) }
      csvFile.close(verbose = false)
    }
  }
  //-------------------------------------------------------------------------
  def getPhotometricModelSeq(imageName: String) =
    synchronized(collWithRegistry
      .find(equal(ObjectDB_Item.COL_NAME_ID, imageName))
      .results()
      .map(_.photometry_model_seq))
  //-------------------------------------------------------------------------
  //map(imageName(map(catalogName,Map(filter,fit)))
  def getCatalogPhotometricModelMap(imageNameSeq: Array[String]): ImagePhotometricMap  =
    imageNameSeq.flatMap {imageName=>
      val photometricModel = getCatalogPhotometricModelMap(imageName)
      if (photometricModel.isEmpty) None
      else Some((imageName,photometricModel))
    }.toMap
  //-------------------------------------------------------------------------
  //map(catalogName,Map(filter,fit))
  def getCatalogPhotometricModelMap(imageName: String) = {

    val photometricModelSeq = getPhotometricModelSeq(imageName).map { _.map {
      item =>  (item.catalogName
                , item.filterName
                , item.getCurveFitPolynomy())
      }
    }.flatten

    val catalogNameSeq = (photometricModelSeq map (_._1)).toSet
    (for (catalogName <- catalogNameSeq) yield {
      (catalogName,
          photometricModelSeq.filter(_._1 == catalogName).map { case (_, filterName, poly) =>
          (filterName, poly)
        }.toMap)
    }).toMap
  }
  //-------------------------------------------------------------------------
  def getPartitionByFocusImageFilterMap(imageNameSeq: Seq[String]
                                        , vr: Double
                                        , isStar: Boolean
                                        , spkVersion: Option[String] = None): (ImageSourceFocusMap,ImageByFilterMap) = {
    //-------------------------------------------------------------------------
    val sourceFocusMap = new ConcurrentHashMap[String,Seq[(SourceDB_Item, ImageFocusDB_Item)]]()
    val imageByFilterMap = new ConcurrentHashMap[String,ArrayBuffer[String]]()
    //-------------------------------------------------------------------------
    class MyParallelTask(seq: Array[String]) extends ParallelTask[String](
      seq
      , threadCount = CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , message = Some("getting image source focus")) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(imageName: String): Unit = {
        Try {
          if (imageName.trim.isEmpty || imageName == null) return
          val imageSourceSeq = getFocusSourceSeq(imageName)
          val filter = getFilter(imageName)
          val (telescope, observingTime, exposureTime, _)  = getBasicInfo(imageName)

          if (imageByFilterMap.get(filter) == null)
            imageByFilterMap.put(filter,ArrayBuffer[String]())
          imageByFilterMap.get(filter) += imageName

          val focusItemSeq = imageSourceSeq.map { sourceFocus =>
            (sourceFocus
             , ImageFocusDB_Item(imageName
                                   , sourceFocus
                                   , telescope
                                   , observingTime
                                   , exposureTime
                                   , vr
                                   , isStar
                                   , spkVersion))
            }
          sourceFocusMap.put(imageName, focusItemSeq)
        }
        match {
          case Success(_) =>
          case Failure(e: Exception) =>
            exception(e, s"Image: '$imageName' error getting the image source focus " + e.toString)
        }
      }
      //-----------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    new MyParallelTask(imageNameSeq.toArray)
    (sourceFocusMap.asScala.toMap,imageByFilterMap.asScala.toMap)
  }
  //-------------------------------------------------------------------------
  def getImageSeq() = {
    synchronized(collWithRegistry
      .find(Document())
      .results())
  }
  //-------------------------------------------------------------------------
  def getImageArea() = {
    synchronized(collWithRegistry
      .find(Document())
      .results()
      .map { o =>
        (o._id  //image_name
         , Point2D_Double(o.ra_min, o.ra_max)
         , Point2D_Double(o.dec_min, o.dec_max))
      })
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file ObjectCatalog.scala
//=============================================================================