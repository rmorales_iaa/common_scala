/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  17/Jan/2023 
 * Time:  13h:08m
 * Description: None
 */
//=============================================================================
package com.common.image.catalog
//=============================================================================
import com.common.image.photometry.absolute.photometricModel.PhotometricFilter
//=============================================================================
object ObjectPhotInfo {
  //---------------------------------------------------------------------------
  private val map = scala.collection.mutable.Map[String,ObjectPhotInfo]()
  //---------------------------------------------------------------------------
  def addToStorage(info: ObjectPhotInfo) = map(info.imageName) = info
  //---------------------------------------------------------------------------
  def get(imageName: String) = map(imageName)
  //---------------------------------------------------------------------------
  def contains(imageName: String) = map.contains(imageName)
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
case class ObjectPhotInfo(imageName: String
                          , filter: PhotometricFilter
                          , telescopeName: String
                          , minAperture: Double
                          , annularAperture: Double
                          , maxAperture: Double)
//=============================================================================
//End of file ObjectPhotInfo.scala
//=============================================================================
