/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  27/Mar/2023
 * Time:  20h:02m
 * Description: None
 */
//=============================================================================
package com.common.image.objectPosition
//=============================================================================
case class ObjectPosition(ra: Double,dec: Double,pmra: Double,pmDec: Double, epoch: Double)
//=============================================================================
//End of file ObjectPosition.scala
//=============================================================================
