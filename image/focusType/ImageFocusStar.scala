/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  01/Feb/2023
 * Time:  09h:58m
 * Description: None
 */
//=============================================================================
package com.common.image.focusType
//=============================================================================
import com.common.coordinate.conversion.Conversion
import com.common.geometry.point.Point2D_Double
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.focusType.ImageFocusTrait.{getComposedObjectName, getNewID}
import com.common.util.string.MyString
import com.common.util.time.Time
//=============================================================================
//=============================================================================
object ImageFocusStar {
  //---------------------------------------------------------------------------
  def apply(name: String): ImageFocusStar =
    ImageFocusStar(name
      , getComposedObjectName(name, isStar = true)
      , raDec = Point2D_Double.POINT_INVALID
      , pmRaDec = Point2D_Double.POINT_INVALID
      , referenceEpoch = -1
      , source = null)
  //---------------------------------------------------------------------------
  def apply(name: String
            , raDec: Point2D_Double //decial degree
            , pmRaDec: Point2D_Double //decial degree
            , referenceEpoch: Double): ImageFocusStar =
    ImageFocusStar(name
                   , getComposedObjectName(name, isStar = true)
                   , raDec
                   , pmRaDec
                   , referenceEpoch
                   , source = null)
  //---------------------------------------------------------------------------
  def apply(imageFocus: ImageFocusStar
            , source: Segment2D): ImageFocusStar =
    ImageFocusStar(imageFocus.name
                   , imageFocus.composedName
                   , imageFocus.raDec
                   , imageFocus.pmRaDec
                   , imageFocus.referenceEpoch
                   , source)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class ImageFocusStar(name: String
                          , composedName: String
                          , raDec: Point2D_Double //decial degree
                          , pmRaDec: Point2D_Double //decial degree
                          , referenceEpoch: Double //year
                          , source: Segment2D
                          ) extends ImageFocusTrait {
  //---------------------------------------------------------------------------
  val isStar = true
  val isNone = false
  val id = getNewID
  //---------------------------------------------------------------------------
  def getRaAsHMS(divider:String = ":") = Conversion.DD_to_HMS_WithDivider(raDec.x, divider)
  //---------------------------------------------------------------------------
  def getDecAsDMS(divider:String = ":") = Conversion.DD_to_DMS_WithDivider(raDec.y, divider)
  //---------------------------------------------------------------------------
  def getEstPosAtTime(observingDate: String): Point2D_Double =
    Conversion.properMotion(
        observingDate
      , raDec
      , referenceEpoch
      , pmRaDec)
  //---------------------------------------------------------------------------
  def getEstPosAtTime(observingDate: Double): Point2D_Double =
    Conversion.properMotion(
      Time.fromJulian(observingDate).toString
      , raDec
      , referenceEpoch
      , pmRaDec)
  //---------------------------------------------------------------------------
  def print(leftSideCharSize: Int ) = {
    val a = MyString.rightPadding("\tstar name", leftSideCharSize)
    val b = MyString.rightPadding("\tstar composed name", leftSideCharSize)
    val c = MyString.rightPadding("\tright ascension", leftSideCharSize)
    val d = MyString.rightPadding("\tdeclination", leftSideCharSize)
    val e = MyString.rightPadding("\tright ascension proper motion", leftSideCharSize)
    val f = MyString.rightPadding("\tdeclination proper motion", leftSideCharSize)
    val g = MyString.rightPadding("\treference epoch", leftSideCharSize)

    info(s"$a: '$name'")
    info(s"$b: '$composedName'")
    if (!raDec.x.isNaN) {
      info(s"$c: '${raDec.x}' (dec deg) ${Conversion.DD_to_HMS_WithDivider(raDec.x,":")}' (hms)")
      info(s"$d: '${raDec.y}' (dec deg) ${Conversion.DD_to_DMS_WithDivider(raDec.y,":")}' (dms)")
      info(s"$e: '${pmRaDec.x}' mas/year")
      info(s"$f: '${pmRaDec.y}' mas/year")
      info(s"$g: '$referenceEpoch' year")
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ImageFocusStar.scala
//=============================================================================
