
/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  01/Feb/2023
 * Time:  09h:57m
 * Description: None
 */
//=============================================================================
package com.common.image.focusType
//=============================================================================
import com.common.geometry.segment.segment2D.Segment2D
import com.common.logger.MyLogger
//=============================================================================
import java.util.concurrent.atomic.AtomicLong
//=============================================================================
//=============================================================================
object ImageFocusTrait {
  //---------------------------------------------------------------------------
  private val id = new AtomicLong(-1)
  //---------------------------------------------------------------------------
  def getNewID: Long = id.addAndGet(1)
  //---------------------------------------------------------------------------
  def getNormalizedName(s: String) = {

    s.trim
      .replaceAll(" +", " ")
      .replaceAll(" ", "_")
      .replaceAll("/", "_")
      .trim
  }
  //---------------------------------------------------------------------------
  def getComposedObjectName(objetName: String
                            , isStar: Boolean = false
                            , blindSearch: Boolean = false
                            , spkID: Option[Long] = None) = {
    val oName = objetName
      .toLowerCase()
      .trim.replaceAll(" +", " ")
      .replaceAll(" ","_")
    val s = if (blindSearch) "mpo_blind_search"
    else {
      if (isStar) "star_" + oName
      else "mpo_" + spkID.get + "_" + oName
    }
    getNormalizedName(s)
  }
  //---------------------------------------------------------------------------
  def getCsvHeader(sep: String) =
    "name" + sep +
      "composedName" + sep +
      "isStar" + sep +
      "id"
  //---------------------------------------------------------------------------
}
//=============================================================================
trait ImageFocusTrait extends MyLogger {
  //---------------------------------------------------------------------------
  val name: String
  val composedName: String
  val id: Long
  val source: Segment2D

  val isStar: Boolean
  val isNone: Boolean
  //---------------------------------------------------------------------------
  def getAsCsvLine(sep: String) =
    name + sep +
    composedName + sep +
    isStar + sep +
    id + sep
  //---------------------------------------------------------------------------
  def getID = composedName
  //---------------------------------------------------------------------------
  def getDebuggingID = composedName +  "_debug"
  //---------------------------------------------------------------------------
  def getNormalizedName() = ImageFocusTrait.getNormalizedName(name)
  //---------------------------------------------------------------------------
  def print(leftSideCharSize: Int ) : Unit
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ImageFocusType.scala
//=============================================================================
