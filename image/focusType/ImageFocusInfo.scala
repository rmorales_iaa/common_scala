/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  18/Dec/2021
 * Time:  10h:37m
 * Description: None
 */
//=============================================================================
package com.common.image.focusType
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.geometry.segment.segment2D.Segment2D
import com.common.logger.MyLogger
import com.common.util.file.MyFile
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import java.util.concurrent.ConcurrentLinkedQueue
//=============================================================================
object ImageFocusInfo extends MyLogger {
  //---------------------------------------------------------------------------
  final val STATUS_OBJET_FOUND                               = "Object found"
  final val STATUS_NONE_SOURCE_FOUND                         = "None source found at expected position"
  final val STATUS_MULTIPLE_SOURCE_FOUND                     = "Multiple sources found at same search area estimated by the orbit, continuing only with the closest one"
  final val STATUS_ERROR_GETING_EPEHEMERIS                   = "Error getting the ephemeris"
  final val STATUS_CATALOG_SOURCE_MATCHES_FOCUS              = "At least one source in the catalog matches the focus position"
  final val STATUS_NONE_SUG_GAIA_GROUP                       = "None sub-gaia group found"
  final val STATUS_NAN_APERTURE                              = "Has one of its apertures (min, anular or max) as value NaN"
  final val STATUS_APERTURE_ERROR                            = "Error calculating the aperture"
  final val STATUS_NO_CATALOG_PHOTOMETRIC_SOURCES            = "Not enough catalog photometric sources"
  final val STATUS_NO_SOURCES_IN_THE_CATALOG                 = "No valid GAIA sources in the catalog to create the photometry"
  final val STATUS_NO_OBJECT_FOUND_IN_DETECTION              = "No object found in detection"
  final val STATUS_NO_VALID_FOCUS_FOUND                      = "No valid focus found"
  //---------------------------------------------------------------------------
  private val sep ="\t"
  private val CSV_HEADER =
    s"image_name$sep" +
    s"status$sep" +
    s"telescope$sep" +
    s"filter$sep" +
    s"exposure_time$sep" +
    s"observing_timestmap_mid_point$sep" +
    s"background$sep" +
    s"background_rms$sep" +
    s"sigma_multipler$sep" +
    Segment2D.getCsvLineHeader(sep)
  //---------------------------------------------------------------------------
  def detectionSummary(csvFileName: String
                       , imageFocusInfoStorage: (String,ConcurrentLinkedQueue[ImageFocusInfo])
                       , drop: Boolean) : Unit = {

    val queue = imageFocusInfoStorage._2
    if (queue.isEmpty) {
      info("No objects to save")
      return
    }
    info(s"Saving photometry focused on mpo:${imageFocusInfoStorage._1} image count:${imageFocusInfoStorage._2.size()}")
    synchronized {
      val append = MyFile.fileExist(csvFileName)
      val bw = new BufferedWriter(new FileWriter(new File(csvFileName), append))
      if (drop || !append) bw.write(CSV_HEADER + "\n")
      while (!queue.isEmpty) {
        val afi = queue.poll()
        val source = afi.getSource()
        bw.write(afi.imageName + sep)
        bw.write(afi.getStatus() + sep)
        bw.write(afi.telescope + sep)
        bw.write(afi.filter + sep)
        bw.write(afi.exposureTime + sep)
        bw.write(afi.observingTimeStampMidPoint + sep)
        bw.write(afi.background + sep)
        bw.write(afi.backgroundRms + sep)
        bw.write(afi.sigmaMultipler + sep)
        if (source != null) bw.write(source.getAsCsvLine(sep))
        bw.write("\n")
      }
      bw.close()
    }
  }
  //---------------------------------------------------------------------------
  def findFocusInfo(imageName: String, queue: ConcurrentLinkedQueue[ImageFocusInfo]): Option[ImageFocusInfo]= {
    val ite = queue.iterator()
    while(ite.hasNext) {
       val afi = ite.next()
      if (afi.imageName == imageName) return Some(afi)
    }
    None
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class ImageFocusInfo(imageName: String
                          , focus: ImageFocusTrait
                          , expectedPosRaDec: Point2D_Double
                          , expectedPosPix: Point2D_Double
                          , telescope: String
                          , filter: String
                          , observingTimeStampMidPoint: String
                          , exposureTime: Double
                          , background: Double
                          , backgroundRms: Double
                          , sigmaMultipler: Double
                          , searchRadiusRa: Double
                          , searchRadiusDec: Double) {
  //---------------------------------------------------------------------------
  private var status = ""
  private var source : Segment2D = null
  //---------------------------------------------------------------------------
  val isStar = focus.isStar
  //---------------------------------------------------------------------------
  def updateStatus(s: String) = {
    if (status.isEmpty) status = s
    else status = status + ";" + s
  }
  //---------------------------------------------------------------------------
  def updateSource(s: Segment2D) = source = s
  //---------------------------------------------------------------------------
  def getStatus() = status
  //---------------------------------------------------------------------------
  def getSource() = source
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ImageFocusInfo.scala
//=============================================================================
