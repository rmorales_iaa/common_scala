/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  01/Feb/2023
 * Time:  09h:58m
 * Description: None
 */
//=============================================================================
package com.common.image.focusType
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.focusType.ImageFocusTrait.{getComposedObjectName, getNewID}
import com.common.util.string.MyString
//=============================================================================
//=============================================================================
object ImageFocusNone {
  //---------------------------------------------------------------------------
  def apply(name: String): ImageFocusNone =
    ImageFocusNone(name
      , getComposedObjectName(name, blindSearch = true)
      , raDec = Point2D_Double.POINT_INVALID
      , pmRaDec = Point2D_Double.POINT_INVALID
      , referenceEpoch = -1
      , source = null)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class ImageFocusNone(name: String
                          , composedName: String
                          , raDec: Point2D_Double //decial degree
                          , pmRaDec: Point2D_Double //decial degree
                          , referenceEpoch: Double //year
                          , source: Segment2D
                         ) extends ImageFocusTrait {
  //---------------------------------------------------------------------------
  val isStar = false
  val isNone = true
  val id = getNewID
  //---------------------------------------------------------------------------
  def print(leftSideCharSize: Int ) = {
    val a = MyString.rightPadding("\tobject name", leftSideCharSize)
    val b = MyString.rightPadding("\tobject composed name", leftSideCharSize)

    info(s"$a: '$name'")
    info(s"$b: '$composedName'")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ImageFocusNone.scala
//=============================================================================
