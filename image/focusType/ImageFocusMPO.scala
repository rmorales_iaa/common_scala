/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  01/Feb/2023
 * Time:  09h:58m
 * Description: mpo: minor planet object
 */
//=============================================================================
package com.common.image.focusType
//=============================================================================
import com.common.coordinate.conversion.Conversion
import com.common.database.mongoDB.spiceSPK.{SpiceSPK_DB, SpiceSPK_Source}
import com.common.estimator.skyPosition.EstimatedSkyPosition
import com.common.geometry.point.Point2D_Double
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.focusType.ImageFocusTrait.getComposedObjectName
import com.common.image.myImage.MyImage
import com.common.jpl.Spice.loadKernel
import com.common.jpl.horizons.Horizons
import com.common.jpl.horizons.db.smallBody.SmallBodyDB
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.path.Path
import com.common.util.string.MyString
import com.common.util.util.Util
//=============================================================================
import java.time.LocalDateTime
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object ImageFocusMPO extends MyLogger {
  //---------------------------------------------------------------------------
  private val spkFileMap = buildMap()
  private val horizonsDB = Horizons()
  //---------------------------------------------------------------------------
  def apply(imageFocus: ImageFocusMPO
            , source: Segment2D): ImageFocusMPO =
    ImageFocusMPO(imageFocus.name
                  , imageFocus.composedName
                  , imageFocus.spiceSpk
                  , source)
  //---------------------------------------------------------------------------
  private def buildImageFocusWithID(_id: String
                                    , source: Segment2D): Option[ImageFocusMPO] = {

    val id = if (!Util.isLong(_id)) return None else _id.toLong

    val horizonsEntry =
      horizonsDB.getBySpk_ID(id).getOrElse(horizonsDB.getByMPC_ID(id).getOrElse(return None))

    val spiceSPK = SpiceSPK_Source(
      _id = "-1"
      , spkID = horizonsEntry.spkId
      , mpcID = horizonsEntry.mpc_id
      , source =""
      , version = ""
      , mainDesignation = horizonsEntry.name
      , alternativeDesignation = horizonsEntry.alternateNameSeq.mkString("{",",","}")
      , date = LocalDateTime.now()
      , byteSize = 0
      , byteSeq = Array[Byte]()
    )
    Some(ImageFocusMPO(
      horizonsEntry.name
      , getComposedObjectName(horizonsEntry.name, isStar = false, spkID = Some(horizonsEntry.spkId))
      , spiceSPK
      , source
      , storedInDB = false))
  }
  //---------------------------------------------------------------------------
  def build(id: String
            , source: Segment2D = null
            , verbose: Boolean = true
            , allowSPK_NotStoredInDB: Boolean = false): Option[ImageFocusMPO] = {
    val spkID = SpiceSPK_DB.getSpkID(id).getOrElse {
      if (allowSPK_NotStoredInDB) {
        val r = buildImageFocusWithID(id, source)
        if (verbose && r.isEmpty) error(s"Can not find the SPK or MPC with id:'$id'")
        return r
      }
      return None
    }
    val db = SpiceSPK_DB(collectionName = spkID.toString)
    val spiceSPK_Seq = db.find()
    db.close()

    if (spiceSPK_Seq.size != 1) {
      if (allowSPK_NotStoredInDB) return buildImageFocusWithID(spkID.toString, source)
      else {
        if (verbose) error(s"Error getting data from SPICE SPK database for id:'$id'")
        return None
      }
    }
    val spiceSPK = spiceSPK_Seq.head

    //avoid weird chars in "229762 G!kun||'homdima (2007 UK126)" and "|=Kagara" (2005 EF298)
    val designation = if (spiceSPK.spkID == 20229762) spiceSPK.alternativeDesignation.split(",").head.drop(1).replaceAll(" ","_")
                      else
                         if (spiceSPK.spkID == 20469705) spiceSPK.alternativeDesignation.split(",").head.drop(1).replaceAll(" ","_")
                         else spiceSPK.mainDesignation
    val r = ImageFocusMPO(id
                       , getComposedObjectName(designation, isStar = false, spkID = Some(spiceSPK.spkID))
                       , spiceSPK
                       , source)
    Some(r)
  }
  //---------------------------------------------------------------------------
  private def buildMap() = {
    Path.createDirectoryIfNotExist(SmallBodyDB.DEFAULT_DOWNLOAD_DIRECTORY)
    val map = scala.collection.mutable.Map[String,String]()
    Path.getSortedFileList(SmallBodyDB.DEFAULT_DOWNLOAD_DIRECTORY, SpiceSPK_DB.DEFAULT_BSP_FILE_EXTENSION).foreach { f=>
      val path = f.getAbsolutePath
      map(MyFile.removeFileExtension(path)) = path
    }
    map
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.common.image.focusType.ImageFocusMPO._
case class ImageFocusMPO(name: String
                         , composedName: String
                         , spiceSpk: SpiceSPK_Source
                         , source: Segment2D = null
                         , storedInDB: Boolean = true) extends ImageFocusTrait {
  //---------------------------------------------------------------------------
  val isStar = false
  val isNone = false
  val id = if(source == null) -1 else source.id
  //---------------------------------------------------------------------------
  private var expectedRaDec = Point2D_Double.POINT_ZERO
  //---------------------------------------------------------------------------
  def setExpectedRaDec(p: Point2D_Double) = expectedRaDec = p
  //---------------------------------------------------------------------------
  def getExpectedRaDec(p: Point2D_Double) = expectedRaDec
  //---------------------------------------------------------------------------
  def getSpkFilePath(spkVersion: Option[String] = None
                     , verbose: Boolean = false): String = {
    val spkUniqueID = spiceSpk.getUniqueID()
    if (spkFileMap.contains(spkUniqueID)) {
      if (verbose) warning(s"Loading existing SPICE SPK:'$spkUniqueID' ${if (spkVersion.isDefined) s" version:${spkVersion.get}" else ""}")
      spkFileMap(spkUniqueID)
    }
    else {
      if (verbose) warning(s"Retrieving from database the SPICE SPK:'$spkUniqueID' ${if (spkVersion.isDefined) s" version:${spkVersion.get}" else ""}")
      val fileNameSeq = ArrayBuffer[String]()
      SpiceSPK_DB.generate(spiceSpk.spkID.toString
        , SmallBodyDB.DEFAULT_DOWNLOAD_DIRECTORY
        , lastMoreRecent = None
        , spkVersion = spkVersion
        , Some(fileNameSeq))

      spkFileMap(spkUniqueID) = fileNameSeq.head
      fileNameSeq.head
    }
  }
  //---------------------------------------------------------------------------
  def print(leftSideCharSize: Int) = {
    val a = MyString.rightPadding("\tmpo name", leftSideCharSize)
    val b = MyString.rightPadding("\tmpo composed name", leftSideCharSize)
    val c = MyString.rightPadding("\tminor planet ID (mpcID)", leftSideCharSize)
    val d = MyString.rightPadding("\tspice kernel ID (spkID)", leftSideCharSize)
    val f = MyString.rightPadding("\tversion", leftSideCharSize)

    info(s"$a: '$name'")
    info(s"$b: '$composedName'")
    info(s"$c: '${spiceSpk.mpcID}'")
    info(s"$d: '${spiceSpk.spkID}'")
    info(s"$f: '${spiceSpk.version}'")
  }

  //---------------------------------------------------------------------------
  def getEstRaDecPosition(img: MyImage): Option[Point2D_Double] = {
    if (!loadKernel(getSpkFilePath(spkVersion = None), verbose = false)) return None
    Some(EstimatedSkyPosition.getPosAtImageWithFallBack(this, img))
  }
  //---------------------------------------------------------------------------
  //(ra,dec), (pixPosX,pixPosY)
  def getEstPosition(img: MyImage
                     , spkVersion: Option[String] = None
                     , verbose: Boolean = false
                     , splitInLinesDebugInfo: Boolean = false): Option[(Point2D_Double, Point2D_Double)] = {

    if(!loadKernel(getSpkFilePath(spkVersion),verbose = verbose)) return None
    val expectedPositionRaDec = EstimatedSkyPosition.getPosAtImageWithFallBack(this, img)

    val spkID = spiceSpk.spkID
    val expectedPositionPix = img.skyToPixSeq(expectedPositionRaDec.toArray()).head
    if (expectedPositionRaDec == Point2D_Double(-1, -1)) { //error getting the ephemeris
      error(s"Image: '${img.getRawName()}'. MPO '$spkID' ($composedName) has an invalid sky position. Possibly because kernel has no enough data")
      return None
    }
    if (!img.isIn(expectedPositionPix.toPoint2D())) {
      error(s"Image: '${img.getRawName()}'. MPO '$spkID' ($composedName) is not at the image. Expected pix (ra,dec)):'$expectedPositionRaDec' Expected pix position:'$expectedPositionPix'. Possible wcs fitting problem")
      return None
    }
    val expectedPositionRa = Conversion.DD_to_HMS_WithDivider(expectedPositionRaDec.x, ":")
    val expectedPositionDec = Conversion.DD_to_DMS_WithDivider(expectedPositionRaDec.y, ":")
    if (verbose) {
      val prefix =  if (splitInLinesDebugInfo) "\n\t" else ""
      info(s"Image: '${img.getRawName()}' mpo '$composedName'. " +
        s"${prefix}Expected position(ra,dec) " +
        s"$prefix(${f"${expectedPositionRaDec.x}%.6f"},${f"${expectedPositionRaDec.y}%.6f"}) (dec deg) " +
        s"$prefix$expectedPositionRa (ra:hms) " +
        s"$prefix$expectedPositionDec (dec:hms) " +
        s"${prefix}pix:(${f"${expectedPositionPix.x}%.3f"},${f"${expectedPositionPix.y}%.3f"})")
    }

    Some((expectedPositionRaDec, expectedPositionPix))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ImageFocusMPO.scala
//=============================================================================
