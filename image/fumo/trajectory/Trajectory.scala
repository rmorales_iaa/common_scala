/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Aug/2020
 * Time:  17h:53m
 * Description: None
 */
//=============================================================================
package com.common.image.fumo.trajectory
//=============================================================================
import com.common.geometry.matrix.matrix2D.Matrix2D
import com.common.geometry.point.Point2D_Double
import com.common.geometry.segment.segment2D.Segment2D
import com.common.logger.MyLogger
import com.common.image.fumo.Fumo._
import com.common.image.myImage.MyImage
import com.common.image.nsaImage.geometry.asterism.MatchAsterism.KnnTreeType
import com.common.image.nsaImage.geometry.point.Point2D

import java.awt.Graphics2D
import java.awt.geom.AffineTransform
import java.awt.image.BufferedImage
//=============================================================================
import java.io.BufferedWriter
import java.time.temporal.ChronoUnit
import scala.collection.mutable.ArrayBuffer
import java.util.concurrent.atomic.AtomicInteger
import java.time.LocalDateTime
import gif.AnimatedGifEncoder
//=============================================================================
//=============================================================================
object Trajectory extends MyLogger {
  //---------------------------------------------------------------------------
  private val idGenerator = new AtomicInteger(-1)
  //---------------------------------------------------------------------------
  private val sep = "\t"
  //---------------------------------------------------------------------------
  private final val TRAJECTORY_RESULT_CHECK_VALID                 = "valid"
  private final val TRAJECTORY_RESULT_CHECK_INVALID_SLOPE         = "invalid slope"
  private final val TRAJECTORY_RESULT_CHECK_INVALID_SPEED         = "invalid speed"
  private final val TRAJECTORY_RESULT_CHECK_INVALID_PIX_DISTANCE  = "invalid pix distance"
  //---------------------------------------------------------------------------
  final val TRAJECTORY_CSV_HEADER =
    s"trajectory_id$sep" +
      s"trajectory_validation$sep" +
      s"slope_pair$sep" +
      s"trajectory_speed_pair(pix/min)$sep" +
      s"pix_distance_pair$sep" +
      s"image_name$sep" +
      s"mid_observing_time$sep" +
      s"source_id$sep" +
      s"ra_dd$sep" +
      s"dec_dd$sep" +
      s"ra_HMS$sep" +
      s"dec_DMS$sep" +
      s"x_pix_$sep" +
      s"y_pix_$sep" +
      s"mpc_query_$sep" +
      "\n"
  //---------------------------------------------------------------------------
  def apply(other: Trajectory): Trajectory = {
    val t = Trajectory()
    t += other.getPointSeq
    t.setTrajectoryType(other.getTrajectoryType())
    t.setTrajectoryValidation(other.getTrajectoryValidation())
    t.setSlopeSeq(other.getSlopeSeq().toArray)
    t.setSpeedSeq(other.getSpeedSeq().toArray)
    t.setPixDistanceSeq(other.getPixDistanceSeq().toArray)
    t
  }
  //---------------------------------------------------------------------------
  def apply(p: TrajectoryPoint): Trajectory = {
    val t = Trajectory()
    t += p
    t
  }

  //---------------------------------------------------------------------------
  def apply(pSeq: Array[TrajectoryPoint]): Trajectory = {
    val t = Trajectory()
    t += pSeq
    t
  }
  //---------------------------------------------------------------------------
  def getNewID: Int = idGenerator.addAndGet(1)
  //---------------------------------------------------------------------------
  private def hasSameDirection(s: Seq[Long]): Boolean = {
    val direction = (s.sliding(2) map { t =>
      val diff = t.last - t.head
      if (diff >= 0) 1
      else if (diff < -1) -1
    }).toArray
    direction.forall(_ == 1) ||
    direction.forall(_ == -1)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
import Trajectory._
case class Trajectory(id : Int = Trajectory.getNewID) extends MyLogger {
  //--------------------------------------------------------------------------
  private var trajectoryType = ""
  private var trajectoryValidation = ""
  //--------------------------------------------------------------------------
  private val slopePairSeq = ArrayBuffer[Double]()
  private val speedPairSeq = ArrayBuffer[Double]()
  private val pixPairDistanceSeq = ArrayBuffer[Double]()
  //--------------------------------------------------------------------------
  private val storage = ArrayBuffer[TrajectoryPoint]()
  //--------------------------------------------------------------------------
  def getID() = {
    f"$id%06d" + ":" +
    f"${storage.length}%02d"
  }
  //---------------------------------------------------------------------------
  def getPointSeq = storage.toArray
  //---------------------------------------------------------------------------
  def length() = storage.length
  //---------------------------------------------------------------------------
  def +=(pointSeq: Array[TrajectoryPoint]) = storage ++= pointSeq
  //---------------------------------------------------------------------------
  def +=(p: TrajectoryPoint) = storage += p

  //---------------------------------------------------------------------------
  def removeLast() = storage.trimEnd(1)
  //---------------------------------------------------------------------------
  def canAddPoint(p: TrajectoryPoint
                  , midObservingDateTimeSeq: Array[LocalDateTime]): Boolean = {
    storage += p
    val r = isValidTrajectory(midObservingDateTimeSeq
                              , TRAJECTORY_MAX_ALLOWED_SLOPE_TOLERANCE
                              , TRAJECTORY_MAX_ALLOWED_SPEED_TOLERANCE
                              , TRAJECTORY_MAX_ALLOWED_PIX_DISTANCE)
    if (!r) removeLast
    r
  }
  //---------------------------------------------------------------------------
  private def hasSameDirection(): Boolean = {
    val centreSeq = storage.map(_.getCentroidPix).toArray
    //round the positions to allow margin in the matching. It margin comes from registration
    Trajectory.hasSameDirection(centreSeq map (p=> Math.round(p.x))) &&
    Trajectory.hasSameDirection(centreSeq map (p=> Math.round(p.y)))
  }
  //---------------------------------------------------------------------------
  private def shortDouble(d: Double) = f"$d%.3f"
  //---------------------------------------------------------------------------
  private def isValidTrajectory(midObservingDateTimeSeq: Array[LocalDateTime]
                                , slopeTolerance: Double
                                , speedTolerance: Double
                                , maxAllowedPixDistance: Int): Boolean = {

    if (storage.length < 3) return true

    if (!hasSameDirection()) return false

    //clear result
    trajectoryValidation = ""

    //check slope value. All of the trajectory points of the path must keep the slope within a tolerance
    //slope = tangent of the angle
    val _slopeSeq = (storage sliding 2 map (t => Math.abs(t(0).getCentroidRaDec.getSlope(t(1).getCentroidRaDec)))).toArray
    val slopeReference = _slopeSeq(0)
    val slopeDiff = _slopeSeq.drop(1) map (diff => Math.abs(slopeReference - diff))
    if (!(slopeDiff forall (_ <= slopeTolerance)))
      trajectoryValidation += TRAJECTORY_RESULT_CHECK_INVALID_SLOPE + s"<Slope diff:'${slopeDiff.map(shortDouble(_)).mkString("{",",","}")}' but max allowed tolerance:'${shortDouble(slopeTolerance)}'>"
    slopePairSeq.clear()
    slopePairSeq ++= _slopeSeq

    //check speed in pix/min
    val _speedSeq = ((storage zip midObservingDateTimeSeq).sliding(2) map { t =>
      val left = t.head._1
      val right = t.last._1

      val leftTime = t.head._2
      val rightTime = t.last._2

      val pixDistance= left.getCentroidPix.getDistance(right.getCentroidPix)
      val elapsedTimeMin = Math.abs(leftTime.until(rightTime, ChronoUnit.SECONDS)) / 60.0
      pixDistance / elapsedTimeMin //pix/min
    }).toArray

    val speedReference = _speedSeq(0)
    val speedDiff = _speedSeq.drop(1) map (diff => Math.abs(speedReference - diff))
    if (!(speedDiff forall (_ <= speedTolerance)))
      trajectoryValidation += TRAJECTORY_RESULT_CHECK_INVALID_SPEED + s" <Slope diff:'${speedDiff.map(shortDouble(_)).mkString("{",",","}")}' but max allowed tolerance:'${shortDouble(speedTolerance)}'>"
    speedPairSeq.clear()
    speedPairSeq ++= _speedSeq

    //check pixel distance
    val pixDistanceSeq = (storage sliding 2 map (t => t(0).getCentroidPix.getDistance(t(1).getCentroidPix))).toArray
    if (!(pixDistanceSeq forall (_ <= maxAllowedPixDistance)))
      trajectoryValidation += TRAJECTORY_RESULT_CHECK_INVALID_PIX_DISTANCE + s"<Pix distance diff:'${pixDistanceSeq.map(shortDouble(_)).mkString("{",",","}")}' but max allowed:'${shortDouble(maxAllowedPixDistance)}'>"
    pixPairDistanceSeq.clear()
    pixPairDistanceSeq ++= pixDistanceSeq

    //check if none error was found
    if (trajectoryValidation.isEmpty)
      trajectoryValidation = TRAJECTORY_RESULT_CHECK_VALID

    true
  }
  //---------------------------------------------------------------------------
  def setTrajectoryType(t:String) = trajectoryType = t
  //---------------------------------------------------------------------------
  def getTrajectoryType() = trajectoryType
  //---------------------------------------------------------------------------
  def setTrajectoryValidation(t: String) = trajectoryValidation = t

  //---------------------------------------------------------------------------
  def getTrajectoryValidation() = trajectoryValidation
  //---------------------------------------------------------------------------
  def isValid = trajectoryValidation == TRAJECTORY_RESULT_CHECK_VALID
  //---------------------------------------------------------------------------
  def getSlopeSeq() = slopePairSeq

  //---------------------------------------------------------------------------
  def setSlopeSeq(ss: Array[Double]) = slopePairSeq ++= ss

  //---------------------------------------------------------------------------
  def getSpeedSeq() = speedPairSeq

  //---------------------------------------------------------------------------
  def setSpeedSeq(ss: Array[Double]) = speedPairSeq ++= ss

  //---------------------------------------------------------------------------
  def getPixDistanceSeq() = pixPairDistanceSeq

  //---------------------------------------------------------------------------
  def setPixDistanceSeq(ss: Array[Double]) = pixPairDistanceSeq ++= ss
  //---------------------------------------------------------------------------
  def isEqual(other: Trajectory) =
    (getPointSeq.map(_.getCentroidPix) zip  other.getPointSeq.map(_.getCentroidPix)).forall { t=>
      val diff = (t._1 - t._2).abs()
      (diff.x <=  1) && (diff.y <=  1)
    }
  //---------------------------------------------------------------------------
  private def getMinMaxPixPos() = {
    val minMaxSeq = storage.map(_.getCentroidPix).toArray
    (Point2D_Double.getMin(minMaxSeq),Point2D_Double.getMax(minMaxSeq))
  }
  //---------------------------------------------------------------------------
  def save(imageNameSeq: Array[String]
           , midObservingTimeSeq: Array[String]
           , telescopeMap: Map[String,String]
           , bw: BufferedWriter) = {

    //assuming RML input order
    storage.zipWithIndex
      .foreach { case (tp, i) =>

      bw.write(id + sep)
      bw.write(trajectoryValidation + sep)
      if (i == 2) {
        bw.write(s"NaN$sep") //slope pair
        bw.write(s"NaN$sep") //speed pair
        bw.write(s"NaN$sep") //pix distance pair
      }
      else {
        bw.write(s"${slopePairSeq(i)}$sep") //slope
        bw.write(s"${speedPairSeq(i)}$sep") //speed
        bw.write(s"${pixPairDistanceSeq(i)}$sep") ///pix distance
      }
      bw.write(s"${imageNameSeq(i)}$sep") //image name
      bw.write(s"${midObservingTimeSeq(i)}$sep") //mid observing time
      tp.save(bw, midObservingTimeSeq(i), telescopeMap(imageNameSeq(i)), sep) //save trajectory point
      bw.write("\n")
    }
  }
  //-------------------------------------------------------------------------
  def saveGif(imageSeq: Array[MyImage]
              , gifFileName: String): Unit = {
    val extraBorderPixSize = Point2D_Double(TRAJECTORY_GIF_X_CROP_PIX_EXTRA_SIZE
                                            , TRAJECTORY_GIF_Y_CROP_PIX_EXTRA_SIZE)
    val (min, max) = getMinMaxPixPos
    val minPos = min.subtractMinZero(extraBorderPixSize)
    val maxPos = max + extraBorderPixSize
    val sourceSeq = storage.map(tp=> tp.source).toArray

    val biSeq = imageSeq.map { img=>
      val originalBi = img.getSubMatrixWithPaddingPixPos(
        Math.round(minPos.x).toInt,
        Math.round(minPos.y).toInt,
        Math.round(maxPos.x).toInt,
        Math.round(maxPos.y).toInt
      ).convertToBufferedImage(usezScale = true)

      Matrix2D.surroundWithRectanglePixPos(
        originalBi
        , sourceSeq
        , Math.round(minPos.inverse.x).toInt
        , Math.round(minPos.inverse.y).toInt
        , borderWidthSize = TRAJECTORY_GIF_SURROUND_REC_BORDER_WIDTH_SIZE
        , distanceToBorder = TRAJECTORY_GIF_SURROUND_REC_DISTANCE_TO_BORDER)

      // Invert the y-axis by flipping the image vertically
      val bi = new BufferedImage(originalBi.getWidth, originalBi.getHeight, originalBi.getType)
      val g2d: Graphics2D = bi.createGraphics()
      val at = AffineTransform.getScaleInstance(1, -1)
      at.translate(0, -originalBi.getHeight)
      g2d.drawRenderedImage(originalBi, at)
      g2d.dispose()

      bi
    }

    val gif = new AnimatedGifEncoder
    gif.start(gifFileName)
    gif.setDelay(TRAJECTORY_GIF_SPEED_MS)
    biSeq map (gif.addFrame(_))
    gif.finish
    info(s"Saved trajectory gif file: '$gifFileName'")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Trajectory.scala
//=============================================================================

