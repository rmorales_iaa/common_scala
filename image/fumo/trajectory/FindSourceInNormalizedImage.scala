/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  09/Jul/2024
 * Time:  10h:51m
 * Description: None
 */
package com.common.image.fumo.trajectory
//=============================================================================
import com.common.geometry.rectangle.RectangleDouble
import com.common.geometry.segment.segment2D.{Morphology, Segment2D}
import com.common.image.fumo.Fumo._
import com.common.util.path.Path
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object FindSourceInNormalizedImage {
  //---------------------------------------------------------------------------
  private var bw: BufferedWriter = null
  private var commonRec: RectangleDouble = null
  private var sep: String = ""
  //---------------------------------------------------------------------------
  private final val REASON_NOT_INSIDE_COMMON_VALID_RECTANGLE = "not inside common valid rectangle"
  private final val REASON_NOT_ENOUGH_PIX_COUNT              = "not enough pix count"
  private final val REASON_NOT_ENOUGH_PERCENTAGE_OF_PIXELS   = "not enough percentage of pixels sources inside rectangle hull"
  private final val REASON_NOT_VALID_ECCENTRICITY            = "not valid eccentricity"
  private final val REASON_DEFINED_AS_UNIQUE_COL             = "source defined as unique column"
  private final val REASON_DEFINED_AS_UNIQUE_ROW             = "source defined as unique row"
  //---------------------------------------------------------------------------
  private def saveDiscardedSource(imageName: String
                                  , source: Segment2D
                                  , morphology: Morphology
                                  , reason: String): Boolean = {
    synchronized {
      bw.write(
        imageName + sep +
        source.id + sep +
        reason + sep +
        source.getCentroid().x + sep +
        source.getCentroid().y + sep +
        source.getCentroidRaDec().x + sep +
        source.getCentroidRaDec().y + sep +
        source.getPixCount() + sep +
        morphology.eccentricity + sep +
        "\n"
      )
    }
    false
  }
  //---------------------------------------------------------------------------

  def init(csvFileName: String
           , _commonRec: RectangleDouble
           , _sep: String) = {
    commonRec = _commonRec
    sep = _sep
    bw = new BufferedWriter(new FileWriter(new File(csvFileName)))
    bw.write(s"image_name${sep}source_id${sep}reason${sep}centroid_x${sep}centroid_y${sep}centroid_ra${sep}centroid_dec${sep}pix_count${sep}eccentricity" + "\n")
  }
  //---------------------------------------------------------------------------
  def close() = bw.close()
  //---------------------------------------------------------------------------
  def validateSourceSeq(imageName:String
                        , source: Segment2D): Boolean = {

    val rawName = Path.getOnlyFilenameNoExtension(imageName)
    val centroidRaDec = source.getCentroidRaDec()

    //get source morphology
    val morphology = source.getMorphology()

    //check if it is inside common rec
    if (!commonRec.isIn(centroidRaDec))
      return saveDiscardedSource(rawName, source, morphology, REASON_NOT_INSIDE_COMMON_VALID_RECTANGLE + s"${commonRec.toStringShort}")

    //check pix count
    if (source.getPixCount() < SOURCE_DETECTION_MIN_PIX_COUNT)
      return saveDiscardedSource(rawName, source, morphology, REASON_NOT_ENOUGH_PIX_COUNT + s". min pix count:'$SOURCE_DETECTION_MIN_PIX_COUNT' but source has:'${source.getPixCount()}'" )

    //check percentage of pixels inside the rectangle hull
    if (morphology.pixelInPercentage < SOURCE_DETECTION_MIN_PIX_IN_PERCENTAGE)
      return saveDiscardedSource(rawName, source, morphology,REASON_NOT_ENOUGH_PERCENTAGE_OF_PIXELS + s". min percentage:'$SOURCE_DETECTION_MIN_PIX_IN_PERCENTAGE' source percentage:'${morphology.pixelInPercentage}'")

    //check eccentricity
    if (morphology.eccentricity.isNaN) return false
    if ((morphology.eccentricity < SOURCE_DETECTION_MIN_ALLOWED_ECCENTRICITY) ||
        (morphology.eccentricity > SOURCE_DETECTION_MAX_ALLOWED_ECCENTRICITY))
      return saveDiscardedSource(rawName, source, morphology,REASON_NOT_VALID_ECCENTRICITY + s". eccentricity range:[$SOURCE_DETECTION_MIN_ALLOWED_ECCENTRICITY,$SOURCE_DETECTION_MAX_ALLOWED_ECCENTRICITY] source eccentricity:'${morphology.eccentricity}'")

    //check form
    if (morphology.hasUniqueCol)
      return saveDiscardedSource(rawName, source, morphology,REASON_DEFINED_AS_UNIQUE_COL)

    if (morphology.hasUniqueRow)
      return saveDiscardedSource(rawName, source, morphology,REASON_DEFINED_AS_UNIQUE_ROW)
    true
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//=============================================================================
//End of file FindSourceInNormalizedImage.scala
//=============================================================================