/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/Jul/2024
 * Time:  15h:37m
 * Description: None
 */
package com.common.image.fumo.trajectory
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.geometry.rectangle.RectangleDouble
//=============================================================================
//=============================================================================
case class SimpleImage(name: String
                       , sourceMap: Map[Int, TrajectoryPoint]
                       , midObservingTime: String) {
  //-------------------------------------------------------------------------
  def getSortedSourceSeq =
    sourceMap.values.toArray.sortWith(_.getCentroidRaDec.x < _.getCentroidRaDec.x)
  //-------------------------------------------------------------------------
  def getDetectionRectangle = {
    val centroidSeq = sourceMap.values.map { tp=> tp.getCentroidPix}.toArray
    val min = Point2D_Double.getMin(centroidSeq)
    val max = Point2D_Double.getMax(centroidSeq)
    RectangleDouble(min,max)
  }
  //-------------------------------------------------------------------------
  def removeSourceNotInRectangle(rec: RectangleDouble) = {
    val filteredMap = sourceMap.filter { case (_,tp)=>
      rec.isIn(tp.getCentroidPix)
    }
    SimpleImage(name, filteredMap, midObservingTime)
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file SimpleImage.scala
//=============================================================================