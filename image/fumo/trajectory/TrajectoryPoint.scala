/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/Jul/2024
 * Time:  15h:36m
 * Description: None
 */
package com.common.image.fumo.trajectory
//=============================================================================
import com.common.coordinate.conversion.Conversion
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.mpo.asteroidChecker.MinorPlanetCenter
//=============================================================================
import java.io.BufferedWriter
//=============================================================================
//=============================================================================
case class TrajectoryPoint(source: Segment2D) {
  //-------------------------------------------------------------------------
  def getID = source.id
  //-------------------------------------------------------------------------
  def getCentroidRaDec = source.getCentroidRaDec()
  //-------------------------------------------------------------------------
  def getCentroidPix = source.getCentroid()
  //-------------------------------------------------------------------------
  def save(bw: BufferedWriter
           , observingDate: String
           , observatoryCode: String
           , sep:String) = {

    val centroidRaDec = getCentroidRaDec
    val ra = Conversion.DD_to_HMS_WithDivider(centroidRaDec.x)
    val dec = Conversion.DD_to_DMS_WithDivider(centroidRaDec.y, threeDecimalPlaces = false)

    val id = getID
    val mpcLine = MinorPlanetCenter.createQueryUnknownObjectSeq(
        raDecMap = Map(id ->(ra,dec))
      , observingDate = observingDate
      , observatoryCode = observatoryCode
    ).head._2

    val centroidPix = getCentroidPix
    bw.write(id + sep +
             centroidRaDec.x + sep +
             centroidRaDec.y + sep +
             Conversion.DD_to_HMS_WithDivider(centroidRaDec.x, divider = ":").mkString.trim + sep +
             Conversion.DD_to_DMS_WithDivider(centroidRaDec.y, divider = ":").mkString.trim + sep +
             centroidPix.x + sep +
             centroidPix.y + sep +
             mpcLine)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file TrajectoryPoint.scala
//=============================================================================