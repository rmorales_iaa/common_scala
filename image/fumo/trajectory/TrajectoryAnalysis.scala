/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  09/Jul/2024
 * Time:  11h:43m
 * Description: iterate thru all possible combinations of sources in 3 images.
    A valid trajectory for sequence of trajectory points (source centroids) a,b,c
    must be at images with the following distribution:
        right image (oldest one) => a and b
        middle image             => a and c
        left image  (newest one) => b and c

    A valid trajectory must comply with:
        i) Point a in right must exist in middle
        ii) Point b in right must exist in left
        iii) Point c in left  must exist in middle

     So, a valid trajectory must be found twice.
      1) The first one starting form a source of right image,left image and middle image (r,l,m) => (a,b,c)
      2) The first one starting form a source of middle image,right image and left image (m,r,l) => (a,b,c)
**/
//=============================================================================
package com.common.image.fumo.trajectory
//=============================================================================
import com.common.hardware.cpu.CPU
import com.common.image.mpo.asteroidChecker.AsteroidChecker
import com.common.image.myImage.MyImage
import com.common.image.telescope.Telescope
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
//=============================================================================
import java.io.BufferedWriter
import java.time.LocalDateTime
import java.io.{File, FileWriter}
import scala.util.{Try,Success,Failure}
import java.util.concurrent.ConcurrentLinkedQueue
import scala.collection.JavaConverters.collectionAsScalaIterableConverter
//=============================================================================
//=============================================================================
object TrajectoryAnalysis {
  //---------------------------------------------------------------------------
  private final val TRAJECTORY_TYPE_L: Char = 'L' //left
  private final val TRAJECTORY_TYPE_L_INDEX = 0

  private val TRAJECTORY_TYPE_M: Char       = 'M'  //middle
  private final val TRAJECTORY_TYPE_M_INDEX = 1

  private val TRAJECTORY_TYPE_R: Char       = 'R'  //right
  private final val TRAJECTORY_TYPE_R_INDEX = 2

  //time order
  private final val TRAJECTORY_TYPE_LMR = Array(TRAJECTORY_TYPE_L, TRAJECTORY_TYPE_M, TRAJECTORY_TYPE_R).mkString

  //used in trajectories analysis
  private final val TRAJECTORY_TYPE_RLM = Array(TRAJECTORY_TYPE_R, TRAJECTORY_TYPE_L, TRAJECTORY_TYPE_M).mkString
  private final val TRAJECTORY_TYPE_MRL = Array(TRAJECTORY_TYPE_M, TRAJECTORY_TYPE_R, TRAJECTORY_TYPE_L).mkString
  //---------------------------------------------------------------------------
  private class ProcessSourceSeq(aSourceSeq: Array[TrajectoryPoint]
                                 , bSourceSeq: Array[TrajectoryPoint]
                                 , cSourceSeq: Array[TrajectoryPoint]
                                 , trajectoryType: String
                                 , midObservingDateTimeSeq: Array[LocalDateTime]
                                 , trajectoryStorage: ConcurrentLinkedQueue[Trajectory]
                                ) extends ParallelTask[TrajectoryPoint](
    aSourceSeq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100
    , message = Some("calculating trajectories:" + trajectoryType)) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(aPoint: TrajectoryPoint) = {
      val trajectory = Trajectory(aPoint)
      bSourceSeq.foreach { bSource =>
        if (trajectory.canAddPoint(bSource, midObservingDateTimeSeq)) {
          cSourceSeq.foreach { cSource =>
            if (trajectory.canAddPoint(cSource, midObservingDateTimeSeq)) {
              val trajectoryToStore = Trajectory(trajectory) //store a copy
              trajectoryToStore.setTrajectoryType(trajectoryType)
              trajectoryStorage.add(trajectoryToStore)
              trajectory.removeLast //remove last cSource
            }
          }
          trajectory.removeLast //remove last bSource
        }
      }
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private class ProcessTrajectorySeq(trajectorySeq: Array[Trajectory]
                                    , tB_Seq:Array[Trajectory]
                                    , commonTrajectorySeq: ConcurrentLinkedQueue[Trajectory])
    extends ParallelTask[Trajectory](
    trajectorySeq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100
    , message = Some("finding common trajectories")) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(tA: Trajectory) = {
      tB_Seq.foreach { tB =>
        if (tA.isEqual(tB)) commonTrajectorySeq.add(tA)
      }
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import TrajectoryAnalysis._
case class TrajectoryAnalysis(simpleImageSeq: Array[SimpleImage]
                              , sourceImageDir: String
                              , movingObjectDir: String
                              , mpoToIgnoreMinAllowedRaDecDistance: Double
                              , trajectoryCsvFileName: String
                              , gifDir: String) extends MyLogger {
  //---------------------------------------------------------------------------
  //images
  private val leftImage   = simpleImageSeq.head  //0
  private val middleImage = simpleImageSeq(1)    //1
  private val rightImage  = simpleImageSeq.last  //2
  //---------------------------------------------------------------------------
  private val midObservingDateTimeSeq = Array(
      LocalDateTime.parse(leftImage.midObservingTime)
    , LocalDateTime.parse(middleImage.midObservingTime)
    , LocalDateTime.parse(rightImage.midObservingTime))
  //---------------------------------------------------------------------------
  private val nameSeq = Array(
      leftImage.name
    , middleImage.name
    , rightImage.name)
  //---------------------------------------------------------------------------
  private def getMidObservingDateTimeSeq(trajectoryType:String) = {
    trajectoryType.toCharArray.map { letter =>
      letter match {
        case TRAJECTORY_TYPE_L => midObservingDateTimeSeq(TRAJECTORY_TYPE_L_INDEX)
        case TRAJECTORY_TYPE_M => midObservingDateTimeSeq(TRAJECTORY_TYPE_M_INDEX)
        case TRAJECTORY_TYPE_R => midObservingDateTimeSeq(TRAJECTORY_TYPE_R_INDEX)
      }
    }
  }
  //---------------------------------------------------------------------------
  private def getNameSeq(trajectoryType: String) = {
    trajectoryType.toCharArray.map { letter =>
      letter match {
        case TRAJECTORY_TYPE_L => nameSeq(TRAJECTORY_TYPE_L_INDEX)
        case TRAJECTORY_TYPE_M => nameSeq(TRAJECTORY_TYPE_M_INDEX)
        case TRAJECTORY_TYPE_R => nameSeq(TRAJECTORY_TYPE_R_INDEX)
      }
    }
  }
  //---------------------------------------------------------------------------
  def run() = {
    //analyse the  trajectory point sequence A: RLM
    val trajectoryStorageRLM = new ConcurrentLinkedQueue[Trajectory]()
    new ProcessSourceSeq(rightImage.getSortedSourceSeq
                       , leftImage.getSortedSourceSeq
                       , middleImage.getSortedSourceSeq
                       , TRAJECTORY_TYPE_RLM
                       , midObservingDateTimeSeq  //keep the original time stamp order
                       , trajectoryStorageRLM)

    //analyse the  trajectory point sequence B: MRL
    val trajectoryRunMRL = new ConcurrentLinkedQueue[Trajectory]()
    new ProcessSourceSeq(middleImage.getSortedSourceSeq
                         , rightImage.getSortedSourceSeq
                         , leftImage.getSortedSourceSeq
                         , TRAJECTORY_TYPE_MRL
                         , midObservingDateTimeSeq //keep the original time stamp order
                         , trajectoryRunMRL)

    //get the common trajectories found at the 2 runs
    val validTrajectoryStorageRLM = trajectoryStorageRLM.asScala.toArray.filter(_.isValid)
    val validTrajectoryStorageMRL = trajectoryRunMRL.asScala.toArray.filter(_.isValid)

    info(s"Getting common trajectories from both runs. Run RLM:'${validTrajectoryStorageRLM.size}' and run MRL:'${validTrajectoryStorageMRL.size}'")

    val commonTrajectoryStorage = new ConcurrentLinkedQueue[Trajectory]()
    new ProcessTrajectorySeq(validTrajectoryStorageRLM
                             , validTrajectoryStorageMRL
                             , commonTrajectoryStorage)

    warning(s"Found potentially:'${commonTrajectoryStorage.size()}' trajectories")
    //filter trajectories by mpo
    val finalTrajectoryRLM_Seq = filterTrajectoriesByMpo(commonTrajectoryStorage.asScala.toArray
                                                         , sourceImageDir
                                                         , nameSeq) //use input order

    //save trajectories
    if(!finalTrajectoryRLM_Seq.isEmpty) {
      warning(s"Saving:'${finalTrajectoryRLM_Seq.length}' compatible trajectories")
      saveTrajectorySeq(getNameSeq(TRAJECTORY_TYPE_LMR)
                        , finalTrajectoryRLM_Seq
                        , getMidObservingDateTimeSeq(TRAJECTORY_TYPE_LMR) map (_.toString)
                        , trajectoryCsvFileName
                        , gifDir)
    }
    finalTrajectoryRLM_Seq.length
  }
  //-------------------------------------------------------------------------
  private def saveTrajectorySeq(imageNameSeq: Array[String]
                                , trajectorySeq: Array[Trajectory]
                                , midObservingTimeSeq:Array[String]
                                , trajectoryCsvFileName: String
                                , gifDir:String) = {

    //get the sources of all images
    val pathPrefix =
      if (sourceImageDir.startsWith("/")) "" else   Path.getCurrentPath() + "/"

    val imageSeq = simpleImageSeq.map(img => pathPrefix + sourceImageDir + "/" + img.name + ".fits")
                    .map(MyImage(_))

    //get telescope map
    val telescopeMap = imageSeq.map { img =>
      (img.getRawName(),Telescope.getOfficialCode(img.getSimpleFits()))
    }.toMap

    //create the file to store the trajectories
    val bw = new BufferedWriter(new FileWriter(new File(trajectoryCsvFileName)))
    bw.write(Trajectory.TRAJECTORY_CSV_HEADER)

    //save trajectories
    trajectorySeq.foreach { trajectory =>
      trajectory.save(
         imageNameSeq
        , midObservingTimeSeq
        , telescopeMap
        , bw)

      if (!gifDir.isEmpty) {
        Try {
          trajectory.saveGif(
            imageSeq
            , gifDir + "/trajectory_" + f"${trajectory.id}%02d" + ".gif")
        }
        match {
          case Success(_) =>
          case Failure(_) =>
            error(s"Error creating gif on trajectory:'${trajectory.id}'. Continuing with next trajectory")
        }
      }
    }

    //close file
    bw.close()
  }
  //---------------------------------------------------------------------------
  private def isValidTrajectoriesByMpo(trajectory: Trajectory
                                       , imageDir: String
                                       , imageNameSeq: Array[String]): Boolean = {

    val imageSeq = imageNameSeq map (s=> MyImage(s"$imageDir/$s.fits"))
    val sourceSeq = trajectory.getPointSeq map (_.source)

    imageSeq.foreach { img =>
      val mpoSeq = AsteroidChecker.query(img, sourceSeq)
      if (mpoSeq.size > 0) {
        mpoSeq.foreach { case (_, mpo) =>
          warning(s"Found mpo:'${mpo.composedName}' at trajectory:'${trajectory.id}'. Ignoring it")
        }
      }
      return false
    }
    true
  }
  //---------------------------------------------------------------------------
  private def filterTrajectoriesByMpo(trajectorySeq: Array[Trajectory]
                                      , imageDir: String
                                      , imageNameSeq: Array[String]): Array[Trajectory] = {

    info(s"Filtering:'${trajectorySeq.size}' trajectories to ignore known mpo")
    val r = trajectorySeq.filter { trajectory =>
      isValidTrajectoriesByMpo(trajectory, imageDir, imageNameSeq)
    }
    r
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file TrajectoryAnalysis.scala
//=============================================================================