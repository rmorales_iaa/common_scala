/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  01/Jul/2024
 * Time:  12h:33m
 * Description: Finding unknown moving object
 */
package com.common.image.fumo
//=============================================================================
import com.common.configuration.MyConf
import com.common.csv.CsvRead
import com.common.dataType.pixelDataType.PixelDataType
import com.common.database.mongoDB.database.gaia.GaiaDB
import com.common.fits.wcs.fit.FitWcs
import com.common.geometry.affineTransformation.AffineTransformation2D
import com.common.geometry.point.Point2D_Double
import com.common.geometry.rectangle.RectangleDouble
import com.common.geometry.segment.segment2D.Segment2D
import com.common.hardware.cpu.CPU
import com.common.image.focusType.ImageFocusMPO
import com.common.image.fumo.trajectory._
import com.common.image.myImage.MyImage
import com.common.image.myImage.dir.MyImageDir
import com.common.image.nsaImage.geometry.point.Point2D
import com.common.image.registration.{Registration, RegistrationByPolygonInvariant}
import com.common.image.registration.RegistrationByPolygonInvariant._
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
//=============================================================================
import scala.collection.mutable.ArrayBuffer
import scala.collection.JavaConverters.asScalaSetConverter
import java.io.{BufferedWriter, File, FileWriter}
import sys.process._
import java.time.LocalDateTime
import java.time.Duration
import java.util.concurrent.ConcurrentHashMap
import scala.util.{Try,Success,Failure}
import scala.collection.JavaConverters.mapAsScalaConcurrentMapConverter
//=============================================================================
//=============================================================================
object Fumo {
  //---------------------------------------------------------------------------
  private final val fitsExtension = MyConf.c.getStringSeq("Common.fitsFileExtension")
  //---------------------------------------------------------------------------
  private final val c = MyConf("input/fumo/fumo.conf")
  //---------------------------------------------------------------------------
  private final val FUMO_ASTROMETRIC_GROUP_NAME = "fumo"
  //---------------------------------------------------------------------------
  private final val RA_DEC_MIN_OVERLAPPING_PERCENTAGE  = c.getFloat("Fumo.tripletSearch.raDecMinOverlappingPercentage")
  //---------------------------------------------------------------------------
  private final val REGISTRATION_ALGORITHM_SEQ = c.getIntSeq("Fumo.registration.algorithmSeq") map (Registration.getAlgorithm(_))
  //---------------------------------------------------------------------------
  //solve astrometry
  private final val SOLVE_ASTROMETRY_SCRIPT = c.getString("Fumo.solveAstrometry.script")
  //---------------------------------------------------------------------------
  //image normalization
  private final val IMAGE_NORMALIZATION_USE_IMAGE_MAP = c.getInt("Fumo.imageNormalization.useImageMap") == 1
  //---------------------------------------------------------------------------
  //source centroid
  private final val SOURCE_CENTROID_ALGORITHM_SEQ = c.getStringSeq("Fumo.sourceCentroid.fallbackAlgorithmSeq")

  //source detection
  final val SOURCE_DETECTION_MIN_PIX_COUNT = c.getFloat("Fumo.sourceDetection.minSourcePixCount")

  private final val SOURCE_DETECTION_ROUND_SOURCE = c.getInt("Fumo.sourceDetection.roundSource")

  private final val SOURCE_DETECTION_FILTER_GAIA_SOURCES = c.getDouble("Fumo.sourceDetection.gaiaSourceMinAllowedDistance")

  final val SOURCE_DETECTION_MIN_ALLOWED_ECCENTRICITY = c.getFloat("Fumo.sourceDetection.minAllowedEccentricity")
  final val SOURCE_DETECTION_MAX_ALLOWED_ECCENTRICITY = c.getFloat("Fumo.sourceDetection.maxAllowedEccentricity")

  final val SOURCE_DETECTION_MIN_PIX_IN_PERCENTAGE = c.getFloat("Fumo.sourceDetection.pixInMinPercentage")

  private final val SOURCE_DETECTION_NOISE_TIDE_NORMALIZED_IMAGES = c.getDouble("Fumo.sourceDetection.noiseTideNormalizedImage")

  private final val SOURCE_DETECTION_MPO_MIN_PIX_DISTANCE = c.getDouble("Fumo.sourceDetection.mpoToIgnoreMinAllowedPixDistance")
  //---------------------------------------------------------------------------
  //trajectory
  final val TRAJECTORY_MAX_ALLOWED_SLOPE_TOLERANCE = c.getFloat("Fumo.trajectory.maxAllowedSlopeTolerance")
  final val TRAJECTORY_MAX_ALLOWED_SPEED_TOLERANCE = c.getFloat("Fumo.trajectory.maxAllowedSpeedTolerance")
  final val TRAJECTORY_MAX_ALLOWED_PIX_DISTANCE    = c.getInt("Fumo.trajectory.maxAllowedPixDistance")
  //---------------------------------------------------------------------------
  //trajectory gif
  final val TRAJECTORY_GIF_SPEED_MS = c.getInt("Fumo.trajectory.gif.speedMs")

  final val TRAJECTORY_GIF_X_CROP_PIX_EXTRA_SIZE = c.getInt("Fumo.trajectory.gif.xAxisCropPixExtraSize")
  final val TRAJECTORY_GIF_Y_CROP_PIX_EXTRA_SIZE = c.getInt("Fumo.trajectory.gif.yAxisCropPixExtraSize")

  final val TRAJECTORY_GIF_SURROUND_REC_BORDER_WIDTH_SIZE = c.getInt("Fumo.trajectory.gif.surroundRectangle.borderWidthSize")
  final val TRAJECTORY_GIF_SURROUND_REC_DISTANCE_TO_BORDER = c.getInt("Fumo.trajectory.gif.surroundRectangle.distanceToBorder")

  final val USE_NORMALIZED_IMAGES_FOR_BUILDING_GIF = c.getInt("Fumo.trajectory.gif.useNormalizedImagesForBuildingGIF") == 1

  //util
  private final val REMOVE_EMPTY_DIR_SCRIPT = c.getString("Fumo.util.removeEmptyDirectoriesScript")
  //---------------------------------------------------------------------------
  //intermediate dirs
  private final val DIR_MOVING_OBJECTS = "moving_objects"
  private final val DIR_MOVING_OBJECTS_SUBTRACTED_NORMALIZED = "moving_objects_subtracted"

  private final val DIR_SOURCE_DETECTION = "source_detection"
  private final val DIR_TRAJECTORY = "trajectory"
  private final val DIR_GIF = "gif"

  private final val DIR_INPUT_TRIPLET = "input_triplet"
  private final val DIR_OUTPUT_TRIPLET = "output_triplet"
  //---------------------------------------------------------------------------
  //intermediate file names
  private final val MEDIAN_FITS_FILE_NAME = "median.fits"
  private final val MOVING_OBJECTS_FITS_FILE_NAME = "moving_objects.fits"

  private final val TRAJECTORY_TSV_FILE_NAME = "trajectory.tsv"
  private final val SOURCE_DISCARDED_FILE_NAME = "source_discarded.tsv"
  //---------------------------------------------------------------------------
  private final val STEP_1 = 1 // Registration, solve astrometry, normalize, subtract median and stack
  private final val STEP_2 = 2 // Source detection
  private final val STEP_3 = 3 // Trajectory calculation
  final val ALL_STEPS = Array(STEP_1, STEP_2, STEP_3)
  //---------------------------------------------------------------------------
  private val sep = "\t"
  //---------------------------------------------------------------------------
  private def formatTripletID(id: Int) = f"$id%06d"
  //---------------------------------------------------------------------------
  private def getSortedTimeStampFilenameSeq(dir: String
                                             , fileExtensionSeq: List[String] = fitsExtension) =
    Path.getSortedFileList(dir
      , fileExtensionSeq)
      .map(_.getAbsolutePath)
      .sortWith(_ < _)
      .toArray
  //---------------------------------------------------------------------------
  private def getTripletSeq(inputDir: String
                            , minElapsedMinutesBetweenImages: Int
                            , loadAllTriplets: Boolean = false): Array[Array[String]] = {
    if (loadAllTriplets)
      Path.getSubDirectoryList(inputDir).map { dir =>
        getSortedTimeStampFilenameSeq(dir.getAbsolutePath)
      }.toArray
    else
      findImageTriplets(inputDir
        , minElapsedMinutesBetweenImages)
  }
  //-------------------------------------------------------------------------
  private def findImageTriplets(inputDir: String
                                , minElapsedMinutesBetweenImages: Int): Array[Array[String]] = {
    //-------------------------------------------------------------------------
    val imageNameSeq = getSortedTimeStampFilenameSeq(inputDir)
    val timeRecMap = new ConcurrentHashMap[LocalDateTime, (String,RectangleDouble)]()
    //-------------------------------------------------------------------------
    class ProcessImageSeq(imageNameSeq: Array[String]) extends ParallelTask[String](
      imageNameSeq
      , CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 100
      , message = Some(" loading observing time")) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(fileName: String) = {
        val img = MyImage(fileName)
        val timeStamp = img.getObservingTime()
        val rec = img.getRaDecRectangle()
        timeRecMap.put(timeStamp, (img.name,rec))
      }
      //-----------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    new ProcessImageSeq(imageNameSeq)

    val sortedTimeSeq = timeRecMap
      .keySet
      .asScala
      .toArray
      .sortWith(_.isBefore(_))

    val minElapsedMinutes = Duration.ofMinutes(minElapsedMinutesBetweenImages)
    val tripletResultSeq = ArrayBuffer[Array[String]]()
    //-------------------------------------------------------------------------
    def getNextImageInTriplet(referenceTime: LocalDateTime
                              , startImageIndex: Int): Option[(LocalDateTime, Int)] = {

      val referenceRec = timeRecMap.get(referenceTime)._2

      sortedTimeSeq.zipWithIndex.drop(startImageIndex).foreach { case (currentTimeStamp, i) =>
        //checks elapsed time
        if (Duration.between(referenceTime, currentTimeStamp).compareTo(minElapsedMinutes) >= 0) {
          //check (ra,dec) intersection
          val currentRec = timeRecMap.get(currentTimeStamp)._2
          val commonRec = referenceRec.getIntersection(currentRec)
          if (commonRec.isDefined) {
            val overlappingPercentage_1 = (commonRec.get.area / referenceRec.area) * 100f
            val overlappingPercentage_2 = (commonRec.get.area / currentRec.area ) * 100f
            if (overlappingPercentage_1 >= RA_DEC_MIN_OVERLAPPING_PERCENTAGE &&
              overlappingPercentage_2 >= RA_DEC_MIN_OVERLAPPING_PERCENTAGE &&
              overlappingPercentage_1 <= 100 &&
              overlappingPercentage_2 <= 100)
              return Some((currentTimeStamp,i))
          }
        }
      }
      None
    }
    def isValidCommonRec(leftRec:RectangleDouble
                         , middleRec: RectangleDouble
                         , rightRec: RectangleDouble): (Boolean,RectangleDouble) = {
      val commonRec = RectangleDouble.getIntersectionSeq(Array(leftRec, middleRec, rightRec))
      if (commonRec.isEmpty) return (false,null)
      val overlappingPercentageLeft = (commonRec.get.area / leftRec.area) * 100f
      val overlappingPercentageMiddle = (commonRec.get.area / middleRec.area) * 100f
      val overlappingPercentageRight = (commonRec.get.area / rightRec.area) * 100f

      val r =
        overlappingPercentageLeft   >= RA_DEC_MIN_OVERLAPPING_PERCENTAGE &&
        overlappingPercentageMiddle >= RA_DEC_MIN_OVERLAPPING_PERCENTAGE &&
        overlappingPercentageRight  >= RA_DEC_MIN_OVERLAPPING_PERCENTAGE &&
        overlappingPercentageLeft   <= 100 &&
        overlappingPercentageMiddle <= 100 &&
        overlappingPercentageRight  <= 100

      (r,commonRec.get)
    }
    //-------------------------------------------------------------------------
    // Iterate through the sorted timestamps and build triplets using
    // 'minElapsedMinutesBetweenImages' criteria and common (ra,dec) interesection
    val maxValidTimePos = sortedTimeSeq.length - 1
    //-------------------------------------------------------------------------
    def tryToBuildTriplet(leftTime: LocalDateTime
                          , leftPos: Int): Option[(Int,Int)] = {

      //find middle image of the triplet
      val middleTimeResult = getNextImageInTriplet(leftTime
        , leftPos + 1)
      if (!middleTimeResult.isDefined) return None
      val middleTime = middleTimeResult.get._1
      val middleTimePos = middleTimeResult.get._2
      if (middleTimePos >= maxValidTimePos) return None

      //find right image of the triplet
      val rightTimeResult = getNextImageInTriplet(middleTime
        , middleTimePos + 1)
      if (!rightTimeResult.isDefined) return None
      val rightTime = rightTimeResult.get._1

      //check the intersection of all of rectangles
      val (r,_) = isValidCommonRec(timeRecMap.get(leftTime)._2
                                   , timeRecMap.get(middleTime)._2
                                   , timeRecMap.get(rightTime)._2)
      if (!r) return None

      //add triplet to result
      tripletResultSeq += Array(
        timeRecMap.get(leftTime)._1
        , timeRecMap.get(middleTime)._1
        , timeRecMap.get(rightTime)._1)
      Some((middleTimePos, rightTimeResult.get._2))
    }
    //-------------------------------------------------------------------------
    info("Finding image triplets")
    var leftTime = sortedTimeSeq.head
    var leftPos = 0
    var continue = true
    val lastTimeStamp = sortedTimeSeq.last

    while(continue){

      //try to build a triplet from left positon, if it is not possible, then
      //go to next position
      val r = tryToBuildTriplet(leftTime,leftPos)
      if (r.isEmpty) {
        leftPos += 1
        leftTime = sortedTimeSeq(leftPos)
        if (leftPos >= maxValidTimePos) continue = false
      }
      else {
        //update position of the next image to build a triplet
        val middleTimePos = r.get._1
        val rightTimePos = r.get._2
        val nextPos = middleTimePos + ((rightTimePos - middleTimePos) / 2) //half position between middle and right
        leftTime = sortedTimeSeq(nextPos)
        leftPos = nextPos
        //check if it is possible to build a new triplet
        if (Duration.between(lastTimeStamp, leftTime).compareTo(minElapsedMinutes) > 0)
          continue = false
      }
    }

    if (tripletResultSeq.isEmpty) {
      error(s"Not enough images in directory:'$inputDir' have at least:'$minElapsedMinutesBetweenImages' minutes between them or have an common FOV")
      return Array()
    }
    tripletResultSeq.toArray
  }
  //---------------------------------------------------------------------------
  def calculate(inputDir: String
                , outputDir: String = "output/fumo"
                , minElapsedMinutesBetweenImages: Int
                , stepSeq: Array[Int]
                , verbose: Boolean = false): Unit = {

    if (!Path.directoryExist(inputDir))
      return error(s"Directory:'$inputDir' does not exists")

    //get image name triplets
    val tripletDir =
      if (stepSeq.head > 1)  outputDir + s"/$DIR_INPUT_TRIPLET/"
      else inputDir

    val imageNameTripletSeq = getTripletSeq(tripletDir
      , minElapsedMinutesBetweenImages
      , stepSeq.head > 1)
    if (imageNameTripletSeq.isEmpty || imageNameTripletSeq.head.isEmpty) {warning(s"No input triplets to process in:'$inputDir'");return}
    warning(s"Found:'${imageNameTripletSeq.length}' image triplets with at least:'$minElapsedMinutesBetweenImages' minutes between each image of the triplet")

    val tripletCount = imageNameTripletSeq.length - 1
    var totalCompatibleTrajectoryFound = 0
    val tripletWithTrajectory = ArrayBuffer[String]()

    //triplet result file id
    var tripletResultFile: BufferedWriter = null
    val tripletResultFileName = s"$outputDir/triplets_with_trajectories.tsv"
    Path.ensureDirectoryExist(outputDir)
    tripletResultFile = new BufferedWriter(new FileWriter(new File(tripletResultFileName)))
    tripletResultFile.write(s"triplet_id${sep}compatible_trajectories_count\n")

    //iterate through all triplets
    imageNameTripletSeq.zipWithIndex.foreach { case (imageNameTriplet,i) =>

      warning(s"----------------> Processing triplet $i/$tripletCount <-------------------")
      val tripletID = formatTripletID(i)

      //prepare the input dir and output dirs
      val inDir = outputDir + s"/$DIR_INPUT_TRIPLET/$tripletID"
      val oDir = outputDir + s"/$DIR_OUTPUT_TRIPLET/$tripletID"

      if (stepSeq.head == STEP_1) {
        Path.resetDirectory(oDir)
        Path.ensureDirectoryExist(oDir)
        Path.resetDirectory(inDir)
        imageNameTriplet.foreach { imageName =>
          s"ln -s $imageName $inDir".!
        }
      }
      Try {
        val compatibleTrajectoryFound =
          Fumo(inDir
            , oDir
            , tripletID: String
            , verbose)
            .calculate(stepSeq)

        //update the compatible trajectories found
        if (compatibleTrajectoryFound > 0) {
          tripletWithTrajectory += tripletID
          tripletResultFile.write(s"${formatTripletID(tripletID.toInt)}$sep$compatibleTrajectoryFound\n")
          tripletResultFile.flush()
        }
        totalCompatibleTrajectoryFound += compatibleTrajectoryFound
      }
      match {
        case Success(_) =>
        case Failure(e: Exception) =>
          e.printStackTrace()
          exception(e, s"Error processing image triplet:'$imageNameTriplet' . Continuing with the next triplet")
      }

      if (stepSeq.head == STEP_3) {
        if (totalCompatibleTrajectoryFound != 0)
          warning(s"Compatible trajectory found:'$totalCompatibleTrajectoryFound'. Please check:'$tripletResultFileName'")
        else
          warning(s"Compatible trajectory found:'$totalCompatibleTrajectoryFound'")
      }

      warning(s"----------------> End of processing triplet $i/$tripletCount <-------------------")
    }
    if (totalCompatibleTrajectoryFound > 0)
      warning(s"fumo algorithm has found: '$totalCompatibleTrajectoryFound' compatible trajectories. Please check:'$tripletResultFileName'" )
    if (tripletResultFile!= null) tripletResultFile.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import Fumo._
case class Fumo(inputDir: String
                , outputDir: String  = "output/fumo"
                , tripletID: String
                , keepMovingObjectImages: Boolean = false
                , keepSourceDetection: Boolean = false
                , verbose: Boolean = false) extends MyLogger {
  //---------------------------------------------------------------------------
  private val normalizeDir                         = outputDir + "/normalized/"

  private val notRegisteredDir                     = outputDir + "/../../not_registered/"
  private val registeredDir                        = normalizeDir + "/registered/"
  private val registeredNotNormalizedDir           = outputDir + "/registered_not_normalized/"

  private val wcsSolvedDir                         = registeredDir + "/wcs_solved/"

  private val medianDir                            = wcsSolvedDir + "/median/"
  private val movingObjectsDir                     = wcsSolvedDir + s"/$DIR_MOVING_OBJECTS/"
  private val medianSubtractedDir                  = medianDir + "/median_subtracted/"
  private val movingObjectsSubtractedNormalizedDir = movingObjectsDir + s"/$DIR_MOVING_OBJECTS_SUBTRACTED_NORMALIZED/"

  private val sourceDetectionDir                   = movingObjectsSubtractedNormalizedDir + s"/$DIR_SOURCE_DETECTION/"
  private val sourceSynthetizationDir              = sourceDetectionDir + s"/png/"

  private val trajectoryDir                        = outputDir + s"/$DIR_TRAJECTORY/"
  private val gifDir                               = trajectoryDir + s"/$DIR_GIF/"

  private val dirSeq = Seq(
      normalizeDir
    , notRegisteredDir
    , registeredDir
    , registeredNotNormalizedDir
    , medianDir
    , movingObjectsSubtractedNormalizedDir
    , medianSubtractedDir
    , movingObjectsDir
    , wcsSolvedDir
    , sourceDetectionDir
    , sourceSynthetizationDir
    , trajectoryDir
    , gifDir
  )

  private val medianImageName = medianDir + s"/$MEDIAN_FITS_FILE_NAME"
  private val medianSubtractedAndStackedImageName = medianDir + "/median_subtracted_stacked.fits"
  private val sourceDetectionDiscardedFileName = sourceDetectionDir + s"/$SOURCE_DISCARDED_FILE_NAME"
  private val movingObjectImageName = movingObjectsDir + s"/$MOVING_OBJECTS_FITS_FILE_NAME"

  private val trajectoryCsvFileName = trajectoryDir + s"/$TRAJECTORY_TSV_FILE_NAME"
  //---------------------------------------------------------------------------
  private var registrationResultMap: Map[String,RegistrationResult] = null
  private val simpleImageMap = new ConcurrentHashMap[String,SimpleImage]()
  //---------------------------------------------------------------------------
  def calculate(stepSeq:Array[Int] = ALL_STEPS): Int = {

    val r = stepSeq.flatMap { step=>
      step match {
        case STEP_1 =>
          dirSeq.foreach{Path.ensureDirectoryExist(_)}

          //fit wcs
          imageWcsFitting(inputDir)

          //normalize images
          normalizingImages(inputDir)

          //register image seq
          if (!imageRegistration(inputDir)) {
            s"mv $inputDir $notRegisteredDir".!
            return 0
          }

          //solve astrometry required due to the registration process
          solvingAstrometry(registeredDir, wcsSolvedDir)

          //fit wcs
          imageWcsFitting(wcsSolvedDir)

          //calculate median, subtract from images and stack them
          subtractMedianAndStack(wcsSolvedDir)

          None

        case STEP_2 =>
          Path.resetDirectory(sourceDetectionDir)
          detectSourceSeq(step)
          None

        case STEP_3 =>
          Path.resetDirectory(trajectoryDir)
          Path.ensureDirectoryExist(gifDir)
          Some(processingTrajectories())
      }
    }
    s"$REMOVE_EMPTY_DIR_SCRIPT $outputDir/../../".!
    if (r.isEmpty) 0 else r.head
  }

  //---------------------------------------------------------------------------
  private def imageWcsFitting(inDir: String
                              , forceWcsFitting: Boolean = false): Boolean = {
    info("---- Image WCS fitting ----")

    val imageNameSeq = getSortedTimeStampFilenameSeq(inDir)
    val imageSeq = imageNameSeq map (MyImage(_))

    //create a directory linking the image that need a wcs fitting
    val tmpDir = Path.resetDirectory(s"$outputDir/tmp/")
    val recSeq = ArrayBuffer[RectangleDouble]()
    imageSeq.foreach { img=>
      if (forceWcsFitting  || !img.hasWCS_fit) {
        recSeq += img.getRaDecRectangle()
        s"ln -s ${img.name} $tmpDir".!
      }
    }

    if (recSeq.isEmpty) {
      Path.deleteDirectoryIfExist(tmpDir)
      return true
    }

    val minPos = Point2D_Double.getMax(recSeq.map { rec => rec.min }.toArray)
    val maxPos = Point2D_Double.getMax(recSeq.map { rec => rec.max }.toArray)

    val imageFocus = ImageFocusMPO(
      FUMO_ASTROMETRIC_GROUP_NAME
      , composedName = FUMO_ASTROMETRIC_GROUP_NAME
      , spiceSpk = null
      , storedInDB = false)

    FitWcs.fitDirectory(
      tmpDir
      , checkFlag = false
      , preciseWCS_SIP = true
      , onlyElapsedDaysLessThan = None
      , verbose = true)
      //, verbose = false) //fixe

    Path.deleteDirectoryIfExist(tmpDir)

    //drop created entries
    val gaiaDB = GaiaDB(GaiaDB.SUB_GAIA_DATABASE_NAME, FUMO_ASTROMETRIC_GROUP_NAME)
    gaiaDB.dropAllCollectionStartingWith(FUMO_ASTROMETRIC_GROUP_NAME)
    gaiaDB.close()

    true
  }
  //---------------------------------------------------------------------------
  private def saveRegistrationImageSeq(imageNameSeq: List[String]
                                       , aftResultMap: ConcurrentHashMap[String, AffineTransformation2D]
                                       , oDir: String) = {
    info(s"Saving images at:'$outputDir'")
    var aft = AffineTransformation2D.AFFINE_TRANSFORMATION_2D_UNIT
    imageNameSeq.zipWithIndex.foreach { case (imageName, i) =>
      info(s"Saving image after applying composed affine transformation ${i + 1}/${imageNameSeq.length}")
      val name = Path.getOnlyFilenameNoExtension(imageName)
      val registrationResult = aftResultMap.get(name)
      val imageAft =
        if (registrationResult == null) AffineTransformation2D.AFFINE_TRANSFORMATION_2D_UNIT
        else registrationResult

      aft = aft.compose(imageAft)
      val outputImageName = s"$oDir/${Path.getOnlyFilename(imageName)}"
      val img = MyImage(imageName)
      val newImg= MyImage(img.applyAffineTransformation(
        aft
        , outputImageName
        , backgroundValue = PixelDataType.PIXEL_ZERO_VALUE
        , integralInterpolationType = 1)
        , img.getSimpleFits())

      //remove wcs if it is required
      val fits = img.getSimpleFits()
      if (aft.isNotUnit) fits.removeWCS()
      newImg.saveAsFits(outputImageName
        , userFitsRecordSeq = fits.getKeyValueMap.toArray)
    }
  }
  //---------------------------------------------------------------------------
  private def imageRegistration(inputDir: String): Boolean = {
    info("---- Image registration ----")

    val aftResultMap = new ConcurrentHashMap[String, AffineTransformation2D]()
    val imageNameSeq = Registration.registerImageDir(normalizeDir
      , aftResultMap
      , REGISTRATION_ALGORITHM_SEQ)
    if (aftResultMap.size() != imageNameSeq.length - 1) //the first image has not atf
      return error(s"Can not save image sequence because expect:'${imageNameSeq.length - 1}' images with aft but found:'${aftResultMap.size()}'")
    else {
      saveRegistrationImageSeq(imageNameSeq
        , aftResultMap
        , oDir = registeredDir)

      //save registered images again but now using original images, not in the normalized ones
      val imageNameNotNormalizeSeq = imageNameSeq.map { imageName=>
        s"$inputDir/${Path.getOnlyFilenameNoExtension(imageName)}.fits"
      }
      saveRegistrationImageSeq(imageNameNotNormalizeSeq
        , aftResultMap
        , registeredNotNormalizedDir)
    }
    //save registration result TSV
    saveResultRegistrationTSV(inputDir
      , imageNameSeq
      , aftResultMap
      , s"$registeredDir/$REGISTRATION_RESULT_TSV")

    //the the map of noise tide
    !getRegistrationResultMap().isEmpty
  }
  //---------------------------------------------------------------------------
  private def saveResultRegistrationTSV(inputDir:String
                                        , imageNameSeq: List[String]
                                        , aftResultMap: ConcurrentHashMap[String, AffineTransformation2D]
                                        , csvFile: String) = {
    info(s"Saving registration result at:'$csvFile'")

    val firstImage = MyImage(imageNameSeq.head)
    val (_, _, firstImageNoiseTide, _, _) = firstImage.getSourceDetectionFromConfiguration()

    val bw = new BufferedWriter(new FileWriter(new File(csvFile)))
    val sep = "\t"
    bw.write(s"${REGISTRATION_TSV_HEADER.mkString(sep)}\n")
    aftResultMap
      .asScala
      .keys
      .toArray
      .sorted
      .zipWithIndex
      .foreach { case (imageName, i) =>
        if (i == 0) {
          val firstImageRec  = firstImage.getRaDecRectangle()
          bw.write(Path.getOnlyFilenameNoExtension(imageNameSeq.head) + sep
            + firstImage.getBackground() + sep
            + firstImage.getBackgroundRMS() + sep
            + firstImageNoiseTide + sep
            + firstImage.getObservingDateMidPoint() + sep
            + firstImage.getExposureTime() + sep
            + firstImage.getDimension().x + sep
            + firstImage.getDimension().y + sep
            + firstImageRec.min.x + sep
            + firstImageRec.min.y + sep
            + firstImageRec.max.x + sep
            + firstImageRec.max.y + sep
            + firstImage.getPixScaleX() + sep
            + firstImage.getPixScaleY() + sep
            + AffineTransformation2D.AFFINE_TRANSFORMATION_2D_UNIT + sep
            + "\n")
        }

        val prefix = if (inputDir.startsWith("/")) "" else Path.getCurrentPath() + "/"
        val sampleImage = MyImage(prefix + inputDir + s"/$imageName.fits")
        sampleImage.getSourceDetectionFromConfiguration()

        val aft = aftResultMap.get(imageName)
        sampleImage.setAft(aft)
        val sampleRec = sampleImage.getRaDecRectangle()

        val registrationResult = RegistrationResult(
          sampleImage.getBackground()
          , sampleImage.getBackgroundRMS()
          , sampleImage.getNoiseTide()
          , sampleImage.getObservingDateMidPoint().toString
          , sampleImage.getExposureTime()
          , Point2D[Double](sampleImage.getDimension().x, sampleImage.getDimension().y)
          , RectangleDouble(sampleRec.min.x,sampleRec.min.y
                            , sampleRec.max.x,sampleRec.max.y)
          , sampleImage.getPixScaleX()
          , sampleImage.getPixScaleY()
          , aft
        )
        bw.write(imageName + sep
          + registrationResult.background + sep
          + registrationResult.backgroundRMS + sep
          + registrationResult.noiseTide + sep
          + registrationResult.midObservingTime + sep
          + registrationResult.exposureTime + sep
          + registrationResult.imageDim.x + sep
          + registrationResult.imageDim.y + sep
          + registrationResult.raDecRectangle.min.x + sep
          + registrationResult.raDecRectangle.min.y + sep
          + registrationResult.raDecRectangle.max.x + sep
          + registrationResult.raDecRectangle.max.y + sep
          + registrationResult.xPixScale + sep
          + registrationResult.yPixScale + sep
          + registrationResult.aft + sep
          + "\n")
      }
    bw.close()
  }
  //---------------------------------------------------------------------------
  private def solvingAstrometry(inDir: String
                                , outDir: String): Unit = {
    info("---- Solving astrometry after registration ----")
    val currentPath = Path.getCurrentPath()

    //solve astrometry
    val command = s"$currentPath/$SOLVE_ASTROMETRY_SCRIPT $inDir $outDir"
    command.!
  }
  //---------------------------------------------------------------------------
  private def normalizingImages(inputDir:String): Unit = {
    info("---- Normalizing images ----")

    if (IMAGE_NORMALIZATION_USE_IMAGE_MAP)
      MyImageDir.subtractImageMap(
        inputDir
        , normalizeDir
        , resetOutputDir = false)
    else {
      val map = getSortedTimeStampFilenameSeq(inputDir).map { path =>
        val img = MyImage(path)
        val (_,_, noiseTide, _, _) = img.getSourceDetectionFromConfiguration()
        img.getRawName() -> noiseTide
      }.toMap
      MyImageDir.subtractValueFromMap(
        inputDir
        , normalizeDir
        , map
        , resetOutputDir = false)
    }
  }
  //---------------------------------------------------------------------------
  private def subtractMedianAndStack(inDir: String): Boolean = {
    info("---- Step: subtract median and stack ----")

    Path.ensureDirectoryExist(medianDir)
    Path.ensureDirectoryExist(medianSubtractedDir)
    Path.ensureDirectoryExist(movingObjectsSubtractedNormalizedDir)

    val medianImage = MyImageDir.getMedian(inDir
      , medianImageName)
    if (medianImage == null) return false

    MyImageDir.subtractImage(
      inDir
      , medianImage
      , medianSubtractedDir
      , resetSubtractedDir = false)

    MyFile.deleteFileIfExist(s"$medianSubtractedDir/$MEDIAN_FITS_FILE_NAME")

    //stack subtracted images
    val medianSubtractedAndStackedImage = MyImageDir.stack(medianSubtractedDir
      , medianSubtractedAndStackedImageName)

    //subtract the median stacked from median to obtain the image with moving objects
    val movingObjetImage = medianSubtractedAndStackedImage.subtract(medianImage)
    movingObjetImage.saveAsFits(movingObjectImageName)

    //link the moving image to some directories that will be used to facilitate the "blink"
    s"ln -s $movingObjectImageName $registeredNotNormalizedDir".!
    s"ln -s $movingObjectImageName $wcsSolvedDir".!

    //subtract moving objects from normalized images
    MyImageDir.subtractImage(
      inDir
      , movingObjetImage
      , movingObjectsSubtractedNormalizedDir
      , resetSubtractedDir = false
      , inverseSubtraction = true)

    true
  }
  //---------------------------------------------------------------------------
  private def detectSourceSeq(step: Int): Unit = {
    info("---- Detecting sources ----")

    getRegistrationResultMap()

    if (registrationResultMap.isEmpty) error("Can not read the registration result file")
    else {
      val commonRec = RectangleDouble.getIntersectionSeq(registrationResultMap.map { t=> t._2.raDecRectangle}.toArray).get

      FindSourceInNormalizedImage.init(sourceDetectionDiscardedFileName, commonRec, sep)

      val imageNameSeq = getSortedTimeStampFilenameSeq(movingObjectsSubtractedNormalizedDir)

      //image has been normalized, so source detection level (noise tide) is low
      val noiseTideMap = (imageNameSeq map { s =>
        Path.getOnlyFilenameNoExtension(s)-> SOURCE_DETECTION_NOISE_TIDE_NORMALIZED_IMAGES}).toMap

      MyImageDir.detectSourceSeq(
        movingObjectsSubtractedNormalizedDir
        , sourceDetectionDir
        , SOURCE_CENTROID_ALGORITHM_SEQ
        , SOURCE_DETECTION_ROUND_SOURCE
        , if (SOURCE_DETECTION_FILTER_GAIA_SOURCES == 0) None else Some(SOURCE_DETECTION_FILTER_GAIA_SOURCES)
        , noiseTideMap = Some(noiseTideMap)
        , validateSourceSeq = Some(FindSourceInNormalizedImage.validateSourceSeq)
        , synthetizeDir = Some(sourceSynthetizationDir)
        , resetOutputDir = false
      )

      FindSourceInNormalizedImage.close()
    }
  }
  //---------------------------------------------------------------------------
  private def processingTrajectories(): Int = {
    info("---- Processing trajectories ----")
    loadSimpleImageMap()

    if (simpleImageMap.isEmpty) {
      warning("No trajectories found")
      return 0
    }
    //load the 3 images ordered by timestamp and its sources
    val sortedImageName   = simpleImageMap.keySet().asScala.toArray.sorted

    //loadSources of first image
    val leftSimpleImage   = simpleImageMap.get(sortedImageName.head)
    val middleSimpleImage = simpleImageMap.get(sortedImageName(1))
    val rightSimpleImage  = simpleImageMap.get(sortedImageName.last)

    //trajectory analysis
    val sourceImageDir = if (USE_NORMALIZED_IMAGES_FOR_BUILDING_GIF) registeredDir else registeredNotNormalizedDir
    val compatibleTrajectoryFound = TrajectoryAnalysis(Array(leftSimpleImage, middleSimpleImage, rightSimpleImage)
      , sourceImageDir
      , movingObjectsSubtractedNormalizedDir
      , SOURCE_DETECTION_MPO_MIN_PIX_DISTANCE
      , trajectoryCsvFileName
      , gifDir).run()
    compatibleTrajectoryFound
  }
  //---------------------------------------------------------------------------
  private def getRegistrationResultMap() = {
    if (registrationResultMap == null) {
      val csvFileName = s"$registeredDir/${RegistrationByPolygonInvariant.REGISTRATION_RESULT_TSV}"
      if (!MyFile.fileExist(csvFileName)) registrationResultMap = Map[String, RegistrationResult]()
      else {
        val csv = CsvRead.readCsv(csvFileName)
        registrationResultMap = csv.getRowSeq.map { row =>

          val aftString = row.getString(REGISTRATION_TSV_AFT_COL_NAME)
          val aft = AffineTransformation2D(aftString.drop(1).dropRight(1).split(",").map(_.toDouble))
          val name = row.getString(REGISTRATION_TSV_COL_NAME)

          (name, RegistrationResult(row.getDouble(REGISTRATION_TSV_BACKGROUND_COL_NAME)
            , row.getDouble(REGISTRATION_TSV_BACKGROUND_RMS_COL_NAME)
            , row.getDouble(REGISTRATION_TSV_NOISE_TIDE_COL_NAME)
            , row.getString(REGISTRATION_TSV_MID_OBSERVING_TIME_COL_NAME)
            , row.getDouble(REGISTRATION_TSV_EXPOSURE_COL_NAME)
            , Point2D[Double](row.getDouble(REGISTRATION_TSV_AFT_IMAGE_X_PIX_COL_NAME), row.getDouble(REGISTRATION_TSV_AFT_IMAGE_Y_PIX_COL_NAME))
            , RectangleDouble(row.getDouble(REGISTRATION_TSV_AFT_IMAGE_MIN_RA_COL_NAME), row.getDouble(REGISTRATION_TSV_AFT_IMAGE_MIN_DEC_COL_NAME)
                             ,row.getDouble(REGISTRATION_TSV_AFT_IMAGE_MAX_RA_COL_NAME), row.getDouble(REGISTRATION_TSV_AFT_IMAGE_MAX_DEC_COL_NAME))
            , row.getDouble(REGISTRATION_TSV_X_AXIS_PIX_SCALE_COL_NAME)
            , row.getDouble(REGISTRATION_TSV_Y_AXIS_PIX_SCALE_COL_NAME)
            , aft)
          )

        }.toMap
      }
    }
    registrationResultMap
  }
  //---------------------------------------------------------------------------
  private def loadSimpleImageMap(): Unit = {
    //-------------------------------------------------------------------------
    class LoadImageSeq(imageNameSeq: Array[String]) extends ParallelTask[String](
      imageNameSeq
      , CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 100
      , message = Some(" loading images sources")) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(fileName: String) = {
        val sourceSeq = ArrayBuffer[TrajectoryPoint]()
        CsvRead.readCsv(fileName).getRowSeq.foreach { row =>
          val sourceData = row.getString("source_data")
          if (!sourceData.trim.isEmpty) {
            val source = Segment2D.deserialization(sourceData)
            val centroidPix = Point2D_Double(row.getDouble("centroid_x")
              , row.getDouble("centroid_y"))
            val centroidRaDec = Point2D_Double(row.getDouble("centroid_ra")
              , row.getDouble("centroid_dec"))
            source.setCentroidRaw(centroidPix)
            source.setCentroidRaDec(centroidRaDec)
            sourceSeq += TrajectoryPoint(source)
          }
        }
        val name = Path.getOnlyFilenameNoExtension(fileName)
        val rr = registrationResultMap.get(name)

        val simpleImage = SimpleImage(name
          , sourceSeq.map { source => (source.getID, source)}.toMap
          , rr.get.midObservingTime)
        simpleImageMap.put(name, simpleImage)
      }
      //-----------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    getRegistrationResultMap()

    val imageNameSeq = getSortedTimeStampFilenameSeq(sourceDetectionDir, List(".tsv"))
      .filter(!_.contains(SOURCE_DISCARDED_FILE_NAME))

    new LoadImageSeq(imageNameSeq)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Fumo.scala
//=============================================================================