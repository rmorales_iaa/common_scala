
/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  20/Oct/2023
 * Time:  12h:52m
 * Description: None
 */
//=============================================================================
package com.common.image.normalize
//=============================================================================
import com.common.configuration.MyConf
import com.common.dataType.pixelDataType.PixelDataType.{PIXEL_DATA_TYPE}
import com.common.fits.simpleFits.SimpleFits.{KEY_EXTEND, KEY_EXTNAME}
import com.common.geometry.matrix.matrix2D.Matrix2D
import com.common.hardware.cpu.CPU
import com.common.image.myImage.MyImage
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
//=============================================================================
import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
object NormalizeGnuAstro extends MyLogger {
  //-------------------------------------------------------------------------
  private val fitsExtension = MyConf.c.getStringSeq("Common.fitsFileExtension")
  //-------------------------------------------------------------------------
  private final val GNU_ASTRO_SCRIPT           = "/home/rafa/proyecto/m2/input/gnuastro/gnuastro_single_file.bash"
  private final val GNU_ASTRO_OUTPUT_DIR       = "/home/rafa/proyecto/m2/output/gnuastro/"
  //-------------------------------------------------------------------------
  private def processImage(imageName: String
                           , outDir: String
                           , sigmaMultiplier: Double = 1.1
                           , sourceSizeRestriction: Option[(Int, Int)] = Some((4,2000))
                           , verbose: Boolean):Unit = {
    //---------------------------------------------------------------------------

    info(s"Processing image: '$imageName'")

    //build the gnuplot image
    val m2_command = s"$GNU_ASTRO_SCRIPT $imageName $GNU_ASTRO_OUTPUT_DIR"
    val processBuilder = sys.process.Process(Seq("bash", "-c", s"$m2_command"))
    processBuilder.!

    //detect sources in gnuImage
    val rawName = Path.getOnlyFilename(imageName)
    val gnuImageName = s"$GNU_ASTRO_OUTPUT_DIR/$rawName"
    val gnuImage = MyImage(gnuImageName)
    val region = gnuImage.findSourceSeq(noiseTide = 0)
    val gnuSourceSeq = region.toSegment2D_seq()

    //render the sources
    val image = MyImage(imageName)
    val imageBackground = image.getBackground(verbose)
    val data = ArrayBuffer.fill[PIXEL_DATA_TYPE](image.xMax * image.yMax)(imageBackground) //uniformize the backgrund
    val xMax = image.xMax
    val imageData = image.data

    gnuSourceSeq.foreach { gnuSource=>

      //detect sources in the source box
      val rec = gnuSource.getRectangle()
      val subMatrixSource = image.getSubMatrixWithPadding(rec.min
                                                         , rec.max)
      val stats = subMatrixSource.getRobustStats()

      val background =
        if (((stats.average - stats.median) / stats.stdDev) > 0.3) stats.median
        else (2.5 * stats.median) - (1.5 * stats.average)

      val backgroundRMS = stats.rms
      val noiseTide = background + (backgroundRMS * sigmaMultiplier)

      subMatrixSource.findSource(noiseTide
                                 , sourceSizeRestriction = sourceSizeRestriction)
                     .toSegment2D_seq()
                     .foreach { subSource =>
        subSource.getAbsolutePosSeq().foreach { case (x, y) =>
          val pos = (y * xMax) + x
          data(pos) = imageData(pos)
        }
      }
    }

    //save image
    val outputImage = s"$outDir/$rawName"
    val fits = gnuImage.getSimpleFits()
    fits.removeKeySeq(Seq(KEY_EXTEND,"BLANK",KEY_EXTNAME))
    MyImage(Matrix2D(image.xMax
                     , image.yMax
                     , data.toArray)
                     , image.getSimpleFits())
      .saveAsFits(outputImage)

    MyFile.deleteFileIfExist(gnuImageName)
  }
  //-------------------------------------------------------------------------
  class MyParallelImageSeq(seq: Array[String]
                           , outDir: String
                           , sigmaMultiplier: Double = 1.1
                           , sourceSizeRestriction: Option[(Int, Int)] = Some((4, 2000))
                           , verbose: Boolean) extends ParallelTask[String](
    seq
    , threadCount = CPU.getCoreCount()
    , isItemProcessingThreadSafe = true) {
    //-----------------------------------------------------------------------
    def userProcessSingleItem(imageName: String) =
      Try {
        processImage(imageName, outDir, sigmaMultiplier, sourceSizeRestriction, verbose)
      }
      match {
        case Success(_) =>
        case Failure(e) =>
          error(e.getMessage + s" Error catalogging the image '$imageName' continuing with the next image")
          error(e.getStackTrace.mkString("\n"))
      }
    //-----------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def processDirectory(inputDir: String
                       , outputDir: String
                       , sigmaMultiplier: Double = 1.1
                       , sourceSizeRestriction: Option[(Int, Int)] = Some((4, 2000))
                       , verbose: Boolean) = {
    Path.ensureDirectoryExist(GNU_ASTRO_OUTPUT_DIR)
    val outDir = Path.resetDirectory(outputDir)
    new MyParallelImageSeq(Path.getSortedFileList(inputDir,fitsExtension).map(_.getAbsolutePath).toArray
                          , outDir
                          , sigmaMultiplier
                          , sourceSizeRestriction
                          , verbose)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file NormalizeGnuAstro.scala
//=============================================================================