//=============================================================================
package com.common.image.region
//=============================================================================
import java.awt.image.BufferedImage
import java.awt.{Color, Font}
import java.io.File
import java.util.concurrent.atomic.AtomicInteger
import javax.imageio.ImageIO
import scala.collection.mutable.ArrayBuffer
//=============================================================================
import com.common.dataType.pixelDataType.PixelDataType._
import com.common.dataType.tree.binary.interval.AVLI_Tree
import com.common.dataType.tree.kdTree.K2d_Tree
import com.common.geometry.matrix.matrix2D.Matrix2D
import com.common.geometry.point.Point2D
import com.common.geometry.segment.Segment1D
import com.common.geometry.segment.segment2D.Segment2D
import com.common.logger.MyLogger
import com.common.util.path.Path
//=============================================================================
object Region {
  //---------------------------------------------------------------------------
  private val id = new AtomicInteger(Int.MinValue)
  //---------------------------------------------------------------------------
  def resetID() = id.set(Int.MinValue)
  //---------------------------------------------------------------------------
  def getNewID: Int = id.addAndGet(1)
  //---------------------------------------------------------------------------
  def getCurrentID: Int = id.get
  //---------------------------------------------------------------------------
}
//=============================================================================
//Region is a image with sources (segment2D)
case class Region(name: String ="No name", offset: Point2D = Point2D.POINT_ZERO, id: Int = Region.getNewID) extends MyLogger {
  //---------------------------------------------------------------------------
  private val map = scala.collection.mutable.Map[Int, Segment2D]()
  private var yAxisTree = AVLI_Tree("yAxisTree")
  private var k2d_TreeByVertex : K2d_Tree = null
  private var k2d_TreeByBoxHull : K2d_Tree = null
  private var voronoiMap : Map[Int, (Segment2D,Segment2D)] = null
  //---------------------------------------------------------------------------
  def apply(_id:Int) = map(_id)
  //---------------------------------------------------------------------------
  def getRawName() = Path.getOnlyFilenameNoExtension(name)
  //---------------------------------------------------------------------------
  def getFromMap = map
  //---------------------------------------------------------------------------
  def getSize = map.size
  //---------------------------------------------------------------------------
  def getItemCount = toSegment2D_seq.size
  //---------------------------------------------------------------------------
  def toSegment2D_seq() = map.values.toArray
  //---------------------------------------------------------------------------
  def clear() = {
    map.clear()
    yAxisTree = AVLI_Tree("yAxisTree")
  }
  //---------------------------------------------------------------------------
  def getSegmentCount2D() = (for (seg2D <- map.values.toArray) yield seg2D.getRowCount).sum
  //---------------------------------------------------------------------------
  def getSegmentCount1D() = (for (seg2D <- map.values.toArray) yield seg2D.getCountSegment1D).sum
  //---------------------------------------------------------------------------
  def getX_AxisAbsolute() = {
    val seg = Segment1D.getMax((for(seg2D <- map.values) yield seg2D.getX_AxisAbsolute()).toArray)
    Segment1D(seg.s, seg.e)
  }
  //---------------------------------------------------------------------------
  def getY_AxisAbsolute() = {
    val seg = Segment1D.getMax((for(seg2D <- map.values) yield seg2D.getY_AxisAbsolute()).toArray)
    Segment1D(seg.s, seg.e)
  }
  //---------------------------------------------------------------------------
  def getX_Axis = {
    val seg = getX_AxisAbsolute
    Segment1D(seg.s - offset.x, seg.e - offset.y)
  }
  //---------------------------------------------------------------------------
  def getY_Axis = {
    val seg = getY_AxisAbsolute
    Segment1D(seg.s - offset.x, seg.e - offset.y)
  }
  //---------------------------------------------------------------------------
  def getMinMax() ={
    val xAxis = getX_Axis
    val yAxis = getY_Axis
    (Point2D(xAxis.s,yAxis.s), Point2D(xAxis.e,yAxis.e))
  }
  //---------------------------------------------------------------------------
  def +=(r: Region) =
    r.toSegment2D_seq() foreach { seg2D =>
       seg2D.offset = seg2D.offset + r.offset
       addToStorage(seg2D)
    }

  //---------------------------------------------------------------------------
  def addToStorage(sourceSeq: Array[Segment2D]): Unit =
    sourceSeq map ( source=> addToStorage(source) )
  //---------------------------------------------------------------------------
  def addToStorage(seg2D: Segment2D): Unit = {
    map(seg2D.id) = seg2D
    yAxisTree += Segment1D(seg2D.offset.y, seg2D.offset.y + seg2D.rowSeq.length - 1, Array(), seg2D.id)
  }
  //---------------------------------------------------------------------------
  def getDimensionAbsolute() = Point2D(getX_AxisAbsolute.e + 1, getY_AxisAbsolute.e + 1)
  //---------------------------------------------------------------------------
  def getDimension() = getDimensionAbsolute
  //---------------------------------------------------------------------------
  def synthesizeValueSeq(): Array[PIXEL_DATA_TYPE] = {
    val dim = getDimension
    Segment2D.synthesizeValueSeq(toSegment2D_seq, dim.x, dim.y, Point2D.POINT_ZERO)
  }
  //---------------------------------------------------------------------------
  def synthesizeImageWithColor(n: String = name
                               , sourceSeq: Array[Segment2D] = toSegment2D_seq
                               , imageDimension : Option[Point2D]  = None
                               , format: String = "png"
                               , saveImage: Boolean = true
                               , blackAndWhite: Boolean = false
                               , userPixColor : Option[Int] = None //set to None for color random color in each source
                               , centroidColor: Option[Int] = None //set to None when no centroid must be show
                               , font: Option[Font] = None //Set to 'None' for no text on source. In other case, a valid alternative is: Some(new Font(Font.MONOSPACED, Font.PLAIN, 10)
                               , fontColor: Color = Color.YELLOW
                               , fontOffsetY: Int = 1
                               , pixZero: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE
                               , dict: scala.collection.mutable.Map[Long, String] = scala.collection.mutable.Map[Long, String]()
                               , differentColorSource: scala.collection.mutable.Map[Long, Color] = scala.collection.mutable.Map[Long, Color]()
                               , arrowDiffMap: Map[Int, Point2D] = Map[Int, Point2D]()
                               , pixelTransformation: Option[(Option[Seq[Double]], Option[Seq[Double]])] = None
                               , inverYAxis: Boolean = true
                               , additionalOffset: Point2D = Point2D.POINT_ZERO)
  : BufferedImage = {
    val dim = if (imageDimension.isDefined) imageDimension.get else getDimension
    val bi = Segment2D.synthesizeImageWithColor(sourceSeq
                                               , dim.x
                                               , dim.y
                                               , blackAndWhite = blackAndWhite
                                               , pixZero = pixZero
                                               , userPixColor = userPixColor
                                               , centroidColor
                                               , font = font
                                               , fontColor = fontColor
                                               , fontOffsetY = fontOffsetY
                                               , dict
                                               , differentColorSource
                                               , arrowDiffMap
                                               , pixelTransformation
                                               , inverYAxis
                                               , additionalOffset)

    val fileName = if (n.endsWith("." + format)) n else n + "." + format
    if (saveImage) ImageIO.write(bi, format, new File(fileName))
    bi
  }
  //---------------------------------------------------------------------------
  def printScalaCodeDefinition(maxSizeFormatValue : Int = 5): Unit =
    toMatrix2D().printScalaCodeDefinition(maxSizeFormatValue)

  //---------------------------------------------------------------------------
  def delete(seg2D: Segment2D) = {
    yAxisTree.delete(seg2D.getY_AxisAbsolute())
    map -=seg2D.id
  }
  //---------------------------------------------------------------------------
  def printY_AxisTree() = yAxisTree.prettyPrint()
  //---------------------------------------------------------------------------
  def printSegment2D_Seq() = {
    println(s"################### Region:'$name' starts ###################")
    map.toSeq.sortBy(_._1) foreach( _._2.prettyPrint())
    println(s"################### Region:'$name' ends ###################")
  }
  //---------------------------------------------------------------------------
  def deleteFromY_AxisTree(seg: Segment1D) = yAxisTree.delete(seg)
  //---------------------------------------------------------------------------
  def stack(seg1D: Segment1D, y:Int): Boolean = {
    val its = getXY_IntersectionAtRow(seg1D, y-1)
    if (its.length > 1) return false //append if not exist an unique intersection
    val seg2D =
      if(its.isEmpty) Segment2D(Array(Array(seg1D)),Point2D(0,y))
      else {
        val seg2D = map(its(0).id)
        yAxisTree.delete(seg2D.getY_AxisAbsolute())
        val stackAtEnd = seg2D.rowSeq.last.head.getFlag == 1
        seg2D.appendY_axis(seg1D,stackAtEnd)
        seg2D.offset = Point2D(0,Math.min(y,seg2D.offset.y))
        seg2D
      }
    addToStorage(seg2D)
    seg1D.setFlag(1)
    true
  }
  //---------------------------------------------------------------------------
  def getY_Intersection(yPos: Int) : Array[Segment2D] = {
    if (yPos == -1) Array()
    else yAxisTree.getIntersection(Segment1D(yPos)) map ( n=> map(n.v) )
  }
  //---------------------------------------------------------------------------
  def getXY_IntersectionAtRow(seg1D: Segment1D, y: Int): Array[Segment1D] =
    if (y < 0) Array[Segment1D]()
    else {
      (for (node<- yAxisTree.getIntersection(Segment1D(y))) yield {
        val seg2D = map(node.v)
        val segSeq = seg2D.yAxisRowAt(y-seg2D.offset.y)
        if(segSeq.isEmpty) None
        else {
          val r = (for (s<-segSeq) yield {
            if (s intersects seg1D) Some(Segment1D(s.s, s.e, Array(), node.v))
            else None
          }).flatten
          if (r.isEmpty) None else Some(r)
        }
      }).flatten.flatten
    }
  //---------------------------------------------------------------------------
  def toMatrix2D() = {
    val dim = getDimension
    Matrix2D(dim.x
      , dim.y
      , synthesizeValueSeq()
      , offset
      , name = "from_region")
  }
  //---------------------------------------------------------------------------
  def filterSourcesSeqSortBySize(maxCount: Int = -1) =
    toSegment2D_seq
      .sortWith(_.getBoxHull().area > _.getBoxHull().area)
      .take(maxCount)
  //---------------------------------------------------------------------------
  //(left,right,top,bottom)
  def getSegment2D_AtBorder(absolutePosition : Boolean = true) = {
    val xAxis = if (absolutePosition) getY_AxisAbsolute else getY_Axis
    val left = Segment1D(xAxis.s)
    val right = Segment1D(xAxis.e)

    val yAxis = if(absolutePosition) getY_AxisAbsolute else getY_Axis
    val top = Segment1D(yAxis.s)
    val bottom = Segment1D(yAxis.e)
    val leftSeq = ArrayBuffer[Segment2D]()
    val rightSeq = ArrayBuffer[Segment2D]()
    toSegment2D_seq() foreach { seg2D=>
      if (seg2D.xAxisIntersects(left)) leftSeq += seg2D
      if (seg2D.xAxisIntersects(right)) rightSeq += seg2D
    }
    val topSeq = getY_Intersection(top.s)
    val bottomSeq = getY_Intersection(bottom.s)
    (leftSeq.toArray, rightSeq, topSeq, bottomSeq)
  }
  //---------------------------------------------------------------------------
  def saveAsCsv(csvFileName: String, sep: String = "\t", sourceSeq: Array[Segment2D] = toSegment2D_seq()) = {
    Segment2D.saveAsCsv(sourceSeq, csvFileName, sep = sep)
  }

  //---------------------------------------------------------------------------
  def getK2D_TreeByVertex(seg2d_Seq: Array[Segment2D]= toSegment2D_seq) = {
    if (k2d_TreeByVertex == null) {
      k2d_TreeByVertex = K2d_Tree()
      val posSeq = seg2d_Seq.flatMap { s=> s.getAbsolutePosByElement(offset).map { p=> (p.toSequence(), s.id) }}
      k2d_TreeByVertex addSeq posSeq
    }
    k2d_TreeByVertex
  }
  //---------------------------------------------------------------------------
  def getK2D_TreeByByBoxHull(seg2d_Seq: Array[Segment2D]= toSegment2D_seq) = {
    if (k2d_TreeByBoxHull == null) {
      k2d_TreeByBoxHull = K2d_Tree(s"Sourround box hull of ${name}")
      val vertexSeq = seg2d_Seq flatMap { seg2D =>
        val id = seg2D.id
        val (minPos, maxPos) = seg2D.getAbsoluteMinMax()
        if (minPos == maxPos) Seq((minPos.toSequence,id))
        else{
          //get the box that sourrounds the source
          val bl = minPos             //bottom left
          val tr = maxPos             //top right
          val br = Point2D(tr.x,bl.y) //bottom right
          val tl = Point2D(bl.x,tr.y) //top left
          Seq((minPos.toSequence,id)) ++ Seq((maxPos.toSequence,id)) ++ Seq((br.toSequence,id)) ++ Seq((tl.toSequence,id))
        }
      }
      k2d_TreeByBoxHull.addSeq(vertexSeq)
    }
    k2d_TreeByBoxHull
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Region.scala
//=============================================================================
