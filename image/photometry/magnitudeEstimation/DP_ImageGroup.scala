/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  27/Mar/2023
 * Time:  11h:31m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.magnitudeEstimation
//=============================================================================
import com.common.image.catalog.ImageCatalog
import com.common.image.catalog.ImageCatalog.ImageSourceFocusMap
import com.common.image.focusType.ImageFocusTrait
import com.common.image.photometry.differential.{DP_Image, PhotometryDifferential}
import com.common.logger.MyLogger
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object DP_ImageGroup extends MyLogger {
  //---------------------------------------------------------------------------
  final val DP_IMAGE_GROUP_ALL_IMAGE_FILTERS = "all_image_filters"
  //---------------------------------------------------------------------------
  private def fillGroupSeq(dpImageSeq: Array[DP_Image]
                          , dpImageGroupSeq: Seq[DP_ImageGroup]) = {
     dpImageSeq foreach { dpImage=>
       dpImageGroupSeq.foreach { dpImageGroup =>
         if (dpImageGroup.containsImageFilter(dpImage.imageFilter)) dpImageGroup + dpImage
       }
    }
  }
  //---------------------------------------------------------------------------
  //map(imageFilter, map(catalogFilter,DP_ImageSeq))
  private def getImageFilterGroupMap(imageFilterSet: Set[String]
                                     , catalogSeq: Set[String]
                                     , catalogFilterSeq: Set[String]) = {
    //create the groups for by image filters
    val imageFilterGroupSeq =
      for (imageFilter <- imageFilterSet;
           catalog <- catalogSeq;
           catalogFilter <- catalogFilterSeq) yield {
        val estMag = MagnitudeEstimation.getEstMagID(catalog, catalogFilter)
        DP_ImageGroup(Set(imageFilter), estMag, estMag)
      }

    //create the groups for all image filters
    val allImageFilterIDSeq = for (imageFilter <- imageFilterSet) yield imageFilter
    val allImageFilterGroupSeq =
      for (catalog <- catalogSeq;
           catalogFilter <- catalogFilterSeq) yield {
        DP_ImageGroup(allImageFilterIDSeq
                     , MagnitudeEstimation.getEstMagID(catalog, catalogFilter)
                     , DP_IMAGE_GROUP_ALL_IMAGE_FILTERS)
    }

    (imageFilterGroupSeq ++ allImageFilterGroupSeq) //join the two sequences of groups
      .map(dpImageGroup => (dpImageGroup.getMapID, dpImageGroup))
     .toMap
  }
  //---------------------------------------------------------------------------
  def buildGroupMap(imageFocus: ImageFocusTrait
                    , imageDB: ImageCatalog
                    , sourceFocusMap: ImageSourceFocusMap
                    , imageNameSeq: Array[String]
                    , imageFilterSet: Set[String]
                    , catalogSeq: Set[String]
                    , catalogFilterSeq: Set[String]
                    , debugStorage : Boolean = false) = {

    val dpImageSeq = PhotometryDifferential.getDP_ImageFocusSeq(
        imageFocus
      , imageDB
      , sourceFocusMap
      , imageNameSeq
      , debugStorage)

    val groupMap = getImageFilterGroupMap(
        imageFilterSet
      , catalogSeq
      , catalogFilterSeq)

    fillGroupSeq(dpImageSeq, groupMap.values.toSeq)

    groupMap
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import DP_ImageGroup._
case class DP_ImageGroup(imageFilterSeq:Set[String]
                         , estMagID: String
                         , label: String) {
  //---------------------------------------------------------------------------
  private val dpImageSeq = ArrayBuffer[DP_Image]()
  val (catalog,catalogFilter) = MagnitudeEstimation.getCatalogAndFilter(estMagID)
  //---------------------------------------------------------------------------
  def getMapID =
    if (label == DP_IMAGE_GROUP_ALL_IMAGE_FILTERS) label + "!" + estMagID
    else estMagID
  //---------------------------------------------------------------------------
  def + (dp:DP_Image) = dpImageSeq += dp
  //---------------------------------------------------------------------------
  def getDP_ImageSeq() = dpImageSeq.toArray
  //---------------------------------------------------------------------------
  def containsImageFilter(imageFilter: String) = imageFilterSeq.contains(imageFilter)
  //---------------------------------------------------------------------------
  def getImageFilterID= {
    if (label == DP_IMAGE_GROUP_ALL_IMAGE_FILTERS) DP_IMAGE_GROUP_ALL_IMAGE_FILTERS
    else imageFilterSeq.head
  }
  //---------------------------------------------------------------------------
  def getImageNameSeq() = (dpImageSeq map (_._id)).toArray
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file DP_ImageGroup.scala
//=============================================================================
