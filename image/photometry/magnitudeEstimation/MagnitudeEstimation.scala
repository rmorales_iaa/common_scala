/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  13/Jan/2023
 * Time:  10h:22m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.magnitudeEstimation
//=============================================================================
import com.common.image.photometry.absolute.PolynomialFit
import com.common.logger.MyLogger
//=============================================================================
//=============================================================================
object MagnitudeEstimation {
  //---------------------------------------------------------------------------
  private val CATALOG_FILTER_ID_SEPARATOR = "!"
  //---------------------------------------------------------------------------
  final val COL_NAME_CATALOG        = "catalog"
  final val COL_NAME_CATALOG_FILTER = "catalog_filter"
  final val COL_NAME_BETA           = "beta"
  //---------------------------------------------------------------------------
  def headerCsv = Seq(
    "catalog"
    , "catalog_filter"
    , "julian_date"
    , "est_magnitude"
    , "est_magnitude_error"
  )
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
  def fluxToMagnitude(flux: Double) = -2.5 * Math.log10(flux)
  //---------------------------------------------------------------------------
  def magnitudeToFlux(mag: Double) = Math.pow(10, mag / -2.5)
  //---------------------------------------------------------------------------
  def estimateMagnitude(flux: Double
                        , curveFit: PolynomialFit
                        , vr: Double) = {
    val mag = fluxToMagnitude(flux)
    if (vr == -1) mag //used in stars
    else mag + curveFit.poly.value(vr)
  }
  //---------------------------------------------------------------------------
  def estimateMagnitudeAndError(flux: Double
                                , snr: Double
                                , curveFit: PolynomialFit
                                , vr: Double) = {
    val estimatedMagnitude = estimateMagnitude(flux, curveFit, vr)
    val estimatedMagnitudeError = (2.5 / Math.log(10)) * (1 / snr)
    (estimatedMagnitude, estimatedMagnitudeError)
  }
  //---------------------------------------------------------------------------
  def getEstMagID(catalog: String, catalogFilter: String) =
    catalog + CATALOG_FILTER_ID_SEPARATOR + catalogFilter
  //---------------------------------------------------------------------------
  def getCatalogAndFilter(estMagID: String) = {
    val seq = estMagID.split(CATALOG_FILTER_ID_SEPARATOR)
    if (seq.size != 2) (estMagID,"")
    else (seq(0), seq(1))
  }
  //---------------------------------------------------------------------------
  def getEstMag(estMagSeq: List[MagnitudeEstimation], estMagID: String) = {
    val (catalog,catalogFilter)  = getCatalogAndFilter(estMagID)
    estMagSeq.filter(estMag=> estMag.catalog == catalog &&
                              estMag.catalog_filter == catalogFilter).head.est_magnitude
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import MagnitudeEstimation._
trait MagnitudeEstimation extends MyLogger {
  //---------------------------------------------------------------------------
  val catalog: String
  val catalog_filter: String
  val julian_date: Double
  var est_magnitude: Double
  val est_magnitude_error: Double
  //---------------------------------------------------------------------------
  def filterByCatalogAndFilter(c: String, f: String) =
                                catalog == c &&
                                catalog_filter == f  &&
                                est_magnitude > 0
  //---------------------------------------------------------------------------
  def getID = getEstMagID(catalog,catalog_filter)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file EstimationMagnitude.scala
//=============================================================================
