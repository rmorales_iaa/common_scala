/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Apr/2022
 * Time:  20h:27m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.differential
//=============================================================================
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.image.photometry.differential.rotationalPeriod.RotationalPeriod
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object GnuPlotScript {
  //---------------------------------------------------------------------------
  private def normalizeNameGnuPlotName(s: String) = {
    val r = s.replaceAll("_", """\\\\_""")
    if (r.startsWith("'")) r else "'" + r + "'"
  }
  //---------------------------------------------------------------------------
  private def normalizeNameGnuPlotName2(s: String) = {
    val r = s.replaceAll("_", " ")
    if (r.startsWith("'")) r else "'" + r + "'"
  }
  //---------------------------------------------------------------------------
  private val colorPaletteObserveNight = Array(
      "web-green"
    , "black"
    , "blue"
    , "dark-spring-green"
    , "gray40"
    , "salmon")
  //---------------------------------------------------------------------------
  private val julianDateCol     = 2
  private val estMagCol         = 4
  private val rotPeriodCol      = 5
  private val observingNightCol = 6
  private val harmonicFitCol    = 7
  //---------------------------------------------------------------------------
  def rotationalPeriod(focusName: String
                       , imageCount: Int
                       , commonSourceCount: Int
                       , rotPeriod: Option[RotationalPeriod]
                       , observingNightSeq: Array[String]
                       , csvDataFilename: String
                       , gnuplotFileName: String
                       , yAxisTitle: String): Unit = {

    val stdDevDiffObservedFit = if (rotPeriod.isDefined)  rotPeriod.get.residualFitStats.stdDev else 0
    val peakCount             =  if (rotPeriod.isDefined)  rotPeriod.get.peakCount else 1
    val minDiffObservedFit    = if (rotPeriod.isDefined)  rotPeriod.get.residualFitStats.min - (rotPeriod.get.residualFitStats.min * 0.10)  else 0
    val maxDiffObservedFit    = if (rotPeriod.isDefined)  rotPeriod.get.residualFitStats.max + (rotPeriod.get.residualFitStats.max * 0.10)   else 0
    val bestFreq              =  if (rotPeriod.isDefined) rotPeriod.get.bestFreqInDays else 0d
    val rotationalPeriodDays  = if (rotPeriod.isDefined) bestFreq else 0d
    val rotationalPeriodHours = 24 / rotationalPeriodDays
    val fourierFitDegree      = rotPeriod.get.fourier.degree
    val fourierCoeff          = rotPeriod.get.fourier.toString
    val fourierCoeffSimple    = rotPeriod.get.fourier.coeffToSimpleString
    val harmonicAmplitude     = if (rotPeriod.isDefined) s"${f"${rotPeriod.get.harmonicAmplitude}%.3f"}" else "0"
    val isDiffEstMag          = !yAxisTitle.startsWith("Abso")
    val xAxisName             = s"'{/:Bold Rotational period}'"
    //-------------------------------------------------------------------------
    def getMainPlotCommand() = {

      val setTitle = observingNightSeq.length < 3
      val nightStringSeq = observingNightSeq.zipWithIndex.map { case (nightName,i) =>
        val title = if (setTitle)  s" title '$nightName'" else "notitle"

        s"my_data_file using col_rot_period:(night_name(column(col_est_mag),stringcolumn(col_observing_night),'$nightName')) " +
        s"""lc rgb '${colorPaletteObserveNight(i%colorPaletteObserveNight.length)}' $title , \\""".stripMargin
      }

      s"plot \\\n${nightStringSeq.mkString("\n")}\n" +
      s"fourier_fit(x) lc rgb 'red' title 'Fourier fit' with lines , \\\n" +
      "B_mean lc rgb '#5500ff' linetype 17 linewidth 2 title 'Avg est mag'"
    }
    //-------------------------------------------------------------------------
    def getCommonSourceCount() = {
     if (commonSourceCount == 0) ""
     else s"{/:Bold Common sources}:$commonSourceCount"
    }
    //-------------------------------------------------------------------------
    val s =
      s"""
#start of gnu script
#--------------------------------------
set datafile separator "\t"
set angles radians
set encoding utf8
set term x11  #activate when 'slow font message' appears
#--------------------------------------
#variables
my_title="{/:Bold Estimated ${if (isDiffEstMag) "diff" else "abso"} magnitude: ${normalizeNameGnuPlotName(yAxisTitle)} of ${normalizeNameGnuPlotName(focusName)}}\\n{/:Bold Images}:$imageCount $getCommonSourceCount {/:Bold Obs night count}:${observingNightSeq.length} {/:Bold Obs-fit stdDev}:${f"$stdDevDiffObservedFit%.3f"}\\n{/:Bold  Fourier fit}: {/:Italic f(t)= A/2 + ∑_{n=1}^{n=$fourierFitDegree}(a_n cos(2 Π n t) + b_n sin(2 Π n t))}\\n$fourierCoeff\\n {/:Bold Peak count: $peakCount {/:Bold Rotational period: ${f"${rotationalPeriodHours * peakCount}%.6f"}(hours)} {/:Bold Amp}:$harmonicAmplitude"

my_x_axis_label=${normalizeNameGnuPlotName2(xAxisName)}
my_y_axis_label=${normalizeNameGnuPlotName2(yAxisTitle)}
#--------------------------------------
my_data_file="$csvDataFilename"
#--------------------------------------
col_julian_Date     = $julianDateCol
col_est_mag         = $estMagCol
col_rot_period      = $rotPeriodCol
col_observing_night = $observingNightCol
col_harmonic_fit    = $harmonicFitCol
#--------------------------------------
#fourier fit
n =  ${fourierCoeffSimple(0)}
A =  ${fourierCoeffSimple(1)}
a_1= ${fourierCoeffSimple(2)}
b_1= ${fourierCoeffSimple(3)}
a_2= ${if(fourierCoeffSimple.length <= 4) 0 else  fourierCoeffSimple(4)}
b_2= ${if(fourierCoeffSimple.length <= 4) 0 else  fourierCoeffSimple(5)}

fourier_fit(t) = A/2 + (a_1 * cos( 2 * pi * n * t) + b_1 * sin(2 * pi * n * t)) +  \\
                       (a_2 * cos( 2 * pi * n * t) + b_2 * sin(2 * pi * n * t))
#--------------------------------------
set multiplot title my_title
#--------------------------------------
#https://www.sciencetronics.com/greenphotons/?p=570
mpl_top    = 1.4 #inch  outer top margin, title goes here
mpl_bot    = 0.8 #inch  outer bottom margin, t label goes here
mpl_left   = 0.4 #inch  outer left margin, y label goes here
mpl_right  = 0.1 #inch  outer right margin, y2 label goes here
mpl_height = 1.5 #inch  height of individual plots
mpl_width  = 2.0 #inch  width of individual plots
mpl_dx     = 0.1 #inch  inter-plot horizontal spacing
mpl_dy     = 0.5 #inch  inter-plot vertical spacing
mpl_ny     = 2   #number of rows
mpl_nx     = 1   #number of columns

# list full dimensions
xsize = mpl_left+mpl_right+(mpl_width*mpl_nx)+(mpl_nx-1)*mpl_dx
ysize = mpl_top+mpl_bot+(mpl_ny*mpl_height)+(mpl_ny-1)*mpl_dy

# placement functions
#   rows are numbered from bottom to top
bot(n) = (mpl_bot+(n-1)*mpl_height+(n-1)*mpl_dy)/ysize
top(n)  = 1-((mpl_top+(mpl_ny-n)*(mpl_height+mpl_dy))/ysize)
#   columns are numbered from left to right
left(n) = (mpl_left+(n-1)*mpl_width+(n-1)*mpl_dx)/xsize
right(n)  = 1-((mpl_right+(mpl_nx-n)*(mpl_width+mpl_dx))/xsize)
#--------------------------------------
#legend position
set key box
set key horiz
set key center top
#--------------------------------------
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
set xtic auto                          # set xtics automatically
set ytic auto                          # set ytics automatically
set grid
#--------------------------------------
set xlabel my_x_axis_label  font "Helvetica Bold,10"
set ylabel my_y_axis_label  font "Helvetica Bold,10"
#--------------------------------------
#average of estimated absolute magnitude
stats my_data_file using col_est_mag prefix "B" nooutput
#--------------------------------------
night_name(est_mag,night,expected_night) = night eq expected_night ? est_mag :1/0
#--------------------------------------
#main plot
unset xlabel

set lmargin at screen left(1)
set rmargin at screen right(1)
set tmargin at screen top(2)
set bmargin at screen bot(2) / 1.9

set xzeroaxis linetype 3 linewidth 1
set object 1 rectangle from graph 0,0 to graph 1,1 behind fillcolor rgb 'lemonchiffon' fillstyle solid noborder

set xtics font ", 1"  0.1
set ytics font ", 8"  0.05

${getMainPlotCommand()}
#--------------------------------------
#(observed - fit) plot

unset title
unset key
set format t ""

set xlabel my_x_axis_label  font "Helvetica Bold,10"
set xtic auto
set xzeroaxis linetype 3 linewidth 1
set object 1 rectangle from graph 0,0 to graph 1,1 behind fillcolor rgb 'khaki1' fillstyle solid noborder
set xtics font ", 0.8"  0.1

set yrange [ $minDiffObservedFit : $maxDiffObservedFit ]
set ylabel "Obs-fit" textcolor "blue"
set ytics font ", 8"  0.1 textcolor "blue"

set lmargin at screen left(1)
set rmargin at screen right(1)
set tmargin at screen top(1) / 1.6
set bmargin at screen bot(1)

plot my_data_file using col_rot_period:(column(col_est_mag)-column(col_harmonic_fit)) lt 7 lc rgb 'dark-grey' title '

unset multiplot
#--------------------------------------
pause -1
#--------------------------------------
#end of gnu script"""
    val fileName = gnuplotFileName
    val bw = new BufferedWriter(new FileWriter(new File(fileName)))
    bw.write(s)
    bw.close()
  }
//-----------------------------------------------------------------------------
def freqPower(focusName: String
              , xlimHours: Point2D_Double
              , ylim: Point2D_Double
              , gnuplotFileName: String
              , csvDataFilename: String) = {
  val s =
    s"""
#start of gnu script
#--------------------------------------
set datafile separator "\t"
set encoding utf8
#--------------------------------------
#variables
my_title="{/:Bold Spectral power of '${normalizeNameGnuPlotName(focusName)}'}"
my_x_axis_label='Frequency (hours)'
my_y_axis_label='Spectral power'
#--------------------------------------
my_data_file="$csvDataFilename"
#--------------------------------------
set title my_title
#--------------------------------------
set xlabel my_x_axis_label
set ylabel my_y_axis_label
#--------------------------------------
set xrange [${xlimHours.x}:${xlimHours.y}]
set yrange [${ylim.x}:${ylim.y}]
unset log                              # remove any log-scaling
set xtics 0.5
set ytic auto                          # set ytics automatically
set grid
#--------------------------------------
plot my_data_file using 1:2 with lines
#--------------------------------------
pause -1
#--------------------------------------
#end of gnu script"""

  val fileName = gnuplotFileName
  val bw = new BufferedWriter(new FileWriter(new File(fileName)))
  bw.write(s)
  bw.close()
}
//-----------------------------------------------------------------------------
def timeDiffMagnitude(focusName: String, gnuplotFileName: String, csvDataFilename: String) = {
    val s =
      s"""
#start of gnu script
#--------------------------------------
set datafile separator "\t"
set encoding utf8
#--------------------------------------
#variables
my_title="{/:Bold Source: '${normalizeNameGnuPlotName(focusName)}'}"
my_x_axis_label='Julian date'
my_y_axis_label='Differential magnitude'
#--------------------------------------
my_data_file="$csvDataFilename"
#--------------------------------------
set title my_title
#--------------------------------------
set xlabel my_x_axis_label
set ylabel my_y_axis_label
#--------------------------------------
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
set xtic auto                          # set xtics automatically
set ytic auto                          # set ytics automatically
set grid
#--------------------------------------
plot my_data_file using 1:2 with points
#--------------------------------------
pause -1
#--------------------------------------
#end of gnu script"""

    val fileName = gnuplotFileName
    val bw = new BufferedWriter(new FileWriter(new File(fileName)))
    bw.write(s)
    bw.close()
  }
  //-----------------------------------------------------------------------------
  def commonSourceEvolutionFluxAndStdev(focusName: String
                                        , solutionAndFilter: String
                                        , timeRange: String
                                        , imageCount: Int
                                        , commonSourcesCount: Int
                                        , _slope: Double
                                        , _intercept: Double
                                        , _r_2: Double
                                        , gnuplotFileName: String
                                        , csvDataFilename: String
                                       ) = {

    val slope     = s"${f"${_slope}%.5f"}"
    val intercept = s"${f"${_intercept}%.5f"}"
    val r_2       = s"${f"${_r_2}%.5f"}"

    val s =
      s"""
#start of gnu script
#--------------------------------------
set datafile separator "\t"
set encoding utf8
#--------------------------------------
#variables
my_title="Differential photometry analysis of {/:Bold '${normalizeNameGnuPlotName(focusName)}'} \\nfor {/:Bold ${normalizeNameGnuPlotName(solutionAndFilter)}\\nImage count: $imageCount Common sources: $commonSourcesCount \\n{/:Bold Time range}: $timeRange\\n{/:Bold Linear fit}: {/:Normal y=${slope}x+$intercept} {/:Bold R^2}:{/:Normal:$r_2}"
my_x_axis_label='{/:Bold Flux average}'
my_y_axis_label='{/:Bold Flux standard deviation}'
#--------------------------------------
my_data_file="$csvDataFilename"
#--------------------------------------
set object rectangle from screen 0,0 to screen 1,1 behind fillcolor rgb 'light-gray' fillstyle solid noborder
#--------------------------------------
set title my_title
#--------------------------------------
set xlabel my_x_axis_label
set ylabel my_y_axis_label
#--------------------------------------
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
set xtic auto                          # set xtics automatically
set ytic auto                          # set ytics automatically
set tics scale 2,2
set grid
#--------------------------------------
plot my_data_file every ::2 using 1:2:3 with labels point pt 1 offset char 0,1 notitle , \\
     my_data_file every ::1::1 using 1:2:3 with labels point pt 7  lc rgb 'blue' tc rgb 'blue' offset char 0,1 notitle, \\
     my_data_file every ::2 using 1:4 lc rgb 'sienna1' notitle with lines
#--------------------------------------
#--------------------------------------
pause -1
#--------------------------------------
#end of gnu script"""

    val fileName = gnuplotFileName
    val bw = new BufferedWriter(new FileWriter(new File(fileName)))
    bw.write(s)
    bw.close()
  }
//-----------------------------------------------------------------------------
}

//=============================================================================
//End of file GnuPlotScript.scala
//=============================================================================
