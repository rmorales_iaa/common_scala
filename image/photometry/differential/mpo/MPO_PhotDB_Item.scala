/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  18/Sep/2024
 * Time:  16h:55m
 * Description: None
 */
package com.common.image.photometry.differential.mpo
//=============================================================================
//=============================================================================
import com.common.image.photometry.absolute.photometricModel.mpo.MPO_EstMagDB_Item
//=============================================================================
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
//=============================================================================
//=============================================================================
object MPO_PhotDB_Item {
  //---------------------------------------------------------------------------
  private val customCodecs = fromProviders(classOf[MPO_PhotDB_Item]
    , classOf[MPO_EstMagDB_Item])

  val codecRegistry = fromRegistries(
      customCodecs
    , DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class MPO_PhotDB_Item(_id: String //image name
                           , m2_id: Int
                           , focus_id: String
                           , observing_time: String
                           , julian_date: Double
                           , julian_date_mid_point: Double
                           , v_r: Double
                           , m2_est_ra: Double //decimal degree
                           , m2_est_dec: Double //decimal degree
                           , m2_est_ra_residual: String //decimal degree
                           , m2_est_dec_residual: String //decimal degree
                           , m2_jpl_ra_orbit:  Double //decimal degree
                           , m2_jpl_dec_orbit: Double //decimal degree
                           , m2_jpl_light_time: Double //min
                           , m2_jpl_phase_angle: Double //degrees
                           , m2_jpl_julian_date_mid_point_corrected: Double
                           , m2_jpl_heliocentric_range: Double //au
                           , m2_observer_range: Double //au
                           , m2_est_azimut: Double //degrees
                           , m2_est_zenith: Double //degrees
                           , m2_est_hour_angle: Double
                           , m2_est_altitude: Double //m
                           , m2_est_air_mass: Double
                           , m2_est_extinction: Double
                           , m2_est_parallactic_angle: Double //deg
                           , m2_jpl_orbit_version: String
                           , magnitude_estimation_seq: List[MPO_EstMagDB_Item] = List[MPO_EstMagDB_Item]()) {
  //---------------------------------------------------------------------------
  def getBeta(esMagID:String) = {
    val r = magnitude_estimation_seq.filter(_.getID == esMagID)
    if (r.isEmpty) None
    else Some(r.head.beta)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MPO_ExtendedDB_Item.scala
//=============================================================================