/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  18/Sep/2024
 * Time:  16h:55m
 * Description: None
 */
package com.common.image.photometry.differential.mpo
//=============================================================================
//=============================================================================
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.image.photometry.absolute.photometricModel.ImageFocusDB
import com.common.logger.MyLogger
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
//=============================================================================
object MPO_PhotDB extends MyLogger {
  //---------------------------------------------------------------------------
  private final val DATABASE_NAME   = "mpo_catalog"
  //---------------------------------------------------------------------------
  final val COL_NAME_ID = "_id"
  //---------------------------------------------------------------------------
}
//=============================================================================
import MPO_PhotDB._
case class MPO_PhotDB(collectionName: String) extends ImageFocusDB {
  //-------------------------------------------------------------------------
  val databaseName = DATABASE_NAME
  //-------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) =
    initWithCodeRegister(MPO_PhotDB_Item.codecRegistry)
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  val collWithRegistry: MongoCollection[MPO_PhotDB_Item] = database.getCollection(collectionName)
  connected = true
  //-------------------------------------------------------------------------
  def get(imageName: String) =
    collWithRegistry
      .find(equal(COL_NAME_ID, imageName))
      .results()
      .head
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file MPO_ExtendedDB.scala
//=============================================================================