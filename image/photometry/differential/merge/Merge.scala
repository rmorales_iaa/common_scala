/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.common.image.photometry.differential.merge
//=============================================================================
import com.common.configuration.MyConf
import com.common.hardware.cpu.CPU
import com.common.image.focusType.ImageFocusTrait
import com.common.image.photometry.differential.DP_Image
import com.common.image.photometry.differential.merge.Merge.{MERGE_ALGORITHM_MIN_RESIDUAL_STACKED_OFFSET, MergeAlgorithm}
import com.common.image.photometry.differential.rotationalPeriod.RotationalPeriod
import com.common.image.photometry.magnitudeEstimation.MagnitudeEstimation
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object Merge extends MyLogger {
  //---------------------------------------------------------------------------
  private final val validEstMag = MyConf.c.getStringSeq("Photometry.differential.estMagSeq")
  //---------------------------------------------------------------------------
  sealed trait MergeAlgorithm
  case object MERGE_ALGORITHM_SIMPLE extends MergeAlgorithm
  case object MERGE_ALGORITHM_NO_OFFSET extends MergeAlgorithm
  case object MERGE_ALGORITHM_MIN_RESIDUAL_STACKED_OFFSET extends MergeAlgorithm
  //---------------------------------------------------------------------------
  val ROTATIONAL_PERIOD_MERGE_OUTPUT_DIR = s"/${RotationalPeriod.ROTATIONAL_PERIOD_DIR}/merge/"
  //---------------------------------------------------------------------------
  val CSV_COLUMN_NAME = Array(
    "image_name"
    , "julian_date"
    , "balanced_normalized_flux"
    , "balanced_estimated_absolute_magnitude"
    , "rotation_period_days"
    , "observing_night"
    , "harmonic_fit_magnitude"
    , "telescope"
    , "image_filter"
    , MagnitudeEstimation.COL_NAME_CATALOG
    , MagnitudeEstimation.COL_NAME_CATALOG_FILTER
  )
  //---------------------------------------------------------------------------
  private def loadDiffPhotSeq(objectName: String, inputDir: String) = {
    val seasonByEstMagMap = scala.collection.mutable.Map[String, ArrayBuffer[Array[DP_Image]]]()
    val csvSolutionSeq = Path.getSortedFileListRecursive(s"$inputDir", fileExtension = Seq(".csv")) flatMap { file =>
      if (file.getAbsolutePath.contains("residuals") ||
         file.getAbsolutePath.contains("lomb_scargle") ||
         file.getAbsolutePath.contains("time_diff_magnitude")
      ) None
      else Some(file.getAbsolutePath)
    }

    csvSolutionSeq.foreach { csvSolution =>
      val dpImageSeq = DP_Image.loadSeqFromCsv(objectName, csvSolution)
      val estMagID = dpImageSeq.head.focus.estMagID
      if (validEstMag.contains(estMagID)) {
        if (!seasonByEstMagMap.contains(estMagID)) seasonByEstMagMap(estMagID) = ArrayBuffer[Array[DP_Image]]()
        seasonByEstMagMap(estMagID) += dpImageSeq.sortWith(_.julian_date < _.julian_date)
      }
      else warning(s"Avoiding '$estMagID' estimated magnitude because it not present in the configuration file entry 'Photometry.differential.estMagSeq'")
    }
    seasonByEstMagMap
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.common.image.photometry.differential.merge.Merge._
case class Merge(imageFocus: ImageFocusTrait
                 , minPeriodHours: Double //single peak
                 , maxPeriodHours: Double //single peak
                 , peakCount: Int
                 , frequencySamplesPerPeak: Int
                 , fourierSeriesFitDegree: Int
                 , step: Double
                 , inputDir: String
                 , outputDir: String
                 , mergeAlgorithm: MergeAlgorithm = MERGE_ALGORITHM_MIN_RESIDUAL_STACKED_OFFSET) {
  //---------------------------------------------------------------------------
  var seasonByEstMagMap = loadDiffPhotSeq(imageFocus.getID, inputDir)
  if (seasonByEstMagMap.isEmpty) error("There is no images to process")
  else
    new MyParallelMerge(seasonByEstMagMap.toArray.map (t=> (t._1,t._2.toArray)))

  //---------------------------------------------------------------------------
  private class MyParallelMerge(inputSeq:Array[(String, Array[Array[DP_Image]])])
    extends ParallelTask[(String, Array[Array[DP_Image]])](
      inputSeq
      , CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 1000) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(t: (String, Array[Array[DP_Image]])) = {

      val estMagID = t._1
      val seasonSeqSeq = t._2
      info(s"Merging all images with catalog filter: '$estMagID'")
      val (catalog, catalogFilter) = MagnitudeEstimation.getCatalogAndFilter(estMagID)
      val oDir = Path.resetDirectory(s"$outputDir/$catalog/$catalogFilter")
      val sortedSeasonSeqSeq = seasonSeqSeq.sortWith(_.size > _.size)
      calculate(sortedSeasonSeqSeq,estMagID,oDir)
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private def calculate(sortedSeasonSeqSeq: Array[Array[DP_Image]]
                        , estMagID: String
                        , oDir: String) = {
    mergeAlgorithm match {
      case MERGE_ALGORITHM_SIMPLE =>
        AlgorithmSimpleNoOffset.run(
          imageFocus
          , sortedSeasonSeqSeq
          , minPeriodHours
          , maxPeriodHours
          , peakCount
          , frequencySamplesPerPeak
          , fourierSeriesFitDegree
          , estMagID
          , oDir)

      case MERGE_ALGORITHM_NO_OFFSET =>
        AlgorithmSimpleMedian.run(
          imageFocus
          , sortedSeasonSeqSeq
          , minPeriodHours
          , maxPeriodHours
          , peakCount
          , frequencySamplesPerPeak
          , fourierSeriesFitDegree
          , estMagID
          , oDir)

      case MERGE_ALGORITHM_MIN_RESIDUAL_STACKED_OFFSET =>
        AlgorithmMinResidualWithStackedOffset(
          imageFocus
          , sortedSeasonSeqSeq
          , minPeriodHours
          , maxPeriodHours
          , peakCount
          , frequencySamplesPerPeak
          , fourierSeriesFitDegree
          , step
          , estMagID
          , oDir)
    }
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file Merge.scala
//=============================================================================