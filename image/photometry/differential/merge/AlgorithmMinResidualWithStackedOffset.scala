/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/May/2022
 * Time:  11h:45m
 * Description:
 *
 */
//=============================================================================
package com.common.image.photometry.differential.merge
//=============================================================================
import com.common.configuration.MyConf
import com.common.image.focusType.ImageFocusTrait
import com.common.image.photometry.differential.DP_Image
import com.common.image.photometry.differential.rotationalPeriod.RotationalPeriod
import com.common.logger.MyLogger
//=============================================================================
import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
//=============================================================================
//=============================================================================
//Every season is a partial curve (piecewise) of the actual curve
//A season is a set of observing that that share the same FOV (usually calculates with command: '--find-common-fov')
//It is assumed that the very first season has the bigger number of images, so it can reproduce the real curve better.
//The merge is done by season pairs:
//  the first season of the pair (seasonA) has the better approximation of the curve,
//  the second season (seasonB) is forced to have the same frequency of seasonA. Then the differential magnitude offsset
//  of seasonB that minimizes the stdDev error of the merged curve (seasonA plus seasonB) is selected.
//  The search of the best offset starts from the differential magnitude offset median (calculated using the absolute manitude)
//  moving it in positive and negative (up and down) steps.
//  This best offset is then used to merge seasonA and seasonB, to create a NEW seasonA
//This calculate is repeated with the remain seasons
//-----------------------------------------------------------------------------
//=============================================================================
object AlgorithmMinResidualWithStackedOffset {
  //---------------------------------------------------------------------------
  private final val MIN_DIFF_TO_CONSIDER_TWO_DOUBLE_EQUAL = 10e-6
  //---------------------------------------------------------------------------
  private final val bestFreqCount = MyConf.c.getInt("RotationalPeriod.best")
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.common.image.photometry.differential.merge.AlgorithmMinResidualWithStackedOffset._
case class AlgorithmMinResidualWithStackedOffset(imageFocus: ImageFocusTrait
                                                 , _seasonSeq: Array[Array[DP_Image]]
                                                 , minPeriodHours: Double //single peak
                                                 , maxPeriodHours: Double //single peak
                                                 , peakCount: Int
                                                 , frequencySamplesPerPeak: Int
                                                 , fourierSeriesFitDegree: Int
                                                 , magnitudeStep: Double
                                                 , estMagID: String
                                                 , outputDir:String) extends MyLogger {
  //---------------------------------------------------------------------------
  private val forceRotPeriodHours: Option[Double] = None
  //---------------------------------------------------------------------------
  iteration(_seasonSeq)
  //---------------------------------------------------------------------------
  private def iteration(seasonSeq: Array[Array[DP_Image]]) = {
    info(s"$estMagID -> using min residual algorithm to merge sessions")

    var seasonA = seasonSeq.head
    info(s"$estMagID -> sing first season as template with: ${seasonA.size} images")

    seasonSeq.sortWith(_.length > _.length) //bigger season first
      .drop(1) //use first season as reference, so skip it
      .zipWithIndex.foreach { case (seasonB, seasonID) =>

        val positiveSeasonA = seasonA
        val positiveSeasonB = seasonB
        val negativeSeasonA = seasonA.map (_.simpleClone())
        val negativeSeasonB = seasonB.map (_.simpleClone())

        val posFuture = Future {
          findBestOffset(positiveSeasonA, positiveSeasonB, magnitudeStep, seasonID, seasonSeq.size, s"Season ${seasonID + 2}/${seasonSeq.size} positive offset")
        }

        Thread.sleep(1000)  //wait a little

        val negFuture = Future {
          findBestOffset(negativeSeasonA, negativeSeasonB, -magnitudeStep, seasonID, seasonSeq.size, s"Season ${seasonID + 2}/${seasonSeq.size} negative offset")
        }

        // Wait for both futures to complete
        val results = for {
          pos <- posFuture
          neg <- negFuture
        } yield (pos, neg)

        // This blocks the main thread until both computations are complete
        val (pos, neg) = Await.result(results, Duration.Inf)
        //get best offset and fix it
        val (bestRotationalPeriod, bestOffset) =
          if (pos._1.residualStdDev < neg._1.residualStdDev) pos else neg

        //fix offset
        RotationalPeriod.fixFocusOffsetMag(seasonB, bestOffset)

        //merge the seasons
        seasonA = (seasonA ++ seasonB).sortWith(_.julian_date < _.julian_date)

        info(s"$estMagID -> season ${seasonID + 2}/${seasonSeq.size} final best offset: ${f"$bestOffset%.5f"} best stdev: ${f"${bestRotationalPeriod.residualStdDev}%.5f"}<-----")
      }
    //save the final season merge
    info(s"$estMagID -> calculating and saving the final merge season for filter: '$estMagID'")
    getRotationalPeriod(seasonA, estMagID, saveResults = true)
  }
  //---------------------------------------------------------------------------
  private def findBestOffset(seasonA: Array[DP_Image]
                             , seasonB: Array[DP_Image]
                             , stepToUse: Double
                             , seasonID: Int
                             , seasonSeqSize: Int
                             , message: String) = {

    val initialOffsetB = 0d
    info(s"--->Season ${seasonID + 2}/$seasonSeqSize initial offset: ${f"$initialOffsetB%.5f"} step: ${f"$stepToUse%.5f"}")

    var continue = true
    var bestRotationalPeriod : RotationalPeriod = null
    var bestStedDev = Double.MaxValue
    var bestOffset = Double.MaxValue
    var deltaOffset = 0d
    var iteration = 0

    //save the original focus normalized diff magnitude
    val originalFocusDiffMagnitiudeSeqB = seasonB map { img => img.focus.normalized_est_mag }

    //iterate until find best stdev
    while (continue) {
      //fix offset
      if (iteration == 0) RotationalPeriod.fixFocusOffsetMag(seasonB, initialOffsetB)
      else RotationalPeriod.fixFocusOffsetMag(seasonB, deltaOffset)

      val newSeason = (seasonA ++ seasonB).sortWith(_.julian_date < _.julian_date)
      val newRotPeriod = getRotationalPeriod(newSeason, estMagID).get
      if ((newRotPeriod.residualStdDev > bestStedDev) ||
        Math.abs(newRotPeriod.residualStdDev - bestStedDev) < MIN_DIFF_TO_CONSIDER_TWO_DOUBLE_EQUAL) {
        warning(s"$estMagID ->\t$message iteration $iteration stopped because last residual stdev: ${f"${newRotPeriod.residualStdDev}%.5f"} is bigger or equal than best stdev: ${f"$bestStedDev%.5f"}")
        continue = false
      }
      else {
        bestRotationalPeriod = newRotPeriod
        bestOffset = seasonB.head.focus.normalized_est_mag - originalFocusDiffMagnitiudeSeqB.head
        bestStedDev = bestRotationalPeriod.residualStdDev
        info(s"$estMagID ->\t$message iteration $iteration new best offset: ${f"$bestOffset%.5f"} stdev: ${f"$bestStedDev%.5f"} images: ${seasonA.size + seasonB.size} rot period hours (single peak): ${f"${newRotPeriod.rotationalPeriodInHours}%.5f"}")
      }
      if (newRotPeriod.residualStdDev > 1) continue = false //not allowed errors in more than one differential magnitude

      deltaOffset += stepToUse
      iteration += 1
    }

    //back the original focus normalized diff magnitude
    seasonB zip originalFocusDiffMagnitiudeSeqB foreach { case (img, mag) => img.focus.normalized_est_mag = mag }

    (bestRotationalPeriod, bestOffset)
  }
  //---------------------------------------------------------------------------
  private def getRotationalPeriod(season: Array[DP_Image]
                                  , estMagID: String
                                  , saveResults: Boolean = false): Option[RotationalPeriod] =
    RotationalPeriod.calculateWithResidual(
      imageFocus
      , outputDir: String
      , _imageSeq = season
      , _timeSeq = season map (_.julian_date)
      , _estMagSeq = season map (_.focus.normalized_est_mag)
      , minPeriodHours
      , maxPeriodHours
      , frequencySamplesPerPeak
      , peakCount
      , fourierSeriesFitDegree
      , best = bestFreqCount
      , forceRotPeriodHours
      , estMagID = estMagID
      , isAbsoluteEstMag = false
      , saveResults = saveResults
      , alternativeCsvHeader = Some(Merge.CSV_COLUMN_NAME)
      , applySigmaClipping = true
      , verbose = false)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file AlgorithmMinResidual.scala
//=============================================================================