/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  31/May/2022
 * Time:  15h:35m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.differential.merge
//=============================================================================
import com.common.configuration.MyConf
import com.common.image.focusType.ImageFocusTrait
import com.common.image.photometry.differential.DP_Image
import com.common.image.photometry.differential.rotationalPeriod.RotationalPeriod
import com.common.logger.MyLogger
import com.common.stat.StatDescriptive

//=============================================================================
//=============================================================================
object AlgorithmSimpleMedian extends MyLogger {
  //--------------------------------------------------------------------------
  private final val bestFreqCount = MyConf.c.getInt("RotationalPeriod.best")
  //--------------------------------------------------------------------------
  def run(imageFocus: ImageFocusTrait
          , seasonSeq: Array[Array[DP_Image]]
          , minPeriodHours: Double
          , maxPeriodHours: Double
          , peakCount: Int
          , frequencySamplesPerPeak: Int
          , fourierSeriesFitDegree: Int
          , estMagID: String
          , outputDir: String) = {
    //--------------------------------------------------------------------------
    def calculateOffsetAndApplyMerge(seasonA: Array[DP_Image]
                                    , seasonB: Array[DP_Image]
                                    , seasonID: Int): Array[DP_Image] = {
      val medianSeq = seasonA.flatMap { img =>
        Some(StatDescriptive.getWithDouble(seasonB map (imgB => img.getOffset(imgB))).median)
      }
      val finalOffset = StatDescriptive.getWithDouble(medianSeq).median

      //apply final offset to imageB focus differential magnitude
      warning(s"Calculated offset for solution: ${seasonID + 1} regarding first solution with a differential magnitude: ${f"$finalOffset%.5f"} and image count: ${seasonB.length}")
      RotationalPeriod.fixFocusOffsetMag(seasonB, finalOffset)
      seasonA ++ seasonB
    }
    //--------------------------------------------------------------------------
    var seasonA = seasonSeq.head
    seasonSeq.drop(1).zipWithIndex.foreach { case (seasonB, seasonID) => seasonA = calculateOffsetAndApplyMerge(seasonA, seasonB, seasonID) }

    RotationalPeriod.calculateWithResidual(
      imageFocus
      , outputDir: String
      , _imageSeq = seasonA
      , _timeSeq = seasonA map (_.julian_date)
      , _estMagSeq = seasonA map (_.focus.normalized_est_mag)
      , minPeriodHours
      , maxPeriodHours
      , frequencySamplesPerPeak
      , peakCount
      , fourierSeriesFitDegree
      , best = bestFreqCount
      , estMagID = estMagID
      , isAbsoluteEstMag = false)

    info(s"Saved merge in directory:'$outputDir'")
  }
  //--------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file AlgorithmSimpleMedian.scala
//=============================================================================
