/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Feb/2023
 * Time:  10h:15m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.differential
//=============================================================================
import com.common.configuration.MyConf
import com.common.image.astrometry.fov.FovSource
import com.common.image.photometry.absolute.photometricModel.{ImageFocusDB}
import com.common.logger.MyLogger
import com.common.stat.StatDescriptive
import com.common.util.path.Path
//=============================================================================
import org.apache.commons.math3.stat.regression.SimpleRegression
import java.io.{BufferedWriter, File, FileWriter}
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object SourceEvolution extends MyLogger {
  //---------------------------------------------------------------------------
  private val filterMaxIteration                       = MyConf.c.getInt("Photometry.differential.filter.filterMaxIteration")
  //source filtering
  private val imageFocusEstFluxSigmaScale              = MyConf.c.getDouble("Photometry.differential.filter.sigmaClipping.image.focusEstFlux")

  private val relativeFluxSigmaScale                   = MyConf.c.getDouble("Photometry.differential.filter.sigmaClipping.sourceEvolution.relativeFlux")
  private val variableFluxSigmaScale                   = MyConf.c.getDouble("Photometry.differential.filter.sigmaClipping.sourceEvolution.variableFlux")
  private val linearFittingSigmaScale                  = MyConf.c.getDouble("Photometry.differential.filter.sigmaClipping.sourceEvolution.linearFitting")

  //source evolution filtering
  private val sourceEvolitionFocusMedianSigmaScale         = MyConf.c.getDouble("Photometry.differential.filter.sourceEvolution.focusSigmaClipping.median")
  private val sourceEvolitionFocusStdDevSigmaScale         = MyConf.c.getDouble("Photometry.differential.filter.sourceEvolution.focusSigmaClipping.stdev")
  private val sourceEvolitionMaxAllowedStdDevEstMagnitude  = MyConf.c.getDouble("Photometry.differential.filter.sourceEvolution.maxAllowedStdDevEstMagnitude")

  private val linearFittingFilterMinAllowedR2          = MyConf.c.getDouble("Photometry.differential.filter.linearFitting.minAllowedR2")
  //---------------------------------------------------------------------------
  private val CSV_COMMON_SOURCE_EVOLUTION_FILENAME = "common_source_evolution.csv"
  private val GNU_PLOT_COMMON_SOURCE_EVOLUTION_FILENAME = CSV_COMMON_SOURCE_EVOLUTION_FILENAME.replace(".csv", ".gnuplot")
  private val sep = "\t"
  //---------------------------------------------------------------------------
  def generatePlot(focusName: String
                   , dpImageSeq: Array[DP_Image]
                   , sourceEvolutionSeq: Array[SourceEvolution]
                   , timeRange: String
                   , oDir: String
                   , dpImageFocusFluxMap: Map[String,Double]): Unit = {

    val sourceEvolutionStatsSeq = sourceEvolutionSeq map { seq =>
      val seq2 = (seq.sourceSeq map (_._2.flux_per_second)).filter(v => !(v.isNaN || v.isInfinite))
      StatDescriptive.getWithDouble(seq2)
    }
    val focusStats = StatDescriptive.getWithDouble(dpImageSeq map (dpImage=> dpImageFocusFluxMap(dpImage._id)))

    val fluxAverageSeq = sourceEvolutionStatsSeq map (_.average)
    val stdDevSeq = sourceEvolutionStatsSeq map (_.stdDev)
    val xySeq = fluxAverageSeq zip stdDevSeq
    val regression = new SimpleRegression()
    xySeq map (p => regression.addData(p._1, p._2))

    val outputFilename = oDir + CSV_COMMON_SOURCE_EVOLUTION_FILENAME
    val bw = new BufferedWriter(new FileWriter(new File(outputFilename)))
    bw.write(s"flux_average${sep}std_dev${sep}_id${sep}lineal_fit${sep}template_source" + "\n")

    //focus
    val focusSourceID =  dpImageSeq.head.focus.m2_id
    val betterFocusName = focusName.split("_").last + "(" + focusSourceID + ")"
    bw.write(s"${focusStats.average}$sep${focusStats.stdDev}$sep$betterFocusName$sep${0}$sep$focusSourceID\n")

    //common sources
    xySeq.zipWithIndex foreach { case ((x, y), i) => bw.write(
      s"$x$sep$y$sep$i$sep${regression.predict(x)}$sep${sourceEvolutionSeq(i).refSource.m2_id}\n")
    }
    bw.close()

    val gnuplotFileName = oDir + GNU_PLOT_COMMON_SOURCE_EVOLUTION_FILENAME
    val oDirSplit = oDir.split("/")
    val solutionAndFilter  = oDirSplit(oDirSplit.size-3) + "_" + oDirSplit(oDirSplit.size-2) + "_" + oDirSplit.last

    GnuPlotScript.commonSourceEvolutionFluxAndStdev(
      focusName
      , solutionAndFilter
      , timeRange
      , sourceEvolutionSeq.head.sourceSeq.length //imageCount
      , sourceEvolutionSeq.length //commonSourcesCount
      , regression.getSlope
      , regression.getIntercept
      , regression.getRSquare
      , gnuplotFileName
      , Path.getCurrentPath() + oDir + CSV_COMMON_SOURCE_EVOLUTION_FILENAME
    )
  }

  //---------------------------------------------------------------------------
  private def sourceEvoltionFilterByFocusMedianAndStdDev(sourceEvolutionSeq: Array[SourceEvolution]
                                                         , estMagID: String
                                                         , statusSeq: ArrayBuffer[String]): Array[SourceEvolution] = {

    if (sourceEvolutionSeq.isEmpty) return sourceEvolutionSeq
    val focusSourceEvolution = sourceEvolutionSeq.head
    val focusSourceEvolutionMedianRelFlux = focusSourceEvolution.getMedianRelativeFlux()
    val focusSourceEvolutionStdev = focusSourceEvolution.getStdevEstMag(estMagID)
    val focusSourceEvolutionFluxPerSecondStats = StatDescriptive.getWithDouble(focusSourceEvolution.sourceSeq map (_._2.flux_per_second))

    warning(s"Focus source evolution median relative flux: '" + f"$focusSourceEvolutionMedianRelFlux%.3f" + s"' and std dev on '$estMagID': '" + f"$focusSourceEvolutionStdev%.3f" +"' ")
    warning(s"Focus source evolution flux per second: '" + f"${focusSourceEvolutionFluxPerSecondStats.average}%.3f" + s"' and std dev on '$estMagID': '" + f"${focusSourceEvolutionFluxPerSecondStats.stdDev}%.3f" +"' ")

    val r = sourceEvoltionFilterByFocusMedian(sourceEvolutionSeq,statusSeq,focusSourceEvolutionFluxPerSecondStats.average)
    sourceEvoltionFilterByFocusStdDev(r,statusSeq,focusSourceEvolutionFluxPerSecondStats.stdDev)
  }
  //---------------------------------------------------------------------------
  private def sourceEvoltionFilterByFocusMedian(sourceEvolutionSeq: Array[SourceEvolution]
                                                , statusSeq: ArrayBuffer[String]
                                                , focusSourceEvolutionRefValue: Double) =
    if (sourceEvolitionFocusMedianSigmaScale == 0) {
      val status = s"\tDisabled source evolution filter by focus median"
      info(status)
      statusSeq += status
      sourceEvolutionSeq
    }
    else {
      var status = s"\tSource evolution count before filtering by focus evolution median flux (sigma scale: $sourceEvolitionFocusMedianSigmaScale): ${sourceEvolutionSeq.length}"
      info(status)
      statusSeq += status

      val pairSeq = sourceEvolutionSeq map { sourceEvolution =>
        val leftValue = StatDescriptive.getWithDouble(sourceEvolution.sourceSeq map (_._2.flux_per_second)).average
        (leftValue - focusSourceEvolutionRefValue, sourceEvolution)
      }
      val r = StatDescriptive.sigmaClippingDoubleWithPair(pairSeq, sigmaScale = sourceEvolitionFocusMedianSigmaScale) map (_._2)

      status = s"\tSource evolution count after  filtering by focus evolution median flux (max allowed: $sourceEvolitionFocusMedianSigmaScale): ${r.length}"
      info(status)
      statusSeq += status
      r
    }

  //---------------------------------------------------------------------------
  private def sourceEvoltionFilterByFocusStdDev(sourceEvolutionSeq: Array[SourceEvolution]
                                                , statusSeq: ArrayBuffer[String]
                                                , focusSourceEvolutionRefValue: Double) =
    if (sourceEvolitionFocusStdDevSigmaScale == 0) {
      val status = s"\tDisabled source evolution filter by focus std dev"
      info(status)
      statusSeq += status
      sourceEvolutionSeq
    }
    else {
      var status = s"\tSource evolution count before filtering by focus estimated magnitude standard deviation (sigma scale: $sourceEvolitionFocusStdDevSigmaScale): ${sourceEvolutionSeq.length}"
      info(status)
      statusSeq += status

      val pairSeq = sourceEvolutionSeq map { sourceEvolution =>
        val leftValue = StatDescriptive.getWithDouble(sourceEvolution.sourceSeq map (_._2.flux_per_second)).stdDev
        (leftValue - focusSourceEvolutionRefValue, sourceEvolution)
      }
      val r = StatDescriptive.sigmaClippingDoubleWithPair(pairSeq, sigmaScale = sourceEvolitionFocusStdDevSigmaScale) map (_._2)

      status = s"\tSource evolution count after  filtering by focus estimated magnitude standard deviation (max allowed: $sourceEvolitionFocusStdDevSigmaScale): ${r.length}"
      info(status)
      statusSeq += status
      r
    }
  //---------------------------------------------------------------------------
  private def sourceEvoltionFilterByHighVariabiltyOnStdDevEstMag(sourceEvolutionSeq: Array[SourceEvolution]
                                                                 , estMagID: String
                                                                 , statusSeq: ArrayBuffer[String]) =
    if (sourceEvolitionMaxAllowedStdDevEstMagnitude == 0) {
      val status = s"\tDisabled source evolution filter by high variabilty on stdev"
      info(status)
      statusSeq += status
      sourceEvolutionSeq
    }
    else {
      var status = s"\tSource evolution count before filtering by estimated magnitude standard deviation (max allowed: $sourceEvolitionMaxAllowedStdDevEstMagnitude): ${sourceEvolutionSeq.length}"
      info(status)
      statusSeq += status
      val r = sourceEvolutionSeq.filter(_.getStdevEstMag(estMagID) < sourceEvolitionMaxAllowedStdDevEstMagnitude)
      status = s"\tSource evolution count after filtering by estimated magnitude standard deviation (max allowed: $sourceEvolitionMaxAllowedStdDevEstMagnitude): ${r.length}"
      info(status)
      statusSeq += status
      r
    }
  //---------------------------------------------------------------------------
  private def filterByHighVariabilityInFlux(sourceEvolutionSeq: Array[SourceEvolution]
                                            , statusSeq: ArrayBuffer[String]) = {
    if (variableFluxSigmaScale == 0) {
      val status = s"\tDisabled common source flux filter R by high variability"
      info(status)
      statusSeq += status
      sourceEvolutionSeq
    }
    else {
      var status = s"\tCommon sources count before filtering by high variability in flux (sigma clipping: $variableFluxSigmaScale) in filter R magnitude: ${sourceEvolutionSeq.length}"
      info(status)
      statusSeq += status
      val fluxPairSeq = sourceEvolutionSeq map { sourceEvolution =>
        (StatDescriptive.getWithDouble(sourceEvolution.sourceSeq.map(_._2.flux_per_second)).median, sourceEvolution)
      }
      val r = StatDescriptive.sigmaClippingDoubleWithPair(fluxPairSeq
        , sigmaScale = variableFluxSigmaScale) map (_._2)

      status = s"\tCommon sources count before filtering by high variability in flux (sigma clipping: $variableFluxSigmaScale) in filter R magnitude:" + s" ${r.length}"
      info(status)
      statusSeq += status
      r
    }
  }
  //-------------------------------------------------------------------------
  private def filterByLinearFitting(sourceEvolutionSeq: Array[SourceEvolution]
                                    , statusSeq: ArrayBuffer[String]) =

    if (linearFittingSigmaScale == 0) {
      val status = s"\tDisabled common sources filter by linear fitting"
      info(status)
      statusSeq += status
      sourceEvolutionSeq
    }
    else {
      if (sourceEvolutionSeq.length > 0) {
        var status = s"\tCommon sources count before linear fitting (x=flux_avg,y=flux_std): ${sourceEvolutionSeq.length}"
        info(status)
        statusSeq += status

        val r = filterByLinearFluxFit(sourceEvolutionSeq, statusSeq)

        status = s"\tCommon sources count after linear fitting filter (x=flux_avg,y=flux_std): ${r.length}"
        info(status)
        statusSeq += status
        r
      }
      else sourceEvolutionSeq
    }
  //---------------------------------------------------------------------------
  //filter common sources too far from a linear fit of (flux average,flux stdDev)
  private def filterByLinearFluxFit(_sourceEvolution: Array[SourceEvolution]
                                    , statusSeq: ArrayBuffer[String]
                                    , verbose: Boolean = true) = {
    //-------------------------------------------------------------------------
    var lastR_2_fit = 0d
    var iteration = 1
    //-------------------------------------------------------------------------
    def filter(iteration: Int, sourceEvolutionSeq: Array[SourceEvolution]) = {
      val commonSourceStatsSeq = sourceEvolutionSeq map (sourceEvolution =>
        StatDescriptive.getWithDouble(sourceEvolution.sourceSeq map (_._2.flux_per_second)))
      val fluxAverageSeq = commonSourceStatsSeq map (_.average)
      val stdDevSeq = commonSourceStatsSeq map (_.stdDev)
      val xySeq = fluxAverageSeq zip stdDevSeq
      val regression = new SimpleRegression()
      xySeq map (p => regression.addData(p._1, p._2))
      lastR_2_fit = regression.getRSquare

      if (verbose) {
        val status = s"\t\tLinear fitting iteration: $iteration. Common sources: ${sourceEvolutionSeq.length} before applying linear fitting with last R^2: ${f"$lastR_2_fit%.5f"}"
        statusSeq += status
        info(status)
      }

      //calculate the differences and apply a sigma clipping filter
      val residualPair = xySeq.zipWithIndex.map { case ((x, y),i) => (y - regression.predict(x), sourceEvolutionSeq(i)) }
      val r = StatDescriptive.sigmaClippingDoubleWithPair(
        residualPair
        , sigmaScale = linearFittingSigmaScale) map (_._2)

      if (verbose) {
        val status = s"\t\tLinear fitting iteration: $iteration. Common sources: ${r.length} after  applying linear fitting with last R^2: ${f"$lastR_2_fit%.5f"}"
        statusSeq += status
        info(status)
      }

      r
    }
    //-------------------------------------------------------------------------
    var continue = true
    var sourceEvolution = _sourceEvolution
    var currentSize = sourceEvolution.length
    while (continue) {
      currentSize = sourceEvolution.length
      sourceEvolution = filter(iteration, sourceEvolution)
      val newSize = sourceEvolution.length
      if ((newSize == 0) || (newSize == currentSize)) continue = false
      else currentSize = newSize
      iteration += 1
    }
    if (lastR_2_fit < linearFittingFilterMinAllowedR2) {
      warning(s"\tR^2 fitting:" + f"$lastR_2_fit%.3f" + " is below the min allowed R^2 fitting: " + f"$linearFittingFilterMinAllowedR2%.3f")
      Array[SourceEvolution]()
    }
    else sourceEvolution
  }

  //---------------------------------------------------------------------------
  private def filterByNotAbsoluteEstMag(sourceEvolutionSeq: Array[SourceEvolution]
                                        , estMagID: String
                                        , statusSeq: ArrayBuffer[String]) = {
    var status = s"\tCommon sources count before filtering by no estimated absolute magnitude on: '$estMagID': ${sourceEvolutionSeq.length}"
    info(status)
    statusSeq += status

    val r = sourceEvolutionSeq map { sourceEvolution =>
      val newSourceSeq = sourceEvolution.sourceSeq.filter(_._2.absolute_est_mag.isNaN)
      SourceEvolution(sourceEvolution.refImage, sourceEvolution.refSource, newSourceSeq)
    }
    status = s"\tCommon sources count after  filtering by no estimated absolute magnitude on: '$estMagID': ${sourceEvolutionSeq.length}"
    info(status)
    statusSeq += status
    r
  }
  //---------------------------------------------------------------------------
  private def filterSourceEvolution(_sourceEvolutionSeq: Array[SourceEvolution]
                                    , minCommonSource: Int
                                    , estMagID: String
                                    , statusSeq: ArrayBuffer[String]) = {

    var sourceEvolutionSeq = filterByHighVariabilityInFlux(_sourceEvolutionSeq, statusSeq)

    sourceEvolutionSeq     = sourceEvoltionFilterByHighVariabiltyOnStdDevEstMag(sourceEvolutionSeq, estMagID, statusSeq)
    sourceEvolutionSeq     = sourceEvoltionFilterByFocusMedianAndStdDev(sourceEvolutionSeq, estMagID, statusSeq)
    sourceEvolutionSeq     = filterByLinearFitting(sourceEvolutionSeq, statusSeq)

    if (sourceEvolutionSeq.length < minCommonSource) {
      val status = s"\tRemain common sources after applying all filters: ${sourceEvolutionSeq.length} but min expected: $minCommonSource . Ignoring : '$estMagID'"
      statusSeq += status
      error(status)
    }
    sourceEvolutionSeq
  }
  //---------------------------------------------------------------------------
  private def filterImageSeqByFocusEstimationFlux(sourceEvolutionSeq: Array[SourceEvolution]) = {
    //image sigma clipping by focus flux
    val dpImageSeq = sourceEvolutionSeq.head.getDP_ImageSeq()
    info(s"\tImage count before sigma clipping: '$imageFocusEstFluxSigmaScale' by normalized flux: ${dpImageSeq.length}")
    val dpImagePair = dpImageSeq flatMap { dpImage=>
      if (dpImage.focus.flux_per_second <= 0 ||
        dpImage.focus.flux_per_second.isNaN) None
      else Some((dpImage.focus.flux_per_second, dpImage))
    }
    val filteredImageSeq = StatDescriptive.sigmaClippingDoubleWithPair(dpImagePair, sigmaScale = imageFocusEstFluxSigmaScale) map (_._2)
    info(s"\tImage count after  sigma clipping '$imageFocusEstFluxSigmaScale' by normalized flux: ${filteredImageSeq.size}")
    SourceEvolution.keepCommonSourceWithImageSeq(sourceEvolutionSeq,filteredImageSeq)
  }
  //---------------------------------------------------------------------------
  private def updateFocus(sourceEvolutionSeq: Array[SourceEvolution]
                          , focusDB: ImageFocusDB
                          , estMagID: String) = {

    //recopile of all common sources in each image
    val dpImageFluxMap = scala.collection.mutable.Map[DP_Image,ArrayBuffer[Double]]()
    sourceEvolutionSeq.map { sourceEvolution =>
      val sourceEvolutionMedianRelativeFlux = sourceEvolution.getMedianRelativeFlux()
      sourceEvolution.sourceSeq map { case (image,fovSource)=>
        if (!dpImageFluxMap.contains(image)) dpImageFluxMap(image) = ArrayBuffer[Double]()
        dpImageFluxMap(image) += fovSource.est_relative_flux / sourceEvolutionMedianRelativeFlux
      }
    }

    //update focus flux and magnitude
    dpImageFluxMap.foreach { case (dpImage, relativeFluxSeq) =>
      //update the the normalized flux and absolute magnitude
      val newFlux  = StatDescriptive.getWithDouble(relativeFluxSeq.toArray).median
      val newAbsoluteEstMag = DP_Image.calculateEstAbsoluteMag(dpImage, focusDB, newFlux, estMagID)
      if (newAbsoluteEstMag.isEmpty) {
        dpImage.focus.flux_per_second    = Double.NaN
        dpImage.focus.normalized_est_mag = Double.NaN
      }
      else {
        dpImage.focus.est_relative_flux = newFlux
        dpImage.focus.absolute_est_mag = newAbsoluteEstMag.get
      }
    }
  }
  //-------------------------------------------------------------------------
  def calculate(sourceEvolutionSeq: Array[SourceEvolution]
                , focusDB: ImageFocusDB
                , minCommonSource: Int
                , imageFilter: String
                , solutionName: String
                , estMagID: String
                , statusSeq: ArrayBuffer[String]
                , isStar: Boolean
               ): Option[Array[SourceEvolution]] = {

    if(sourceEvolutionSeq.isEmpty) return None
    var continue = true
    var iteration = 0
    var lastSourceEvolutionSeq = sourceEvolutionSeq
    while (continue) {
      var status = s"Image filter: '$imageFilter' solution: '$solutionName' estimated magnitude filter: '$estMagID' iteration: $iteration starts. Starting images: ${lastSourceEvolutionSeq.head.getImageCount} and common sources: ${lastSourceEvolutionSeq.size}"
      statusSeq += status
      info(status)
      info(s"------------------------ iteration: $iteration starts ------------------------")
      var newSourceEvolution = filterImageSeqByFocusEstimationFlux(lastSourceEvolutionSeq)

      newSourceEvolution = filterSourceEvolution(
          newSourceEvolution
        , minCommonSource
        , estMagID
        , statusSeq)

      if (newSourceEvolution.isEmpty || newSourceEvolution.length < minCommonSource) {
        status = s"\tNot enough common sources after filtering by '$estMagID'"
        statusSeq += status
        error(status)
        if (isStar) {
          newSourceEvolution = lastSourceEvolutionSeq
          warning(s"Kepping the current source evolution because it is a star. Current common sources:${newSourceEvolution.size}")
        }
        else return None
      }

      status = s"Image filter: '$imageFilter' solution: '$solutionName' estimated magnitude filter: '$estMagID' iteration: $iteration ends. Remain images: ${newSourceEvolution.head.getImageCount} and common sources: ${newSourceEvolution.size}"
      info(status)
      info(s"++++++++++++++++++++++++ iteration: $iteration ends ++++++++++++++++++++++++")
      statusSeq += status

      if (lastSourceEvolutionSeq.size <= newSourceEvolution.size) continue = false
      lastSourceEvolutionSeq = newSourceEvolution
      iteration += 1

      if ((filterMaxIteration > 0) && (iteration >= filterMaxIteration)) {
        warning(s"Iteration stops due to max iteration filter: '$filterMaxIteration'")
        continue = false
      }
    }

    updateFocus(lastSourceEvolutionSeq, focusDB, estMagID)
    lastSourceEvolutionSeq = filterByNotAbsoluteEstMag(lastSourceEvolutionSeq, estMagID, statusSeq)

    val status = s"Final summary for solution: '$solutionName' image filter: '$imageFilter' estimated magnitude: '$estMagID' remain images: ${lastSourceEvolutionSeq.head.getImageCount} and common sources: ${lastSourceEvolutionSeq.size}"
    info(status)
    statusSeq += status

    Some(lastSourceEvolutionSeq)
  }
  //-------------------------------------------------------------------------
  def keepCommonSourceWithImageSeq(sourceEvolutionSeq: Array[SourceEvolution]
                                   , dpImageSeq: Array[DP_Image]) =
    sourceEvolutionSeq map ( _.keepCommonSourceWithImageSeq(dpImageSeq))
  //---------------------------------------------------------------------------
}
//=============================================================================
import SourceEvolution._
case class SourceEvolution(refImage: DP_Image
                           , refSource: FovSource
                           , sourceSeq: Array[(DP_Image,FovSource)]) {   //(phometry defferencital image,source inthis image)
  //-------------------------------------------------------------------------
  def getMedianRelativeFlux() = {
    val estRelativeFlux = sourceSeq map { _._2.est_relative_flux }
    if (relativeFluxSigmaScale == 0)
      StatDescriptive.getWithDouble(estRelativeFlux).median
    else {
      val sigmaClipped = StatDescriptive.sigmaClippingDouble(estRelativeFlux, sigmaScale = relativeFluxSigmaScale)
      StatDescriptive.getWithDouble(sigmaClipped).median
    }
  }
  //-------------------------------------------------------------------------
  def keepCommonSourceWithImageSeq(dpImageSeq: Array[DP_Image]) = {
    val idSet = dpImageSeq.map (_._id).toSet
    SourceEvolution(refImage
      , refSource
      , sourceSeq.filter(p=> idSet.contains(p._1._id)))
  }
  //-------------------------------------------------------------------------
  def getStdevEstMag(estMagID: String) =
    StatDescriptive.getWithDouble{sourceSeq map ( source=> source._2.getEstMag(estMagID) )} .stdDev
  //-------------------------------------------------------------------------
  def getDP_ImageSeq() = sourceSeq map (_._1)
  //-------------------------------------------------------------------------
  def getCommonSourceSeq() = sourceSeq map (_._2)
  //-------------------------------------------------------------------------
  def getImageCount = sourceSeq.size
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file SourceEvolution.scala
//=============================================================================
