/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  07/Apr/2022
 * Time:  11h:45m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.differential
//=============================================================================
import com.common.configuration.MyConf
import com.common.coordinate.conversion.Conversion
import com.common.geometry.point.Point2D
import com.common.image.astrometry.fov.CommonFov._
import com.common.image.astrometry.fov.{CommonFov, FovSource}
import com.common.image.catalog.ImageCatalog
import com.common.image.catalog.ImageCatalog.{ImagePhotometricMap, ImageSourceFocusMap}
import com.common.image.focusType.ImageFocusTrait
import com.common.image.photometry.absolute.lightCurve.LightCurve
import com.common.image.photometry.absolute.photometricModel.ImageFocusDB
import com.common.image.photometry.absolute.photometricModel.mpo.MPO_CatalogDB
import com.common.image.photometry.absolute.photometricModel.star.{StarCatalogDB, StarEstMagDB_Item}
import com.common.image.photometry.differential.PhotometryDifferential.sep
import com.common.image.photometry.differential.rotationalPeriod.RotationalPeriod
import com.common.image.photometry.magnitudeEstimation.MagnitudeEstimation
import com.common.image.telescope.Telescope
import com.common.logger.MyLogger
import com.common.stat.StatDescriptive
import com.common.util.path.Path
import com.common.util.time.Time
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
//=============================================================================
//=============================================================================
object PhotometryDifferential extends MyLogger {
  //---------------------------------------------------------------------------
  private val imageMaxSourceDropPercentageAverage            = MyConf.c.getDouble("Photometry.differential.filter.sourceCount.imageMaxSourceDropPercentageGroupAverage")

  private val maxClosestSourcesToFocus                       = MyConf.c.getInt("Photometry.differential.maxClosestSourcesToFocus")

  private val xBorderPercentageToIgnore                      = MyConf.c.getInt("Photometry.differential.borderImagePercentageToIgnore.xAxis")
  private val yBorderPercentageToIgnore                      = MyConf.c.getInt("Photometry.differential.borderImagePercentageToIgnore.yAxis")

  private val maxDropRateAllowed                             = MyConf.c.getDouble("Photometry.differential.maxDropRateAllowed")
  private val maxMagnitudeDiffAllowed                        = MyConf.c.getDouble("Photometry.differential.maxMagnitudeDiffAllowed")

  private final val bestFreqCount                            = MyConf.c.getInt("RotationalPeriod.best")
  //---------------------------------------------------------------------------
  private final val validEstMag                              = MyConf.c.getStringSeq("Photometry.differential.estMagSeq")
  //---------------------------------------------------------------------------
  val ROOT_PATH               = "/differential/"
  private val SUMMARY_CSV     = "solution_summary.csv"
  //---------------------------------------------------------------------------
  private val sep = ","
  //---------------------------------------------------------------------------
  private def calculateEstMagSeq(dpImage: DP_Image
                                 , catalogPhotometricModelMap: ImagePhotometricMap
                                 , catalogSeq: Set[String]
                                 , catalogFilterSeq: Set[String]
                                 , vr: Double) =
    dpImage.fovSourceSeq.foreach { fovSource =>
      val estMagSeq = ArrayBuffer[MagnitudeEstimation]()
      catalogSeq.foreach { catalog =>
        catalogFilterSeq.foreach { catalogFilter =>
          val id = dpImage._id
          if (catalogPhotometricModelMap.contains(id) &&
            catalogPhotometricModelMap(id).contains(catalog) &&
            catalogPhotometricModelMap(id)(catalog).contains(catalogFilter)) {
            estMagSeq += StarEstMagDB_Item(
                catalog
              , catalogFilter
              , fovSource
              , dpImage.julian_date
              , catalogPhotometricModelMap(id)(catalog)(catalogFilter)
              , vr)
          }
        }
      }
      fovSource.est_mag_seq = estMagSeq.toList
    }
  //---------------------------------------------------------------------------
  private def getDP_ImageSeq(imageFocus: ImageFocusTrait
                             , imageDB: ImageCatalog
                             , imageNameSeq: Array[String]
                             , vr: Double
                             , debugStorage : Boolean = false): (Array[DP_Image],Set[String],Set[String]) = {

    val catalogPhotometricModelMap = imageDB.getCatalogPhotometricModelMap(imageNameSeq)
      .filter { case (_,v) => !v.isEmpty }  //filter images with no photometric models

    if (catalogPhotometricModelMap.isEmpty)
      return (Array[DP_Image](),Set[String](),Set[String]())

    val (catalogSeq,catalogFilterSeq) = LightCurve.getPhotometricDatabaseAndFilterSeq(catalogPhotometricModelMap)

    val dpImageSeq = imageNameSeq.map { imageName =>
      val dpImage = DP_Image(
          imageDB.get(imageName)
        , imageFocus
        , FovSource.build(imageDB, imageName)
        , debugStorage)

      //apply the photometric models to all sources of a particular image
      calculateEstMagSeq(
        dpImage
        , catalogPhotometricModelMap
        , catalogSeq
        , catalogFilterSeq
        , vr)

      dpImage
    }
    //sort by date
    (dpImageSeq.sortWith(_.julian_date < _.julian_date)
      , catalogSeq
      , catalogFilterSeq)
  }

  //---------------------------------------------------------------------------
  def getDP_ImageFocusSeq(imageFocus: ImageFocusTrait
                          , imageDB: ImageCatalog
                          , sourceFocusMap: ImageSourceFocusMap
                          , imageNameSeq: Array[String]
                          , debugStorage : Boolean = false) = {

    val isStar = imageFocus.isStar
    val focusDB = if (imageFocus.isStar) StarCatalogDB(imageFocus.getID)
                  else MPO_CatalogDB(imageFocus.getID)

    val focusPairSeq =
      if (isStar) ImageFocusDB.getStarFocusSeq(sourceFocusMap,imageNameSeq)
      else ImageFocusDB.getMpoFocusSeq(sourceFocusMap,imageNameSeq)

    val dpImageSeq = imageNameSeq.zipWithIndex.map { case (imageName,i) =>
      val focusSourcePair = focusPairSeq(i)
      DP_Image(
        imageDB.get(imageName)
        , imageFocus
        , Seq( FovSource(focusSourcePair._1,focusSourcePair._2))
        , debugStorage)
    }

   focusDB.close()
   dpImageSeq
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import PhotometryDifferential._
case class PhotometryDifferential(imageFocus:ImageFocusTrait
                                  , vr: Double = 0d
                                  , minCommonSource: Int
                                  , minPeriodHours: Double
                                  , maxPeriodHours: Double
                                  , peakCount: Int
                                  , frequencySamplesPerPeak: Int
                                  , fourierSeriesFitDegree: Int
                                  , photometryRootPath: String
                                  , debugStorage : Boolean = false
                                  , verbose: Boolean = false
                                 ) extends MyLogger {
  //---------------------------------------------------------------------------
  private val objectName                = imageFocus.getID
  private val commonFovRootDir          = s"$photometryRootPath/${CommonFov.DEFAULT_OUTPUT_COMMON_FOV_DIR}/"
  private val rotationalPeriod          = Path.resetDirectory(s"$photometryRootPath/${RotationalPeriod.ROTATIONAL_PERIOD_PARTIAL_DIR}/")
  //---------------------------------------------------------------------------
  calculate()
  //---------------------------------------------------------------------------
  private def calculate(): Unit = {
    info(s"Loading the sequence of images for object: '$objectName'")

    val objectID = if (debugStorage) imageFocus.getDebuggingID else imageFocus.getID
    val imageDB = ImageCatalog(objectID)
    val focusDB = if (imageFocus.isStar) StarCatalogDB(objectID) else MPO_CatalogDB(objectID)
    val imageByFilterMap = imageDB.getNameSeqGroupByFilterMap(focusDB.getImageNameMap())
    val isStar = imageFocus.isStar

      for ((imageFilter, imageNameSeq) <- imageByFilterMap) {
        info(s"Getting the common FOV file for object: '$objectName' and image filter: '$imageFilter' with: ${imageNameSeq.size} images")
        val commonFovDir = s"$commonFovRootDir/$imageFilter/"

        val summaryFile = new BufferedWriter(new FileWriter(new File(commonFovDir + SUMMARY_CSV)))
        summaryFile.write(s"image_filter${sep}catalog${sep}catalog_filter${sep}solution${sep}status\n")

        getSolutionNameSeq(commonFovDir).foreach { solutionName =>
          val imageNameSeq = getImageNameSeqInSolution(commonFovDir + s"/$solutionName/image_seq.csv")
          if (imageNameSeq.isEmpty)
           error(s"Solution: '$solutionName' there is no valid images to calculate the differential photometry on object: '${imageFocus.composedName}' and filter: '$imageFilter' solution: '$solutionName'")
          else {
            info(s"Solution: '$solutionName' calculating the differential photometry on object: '${imageFocus.composedName}' and filter: '$imageFilter' with: ${imageNameSeq.size} images")
            info(s"Solution: '$solutionName' estimating all photometric models in all sources of all images")
            val (_dpImageSeq, catalogSeq, catalogFilterSeq) = getDP_ImageSeq(imageFocus, imageDB, imageNameSeq, vr, debugStorage)
            val dpImageSeq  = filterImageSeqBySourceCountDrop(_dpImageSeq)
              .filter{ dpImage=> !(dpImage.focus.flux_per_second.isNaN || dpImage.focus.flux_per_second.isInfinity) }
              .sortWith(_.julian_date < _.julian_date)

            //create a map with the image flux
            val dpImageFocusFluxMap = (dpImageSeq map (dpImage => (dpImage._id, dpImage.focus.flux_per_second))).toMap

            catalogSeq.foreach { catalog =>
              catalogFilterSeq.foreach { catalogFilter =>
                val estMagID = MagnitudeEstimation.getEstMagID(catalog, catalogFilter)

                if (validEstMag.contains(estMagID)) {
                  info(s"......................... $solutionName:$catalog:$catalogFilter .........................")
                  //calculate common sources in all images
                  if(calculateCommonSourceSeq(
                    dpImageSeq
                    , imageFilter
                    , estMagID
                    , solutionName
                    , summaryFile
                    , verbose)) {

                    //calculate common source evolution
                    calculateSourceEvolution(
                      imageFocus
                      , dpImageSeq
                      , focusDB
                      , imageFilter
                      , solutionName
                      , estMagID
                      , dpImageFocusFluxMap
                      , summaryFile
                      , isStar
                      , verbose)
                  }
                }
                else warning(s"Avoiding '$estMagID' estimated magnitude because it not present in the configuration file entry 'Photometry.differential.estMagSeq'")
              }
          }
        }
      }
      summaryFile.close
    }
    imageDB.close()
    focusDB.close()
  }
  //---------------------------------------------------------------------------
  private def filterImageSeqBySourceCountDrop(dpImageSeq: Array[DP_Image]) = {
    if (imageMaxSourceDropPercentageAverage == 0) dpImageSeq
    else {
      //image sigma clipping by focus flux
      info(s"\tImage source count before filtering by max source drop: ${dpImageSeq.length}")
      val groupAverage = StatDescriptive.getWithDouble(dpImageSeq map (_.fovSourceSeq.size.toDouble)).average
      val minSourceCount = Math.round(groupAverage - (groupAverage  * (imageMaxSourceDropPercentageAverage / 100)))
      info(s"\tMin image source count: $minSourceCount Group average: ${f"$groupAverage%.3f"}")
      val r = dpImageSeq.filter { dpImage=> dpImage.fovSourceSeq.size >= minSourceCount}
      info(s"\tImage source count after filtering by max source drop: ${r.length}")
      r
    }
  }
  //---------------------------------------------------------------------------
  private def getSolutionNameSeq(inDir: String) =
    Path.getSortedSubDirectoryList(inDir).map ( _.getName).filter( _.startsWith("solution_"))
  //---------------------------------------------------------------------------
  private def getImageNameSeqInSolution(csvName: String) = {
    val bufferedSource = Source.fromFile(csvName)
    var firstLine = true
    val imageNameSeq =
      (for (line <- bufferedSource.getLines) yield {
        if (firstLine) {
          firstLine = false
          None
        }
        else Some(line.split(CommonFov.sep).head)
      }).toArray.flatten

    bufferedSource.close
    imageNameSeq
  }
  //---------------------------------------------------------------------------
  private def calculateCommonSourceSeq(_dpImageSeq: Array[DP_Image]
                                       , imageFilter: String
                                       , estMagID: String
                                       , solutionName: String
                                       , summaryFile: BufferedWriter
                                       , verbose: Boolean) : Boolean = {

    val dpImageFilteredSeq = _dpImageSeq.filter { dpImage =>
      dpImage.focus.hasEstMag(estMagID) &&
      dpImage.focus.isValidEstMag(estMagID) //mpo and stars use the same estimate magnitude
    }

    if(dpImageFilteredSeq.isEmpty) {
      writeStatus(summaryFile,imageFilter,estMagID,solutionName,s"No images after filtering by valid estimages magnitude:'$estMagID'")
      return false
    }

    val focusExpectedEstMag = StatDescriptive.getWithDouble(dpImageFilteredSeq.map (_.focus.getEstMag(estMagID))).average
    info(s"\tEstimated magnitude: '$estMagID' estimated average magnitude of focus: ${f"$focusExpectedEstMag%.3f"}")
    val commonSourceSeq = getCommonSourceSeq(dpImageFilteredSeq, estMagID, focusExpectedEstMag, verbose)

    //find the common sources in each image
    info(s"\tEstimated magnitude: '$estMagID' finding common sources in all images")
    dpImageFilteredSeq.map { dpImage => dpImage.setLocalCommonSourceSeq(commonSourceSeq) }

    //calculate the relative flux of the common sources
    dpImageFilteredSeq.map(_.commonSourceCalculateRelativeFlux)

    true
  }
  //---------------------------------------------------------------------------
  private def getCommonSourceSeq(dpImageSeq: Array[DP_Image]
                                 , estMagID: String
                                 , focusExpectedEstMag: Double
                                 , verbose: Boolean): Array[FovSource] = {

    val templateImage = dpImageSeq.head
    val c = Telescope.getConfigurationFile(templateImage.telescope)
    val searchRadiusRa  = Conversion.pixToDecimalDegree(c.getDouble("SourceDetection.image.searchPixRadiusRa"), templateImage.x_pix_scale)
    val searchRadiusDec = Conversion.pixToDecimalDegree(c.getDouble("SourceDetection.image.searchPixRadiusDec"), templateImage.y_pix_scale)

    info(s"\tEstimated magnitude: '$estMagID' using first image taken as template: '${templateImage._id}'")
    info(s"\tEstimated magnitude: '$estMagID' detected source count in template image: ${templateImage.fovSourceSeq.length}")
    info(s"\tEstimated magnitude: '$estMagID' using min search radius in RA  in decimal degrees : ${f"$searchRadiusRa%.6f"}")
    info(s"\tEstimated magnitude: '$estMagID' using min search radius in DEC in decimal degrees : ${f"$searchRadiusDec%.6f"}")

    getCommonSourceSeq(
      dpImageSeq.drop(0) //avoid the first image that is used as template
      , searchRadiusRa
      , searchRadiusDec
      , estMagID
      , focusExpectedEstMag
      , verbose)
  }
  //---------------------------------------------------------------------------
  //for a input image sequence, get the image count that can share a common fov having compatible common sources
  private def getCommonSourceSeq(currentDP_ImageSeq: Array[DP_Image]
                                 , searchRadiusRa: Double
                                 , searchRadiusDec: Double
                                 , estMagID: String
                                 , focusExpectedEstMag: Double
                                 , verbose: Boolean) = {

    val templateDP_Image = currentDP_ImageSeq.head
    var templateCommonSourceSeq = templateDP_Image
      .fovSourceSeq
      .filter(_.m2_id != templateDP_Image.focus.m2_id).toArray //remove the focus from the common sources
    val imageCount = currentDP_ImageSeq.length
    var ignoredImageCount = 0

    //get closest sources to the template
    templateCommonSourceSeq =
      if (maxClosestSourcesToFocus == 0) templateCommonSourceSeq
      else templateDP_Image.getClosestSourceSeq(maxClosestSourcesToFocus, templateCommonSourceSeq)

    //filer sources at border
    templateCommonSourceSeq = filterSourceInBorder(templateCommonSourceSeq, templateDP_Image.getImageDimensions())

    info(s"\tEstimated magnitude: '$estMagID' starting with common sources: ${templateCommonSourceSeq.length} in the template image and ${currentDP_ImageSeq.length} images in the solution")

    //process all images in the sequence
    //get the intersection of all catalogs using the sources of the first image as template
    currentDP_ImageSeq
      .drop(1) //avoid the template image
      .zipWithIndex
      .foreach {
        case (dpImage, i) =>
          val prevCommonSourceSeq = templateCommonSourceSeq
          templateCommonSourceSeq = dpImage.findCommonSourceSeq(
            templateCommonSourceSeq
            , searchRadiusRa
            , searchRadiusDec)
          val dropRate = 100 - (templateCommonSourceSeq.length.toFloat / prevCommonSourceSeq.length) * 100

          if (verbose) info(s"Image '${dpImage._id}' ($i/$imageCount) remain common sources after matching: ${templateCommonSourceSeq.length} drop rate: ${f"$dropRate%.3f"} %")
          if (templateCommonSourceSeq.length < minCommonSource || templateCommonSourceSeq.isEmpty || dropRate > maxDropRateAllowed) {
            if (verbose) info(s"Image '${dpImage._id}' ($i/$imageCount) ignored min common sources:$minCommonSource)")
            templateCommonSourceSeq = prevCommonSourceSeq
            ignoredImageCount += 1
          }
      }

    info(s"\tEstimated magnitude: '$estMagID' common sources: ${templateCommonSourceSeq.length} in ${currentDP_ImageSeq.length} images in the solution")

    templateCommonSourceSeq  =
      if (maxMagnitudeDiffAllowed == 0) templateCommonSourceSeq
      else {
        //keep the common sources that are close to the focus magnitude
        templateCommonSourceSeq.filter { source =>
          val estMag = source.getEstMag(estMagID)
           Math.abs(estMag - focusExpectedEstMag) <= maxMagnitudeDiffAllowed
        }
      }

    info(s"\tEstimated magnitude: '$estMagID' common sources in all images after filtering by similar focus magnitude (${f"$focusExpectedEstMag%.3f"}): ${templateCommonSourceSeq.length}")

    templateCommonSourceSeq
  }
  //---------------------------------------------------------------------------
  private def filterSourceInBorder(commonSourceSeq: Array[FovSource], imageDimension: Point2D) = {

    val xAxisBorserSize = imageDimension.x * (xBorderPercentageToIgnore / 100d)
    val yAxisBorserSize = imageDimension.y * (yBorderPercentageToIgnore / 100d)

    val xLeftBorderPix = xAxisBorserSize
    val xRightBorderPix = imageDimension.x - xAxisBorserSize
    val yBottomBorderPix = yAxisBorserSize
    val yTopBorderPix = imageDimension.x - yAxisBorserSize

    commonSourceSeq filter { fov =>
      val pos = fov.getCentroidPix()
      (pos.x >= xLeftBorderPix) && (pos.x <= xRightBorderPix) &&
        (pos.y >= yBottomBorderPix) && (pos.y <= yTopBorderPix)
    }
  }
  //-------------------------------------------------------------------------
  private def generateCsv(sourceEvolutionSeq: Array[SourceEvolution]
                          , oDir:String
                          , estMagID:String) = {
    val dpImageSeq = sourceEvolutionSeq.head.getDP_ImageSeq()

    //image seq
    var dir = Path.resetDirectory(s"$oDir/image_seq" )
    dpImageSeq.foreach { img =>
      val csvName = s"$dir/${img._id}.csv"
      FovSource.saveAsCSV(csvName, img.getCommonSourceSeq(), estMagID)
    }

    //source evolution
    dir = Path.resetDirectory(s"$oDir/source_evolution" )

    //focus source evolution
    val focusSourceEvolution = dpImageSeq map (_.focus)
    FovSource.saveAsCSV(s"$dir/source_${focusSourceEvolution.head.m2_id}_focus.csv", focusSourceEvolution, estMagID)

    //remain source evolution
    sourceEvolutionSeq.foreach { sourceEvolution =>
      val csvName = s"$dir/source_${sourceEvolution.refSource.m2_id}.csv"
      FovSource.saveAsCSV(csvName
        , sourceEvolution.sourceSeq.map(_._2)
        , estMagID
        , sourceEvolution.sourceSeq.map(_._1._id))
    }
  }
  //-------------------------------------------------------------------------
  def filterByEstMag(sourceEvolution: SourceEvolution, estMagID: String) = {
    if (!sourceEvolution.refSource.hasEstMag(estMagID)) None
    else {
      val newSourceEvolution = sourceEvolution.sourceSeq.filter { case (dpImage,fovSource)=>
        dpImage.focus.hasEstMag(estMagID) && fovSource.hasEstMag(estMagID)
      }
      if (newSourceEvolution.isEmpty) None
      else Some(SourceEvolution(sourceEvolution.refImage
        , sourceEvolution.refSource
        , newSourceEvolution))
    }
  }
  //-------------------------------------------------------------------------
  private def writeStatus(summaryFile: BufferedWriter
                         , imageFilter: String
                         , estMagID: String
                         , solutionName: String
                         , status: String
                         , reportStatus: Boolean  = true) = {

    val (catalog,catalogFilter) = MagnitudeEstimation.getCatalogAndFilter(estMagID)
    summaryFile.write(imageFilter + sep +
                      catalog + sep +
                      catalogFilter + sep +
                      solutionName + sep +
                      status + "\n")
    if (reportStatus) error(status)
  }
  //-------------------------------------------------------------------------
  private def calculateSourceEvolution(imageFocus: ImageFocusTrait
                                       , _dpImageSeq: Array[DP_Image]
                                       , focusDB: ImageFocusDB
                                       , imageFilter: String
                                       , solutionName: String
                                       , estMagID: String
                                       , dpImageFocusFluxMap: Map[String, Double]
                                       , summaryFile: BufferedWriter
                                       , isStar: Boolean
                                       , verbose: Boolean): Unit = {
    if (_dpImageSeq.isEmpty) {
      writeStatus(summaryFile, imageFilter, estMagID, solutionName, s"filter: '$imageFilter' solution: '$solutionName' has no images")
      return
    }

    if (_dpImageSeq.head.getCommonSourceSeq() == null ||
      _dpImageSeq.head.getCommonSourceSeq().isEmpty) {
      writeStatus(summaryFile, imageFilter, estMagID, solutionName, s"filter: '$imageFilter' solution: '$solutionName' has no common sources")
      return
    }

    if (!isStar && _dpImageSeq.head.getCommonSourceSeq().size < minCommonSource) {
      writeStatus(summaryFile, imageFilter, estMagID, solutionName, s"filter: '$imageFilter' solution: '$solutionName' has too few common sources")
      return
    }

    val (catalog,catalogFilter) = MagnitudeEstimation.getCatalogAndFilter(estMagID)

    //get the intial evolution for all common sources of the template image
    val templateImage = _dpImageSeq.head
    val templateCommonSourceSeq = templateImage.getCommonSourceSeq()
    val initialSourceEvolutionSeq = (
      for (i <- 0 until templateCommonSourceSeq.size) yield {

        val sourceSeq =  _dpImageSeq flatMap { dpImage =>
          val imageCommonSource = dpImage.getCommonSourceSeq()
          if (imageCommonSource == null ||
              imageCommonSource.isEmpty ||
              imageCommonSource.size < templateCommonSourceSeq.size)
            None
          else Some((dpImage, dpImage.getCommonSourceWithPos(i)))
        }
        SourceEvolution(templateImage , templateCommonSourceSeq(i) , sourceSeq)
      }).toArray

    //process the initial source evolution
    val commonSourceDir = Path.resetDirectory(commonFovRootDir + s"$imageFilter/$solutionName/$DEFAULT_OUTPUT_COMMON_SOURCE_EVOL_DIR/$catalog/$catalogFilter/")
    var sourceEvolutionSeq = initialSourceEvolutionSeq flatMap (filterByEstMag(_, estMagID))

    //processs the source evolution
    val statusSeq = ArrayBuffer[String]()
    sourceEvolutionSeq = SourceEvolution.calculate(
      sourceEvolutionSeq
      , focusDB
      , minCommonSource
      , imageFilter
      , solutionName
      , estMagID
      , statusSeq
      , isStar).getOrElse {

      //status
      writeStatus(summaryFile, imageFilter, estMagID, solutionName, statusSeq.mkString(";"), reportStatus = false)
      Path.deleteDirectoryIfExist(commonSourceDir)
      return
    }

    //status
    writeStatus(summaryFile, imageFilter, estMagID, solutionName, statusSeq.mkString(";"), reportStatus = false)

    //debug
    if (verbose) generateCsv(sourceEvolutionSeq, commonSourceDir, estMagID)

    //plot and rotational period
    val dpImageSeq = sourceEvolutionSeq.head.getDP_ImageSeq().sortWith(_.julian_date < _.julian_date)
    val timeSeq = dpImageSeq map (_.julian_date)

    val estMagSeq = dpImageSeq map (_.focus.absolute_est_mag)
    val estMagRefValue = StatDescriptive.getWithDouble(estMagSeq.filter( !_.isNaN )).median

    if (!dpImageSeq.isEmpty) {
      //generate plot
      SourceEvolution.generatePlot(
        imageFocus.composedName
        , dpImageSeq
        , sourceEvolutionSeq
        , s"${Time.fromJulian(timeSeq.head).toLocalDate} - ${Time.fromJulian(timeSeq.last).toLocalDate}"
        , commonSourceDir
        , dpImageFocusFluxMap)

      //rotational period
      val rotPeriodDir = Path.resetDirectory(rotationalPeriod + s"$imageFilter/$solutionName/$catalog/$catalogFilter/")
      RotationalPeriod.calculateWithResidual(
        imageFocus
        , rotPeriodDir
        , dpImageSeq
        , timeSeq
        , estMagSeq map (_ - estMagRefValue)
        , minPeriodHours
        , maxPeriodHours
        , frequencySamplesPerPeak
        , peakCount
        , fourierSeriesFitDegree
        , bestFreqCount
        , forceRotPeriodHours = None
        , estMagID
        , isAbsoluteEstMag = false)
    }
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file PhotometryDifferential.scala
//=============================================================================