/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  07/Apr/2022
 * Time:  10h:20m
 * Description: Differential photometry image
 */
//=============================================================================
package com.common.image.photometry.differential
//=============================================================================
import com.common.geometry.point.{Point2D, Point2D_Double}
import com.common.image.astrometry.fov.FovSource
import com.common.image.catalog.ObjectDB_Item
import com.common.image.focusType.{ImageFocusStar, ImageFocusTrait}
import com.common.image.photometry.absolute.photometricModel.mpo.{MPO_CatalogDB, MPO_EstMagDB_Item}
import com.common.image.photometry.absolute.photometricModel.star.StarCatalogDB
import com.common.image.photometry.absolute.photometricModel.{ImageFocusDB, PhotometricModelDB_Item}
import com.common.image.photometry.differential.mpo.MPO_PhotDB
import com.common.image.photometry.differential.rotationalPeriod.RotationalPeriod
import com.common.image.photometry.magnitudeEstimation.MagnitudeEstimation
import com.common.logger.MyLogger
import com.common.util.time.Time
//=============================================================================
import org.apache.commons.csv.{CSVFormat, CSVParser}

import java.io.{BufferedReader, File, FileReader}
import scala.collection.JavaConverters.asScalaBufferConverter
//=============================================================================
//=============================================================================
object DP_Image extends MyLogger {
  //---------------------------------------------------------------------------
  private final val rotPeriodInputFormat =
    CSVFormat.DEFAULT
      .withDelimiter('\t')
      .withHeader(RotationalPeriod.CSV_COLUMN_NAME: _*)
      .withSkipHeaderRecord()
  //---------------------------------------------------------------------------
  def apply(image: ObjectDB_Item
            , imageFocus: ImageFocusTrait
            , fovSourceSeq: Seq[FovSource]
            , debugStorage: Boolean): DP_Image = {

    val isStar = imageFocus.isStar
    val objectID = if (debugStorage) imageFocus.getDebuggingID else imageFocus.getID

    val julianDate =
      if (isStar) image.julian_date_mid_point
      else {
        val mpoDB = MPO_CatalogDB(objectID)
        val jd = mpoDB.getJuliandDateCorrected(image._id)
        mpoDB.close()
        jd
      }

    val correctedRaDec =
      if (isStar) Some(imageFocus.asInstanceOf[ImageFocusStar].getEstPosAtTime(julianDate))
      else None

    DP_Image(image._id
      , image.observing_time
      , image.filter
      , image.telescope
      , julianDate
      , Time.getObservingNightDate(julianDate).toString
      , image.noise_tide
      , image.x_pix_size
      , image.y_pix_size
      , image.x_pix_scale
      , image.y_pix_scale
      , correctedRaDec
      , image.photometry_model_seq
      , fovSourceSeq)
  }
  //---------------------------------------------------------------------------
  def loadSeqFromCsv(objectName: String, csvFileName: String) = {
    val br = new BufferedReader(new FileReader(new File(csvFileName)))
    new CSVParser(br, rotPeriodInputFormat).getRecords.asScala.map { r =>
      val imageName = r.get("image_name")
      val julianDate = r.get("julian_date").toDouble
      val flux_per_second = r.get("flux_per_second").toDouble
      val estMag = r.get("estimated_absolute_magnitude").toDouble
      val observingNight = r.get("observing_night")
      val telescope = r.get("telescope")
      val imageFilter = r.get("image_filter")
      val catalog = r.get(MagnitudeEstimation.COL_NAME_CATALOG)
      val catalogFilter = r.get(MagnitudeEstimation.COL_NAME_CATALOG_FILTER)
      val estMagID = MagnitudeEstimation.getEstMagID(catalog,catalogFilter)

      val focusSource = FovSource(
        m2_id = -1
        , focus_id = objectName
        , gaia_id = -1
        , x_pix = -1
        , y_pix = -1
        , ra = -1d
        , dec = -1d
        , flux_per_second = flux_per_second
        , normalized_est_mag = estMag
        , snr = -1
        , absolute_est_mag = estMag
        , estMagID = estMagID)

      DP_Image(
        imageName
        , observing_time = ""
        , imageFilter = imageFilter
        , telescope = telescope
        , julianDate
        , observingNight
        , noise_tide = -1
        , x_pix_size = -1
        , y_pix_size = -1
        , x_pix_scale = -1
        , y_pix_scale = -1
        , raDec = None
        , fovSourceSeq = Seq(focusSource))
    }.toArray
  }
  //---------------------------------------------------------------------------
  def calculateEstAbsoluteMag(dpImage: DP_Image
                              , focusDB: ImageFocusDB
                              , newFlux: Double
                              , estMagID: String): Option[Double] = {
    val estMag = MagnitudeEstimation.fluxToMagnitude(newFlux)

    //update the the normalized magnitude
    if (focusDB.isInstanceOf[StarCatalogDB]) Some(estMag)
    else {
      // using the equations of the absolute magnitude and applying corrections in r, delta and alfa
      val dbExtended = MPO_PhotDB(focusDB.collectionName)
      val mpo = dbExtended.get(dpImage._id)
      val beta =  mpo.getBeta(estMagID).getOrElse {return None}
      val estReducedMag = MPO_EstMagDB_Item.getReducedMagnitude(estMag, mpo.m2_observer_range, mpo.m2_jpl_heliocentric_range)
      Some(MPO_EstMagDB_Item.getAbsoluteMagnitude(estReducedMag, mpo.m2_jpl_phase_angle, beta))
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class DP_Image(_id: String //image name
                    , observing_time: String
                    , imageFilter: String
                    , telescope: String
                    , julian_date: Double
                    , observing_night: String
                    , noise_tide: Double //counts
                    , x_pix_size: Int
                    , y_pix_size: Int
                    , x_pix_scale: Double
                    , y_pix_scale: Double
                    , raDec: Option[Point2D_Double] //use when focus is a star
                    , photometry_model_seq: Seq[PhotometricModelDB_Item] = Seq[PhotometricModelDB_Item]()
                    , fovSourceSeq : Seq[FovSource] = Seq[FovSource]()) extends MyLogger {
  //---------------------------------------------------------------------------
  //kdtree
  private val (kdTree,sourceMap) = FovSource.buildTree(fovSourceSeq)

  //common sources
  private var commonSourceSeq: Array[FovSource] = null

  //focus
  val focus = getFocus()
  val focusRotationalPeriodInDays = scala.collection.mutable.Map[String,Double]()
  //---------------------------------------------------------------------------
  def == (that: DP_Image): Boolean = _id == that._id
  //---------------------------------------------------------------------------
  def simpleClone(): DP_Image = {
   new  DP_Image(
      _id = this._id
      , observing_time = this.observing_time
      , imageFilter = this.imageFilter
      , telescope = this.telescope
      , julian_date = this.julian_date
      , observing_night = this.observing_night
      , noise_tide = this.noise_tide
      , x_pix_size = this.x_pix_size
      , y_pix_size = this.y_pix_size
      , x_pix_scale = this.x_pix_scale
      , y_pix_scale = this.y_pix_scale
      , raDec = this.raDec
      , photometry_model_seq = this.photometry_model_seq
      , fovSourceSeq = this.fovSourceSeq.map(_.simpleClone())
    )
  }
  //---------------------------------------------------------------------------
  def getSource(id: Int) = sourceMap(id)
  //---------------------------------------------------------------------------
  def hasFocus() = focus != null
  //---------------------------------------------------------------------------
  def getCommonSourceSeq() = commonSourceSeq
  //---------------------------------------------------------------------------
  def getCommonSourceWithPos(pos: Int) = commonSourceSeq(pos)
  //---------------------------------------------------------------------------
  def getImageDimensions() = Point2D(x_pix_size,y_pix_size)
  //---------------------------------------------------------------------------
  def commonSourceCalculateRelativeFlux =
    commonSourceSeq map (s => s.est_relative_flux = focus.flux_per_second / s.flux_per_second )
  //---------------------------------------------------------------------------
  private def getFocus(): FovSource = {
    if (raDec.isDefined) findClosestSource(raDec.get)
    else {
      val r = fovSourceSeq.filter(_.isFocus)
      if (r.isEmpty) null
      else r.head
    }
  }
  //---------------------------------------------------------------------------
  def findClosestSource(pos: Point2D_Double) =
    sourceMap(kdTree.getKNN(pos.toSequence()).head.v)
  //---------------------------------------------------------------------------
  def setLocalCommonSourceSeq(seq: Array[FovSource]) =
    commonSourceSeq = seq.map { s => findClosestSource(s.getRaDec()) }
  //---------------------------------------------------------------------------
  def getClosestSourceSeq(maxNeighbours: Int, sourceSeq: Array[FovSource]) = {
    val focusCentralPosition = focus.getRaDec()
    val sourceSeqSortByDistance = (sourceSeq map { source=>
      (source.getRaDec().getDistance(focusCentralPosition), source)
    }).sortWith(_._1 < _._1)
    sourceSeqSortByDistance.take(maxNeighbours).map(_._2)
  }
  //---------------------------------------------------------------------------
  //find the intesection of 'commonSourceSeq' with the sources of the image avoiding
  //the focus of the image
  def findCommonSourceSeq(commonSourceSeq: Array[FovSource]
                          , searchRadiusRa: Double
                          , searchRadiusDec: Double) = {
    val matchedSourceID = commonSourceSeq.flatMap { cs =>
      val r = kdTree.getKNN(cs.getRaDec().toSequence())
      if (!r.isEmpty) {
        val diff = (sourceMap(r.head.v).getRaDec() - cs.getRaDec()).abs()

        if (diff.x < searchRadiusRa &&
          diff.y < searchRadiusDec &&
          cs.m2_id != focus.m2_id) Some(cs.m2_id)
        else None
      }
      else None
    }
    commonSourceSeq.filter(s => matchedSourceID.contains(s.m2_id))
  }
  //---------------------------------------------------------------------------
  def getOffset(imgB: DP_Image) = {
    val estAbsoluteMagDiff = imgB.focus.absolute_est_mag    - focus.absolute_est_mag
    val estMagDiff         = imgB.focus.normalized_est_mag  - focus.normalized_est_mag
    estAbsoluteMagDiff - estMagDiff
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file DP_Image.scala
//=============================================================================
