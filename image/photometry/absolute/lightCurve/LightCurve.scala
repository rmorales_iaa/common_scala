/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Jan/2023
 * Time:  11h:39m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.absolute.lightCurve
//=============================================================================
import com.common.configuration.MyConf
import com.common.csv.{CsvItem, CsvRead, CsvRow, CsvWrite}
import com.common.image.catalog.ImageCatalog.{ImageByFilterMap, ImagePhotometricMap, ImageSourceFocusMap}
import com.common.image.catalog.{ImageCatalog, ObjectDB_Item, SourceDB_Item}
import com.common.image.focusType.ImageFocusTrait
import com.common.image.photometry.absolute.PhotometryAbsolute
import com.common.image.photometry.absolute.PhotometryAbsolute._
import com.common.image.photometry.absolute.photometricModel._
import com.common.util.path.Path
import com.common.image.photometry.absolute.photometricModel.mpo.{MPO_CatalogDB, MPO_DB_Item, MPO_EstMagDB_Item}
import com.common.image.photometry.absolute.photometricModel.star.{StarCatalogDB, StarEstMagDB_Item}
import com.common.image.photometry.differential.DP_Image
import com.common.image.photometry.differential.rotationalPeriod.RotationalPeriod
import com.common.logger.MyLogger
import com.common.stat.StatDescriptive
import com.common.util.file.MyFile
import com.common.image.photometry.magnitudeEstimation.{DP_ImageGroup, MagnitudeEstimation}
//=============================================================================
import java.io.BufferedWriter
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object LightCurve extends MyLogger {
  //---------------------------------------------------------------------------
  //configuration parameters
  private final val conf = MyConf(MyConf.c.getString("LightCurve.configurationFile"))

  private final val minValidMagnitude = conf.getDouble("LightCurve.sourceFilter.minValidMagnitude")
  private final val maxValidMagnitude = conf.getDouble("LightCurve.sourceFilter.maxValidMagnitude")

  private final val sigmaScale              = conf.getDouble("LightCurve.rotationalPeriod.sigmaScale")
  private final val minPeriodHours          = conf.getDouble("LightCurve.rotationalPeriod.minPeriodHours")
  private final val maxPeriodHours          = conf.getDouble("LightCurve.rotationalPeriod.maxPeriodHours")
  private final val frequencySamplesPerPeak = conf.getInt("LightCurve.rotationalPeriod.peakCount")
  private final val peakCount               = conf.getInt("LightCurve.rotationalPeriod.peakCount")
  private final val fourierSeriesFitDegree  = conf.getInt("LightCurve.rotationalPeriod.fourierSeriesFitDegree")

  private final val bestFreqCount           = MyConf.c.getInt("RotationalPeriod.best")
  //---------------------------------------------------------------------------
  private final val sep = "\t"
  //---------------------------------------------------------------------------
  private final val LIGHT_CURVE_DIR                         =  "/light_curve/"

  private final val LIGHT_CURVE_BY_CATALOG_FILTER_DIR       =  "by_catalog_filter"

  private final val LIGHT_CURVE_CSV_FILE_NAME               = "light_curve.csv"
  final val JPL_ORBIT_RESIDUALS_SUMMARY_CSV_FILE_NAME       = "jpl_orbit_residuals_summary.csv"

  private final val COMPILED_RESULTS_DIRECTORY              = "3_compiled_results"

  private final val DISCARDED_IMAGE_SEQ_BY_EST_MAG_CSV      = "discarded_image_seq_by_est_mag.csv"
  //---------------------------------------------------------------------------
  private val plot_name = Seq(
    "plot_jd_est_magnitude"
    , "plot_jd_est_reduced_magnitude"
    , "plot_jd_est_absolute_magnitude"
    , "plot_phase_angle_reduced_magnitude")
  //---------------------------------------------------------------------------
  private val plot_title= Seq(
    "Estimated magnitude"
    , "Reduced magnitude (r=1,au=1,alpha)"
    , "Absolute magnitude (H)"
    , "Reduced magnitude (r=1,au=1,alpha) vs phase angle" )
  //---------------------------------------------------------------------------
  private final val PLOT_EST_MAGNITUDE_JULIAN_DATE_AXIS_FORMAT  = "%.4s%c"
  private final val plot_x_axis_format_seq = Seq(
    PLOT_EST_MAGNITUDE_JULIAN_DATE_AXIS_FORMAT
    , PLOT_EST_MAGNITUDE_JULIAN_DATE_AXIS_FORMAT
    , PLOT_EST_MAGNITUDE_JULIAN_DATE_AXIS_FORMAT
    , "")
  //---------------------------------------------------------------------------
  private final val PLOT_EST_MAGNITUDE_JULIAN_DATE_X_AXIS_LABEL = "julian date"
  private final val plot_x_col_name_seq = Seq(
    PLOT_EST_MAGNITUDE_JULIAN_DATE_X_AXIS_LABEL
    , PLOT_EST_MAGNITUDE_JULIAN_DATE_X_AXIS_LABEL
    , PLOT_EST_MAGNITUDE_JULIAN_DATE_X_AXIS_LABEL
    , "Phase angle" )
  //---------------------------------------------------------------------------
  private final val plot_x_col_pos_seq = Seq(11,11,11,16)
  private final val plot_y_col_pos_seq = Seq(12,14,15,14)
  //---------------------------------------------------------------------------
  private val plot_enableCuadrticFit = Seq(false,false,false,true)
  //---------------------------------------------------------------------------
  private val plot_star_max_index_incl     = 1 //included
  private val plot_mpo_max_index_incl = 3 //included
  //---------------------------------------------------------------------------
  private def generatePlot(index: Int
                           , objectName: String
                           , composedFilterName: String
                           , vr: Double
                           , oDirSeq: Seq[String]) = {
    for(oDir <- oDirSeq) {
      val fileName = oDir + s"${plot_name(index)}.gnuplot"
      if (!MyFile.fileExist(fileName)) {
        GnuPlotScript.magnitudeJulianDate(
          fileName
          , dataFileNameCsv = oDir + LIGHT_CURVE_CSV_FILE_NAME
          , title = plot_title(index) + s" for filter '$composedFilterName' with V-R: $vr"
          , x_axis_label = plot_x_col_name_seq(index)
          , y_axis_label = s"Estimated magnitude for filter $composedFilterName"
          , posColX = plot_x_col_pos_seq(index)
          , posColY = plot_y_col_pos_seq(index)
          , objectName = objectName
          , xAxisFormat = plot_x_axis_format_seq(index)
          , plot_enableCuadrticFit(index))
      }
    }
  }
  //---------------------------------------------------------------------------
  private def calculateRotPeriod(imageFocus: ImageFocusTrait
                                 , dpImageSeq: Array[DP_Image]
                                 , estMagID: String
                                 , catalogFilter: String
                                 , oDir: String
                                 , verbose: Boolean = false): Unit = {

    info(s"Starting with ${dpImageSeq.length} points before getting the estimate id:'$estMagID' of filter:'$catalogFilter'")

    //calculate the estimated magnitude
    val estMagPairSeq = dpImageSeq.flatMap { dpImage =>
      val estMag = dpImage.focus.getEstMag(estMagID, useAbsoluteMag = !imageFocus.isStar)
      if(estMag.isNaN || estMag.isInfinite) {
        if (verbose) warning(s"The image: '${dpImage._id}' has an invalid manitude: '$estMag' for estamation ID: '$estMagID' of filter:'$catalogFilter'. Ignoring it")
        None
      }
      else Some((estMag, dpImage))
    }

    info(s"Starting with ${estMagPairSeq.length} points before applying the the sigma clipping with estimate id:'$estMagID' of filter:'$catalogFilter'")

    //apply sigma clipping
    val sortedFilteredPair =
      (if (sigmaScale > 0) StatDescriptive.sigmaClippingDoubleWithPair(estMagPairSeq, sigmaScale = sigmaScale)
       else estMagPairSeq)
        .sortWith(_._2.julian_date < _._2.julian_date)

    info(s"Remain: ${sortedFilteredPair.length} points after applying the the sigma clipping filter with sigma:'$sigmaScale' with estimate id:'$estMagID' of filter:'$catalogFilter'")

    if (sortedFilteredPair.size < 2) {
      warning(s"No enough points to calculate the rotation period in estimate id:'$estMagID' of filter:'$catalogFilter'")
      return
    }

    //calculate rotational period
    val imageSeq = sortedFilteredPair map { t => t._2 }
    val estMagSeq = sortedFilteredPair map { t =>
      t._2.focus.absolute_est_mag = t._1
      t._1}
    val timeSeq   = sortedFilteredPair map { t => t._2.julian_date }
    RotationalPeriod.calculateWithResidual(
      imageFocus
      , oDir
      , imageSeq
      , timeSeq
      , _estMagSeq = estMagSeq
      , minPeriodHours
      , maxPeriodHours
      , frequencySamplesPerPeak
      , peakCount
      , fourierSeriesFitDegree
      , bestFreqCount
      , forceRotPeriodHours = None
      , estMagID
      , isAbsoluteEstMag = true
      , verbose = true)
  }
  //---------------------------------------------------------------------------
  def getPhotometricDatabaseAndFilterSeq(catalogPhotometricModelMap: ImagePhotometricMap) = {
    val catalogSeq = catalogPhotometricModelMap.head._2.keys.toSet
    val catalogFilterSeq = (catalogPhotometricModelMap.values map (t => t.values.map(t => t.keys)))
      .flatten
      .flatten
      .toSet
      .toArray
      .sorted
      .reverse
    (catalogSeq,catalogFilterSeq.toSet)
  }
  //---------------------------------------------------------------------------
  private def calculateEstMagSeq(sourceFocusMap: ImageSourceFocusMap
                                 , imageByFilterMap: ImageByFilterMap
                                 , catalogPhotometricModelMap: ImagePhotometricMap
                                 , vr: Double
                                 , errorMagEstSeq: ArrayBuffer[(ImageFocusDB_Item,MagnitudeEstimation)]) = {

    info("Estimating all photometric models in all focus of all images")

    val (catalogSeq,catalogFilterSeq) = getPhotometricDatabaseAndFilterSeq(catalogPhotometricModelMap)
    warning(s"Found:'${catalogSeq.size}' photometric catalog:${catalogSeq.mkString("{",",","}")}")
    warning(s"Found:'${catalogFilterSeq.size}' photometric catalog filters:${catalogFilterSeq.mkString("{",",","}")}")

    //iterate over all images using image filter partition
    for ((_, imageNameSeq) <- imageByFilterMap) {
      val sameFilterPairSeq = (imageNameSeq flatMap { imageName =>
        if(sourceFocusMap.contains(imageName)) Some(sourceFocusMap(imageName))
        else {
          error(s"Can not estimate the magnitude of:'$imageName'. Image does has no source focus")
          None}})
        .flatten
        .toArray
      sameFilterPairSeq.foreach { case (imageSource, imageFocus) =>
        val estMagSeq = ArrayBuffer[MagnitudeEstimation]()
        catalogSeq.foreach { catalog =>
          catalogFilterSeq.foreach { catalogFilter =>
            val id = imageFocus._id
            if (catalogPhotometricModelMap.contains(id) &&
              catalogPhotometricModelMap(id).contains(catalog) &&
              catalogPhotometricModelMap(id)(catalog).contains(catalogFilter)) {
              val estMagnitude = StarEstMagDB_Item(
                catalog //use it for all types of objects
                , catalogFilter
                , imageSource
                , imageFocus
                , catalogPhotometricModelMap(id)(catalog)(catalogFilter)
                , vr)
              if (estMagnitude.est_magnitude < minValidMagnitude ||
                  estMagnitude.est_magnitude > maxValidMagnitude)
                errorMagEstSeq += ((imageFocus,estMagnitude))
              else estMagSeq += estMagnitude
            }
          }
        }
        imageFocus.magnitude_estimation_seq = estMagSeq.toList
      }
    }
  }
  //---------------------------------------------------------------------------
  private def saveCsvEstMagSeq(source: SourceDB_Item
                               , focusSource: ImageFocusDB_Item
                               , filteredEstMagSeq: Seq[MagnitudeEstimation]
                               , imageFilter: String
                               , catalog: String
                               , catalogFilter: String
                               , telescope: String
                               , vr: Double
                               , csvFile: BufferedWriter
                               , isStar: Boolean) = {

    filteredEstMagSeq.filter {_.est_magnitude > 0}.foreach { estMag =>
      if (isStar)
        estMag.asInstanceOf[StarEstMagDB_Item].saveAsCsvLine(
          focusSource._id
          , imageFilter
          , focusSource.julian_date
          , source
          , catalog
          , catalogFilter
          , vr
          , Seq(csvFile)
          , sep)
      else
        estMag.asInstanceOf[MPO_EstMagDB_Item].saveAsCsvLine(
          focusSource._id
          , imageFilter
          , focusSource.julian_date
          , source
          , catalog
          , catalogFilter
          , vr
          , Seq(csvFile)
          , mpcID = focusSource.asInstanceOf[MPO_DB_Item].focus_id.split("_")(1)
          , telescope
          , sep)
    }
  }
  //---------------------------------------------------------------------------
  private def generatePlots(imageFocus: ImageFocusTrait
                            , catalog: String
                            , catalogFilter: String
                            , vr: Double
                            , maxPlotIndex: Int
                            , catalogFilterDir: String) = {
    //plots
    val composedFilterName = s"${catalog.replaceAll(" ", "_")}_$catalogFilter"
    for (index <- 0 to maxPlotIndex) {
      generatePlot(
        index
        , imageFocus.getID
        , composedFilterName
        , vr
        , oDirSeq = Seq(Path.getCurrentPath() + "/" + catalogFilterDir)
      )
    }
  }
  //---------------------------------------------------------------------------
  private def saveLightCurveByCatalogFilter(imageFocus: ImageFocusTrait
                                            , focusSourcePairSeq: Seq[(SourceDB_Item, ImageFocusDB_Item)]
                                            , catalog: String
                                            , catalogFilter: String
                                            , imageInfoMap: Map[String, (String, String)]
                                            , imageFilter: String
                                            , vr: Double
                                            , outputDir: String) = {
    //-------------------------------------------------------------------------
    val isStar = imageFocus.isStar
    val maxPlotIndex = if (isStar) plot_star_max_index_incl else plot_mpo_max_index_incl

    info(s"\tPhotometric model catalog:'$catalog' and catalog filter:'$catalogFilter'")
    val csvFile =
      if (isStar) StarEstMagDB_Item.createCsv(outputDir + LIGHT_CURVE_CSV_FILE_NAME, sep)
      else MPO_EstMagDB_Item.createCsv(outputDir + LIGHT_CURVE_CSV_FILE_NAME, sep)

    //generate plots
    generatePlots(
      imageFocus
      , catalog
      , catalogFilter
      , vr
      , maxPlotIndex
      , outputDir)

    //process the estimated magnitude sequence
    focusSourcePairSeq.foreach { case (source, focusSource) =>

      //the magnitude estimate for this catalog and catalog filter
      val filteredMagEstSeq = focusSource.magnitude_estimation_seq.filter { magEst =>
        magEst.filterByCatalogAndFilter(catalog, catalogFilter)
      }.sortWith(_.julian_date < _.julian_date)

      //save the csv with the magnitude estimate
      saveCsvEstMagSeq(
        source
        , focusSource
        , filteredMagEstSeq
        , imageFilter
        , catalog
        , catalogFilter
        , telescope = imageInfoMap(focusSource._id)._2
        , vr
        , csvFile
        , isStar)

    }
    csvFile.close()
  }
  //---------------------------------------------------------------------------
  private def saveLightCurve(imageFocus: ImageFocusTrait
                             , sourceFocusMap: ImageSourceFocusMap
                             , imageGroupMap: Map[String,DP_ImageGroup]
                             , imageInfoMap: Map[String, (String, String)]
                             , vr: Double
                             , rotPeriod: Boolean
                             , lightCurveRootPath: String): Unit = {

    val isStar = imageFocus.isStar

    imageGroupMap
      .toSeq
      .sortWith(_._1 < _._1)
      .foreach { case (groupID,dpImageGroup) =>
        val imageFilter =  dpImageGroup.getImageFilterID
        val imageNameSeq = dpImageGroup.getImageNameSeq()
        info (s"Saving light curve for: '$groupID' image filter:'$imageFilter' image count: ${imageNameSeq.size}")
        val pairSeq =
          if (isStar) ImageFocusDB.getStarFocusSeq(sourceFocusMap, imageNameSeq)
          else ImageFocusDB.getMpoFocusSeq(sourceFocusMap, imageNameSeq)

        val outputDir = Path.resetDirectory(s"$lightCurveRootPath/$imageFilter/$LIGHT_CURVE_BY_CATALOG_FILTER_DIR/${dpImageGroup.catalog}/${dpImageGroup.catalogFilter}")

        saveLightCurveByCatalogFilter(
          imageFocus
          , pairSeq
          , dpImageGroup.catalog
          , dpImageGroup.catalogFilter
          , imageInfoMap
          , imageFilter
          , vr
          , outputDir)

        //rotational period
        if (rotPeriod)
          calculateRotPeriod(
            imageFocus
            , dpImageGroup.getDP_ImageSeq
            , dpImageGroup.estMagID
            , dpImageGroup.catalogFilter
            , Path.resetDirectory(s"$outputDir/${RotationalPeriod.ROTATIONAL_PERIOD_DIR}"))

      }
  }
  //---------------------------------------------------------------------------
  private def reportEstimatedMagSeq(sourceFocusMap:ImageSourceFocusMap) = {
    sourceFocusMap.foreach { case (imageName, pairSeq) =>
      pairSeq.foreach { case (_, focusSource) =>
        focusSource.magnitude_estimation_seq.foreach { estMag =>
          info(s"Image: '$imageName' catalog filter: '${MagnitudeEstimation.getEstMagID(estMag.catalog,estMag.catalog_filter)}' est_mag: ${f"${estMag.est_magnitude}%.3f"}")
        }
      }
    }
  }

  //---------------------------------------------------------------------------
  private def reportErrorEstimatedMagSeq(photometryRootPath: String
                                         , errEstMagSeq: Array[(ImageFocusDB_Item,MagnitudeEstimation)]) = {
    //-------------------------------------------------------------------------
    val csvFilename = photometryRootPath + s"/$DISCARDED_IMAGE_SEQ_BY_EST_MAG_CSV"
    warning(s"Ignoring: '${errEstMagSeq.length}' estimated magnitudes after applying the filter range[$minValidMagnitude,$maxValidMagnitude]. Please see '$csvFilename'")
    //-------------------------------------------------------------------------
    val itemSeq = errEstMagSeq map (_._1)
    val estSeq = errEstMagSeq map (_._2)
    //-------------------------------------------------------------------------
    def getEstMagMap(est: MagnitudeEstimation) = {
      val valueSeq = Seq(est.catalog
        , est.catalog_filter
        , est.julian_date.toString
        , est.est_magnitude.toString
        , est.est_magnitude_error.toString)

      (MagnitudeEstimation.headerCsv zip valueSeq).toMap
    }
    //-------------------------------------------------------------------------
    val headerRow = MPO_DB_Item.headerRow
    MagnitudeEstimation.headerCsv map (headerRow + CsvItem(_))
    val csvFile = CsvWrite(csvFilename, headerRow)

    val rowSeq =
      if (itemSeq.head.isInstanceOf[MPO_DB_Item])
        (itemSeq zip estSeq) map {case (a,b)=>
          CsvRow(a.asInstanceOf[MPO_DB_Item].toMap() ++ getEstMagMap(b))
        }
      else
        (itemSeq zip estSeq) map { case (a, b) =>
          CsvRow(a.asInstanceOf[MPO_DB_Item].toMap() ++ getEstMagMap(b))
        }
    csvFile.append(rowSeq)
    csvFile.close()
  }
  //---------------------------------------------------------------------------
  private def mpoUpdateEstMag(sourceFocusMap: ImageSourceFocusMap
                              , catalogPhotometricModelMap: ImagePhotometricMap) = {

    info("Updating mpo estimated magnitudes")
    val (estBetaMap, estReducedMagnitudeMap) =
      MPO_CatalogDB.calculateBetaMap(sourceFocusMap
                                     , catalogPhotometricModelMap)

    sourceFocusMap.foreach { case (_, pairSeq) =>
      pairSeq.foreach { case (_, focusSource) =>
        val mpo = focusSource.asInstanceOf[MPO_DB_Item]
        if (mpo.magnitude_estimation_seq != null) {
          val newEstMagSeq = mpo.magnitude_estimation_seq.flatMap { estMag =>

            val estMagID = estMag.getID
            if (!estBetaMap.contains(estMagID) ||
              !estReducedMagnitudeMap.contains(mpo._id) ||
              !estReducedMagnitudeMap(mpo._id).contains(estMag.getID))
              None
            else {
              val beta = estBetaMap(estMag.getID)
              val estReducedMag = estReducedMagnitudeMap(mpo._id)(estMag.getID)
              val estAbsoluteMag = MPO_EstMagDB_Item.getAbsoluteMagnitude(estReducedMag, mpo.m2_jpl_phase_angle, beta)

              Some(new MPO_EstMagDB_Item(
                estMag.catalog
                , estMag.catalog_filter
                , mpo.m2_jpl_julian_date_mid_point_corrected
                , estMag.est_magnitude
                , estMag.est_magnitude_error
                , estReducedMag
                , estAbsoluteMag
                , mpo.m2_jpl_phase_angle
                , estBetaMap(estMag.getID)))
            }
          }
          mpo.magnitude_estimation_seq = newEstMagSeq.asInstanceOf[List[MagnitudeEstimation]]
        }
      }
    }
  }
  //---------------------------------------------------------------------------
  def compiledResultsSummary(objectDetectionRootPath: String
                             , astrometryRootPath: String
                             , photometryAbsoluteRootPath: String
                             , isStar: Boolean
                             , debugStorage : Boolean = false): Unit = {
    //-------------------------------------------------------------------------
    def loadLightCurveCsvSeq(inputDir: String) = {
      Path.getSortedFileListRecursive(
        inputDir
        , fileExtension = Seq(".csv")
        , pattern = Some(".*" + Path.getOnlyFilenameNoExtension(LIGHT_CURVE_CSV_FILE_NAME) + ".*"))
        . map { file=>
          CsvRead.readCsv(file.getAbsolutePath)
        }
    }
    //-------------------------------------------------------------------------
    def getImageFilterSeq =
      (Path.getSubDirectoryList(photometryAbsoluteRootPath +  LIGHT_CURVE_DIR) map (file =>
        Path.getOnlyFilenameNoExtension(file.getAbsolutePath))).sorted
    //-------------------------------------------------------------------------
    val joinColNameImageName = ObjectDB_Item.COL_NAME_IMAGE_NAME
    val joinColNameID        = ObjectDB_Item.COL_NAME_ID
    val imageFilterSeq        = getImageFilterSeq
    val csvMetadata          = CsvRead.readCsv(s"$objectDetectionRootPath/$PHOTOMETRY_ABSOLUTE_IMAGE_METADATA_CSV", verbose = true)
    val csvDetectionSummary  = CsvRead.readCsv(s"$objectDetectionRootPath/$PHOTOMETRY_ABSOLUTE_DETECTION_SUMMARY_CSV", verbose = true)
    val csvAstrometry        = if (isStar) csvMetadata else CsvRead.readCsv(s"$astrometryRootPath/$JPL_ORBIT_RESIDUALS_SUMMARY_CSV_FILE_NAME", verbose = true)
    val csvPhotometricModel  = CsvRead.readCsv(s"$photometryAbsoluteRootPath/${PhotometryAbsolute.PHOTOMETRY_ABSOLUTE_PHOTOMETRIC_MODELS_SUMMARY_CSV}", verbose = true)
    var compiledResultsCsv : CsvRead = null

    if (csvMetadata.isEmpty || csvDetectionSummary.isEmpty || csvAstrometry.isEmpty)
      return

    //join image metadata with image detection
    csvDetectionSummary.removeColumn(Seq("status", "telescope","filter","exposure_time","observing_timestmap","background","background_rms","sigma_multipler","fwhm"))
    compiledResultsCsv = csvMetadata.join(csvDetectionSummary
                                          , joinColNameImageName
                                          , joinColNameImageName
                                          , verbose = false)

    //remove duplicated
    compiledResultsCsv.removeDuplicated(Seq(joinColNameImageName))

    val outputDir =  Path.resetDirectory(astrometryRootPath + s"../$COMPILED_RESULTS_DIRECTORY")

    for (imageFilter <- imageFilterSeq) {

      val csvLightCurveSeq = loadLightCurveCsvSeq(s"$photometryAbsoluteRootPath/$LIGHT_CURVE_DIR/${DP_ImageGroup.DP_IMAGE_GROUP_ALL_IMAGE_FILTERS}/$LIGHT_CURVE_BY_CATALOG_FILTER_DIR/")
      info(s"Image filter: '$imageFilter' joining metadata and astrometry")
      val csvImage = if (isStar) compiledResultsCsv
      else compiledResultsCsv.join(csvAstrometry, joinColNameImageName, joinColNameID, verbose = false)

      //remove duplicated columns in the new csv
      csvImage.removeColumn(Seq(ObjectDB_Item.COL_NAME_ID))

      info(s"Image filter: '$imageFilter' join all light curves corresponding to: ${csvLightCurveSeq.size} catalog photometric filters")
      csvLightCurveSeq foreach { csvLightCurve =>

        if (csvLightCurve.hasRows) {
          val catalog = csvLightCurve(MagnitudeEstimation.COL_NAME_CATALOG).head
          val catalogFilter = csvLightCurve(MagnitudeEstimation.COL_NAME_CATALOG_FILTER).head
          val estMagID = MagnitudeEstimation.getEstMagID(catalog, catalogFilter)
          info(s"Image filter: '$imageFilter' joining light curve: '$estMagID'")

          //prepare the light curve csv
          csvLightCurve.removeColumn(Seq(SourceDB_Item.COL_NAME_M2_ID
            , SourceDB_Item.COL_NAME_FOCUS_ID
            , SourceDB_Item.COL_NAME_GAIA_ID
            , ObjectDB_Item.COL_NAME_JULIAN_DATE))

          //join light curve
          compiledResultsCsv = CsvRead(csvImage)
          compiledResultsCsv = compiledResultsCsv.join(csvLightCurve, joinColNameImageName, joinColNameImageName, verbose = false)

          if (compiledResultsCsv.isEmpty) warning(s"The joined csv with:'${csvLightCurve.fileName}' is empty")
          else {
            if (!csvPhotometricModel.isEmpty) {
              //create a new csv photometric model that match with light curve
              val csvPhotometric = CsvRead("", csvPhotometricModel.hasHeader)
              val csvPhotometricModelRow = csvPhotometricModel.filterRow(Seq(MagnitudeEstimation.COL_NAME_CATALOG
                , MagnitudeEstimation.COL_NAME_CATALOG_FILTER)
                , Seq(catalog, catalogFilter))

              if (csvPhotometricModelRow.size > 0) {
                csvPhotometricModelRow map { row => csvPhotometric + row }
                csvPhotometric.setColNameSeq(csvPhotometricModelRow.head.getColNameSeq())

                //join photometric model
                compiledResultsCsv = compiledResultsCsv.join(csvPhotometric, joinColNameImageName, joinColNameImageName, verbose = false)

                //prepare the columns of the final csv to avoid duplicates
                csvLightCurve.removeColumn(Seq(PhotometricModel.COL_NAME_IMAGE_NAME, MPO_EstMagDB_Item.PHASE_ANGLE))
                compiledResultsCsv.renameColumn(Seq(ObjectDB_Item.COL_NAME_ID), Seq(PhotometricModel.COL_NAME_IMAGE_NAME))

                //save the compiled csv
                compiledResultsCsv.save(s"$outputDir/$estMagID.csv")
              }
            }
          }
        }
      }
    }
  }
  //---------------------------------------------------------------------------
  def calculate(imageFocus: ImageFocusTrait
                , vr: Double
                , rotPeriod: Boolean
                , astrometryRootPath: String
                , photometryRootPath: String
                , spkVersion: Option[String] = None
                , debugStorage : Boolean = false
                , verbose: Boolean): Unit = {

    val lightCurveRootPath = Path.resetDirectory(photometryRootPath + LIGHT_CURVE_DIR)
    info(s"Getting the focus on images with object: '${imageFocus.getID}'")

    val objectID = if (debugStorage) imageFocus.getDebuggingID else imageFocus.getID
    val imageDB                                = ImageCatalog(objectID)
    val imageInfoMap                           = imageDB.getFilterAndTelescopeMap()   //(name,filter,telescope)
    var imageNameSeq                           = imageInfoMap.keys.toArray
    val catalogPhotometricModelMap             = imageDB.getCatalogPhotometricModelMap(imageNameSeq)
    val (sourceFocusMap,imageByImageFilterMap) = imageDB.getPartitionByFocusImageFilterMap(imageNameSeq, vr, imageFocus.isStar, spkVersion)

    imageNameSeq = catalogPhotometricModelMap.keys.toArray

    if (imageNameSeq.isEmpty) {
      error(s"The object: '${imageFocus.getID}' has no images stored")
      return
    }

    if (catalogPhotometricModelMap.isEmpty) {
      error(s"The object: '${imageFocus.getID}' has no photometric models")
      return
    }

    //estimate the focus magnitude with all photometric models
    val errorMagEstSeq = ArrayBuffer[(ImageFocusDB_Item,MagnitudeEstimation)]()
    calculateEstMagSeq(
        sourceFocusMap
      , imageByImageFilterMap
      , catalogPhotometricModelMap
      , vr
      , errorMagEstSeq)

    //report the errors in the magnitude estimation
    if (!errorMagEstSeq.isEmpty)
       reportErrorEstimatedMagSeq(photometryRootPath, errorMagEstSeq.toArray)

    //report calculated magnitudes
    if (verbose) reportEstimatedMagSeq(sourceFocusMap)

    //calculate the beta map
    if (!imageFocus.isStar) mpoUpdateEstMag(sourceFocusMap, catalogPhotometricModelMap)

    //add to database
    if (imageFocus.isStar)
      StarCatalogDB.addToDatabase(
          objectID
        , sourceFocusMap
        , imageByImageFilterMap)
    else
      MPO_CatalogDB.addToDatabase(
          objectID
        , sourceFocusMap
        , imageByImageFilterMap)

    //get only the images with the focus
    imageNameSeq = ImageFocusDB.getImageNameSeq(imageFocus,objectID)

    //build the image group
    val (catalogSeq,catalogFilterSeq) = getPhotometricDatabaseAndFilterSeq(catalogPhotometricModelMap)
    val imageGroupMap = DP_ImageGroup.buildGroupMap(imageFocus
      , imageDB
      , sourceFocusMap
      , imageNameSeq
      , imageFilterSet = imageByImageFilterMap.keys.toSet
      , catalogSeq
      , catalogFilterSeq
      , debugStorage)

    //save light curve
    saveLightCurve(
        imageFocus
      , sourceFocusMap
      , imageGroupMap
      , imageInfoMap
      , vr
      , rotPeriod
      , lightCurveRootPath)

    if (!imageFocus.isStar) {
      info(s"Saving JPL's orbit residual of object: '${imageFocus.composedName}'")
      MPO_CatalogDB.saveJplOrbitResiduals(astrometryRootPath, sourceFocusMap, imageByImageFilterMap)
    }

    imageDB.close()
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file LightCurve.scala
//=============================================================================
