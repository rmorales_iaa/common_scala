/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Dec/2021
 * Time:  20h:46m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.absolute.lightCurve
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object GnuPlotScript {
  //---------------------------------------------------------------------------
  private def normalizeName(s:String) =
    s.replaceAll("_","""\\\\_""")
  //---------------------------------------------------------------------------
  def magnitudeJulianDate(fileName: String
                          , dataFileNameCsv: String
                          , title: String
                          , x_axis_label: String
                          , y_axis_label: String
                          , posColX: Int
                          , posColY: Int
                          , objectName: String
                          , xAxisFormat: String =""
                          , enableCuadrticFit: Boolean = false) {

    val xAxisFormatString = if (xAxisFormat.isEmpty) "" else  s"""set format x "$xAxisFormat"""" + "\n"
    val enableCuadrticFitString = if (!enableCuadrticFit) "" else s"""cuadratic_fit(x) w l lc rgb "red" ti sprintf("Cuadratic fit: f(x)=a^x2+bx+c, a=%g, b=%g, c=%g",ca,cb,cc)"""

    val s = s"""
#star of gnu script
#--------------------------------------
#data file separator
set datafile separator "\\t"
#--------------------------------------
#variables
my_title = \"${normalizeName(title)}'\"

my_x_axis_label = '${normalizeName(x_axis_label)}'
my_y_axis_label = '${normalizeName(y_axis_label)}'

my_col_x = $posColX
my_col_y = $posColY

csv = '$dataFileNameCsv'
#--------------------------------------
#fonts
my_axis_ticks_font = "Courier-New,11"
my_axis_title_font = "Times-Roman,11"
#--------------------------------------
#colors
#background
my_plot_background_color = "#dbdcab"
#--------------------------------------
#title
set title font ",14"
set title my_title
#--------------------------------------
#title
my_point_type = 1   #circle
my_point_size = 1
#--------------------------------------
#plot background color
set object 1 rectangle from screen 0,0 to screen 1,1 fillcolor rgb my_plot_background_color behind
#--------------------------------------
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
set grid
#--------------------------------------
#no fit log file
set fit quiet
set fit logfile '/dev/null'
#--------------------------------------
#axis
set xtics font my_axis_ticks_font
set ytics font my_axis_ticks_font

set xtic auto
set ytic auto

$xAxisFormatString

set xlabel my_x_axis_label font my_axis_title_font
set ylabel my_y_axis_label font my_axis_title_font
#--------------------------------------
linear_fit(x) = la*x + lb
fit linear_fit(x) csv using my_col_x:my_col_y via la,lb
#--------------------------------------
cuadratic_fit(x) = ca*x**2 + cb*x + cc
fit cuadratic_fit(x) csv using my_col_x:my_col_y via ca,cb,cc
#--------------------------------------
plot csv  \\
    using my_col_x:my_col_y with points pointtype my_point_type pointsize my_point_size lc rgb "blue" title "{/:Bold '$objectName'} curve" , \\
    linear_fit(x) w l lc rgb "black" ti sprintf("Linear fit: f(x)=a*x+b, a=%g, b=%g",la,lb), \\
    $enableCuadrticFitString
#--------------------------------------
pause -1

#--------------------------------------
#end of gnu script""".stripMargin

    val bw = new BufferedWriter(new FileWriter(new File(fileName)))
    bw.write(s)
    bw.close()
  }
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
//=============================================================================
//End of file GnuPlotScript.scala
//=============================================================================
