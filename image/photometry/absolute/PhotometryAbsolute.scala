/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  30/Mar/2021
 * Time:  18h:47m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.absolute
//=============================================================================
import com.common.configuration.{MyConf, MyConfTrait}
import com.common.coordinate.conversion.Conversion
import com.common.csv.{CsvItem, CsvRow}
import com.common.dataType.pixelDataType.PixelDataType
import com.common.dataType.pixelDataType.PixelDataType.{PIXEL_DATA_TYPE, PIXEL_ZERO_VALUE}
import com.common.dataType.tree.kdTree.K2d_TreeDouble
import com.common.database.mongoDB.database.MatchedImageSource
import com.common.database.mongoDB.database.gaia.{GaiaDB, GaiaPhotometricSource, GaiaSourceReduced}
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.simpleFits.SimpleFits._
import com.common.fits.wcs.WCS
import com.common.geometry.point.{Point2D, Point2D_Double}
import com.common.geometry.rectangle.Rectangle
import com.common.geometry.segment.segment2D._
import com.common.hardware.cpu.CPU
import com.common.image.astrometry.GaiaCatalog
import com.common.image.catalog.ObjectDB_Item.{COL_NAME_FWHM, _}
import com.common.image.catalog.SourceDB_Item._
import com.common.image.catalog.{ImageCatalog, SourceDB_Item}
import com.common.image.estimator.centroid.Centroid
import com.common.image.estimator.flux.ApertureTrait
import com.common.image.estimator.fwhm.Fwhm
import com.common.image.focusType._
import com.common.image.mpo.asteroidChecker.{AsteroidChecker, MinorPlanetCenter}
import com.common.image.myImage.MyImage
import com.common.image.photometry.absolute.photometricModel.PhotometricModel
import com.common.image.photometry.absolute.photometricModel.PhotometricModel.headerCsv
import com.common.image.photometry.absolute.photometricModel.mpo.MPO_CatalogDB
import com.common.image.photometry.absolute.photometricModel.star.StarCatalogDB
import com.common.image.telescope.Telescope
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
import com.common.util.time.Time
//=============================================================================
import java.awt.{Color, Font}
import java.io.{BufferedWriter, File, FileWriter}
import java.util.concurrent.{ConcurrentHashMap, ConcurrentLinkedQueue}
import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}
import scala.jdk.CollectionConverters.ConcurrentMapHasAsScala
//=============================================================================
//=============================================================================
object PhotometryAbsolute extends MyLogger {
  //---------------------------------------------------------------------------
  val sep ="\t"
  //---------------------------------------------------------------------------
  final val ROOT_PATH = "/absolute/"
  //---------------------------------------------------------------------------
  final val PHOTOMETRY_ABSOLUTE_IMAGE_CONTENT_DIR              = "image_content"
  final val PHOTOMETRY_ABSOLUTE_IMAGE_METADATA_CSV             = "image_metadata.csv"
  final val PHOTOMETRY_ABSOLUTE_PHOTOMETRIC_MODELS_SUMMARY_CSV = "photometric_models_summary.csv"
  final val PHOTOMETRY_ABSOLUTE_DETECTION_SUMMARY_CSV          = "detection_summary.csv"
  final val PHOTOMETRY_ABSOLUTE_IMAGE_DUPLICATED_CSV           = "image_duplicated.csv"
  //---------------------------------------------------------------------------
  private final val SOURCES_DIRECTORY = "gaia_known_sources"
  private final val UNKNOWN_SOURCES   = "gaia_unknown_sources"
  private final val MULTIPLE_SOURCES  = "gaia_multiple_sources"
  //---------------------------------------------------------------------------
  private final val NONE_FOCUS_OBJECT = "none"
  //---------------------------------------------------------------------------
  //crappies
  private final val CROPPY_DEFAULT_PIX_SIZE_X = MyConf.c.getInt("Astrometry.catalog.croppy.defaultPixSizeX")
  private final val CROPPY_DEFAULT_PIX_SIZE_Y = MyConf.c.getInt("Astrometry.catalog.croppy.defaultPixSizeY")
  private final val CROPPY_SOURCE_DETECTION_MIN_PIX_COUNT = MyConf.c.getInt("Astrometry.catalog.croppy.minPixCount")
  private final val CROPPY_SOURCE_DETECTION_MAX_PIX_COUNT = MyConf.c.getInt("Astrometry.catalog.croppy.maxPixCount")
  private final val CROPPY_SOURCE_DETECTION_SIZE_RESTRICTION = Some((CROPPY_SOURCE_DETECTION_MIN_PIX_COUNT,CROPPY_SOURCE_DETECTION_MAX_PIX_COUNT))
  //---------------------------------------------------------------------------
  //image duplicated
  private val imageObservingTimeMap = new ConcurrentHashMap[String, String]() //used image duplicated checking
  private val duplicatedImageMap =  new ConcurrentHashMap[String, String]() //for image duplicated checking
  //---------------------------------------------------------------------------
  //typically used when fitting astrometry
  def apply(img: MyImage
            , imageFocus: ImageFocusTrait
            , subGaia: GaiaDB
            , verbose: Boolean): PhotometryAbsolute = {
    PhotometryAbsolute(
      img
      , imageFocus
      , subGaia
      , outputRootDir = ""
      , applyOnAllSources = false
      , imageFocusInfoStorage = None
      , imageFocusInfo = None
      , fixedApertureSeq = None
      , isStar = false
      , blindSearch = false
      , additionalResults = false
      , verbose = verbose)
  }
  //---------------------------------------------------------------------------
  private def addFileExtension(s: String, extension: String = ".csv") =
    if (MyFile.getFileExtension(s) != extension) s + extension else s
  //---------------------------------------------------------------------------
  private def saveSimpleCatalog(csvName: String, catalog: Array[Segment2D]): Unit = {
    val bw = new BufferedWriter(new FileWriter(new File(addFileExtension(csvName))))
    val headerSource = s"sequence${sep}m2_id${sep}x_pix${sep}y_pix${sep}ra${sep}dec${sep}" +
      s"ra_hms${sep}dec_dms${sep}pixCount${sep}flux" + "\n"

    bw.write(headerSource)
    if (catalog.isEmpty) bw.write("#None unknown source detected\n")
    else {
      catalog.zipWithIndex.foreach { case (source, i) =>
        bw.write(f"$i%010d" + sep + source.id + sep +
          source.getPosAsCsvLine(source.getCentroid, sep) + source.getElementCount + sep + source.getFlux() + "\n")
      }
    }
    bw.close
  }
  //---------------------------------------------------------------------------
  private def isImageDuplicated(img: MyImage): Boolean = {
    val timeStamp = img.getObservingTime().toString
    if (imageObservingTimeMap.containsKey(timeStamp)) {
      duplicatedImageMap.put(timeStamp,img.getRawName())
      true
    }
    else {
      imageObservingTimeMap.put(timeStamp,img.getRawName())
      false
    }
  }
  //---------------------------------------------------------------------------
  //In blind mode return none,
  // In focused mode return the theoretical position in the image using JPL spice orbit of the requested mpo
  private def getImageFocusInfo(img: MyImage
                                , imageFocus: ImageFocusTrait
                                , searchRadiusRa: Double
                                , searchRadiusDec: Double
                                , imageFocusInfoStorage: Option[(String, ConcurrentLinkedQueue[ImageFocusInfo])]
                                , spkVersion: Option[String] = None
                                , verbose: Boolean = false)
  : Option[ImageFocusInfo] = {


    val fits = img.getSimpleFits()
    val c = Telescope.getConfigurationFile(fits)

    //get some parameters from configuration
    val sigmaMultiplier = c.getFloat("SourceDetection.image.sigmaMultiplier")

    val mpcID = imageFocusInfoStorage.get._1
    if(verbose) info(s"Image: '${img.getRawName()}' Calculating photometry for object '$mpcID'")
    val expectedPos =
      if (imageFocus.isStar) {
        val expectedPosition = imageFocus.asInstanceOf[ImageFocusStar].getEstPosAtTime(img.getObservingDateMidPoint().toString)
        Some((expectedPosition, img.skyToPix(expectedPosition)))
      }
      else imageFocus.asInstanceOf[ImageFocusMPO].getEstPosition(img, spkVersion, verbose)

    if (expectedPos.isEmpty) return None
    val afi = ImageFocusInfo(
      img.getRawName()
      , imageFocus
      , expectedPosRaDec = expectedPos.get._1
      , expectedPosPix = expectedPos.get._2
      , fits.getStringValueOrEmptyNoQuotation(KEY_OM_TELESCOPE)
      , fits.getStringValueOrEmptyNoQuotation(KEY_OM_FILTER)
      , img.getObservingDateMidPoint().toString
      , fits.getStringValueOrEmptyNoQuotation(KEY_OM_EXPTIME).toDouble
      , img.getBackground(verbose)
      , img.getBackgroundRMS(verbose)
      , sigmaMultiplier
      , searchRadiusRa
      , searchRadiusDec
    )
    imageFocusInfoStorage.get._2.add(afi)
    Some(afi)
  }
  //---------------------------------------------------------------------------
  def processImage(img: MyImage
                   , imageFocus: ImageFocusTrait
                   , applyOnAllSources: Boolean
                   , imageFocusInfoStorage: Option[(String,ConcurrentLinkedQueue[ImageFocusInfo])] = None
                   , fixedApertureSeq: Option[Array[Double]] = None
                   , isStar: Boolean
                   , blindSearch: Boolean
                   , outputDir : String
                   , additionalResults: Boolean = false
                   , spkVersion: Option[String] = None
                   , debugStorage: Boolean = false
                   , astrometry: Boolean = false
                   , verbose: Boolean = false): Unit = {

    if (isImageDuplicated(img)) {
      warning(s"Image: '${img.getRawName()}' is duplicated, avoiding the calculation of its catalog")
      return
    }

    val fits = img.getSimpleFits()

    //get some parameters from configuration of the telescope
    val c = Telescope.getConfigurationFile(fits)

    val pixScaleX = fits.getStringValueOrEmptyNoQuotationWithDefault(KEY_OM_PIX_SCALE_X,"1").toDouble
    val pixScaleY = fits.getStringValueOrEmptyNoQuotationWithDefault(KEY_OM_PIX_SCALE_Y,"1").toDouble
    val searchRadiusRa  = Conversion.pixToDecimalDegree(c.getDouble("SourceDetection.image.searchPixRadiusRa"), pixScaleX)
    val searchRadiusDec = Conversion.pixToDecimalDegree(c.getDouble("SourceDetection.image.searchPixRadiusDec"), pixScaleY)

    //get expected object if it is possible
    val focusInfo =
      if (blindSearch) None
      else getImageFocusInfo(
          img
        , imageFocus
        , searchRadiusRa
        , searchRadiusDec
        , imageFocusInfoStorage
        , spkVersion
        , additionalResults)

    if (!blindSearch && focusInfo.isEmpty) {
      error (s"Image: '${img.getRawName()}' has no no focus information")
      return
    }

    //get sub-gaia
    val subGaia =  GaiaDB(GaiaDB.SUB_GAIA_DATABASE_NAME, "None")

    //process catalog
    val catalog = PhotometryAbsolute(
      img
      , imageFocus
      , subGaia
      , outputDir
      , applyOnAllSources
      , imageFocusInfoStorage
      , focusInfo
      , fixedApertureSeq
      , isStar
      , blindSearch
      , additionalResults
      , debugStorage
      , astrometry
      , verbose
    )
    subGaia.close()
    catalog.process()
  }
  //---------------------------------------------------------------------------
  def processDir(imageFocus:ImageFocusTrait
                 , inputDir: String
                 , outputRootPath: String
                 , applyOnAllSources: Boolean
                 , drop: Boolean
                 , imageFocusInfoStorage: Option[(String, ConcurrentLinkedQueue[ImageFocusInfo])] = None
                 , fixedApertureSeq: Option[Array[Double]] = None
                 , isStar: Boolean
                 , blindSearch: Boolean
                 , additionalResults: Boolean = false
                 , spkVersion: Option[String] = None
                 , debugStorage: Boolean = false
                 , astrometry: Boolean = false
                 , verbose: Boolean = false) = {
    //-------------------------------------------------------------------------
    val fitsExtension = MyConf.c.getStringSeq("Common.fitsFileExtension")
    //-------------------------------------------------------------------------
    class MyParallelImageSeq(imageCatalogDB: ImageCatalog, seq: Array[String]) extends ParallelTask[String](
      seq
      , CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 500) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(fileName: String) =
        Try {
          val rawFileName = Path.getOnlyFilenameNoExtension(fileName)
          if (imageCatalogDB.exist(rawFileName))
            warning(s"Avoiding the re-calculation the photometry of the image '$rawFileName', reusing the existing one")
          else
            processImage(
              MyImage(fileName)
              , imageFocus
              , applyOnAllSources
              , imageFocusInfoStorage
              , fixedApertureSeq
              , isStar
              , blindSearch
              , s"$outputRootPath/"
              , additionalResults
              , spkVersion
              , debugStorage
              , astrometry
              , verbose)
        }
        match {
          case Success(_) =>
          case Failure(e) =>
            error(e.getMessage + s" Error catalogging the image '$fileName' continuing with the next image")
            error(e.getStackTrace.mkString("\n"))
        }
      //-----------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    if (!Path.directoryExist(inputDir)) error(s"Input directory '$inputDir' does not exist")
    else {

      val imageFocusID = if (debugStorage) imageFocus.getDebuggingID
                         else imageFocus.getID
      val objectCatalogDB = ImageCatalog(imageFocusID)
      if (drop) {
        objectCatalogDB.dropCollection()
        val focusDB =
          if(imageFocus.isStar) StarCatalogDB(imageFocusID)
          else MPO_CatalogDB(imageFocusID)
        focusDB.dropCollection()
        focusDB.close()
      }

      //calculate al files in the input directory
      val fileSeq = Path.getSortedFileList(inputDir, fitsExtension).map(_.getAbsolutePath)
      if (fileSeq.length > 0) new MyParallelImageSeq(objectCatalogDB, fileSeq.toArray)

      //calculate all subdirs
      Path.getSubDirectoryList(inputDir).map(_.getAbsolutePath).foreach { subDirName =>
        info(s"Parsing directory: '$subDirName'")
        val fileSeq = Path.getSortedFileListRecursive(subDirName, fitsExtension).map(_.getAbsolutePath)
        if (fileSeq.length > 0) new MyParallelImageSeq(objectCatalogDB, fileSeq.toArray)
      }

      objectCatalogDB.close()
    }

    //generate image duplicated report
    if (!duplicatedImageMap.isEmpty) {
      val csvName = s"$outputRootPath/../$PHOTOMETRY_ABSOLUTE_IMAGE_DUPLICATED_CSV"
      info(s"Found: ${duplicatedImageMap.size} duplicated images")
      val bw = new BufferedWriter(new FileWriter(new File(csvName)))
      duplicatedImageMap.asScala.map { case (_,imageName) =>
        bw.write(imageName + "\n")
      }
      bw.close()
      warning(s"Found: ${duplicatedImageMap.size} duplicated images calculating the photometry. Please check: '$csvName'")
    }
  }
  //---------------------------------------------------------------------------
  def report(imageFocus: ImageFocusTrait
             , onlyPhotometricModels: Boolean
             , objectDetectionRootPath: String
             , photometryRootPath: String
             , debugStorage: Boolean = false): Unit = {

    info("Checking the database")
    val objectID = if (debugStorage) imageFocus.getDebuggingID else imageFocus.getID
    val imageCatalogDB = ImageCatalog(objectID)

    if (!onlyPhotometricModels) {
      //generate image metadata
      imageCatalogDB.generateCsvImageMetadata(s"$objectDetectionRootPath/$PHOTOMETRY_ABSOLUTE_IMAGE_METADATA_CSV")

      //sources per image
      imageCatalogDB.generateCsvSourceSeq(Path.resetDirectory(s"$objectDetectionRootPath/$PHOTOMETRY_ABSOLUTE_IMAGE_CONTENT_DIR/"))
    }

    //save photometric models
    val imageNameSeq = imageCatalogDB.getNameSeq()
    if (imageNameSeq.isEmpty) {
      warning(s"No images to process for: '${imageFocus.getID}'")
      return
    }

    val bwSummary = new BufferedWriter(new FileWriter(new File(s"$photometryRootPath/$PHOTOMETRY_ABSOLUTE_PHOTOMETRIC_MODELS_SUMMARY_CSV")))
    bwSummary.write(headerCsv.mkString(sep) + "\n")

    imageNameSeq.foreach{ imageName =>
      PhotometricModel.save(imageName
                           , imageCatalogDB.getCatalogPhotometricModelMap(imageName)
                           , bwSummary)
    }

    bwSummary.close()

    //close image catalog
    imageCatalogDB.close()
  }
  //---------------------------------------------------------------------------
  private def getApertureCoef(fixedApertureSeq: Option[Array[Double]]
                             , c: MyConfTrait) = {
    val useFixedAperture = fixedApertureSeq.isDefined

    val minApertureCoef     = if (useFixedAperture) fixedApertureSeq.get(0) else c.getDouble("Photometry.aperture.fwhmAverage.minRadioFactor")
    val annularApertureCoef = if (useFixedAperture) fixedApertureSeq.get(1) else c.getDouble("Photometry.aperture.fwhmAverage.annularRadioFactor")
    val maxApertureCoef     = if (useFixedAperture) fixedApertureSeq.get(2) else c.getDouble("Photometry.aperture.fwhmAverage.maxRadioFactor")

    (minApertureCoef,annularApertureCoef,maxApertureCoef)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.common.image.photometry.absolute.PhotometryAbsolute._
case class PhotometryAbsolute(img: MyImage
                              , imageFocus: ImageFocusTrait
                              , subGaia: GaiaDB
                              , outputRootDir: String
                              , applyOnAllSources: Boolean
                              , imageFocusInfoStorage: Option[(String,ConcurrentLinkedQueue[ImageFocusInfo])]
                              , imageFocusInfo: Option[ImageFocusInfo]
                              , fixedApertureSeq: Option[Array[Double]]
                              , isStar: Boolean
                              , blindSearch: Boolean
                              , additionalResults: Boolean
                              , debugStorage: Boolean = false
                              , astrometry: Boolean = false
                              , verbose: Boolean = false)  extends  MyLogger {
  //---------------------------------------------------------------------------
  private val outputDir = outputRootDir + img.getRawName()
  //---------------------------------------------------------------------------
  private val fits = img.getSimpleFits()
  //---------------------------------------------------------------------------
  //configuration parameters
  private val c = Telescope.getConfigurationFile(fits)

  private val sigmaMultiplier = c.getFloat("SourceDetection.image.sigmaMultiplier")

  private val sourceMinPixCount = c.getInt("SourceDetection.source.minPixCount")
  private val sourceMaxPixCount = c.getInt("SourceDetection.source.maxPixCount")

  private val sourceMaxPixValueAllowed = c.getInt("SourceDetection.source.maxPixValueAllowed")

  private val pixScaleX = fits.getStringValueOrEmptyNoQuotationWithDefault(KEY_OM_PIX_SCALE_X,"1").toDouble
  private val pixScaleY = fits.getStringValueOrEmptyNoQuotationWithDefault(KEY_OM_PIX_SCALE_Y,"1").toDouble
  private val searchRadiusRa =  Conversion.pixToDecimalDegree(c.getDouble("SourceDetection.image.searchPixRadiusRa"), pixScaleX)
  private val searchRadiusDec = Conversion.pixToDecimalDegree(c.getDouble("SourceDetection.image.searchPixRadiusDec"), pixScaleY)

  private val sourceMinSNR_Allowed = c.getFloat("SourceDetection.source.minSNR_Allowed")
  private val sourceMinIsolatedDistance = c.getInt("SourceDetection.source.minIsolatedDistance")

  private val gaiaMaxAllowedRaError              = c.getFloat("Photometry.gaia.maxAllowedRaError")
  private val gaiaMaxAllowedDecError             = c.getFloat("Photometry.gaia.maxAllowedDecError")
  private val gaiaMaxAllowedBP_RP_ExcessFactor   = c.getFloat("Photometry.gaia.maxAllowedBP_RP_ExcessFactor")

  private val keepSourcesWithLowResolutionCentroid = c.getInt("Centroid.keepSourcesWithLowResolutionCentroid") == 1

  private val gaiaMaxAllowedRaProperMotionError    = c.getFloat("Photometry.gaia.maxAllowedRaProperMotionError")
  private val gaiaMaxAllowedDecProperMotionFactor  = c.getFloat("Photometry.gaia.maxAllowedDecProperMotionError")
  private val gaiaMaxAllowedRUWE_Error             = c.getFloat("Photometry.gaia.maxAllowedRUWE_Error")
  private val gaiaMinMagnitude                     = c.getFloat("Photometry.gaia.minMagnitude")

  private val useFixedAperture                     = fixedApertureSeq.isDefined

  private val minApertureCoef                      = if (useFixedAperture) fixedApertureSeq.get(0) else c.getDouble("Photometry.aperture.fwhmAverage.minRadioFactor")
  private val annularApertureCoef                  = if (useFixedAperture) fixedApertureSeq.get(1) else c.getDouble("Photometry.aperture.fwhmAverage.annularRadioFactor")
  private val maxApertureCoef                      = if (useFixedAperture) fixedApertureSeq.get(2) else c.getDouble("Photometry.aperture.fwhmAverage.maxRadioFactor")
  //---------------------------------------------------------------------------
  //cropies
  private val croppyXpixSize = c.getInt("Photometry.croppy.xPixSize")
  private val croppyYpixSize = Math.round((img.yMax / img.xMax.toDouble) * croppyXpixSize).toInt
  //---------------------------------------------------------------------------
  //directories
  //---------------------------------------------------------------------------
  private val OUTPUT_FITS_CROPPY           = outputDir + "/croppy/"
  private val OUTPUT_FITS_CROPPY_FOUND_DIR = s"$OUTPUT_FITS_CROPPY/mpo_found/fits/"
  private val OUTPUT_PNG_CROPPY_FOUND_DIR  = s"$OUTPUT_FITS_CROPPY/mpo_found/png/"

  private val OUTPUT_FITS_CROPPY_NOT_FOUND_DIR = s"$OUTPUT_FITS_CROPPY/mpo_NOT_found/fits/"
  private val OUTPUT_PNG_CROPPY_NOT_FOUND_DIR  = s"$OUTPUT_FITS_CROPPY/mpo_NOT_found/png/"
  //---------------------------------------------------------------------------
  private val catalogPath                = if (additionalResults) Path.resetDirectory(outputDir + "/catalog/all_sources") else ""
  //---------------------------------------------------------------------------
  private val oDirFinalPng               = if (additionalResults) Path.resetDirectory(catalogPath + "final/png") else ""
  private val oDirFinalCsv               = if (additionalResults) Path.resetDirectory(catalogPath + "final/csv") else ""

  private val oDirGaiaSourcesNotFoundPng = if (additionalResults) Path.resetDirectory(catalogPath + "gaia_sources_not_found/png") else ""
  private val oDirGaiaSourcesNotFoundCsv = if (additionalResults) Path.resetDirectory(catalogPath + "gaia_sources_not_found/csv") else ""
  //---------------------------------------------------------------------------
  private val oDirPlot = outputDir +  "/gaia_match/"
  //---------------------------------------------------------------------------
  //telescope and observing date
  private val observingDate = fits.getStringValueOrEmptyNoQuotationWithDefault(SimpleFits.KEY_OM_DATEOBS,"")
  private val observingDateMidPoint = img.getObservingDateMidPoint()
  //---------------------------------------------------------------------------
  //stats
  private val gaiaSourceSeq         = ArrayBuffer[MatchedImageSource]()
  private val zeroMagnitude         = ArrayBuffer[MatchedImageSource]()
  private val bigBP_RP_ExcessFactor = ArrayBuffer[MatchedImageSource]()
  private val bigRaDecError         = ArrayBuffer[MatchedImageSource]()

  private val gaiaNoMatch  = ArrayBuffer[Segment2D]()
  private val gaiaMultiple = ArrayBuffer[Segment2D]()

  private val wrongSizeSource  = ArrayBuffer[Segment2D]()
  private val saturatedSource  = ArrayBuffer[Segment2D]()

  private val notEnoughSNR         = ArrayBuffer[MatchedImageSource]()
  private val notIsolated          = ArrayBuffer[MatchedImageSource]()

  private val bigProperMotionError = ArrayBuffer[MatchedImageSource]()
  private val invalidRUWE          = ArrayBuffer[MatchedImageSource]()
  private val invalidMagnitude     = ArrayBuffer[MatchedImageSource]()

  private val gaiaSourceSeqAtImageMap = scala.collection.mutable.Map[Long,GaiaSourceReduced]()
  private val gaiaSourceSeqNotFound  = ArrayBuffer[GaiaSourceReduced]()
  //---------------------------------------------------------------------------
  //quality
  private var qualityRa  = -1d
  private var qualityDec = -1d
  //---------------------------------------------------------------------------
  //apertures
  private var finalMinAperture     = -1d
  private var finalAnnularAperture = -1d
  private var finalMaxAperture     = -1d
  //---------------------------------------------------------------------------
  //catalog
  private var gaiaSourceCatalog: Array[MatchedImageSource] = null
  private var unknownGaiaSourceCatalog: Array[Segment2D] = null
  private var multipleMatchGaiaSourceCatalog: Array[Segment2D] = null
  private var focusCatalog: Array[ImageFocusTrait] = null
  //---------------------------------------------------------------------------
  //noise tide
  private var noiseTide: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE
  //---------------------------------------------------------------------------
  //reset croppy directories
  if (additionalResults) initCroppyDirSeq()
  //---------------------------------------------------------------------------
  //(sources, noise_tide)
  def getSourceSeq(additionalNoiseTideMultiplier: Double = 1d): (Array[Segment2D], PIXEL_DATA_TYPE) = {
    val fits = img.getSimpleFits()

    val c = Telescope.getConfigurationFile(fits)
    val sigmaMultiplier   = c.getFloat("SourceDetection.image.sigmaMultiplier")
    val sourceMinPixCount = c.getInt("SourceDetection.source.minPixCount")
    val sourceMaxPixCount = if (isStar) {
                               warning("Assuming no max pixel restriction when calculating photometry in stars")
                               Int.MaxValue
                             }
                             else c.getInt("SourceDetection.source.maxPixCount")
    val roundSources = c.getInt("SourceDetection.source.roundSource")

    val background = img.getBackground(verbose)
    val backgroundRMS = img.getBackgroundRMS(verbose)
    noiseTide = background + (backgroundRMS * sigmaMultiplier * additionalNoiseTideMultiplier)

    if(roundSources != 0) if(verbose) info(s"Image: '${img.getRawName()}' rounding sources with neighbours: $roundSources pixel allowed:(min,max): ($sourceMinPixCount,$sourceMaxPixCount)")
    val imageMask = img.getMaskSeq
    if(verbose) info(s"Image: '${img.getRawName}' sigma multiplier: $sigmaMultiplier background:${f"$background%.3f"} background RMS:${f"$backgroundRMS%.3f"} noise tide:${f"$noiseTide%.3f"}")
    if(verbose) info(s"Image: '${img.getRawName}' mask: '${if (imageMask ==null || imageMask.isEmpty) "Mask none" else  imageMask.mkString("[",",","]")}'")
    val region = img.findSourceSeq(noiseTide, imageMask, Some((sourceMinPixCount, sourceMaxPixCount)))
    var regionSourceSeq = region.toSegment2D_seq()

    if(verbose) info(s"Image: '${img.getRawName()}' found: ${regionSourceSeq.size} sources before rounding")

    if (additionalResults) {
      saveSimpleCatalog(catalogPath + "all_sources", regionSourceSeq)
      region.synthesizeImageWithColor(
        catalogPath + "all_sources"
        , regionSourceSeq
        , imageDimension = Some(img.getDimension)
        , font = Some(new Font(Font.MONOSPACED, Font.PLAIN, 10))
        , fontColor = Color.ORANGE)

      region.synthesizeImageWithColor(
        catalogPath + "all_sources_no_number"
        , regionSourceSeq
        , imageDimension = Some(img.getDimension)
        , fontColor = Color.ORANGE
      )
    }

    //round sources
    var sourceSeq =
      if (roundSources == 0) regionSourceSeq
      else {
        val newSourceSeq = regionSourceSeq flatMap (_.round(noiseTide, roundSources))
        region.clear()
        regionSourceSeq = null
        newSourceSeq filter { source =>
          (source.getPixCount() >= sourceMinPixCount
            && !source.getData().forall(_ == PixelDataType.PIXEL_ZERO_VALUE))
        }
      }

    if(verbose) info(s"Image: '${img.getRawName()}' found: ${sourceSeq.size} sources after rounding: $roundSources with pixel count between:[$sourceMinPixCount,$sourceMaxPixCount]")

    if (additionalResults) {
      saveSimpleCatalog(catalogPath + "all_sources_after_rounding", sourceSeq)
      region.synthesizeImageWithColor(
        catalogPath + "all_sources_after_rounding"
        , sourceSeq
        , imageDimension = Some(img.getDimension())
        , font = Some(new Font(Font.MONOSPACED, Font.PLAIN, 10))
        , fontColor = Color.ORANGE)
    }

    //calculate img fwhm
    Fwhm.calculate(img, sourceSeq, verbose)

    //calculate centroid and (ra,dec)
    Centroid.calculateCentroiWithFallBack(sourceSeq
                                          , img
                                          , MyConf.c.getStringSeq("SourceCentroid.fallbackAlgorithmSeq")
                                          , verbose)

    //filter by low resolution centroid
    if (!keepSourcesWithLowResolutionCentroid) {
      if(verbose) info(s"Image: '${img.getRawName()}' sources before filter by low resolution centroid: ${sourceSeq.size}")
      sourceSeq = sourceSeq filter (!_.hasLowResolutionCentroid)
      if(verbose) info(s"Image: '${img.getRawName()}' sources after filter by low resolution centroid: ${sourceSeq.size}")
    }

    //calculate the (ra,dec) using the centroids
    img.setCentroidRaDec(sourceSeq)

    (sourceSeq, noiseTide)
  }
  //--------------------------------------------------------------------------
  private def filterByClosestSourceOnCatalog(catalog: Array[Segment2D]
                                             , raDecSeq: Array[Point2D_Double]
                                             , searchRadiusRa: Double
                                             , searchRadiusDec: Double
                                             , neighbourCount: Int = 2): Array[Segment2D] = {
    val kd_tree = K2d_TreeDouble()
    val map = scala.collection.mutable.Map[Int, Segment2D]()
    val seq = catalog map { s =>
      map(s.id) = s
      (s.getCentroidRaDec().toSequence(), s.id)
    }
    kd_tree.addSeq(seq)

    val result = ArrayBuffer[Segment2D]()

    raDecSeq foreach  { raDec =>
      val r = kd_tree.getKNN(raDec.toSequence, requestedK = neighbourCount)
      if (r.isEmpty) return result.toArray
      else {
        r.foreach { item =>
          val s = map(item.v)

          val dRa = Math.abs(s.getCentroidRaDec().x - raDec.x)
          val dDec = Math.abs(s.getCentroidRaDec().y - raDec.y)
          if ((dRa < searchRadiusRa) && (dDec < searchRadiusDec))
            result += s
        }
      }
    }
    result.toArray
  }

  //---------------------------------------------------------------------------
  private def getImageQuality(matchedImageSourceSeq: Array[MatchedImageSource]
                              , prefix: String = "") = {

    val (raPM_DiffStdevMas, decPM_DiffStdevMas) = MatchedImageSource.getResidualRaDecStatsMas(
      matchedImageSourceSeq
      , img.getObservingDateMidPoint().toString)
    if(verbose) info(s"Image: '${img.getRawName()}'" +
      prefix + s"wcs fitting quality: (ra,dec) residual stdDev : (${f"$raPM_DiffStdevMas%.3f"}, ${f"$decPM_DiffStdevMas%.3f"}) (mas)")

    (raPM_DiffStdevMas, decPM_DiffStdevMas)
  }
  //---------------------------------------------------------------------------
  private def getFocusSeq(): Array[ImageFocusTrait] = {

    if (blindSearch) {
      warning("Using a local copy of 'Astrochecker' to find all known mpo(s) objects")
      return AsteroidChecker.query(img, unknownGaiaSourceCatalog).values.toArray
    }

    val ifi = imageFocusInfo.get
    if (ifi.isStar && ifi.expectedPosRaDec == Point2D_Double.POINT_INVALID) {
      ifi.updateStatus(ImageFocusInfo.STATUS_ERROR_GETING_EPEHEMERIS)
      return Array[ImageFocusTrait]()
    }
    val expectedRaDec    = ifi.expectedPosRaDec
    val searchRadiusRa  = ifi.searchRadiusRa
    val searchRadiusDec = ifi.searchRadiusDec

    if(verbose) info(s"Image: '${img.getRawName}' expected focus at (ra,dec): ${ifi.expectedPosRaDec.getFormattedShortString} pix: ${ifi.expectedPosPix.getFormattedShortString}")
    val sourceCatalog = gaiaSourceCatalog.map (_.imageSource) ++ unknownGaiaSourceCatalog ++ multipleMatchGaiaSourceCatalog

    val sourceSeq = filterByClosestSourceOnCatalog(
      sourceCatalog
      , Array(expectedRaDec)
      , searchRadiusRa
      , searchRadiusDec)

    if (sourceSeq.isEmpty) {
      ifi.updateStatus(ImageFocusInfo.STATUS_NONE_SOURCE_FOUND)
      return Array[ImageFocusTrait]()
    }

    if (sourceSeq.size > 1) { //getting the closest one
      ifi.updateStatus(ImageFocusInfo.STATUS_MULTIPLE_SOURCE_FOUND)
    }

    val focusSource = sourceSeq.head
    ifi.updateSource(focusSource)
    ifi.updateStatus(ImageFocusInfo.STATUS_OBJET_FOUND)

    val sourceCentre = focusSource.getCentroidRaDec()
    val residualMas = expectedRaDec.getResidualMas(focusSource.getCentroidRaDec)

    info(s"--->Image '${img.getRawName}'" +
      s" object: '${ifi.focus.getID}'" +
      s" found at (ra,dec):${sourceCentre.getFormattedShortString}" +
      s" pix:${focusSource.getCentroid().toPoint2D()}" +
      s" m2 source id: ${focusSource.id}" +
      (if (isStar) s" distance to the expected: (${f"${residualMas.x}%.4f"},${f"${residualMas.y}%.4f"}) mas"
       else s" distance to the expected orbit position: (${f"${residualMas.x}%.4f"},${f"${residualMas.y}%.4f"}) mas"))


    val newImageFocus =
      if (isStar) ImageFocusStar(imageFocus.asInstanceOf[ImageFocusStar], focusSource)
      else {
        val newImageFocus = ImageFocusMPO(imageFocus.asInstanceOf[ImageFocusMPO], focusSource)
        newImageFocus.setExpectedRaDec(Point2D_Double(Math.round(sourceCentre.x)
                                                    , Math.round(sourceCentre.y)))
        newImageFocus
      }
    Array(newImageFocus)
  }
  //---------------------------------------------------------------------------
  def generatePlotSeq(knownCsv: String
                      , unknownCsv: String
                      , multipleCsv: String
                      , mpoCsv: String) = {
    val knownPng = knownCsv.replace(".csv", ".png")
    val unknownPng = unknownCsv.replace(".csv", ".png")
    val multiplePng = multipleCsv.replace(".csv", ".png")
    val mpoPng = mpoCsv.replace(".csv", ".png")

    val dict = scala.collection.mutable.Map[Long, String]()
    val mpoColorMap = scala.collection.mutable.Map[Long, Color]()

    //create the dictionary for known gaia sources
    gaiaSourceCatalog map (s => dict(s.imageSource.id) = s"'g: ${s.catalogSource._id.toString}'")

    //create the dictionary for known imageFocus
    focusCatalog map { s =>
      val id = s.id
      val v = s"'u: ${s.source.id}'"
      if (dict.contains(id)) dict(id) = dict(id) + " & " + v
      else dict(id) = v
      mpoColorMap(id) = Color.RED
    }
    //create the known source png file
    val region  = img.getRegion
    region.synthesizeImageWithColor(
      knownPng
      , focusCatalog.map(_.source) ++ gaiaSourceCatalog.map(_.imageSource)
      , imageDimension = Some(img.getDimension)
      , blackAndWhite = true
      , font = Some(new Font(Font.MONOSPACED, Font.PLAIN, 10))
      , fontOffsetY = 3
      , dict = dict
      , differentColorSource = mpoColorMap
    )

    //create the unknown source png file
    region.synthesizeImageWithColor(
      unknownPng
      , focusCatalog.map(_.source) ++ unknownGaiaSourceCatalog
      , imageDimension = Some(img.getDimension)
      , blackAndWhite = true
      , font = Some(new Font(Font.MONOSPACED, Font.PLAIN, 10))
      , fontOffsetY = 3
      , differentColorSource = mpoColorMap
    )

    //create the multiple GAIA source png file
    if (multipleMatchGaiaSourceCatalog.length > 0)
      region.synthesizeImageWithColor(
        multiplePng
        , multipleMatchGaiaSourceCatalog
        , imageDimension = Some(img.getDimension)
        , blackAndWhite = true
        , font = Some(new Font(Font.MONOSPACED, Font.PLAIN, 10))
        , fontOffsetY = 3)

    //create the mpo catalog png file
    if (focusCatalog.length > 0) {
      region.synthesizeImageWithColor(
        mpoPng
        , focusCatalog.map(_.source)
        , imageDimension = Some(img.getDimension)
        , blackAndWhite = true
        , font = Some(new Font(Font.MONOSPACED, Font.PLAIN, 10))
        , fontOffsetY = 3
        , differentColorSource = mpoColorMap
        , dict = scala.collection.mutable.Map[Long, String]())
    }
  }

  //---------------------------------------------------------------------------
  def getGaiaSourcesAtImage() = {
    val relevantPos = img.getRaDecRelevantPos()
    val minRa = relevantPos(0)
    val maxRa = relevantPos(1)
    val minDec = relevantPos(2)
    val maxDec = relevantPos(3)

    subGaia.getSourceMap.values.foreach { gas=>
      val raDec = gas.getRaDecWithPM(observingDate)
      if (raDec.x >= minRa &&
        raDec.x <= maxRa &&
        raDec.y >= minDec &&
        raDec.y <= maxDec)
        gaiaSourceSeqAtImageMap(gas._id) = gas
    }
  }
  //---------------------------------------------------------------------------
  def getGaiaSourcesNotFoundInImage(inputSeq: Array[MatchedImageSource]) = {
    val inputMap = (inputSeq map (source=> source.catalogSource._id -> source.catalogSource)).toMap
    gaiaSourceSeqAtImageMap.values.foreach { gaiaSource =>
      if (!inputMap.contains(gaiaSource._id)) gaiaSourceSeqNotFound += gaiaSource
    }
  }
  //---------------------------------------------------------------------------
  private def saveFocusCatalog(csvName: String, catalog: Array[ImageFocusTrait]): Unit = {
    val bw = new BufferedWriter(new FileWriter(new File(addFileExtension(csvName))))

    //write header
    val headerSource = s"sequence${sep}m2_id${sep}x_pix${sep}y_pix${sep}ra${sep}dec${sep}" +
      s"ra_hms${sep}dec_dms${sep}pixCount${sep}flux$sep" + ImageFocusTrait.getCsvHeader(sep) + sep + "mpc"
    bw.write(headerSource)
    bw.write("\n")

    //write content
    if (catalog.isEmpty) bw.write("#None unknown source detected\n")
    else {
      catalog.zipWithIndex.foreach { case (focus, i) =>
        val source = focus.source
        val mpcQueryLine = MinorPlanetCenter.createQueryKnownObject(mpcID = focus.asInstanceOf[ImageFocusMPO].spiceSpk.mpcID.toString
                                                                    , observingDate  = observingDateMidPoint.toString
                                                                    , ra = Conversion.DD_to_HMS_WithDivider(source.getCentroidRaDec().x)
                                                                    , dec = Conversion.DD_to_DMS_WithDivider(source.getCentroidRaDec().y, threeDecimalPlaces = false)
                                                                    , observatoryCode = Telescope.getOfficialCode(img.getSimpleFits())
                                                                    )

        bw.write(f"$i%010d" + sep
          + source.id + sep
          + source.getPosAsCsvLine(source.getCentroid, sep)
          + source.getElementCount + sep
          + source.getFlux() + sep
          + focus.getAsCsvLine(sep) + sep
          + mpcQueryLine)
        bw.write("\n")
      }
    }
    bw.close
  }
  //---------------------------------------------------------------------------
  def updateImageQuality() = {
    val r = if (gaiaSourceCatalog == null || gaiaSourceCatalog.isEmpty) (-1d,-1d)
    else getImageQuality(gaiaSourceCatalog, " ")
    qualityRa  = r._1
    qualityDec = r._2
  }
  //---------------------------------------------------------------------------
  private def saveImageCatalog(minAperture: Double
                               , annularAperture: Double
                               , maxAperture: Double
                               , qualityRa: Double
                               , qualityDec: Double
                               , imageSourceCount: Int
                               , gaiaMatchSourceCount: Int) = {

    val row = CsvRow()

    fillImageInfoRow(img,row)

    row + CsvItem(COL_NAME_SOURCE_COUNT, imageSourceCount)
    row + CsvItem(COL_NAME_GAIA_SOURCE_COUNT, gaiaMatchSourceCount)
    row + CsvItem(COL_NAME_RA_QUALITY, qualityRa)
    row + CsvItem(COL_NAME_DEC_QUALITY, qualityDec)

    row + CsvItem(COL_NAME_MIN_APERTURE, minAperture)
    row + CsvItem(COL_NAME_ANNULAR_APERTURE, annularAperture)
    row + CsvItem(COL_NAME_MAX_APERTURE, maxAperture.toString)

    val catalogDB = if (debugStorage) ImageCatalog(imageFocus.getDebuggingID)
                    else ImageCatalog(imageFocus.getID)

    catalogDB.add(row)
    catalogDB.close()
  }
  //---------------------------------------------------------------------------
  private def fillImageInfoRow(img: MyImage, row: CsvRow) = {
    val observingDate = img.getObservingDateMidPoint()
    val pos = img.getRaDecRelevantPos()

    row + CsvItem(COL_NAME_IMAGE_NAME, img.getRawName())
    row + CsvItem(COL_NAME_OBSERVING_TIME, fits.getStringValueOrEmptyNoQuotationWithDefault(KEY_OM_DATEOBS,"none"))
    row + CsvItem(COL_NAME_FILTER, fits.getStringValueOrEmptyNoQuotationWithDefault(KEY_OM_FILTER,"none"))
    row + CsvItem(COL_NAME_TELESCOPE, fits.getStringValueOrEmptyNoQuotationWithDefault(KEY_OM_TELESCOPE,"none"))
    row + CsvItem(COL_NAME_INSTRUMENT, fits.getStringValueOrEmptyNoQuotationWithDefault(KEY_OM_INSTRUMENT,"none"))
    row + CsvItem(COL_NAME_EXPOSURE_TIME, fits.getStringValueOrEmptyNoQuotationWithDefault(KEY_OM_EXPTIME,"0").toDouble)
    row + CsvItem(COL_NAME_JULIAN_DATE, Time.toJulian(img.getObservingDateMidPoint()))
    row + CsvItem(COL_NAME_OBSERVING_NIGHT, Time.getObservingNightDate(observingDate).toString)
    row + CsvItem(COL_NAME_BACKGROUND, img.getBackground(verbose))
    row + CsvItem(COL_NAME_NOISE_TIDE, img.getNoiseTide())
    row + CsvItem(COL_NAME_FWHM, img.getFwhmAverage())
    row + CsvItem(COL_NAME_X_PIX_SIZE, img.xMax)
    row + CsvItem(COL_NAME_Y_PIX_SIZE, img.yMax)
    row + CsvItem(COL_NAME_RA_MIN, pos(0))
    row + CsvItem(COL_NAME_RA_MAX, pos(1))
    row + CsvItem(COL_NAME_DEC_MIN, pos(2))
    row + CsvItem(COL_NAME_DEC_MAX, pos(3))
    row + CsvItem(COL_NAME_RA_FOV, img.getFovX)
    row + CsvItem(COL_NAME_DEC_FOV, img.getFovY)
    row + CsvItem(COL_NAME_X_PIX_SCALE, img.getPixScaleX)
    row + CsvItem(COL_NAME_Y_PIX_SCALE, img.getPixScaleY)

    row + CsvItem(COL_NAME_M2_WCS_FITTING_FLAGS, WCS.getImageWcsFitInfo(img))
    row + CsvItem(COL_NAME_OM_FLAGS, fits.getStringValueOrEmptyNoQuotationWithDefault(KEY_OM_FLAGS,"none"))
  }
  //---------------------------------------------------------------------------
  private def saveSourceCatalog(matchedImageSourceSeq: Array[MatchedImageSource]
                                , focusMap: Map[Int, ImageFocusTrait]) = {

    if(verbose) info(s"Image: '${img.getRawName()}' Saving ${matchedImageSourceSeq.size} sources and ${focusCatalog.seq.size} mpo(s) into source catalog")
    val rowSeq = matchedImageSourceSeq.map { matchedImageSource =>
      val row = CsvRow()
      val source = matchedImageSource.imageSource
      val photometricSource = matchedImageSource.catalogSource.asInstanceOf[GaiaPhotometricSource]

      val centroid = source.getCentroid()
      val centroidRaDec = source.getCentroidRaDec()
      val raHms = Conversion.DD_to_HMS_WithDivider(centroidRaDec.x, divider = ":")
      val decDms = Conversion.DD_to_DMS_WithDivider(centroidRaDec.y, divider = ":")
      val morphology = source.getMorphology()
      val focusID = if (focusMap.contains(source.id)) focusMap(source.id).getID else NONE_FOCUS_OBJECT

      row + CsvItem(COL_NAME_FOCUS_ID, focusID)
      row + CsvItem(COL_NAME_GAIA_ID, matchedImageSource.catalogSource._id)
      row + CsvItem(COL_NAME_M2_ID, source.id)
      row + CsvItem(COL_NAME_X_PIX, centroid.x)
      row + CsvItem(COL_NAME_Y_PIX, centroid.y)
      row + CsvItem(COL_NAME_RA_DEC_DEG, centroidRaDec.x)
      row + CsvItem(COL_NAME_DEC_DEC_DEG, centroidRaDec.y)
      row + CsvItem(COL_NAME_RA_HMS, raHms)
      row + CsvItem(COL_NAME_DEC_DMS, decDms)
      row + CsvItem(COL_NAME_PIX_COUNT, source.getElementCount)
      row + CsvItem(SourceDB_Item.COL_NAME_FWHM, source.getFwhm())
      row + CsvItem(COL_NAME_SNR, photometricSource.getFluxSNR())
      row + CsvItem(COL_NAME_PERIMETER, morphology.perimeter)
      row + CsvItem(COL_NAME_ECCENTRICITY, morphology.eccentricity)
      row + CsvItem(COL_NAME_PIX_IN_PERCENTAGE, morphology.pixelInPercentage)
      row + CsvItem(COL_NAME_FLUX_PER_SECOND, photometricSource.getFluxPerSecond())
      row + CsvItem(COL_NAME_ESTIMATED_BACKGROUND, photometricSource.getEstimatedBackground)
      row
    }
    val objectID = if (debugStorage) imageFocus.getDebuggingID else imageFocus.getID
    val catalogDB = ImageCatalog(objectID)
    catalogDB.updateSourceSeq(rowSeq map (SourceDB_Item( _ )), img.getRawName())
    catalogDB.close()
  }
  //---------------------------------------------------------------------------
  private def saveCatalog(sourceSeq: Array[MatchedImageSource]) = {
    //save image catalog
    saveImageCatalog(
      finalMinAperture
      , finalAnnularAperture
      , finalMaxAperture
      , qualityRa
      , qualityDec
      , sourceSeq.size
      , gaiaSourceCatalog.length)

    //save source catalog
    val focusMap = (focusCatalog map  {focus => (focus.source.id,focus)}).toMap
    saveSourceCatalog(sourceSeq, focusMap)
  }
  //---------------------------------------------------------------------------
  private def saveDebugCatalog() = {

    val oDir = Path.resetDirectory(oDirPlot)

    val knownCsv    = oDir + addFileExtension(SOURCES_DIRECTORY)
    val unknownCsv  = oDir + addFileExtension(UNKNOWN_SOURCES)
    val multipleCsv = oDir + addFileExtension(MULTIPLE_SOURCES)
    val mpoCsv      = oDir + addFileExtension(MULTIPLE_SOURCES)

    generatePlotSeq(knownCsv, unknownCsv, multipleCsv, mpoCsv)
    saveSimpleCatalog(knownCsv, gaiaSourceCatalog map (_.imageSource))
    saveSimpleCatalog(unknownCsv, unknownGaiaSourceCatalog)
    saveSimpleCatalog(multipleCsv, multipleMatchGaiaSourceCatalog)
    saveFocusCatalog(mpoCsv, focusCatalog)
  }
  //---------------------------------------------------------------------------
  private def initCroppyDirSeq() =
    if (!Path.directoryExist(OUTPUT_FITS_CROPPY_FOUND_DIR)) {
      Path.resetDirectory(OUTPUT_FITS_CROPPY_FOUND_DIR)
      Path.resetDirectory(OUTPUT_PNG_CROPPY_FOUND_DIR)
      Path.resetDirectory(OUTPUT_FITS_CROPPY_NOT_FOUND_DIR)
      Path.resetDirectory(OUTPUT_PNG_CROPPY_NOT_FOUND_DIR)
    }
  //---------------------------------------------------------------------------
  private def saveObjectCroppy(mpoIsInvalid: Boolean = false): Unit = {

    if(verbose) info(s"Image: '${img.getRawName()}' Saving croppies")
    if ((!focusCatalog.isEmpty && !mpoIsInvalid) || imageFocusInfo.isEmpty)
      saveFoundObjectCroppy(
        focusCatalog
        , OUTPUT_FITS_CROPPY_FOUND_DIR
        , OUTPUT_PNG_CROPPY_FOUND_DIR
        , croppyXpixSize
        , croppyYpixSize)
    else {
      if (!blindSearch && mpoIsInvalid)
        saveExpectedObjectPositionCroppy(
          imageFocusInfoStorage.get._1 //mpc m2_id
          , imageFocusInfo.get.expectedPosPix
          , OUTPUT_FITS_CROPPY_NOT_FOUND_DIR
          , OUTPUT_PNG_CROPPY_NOT_FOUND_DIR
          , croppyXpixSize
          , croppyYpixSize)
    }
  }
  //---------------------------------------------------------------------------
  private def saveFoundObjectCroppy(mpoSeq: Array[ImageFocusTrait]
                                    , outputDirFits: String
                                    , outputDirPng: String
                                    , croppyXpixSize: Int
                                    , croppyYpixSize: Int
                                    , rectangleOffset: Point2D = Point2D(1, 1)) = {
    mpoSeq foreach { ast =>
      val croppyFitsName = outputDirFits + img.getRawName() + ".fits"
      val croppyPngName = outputDirPng + img.getRawName() + ".png"
      img.saveCroppy(
          ast.source.getCentroid()
        , croppyFitsName
        , croppyPngName
        , croppyXpixSize
        , croppyYpixSize
        , ast.source.getRectangle()
        , rectangleOffset
        , noiseTide
        , sigmaMultiplier
        , CROPPY_SOURCE_DETECTION_SIZE_RESTRICTION
      )
    }
  }
  //---------------------------------------------------------------------------
  private def saveExpectedObjectPositionCroppy(objectName: String
                                               , pixPos: Point2D_Double
                                               , outputDirFits: String
                                               , outputDirPng: String
                                               , croppyXpixSize: Int
                                               , croppyYpixSize: Int) = {
    if (!img.isIn(pixPos.toPoint2D()))
      error(s"Image: '${img.getRawName()}' Saving croppy. The central position $pixPos is out pf the image")
    else {
      Path.createDirectoryIfNotExist(outputDirFits)
      Path.createDirectoryIfNotExist(outputDirPng)
      val rectangleSize = Point2D(CROPPY_DEFAULT_PIX_SIZE_X,CROPPY_DEFAULT_PIX_SIZE_Y)  //the source is unknown, so just assume this size
      val pos = pixPos.toPoint2D()
      img.saveCroppy(
        pixPos
        , outputDirFits + img.getRawName() + "_" + objectName + ".fits"
        , outputDirPng + img.getRawName() + "_" + objectName + ".png"
        , croppyXpixSize
        , croppyYpixSize
        , Rectangle(pos - rectangleSize,pos + rectangleSize)
        , Point2D(1, 1)
        , noiseTide
        , sigmaMultiplier
        , CROPPY_SOURCE_DETECTION_SIZE_RESTRICTION
      )
    }
  }
  //---------------------------------------------------------------------------
  private def errorProcessingCatalog(message: String): Unit = {
    warning(s"Image: '${img.getRawName()}' $message. Skipping photometry calculations")
    if (imageFocusInfo.isDefined) imageFocusInfo.get.updateStatus(message)
    saveDebugCatalog()
    saveObjectCroppy(true)
  }
  //---------------------------------------------------------------------------
  private def filterFocusByOverlappingWithSourceInCatalog(): Unit = {
    val focusCatalogRaDecSeq = focusCatalog.map(_.source.getCentroidRaDec())
    val r = filterByClosestSourceOnCatalog(
      gaiaSourceCatalog map (_.imageSource)
      , focusCatalogRaDecSeq
      , searchRadiusRa
      , searchRadiusDec
    )
    if (!blindSearch) {
      if (!r.isEmpty) {  //there at least one source in the catalog close to the focus position?
        focusCatalog = Array() //clear the focus, because it is contaminated
        val ifis = ImageFocusInfo.findFocusInfo(img.getRawName(), imageFocusInfoStorage.get._2)
        val source = r.head
        ifis.get.updateSource(source)
        ifis.get.updateStatus(s"Source: ${source.id};" + ImageFocusInfo.STATUS_CATALOG_SOURCE_MATCHES_FOCUS + source.getCentroidRaDec() + s" catalog source: '${r.head.getCatalogSourceID}'")
      }
    }
  }
  //---------------------------------------------------------------------------
  private def filterSource(s: Segment2D): Boolean = {
    //filter by wrong size
    val pixCount = s.getElementCount
    if ((pixCount <= sourceMinPixCount) ||
      (pixCount >= sourceMaxPixCount)) {
      wrongSizeSource += s
      return false
    }

    //filter saturated sources
    if (s.getMaxValue() >= sourceMaxPixValueAllowed) {
      saturatedSource += s
      return false
    }
    true
  }
  //---------------------------------------------------------------------------
  private def filterGaiaSourceSeq(): Unit = {
    //-------------------------------------------------------------------------
    def filterGaiaSource(s: MatchedImageSource) = {
      val gas = s.catalogSource.asInstanceOf[GaiaPhotometricSource].gas
      if ((gas.gMeanMag == 0) || (gas.bpMeanMag == 0) || (gas.rpMeanMag == 0)) zeroMagnitude += s
      else if (!gas.bp_rpExcessFactor.isNaN  && gas.bp_rpExcessFactor > gaiaMaxAllowedBP_RP_ExcessFactor) bigBP_RP_ExcessFactor += s
      else if ((!gas.raError.isNaN && gas.raError > gaiaMaxAllowedRaError) || (gas.decError > gaiaMaxAllowedDecError)) bigRaDecError += s
      else if ((!gas.pmra_error.isNaN && gas.pmra_error > gaiaMaxAllowedRaProperMotionError) || (gas.pmdec_error > gaiaMaxAllowedDecProperMotionFactor)) bigProperMotionError += s
      else if (!gas.ruwe.isNaN && gas.ruwe > gaiaMaxAllowedRUWE_Error) invalidRUWE += s
      else if ((!gas.gMeanMag.isNaN && gas.gMeanMag < gaiaMinMagnitude) || (gas.bpMeanMag < gaiaMinMagnitude) || (gas.rpMeanMag < gaiaMinMagnitude)) invalidMagnitude += s
      else gaiaSourceSeq += s
    }
    //-------------------------------------------------------------------------
    if(verbose) info(s"Image: '${img.getRawName}' Gaia catalog before filtering: ${gaiaSourceCatalog.length}")
    gaiaSourceCatalog.foreach { gas =>
      if (filterSource(gas.imageSource)) filterGaiaSource(gas)
    }
    if(verbose) printSummary_1
  }
  //---------------------------------------------------------------------------
  def getApertureSeq(): Boolean = {
    if (useFixedAperture) {
      if(verbose) info(s"Image: '${img.getRawName()}' using fixed apertures ($minApertureCoef,$annularApertureCoef,$maxApertureCoef)")
      finalMinAperture     = minApertureCoef
      finalAnnularAperture = annularApertureCoef
      finalMaxAperture     = maxApertureCoef
      if(verbose) info(s"Image: '${img.getRawName()}' Fixed aperture: min: $finalMinAperture annular: $finalAnnularAperture max: $finalMaxAperture")
      true
    }
    else {
      finalMinAperture     = img.getFwhmAverage() * minApertureCoef
      finalAnnularAperture = img.getFwhmAverage() * annularApertureCoef
      finalMaxAperture     = finalAnnularAperture + maxApertureCoef
      if (finalMinAperture.isNaN || finalAnnularAperture.isNaN || finalMaxAperture.isNaN) {
        errorProcessingCatalog(ImageFocusInfo.STATUS_NAN_APERTURE)
        return false
      }
      if(verbose) info(s"Image: '${img.getRawName()}' FWHM average: " + f"${img.getFwhmAverage()}%.3f" + " aperture: min: " + f"$finalMinAperture%.3f" +
        " annular: " + f"$finalAnnularAperture%.3f" + " max: " + f"$finalMaxAperture%.3f")
      true
    }
  }
  //---------------------------------------------------------------------------
  private def printSummary_1(): Unit = {
    val name = img.getRawName
    info(s"Image: '$name' Image sources after GAIA catalog filter               :${gaiaSourceSeq.length}")
    info(s"Image: '$name' Discarded. Wrong size source count                    :${wrongSizeSource.length}")
    info(s"Image: '$name' Discarded. Saturated source count                     :${saturatedSource.length}")

    info(s"Image: '$name' Discarded. GAIA zero magnitude source count           :${zeroMagnitude.length}")
    info(s"Image: '$name' Discarded. GAIA high BP_RP excess factor source count :${bigBP_RP_ExcessFactor.length}")
    info(s"Image: '$name' Discarded. GAIA high (ra,dec) error source count      :${bigRaDecError.length}")
    info(s"Image: '$name' Discarded. GAIA too much error in proper motion       :${bigProperMotionError.length}")
    info(s"Image: '$name' Discarded. GAIA invalid ruwe                          :${invalidRUWE.length}")
    info(s"Image: '$name' Discarded. GAIA invalid magnitude                     :${invalidMagnitude.length}")

    info(s"Image: '$name' Discarded. Not matching GAIA source count             :${gaiaNoMatch.length}")
    info(s"Image: '$name' Discarded. Multiple matching GAIA source count        :${gaiaMultiple.length}")
  }
  //---------------------------------------------------------------------------
  private def printSummary_2(sourceSeq: Array[MatchedImageSource]): Unit = {
    //info
    info(s"Image: '${img.getRawName}' Discarded. Not enough SNR                             :${notEnoughSNR.length}")
    info(s"Image: '${img.getRawName}' Discarded. Source too close                           :${notIsolated.length}")
    info(s"Image: '${img.getRawName}' Final source count in photometry calculations         :${sourceSeq.size}")
  }

  //-------------------------------------------------------------------------
  private def synthesizeImage(name: String, seq: Array[Segment2D]): Unit =
    img.getRegion.synthesizeImageWithColor(
      name
      , seq
      , imageDimension = Some(img.getDimension)
      , blackAndWhite = true
      , font = Some(new Font(Font.MONOSPACED, Font.PLAIN, 10))
      , fontOffsetY = 3)
  //---------------------------------------------------------------------------
  private def saveDebugResults(sourceSeq: Array[MatchedImageSource]): Unit = {
    //-------------------------------------------------------------------------
    def saveCsvSourceSeq(_seq: ArrayBuffer[Segment2D], name: String): Unit = {
      if (_seq.length > 0) {
        val seq = _seq.toArray
        synthesizeImage(oDirFinalPng + s"$name(${seq.length})", seq)
        Segment2D.saveAsCsv(seq, oDirFinalCsv + s"$name(${seq.length}).tsv")
      }
    }
    //-------------------------------------------------------------------------
    def saveCsvPhotometricSourceSeq(_seq: Array[MatchedImageSource], name: String): Unit = {
      if (_seq.length > 0) {
        val seq = _seq.toArray
        synthesizeImage(oDirFinalPng + s"$name(${seq.length})", seq map (_.imageSource))
        GaiaPhotometricSource.saveAsCsv(seq, oDirFinalCsv + s"$name(${seq.length}).csv")
      }
    }
    //-------------------------------------------------------------------------
    saveCsvSourceSeq(wrongSizeSource, "discarded_00_wrong_size_source")
    saveCsvSourceSeq(saturatedSource, "discarded_01_saturated_source")

    saveCsvSourceSeq(gaiaNoMatch, "discarded_02_gaia_no_match")
    saveCsvSourceSeq(gaiaMultiple, "discarded_03_gaia_multiple_match")
    saveCsvPhotometricSourceSeq(zeroMagnitude.toArray, "discarded_04_gaia_zero_magnitude")
    saveCsvPhotometricSourceSeq(bigRaDecError.toArray, "discarded_05_big_ra_dec_error")
    saveCsvPhotometricSourceSeq(bigBP_RP_ExcessFactor.toArray, "discarded_06_gaia_big_rp_execess_factor")

    saveCsvPhotometricSourceSeq(bigProperMotionError.toArray, "discarded_07_proper_motion_error")
    saveCsvPhotometricSourceSeq(invalidRUWE.toArray, "discarded_08_ruwe")
    saveCsvPhotometricSourceSeq(invalidMagnitude.toArray, "discarded_09_magnitude")

    saveCsvPhotometricSourceSeq(notEnoughSNR.toArray, "discarded_10_not_enough_snr")
    saveCsvPhotometricSourceSeq(notIsolated.toArray, "discarded_11_not_isolated")

    saveCsvPhotometricSourceSeq(sourceSeq, "final_source_aperture")
    //-------------------------------------------------------------------------
  }

  //---------------------------------------------------------------------------
  private def filterBySnr(sourceSeq: Array[MatchedImageSource]) = {
    if(verbose) info(s"Image: '${img.getRawName()}' has: ${sourceSeq.size} sources before filtering photometric sources by SNR: $sourceMinSNR_Allowed")
    val mpoID_map = (focusCatalog map  { ast=> (ast.source.id, true)}).toMap

    val r = sourceSeq.filter { source =>
      if (mpoID_map.contains(source.imageSource.id)) true //avoid to filter the mpos
      else {
        val snr = source.catalogSource.asInstanceOf[GaiaPhotometricSource].getFluxSNR()
        if (snr.isNaN || snr.isInfinite || snr < sourceMinSNR_Allowed) {
          notEnoughSNR += source
          false
        }
        else true
      }
    }
    if(verbose) info(s"Image: '${img.getRawName()}' has: ${r.size} sources after filtering photometric sources by SNR: $sourceMinSNR_Allowed")
    r
  }

  //---------------------------------------------------------------------------
  private def filterByNoIsolatedSource(sourceSeq: Array[MatchedImageSource]) = {
    if(verbose) info(s"Image: '${img.getRawName()}' Has: ${sourceSeq.size} sources before filtering photometric sources by isolation: $sourceMinIsolatedDistance (pix)")
    import com.github.plokhotnyuk.rtree2d.core._
    import EuclideanPlane._
    val recSeq = sourceSeq.map { s => (s.imageSource.getRectangle(), s.imageSource.id) }
    val entrySeq = recSeq.map { case (rec, id) => EuclideanPlane.entry(rec.min.x, rec.min.y, rec.max.x, rec.max.y, id) }
    val rTree = RTree(entrySeq)
    val r = recSeq.zipWithIndex.flatMap { case ((rec, _), i) =>
      val closestSeqMin = rTree.nearestK(rec.min.x, rec.min.y, k = 2, sourceMinIsolatedDistance) //k =2 because 'rec' is included
      if (closestSeqMin.length > 1) {
        notIsolated += sourceSeq(i)
        None
      }
      else {
        val closestSeqMax = rTree.nearestK(rec.max.x, rec.max.y, k = 2, sourceMinIsolatedDistance) //k =2 because 'rec' is included
        if (closestSeqMax.length > 1) {
          notIsolated += sourceSeq(i)
          None
        }
        else Some(sourceSeq(i)) //None source is close enough
      }
    }
    if(verbose) info(s"Image: '${img.getRawName()}' Has: ${r.size} sources after filtering photometric sources by isolation: $sourceMinIsolatedDistance (pix)")
    r
  }
  //---------------------------------------------------------------------------
  def getFocusCatalogAsMatchedImageSource() =
    focusCatalog.map { s => MatchedImageSource(s.source, GaiaPhotometricSource(GaiaSourceReduced(s.source)))}
  //---------------------------------------------------------------------------
  def getUnknownGaiaCatalogAsMatchedImageSource() =
    unknownGaiaSourceCatalog map { s => MatchedImageSource(s, GaiaPhotometricSource(GaiaSourceReduced(s)))}
  //---------------------------------------------------------------------------
  def checkGaiaSourcesNotFound(sourceSeq: Array[MatchedImageSource]) = {
    val remainGaiaSourceSeq = sourceSeq flatMap { s => if (s.catalogSource._id != -1) Some(s) else None }
    getGaiaSourcesNotFoundInImage(remainGaiaSourceSeq)
    if(verbose) info(s"Image: '${img.getRawName}' Matched Gaia sources: ${gaiaSourceCatalog.length} Gaia sources not found in the image: ${gaiaSourceSeqNotFound.size}")

    if(additionalResults){
      val simulatedGaiaSource = gaiaSourceSeqNotFound flatMap { gaiaSource=>
        val pixPos = img.skyToPix(gaiaSource.getRaDec())
        if (!img.isIn(pixPos.toPoint2D())) None
        else {
          val simuluatedSourcePixSeq = Array.fill[PIXEL_DATA_TYPE](10, 10)(PixelDataType.PIXEL_ZERO_VALUE)
          val simulatedSource = Segment2D(simuluatedSourcePixSeq, img.offset)
          simulatedSource.setCentroidRaw(pixPos)
          simulatedSource.setCentroidRaDec(gaiaSource.getRaDec())
          Some(simulatedSource)
        }
      }
      synthesizeImage(oDirGaiaSourcesNotFoundPng + s"gaia_sources_not_found(${gaiaSourceSeqNotFound.length})"
        , simulatedGaiaSource.toArray)
      GaiaSourceReduced.saveAsCsv(gaiaSourceSeqNotFound.toArray
        , oDirGaiaSourcesNotFoundCsv + s"gaia_sources_not_found(${gaiaSourceSeqNotFound.length}).csv")
    }
  }
  //---------------------------------------------------------------------------
  def process(): Unit = {

    //get the source seq and the group
    val (allSourceSeq,noiseTide) = getSourceSeq()

    //get the catalog
    subGaia.buildMapAndKdTree(GaiaCatalog.getCatalog(img))
    val (_gaiaCatalog, _unknownGaiaCatalog, _multipleMatchGaiaCatalog) =
      subGaia.matchImageSourceSeq(
        img
        , allSourceSeq
        , matchRadiusMas = GaiaDB.matchRadiusMas
        , isBlindSearch = blindSearch
        , applyFilterByRaDecError = false
        , verbose)

    //set the calculated variables
    gaiaSourceCatalog = _gaiaCatalog
    unknownGaiaSourceCatalog = _unknownGaiaCatalog
    multipleMatchGaiaSourceCatalog = _multipleMatchGaiaCatalog
    focusCatalog = getFocusSeq()

    //try to get imageFocus on the image
    if (focusCatalog.length == 0) if(verbose) info(s"Image: '${img.getRawName}' can not find the required object")

    if (!blindSearch && focusCatalog.isEmpty) {
      errorProcessingCatalog("has no focus object after source detecting phase;" + ImageFocusInfo.STATUS_NO_OBJECT_FOUND_IN_DETECTION)
      if (!applyOnAllSources) return
      //check if all Gaia sources are present in the catalog is also present at the image
      getGaiaSourcesAtImage()
      if (gaiaSourceSeqAtImageMap.isEmpty) {
        errorProcessingCatalog(ImageFocusInfo.STATUS_NO_SOURCES_IN_THE_CATALOG)
        if (!applyOnAllSources) return
      }

      //check Gaia source not found in the image
      checkGaiaSourcesNotFound(gaiaSourceCatalog)
    }

    //filter image imageFocus that match with any GAIA source
    if (!astrometry && !blindSearch && !imageFocusInfo.get.isStar)
      filterFocusByOverlappingWithSourceInCatalog()

    if (!blindSearch && focusCatalog.isEmpty) {
      errorProcessingCatalog(ImageFocusInfo.STATUS_NO_VALID_FOCUS_FOUND + "->" + imageFocusInfo.get.getStatus())
      if (!applyOnAllSources) return
    }

    //filter the Gaia sources
    if (!astrometry && !blindSearch) {
      filterGaiaSourceSeq()
      if (gaiaSourceSeq.isEmpty) {
        errorProcessingCatalog("has not enough valid catalog sources. Possible wrong sources shape in the image " + ImageFocusInfo.STATUS_NO_CATALOG_PHOTOMETRIC_SOURCES)
        if (!applyOnAllSources) return
      }
    }

    var finalSourceSeq = Array[MatchedImageSource]()
    if (astrometry) finalSourceSeq ++= getFocusCatalogAsMatchedImageSource()
    else {
      //calculate the apertures to be used in the normalized flux
      if (!getApertureSeq()) return

      //get the sequence of sources to be processed
      finalSourceSeq = gaiaSourceSeq.toArray ++
        (if (applyOnAllSources) getUnknownGaiaCatalogAsMatchedImageSource else Array[MatchedImageSource]())

      if(verbose) info(s"Image: '${img.getRawName()}' Sources before filtering: ${finalSourceSeq.size} (GAIA sources: ${gaiaSourceSeq.size} and unknown sources: ${unknownGaiaSourceCatalog.size})")

      //filter not isolated sources
      finalSourceSeq = filterByNoIsolatedSource(finalSourceSeq)

      //add to final source seq the focus found if it was filtered
      val focusFilteredSeq = getFocusCatalogAsMatchedImageSource.flatMap { matchedSource =>
        if (finalSourceSeq.filter(_.imageSource.id == matchedSource.imageSource.id).isEmpty)
          Some(matchedSource)
        else None
      }

      finalSourceSeq = finalSourceSeq ++ focusFilteredSeq

      //calculate aperture photometry
      val apertureErrorMessage = ApertureTrait.calculate(
        img
        , finalSourceSeq
        , finalMinAperture
        , finalAnnularAperture
        , finalMaxAperture
        , apertureCoef = (useFixedAperture,minApertureCoef,annularApertureCoef,maxApertureCoef)
        , noiseTide = noiseTide
        , sourceMaxPix = sourceMaxPixCount)

      if (!apertureErrorMessage.isEmpty) {
        errorProcessingCatalog("Error calculating the aperture. Possible wrong sources shape in the image " + apertureErrorMessage + ImageFocusInfo.STATUS_APERTURE_ERROR)
        if (!applyOnAllSources) return
      }

      //filter sources by snr
      finalSourceSeq = filterBySnr(finalSourceSeq)
    }

    //summary
    if (verbose) printSummary_2(finalSourceSeq)

    //get the image quality
    updateImageQuality()

    //debug info
    if (additionalResults) {
      saveDebugResults(finalSourceSeq)
      saveDebugCatalog()
      saveObjectCroppy()
    }

    //finally, save the catalog
    saveCatalog(finalSourceSeq)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file PhotometryAbsolute.scala
//=============================================================================
