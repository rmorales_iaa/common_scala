/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  27/Jan/2023
 * Time:  10h:43m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.absolute.photometricModel
//=============================================================================
import com.common.geometry.point.Point2D_Double
//=============================================================================
//=============================================================================
case class MagnitudeEstimationPlotPoint(photoFilter: PhotometricFilter
                                        , fluxPerSecond: Double
                                        , filterValue: Double
                                        , referenceMagnitude: Double
                                        , m2_ID: Int) { //x axis
  //---------------------------------------------------------------------------
  val estimatedMagnitude = filterValue + (2.5 * Math.log10(fluxPerSecond)) //y axis
  //---------------------------------------------------------------------------
  def getPlotPoint = Point2D_Double(referenceMagnitude,estimatedMagnitude)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MagnitudeEstimationPlotPoint.scala
//=============================================================================
