/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  06/Feb/2023
 * Time:  10h:43m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.absolute.photometricModel
//=============================================================================
import com.common.database.mongoDB.Helpers.DocumentObservable
import com.common.database.mongoDB.MongoDB
import com.common.image.catalog.ImageCatalog.ImageSourceFocusMap
import com.common.image.catalog.SourceDB_Item
import com.common.image.focusType.ImageFocusTrait
import com.common.image.photometry.absolute.photometricModel.mpo.{MPO_CatalogDB, MPO_DB_Item}
import com.common.image.photometry.absolute.photometricModel.star.{StarCatalogDB, StarDB_Item}
import org.mongodb.scala.bson.collection.immutable.Document
//=============================================================================
//=============================================================================
object ImageFocusDB {
  //---------------------------------------------------------------------------
  final val COL_NAME_ID                        = "_id"
  //---------------------------------------------------------------------------
  def getStarFocusSeq(sourceFocusMap: ImageSourceFocusMap
                        , imageNameSeq: Seq[String]): Array[(SourceDB_Item, StarDB_Item)] = {
      (for(imageName <- imageNameSeq) yield {
        StarCatalogDB.getSourcePairSeq(sourceFocusMap(imageName).toArray)
      }).flatten.toArray
  }
  //---------------------------------------------------------------------------
  def getMpoFocusSeq(sourceFocusMap: ImageSourceFocusMap
                     , imageNameSeq: Seq[String]): Array[(SourceDB_Item, MPO_DB_Item)] = {
    (for (imageName <- imageNameSeq) yield {
      MPO_CatalogDB.getSourcePairSeq(sourceFocusMap(imageName).toArray)
    }).flatten.toArray
  }
  //---------------------------------------------------------------------------
  def getImageNameSeq(imageFocus: ImageFocusTrait, objectID : String) = {
    val focusDB = if (imageFocus.isStar) StarCatalogDB(objectID)
                  else MPO_CatalogDB(objectID)
    val r = focusDB.getImageNameSeq()
    focusDB.close()
    r
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import ImageFocusDB._
trait ImageFocusDB extends MongoDB {
  //---------------------------------------------------------------------------
  def getImageNameSeq(): Array[String] =
    collection
      .find(Document())
      .results()
      .map(doc => doc(COL_NAME_ID).asString().getValue)
      .toArray
  //---------------------------------------------------------------------------
  def getImageNameMap(): Map[String,String] =
    (getImageNameSeq() map {s=> (s,s)}).toMap
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ImageFocusDB.scala
//=============================================================================
