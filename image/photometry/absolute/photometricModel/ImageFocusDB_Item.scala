/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Feb/2023
 * Time:  20h:28m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.absolute.photometricModel
//=============================================================================
import com.common.image.catalog.SourceDB_Item
import com.common.image.photometry.absolute.photometricModel.mpo.MPO_DB_Item
import com.common.image.photometry.absolute.photometricModel.star.StarDB_Item
import com.common.util.time.Time
import com.common.image.photometry.magnitudeEstimation.MagnitudeEstimation
//=============================================================================
//=============================================================================
object ImageFocusDB_Item {
  //---------------------------------------------------------------------------
  def apply(imageName: String
            , source: SourceDB_Item
            , telescope: String
            , observingTime: String
            , exposureTime: Double
            , vr: Double
            , isStar: Boolean
            , spkVersion: Option[String] = None): ImageFocusDB_Item =
    if (isStar)
      new StarDB_Item(
        imageName
        , source.m2_id
        , source.focus_id
        , vr
        , Time.toJulian(observingTime))
    else {
      val mpo = MPO_DB_Item.build(
        imageName
        , source
        , telescope
        , observingTime
        , exposureTime
        , vr
        , spkVersion)
      if (mpo.isDefined) mpo.get
      else null
    }

  //---------------------------------------------------------------------------
}
//=============================================================================
trait ImageFocusDB_Item {
  val _id: String //image name
  val m2_id: Int
  val focus_id: String  //source id
  val v_r: Double
  val julian_date: Double
  var magnitude_estimation_seq: List[MagnitudeEstimation]
}
//=============================================================================
//End of file ImageFocusDB_Item.scala
//=============================================================================
