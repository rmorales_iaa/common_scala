/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  13/Jan/2023
 * Time:  10h:03m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.absolute.photometricModel
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.MongoDB
import com.common.database.mongoDB.database.CatalogSource
import com.common.geometry.point.Point2D_Double
import com.common.hardware.cpu.CPU
import com.common.image.astrometry.GaiaCatalog
import com.common.image.catalog.{ObjectPhotInfo, SourceDB_Item}
import com.common.image.photometry.absolute.PolynomialFit
import com.common.image.photometry.magnitudeEstimation.MagnitudeEstimation
import com.common.logger.MyLogger
import com.common.math.regression.Regression
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
object PhotometricModel extends MyLogger {
  //---------------------------------------------------------------------------
  val FIT_POLYNOMY_DEGREE = MyConf.c.getInt("Photometry.absolute.fitPolynomyDegree")
  //---------------------------------------------------------------------------
  val SIGMA_SCALE = MyConf.c.getDouble("Photometry.absolute.sigmaClipping")
  //---------------------------------------------------------------------------
  final val COL_NAME_IMAGE_NAME            = "image_name"
  //---------------------------------------------------------------------------
  final val VERBOSE = MyConf.c.getInt("Verbosity.verbose") == 1
  //---------------------------------------------------------------------------
  val headerCsv = Seq(
      COL_NAME_IMAGE_NAME
    , MagnitudeEstimation.COL_NAME_CATALOG
    , MagnitudeEstimation.COL_NAME_CATALOG_FILTER
  ) ++ PolynomialFit.coefficientsNameSeq(FIT_POLYNOMY_DEGREE)
  //---------------------------------------------------------------------------
  def save(imageName: String
           , photoMap: Map[String, Map[String, PolynomialFit]]
           , bwSummary: BufferedWriter
           , sep: String = "\t"): Unit = {


    if (photoMap.isEmpty) return  //avoid images with no photometric model
    info(s"Parsing file: '$imageName'")

    photoMap.keys.foreach { catalogName =>
      photoMap(catalogName).keys.foreach { filterName =>
        val curveFit = photoMap(catalogName)(filterName)
        val line = imageName +  sep +
                   catalogName + sep +
                   filterName + sep +
                   curveFit.asCsvLine().mkString(sep) + "\n"
        bwSummary.write(line)
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.common.image.photometry.absolute.photometricModel.PhotometricModel._
trait PhotometricModel extends MyLogger {
  //---------------------------------------------------------------------------
  val objectName: String
  val db:         MongoDB
  val catalogDB:  com.common.image.catalog.ImageCatalog
  val catalogName: String
  val photometricFilterSeq: Array[PhotometricFilter]
  //---------------------------------------------------------------------------
  def close() = {
    db.close()
    catalogDB.close()
  }
  //---------------------------------------------------------------------------
  def getFilterValueSeq(source: CatalogSource): (Double,Array[Double])  //(reference value, sequence of filter values od source)
  //---------------------------------------------------------------------------
  def getPhotometricFilter(catalogFilterName: String) =
    photometricFilterSeq.filter( _.name == catalogFilterName).head
  //---------------------------------------------------------------------------
  private def polynomialFit(magnitudeSeq: Seq[MagnitudeEstimationPlotPoint]
                         , filter: PhotometricFilter) = {
    val sortedSeq = magnitudeSeq
      .filter(!_.referenceMagnitude.isNaN)
      .sortWith(_.referenceMagnitude < _.referenceMagnitude)
    if (sortedSeq.isEmpty) None
    else {
      val plotPointSortedSeq = sortedSeq map (_.getPlotPoint)
      val (fittedPolinomy, (r2, stdDevResidualsY)) = Regression.fit(FIT_POLYNOMY_DEGREE, plotPointSortedSeq)
      if (r2.isNaN || stdDevResidualsY.isNaN) None
      else Some(PolynomialFit(fittedPolinomy, stdDevResidualsY, r2, FIT_POLYNOMY_DEGREE, filter.name))
    }
  }
  //-------------------------------------------------------------------------
  private def evaluatePoly(MagnitudeEstimationPlotPointSeq: Array[MagnitudeEstimationPlotPoint]
                            , fit: PolynomialFit) =
    MagnitudeEstimationPlotPointSeq.flatMap { s =>
      val plotPoint = s.getPlotPoint
      val actualValue = plotPoint.y
      val estimatedValue = fit.poly.value(plotPoint.x)
      val dif = Math.abs(actualValue - estimatedValue)
      if (dif.isNaN) None
      else Some(dif, s)
    }.map(_._2)
  //---------------------------------------------------------------------------
  private def calculateCurveFit(imageName: String
                                , magnitudeEstimationPlotPointSeq: Array[MagnitudeEstimationPlotPoint]
                                , filter: PhotometricFilter
                                , sigmaScale: Double
                                , outputRootPath: String
                                , additionaResults: Boolean): Option[PolynomialFit] = {

    if (VERBOSE) info(s"Image: '$imageName' catalog: '$catalogName' and catalog filter: '${filter.name}' using ${magnitudeEstimationPlotPointSeq.size} plot points")
    var dataFiltered = magnitudeEstimationPlotPointSeq
    var fit: Option[PolynomialFit] = None
    while (true) {
      fit = polynomialFit(dataFiltered,filter)
      if (fit.isEmpty) {
        warning(s"Image: '$imageName' error fitting a polynomial using photometric data. No data available")
        return None
      }
      val newDataFiltered = evaluatePoly(dataFiltered, fit.get)
      if (newDataFiltered.isEmpty) {
        warning(s"Image: '$imageName' error fitting a polynomial using photometric data. No data available after sigma clipping")
        return None
      }
      if (dataFiltered.length == newDataFiltered.length) {
        if (VERBOSE) info(s"Image: '$imageName' catalog: '$catalogName' and catalog filter: '${filter.name}' remain ${newDataFiltered.size} plot points")
        info(s"Image: '$imageName' catalog: '$catalogName' and catalog filter: '${filter.name}' Polynomial fitting degree: ${fit.get.degree} coef: ${fit.get.coefficientsAsString()}")
        if (additionaResults) saveCurveFit(imageName
                                           , filter
                                           , newDataFiltered
                                           , fit.get
                                           , outputRootPath)
        return fit
      }
      dataFiltered = newDataFiltered
    }
    None
  }
  //---------------------------------------------------------------------------
  private def saveCurveFit(imageName: String
                           , filter: PhotometricFilter
                           , sourceSeq : Array[MagnitudeEstimationPlotPoint]
                           , fit: PolynomialFit
                           , outputRootPath: String) = {

    val oDir = Path.resetDirectory(s"$outputRootPath/photometric_model_detailed/$imageName/${catalogName.replaceAll(" ","_")}/${filter.name}/")
    val csvFileName = oDir + "curve_fit.csv"
    val gnuPlotFileName = oDir + "curve_fit.gnuplot"

    additionaResultsCsv(imageName, csvFileName, filter, sourceSeq, fit)
    fit.save(oDir + "description.txt")

    val imageInfo = ObjectPhotInfo.get(imageName)
    GnuPlot.polynomyFit(imageName
      , csvFileName
      , gnuPlotFileName
      , fit
      , filter.name
      , imageInfo.minAperture
      , imageInfo.annularAperture
      , imageInfo.maxAperture)
  }
  //---------------------------------------------------------------------------
  private def processImage(imageName: String
                           , raRange: Point2D_Double
                           , decRange: Point2D_Double
                           , sourceSeq: Array[SourceDB_Item]
                           , outputRootPath: String
                           , additionalResults: Boolean) : Unit = {
    val plotPointMap = scala.collection.mutable.Map[String,ArrayBuffer[MagnitudeEstimationPlotPoint]]()
    val gaiaCatalog = GaiaCatalog.getCatalog(raRange.x, raRange.y, decRange.x, decRange.y)

    sourceSeq.foreach { source =>
      val sourceGaiaID = source.gaia_id
      if (sourceGaiaID != -1) { //avoid mpo(s) and non Gaia sources

         val gaiaSource = gaiaCatalog.find(_._id == sourceGaiaID).get
         val (referenceValue,filterValueSeq) = getFilterValueSeq(gaiaSource)

        (photometricFilterSeq zip filterValueSeq).foreach { case (catalogFilter, filterValue) =>
          val plotPoint = MagnitudeEstimationPlotPoint(
            catalogFilter
            , source.flux_per_second
            , filterValue
            , referenceValue
            , source.m2_id)
          if (!plotPointMap.contains(catalogFilter.name)) plotPointMap(catalogFilter.name) = ArrayBuffer[MagnitudeEstimationPlotPoint]()
          plotPointMap(catalogFilter.name) += plotPoint
        }

      }
    }

    //calculate the fit
    val curveFitSeq = (for((catalogFilterName,plotPointSeq) <- plotPointMap) yield {
      calculateCurveFit(imageName
                        , plotPointSeq.toArray
                        , getPhotometricFilter(catalogFilterName)
                        , SIGMA_SCALE
                        , outputRootPath
                        , additionalResults)
    }).toArray.flatten

    if (!curveFitSeq.isEmpty) {
      val photometricModelSeq = PhotometricModelDB_Item.buildSeq(
          catalogName
        , photometricFilterSeq
        , curveFitSeq)

      //add to database
      if (!photometricModelSeq.isEmpty)
        catalogDB.updatePhotometryModelSeq(photometricModelSeq, imageName)
    }
  }
  //---------------------------------------------------------------------------
  def calculate(outputRootPath:String
                , inputImageNameSeq: List[String]
                , additionalResults: Boolean) = {
    //-------------------------------------------------------------------------
    class MyParallelImageSeq(seq: Array[(String, Point2D_Double, Point2D_Double)]) extends ParallelTask[(String, Point2D_Double, Point2D_Double)](
      seq
      , CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 100) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(t: (String, Point2D_Double, Point2D_Double)): Unit = {
        Try {
          val imageName = t._1
          if (!catalogDB.imageHasPhotometryModel(imageName)) {
            catalogDB.addAstrometricInfo(imageName)
            processImage(
                imageName
              , t._2
              , t._3
              , catalogDB.getSourceSeq(imageName).filter(_.gaia_id > 0)
              , outputRootPath
              , additionalResults)
          }
          else
            warning(s"Image: '$imageName' has already a photometric model calculated, ignoring it")
        }
        match {
          case Success(_) =>
          case Failure(ex) =>
            error(s"Error getting the photometric model of '${t._1}'" + ex.toString)
        }
      }
      //-----------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    val imagePosSeq = catalogDB.getImageArea().filter(s=> inputImageNameSeq.contains(s._1))
    info(s"Applying photometric models on: ${imagePosSeq.size} images")
    new MyParallelImageSeq(imagePosSeq.toArray)
  }
  //---------------------------------------------------------------------------
  private def additionaResultsCsv(imageName: String
                                  , csvFileName: String
                                  , filter: PhotometricFilter
                                  , sourceSeq: Array[MagnitudeEstimationPlotPoint]
                                  , fit: PolynomialFit) = {

    val filterName = filter.name
    info(s"Image: '$imageName' generating photometric curve and gnuplot script for filter: '$filterName'")
    val sortedSeq = sourceSeq.sortWith( _.referenceMagnitude < _.referenceMagnitude)
    //---------------------------------------------------------------------------
    val bw = new BufferedWriter(new FileWriter(new File(csvFileName)))
    val stdDevResidualsY = fit.stdDevResiduals
    val coefficientsAsString = fit.coefficientsAsString()
    val r2 = fit.r2
    val sep = "\t"
    if (sortedSeq.isEmpty) bw.write("#None source matched\n")
    else {
      val plotPointSortedSeq = sortedSeq map (_.getPlotPoint)
      bw.write(s"cat_ref_mag" + sep)
      bw.write(s"cat_mag_$filterName" + sep)
      bw.write(s"estimated_mag(curve_fit)" + sep)
      bw.write(s"estimated_mag_min" + sep)
      bw.write(s"estimated_mag_max" + sep)
      bw.write(s"source_id \n")

      plotPointSortedSeq.zipWithIndex.foreach { case (p, i) =>
        val yPol = fit.poly.value(p.x)
        bw.write(s"${p.x}$sep${p.y}$sep$yPol$sep${yPol - stdDevResidualsY}$sep${yPol + stdDevResidualsY}$sep${sourceSeq(i).m2_ID}\n")
      }
      info(s"Image: '$imageName' polynomial curve fitting filter $filterName. Degree ${fit.degree}        : $coefficientsAsString")
      info(s"Image: '$imageName' polynomial curve fitting filter $filterName. Standard deviation of residuals   : ${f"$stdDevResidualsY%.3f"}")
      info(s"Image: '$imageName' polynomial curve fitting filter $filterName. Coefficient of determination (r2) : ${f"$r2%.3f"}")
    }
    bw.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file PhotometricModel.scala
//=============================================================================
