/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  17/Jan/2023
 * Time:  12h:12m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.absolute.photometricModel
//=============================================================================
import com.common.image.photometry.absolute.PolynomialFit
import com.common.util.path.Path
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object GnuPlot {
  //---------------------------------------------------------------------------
  private def normalizeString(s: String) =
    s.replaceAll("_", """\\\\_""")
  //---------------------------------------------------------------------------
  private def normalizeString2(s: String) =
    s.replaceAll("_", """\\\_""")
  //---------------------------------------------------------------------------
  def polynomyFit(imageName: String
                  , csvFileName: String
                  , gnuPlotFileName: String
                  , fit: PolynomialFit
                  , filterName: String
                  , minAperture: Double
                  , anularAperture: Double
                  , maxAperture: Double): Unit = {

    val coefSeq = fit.coefficientsAsString()
    val stdDev = fit.stdDevResiduals
    val r2 = fit.r2
    val s =
      s"""
#start of gnu script
#--------------------------------------
#variables

my_filter='filter: ${normalizeString2(filterName)}'
my_equation='y = Ri + (2.5 log10(fluxi)) =  B3(Vi-Ri)^3 + B2(Vi-Ri)^2 +  B1(Vi-Ri) + B0'

my_coef_seq='${normalizeString(coefSeq)}'
my_std='std dev residuals = ${f"$stdDev%.3f"} r2: ${f"$r2%.3f"}'
my_apeture='aperture: min:${f"$minAperture%.3f"} annular:${f"$anularAperture%.3f"} max:${f"$maxAperture%.3f"}'
my_title="Absolute photometric calibration of image: '${normalizeString(imageName)}'" . "\\n" . my_filter . "\\n" . my_equation . "\\n" . my_coef_seq . "\\n" . my_std . "\\n" . my_apeture

my_x_axis_label='x (ref mag)'
my_y_axis_label='y (est mag)'
#--------------------------------------
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
set xtic auto                          # set xtics automatically
set ytic auto                          # set ytics automatically
set title my_title
set xlabel my_x_axis_label
set ylabel my_y_axis_label
set style fill transparent solid 0.3
#--------------------------------------
plot '${Path.getCurrentPath() + csvFileName}' \\
   using 1:2 lc rgb 'blue' title 'flux magnitude' ,\\
'' using 1:3 lc rgb 'red' with lines title 'fitted polynomy' ,\\
'' using 1:4:5 lc rgb 'gold' with filledcurves title 'std dev residual error'
#--------------------------------------
pause -1
#--------------------------------------
#end of gnu script"""
    val bw = new BufferedWriter(new FileWriter(new File(gnuPlotFileName)))
    bw.write(s)
    bw.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file GnuPlot.scala
//=============================================================================
