/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  11/Jan/2023
 * Time:  20h:12m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.absolute.photometricModel.catalog.gaia
//=============================================================================
import com.common.database.mongoDB.database.CatalogSource
import com.common.database.mongoDB.database.gaia.{GaiaDB, GaiaSourceReduced}
import com.common.image.filter._
import com.common.image.photometry.absolute.photometricModel._
//=============================================================================
//=============================================================================
object GaiaPhotometricModel {
  //---------------------------------------------------------------------------
  val photometricFilterSeq = Array(
      PhotometricFilter(JohnsonV().name)
    , PhotometricFilter(JohnsonR().name)
    , PhotometricFilter(JohnsonI().name)
  )
  //---------------------------------------------------------------------------
  //"Gaia Early Data Release 3: Photometric content and validation" Riello et al. 2021. AsciiTable C.2. Jhonson-Cousins relationships. https://arxiv.org/abs/2012.01916
  //fixme: update the equations when it is available
  private def calculateV_R_I_GAIA_3(source: GaiaSourceReduced) = {
    if ((source.bpMeanMag == -1) || (source.rpMeanMag == -1)) (-1d, -1d, -1d)
    else {
      val x = source.bpMeanMag - source.rpMeanMag
      val x2 = x * x
      val x3 = x2 * x
      val x4 = x3 * x
      val V = source.gMeanMag + 0.02704 - (0.01424 * x) + (0.2156 * x2) - (0.01426 * x3)
      val R = source.gMeanMag + 0.02275 - (0.3961 * x) + (0.1243 * x2) + (0.01396 * x3) - (0.003775 * x4)
      val I = source.gMeanMag - 0.01753 - (0.76 * x) + (0.0991 * x2)
      (V, R, I)
    }
  }
  //---------------------------------------------------------------------------
  private def calculateV_R_I_GAIA_3_AsSeq(source: GaiaSourceReduced): Array[Double] = {
    val (gaiafilterV, gaiafilterR, gaiafilterI) = calculateV_R_I_GAIA_3(source)
    Array(gaiafilterV, gaiafilterR, gaiafilterI)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import GaiaPhotometricModel._
case class GaiaPhotometricModel(objectName:String) extends PhotometricModel {
  //---------------------------------------------------------------------------
  val db          = GaiaDB()
  val catalogName = GaiaDB.CATALOG_NAME
  val catalogDB   = com.common.image.catalog.ImageCatalog(objectName)
  val photometricFilterSeq = GaiaPhotometricModel.photometricFilterSeq
  //---------------------------------------------------------------------------
  def getFilterValueSeq(source: CatalogSource): (Double,Array[Double]) = {
    val gaiaFilterValueSeq = calculateV_R_I_GAIA_3_AsSeq(source.asInstanceOf[GaiaSourceReduced])
    val gaiaV_Minus_R = gaiaFilterValueSeq(0) - gaiaFilterValueSeq(1)
    (gaiaV_Minus_R,gaiaFilterValueSeq)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file GaiaPhotometricModel.scala
//=============================================================================
