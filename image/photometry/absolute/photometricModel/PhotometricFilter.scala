/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/Jan/2023
 * Time:  18h:33m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.absolute.photometricModel
//=============================================================================
//=============================================================================
case class PhotometricFilter(name: String) {
  //---------------------------------------------------------------------------
  def == (other: PhotometricFilter) = name == other.name
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file PhotometricFilter.scala
//=============================================================================