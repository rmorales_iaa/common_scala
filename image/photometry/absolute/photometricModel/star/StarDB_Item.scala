/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Feb/2023
 * Time:  20h:36m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.absolute.photometricModel.star
//=============================================================================
import com.common.csv.{CsvItem, CsvRow}
import com.common.image.catalog.SourceDB_Item
import com.common.image.photometry.absolute.photometricModel.ImageFocusDB_Item
import org.mongodb.scala.bson.codecs.Macros._
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import com.common.image.photometry.magnitudeEstimation.MagnitudeEstimation

//=============================================================================
//=============================================================================
object StarDB_Item {
  //---------------------------------------------------------------------------
  //https://www.jannikarndt.de/blog/2017/08/writing_case_classes_to_mongodb_in_scala/
  private val customCodecs = fromProviders(classOf[StarDB_Item]
                                         , classOf[StarEstMagDB_Item])

  val codecRegistry = fromRegistries(customCodecs
    , DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  final val COL_NAME_ID              = "_id"
  final val COL_NAME_M2_ID           = SourceDB_Item.COL_NAME_M2_ID
  final val COL_NAME_FOCUS_OBJECT_ID = SourceDB_Item.COL_NAME_FOCUS_ID
  final val COL_NAME_V_R             = "v_r"
  final val COL_NAME_JULIAN_DATE     = "julian_date"
  //---------------------------------------------------------------------------
  private val columnSeq = Seq(
      COL_NAME_ID
    , COL_NAME_M2_ID
    , COL_NAME_FOCUS_OBJECT_ID
    , COL_NAME_V_R
    , COL_NAME_JULIAN_DATE)
  //---------------------------------------------------------------------------
  final val headerRow = {
    val row = CsvRow()
    columnSeq foreach (row + CsvItem(_))
    row
  }
  //---------------------------------------------------------------------------
  def apply(imageName: String
            , source: SourceDB_Item
            , vr: Double): StarDB_Item = {
    val row = CsvRow()

    row + CsvItem(COL_NAME_ID, imageName)
    row + CsvItem(COL_NAME_M2_ID, source.m2_id)
    row + CsvItem(COL_NAME_FOCUS_OBJECT_ID, source.focus_id)
    row + CsvItem(COL_NAME_V_R, vr)
    row + CsvItem(COL_NAME_JULIAN_DATE, vr)

    //finnaly, build the object
    StarDB_Item(row)
  }
  //---------------------------------------------------------------------------
  def apply(row: CsvRow): StarDB_Item =
    StarDB_Item(
      _id            = row.getString(COL_NAME_ID)
      , focus_id      = row.getString(COL_NAME_FOCUS_OBJECT_ID)
      , m2_id        = row.getInt(COL_NAME_M2_ID)
      , v_r          = row.getDouble(COL_NAME_V_R)
      , julian_date  = row.getDouble(COL_NAME_JULIAN_DATE)
    )

  //---------------------------------------------------------------------------
  private final val headerNameToDB_ColName = {
    classOf[StarDB_Item].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val headerName = f.getName()
      val dbColName = headerRow.getColName(headerName).getOrElse("None")
      f.setAccessible(false)
      (headerName, dbColName)
    }.toMap
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import StarDB_Item._
case class StarDB_Item(_id: String //image name
                       , m2_id: Int
                       , focus_id: String
                       , v_r: Double
                       , julian_date: Double
                       , var magnitude_estimation_seq: List[MagnitudeEstimation] = null
                      ) extends ImageFocusDB_Item {
  //---------------------------------------------------------------------------
  def toRow() = {
    val nameValueMap = this.getClass.getDeclaredFields.map { f =>
      f.setAccessible(true)
      val headerName = f.getName()
      val r = (headerNameToDB_ColName(headerName), f.get(this).toString)
      f.setAccessible(false)
      r
    }.toMap
    CsvRow(nameValueMap)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file StarDB_Item.scala
//=============================================================================
