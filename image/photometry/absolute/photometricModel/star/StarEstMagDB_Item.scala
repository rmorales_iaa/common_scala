/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  27/Jan/2023
 * Time:  10h:51m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.absolute.photometricModel.star
//=============================================================================
import com.common.image.astrometry.fov.FovSource
import com.common.image.catalog.SourceDB_Item
import com.common.image.photometry.absolute.PolynomialFit
import com.common.image.photometry.absolute.photometricModel.{ImageFocusDB_Item, PhotometricModel}
import com.common.image.photometry.magnitudeEstimation.MagnitudeEstimation.estimateMagnitudeAndError
import com.common.util.file.MyFile
import com.common.image.photometry.magnitudeEstimation.MagnitudeEstimation
//=============================================================================
import java.io.BufferedWriter
import java.io.{File, FileWriter}
//=============================================================================
//=============================================================================
object StarEstMagDB_Item {
  //---------------------------------------------------------------------------
  private val headerCsv = Seq(
    PhotometricModel.COL_NAME_IMAGE_NAME
    , s"${SourceDB_Item.COL_NAME_M2_ID}"
    , s"${SourceDB_Item.COL_NAME_FOCUS_ID}"
    , s"${SourceDB_Item.COL_NAME_GAIA_ID}"
    , "image_filter"
    , MagnitudeEstimation.COL_NAME_CATALOG
    , MagnitudeEstimation.COL_NAME_CATALOG_FILTER
    , "V_R"
    , "flux_per_second"
    , "snr"
    , "julian_date_mid_point"
    , "estimated_magnitude"
    , "estimated_magnitude_error"
  )
  //---------------------------------------------------------------------------
  def apply(catalog: String
            , filter: String
            , source: SourceDB_Item
            , focus: ImageFocusDB_Item
            , curveFit: PolynomialFit
            , vr: Double) : StarEstMagDB_Item  = {
    val (estimatedMagnitude, estimatedMagnitudeError) =
      estimateMagnitudeAndError(source.flux_per_second
                               , source.snr
                               , curveFit, vr)
    new StarEstMagDB_Item(catalog
                          , filter
                          , focus.julian_date
                          , estimatedMagnitude
                          , estimatedMagnitudeError)
  }

  //---------------------------------------------------------------------------
  def apply(catalog: String
            , filter: String
            , source: FovSource
            , julianDate: Double
            , curveFit: PolynomialFit
            , vr: Double): StarEstMagDB_Item = {
    val (estimatedMagnitude, estimatedMagnitudeError) =
      estimateMagnitudeAndError(source.flux_per_second
                                , source.snr
                                , curveFit
                                , vr)
    new StarEstMagDB_Item(
        catalog
      , filter
      , julianDate
      , estimatedMagnitude
      , estimatedMagnitudeError)
  }
  //---------------------------------------------------------------------------
  def createCsv(csvFileName: String
                , sep: String): BufferedWriter = {
    val append = MyFile.fileExist(csvFileName)
    val bw = new BufferedWriter(new FileWriter(new File(csvFileName),append))
    if (append) return bw
    bw.write(headerCsv.mkString(sep) + "\n")
    bw
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class StarEstMagDB_Item(catalog: String
                             , catalog_filter: String
                             , julian_date: Double
                             , var est_magnitude: Double
                             , est_magnitude_error: Double) extends MagnitudeEstimation {
  //---------------------------------------------------------------------------
  def saveAsCsvLine(imageName: String
                    , imageFilter: String
                    , julianDate: Double
                    , source: SourceDB_Item
                    , catalogName: String
                    , catalogFilter: String
                    , vr: Double
                    , bwSeq: Seq[BufferedWriter]
                    , sep: String = "\t") =
    bwSeq.foreach { bw =>
      bw.write(imageName + sep)
      bw.write(source.m2_id + sep)
      bw.write(source.focus_id + sep)
      bw.write(source.gaia_id + sep)
      bw.write(imageFilter + sep)
      bw.write(catalogName + sep)
      bw.write(catalogFilter + sep)
      bw.write(vr + sep)
      bw.write(source.flux_per_second + sep)
      bw.write(source.snr + sep)
      bw.write(julianDate + sep)
      bw.write(est_magnitude + sep)
      bw.write(est_magnitude_error + "\n")
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file StarEstMagDB_Item.scala
//=============================================================================
