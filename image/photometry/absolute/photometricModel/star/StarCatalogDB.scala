/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  11/Jan/2023
 * Time:  13h:12m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.absolute.photometricModel.star
//=============================================================================
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.image.catalog.ImageCatalog.{ImageByFilterMap, ImageSourceFocusMap}
import com.common.image.catalog.SourceDB_Item
import com.common.image.photometry.absolute.photometricModel.{ImageFocusDB, ImageFocusDB_Item}
import com.common.image.photometry.absolute.photometricModel.star.StarDB_Item._
import com.common.logger.MyLogger
//=============================================================================
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
//=============================================================================
object StarCatalogDB extends MyLogger {
  //---------------------------------------------------------------------------
  private final val DATABASE_NAME   = "star_catalog"
  //---------------------------------------------------------------------------
  def normalizeColNameForDatabase(s: String) = {
    val posOpenParenthesis = s.indexOf("(")
    if (posOpenParenthesis == -1) s else s.take(posOpenParenthesis)
  }
  //---------------------------------------------------------------------------
  def getSourcePairSeq(seq: Array[(SourceDB_Item, ImageFocusDB_Item)]) =
    seq.map(t => (t._1, t._2.asInstanceOf[StarDB_Item])).toArray
  //-------------------------------------------------------------------------
  def addToDatabase(objectName: String
                    , sourceFocusMap: ImageSourceFocusMap
                    , imageByFilterMap: ImageByFilterMap): Unit = {
    //add to database
    val db = StarCatalogDB(objectName)
    db.dropCollection()
    info(s"Adding to star database the object '$objectName'")

    for ((_, imageNameSeq) <- imageByFilterMap) yield {
      val starSeq = getSourcePairSeq((imageNameSeq map (sourceFocusMap(_))).flatten.toArray).map(_._2).toSeq
      if (!starSeq.isEmpty) db.add(starSeq)
    }
    db.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import StarCatalogDB._
case class StarCatalogDB(collectionName: String) extends ImageFocusDB {
  //-------------------------------------------------------------------------
  val databaseName = DATABASE_NAME
  //-------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) =
    initWithCodeRegister(StarDB_Item.codecRegistry)
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  val collWithRegistry: MongoCollection[StarDB_Item] = database.getCollection(collectionName)
  connected = true
  //-------------------------------------------------------------------------
  def add(StarSeq: Seq[StarDB_Item]): Unit = {
    if (existCollection(collectionName))
      StarSeq.foreach { Star =>
        if (exist(Star._id)) collWithRegistry.replaceOne(equal(COL_NAME_ID, Star._id), Star).headResult
        else collWithRegistry.insertOne(Star).headResult
      }
    else
      collWithRegistry.insertMany(StarSeq).headResult
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file StarCatalogDB.scala
//=============================================================================