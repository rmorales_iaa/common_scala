/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  17/Jan/2023
 * Time:  18h:23m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.absolute.photometricModel
//=============================================================================
import com.common.image.photometry.absolute.PolynomialFit
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
//=============================================================================
//=============================================================================
object PhotometricModelDB_Item {
  //---------------------------------------------------------------------------
  //https://www.jannikarndt.de/blog/2017/08/writing_case_classes_to_mongodb_in_scala/
  private val customCodecs = fromProviders(classOf[PhotometricModelDB_Item])

  val codecRegistry = fromRegistries(customCodecs
                                     , DEFAULT_CODEC_REGISTRY)

  //---------------------------------------------------------------------------
  def buildSeq(catalogName: String
               , photometricFilterSeq: Array[PhotometricFilter]
               , curveFitSeq: Array[PolynomialFit]) =

    photometricFilterSeq.flatMap { photometricFilter =>
      val curveSeq = curveFitSeq.filter( _.filterName == photometricFilter.name)
      if (curveSeq.isEmpty) None
      else {
        val curve = curveSeq.head
        Some(PhotometricModelDB_Item(catalogName
          , photometricFilter.name
          , curve.toString))
      }
    }.toSeq
  //---------------------------------------------------------------------------
  def getCatalogAndFilterSeq(seq: Seq[PhotometricModelDB_Item]) = {
    val catalogSet = scala.collection.mutable.Set[String]()
    val filterSet = scala.collection.mutable.Set[String]()
    seq.foreach { pm=>
      catalogSet += pm.catalogName
      filterSet  += pm.filterName
    }
    (catalogSet.toSeq,filterSet.toSeq)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class PhotometricModelDB_Item(catalogName: String
                                    , filterName: String
                                    , coefSeq: String) {
  //---------------------------------------------------------------------------
  def getCurveFitPolynomy() = PolynomialFit(coefSeq, filterName).get
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file PhotometricModelDB_Item.scala
//=============================================================================
