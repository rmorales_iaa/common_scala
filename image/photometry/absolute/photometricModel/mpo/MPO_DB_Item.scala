/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Jan/2023
 * Time:  12h:27m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.absolute.photometricModel.mpo
//=============================================================================
import com.common.constant.astronomy.AstronomicalUnit.{ASTRONOMICAL_UNIT_KM, LIGHT_SPEED_KM_S}
import com.common.coordinate.spherical.SphericalCoordinate.getObservedApparentPosition
import com.common.csv.{CsvItem, CsvRow}
import com.common.geometry.point.Point2D_Double
import com.common.image.catalog.SourceDB_Item
import com.common.image.focusType.ImageFocusMPO
import com.common.image.photometry.absolute.photometricModel.ImageFocusDB_Item
import com.common.image.photometry.absolute.photometricModel.star.StarEstMagDB_Item
import com.common.image.photometry.magnitudeEstimation.MagnitudeEstimation
import com.common.image.telescope.Telescope
import com.common.jpl.Spice
import com.common.jpl.Spice.{SpiceSourceInfo, getHeliocentricRange}
import com.common.jpl.horizons.db.smallBody.SmallBodyDB
import com.common.util.time.Time
//=============================================================================
import org.bson.codecs.configuration.CodecRegistries._
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
//=============================================================================
//=============================================================================
object MPO_DB_Item {
  //---------------------------------------------------------------------------
  private val customCodecs = fromProviders(
      classOf[MPO_DB_Item]
    , classOf[MPO_EstMagDB_Item]
    , classOf[StarEstMagDB_Item]
    , classOf[MagnitudeEstimation])

  val codecRegistry = fromRegistries(
      customCodecs
    , DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------¡
  private val smallBodyDB = SmallBodyDB()
  //---------------------------------------------------------------------------
  final val COL_NAME_ID                        = "_id"
  final val COL_NAME_M2_ID                     = SourceDB_Item.COL_NAME_M2_ID
  final val COL_NAME_FOCUS_OBJECT_ID           = SourceDB_Item.COL_NAME_FOCUS_ID
  //---------------------------------------------------------------------------
  final val COL_NAME_V_R                       = "v_r"
  //---------------------------------------------------------------------------
  final val COL_NAME_M2_EST_RA                 = "m2_est_ra(dec_deg)"
  final val COL_NAME_M2_EST_DEC                = "m2_est_dec(dec_deg)"
  final val COL_NAME_M2_EST_RA_RESIDUAL        = "m2_est_ra_residual(mas)"
  final val COL_NAME_M2_EST_DEC_RESIDUAL       = "m2_est_dec_residual(mas)"
  //---------------------------------------------------------------------------
  final val COL_NAME_M2_JPL_RA                 = "m2_jpl_ra_orbit(dec_deg)"
  final val COL_NAME_M2_JPL_DEC                = "m2_jpl_dec_orbit(dec_deg)"
  final val COL_NAME_M2_JPL_LIGHT_TIME         = "m2_jpl_light_time(min)"
  final val COL_NAME_M2_JPL_PHASE_ANGLE        = "m2_jpl_phase_angle(deg)"
  final val COL_NAME_M2_JPL_JD_MID_CORRECTED   = "m2_jpl_julian_date_mid_point_corrected"
  final val COL_NAME_M2_JPL_HELIOCENTRE_RANGE  = "m2_jpl_heliocentric_range(r)(au)"
  final val COL_NAME_M2_JPL_OBSERVER_RANGE     = "m2_observer_range(delta)(au)"
  final val COL_NAME_M2_JPL_EST_AZIMUT         = "m2_est_azimut(deg)"
  final val COL_NAME_M2_JPL_EST_ZENITH         = "m2_est_zenith(deg)"
  final val COL_NAME_M2_JPL_EST_HOUR_ANGLE     = "m2_est_hour_angle(deg)"
  final val COL_NAME_M2_JPL_EST_ALTITUDE       = "m2_est_altitude(km)"
  final val COL_NAME_M2_JPL_EST_AIRMASS        = "m2_est_air_mass"
  final val COL_NAME_M2_JPL_EST_EXTINTION      = "m2_est_extinction"
  final val COL_NAME_M2_JPL_EST_PAR_ANGLE      = "m2_est_parallactic_angle(deg)"
  final val COL_NAME_M2_JPL_ORBIT_VERSION      = "m2_jpl_orbit_version"
  //---------------------------------------------------------------------------
  final val COL_NAME_OBSERVING_TIME            = "observing_time"
  final val COL_NAME_JULIAN_DATE               = "julian_date"
  final val COL_NAME_JULIAN_DATE_MID_POINT     = "julian_date_mid_point"
  //---------------------------------------------------------------------------
  private val columnSeq = Seq(
      COL_NAME_ID
    , COL_NAME_M2_ID
    , COL_NAME_FOCUS_OBJECT_ID
    //----------------------------------------
    , COL_NAME_OBSERVING_TIME
    , COL_NAME_JULIAN_DATE
    , COL_NAME_JULIAN_DATE_MID_POINT
    //----------------------------------------
    , COL_NAME_V_R
    //----------------------------------------
    , COL_NAME_M2_JPL_RA
    , COL_NAME_M2_JPL_DEC
    , COL_NAME_M2_EST_RA_RESIDUAL
    , COL_NAME_M2_EST_DEC_RESIDUAL
    //----------------------------------------
    , COL_NAME_M2_JPL_LIGHT_TIME
    , COL_NAME_M2_JPL_PHASE_ANGLE
    , COL_NAME_M2_JPL_JD_MID_CORRECTED
    , COL_NAME_M2_JPL_HELIOCENTRE_RANGE
    , COL_NAME_M2_JPL_OBSERVER_RANGE
    , COL_NAME_M2_JPL_EST_AZIMUT
    , COL_NAME_M2_JPL_EST_ZENITH
    , COL_NAME_M2_JPL_EST_HOUR_ANGLE
    , COL_NAME_M2_JPL_EST_ALTITUDE
    , COL_NAME_M2_JPL_EST_AIRMASS
    , COL_NAME_M2_JPL_EST_EXTINTION
    , COL_NAME_M2_JPL_EST_PAR_ANGLE
    , COL_NAME_M2_JPL_ORBIT_VERSION
    //----------------------------------------
  )
  //---------------------------------------------------------------------------
  final val headerRow = {
    val row = CsvRow()
    columnSeq foreach (row + CsvItem(_))
    row
  }
  //---------------------------------------------------------------------------
  def build(imageName: String
            , source: SourceDB_Item
            , telescope: String
            , observingTime: String
            , exposureTime: Double
            , vr: Double
            , spkVersion: Option[String]): Option[MPO_DB_Item] = {

    //spice info
    val observerSpkID = Telescope.getSpkId(telescope)
    val spkPath = getSpkPath(source,spkVersion)
    val targetSpkID = source.focus_id.split("_")(1).toLong

    //load spìce kernel
    if (!Spice.loadKernel(spkPath)) return null   //it will be unloaded when 'Spice.close'

    val spiceSourceInfo = getSpiceSourceInfo(
        source.ra
      , source.dec
      , observerSpkID
      , targetSpkID.toString
      , telescope
      , observingTime
      , exposureTime
      , calculateTimeMidPoint = true)

    //build the mpo info
    Some(
      MPO_DB_Item(
        imageName
      , observingTime
      , exposureTime
      , source
      , spiceSourceInfo
      , targetSpkID.toString
      , vr))
    }
  //---------------------------------------------------------------------------
  def apply(imageName: String
            , observingTime: String
            , exposureTime: Double
            , source: SourceDB_Item
            , spiceSourceInfo: SpiceSourceInfo
            , spkID: String
            , vr: Double) : MPO_DB_Item = {

    //image julian date
    val ldt = LocalDateTime.parse(observingTime)
    val julianDate = Time.toJulian(ldt)
    val julianDateMidPoint = Time.toJulian(ldt.plusNanos(Time.secondsToNanos(exposureTime / 2d))) //append the half of the exposure time

    //calculate the distance to jpl orbit
    val raDecImage = Point2D_Double(source.ra, source.dec)
    val jplRaDec = Point2D_Double(spiceSourceInfo.ra, spiceSourceInfo.dec)
    val raDecResidual = jplRaDec.getResidualMas(raDecImage)

    val row = CsvRow()

    row + CsvItem(COL_NAME_ID,                        imageName)
    row + CsvItem(COL_NAME_M2_ID,                     source.m2_id)
    row + CsvItem(COL_NAME_FOCUS_OBJECT_ID,           source.focus_id)
    //---------------------------------------------------------------------------
    row + CsvItem(COL_NAME_OBSERVING_TIME,            observingTime)
    row + CsvItem(COL_NAME_JULIAN_DATE,               julianDate.toString)
    row + CsvItem(COL_NAME_JULIAN_DATE_MID_POINT,     julianDateMidPoint.toString)
    //---------------------------------------------------------------------------
    row + CsvItem(COL_NAME_V_R,                       vr)
    //---------------------------------------------------------------------------
    row + CsvItem(COL_NAME_M2_EST_RA,                 source.ra)
    row + CsvItem(COL_NAME_M2_EST_DEC,                source.dec)
    row + CsvItem(COL_NAME_M2_EST_RA_RESIDUAL,        raDecResidual.x)
    row + CsvItem(COL_NAME_M2_EST_DEC_RESIDUAL,       raDecResidual.y)
    //----------------------------------------
    row + CsvItem(COL_NAME_M2_JPL_RA,                 spiceSourceInfo.ra)
    row + CsvItem(COL_NAME_M2_JPL_DEC,                spiceSourceInfo.dec)
    row + CsvItem(COL_NAME_M2_JPL_LIGHT_TIME,         spiceSourceInfo.lightTime)
    row + CsvItem(COL_NAME_M2_JPL_PHASE_ANGLE,        spiceSourceInfo.phaseAngle)
    row + CsvItem(COL_NAME_M2_JPL_JD_MID_CORRECTED,   spiceSourceInfo.julianDateMidPointCorrected)
    row + CsvItem(COL_NAME_M2_JPL_HELIOCENTRE_RANGE,  spiceSourceInfo.heliocentricRange)
    row + CsvItem(COL_NAME_M2_JPL_OBSERVER_RANGE,     spiceSourceInfo.observerRange)
    row + CsvItem(COL_NAME_M2_JPL_EST_AZIMUT,         spiceSourceInfo.observedPos.azimuth)
    row + CsvItem(COL_NAME_M2_JPL_EST_ZENITH,         spiceSourceInfo.observedPos.zenith)
    row + CsvItem(COL_NAME_M2_JPL_EST_HOUR_ANGLE,     spiceSourceInfo.observedPos.hourAngle)
    row + CsvItem(COL_NAME_M2_JPL_EST_ALTITUDE,       spiceSourceInfo.observedPos.altitude)
    row + CsvItem(COL_NAME_M2_JPL_EST_AIRMASS,        spiceSourceInfo.observedPos.airMass)
    row + CsvItem(COL_NAME_M2_JPL_EST_EXTINTION,      spiceSourceInfo.observedPos.extinction)
    row + CsvItem(COL_NAME_M2_JPL_EST_PAR_ANGLE,      spiceSourceInfo.observedPos.parallacticAngle)
    row + CsvItem(COL_NAME_M2_JPL_ORBIT_VERSION,      smallBodyDB.getVersion(spkID.toLong))
    //----------------------------------------

    //finnaly, build the object
    MPO_DB_Item(row)
  }
  //---------------------------------------------------------------------------
  def apply(row: CsvRow): MPO_DB_Item =
    MPO_DB_Item(
      _id = row.getString(COL_NAME_ID)
      //----------------------------------------
      , m2_id = row.getInt(COL_NAME_M2_ID)
      , focus_id = row.getString(COL_NAME_FOCUS_OBJECT_ID)
      //----------------------------------------
      , observing_time                         = row.getString(COL_NAME_OBSERVING_TIME)
      , julian_date                            = row.getDouble(COL_NAME_JULIAN_DATE)
      , julian_date_mid_point                  = row.getDouble(COL_NAME_JULIAN_DATE_MID_POINT)
      //----------------------------------------
      , v_r                                    = row.getDouble(COL_NAME_V_R)
      //----------------------------------------
      , m2_est_ra                              = row.getDouble(COL_NAME_M2_EST_RA)
      , m2_est_dec                             = row.getDouble(COL_NAME_M2_EST_DEC)
      , m2_est_ra_residual                     = row.getString(COL_NAME_M2_EST_RA_RESIDUAL)  //more precison than using a double
      , m2_est_dec_residual                    = row.getString(COL_NAME_M2_EST_DEC_RESIDUAL) //more precison than using a double
      //----------------------------------------
      , m2_jpl_ra_orbit                        = row.getDouble(COL_NAME_M2_JPL_RA)
      , m2_jpl_dec_orbit                       = row.getDouble(COL_NAME_M2_JPL_DEC)
      , m2_jpl_light_time                      = row.getDouble(COL_NAME_M2_JPL_LIGHT_TIME)
      , m2_jpl_phase_angle                     = row.getDouble(COL_NAME_M2_JPL_PHASE_ANGLE)
      , m2_jpl_julian_date_mid_point_corrected = row.getDouble(COL_NAME_M2_JPL_JD_MID_CORRECTED)
      , m2_jpl_heliocentric_range              = row.getDouble(COL_NAME_M2_JPL_HELIOCENTRE_RANGE)
      , m2_observer_range                      = row.getDouble(COL_NAME_M2_JPL_OBSERVER_RANGE)
      , m2_est_azimut                          = row.getDouble(COL_NAME_M2_JPL_EST_AZIMUT)
      , m2_est_zenith                          = row.getDouble(COL_NAME_M2_JPL_EST_ZENITH)
      , m2_est_hour_angle                      = row.getDouble(COL_NAME_M2_JPL_EST_HOUR_ANGLE)
      , m2_est_altitude                        = row.getDouble(COL_NAME_M2_JPL_EST_ALTITUDE)
      , m2_est_air_mass                        = row.getDouble(COL_NAME_M2_JPL_EST_AIRMASS)
      , m2_est_extinction                      = row.getDouble(COL_NAME_M2_JPL_EST_EXTINTION)
      , m2_est_parallactic_angle               = row.getDouble(COL_NAME_M2_JPL_EST_PAR_ANGLE)
      , m2_jpl_orbit_version                   = row.getString(COL_NAME_M2_JPL_ORBIT_VERSION)
      //----------------------------------------
    )
  //---------------------------------------------------------------------------
  private def getSpiceSourceInfo(ra: Double
                                 , dec: Double
                                 , observerSpkID: String
                                 , targetSpkID: String
                                 , telescope: String
                                 , timeStamp: String //no mid point, just the observing time. It is calculated inside this function
                                 , exposureTime: Double
                                 , calculateTimeMidPoint: Boolean) = {
    //calculate info from local JPL
    val time = LocalDateTime.parse(timeStamp, DateTimeFormatter.ISO_DATE_TIME)
    val timeStampMidPoint =
       if (calculateTimeMidPoint) time.plusNanos(Time.secondsToNanos(exposureTime / 2d)) //append the half of the exposure time
       else time

    val (jplRa, jplDec, jplLtMin, jplPhaseAngle) =
      Spice.getEphemerisAndPhaseAngle(targetSpkID
                                      , Telescope.getSpkId(telescope)
                                      , timeStampMidPoint.format(Spice.ephemerisTimeFormatterWithMillis))
    val jplJD_Corrected = Time.toJulian(timeStampMidPoint.minusNanos(Time.minuteToNanos(jplLtMin)))

    //heliocentric and observer range
    val heliocentric_range = getHeliocentricRange(targetSpkID, observerSpkID, timeStampMidPoint)
    val observerRange = (jplLtMin * 60 * LIGHT_SPEED_KM_S) / ASTRONOMICAL_UNIT_KM

    //azimut, airMass, parallactic angle
    val latLonAlt = Telescope.getLatLonAlt(telescope)
    val observedPos = getObservedApparentPosition(
        timeStampMidPoint
      , latLonAlt.head
      , latLonAlt(1)
      , latLonAlt.last
      , ra
      , dec)

    SpiceSourceInfo(
      timeStampMidPoint
      , jplRa
      , jplDec
      , jplLtMin
      , jplPhaseAngle
      , jplJD_Corrected
      , heliocentric_range
      , observerRange
      , observedPos)
  }
  //---------------------------------------------------------------------------
  private def getSpkPath(source: SourceDB_Item
                        , spkVersion: Option[String] = None) = {
    val spkID = source.focus_id.split("_")(1)
    val mpo = ImageFocusMPO.build(spkID).get
    mpo.getSpkFilePath(spkVersion)
  }
  //---------------------------------------------------------------------------
  private final val headerNameToDB_ColName = {
    classOf[MPO_DB_Item].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val headerName = f.getName()
      val dbColName = headerRow.getColName(headerName).getOrElse("None")
      f.setAccessible(false)
      (headerName, dbColName)
    }.toMap
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.common.image.photometry.absolute.photometricModel.mpo.MPO_DB_Item._
case class MPO_DB_Item(_id: String //image name
                       , m2_id: Int
                       , focus_id: String
                       , observing_time: String
                       , julian_date: Double
                       , julian_date_mid_point: Double
                       , v_r: Double
                       , m2_est_ra: Double //decimal degree
                       , m2_est_dec: Double //decimal degree
                       , m2_est_ra_residual: String //decimal degree
                       , m2_est_dec_residual: String //decimal degree
                       , m2_jpl_ra_orbit:  Double //decimal degree
                       , m2_jpl_dec_orbit: Double //decimal degree
                       , m2_jpl_light_time: Double //min
                       , m2_jpl_phase_angle: Double //degrees
                       , m2_jpl_julian_date_mid_point_corrected: Double
                       , m2_jpl_heliocentric_range: Double //au
                       , m2_observer_range: Double //au
                       , m2_est_azimut: Double //degrees
                       , m2_est_zenith: Double //degrees
                       , m2_est_hour_angle: Double
                       , m2_est_altitude: Double //m
                       , m2_est_air_mass: Double
                       , m2_est_extinction: Double
                       , m2_est_parallactic_angle: Double //deg
                       , m2_jpl_orbit_version: String
                       , var magnitude_estimation_seq: List[MagnitudeEstimation] = List.empty
                     ) extends ImageFocusDB_Item {
  //---------------------------------------------------------------------------
  def toRow() = CsvRow(toMap)

  //---------------------------------------------------------------------------
  def toMap() = {
    this.getClass.getDeclaredFields.map { f =>
      f.setAccessible(true)
      val headerName = f.getName()
      val r = (headerNameToDB_ColName(headerName), f.get(this).toString)
      f.setAccessible(false)
      r
    }.toMap
  }
  //---------------------------------------------------------------------------
  def getEstMag(estMagID: String): Option[Double] = {
    if (magnitude_estimation_seq == null) None
    else {
      val r = magnitude_estimation_seq.filter(_.getID == estMagID)
      if (r.isEmpty) None
      else Some(r.head.est_magnitude)
    }
  }
  //---------------------------------------------------------------------------
  def getBeta(esMagID:String) = {
    if (magnitude_estimation_seq == null || magnitude_estimation_seq.isEmpty) None
    else {
      val r = magnitude_estimation_seq.filter(_.getID == esMagID)
      if (r.isEmpty) None
      else Some(r.head.asInstanceOf[MPO_EstMagDB_Item].beta)
    }
  }
  //---------------------------------------------------------------------------
}
//---------------------------------------------------------------------------
//=============================================================================
//End of file MPO_DB_Item.scala
//=============================================================================
