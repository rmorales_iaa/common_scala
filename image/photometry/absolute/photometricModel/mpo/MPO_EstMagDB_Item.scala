/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  18/Jan/2023
 * Time:  21h:00m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.absolute.photometricModel.mpo
//=============================================================================
import com.common.coordinate.conversion.Conversion
import com.common.image.catalog.SourceDB_Item
import com.common.image.mpo.asteroidChecker.MinorPlanetCenter
import com.common.image.photometry.absolute.photometricModel.PhotometricModel
import com.common.image.photometry.magnitudeEstimation.MagnitudeEstimation
import com.common.image.telescope.Telescope
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.time.Time
import org.bson.codecs.configuration.CodecRegistries._
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object MPO_EstMagDB_Item extends MyLogger {
  //---------------------------------------------------------------------------
  val codecRegistry = fromRegistries(fromProviders(classOf[MPO_EstMagDB_Item]
                                                 , classOf[MagnitudeEstimation]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  final val PHASE_ANGLE = "phase_angle"
  //---------------------------------------------------------------------------
  val headerCsv = Seq(
      PhotometricModel.COL_NAME_IMAGE_NAME
    , s"${SourceDB_Item.COL_NAME_M2_ID}"
    , s"${SourceDB_Item.COL_NAME_FOCUS_ID}"
    , s"${SourceDB_Item.COL_NAME_GAIA_ID}"
    , "image_filter"
    , MagnitudeEstimation.COL_NAME_CATALOG
    , MagnitudeEstimation.COL_NAME_CATALOG_FILTER
    , "V_R"
    , "flux_per_second"
    , "snr"
    , "julian_date"
    , "estimated_magnitude"
    , "estimated_magnitude_error"
    , "est_reduced_magnitude"
    , "absolute_est_mag"
    , PHASE_ANGLE
    , MagnitudeEstimation.COL_NAME_BETA
    , "mpc_query"
  )
  //--------------------------------------------------------------------------
  def getReducedMagnitude(estimatedMagnitude: Double
                          , observerRange: Double
                          , heliocentricRange: Double): Double =
    estimatedMagnitude - 5 * Math.log10(observerRange * heliocentricRange)
  //--------------------------------------------------------------------------
  def getAbsoluteMagnitude(estReducedMag: Double
                           , phaseAngle: Double
                           , beta: Double) =
    estReducedMag - (beta * phaseAngle)
  //---------------------------------------------------------------------------
  def getCsvHeader(sep: String) = {
    classOf[MPO_EstMagDB_Item].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val headerName = f.getName()
      val dbColName = {
        val posOpenParenthesis = headerName.indexOf("(")
        if (posOpenParenthesis == -1) headerName else headerName.take(posOpenParenthesis)
      }
      f.setAccessible(false)
      dbColName
    }.mkString(sep)
  }
  //--------------------------------------------------------------------------
  def buildMap(seq: Seq[MPO_EstMagDB_Item]) = {
    val catalogSet = (seq map (_.catalog)).toSet
    val filterSet = (seq map (_.catalog_filter)).toSet
    (for(catalog<- catalogSet) yield {
      val filterMap = (for(filter<-filterSet) yield
        (filter, seq.filter( _.catalog_filter == filter))).toMap
      (catalog,filterMap)
    }).toMap
  }
  //---------------------------------------------------------------------------
  def createCsv(csvFileName: String, sep: String): BufferedWriter = {
    val append = MyFile.fileExist(csvFileName)
    val bw = new BufferedWriter(new FileWriter(new File(csvFileName), append))
    if (append) return bw
    bw.write(headerCsv.mkString(sep) + "\n")
    bw
  }
  //--------------------------------------------------------------------------
}
//=============================================================================
case class MPO_EstMagDB_Item(catalog: String
                             , catalog_filter: String
                             , julian_date: Double
                             , var est_magnitude: Double
                             , est_magnitude_error: Double
                             , est_reduced_magnitude: Double
                             , est_absolute_magnitude: Double
                             , phase_angle: Double
                             , beta: Double) extends MagnitudeEstimation {
  //---------------------------------------------------------------------------
  def saveAsCsvLine(imageName: String
                    , imageFilter: String
                    , julianDate: Double
                    , source: SourceDB_Item
                    , catalogName: String
                    , catalogFilter: String
                    , vr: Double
                    , bwSeq: Seq[BufferedWriter]
                    , mpcID: String
                    , telescope: String
                    , sep: String = "\t") = {

    val mpcQuery = MinorPlanetCenter.createQueryKnownObject(
      mpcID = mpcID
      , observingDate = Time.fromJulian(julian_date).toString
      , ra = Conversion.DD_to_HMS_WithDivider(source.getRaDec.x)
      , dec = Conversion.DD_to_DMS_WithDivider(source.getRaDec.y, threeDecimalPlaces = false)
      , observatoryCode = Telescope.getOfficialCode(telescope)
    )

    bwSeq.foreach { bw =>
      bw.write(imageName + sep)
      bw.write(source.m2_id + sep)
      bw.write(source.focus_id + sep)
      bw.write(source.gaia_id + sep)
      bw.write(imageFilter + sep)
      bw.write(catalogName + sep)
      bw.write(catalogFilter + sep)
      bw.write(vr + sep)
      bw.write(source.flux_per_second + sep)
      bw.write(source.snr + sep)
      bw.write(julianDate + sep)
      bw.write(est_magnitude + sep)
      bw.write(est_magnitude_error + sep)
      bw.write(est_reduced_magnitude + sep)
      bw.write(est_absolute_magnitude + sep)
      bw.write(phase_angle + sep)
      bw.write(beta + sep)
      bw.write(mpcQuery + "\n")
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MPO_EstMagDB_Item.scala
//=============================================================================
