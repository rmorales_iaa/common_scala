/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  11/Jan/2023
 * Time:  13h:12m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.absolute.photometricModel.mpo
//=============================================================================
import com.common.csv.CsvWrite
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.image.catalog.ImageCatalog.{ImageByFilterMap, ImagePhotometricMap, ImageSourceFocusMap}
import com.common.image.catalog.SourceDB_Item
import com.common.image.photometry.absolute.lightCurve.LightCurve
import com.common.image.photometry.absolute.photometricModel.mpo.MPO_DB_Item._
import com.common.image.photometry.absolute.photometricModel.{ImageFocusDB, ImageFocusDB_Item}
import com.common.image.photometry.magnitudeEstimation.MagnitudeEstimation
import com.common.logger.MyLogger
import com.common.math.regression.Regression
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
import org.mongodb.scala.model.Projections.{excludeId, fields, include}

import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object MPO_CatalogDB extends MyLogger {
  //---------------------------------------------------------------------------
  private final val DATABASE_NAME   = "mpo_catalog"
  //---------------------------------------------------------------------------
  def normalizeColNameForDatabase(s: String) = {
    val posOpenParenthesis = s.indexOf("(")
    if (posOpenParenthesis == -1) s else s.take(posOpenParenthesis)
  }
  //---------------------------------------------------------------------------
  def getSourcePairSeq(seq:Array[(SourceDB_Item, ImageFocusDB_Item)])  =
    seq.map(t => (t._1, t._2.asInstanceOf[MPO_DB_Item])).toArray
  //---------------------------------------------------------------------------
  //(beta,reducedMagnitudeMap)
  def calculateBetaMap(sourceFocusMap: ImageSourceFocusMap
                       , catalogPhotometricModelMap: ImagePhotometricMap) = {
    val estReducedMagnitudeMap = scala.collection.mutable.Map[String,scala.collection.mutable.Map[String,Double]]()
    val phaseAngleEstReducedMagMap = scala.collection.mutable.Map[String,ArrayBuffer[Seq[Double]]]()

    val (catalogSeq,catalogFilterSeq) = LightCurve.getPhotometricDatabaseAndFilterSeq(catalogPhotometricModelMap)

    catalogSeq.foreach { catalog =>
      catalogFilterSeq.foreach { catalogFilter =>
        val estMagID = MagnitudeEstimation.getEstMagID(catalog, catalogFilter)
        sourceFocusMap.foreach { case (_, pairSeq) =>
          pairSeq.foreach { case (_, focusSource) =>
            val mpo = focusSource.asInstanceOf[MPO_DB_Item]
            val mag = mpo.getEstMag(estMagID).getOrElse(Double.NaN)
            if (!mag.isNaN) {
              val reducedMag = MPO_EstMagDB_Item.getReducedMagnitude(mag, mpo.m2_observer_range, mpo.m2_jpl_heliocentric_range)
              if (!reducedMag.isNaN ) {
                if (!estReducedMagnitudeMap.contains(focusSource._id))
                  estReducedMagnitudeMap(focusSource._id) = scala.collection.mutable.Map[String,Double]()
                estReducedMagnitudeMap(focusSource._id)(estMagID) = reducedMag

                if (!phaseAngleEstReducedMagMap.contains(estMagID))
                  phaseAngleEstReducedMagMap(estMagID) = ArrayBuffer[Seq[Double]]()
                phaseAngleEstReducedMagMap(estMagID) += Seq(mpo.m2_jpl_phase_angle,reducedMag)
              }
            }
          }
        }
      }
    }

    //calculate the beta map
    val betaMap = phaseAngleEstReducedMagMap.map { case (estMagID,phaseAngleReducedMagnitudeSeq) =>
      val beta = Regression.linear(phaseAngleReducedMagnitudeSeq.toArray)._1 //get only the slope with no sigma clipping
      (estMagID,beta)
    }.toMap

    (betaMap,estReducedMagnitudeMap)
  }
  //-------------------------------------------------------------------------
  def addToDatabase(objectName: String
                    , sourceFocusMap: ImageSourceFocusMap
                    , imageByFilterMap: ImageByFilterMap): Unit = {
    //add to database
    val db = MPO_CatalogDB(objectName)
    db.dropCollection()
    info(s"Adding to mpo database the object '$objectName'")

    for ((_, imageNameSeq) <- imageByFilterMap) yield {
      val nameSeq = {
        imageNameSeq.flatMap { s =>
          if (sourceFocusMap.contains(s)) Some(sourceFocusMap(s))
          else {
            error(s"Can not find the source focus at the image:'$s'")
            None
          }
        }
      }.flatten

      val mpoSeq = getSourcePairSeq(nameSeq.toArray).map(_._2).toSeq
      if (mpoSeq.size > 0)
        db.add(mpoSeq)
    }
    db.close()
  }
  //---------------------------------------------------------------------------
  def saveJplOrbitResiduals(oDir: String
                            , sourceFocusMap: ImageSourceFocusMap
                            , imageByFilterMap: ImageByFilterMap) = {
    //save the csv with orbit information
    val csvFile = CsvWrite(oDir + s"/${LightCurve.JPL_ORBIT_RESIDUALS_SUMMARY_CSV_FILE_NAME}", MPO_DB_Item.headerRow)
    for ((_, imageNameSeq) <- imageByFilterMap) yield {
      val nameSeq = (imageNameSeq flatMap {name=>
        if (sourceFocusMap.contains(name)) Some(sourceFocusMap(name))
        else {
          error(s"Can not find the source focus for image:'$name'")
          None
        }}).flatten

      val mpoSeq = getSourcePairSeq(nameSeq.toArray).map(_._2)
      csvFile.append(mpoSeq map (_.toRow()))
    }
    csvFile.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.common.image.photometry.absolute.photometricModel.mpo.MPO_CatalogDB._
case class MPO_CatalogDB(collectionName: String) extends ImageFocusDB {
  //-------------------------------------------------------------------------
  val databaseName = DATABASE_NAME
  //-------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) =
    initWithCodeRegister(MPO_DB_Item.codecRegistry)
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  val collWithRegistry: MongoCollection[MPO_DB_Item] = database.getCollection(collectionName)
  connected = true
  //-------------------------------------------------------------------------
  def add(mpoSeq: Seq[MPO_DB_Item]): Unit =
    if (existCollection(collectionName))
      mpoSeq.foreach { mpo =>
        if (exist(mpo._id)) collWithRegistry.replaceOne(equal(COL_NAME_ID, mpo._id), mpo).headResult
        else collWithRegistry.insertOne(mpo).headResult
      }
    else
      collWithRegistry.insertMany(mpoSeq).headResult
  //-------------------------------------------------------------------------
  def getJuliandDateCorrected(imageName: String): Double = {
    val docSec = collection
      .find(equal(MPO_DB_Item.COL_NAME_ID, imageName))
      .projection(
        fields(include(COL_NAME_M2_JPL_JD_MID_CORRECTED)
          , excludeId()))
      .results()
      docSec.head(COL_NAME_M2_JPL_JD_MID_CORRECTED).asDouble().getValue
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file MPO_CompletedDB.scala
//=============================================================================