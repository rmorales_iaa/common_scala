/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Apr/2022
 * Time:  14h:24m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.absolute
//=============================================================================
import org.apache.commons.math3.analysis.polynomials.PolynomialFunction
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object PolynomialFit {
  //---------------------------------------------------------------------------
  def apply(s:String, filterName: String) : Option[PolynomialFit] = {
    if (s != "None"){
      val coeffSeq = s.split(" ").map(t=>t.split("=")(1).toDouble).reverse
      val degree = coeffSeq.head.toInt
      val r2 = coeffSeq.drop(1).head
      val stdDevResiduals = coeffSeq.drop(2).head
      val poly = new PolynomialFunction(coeffSeq.drop(3))
      Some(PolynomialFit(poly, stdDevResiduals, r2, degree, filterName))
    }
    else None
  }
  //---------------------------------------------------------------------------
  def coefficientsNameSeq(degree: Int,sep:String): String =
    coefficientsNameSeq(degree).mkString(sep)
  //---------------------------------------------------------------------------
  def coefficientsNameSeq(degree: Int): Seq[String] =
    ((for (x <- 0 to degree) yield s"B$x")
      ++ Seq("std_dev_residuals", "r2"))
  //---------------------------------------------------------------------------
}
//=============================================================================
//r2 = coefficient of determination
case class PolynomialFit(poly: PolynomialFunction
                         , stdDevResiduals: Double
                         , r2: Double
                         , degree: Int
                         , filterName: String)  {
  //---------------------------------------------------------------------------
  def asCsvLine(): Array[String] = {
    val _coefSeq = poly.getCoefficients
    val spareCoeffCount = (degree+1) - _coefSeq.size
    val coefSeq = if(spareCoeffCount > 0) _coefSeq ++ (for(_<-0 until spareCoeffCount) yield 0d)
                  else _coefSeq
    (coefSeq map {c => s"${f"$c%.12f"}"}) ++ Array(s"${f"$stdDevResiduals%.6f"}", s"${f"$r2%.6f"}")
  }
  //---------------------------------------------------------------------------
  override def toString()  =
    coefficientsAsString + s" stdDevResiduals=${f"$stdDevResiduals%.6f"}" + s" r2=${f"$r2%.6f"}" + s" degree=$degree"
  //---------------------------------------------------------------------------
  def coefficientsAsString()  = {
    val coefSeq = poly.getCoefficients.reverse
    val curveFittingDegree = coefSeq.length -1
    val coefNameSeq = (for (x <- 1 to curveFittingDegree) yield s"B${curveFittingDegree - x + 1}=") :+ "B0="
    ((coefNameSeq zip coefSeq) map {t =>
      val formattedValue = s"${f"${t._2}%.6f"}"
      t._1 + formattedValue}).mkString(" ")
  }
  //---------------------------------------------------------------------------
  def save(fileName: String) = {
    val bw = new BufferedWriter(new FileWriter(new File(fileName)))
    bw.write(s"CurveFitting (${filterName}) {\n")
    bw.write(s"\tdegree = ${poly.degree()}\n")
    bw.write(s"\tcoefficientSeq =  ${coefficientsAsString}\n")
    bw.write(s"\tstandardDeviationOfResiduals =  $stdDevResiduals\n")
    bw.write(s"\tcoefficientOfDetermination(r2) = $r2\n")
    bw.write("}\n")
    bw.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file PolynomialFit.scala
//=============================================================================
