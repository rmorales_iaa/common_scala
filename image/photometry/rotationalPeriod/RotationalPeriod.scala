/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  09/May/2022
 * Time:  10h:18m
 * Description: None
 */
//=============================================================================
package com.common.image.photometry.differential.rotationalPeriod
//=============================================================================
import com.common.configuration.MyConf
import com.common.geometry.point.Point2D_Double
import com.common.image.focusType.ImageFocusTrait
import com.common.image.photometry.differential.{DP_Image, GnuPlotScript}
import com.common.logger.MyLogger
import com.common.math.fourier.{Fourier, FourierFit}
import com.common.stat.{SimpleStatDescriptive, StatDescriptive}
import com.common.timeSeries.LombScargle.LombScargle
import com.common.util.path.Path
import com.common.image.photometry.magnitudeEstimation.MagnitudeEstimation
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object RotationalPeriod extends MyLogger {
  //---------------------------------------------------------------------------
  private val sep = "\t"
  //---------------------------------------------------------------------------
  val ROTATIONAL_PERIOD_DIR = "/rotational_period/"
  val ROTATIONAL_PERIOD_PARTIAL_DIR = s"$ROTATIONAL_PERIOD_DIR/partial/"
  //---------------------------------------------------------------------------
  private final val ROTATIONAL_PERIOD_SIGMA_CLIPPING_APPLY       =  MyConf.c.getInt("RotationalPeriod.applySigmaClipping") == 1
  private final val ROTATIONAL_PERIOD_SIGMA_CLIPPING_SIGMA_SCALE =  MyConf.c.getDouble("RotationalPeriod.sigmaScale")
  //---------------------------------------------------------------------------
  val CSV_COLUMN_NAME = Seq(
    "image_name"
    , "julian_date"
    , "flux_per_second"
    , "estimated_absolute_magnitude"
    , "rotation_period_days"
    , "observing_night"
    , "harmonic_fit_magnitude"
    , "telescope"
    , "image_filter"
    , MagnitudeEstimation.COL_NAME_CATALOG
    , MagnitudeEstimation.COL_NAME_CATALOG_FILTER
  )
  //---------------------------------------------------------------------------
  val CSV_ESTIMATED_MAGNITUDE_FILENAME         = "estimated_magnitude.csv"
  private val CSV_LOMB_SCARGLE_FILENAME        = "lomb_scargle_periodogram.csv"
  private val CSV_TIME_DIFF_MAGNITUDE_FILENAME = "time_diff_magnitude_corrected.csv"
  //---------------------------------------------------------------------------
  private val GNU_PLOT_DIFFERENTIAL_FILENAME            = CSV_ESTIMATED_MAGNITUDE_FILENAME.replace(".csv", ".gnuplot")
  private val GNU_PLOT_LOMB_SCARGLE_FILENAME            = CSV_LOMB_SCARGLE_FILENAME.replace(".csv", ".gnuplot")
  private val GNU_PLOT_TIME_DIFF_MAGNITUDE_FILENAME     = CSV_TIME_DIFF_MAGNITUDE_FILENAME.replace(".csv", ".gnuplot")
  //---------------------------------------------------------------------------
  private def generatePlotTimeDiffMagnitude(focusName: String
                                            , oDir: String
                                            , imageSeq: Array[DP_Image]
                                            , isAbsoluteEstMag: Boolean) = {

    val outputFilename = oDir + CSV_TIME_DIFF_MAGNITUDE_FILENAME
    val bw = new BufferedWriter(new FileWriter(new File(outputFilename)))
    bw.write(s"julian_date${sep}normalized_est_mag${sep}image" + "\n")
    imageSeq foreach { img =>
      if (isAbsoluteEstMag)
        bw.write(s"${img.julian_date}$sep${img.focus.absolute_est_mag}$sep${img._id}\n")
      else
        bw.write(s"${img.julian_date}$sep${img.focus.normalized_est_mag}$sep${img._id}\n")
    }
    bw.close()

    val gnuplotFileName = oDir + GNU_PLOT_TIME_DIFF_MAGNITUDE_FILENAME
    GnuPlotScript.timeDiffMagnitude(focusName
      , gnuplotFileName
      , Path.getCurrentPath() + oDir + CSV_TIME_DIFF_MAGNITUDE_FILENAME)
  }
  //---------------------------------------------------------------------------
  private def generatePlotPeriodogram(focusName: String
                                      , oDir: String
                                      , freqDaysSeq: Array[Double]
                                      , powerSeq: Array[Double]): Unit = {

    val pairOrdered = ((freqDaysSeq zip powerSeq) map { case (f, p) => (24 * (1 / f), p) }).sortWith(_._1 < _._1) //sort by frew
    val bw = new BufferedWriter(new FileWriter(new File(oDir + CSV_LOMB_SCARGLE_FILENAME)))
    bw.write(s"frequency(hours)${sep}spectral_power" + "\n")
    pairOrdered foreach { case (freqHour, power) => bw.write(s"$freqHour$sep$power\n") }
    bw.close()

    val xlim = Point2D_Double(pairOrdered.head._1 - 0.1, pairOrdered.last._1 - 0.1)
    val ylim = Point2D_Double(0d, powerSeq.sorted.last + 0.1)

    GnuPlotScript.freqPower(focusName
      , xlim
      , ylim
      , oDir + GNU_PLOT_LOMB_SCARGLE_FILENAME
      , Path.getCurrentPath() + oDir + CSV_LOMB_SCARGLE_FILENAME)
  }

  //---------------------------------------------------------------------------
  private def generateCSV(imageSeq: Array[DP_Image]
                          , estMagSeq: Array[Double]
                          , bw: BufferedWriter
                          , rotPeriod: RotationalPeriod
                          , estMagID: String) = {

    val fourier = rotPeriod.fourier
    val (catalog, catalogFilter) = MagnitudeEstimation.getCatalogAndFilter(estMagID)
    val harmonicFitSeq = imageSeq map (img => fourier.eval(img.focusRotationalPeriodInDays(estMagID)))
    val sortedLine = imageSeq.zipWithIndex.map { case (img, i) =>
      (img.focusRotationalPeriodInDays(estMagID)
        , s"${img._id}$sep" +
          s"${img.julian_date}$sep" +
          s"${MagnitudeEstimation.magnitudeToFlux(estMagSeq(i))}$sep" +
          s"${estMagSeq(i)}$sep" +
          s"${img.focusRotationalPeriodInDays(estMagID)}$sep" +
          s"${img.observing_night}$sep" +
          harmonicFitSeq(i) + sep +
          s"${img.telescope}$sep" +
          s"${img.imageFilter}$sep" +
          s"$catalog$sep" +
          s"$catalogFilter" +
          "\n")
    }.sortWith(_._1 < _._1)
    sortedLine.foreach { t=> bw.write( t._2 )}
  }
  //---------------------------------------------------------------------------
  private def generateCsvAndPlot(focusName: String
                                 , oDir: String
                                 , imageSeq: Array[DP_Image]
                                 , estMagSeq: Array[Double]
                                 , observingNightSet: Array[String]
                                 , rotPeriod: RotationalPeriod
                                 , commonSourceCount: Int
                                 , estMagID: String
                                 , yAxisTitle: String
                                 , alternativeCsvHeader: Option[Seq[String]] = None): Unit = {

    val outputFilename = s"$oDir$CSV_ESTIMATED_MAGNITUDE_FILENAME"
    val bw = new BufferedWriter(new FileWriter(new File(outputFilename)))

    //generate csv
    bw.write(alternativeCsvHeader.getOrElse(CSV_COLUMN_NAME).mkString(sep) + "\n")
    generateCSV(imageSeq, estMagSeq, bw, rotPeriod, estMagID)
    bw.close()

    //plot
    GnuPlotScript.rotationalPeriod(
      focusName
      , imageSeq.length
      , commonSourceCount
      , Some(rotPeriod)
      , observingNightSet
      , Path.getCurrentPath() + oDir + CSV_ESTIMATED_MAGNITUDE_FILENAME
      , oDir + GNU_PLOT_DIFFERENTIAL_FILENAME
      , yAxisTitle)

  }
  //---------------------------------------------------------------------------
  private def saveResults(focusName: String
                          , oDir: String
                          , imageSeq: Array[DP_Image]
                          , estMagSeq: Array[Double]
                          , commonSourceSeqCount: Int
                          , rotPeriod: RotationalPeriod
                          , estMagID: String
                          , isAbsoluteEstMag: Boolean
                          , alternativeCsvHeader: Option[Seq[String]] = None) = {

    val (catalog,filter) = MagnitudeEstimation.getCatalogAndFilter(estMagID)
    val yAxisTitle =
      if (isAbsoluteEstMag) s"Abso. mag. filter $catalog $filter"
      else s"Diff. mag. filter $catalog $filter"
    val observingNightSet = imageSeq.map { _.observing_night }.toSet
    generateCsvAndPlot(
      focusName
      , oDir
      , imageSeq
      , estMagSeq
      , observingNightSet.toArray
      , rotPeriod
      , commonSourceSeqCount
      , estMagID
      , yAxisTitle
      , alternativeCsvHeader)

    generatePlotPeriodogram(focusName, oDir, rotPeriod.freqSeqDays, rotPeriod.spectralPowerSeq)
    generatePlotTimeDiffMagnitude(focusName, oDir, imageSeq, isAbsoluteEstMag)
  }
  //---------------------------------------------------------------------------
  //(stats,harmonic amplitude,residualSeq)
  private def getHarmonicResidualStats(xySeq: Array[Point2D_Double], fourier: Fourier) = {
    var minHarmonicValue = Double.MaxValue
    var maxHarmonicValue = Double.MinValue
    val residualSeq = xySeq map { p =>
      val harmonicValue = fourier.eval(p.x)
      if (harmonicValue < minHarmonicValue) minHarmonicValue = harmonicValue
      if (harmonicValue > maxHarmonicValue) maxHarmonicValue = harmonicValue
      p.y - harmonicValue
    }
    (StatDescriptive.getWithDouble(residualSeq), maxHarmonicValue - minHarmonicValue, residualSeq)
  }
  //---------------------------------------------------------------------------
  private def getLombScarglePeriodogram(timeSeq: Array[Double]
                                        , magnitudeSeq: Array[Double]
                                        , minPeriodHours: Double //single peak
                                        , maxPeriodHours: Double //single peak
                                        , frequencySamplesPerPeak: Int) = {
    val r = LombScargle.calculate(
      timeSeq
      , magnitudeSeq
      , freq_samples_per_peak = frequencySamplesPerPeak
      , freq_minimum_frequency = Some(24d / maxPeriodHours) //in days. min bestFreqInDays correspond to the max period
      , freq_maximum_frequency = Some(24d / minPeriodHours) //in days. max bestFreqInDays correspond to the min period
    )

    if (r.isEmpty) None
    else Some((r.get._1, r.get._2))
  }
  //---------------------------------------------------------------------------
  private def calculateRotationalPeriodInDays(imageSeq: Array[DP_Image]
                                             , bestFreqInDays: Double
                                             , peakCount: Int
                                             , referenceTime: Double
                                             , estMagID: String) =
    imageSeq.map { img =>
      val elapsedJulianDate = img.julian_date - referenceTime
      img.focusRotationalPeriodInDays(estMagID) = (elapsedJulianDate * (bestFreqInDays / peakCount)) % 1
    }
  //---------------------------------------------------------------------------
  def fixFocusOffsetMag(imageSeq: Array[DP_Image], offset: Double) =
    imageSeq foreach { img =>
      img.focus.normalized_est_mag += offset
      img.focus.flux_per_second += MagnitudeEstimation.magnitudeToFlux(img.focus.normalized_est_mag)
    }
  //---------------------------------------------------------------------------
  private def residualMagnitude(mainRotPeriod: RotationalPeriod
                                , imageFocus:ImageFocusTrait
                                , imageSeq: Array[DP_Image]
                                , timeSeq: Array[Double]
                                , peakCount: Int
                                , minPeriodHours: Double //single peak
                                , maxPeriodHours: Double //single peak
                                , fourierSeriesFitDegree: Int
                                , frequencySamplesPerPeak: Int
                                , oDir: String
                                , bestIndex: Int = 0
                                , estMagID: String
                                , isAbsoluteEstMag: Boolean
                                , _saveResults: Boolean = true
                                , alternativeCsvHeader: Option[Seq[String]] = None
                                , verbose: Boolean = false
                                ): Unit = {

    val referenceJulianDate = timeSeq.head
    val estMagSeq = mainRotPeriod.residualFitEstMagSeq
    val rotPeriodResiduals = calculate(
      timeSeq
      , estMagSeq
      , peakCount
      , minPeriodHours
      , maxPeriodHours
      , frequencySamplesPerPeak
      , fourierSeriesFitDegree
      , bestIndex = bestIndex).getOrElse(return)

    if (verbose) {
      warning(s"Best: ${bestIndex + 1} residual rotation period searching in range:[$minPeriodHours,$maxPeriodHours] hours with ${imageSeq.size} images with reference date: $referenceJulianDate")
      warning(s"\tBest (freq_cycle_days,power)        : (${f"${rotPeriodResiduals.bestFreqInDays}%.8f"},${f"${rotPeriodResiduals.bestSpectralPower}%.8f"})")
      warning(s"\tPeak count: 1 rotational period     : ${f"${rotPeriodResiduals.rotationalPeriodInHours}%.8f"} (hours)")
      if (peakCount > 1) warning(s"\tPeak count: $peakCount rotational period     : ${f"${rotPeriodResiduals.rotationalPeriodInHours * peakCount}%.8f"} (hours)")
    }

    //save the results
    if (_saveResults)
      saveResults(
       imageFocus.composedName + "_residuals"
        , Path.resetDirectory(oDir + "/residuals")
        , imageSeq
        , estMagSeq
        , commonSourceSeqCount = 0
        , rotPeriodResiduals
        , estMagID
        , isAbsoluteEstMag
        , alternativeCsvHeader)
  }
  //---------------------------------------------------------------------------
  private def calculate(timeSeq: Array[Double]
                        , estMagSeq: Array[Double]
                        , peakCount: Int
                        , minPeriodHours: Double //single peak
                        , maxPeriodHours: Double //single peak
                        , frequencySamplesPerPeak: Int
                        , fourierSeriesFitDegree: Int
                        , bestIndex: Int = 0
                        , calculatedFreqPowerSeq: Option[(Array[Double], Array[Double])] = None
                        , verbose: Boolean = false): Option[RotationalPeriod] = {

    val referenceJulianDate = timeSeq.sorted.head

    //Lomb-Scargle periodogram
    val (freqDaysSeq, spectralPowerSeq) = calculatedFreqPowerSeq.getOrElse(
      getLombScarglePeriodogram(timeSeq, estMagSeq, minPeriodHours, maxPeriodHours, frequencySamplesPerPeak).getOrElse(return None)
    )

    if (freqDaysSeq.isEmpty) return None

    val freqSpectralPowerSeq = ((freqDaysSeq zip spectralPowerSeq) map
      {p => Point2D_Double(p._1, p._2)})
      .sortWith(_.y > _.y) //sort by spectral power

    val bestMax = Math.min(bestIndex, freqSpectralPowerSeq.length - 1)
    val bestFreqInDays = freqSpectralPowerSeq(bestMax).x
    val bestSpectralPower = freqSpectralPowerSeq(bestMax).y

    val xySeq = (timeSeq zip estMagSeq).map { case (time, magnitude) =>
      val elapsedJulianDate = time - referenceJulianDate
      val rotationalPeriodIndays = (elapsedJulianDate * (bestFreqInDays / peakCount)) % 1
      Point2D_Double(rotationalPeriodIndays, magnitude)
    }
    val bestFourier = FourierFit(fourierSeriesFitDegree, xySeq map (_.x), xySeq map (_.y)).fit()
    if (bestFourier.isEmpty) return None

    val harmonicDiffStats = getHarmonicResidualStats(xySeq, bestFourier.get)
    val rotPeriod = RotationalPeriod(
        referenceJulianDate
      , bestFreqInDays
      , bestSpectralPower
      , minPeriodHours
      , maxPeriodHours
      , peakCount
      , bestFourier.get
      , harmonicDiffStats._1
      , harmonicDiffStats._2
      , harmonicDiffStats._3
      , freqDaysSeq
      , spectralPowerSeq)
    if (verbose) {
      warning(s"Best rotational period searching in range:[$minPeriodHours,$maxPeriodHours] hours with ${timeSeq.size} images")
      warning(s"\tBest (freq_cycle_days,power)        : (${f"${rotPeriod.bestFreqInDays}%.8f"},${f"${rotPeriod.bestSpectralPower}%.8f"})")
      warning(s"\tPeak count: 1 rotational period     : ${f"${rotPeriod.rotationalPeriodInHours}%.8f"} (hours)")
      if (peakCount > 1) warning(s"\tPeak count: $peakCount rotational period     : ${f"${rotPeriod.rotationalPeriodInHours * peakCount}%.8f"} (hours)")
    }
    Some(rotPeriod)
  }
  //---------------------------------------------------------------------------
  def calculateWithResidual(imageFocus:ImageFocusTrait
                            , oDir: String
                            , _imageSeq: Array[DP_Image] //it must be sorted by julian date
                            , _timeSeq : Array[Double]
                            , _estMagSeq: Array[Double] //it must be sorted by julian date
                            , minPeriodHours: Double
                            , maxPeriodHours: Double
                            , frequencySamplesPerPeak: Int
                            , peakCount: Int
                            , fourierSeriesFitDegree: Int
                            , best: Int
                            , forceRotPeriodHours: Option[Double] = None
                            , estMagID: String
                            , isAbsoluteEstMag: Boolean
                            , saveResults: Boolean = true
                            , alternativeCsvHeader: Option[Seq[String]] = None
                            , applySigmaClipping: Boolean = ROTATIONAL_PERIOD_SIGMA_CLIPPING_APPLY
                            , verbose: Boolean = false): Option[RotationalPeriod] = {

    val (imageSeq,timeSeq,estMagSeq) =
    if (applySigmaClipping) {
      info(s"Applying sigma clipping: $ROTATIONAL_PERIOD_SIGMA_CLIPPING_SIGMA_SCALE on estimate magnitude count: '${_estMagSeq.length}'")
      val seq = _estMagSeq.zipWithIndex.map {case (e,i) => (e,(_imageSeq(i),_timeSeq(i)))}.filter(!_._1.isNaN)

      val r = StatDescriptive.sigmaClippingDoubleWithPair(seq
                                                          ,sigmaScale = ROTATIONAL_PERIOD_SIGMA_CLIPPING_SIGMA_SCALE)
      val sortedR = r.sortWith(_._2._2  < _._2._2) //sorted by time
      (sortedR map (_._2._1)
      ,sortedR map (_._2._2)
      ,sortedR map (_._1))
    } else (_imageSeq,_timeSeq,_estMagSeq)

    //check size
    if (imageSeq.size < 2){
      error(s"Can not calculate the Lomb-Scargle periodogram for: '${estMagSeq.size}' points")
      return None
    }

    //process the point sequence to list the best harmonic
    val referenceTime = timeSeq.head

    //get the rotational period fit
    if (verbose) info(s"Calculating the Lomb-Scargle periodogram for: '${estMagSeq.size}' points")

    info(s"Calculating Lomb-Scargle periodogram")
    //Lomb-Scargle periodogram
    val r = if (forceRotPeriodHours.isDefined) {
      warning(s"Forcing to: ${ forceRotPeriodHours.get}h the rotational period")
      (Array(24d / forceRotPeriodHours.get), Array(0d))
    }
    else
      LombScargle.calculate(
        timeSeq
        , estMagSeq
        , freq_samples_per_peak = frequencySamplesPerPeak
        , freq_minimum_frequency = Some(24d / maxPeriodHours) //in days. min bestFreqInDays correspond to the max period
        , freq_maximum_frequency = Some(24d / minPeriodHours) //in days. max bestFreqInDays correspond to the min period
      ).getOrElse(return None)

    //sort results
    val freqSeq = r._1
    val spectralPowerSeq = r._2

    //explore the best results according to spectral power
    var bestRotPeriod: Option[RotationalPeriod] = None
    for (i <- 0 until best) {
      val rp = RotationalPeriod.calculate(
        timeSeq
        , estMagSeq
        , peakCount
        , minPeriodHours
        , maxPeriodHours
        , frequencySamplesPerPeak
        , fourierSeriesFitDegree
        , bestIndex = i
        , calculatedFreqPowerSeq = Some((freqSeq, spectralPowerSeq))
      ).getOrElse(return None)

      if (bestRotPeriod.isEmpty) bestRotPeriod = Some(rp)

      //update the rotational period with the best frequency found
      RotationalPeriod.calculateRotationalPeriodInDays(imageSeq, rp.bestFreqInDays, peakCount, referenceTime, estMagID)

      if (verbose) {
        warning(s"Best: ${i + 1}/$best rotation period searching in range: [$minPeriodHours,$maxPeriodHours] hours with ${estMagSeq.size} images with reference date: $referenceTime")
        warning(s"\tBest (freq_cycle_days,power)        : (${f"${rp.bestFreqInDays}%.4f"},${f"${rp.bestSpectralPower}%.4f"})")
        warning(s"\tPeak count: 1 rotational period     : ${f"${rp.rotationalPeriodInHours}%.4f"} (hours)")
        if (peakCount > 1) warning(s"\tPeak count: $peakCount rotational period     : ${f"${rp.rotationalPeriodInHours * peakCount}%.8f"} (hours)")
      }

      //save the results
      val o = oDir + "best_period_" + i + "/"
      Path.ensureDirectoryExist(o)
      if (saveResults) {
        RotationalPeriod.saveResults(
          imageFocus.composedName
          , o
          , imageSeq
          , estMagSeq
          , commonSourceSeqCount = 0
          , rp
          , estMagID
          , isAbsoluteEstMag
          , alternativeCsvHeader)
      }

      //residual rotation period
      RotationalPeriod.residualMagnitude(
          rp
        , imageFocus
        , imageSeq
        , timeSeq
        , peakCount
        , minPeriodHours
        , maxPeriodHours
        , fourierSeriesFitDegree
        , frequencySamplesPerPeak
        , o
        , bestIndex = i
        , estMagID
        , isAbsoluteEstMag
        , saveResults
        , alternativeCsvHeader
        , verbose)

      info(s"Generated result at: '$o'")
    }
    bestRotPeriod

  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class RotationalPeriod(julianDateMidPointReference: Double
                            , bestFreqInDays: Double //u.k.u. rotation period in days
                            , bestSpectralPower: Double
                            , minPeriodHours: Double
                            , maxPeriodHours: Double
                            , peakCount: Int
                            , fourier: Fourier
                            , residualFitStats: SimpleStatDescriptive
                            , harmonicAmplitude: Double //between the range of frequencies used
                            , residualFitEstMagSeq: Array[Double]
                            , freqSeqDays: Array[Double]
                            , spectralPowerSeq: Array[Double]) {
  //-------------------------------------------------------------------------
  val rotationalPeriodInHours = 24 * (1 / bestFreqInDays)
  val residualStdDev = residualFitStats.stdDev
  //-------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file rotationalPeriod.scala
//=============================================================================
