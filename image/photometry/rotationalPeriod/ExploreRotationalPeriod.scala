/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

//=============================================================================
package com.common.image.photometry.differential.rotationalPeriod
//=============================================================================
import com.common.image.focusType.ImageFocusTrait
import com.common.logger.MyLogger
import com.common.util.time.Time
import com.common.image.photometry.differential.DP_Image
//=============================================================================
import java.time.LocalDate
//=============================================================================
//=============================================================================
object ExploreRotationalPeriod extends MyLogger {
  //---------------------------------------------------------------------------
  val EXPLORE_OUTPUT_DIR  = "/explore_rot_period/"
  //---------------------------------------------------------------------------
  private def applyingImageFilters(imageSeq: Array[DP_Image]
                                   , telescopeFilterSeq: Option[Array[String]]
                                   , filterSeq: Option[Array[String]]
                                   , timeRangeSeq: Option[Array[String]]) = {

    var r = imageSeq
    if (telescopeFilterSeq.isDefined) {
      val set =  telescopeFilterSeq.get.toSet
      r = r.filter(img=> set.contains(img.telescope))
    }
    if (filterSeq.isDefined) {
      val set =  filterSeq.get.toSet
      r = r.filter(img=> set.contains(img.imageFilter))
    }

    if (timeRangeSeq.isDefined) {
      val startDate = Time.toJulian(LocalDate.parse(timeRangeSeq.get(0)))
      val endDate = Time.toJulian(LocalDate.parse(timeRangeSeq.get(1)))
      r = r.filter(img => img.julian_date >= startDate &&
                   img.julian_date <= endDate)
    }
    r
  }
  //---------------------------------------------------------------------------
  def run(imageFocus: ImageFocusTrait
          , csvFileName: String
          , minPeriodHours: Double
          , maxPeriodHours: Double
          , frequencySamplesPerPeak: Int
          , peakCount: Int
          , fourierSeriesFitDegree: Int
          , best: Int
          , oDir: String
          , forceRotPeriodHours: Option[Double] = None
          , telescopeFilterSeq: Option[Array[String]] = None
          , filterSeq: Option[Array[String]] = None
          , timeRangeSeq: Option[Array[String]] = None): Unit = {

    //load the image seq
    val imageSeq = applyingImageFilters(
        DP_Image.loadSeqFromCsv(imageFocus.getID,csvFileName)
      , telescopeFilterSeq
      , filterSeq
      , timeRangeSeq).sortWith(_.julian_date < _.julian_date)

    if (imageSeq.isEmpty)  {
      error("No images to process")
      return
    }

    //build the point sequence to list the best harmonic
    RotationalPeriod.calculateWithResidual(
      imageFocus
      , oDir
      , imageSeq
      , _timeSeq = imageSeq.map(_.julian_date)
      , _estMagSeq = imageSeq.map(_.focus.normalized_est_mag)
      , minPeriodHours
      , maxPeriodHours
      , frequencySamplesPerPeak
      , peakCount
      , fourierSeriesFitDegree
      , best
      , forceRotPeriodHours
      , estMagID = "None"
      , isAbsoluteEstMag = csvFileName.contains("absolute"))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ExploreRotationalPeriod.scala
//=============================================================================
