/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  11/Apr/2022
 * Time:  21h:31m
 * Description: None
 */
//=============================================================================
package com.common.image.dataTypeConversion
//=============================================================================
import BuildInfo.BuildInfo
import com.common.configuration.MyConf
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
import com.common.fits.simpleFits.SimpleFits._
import com.common.image.myImage.MyImage
import com.common.hardware.cpu.CPU
import com.common.dataType.conversion.DataTypeConversion
import com.common.dataType.pixelDataType.PixelDataType
import com.common.util.time.Time
//=============================================================================
import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
object ImageDataTypeConversion {
  //---------------------------------------------------------------------------
  private val fitsExtension = MyConf.c.getStringSeq("Common.fitsFileExtension")
  private val coreCount: Int = CPU.getCoreCount()
  //---------------------------------------------------------------------------
  private def processImage(imageName: String, newImageName: String, newBitPix: Int) = {
    val img = MyImage(imageName)
    val data = img.data
    val bitPix = img.bitPix
    val invScale = 1d / img.bScale
    val invZero = - img.bZero
    val newData = PixelDataType.scalePixelSeq(data, img.bScale, img.bZero)
    val byteSeq = bitPix match {
      case -32 =>
        val d = newData.map(v => v.asInstanceOf[Float])
        newBitPix match {
          case 16 =>
            val maxRepresentableData = Short.MaxValue
            val newPixSeq = d.map{ p=>
              val newP = (p * invScale) + invZero //apply inverse transformation
              val finalP = {
                  if (newP > maxRepresentableData) maxRepresentableData
                  else
                    if (newP < 0.toShort) 0.toShort
                    else Math.round(newP).toShort
              }
              finalP
            }
            Some(DataTypeConversion.shortSeqToByteSeq(newPixSeq))
          case _  => warning(s"Image data coversion from $bitPix to $newBitPix is not implemented yet")
                     None
       }
      case _ => warning(s"Image data coversion from $bitPix is not implemented yet")
      None
    }
    if (byteSeq.isDefined) {
      val fits = img.getSimpleFits()
      fits.updateRecord(KEY_BIT_PIX,newBitPix.toString)
      img.saveAsFits(newImageName
        , ArrayBuffer(fits.getKeyValueMap.toSeq:_*).toArray
        , rawData = byteSeq.get.toArray
        , applyTransformation = false
        , _bitPix = newBitPix
        , buildInfo = Array((KEY_M2_VERSION, s"'${BuildInfo.version.trim}'")
                           , (KEY_M2_RUN_DATE, s"'${Time.getISO_DateTimeStamp}'")))
    }
  }
  //---------------------------------------------------------------------------
  def processDir(inputDir: String, outputDir: String, newBitPix: Int) = {
    //-------------------------------------------------------------------------
    class MyParallelImageSeq(seq: Array[String]) extends ParallelTask[String](
      seq
      , coreCount
      , isItemProcessingThreadSafe = true) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(imageName: String) =
        Try {
          processImage(imageName, outputDir + Path.getOnlyFilename(imageName), newBitPix)
        }
        match {
          case Success(_) =>
          case Failure(e) =>
            error(e.getMessage + s" Error catalogging the image '$imageName' continuing with the next image")
            error(e.getStackTrace.mkString("\n"))
        }
      //-----------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    if(!Path.directoryExist(inputDir)) error(s"Input directory '$inputDir' does not exist")
    else {
      Path.ensureDirectoryExist(outputDir)
      //calculate all subdirs
      val currentDir = Path.ensureEndWithFileSeparator(inputDir) +  "."
      val subDirToProcess = Path.getSubDirectoryList(inputDir).map(_.getAbsolutePath)
      val fitsSeq = ArrayBuffer[String]()
      fitsSeq ++= Path.getSortedFileList(currentDir,fitsExtension) map (_.getAbsolutePath)
      new MyParallelImageSeq(fitsSeq.toArray)
      fitsSeq.clear()
      subDirToProcess.foreach { subDirName =>
        info(s"Parsing directory: '$subDirName'")
        fitsSeq ++= Path.getSortedFileListRecursive(subDirName, fitsExtension).map (_.getAbsolutePath)
        new MyParallelImageSeq(fitsSeq.toArray)
        fitsSeq.clear()
      }
      fitsSeq.clear()
    }
  }
}
//=============================================================================
//End of file ImageDataTypeConversion.scala
//=============================================================================
