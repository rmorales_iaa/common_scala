//=============================================================================
//File: ConversionFunction.scala
//=============================================================================
/** It implements conversion distribution.function management for packets items
  *  @author  Rafael Morales Muñoz
  *  @mail    rmorales.iaa.es
  *  @version 1.0
  *  @date    9 Nov 2016
  *  @history None
  */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.packet.conversion
//=============================================================================
// System import sectionName
//=============================================================================

import scala.collection.mutable.Map
import scala.xml.Node
import scala.language.postfixOps

//=============================================================================
// User import sectionName
//=============================================================================

//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================
//-------------------------------------------------------------------------
object ConversionFunction extends MyLogger {
  //-------------------------------------------------------------------------
  // Constant
  //-------------------------------------------------------------------------
  private val ConversionFunctionLinearName="linear"
  //-------------------------------------------------------------------------
  // Variable declaration
  //-------------------------------------------------------------------------
  private var storage = Map[String, ConversionFunction]()  //store all defined packet item

  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
  def parse(xml: VisualXML) : Boolean = {

    //fist level, no recursive packet item
    val itemList = (xml.root \\ "Conversion_function_list" \ "distribution/function")
    itemList.foreach { x => parseFunction(x) }

    true
  }
  //-------------------------------------------------------------------------
  private def parseFunction(node: Node) {

    val name = (node \ "@name") text
    var typeName = (node \ "@type_name") text
    val a = getDouble((node \ "@u") text)
    val b = getDouble((node \ "@b") text)
    var inputUnit = (node \ "@input_unit") text
    var outputUnit = (node \ "@output_unit") text

    //default values
    if (typeName.isEmpty()) typeName="linear"
    if (inputUnit.isEmpty()) inputUnit="none"
    if (outputUnit.isEmpty()) outputUnit="none"

    //validate
    if(checkValueConsistency(name,typeName,inputUnit,outputUnit)){
      val function= apply(name,typeName,a,b,Unit.get(inputUnit),Unit.get(outputUnit))
      storage += (name->function)
      info(s"Added conversion distribution.function '$name'")
    }
  }

  //-------------------------------------------------------------------------
  private def checkValueConsistency(name: String
                                    ,  typeName: String
                                    ,  inputUnit: String
                                    ,  outputUnit: String) : Boolean = {

    if(get(name) != null)
      error(s"ConversionFunction '$name' exist in the storage, can not append again")

    if(typeName != ConversionFunctionLinearName)
      error(s"ConversionFunction '$name'. Unknown type '$typeName'")

    if(!Unit.contain(inputUnit))
      error(s"ConversionFunction '$name'. Unknown input unit '$inputUnit'")

    if(!Unit.contain(outputUnit))
      error(s"ConversionFunction '$name'. Unknown output unit '$outputUnit'")

    true
  }

  //-------------------------------------------------------------------------
  def get(s:String) : ConversionFunction = {
    if (!storage.contains(s)) return null
    storage(s)
  }

  //-------------------------------------------------------------------------
  def contain(s:String) : Boolean = storage.contains(s)

  //-------------------------------------------------------------------------
  def getDouble(s: String) : Double = {

    if (s.trim().isEmpty()) return 0  //some values are optional
    var input = s

    var l: Double =0
    try { l = input.toDouble}
    catch {
      case ex: Exception => exception(ex,s"Error creating a double with value '$s")
        return 0
    }
    l
  }
  //-------------------------------------------------------------------------
  def apply(name: String
            ,  typeName: String
            ,  a: Double
            ,  b: Double
            ,  inputUnit: Unit
            ,  outputUnit: Unit): ConversionFunction = {

    name match {

      case "ms_to_date_format_1" =>  new MsToDate()
      case _                     =>  new ConversionFunction(name,typeName,a,b,inputUnit,outputUnit)
    }
  }

  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
class ConversionFunction(name: String
                         ,  typeName: String
                         ,  a: Double
                         ,  b: Double
                         ,  inputUnit: Unit
                         ,  outputUnit: Unit)  extends Cloneable with MyLogger {

  //-------------------------------------------------------------------------
  // Variable declaration
  //-------------------------------------------------------------------------

  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------

  //-------------------------------------------------------------------------
  override def clone : ConversionFunction = super.clone.asInstanceOf[ConversionFunction]

  //-------------------------------------------------------------------------
  def getName = name

  //-------------------------------------------------------------------------
  def getOutputFunction = outputUnit

  //-------------------------------------------------------------------------
  def translate(v: Long): String = {

    (if ((a==1.0) && (b==0)) v
    else (a * v.toDouble + b)).toString
  }
  //-------------------------------------------------------------------------
  def log(prefix: String) = {
    info(s"$prefix-Conversion distribution.function:")
    info(s"$prefix-|---Name       : $name")
    info(s"$prefix-|---Type       : $name")
    info(s"$prefix-|---Equation   : y = $a x + $b")
    info(s"$prefix-|---Input  unit: " + inputUnit.shortName)
    info(s"$prefix-|---Output unit: " + outputUnit.shortName)
  }
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //End of case class 'ConversionFunction'

//=============================================================================
// End of 'ConversionFunction.scala' file
//=============================================================================
