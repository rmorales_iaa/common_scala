//=============================================================================
//File: packetItem.scala
//=============================================================================
/** It implements a basic components of a packet
  *  @author  Rafael Morales Muñoz
  *  @mail    rmorales.iaa.es
  *  @version 1.0
  *  @date    9 Nov 2016
  *  @history None
  */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.packet

//=============================================================================
// System import sectionName
//=============================================================================
import scala.collection.mutable.{ListBuffer, Map}
import scala.language.postfixOps
import scala.xml.Node
import java.math.BigInteger

//=============================================================================
// User import sectionName
//=============================================================================

//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================

object PacketItem extends MyLogger {

  //-------------------------------------------------------------------------
  // Constants
  //-------------------------------------------------------------------------
  private val DefaultConversionFunction="linear_no_conversion"

  //-------------------------------------------------------------------------
  // Variable declaration
  //-------------------------------------------------------------------------
  private var storage = Map[Long, PacketItem]()  //store all defined packet item

  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
  def parse(xml: VisualXML) : Boolean = {

    //fist level, no recursive packet item
    val itemList = (xml.root \\ "Packet_item_list" \ "packet_item" )
    itemList.foreach { x => parseItem(x) }

    true
  }

  //-------------------------------------------------------------------------
  private def parseItem(node: Node, parent:PacketItem = null) {

    val id = ((node \ "@m2_id") text).toLong
    val name = (node \ "@name") text
    val label =(node \ "@name") text
    val version = getLong((node \ "@version") text).toInt
    val description = (node \ "description" \ "@val") text
    var min = getLong((node \ "limit" \ "@min") text)
    var max = getLong((node \ "limit" \ "@max") text)
    var defaultText = (node \ "limit" \ "@default") text
    val bitSize = getLong((node \ "storage" \ "@bit_size") text).toInt
    val isArray = getBoolean((node \ "storage" \ "@is_array") text)
    val showValueAsHex = getBoolean((node  \ "format string" \ "@show_as_hex") text)
    var conversionfunction = (node \ "format string" \ "@conversion_function") text
    val validValue = (node \\ "valid_value" \ "value" \\ "@val" )
    val validValueLabel = (node \\  "valid_value" \ "value" \\ "@name" )

    var default : Long=0
    var validValueMap = Map[Long, String]()
    if (validValue.size >0){
      min=(scala.math.pow(2,bitSize)-1).toLong
      max=0
      validValue.zipWithIndex.foreach {case(v,i) => {
        val value=getLong(v.text)
        if (value < min) min = value
        if (value > max) max = value
        validValueMap += (value->validValueLabel(i).text)}}
    }

    //default value
    if (defaultText.isEmpty) default = min
    else default =  getLong(defaultText)
    if(conversionfunction.isEmpty) conversionfunction=DefaultConversionFunction

    if (checkConsistency(id,name,version,min,max,default,validValueMap,bitSize,conversionfunction)){
      val item= new PacketItem(id,name,label,version,description,min,max,default,validValueMap
        ,bitSize,isArray,showValueAsHex
        ,ConversionFunction.get(conversionfunction))
      storage += (id->item)

      //child list
      val child_list = (node \ "child_list" \ "packet_item")
      child_list.foreach { child => parseItem(child,item) }
      if(parent!=null) parent += item

      info(s"Added packet item '$name' with m2_id '$id'")
    }
  }

  //-------------------------------------------------------------------------
  def getLong(s: String) : Long = {

    if (s.trim().isEmpty()) return 0  //some values are optional
    var input = s
    if (s.toLowerCase().startsWith("0x")) {
      input=s.replaceFirst("0x","")
      return new BigInteger(input, 16).longValue()
    }

    var l: Long =0
    try { l = input.toLong }
    catch {
      case ex: Exception => exception(ex,s"Error creating a long with value '$s")
        return 0
    }
    l
  }

  //-------------------------------------------------------------------------
  def getBoolean(s: String) : Boolean = {

    if (s.trim().isEmpty()) return false  //some values are optional
    var l = s.toLowerCase()
    if (l == "0" || l == "false") return false
    return true
  }

  //-------------------------------------------------------------------------
  private def checkConsistency(id: Long
                               ,  name: String
                               ,  version: Int
                               ,  min: Long
                               ,  max: Long
                               ,  default: Long
                               ,  validValue: Map[Long, String]
                               ,  bitSize: Int
                               ,  conversionfunction: String) : Boolean = {

    if (storage.contains(id.toLong)) return true

    if (name.isEmpty())
      error(s"The packet item with '$id' has an empty name")

    if (version < 0)
      error(s"PacketItem '$name' with m2_id '$id'. Version '$version' can not be negative")

    if (!ConversionFunction.contain(conversionfunction))
      error(s"PacketItem '$name' with m2_id '$id'. Conversion distribution.function '$conversionfunction' does not exist")

    checkValueConsistency(id,name,min,max,default,validValue,bitSize)
  }

  //-------------------------------------------------------------------------
  private def checkValueConsistency(id: Long
                                    ,  name: String
                                    ,  min: Long
                                    ,  max: Long
                                    ,  default: Long
                                    ,  validValue: Map[Long, String]
                                    ,  bitSize: Int) : Boolean = {

    if ((bitSize < 1) || (bitSize > 64))
      error(s"PacketItem '$name' with m2_id '$id'. Expecting bit size between [1,64], both inclusives, but provided: '$bitSize'")

    val calculatedMaxValue=scala.math.pow(2,bitSize)-1
    val calculatedMinValue=0

    if (validValue.size > 0){
      validValue.keys.foreach( v => {

        if (v < min)
          error(s"PacketItem '$name' with m2_id '$id'. Valid value '$v' is less than min value 'min'")

        if (v > max)
          error(s"PacketItem '$name' with m2_id '$id'. Valid value '$v' is greater than max value 'max'")

      })
    }
    else {
      if (min < calculatedMinValue)
        error(s"PacketItem '$name' with m2_id '$id'. Min value '$min' is less than min calculated value '$calculatedMinValue'")

      if (max > calculatedMaxValue)
        error(s"PacketItem '$name' with m2_id '$id'. Max value '$max' is greater than max calculated value '$calculatedMaxValue'")

      if (min > max)
        error(s"PacketItem '$name' with m2_id '$id'. Min value '$min' is greater than max value '$max'")

      if (default < min)
        error(s"PacketItem '$name' with m2_id '$id'. Default value '$default' is less than min value '$min'")

      if (default > max)
        error(s"PacketItem '$name' with m2_id '$id'. Default value '$default' is greater than max value '$max'")
    }

    true
  }

  //-------------------------------------------------------------------------
  def get(i:Long) : PacketItem = {
    if (!storage.contains(i)) return null
    storage(i)
  }

  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
}

//=============================================================================
//=============================================================================

case class PacketItem (id: Long
                       , name: String
                       , label: String
                       , version: Int
                       , description: String
                       , min: Long
                       , max: Long
                       , default: Long
                       , validValue: Map[Long, String]
                       , bitSize: Int
                       , isArray: Boolean
                       , showValueAsHex: Boolean = false
                       , conversionFunction: ConversionFunction) extends Cloneable with MyLogger {

  //-------------------------------------------------------------------------
  // Variable declaration
  //-------------------------------------------------------------------------
  private var childList = ListBuffer[PacketItem]() //children
  var value: Long = default
  private var valueArray: Array[Byte] = null
  val byteSize :Int = getNumberOfByte(bitSize).toInt

  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  def +=(item:PacketItem) = childList += item

  //-------------------------------------------------------------------------
  def equal(p: PacketItem) = p.id==id

  //-------------------------------------------------------------------------
  def get = value

  //-------------------------------------------------------------------------
  def set(v:Long) : Boolean ={

    if (canSetValue(v)){
      value = v
      true
    }
    else false
  }

  //-------------------------------------------------------------------------
  def setArrayData(data:Array[Byte]) = valueArray = data
  //-------------------------------------------------------------------------
  def getArrayData() = valueArray
  //-------------------------------------------------------------------------
  def calculateBitSize : Long = {

    if (isArray) {
      if (valueArray != null) valueArray.size * 8
      else 0
    }
    else bitSize
  }

  //-------------------------------------------------------------------------
  override def clone : PacketItem = {
    var newI : PacketItem = super.clone.asInstanceOf[PacketItem]
    newI.childList = ListBuffer[PacketItem]()
    childList.map { child => newI += child.clone }
    newI
  }

  //-------------------------------------------------------------------------
  def isComposed = childList.size >0

  //-------------------------------------------------------------------------
  def canSetValue(v: Long) : Boolean =  {

    if(validValue.size > 0) validValue.contains(v)
    else
    if ((v<min) || (v>max)) false
    true
  }

  //-------------------------------------------------------------------------
  def getHex: Option[Array[Byte]] = {

    isArray match{
      case true =>  if (valueArray == null) None else Some(valueArray)
      case false => isComposed match {
        case true =>  Some(DataType.getHex(getHexComposedValue(bitSize), byteSize))
        case false => Some(DataType.getHex(value, byteSize))
      }}
  }

  //-------------------------------------------------------------------------
  private def getHexComposedValue(totalBitCount: Int
                                  , _usedBitCount: Int=0): Long = {

    var composedValue: Long =0
    var usedBitCount =_usedBitCount

    childList.foreach { child =>

      child.isComposed match {
        case true =>   composedValue += child.getHexComposedValue(totalBitCount,usedBitCount)
        case false =>  {composedValue += (child.value << (totalBitCount-usedBitCount-child.bitSize))
          usedBitCount+=child.bitSize}}}

    composedValue
  }

  //-------------------------------------------------------------------------
  def setHex(l: Array[Byte],  bytePos: Long) : Boolean = setHex(getValueFromList(l, byteSize, bytePos))

  //-------------------------------------------------------------------------
  def setHex(v: Long) : Boolean = {

    //array is managed in the packet
    isComposed match{
      case true =>  if (!setHexComposedValue(v,bitSize)) return false
      case false => if (!set(v)) return error(s"Value '$v' is not in range [$min,$max]")}

    true
  }

  //-------------------------------------------------------------------------
  private def setHexComposedValue(v: Long
                                  ,  totalBitCount: Int
                                  ,  _usedBitCount: Int=0) : Boolean = {

    var usedBitCount =_usedBitCount

    childList.foreach { child =>

      child.isComposed match {
        case true => { if(!child.setHexComposedValue(v,totalBitCount,usedBitCount)) return false
        else true}

        case false =>{ val mask = (scala.math.pow(2, child.bitSize) -1).toLong
          val newV = (v >> totalBitCount - usedBitCount - child.bitSize) //remove used bytes
          if (!child.set(newV & mask))
            return false
          usedBitCount+=child.bitSize
          true}
      }}
    true
  }

  //-------------------------------------------------------------------------
  import scala.xml.{Elem}

  def toXmlNode : Elem= {

    val node=
      <packet_item id={id.toString} name={name} label={label} version={version.toString}>
        <description val={description}/>
        <limit min={min.toString} max={max.toString} default={default.toString}/>
        {if (validValue.size > 0){
        <valid_value>
          {validValue.keys.map(key => <value val={key.toString} label={validValue(key)}/>)}
        </valid_value>
      }
        }
        <storage bit_size={bitSize.toString} is_array={isArray.toString}/>
        <format conversion_function={conversionFunction.getName} show_as_hex={showValueAsHex.toString}/>
        {if (childList.size > 0)
        <child_list>
          {childList.map { child => child.toXmlNode}}
        </child_list>}
      </packet_item>
    node
  }

  //-------------------------------------------------------------------------
  def valueXmlNode : Elem= {

    val node=
      <packet_item name={name} val={valueAsString}>
        {if (childList.size > 0)
        <child_list>
          {childList.map { child => child.valueXmlNode}}
        </child_list>}
      </packet_item>
    node
  }

  //-------------------------------------------------------------------------
  def valueAsString: String = {
    conversionFunction.translate(value).toString
  }

  //-------------------------------------------------------------------------
  def log(prefix: String, longVersion: Boolean = false)  = {
    info(s"$prefix-Item: $id")
    val seq = getHex.getOrElse(Array())
    info(s"$prefix-|-Name            : $name")
    info(s"$prefix-|-Is array        : $isArray")
    if (isArray) {
      if (seq.isEmpty)  info(s"$prefix-|-Raw hex value 0x: None")
      else              info(s"$prefix-|-Raw hex value 0x: " + getHexString(valueArray))
    }
    else {
      info(s"$prefix-|-Raw value       : $value")
      info(s"$prefix-|-Raw hex value 0x: " + getHexString(DataType.getHex(value, byteSize)))
      info(s"$prefix-|-Converted value : "  + valueAsString)
    }
    if (longVersion){
      info(s"$prefix-|-Byte Size    : " +  seq.length)
      info(s"$prefix-|-Bit size     : $bitSize")
      info(s"$prefix-|-Show as hex  : $showValueAsHex")
      info(s"$prefix-|-Label        : $label")
      info(s"$prefix-|-Version      : $version")
      info(s"$prefix-|-Description  : $description")
      info(s"$prefix-|-Min          : $min")
      info(s"$prefix-|-Max          : $max")
      info(s"$prefix-|-Default      : $default")
      info(s"$prefix-|-Valid value")
      validValue.zipWithIndex.map{ case (v,i) => info(s"$prefix-|-|--Value $i: $v")}
      conversionFunction.log(prefix+"-|")
    }
  }
  //-------------------------------------------------------------------------
}//End of case class 'PacketItem'

//=============================================================================
// End of 'packetItem.scala' file
//=============================================================================
