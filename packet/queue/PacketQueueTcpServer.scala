//=============================================================================
//File: packetQueueTcpServer.scala
//=============================================================================
/** It implements a packet queue that run as TCP server
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    24 April 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.packet.queue

//=============================================================================
// System import sectionName
//=============================================================================
import akka.actor.ActorSystem
import akka.io.Tcp._

//=============================================================================
// User import sectionName
//=============================================================================
//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================

case class PacketQueueTcpServer(templatePacketID: Long
                                , name:String
                                , port: Int
                                , maxCLientCount: Int
                                , maxBufferStorageSize: Long = 100000000L
                                , actorSystem: ActorSystem
                                , session: PacketSession) extends PacketQueueTcpCommon {
  //-------------------------------------------------------------------------
  // Variable declaration
  //-------------------------------------------------------------------------  
  
   //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------

  tcpActor = actorSystem.actorOf(Server.props(classOf[ClientServerSide],name,port,maxCLientCount,maxBufferStorageSize
    ,  userRead=Some(myRead)    
    ,  userServerConnectedClient=Some(myClientConnected)
    ,  userClosed=Some(socketClosed)))
    
    
  //-------------------------------------------------------------------------
  def myClientConnected() {
    
    info(s"PacketQueueTcpServer '$name' has connected a NEW client")
  }
    
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //End of case class 'PacketTcpServerQueue'

//=============================================================================
// End of 'packetQueueTcpServer.scala' file
//=============================================================================
