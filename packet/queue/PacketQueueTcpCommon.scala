//=============================================================================
//File: packetQueueTcpCommon.scala
//=============================================================================
/** It implements commonHenosis activities of tcp paquet queue
  *  @author  Rafael Morales Muñoz
  *  @mail    rmorales.iaa.es
  *  @version 1.0
  *  @date    24 April 2016
  *  @history None
  */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.packet.queue
//=============================================================================
// System import sectionName
//=============================================================================

//=============================================================================
// User import sectionName
//=============================================================================
//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================
object PacketQueueTcpCommon {
  //-------------------------------------------------------------------------
  var actorSystem: ActorSystem = null
  var packetSession: PacketSession = null
  //-------------------------------------------------------------------------
}

//=============================================================================

trait PacketQueueTcpCommon extends MyLogger {

  //-------------------------------------------------------------------------
  val name:String
  val port: Int
  val maxBufferStorageSize: Long
  val templatePacketID: Long
  var packeTemplate : Packet = _
  //-------------------------------------------------------------------------
  // Message queue class declaration
  //-------------------------------------------------------------------------
  class PacketWriteQueue(s:String) extends MyQueue[Packet]  {

    //-------------------------------------------------------------------------
    val name = "PacketWriteQueue"
    //-------------------------------------------------------------------------
    def processItem(p: Packet) : Unit = {
      //too much debug
      //debug("Writing to IOT: " + getHexString(p.getHex))

      tcpActor ! Write(ByteString(p.getHex))
    }
    //-------------------------------------------------------------------------
    def printQueueMessage(s:String) = info(s)
  }

  //-------------------------------------------------------------------------
  // Variable declaration
  //-------------------------------------------------------------------------
  private val writeQueue = new PacketWriteQueue(name)
  protected var tcpActor : ActorRef = null
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
  def init(): Unit ={
    packeTemplate = Packet.get(templatePacketID)
    if (packeTemplate==null) logger.fatal(s"PacketQueueTcpCommon. Can not find packet with m2_id '$packeTemplate'. Can not clone")
  }
  //-------------------------------------------------------------------------
  def myRead(data: ByteString){  //should be override by user

    //too much debug
    //debug(s"'TCP $name' byteSeq received: 0x"+ getHexString(data))
    val p = buildPacket(data)
    if (p.isDefined){
      p.get.addPath(s"Read from TCP '$name'")  //set the tag
      packetSession += p.get
    }
  }

  //-------------------------------------------------------------------------
  def += (p:Packet) = {

    p.addPath(s"Write from TCP '$name'") //set the tag
    packetSession += p                   //append to packetSession
    writeQueue += (p)
  }

  //-------------------------------------------------------------------------
  def socketClosed() {

    warning(s"TCP '$name' has closed!!!")
  }

  //-------------------------------------------------------------------------
  protected def buildPacket(data: ByteString): Option[Packet] = {
    val result=Packet.build(packeTemplate, data)
    if (result._1) Some(result._2)
    else None
  }

  //-------------------------------------------------------------------------
  def close() {

    warning(s"Closing TCP '$name' with queue item count: "+writeQueue.size)
    tcpActor ! Close
  }

  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //End of abstract class 'PacketQueueTcpCommon'

//=============================================================================
// End of 'packetQueueTcpCommon.scala' file
//=============================================================================
