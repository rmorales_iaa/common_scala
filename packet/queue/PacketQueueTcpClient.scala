//=============================================================================
//File: packetQueueTcpClient.scala
//=============================================================================
/** It implements a packet queue that run as TCP client
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    24 April 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.packet.queue
//=============================================================================
//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================
import PacketQueueTcpCommon._

trait PacketQueueTcpClient extends PacketQueueTcpCommon {

  val address: String
  val timeBetweenReconnectionsInS : Int = 5

  val maxBufferStorageSize: Long = 100000000L
  val socketKeepAlive : Boolean = true
  val socketOOBIOnline : Boolean = true
  val socketNoDelay : Boolean = false
  val socketReuseAddress : Boolean = false
  val socketReceiveBufferize : Int = (65 * 1024)
  val socketSendBufferSize : Int = (65 * 1024)
  //-------------------------------------------------------------------------
  // Variable declaration
  //-------------------------------------------------------------------------  
      
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
  override def init(): Unit = {

    super.init()

    tcpActor = actorSystem.actorOf(Client.props(name,address,port,timeBetweenReconnectionsInS,maxBufferStorageSize
      ,  userRead=Some(myRead)
      ,  userClientConnectedServer=Some(connectedWithServer)
      ,  userClosed=Some(socketClosed)))
  }
  //-------------------------------------------------------------------------
  def connectedWithServer() {
      
    info(s"PacketQueueTcpClient '$name' has connected with server")
  }
  
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //End of case class 'PacketTcpClientQueue'

//=============================================================================
// End of 'packetQueueTcpClient.scala' file
//=============================================================================
