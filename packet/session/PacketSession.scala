//=============================================================================
//File: PacketSession.scala
//=============================================================================
/** It implements a packet packetSession
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.packet.session

//=============================================================================
// System import sectionName
//=============================================================================

import scala.collection.mutable.Map

//=============================================================================
// User import sectionName
//=============================================================================

//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================

case class PacketSession(name:String, outputDirectory: String)
  extends MyQueue[Packet] with MyLogger {

  //-------------------------------------------------------------------------
  // Variable declaration
  //-------------------------------------------------------------------------
  private var stat = Map[Long, StatItemLong]()  //store all packet stat
  private val xmlFormatter = new scala.xml.PrettyPrinter(80, 4)
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
  var xmlFile = MyFile(outputDirectory)
  xmlFile.write("<Packet_list>" + LineSeparator)
  info(s"Packet packetSession '$name' initialized")
  //-------------------------------------------------------------------------
  def processItem(p:Packet){
        
    info("Packet added "+p.getQueuePath+" with a size of "+ formatSize(p.getBytSize))
    addToStat(p)
  }
 	//-------------------------------------------------------------------------	
	def printQueueMessage(s:String) = info(s)
  //-------------------------------------------------------------------------
  protected def addToStat(p:Packet){
  
    xmlFile.write(p.valueToString)    
     
    //stats
    val t=p.getType
    if (!stat.contains(t)) stat += (t->new StatItemLong(t,p.name))    
    val s=stat(t)
    s.add(p.calculateByteSize)      
    stat(t) = s
  }
  //-------------------------------------------------------------------------
  def restart() {
    
    pauseStart
    close
    stat.clear
    xmlFile = MyFile(outputDirectory)
    pauseEnd
  }
  //-------------------------------------------------------------------------
  override def close() {
    info(s"Closing packet packetSession: '$name'")
    super.close  //parent call to calculate remain packets
    writeStat
    xmlFile.write(LineSeparator + "</Packet_list>")
    xmlFile.close    
  }
  //-------------------------------------------------------------------------
  private def writeStat = xmlFile.write(LineSeparator + getStat)
  //-------------------------------------------------------------------------
  private def getStat : String = {
    
    val node=
      <Packet_list_stat>
       {stat.keys.map(key => <packet packet={key.toString} name={Packet.get(key).name}>
                                       <count val={stat(key).count.toString}/>
																			 <byteSize val={stat(key).byteSize.toString}/>
																			 <firstItemTimeStamp val={stat(key).firstItemTimeStamp}/>
																			 <lastItemTimeStamp val={stat(key).lastItemTimeStamp}/>
																			{if(!stat(key).minValueTimeStamp.isEmpty){
																			   <minValue val={stat(key).minValue.toString}/>																			
  																		   <maxValue val={stat(key).maxValue.toString}/>
																			   <minValueTimeStamp val={stat(key).minValueTimeStamp}/>
																			   <maxValueTimeStamp val={stat(key).maxValueTimeStamp}/>}}
                             </packet>)}
		  </Packet_list_stat>
    
	  xmlFormatter.format(node)    	
  }
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //End of case class 'PacketSession'

//=============================================================================
// End of 'PacketSession.scala' file
//=============================================================================
