//=============================================================================
//File: Packet.scala
//=============================================================================
/** It implements NEW control structures for com.common. See object declaration
  *  @author  Rafael Morales Muñoz
  *  @mail    rmorales.iaa.es
  *  @version 1.0
  *  @date    9 Nov 2016
  *  @history None
  */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.packet
//=============================================================================
// System import sectionName
//=============================================================================
import akka.util.ByteString
//=============================================================================
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Map
import scala.xml.{Elem, Node}
import scala.language.postfixOps
//=============================================================================
// User import sectionName
//=============================================================================

//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================
//Format of packet supported
//  1.- There is only one field (packetItem) with the size of the data array in bytes. It is set in the packet xml definition
//  2.- There is only one field (packetItem) with the type of the packet. It is set in the packet xml definition
//  3.- Fields with bit size are allowed but the must be contiguous forming a complete set of bytes (1 or more bytes)
//  4.- Arrays are allowed but only as sequence of complete bytes
//  5.- The size of an array is defined implicitly by 1.
//  6.- The m2_id of each packet must be unique for all packets defined
//  7.- The m2_id of each packetItem must be unique for all packetItems defined
//  8.- The packetItems can be composed (nested) but only in a complete byte sequence
//  9.- The packet definition will include only the top parent in case of composed packetItems
// 10.- The field with the size must appear before the array size
// 11.- The field with the size must appear after the field with the type
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
object Packet extends MyLogger {

  //-------------------------------------------------------------------------
  // Variable declaration
  //-------------------------------------------------------------------------
  private var storage = Map[Long, Packet]()  //store all defined packet item
  private val xmlFormatter = new scala.xml.PrettyPrinter(80, 4)

  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------

  def init : Boolean = {

    if (!MyConf.c.getBoolean("Common.packet.use")) return false

    //xml validation
    val xsdDir=ensureEndWithFileSeparator(MyConf.c.getString("Common.packet.xsdFilePath"))

    if (!parseUnit(xsdDir)) return false
    if (!parseConversionUnit(xsdDir)) return false
    if (!parsePacketItem(xsdDir)) return false
    if (!parsePacket(xsdDir)) return false

    true
  }

  //-------------------------------------------------------------------------
  private def parseUnit(xsdDir:String) : Boolean = {

    val fileList = getFileList(MyConf.c.getString("Common.packet.unitXmlFilePath"))

    fileList.map { file =>
      val xml=new VisualXML(file.getPath
        , xsdDir+"unit.xsd")
      if(!xml.valid) return fatal(s"Error validating the unit xml file '$file'")
      if(!Unit.parse(xml)) return fatal(s"Error parsing the unit xml file '$file'")
    }
    true
  }

  //-------------------------------------------------------------------------
  private def parseConversionUnit(xsdDir:String) : Boolean = {

    val fileList = getFileList(MyConf.c.getString("Common.packet.conversionFunctionFilePath"))

    fileList.map { file =>
      val xml=new VisualXML(file.getPath
        , xsdDir+"conversion_function.xsd")
      if(!xml.valid) return fatal(s"Error validating the conversion distribution.function xml file '$file'")
      if(!ConversionFunction.parse(xml)) return fatal(s"Error parsing the conversion distribution.function xml file '$file'")
    }
    true
  }

  //-------------------------------------------------------------------------
  private def parsePacketItem(xsdDir:String) : Boolean = {

    val fileList = getFileList(MyConf.c.getString("Common.packet.packetItemFilePath"))

    fileList.map { file =>
      val xml=new VisualXML(file.getPath, xsdDir+"packet_item.xsd")
      if(!xml.valid) return fatal(s"Error validating the packetItem xml file '$file'")
      if(!PacketItem.parse(xml)) return fatal(s"Error parsing the packetItem xml file '$file'")
    }
    true
  }

  //-------------------------------------------------------------------------
  private def parsePacket(xsdDir:String) : Boolean = {

    val fileList = getFileList(MyConf.c.getString("Common.packet.packetFilePath"))

    fileList.map { file =>
      val xml=new VisualXML(file.getPath, xsdDir+"packet.xsd")
      if(!xml.valid) return fatal(s"Error validating the packet xml file '$file'")
      if(!Packet.parse(xml)) return fatal(s"Error parsing the packet xml file '$file'")
    }
    true
  }

  //-------------------------------------------------------------------------
  def parse(xml: VisualXML) : Boolean = {

    //fist level, no recursive packet item
    val itemList = (xml.root \\ "Packet_list" \ "packet" )
    itemList.foreach { x => parseItem(x) }

    true
  }

  //-------------------------------------------------------------------------
  private def parseItem(node: Node) {

    val id = PacketItem.getLong((node \ "@m2_id") text)
    val name = (node \ "@name") text
    val childList = (node \ "packet_item")
    var errorAddingPacketItem=false

    if (checkConsistency(id,name)){

      val packet= new Packet(name,id)
      childList.foreach { child => {

        val itemID = PacketItem.getLong((child \ "@m2_id") text)
        val isType = PacketItem.getBoolean((child \ "@is_type") text)
        val isSize = PacketItem.getBoolean((child \ "@is_size") text)

        val item = PacketItem.get(itemID)
        if (item == null) {
          error(s"Packet '$name'. The packet item with '$itemID' does not exist")
          errorAddingPacketItem = true
        }
        else{
          packet+=item
          if (isType) packet.setType(item)
          if (isSize) packet.setSize(item)
        }
      }}

      if(!errorAddingPacketItem){
        storage += (id->packet)
        logger.info(s"Added packet '$name' with m2_id '$id'")
      }
    }
  }

  //-------------------------------------------------------------------------
  private def checkConsistency(id: Long
                               ,  name: String) : Boolean = {

    if (storage.contains(id.toLong))
      error(s"The packet '$name' has duplicated m2_id '$id'")

    if (name.isEmpty())
      error(s"The packet with '$id' has an empty name")

    true
  }

  //-------------------------------------------------------------------------
  def get(i:Long) : Packet = {
    if (!storage.contains(i)){
      error(s"The packet with m2_id '$i' does not exist")
      return null
    }
    storage(i)
  }

  //-------------------------------------------------------------------------
  def getClone(i:Long) : Packet = {
    val p = get(i)
    p.clone
  }

  //-------------------------------------------------------------------------
  def contain(i:Long)  = {

    if (!storage.contains(i)){
      error(s"The packet with m2_id '$i' does not exist")
      false
    }
    else true
  }

  //-------------------------------------------------------------------------
  def allPacketToXmlNode : Elem= {
    val node=
      <Packet_list>
        {storage.keys.map(pID =>
        <packet id={pID.toString} name={storage(pID).name}>
          {storage(pID).childList.map {item =>
            <packet_item id={item.id.toString} />}}
        </packet>)}
      </Packet_list>
    node
  }
  //-------------------------------------------------------------------------
  def build(t: Packet, byteSeq:ByteString) : (Boolean, Packet, ByteString) = {

    if (t == null) {
      error("Can not process a packet. Packet template is null")
      return (false, null, null)
    }

    if (byteSeq == null) {
      error("Can not process a packet. Byte sequence is null")
      return (false, null, null)
    }

    val newP = t.clone //clone template

    val firstChunkByteSize = newP.byteSizeUntilItemSize
    if (byteSeq.size < firstChunkByteSize){ //enough bytes to get the size?
      error(s"Not enough bytes to get the size of packet '${t.name}'")
      return (false,null,null)
    }

    val dataChunk = byteSeq.take(firstChunkByteSize.toInt).toByteBuffer.array
    newP.setHexUntilPacketItem(dataChunk, newP.itemSize, included=true)

    val arrayDataSize = newP.itemSize.value
    //array if it exists does not set yet, so fix the packet byte size
    val packetSize = if (arrayDataSize == 0) newP.getBytSize else newP.getBytSize + arrayDataSize

    if (byteSeq.size < packetSize){ //enough bytes to get the full packet?
      error(s"Not enough bytes to get full bytes of packet '${t.name}'")
      return (false,null,null)
    }
    if (packetSize < t.getItemSize.min){ //wrong packet size
      error(s"Wrong packet size: ${byteSeq.size} in packet '${t.name}'. Min valid value: ${t.getItemSize.min}")
      return (false,null,null)
    }

    if (packetSize > t.getItemSize.max){ //wrong packet size
      error(s"Wrong packet size: ${byteSeq.size} in packet '${t.name}'. Max valid value: ${t.getItemSize.max}")
      return (false,null,null)
    }

    val completeData = byteSeq.take(packetSize.toInt).toByteBuffer.array
    if (!newP.setHex(completeData, packetSize)){
      error(s"Error setting the byteSeq of packet '${t.name}'")
      return (false,null,null)
    }

    val newData = byteSeq.drop(packetSize.toInt) //drop used byteSeq
    (true,newP,newData)
  }

  //-------------------------------------------------------------------------
  def getPacketListId = storage.keys.toList

  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
import Packet._

case class Packet (name: String, id: Long)  extends Cloneable  {

  //-------------------------------------------------------------------------
  // Variable declaration
  //-------------------------------------------------------------------------
  private var childList = ListBuffer[PacketItem]() //PacketItem children
  private var itemType: PacketItem = null
  private var itemSize: PacketItem = null
  private var queuePath = ListBuffer[String]() //queues that processed this packet
  private var byteSizeUntilItemSize: Long = 0  //itemSize included (itemType must appear before itemSize)

  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
  def validate(p: Packet) = true //Override by the user
  //-------------------------------------------------------------------------
  def +=(item:PacketItem) = childList += item
  //-------------------------------------------------------------------------
  def equal(p: Packet) = p.id==id
  //-------------------------------------------------------------------------
  def setType(item:PacketItem) = itemType = item
  //-------------------------------------------------------------------------
  def calculateByteSize = calculateByteSizePartial(null)
  //-------------------------------------------------------------------------
  def getByteSizeUntilItemSize  =  byteSizeUntilItemSize
  //-------------------------------------------------------------------------
  def getItemType = itemType
  //-------------------------------------------------------------------------
  def getItemSize = itemSize
  //-------------------------------------------------------------------------
  def getType = itemType.get
  //-------------------------------------------------------------------------
  def getSize = itemSize.get
  //-------------------------------------------------------------------------
  def getChildList= childList
  //-------------------------------------------------------------------------
  def calculateByteSizePartial(p:PacketItem = null, included: Boolean = false): Long =  
    getNumberOfByte(bitSizePartial(p,included))
  //-------------------------------------------------------------------------
  def bitSize = bitSizePartial(null)
  //-------------------------------------------------------------------------
  def getBytSize = getNumberOfByte(bitSizePartial(null))
  //-------------------------------------------------------------------------
  def bitSizePartial(p:PacketItem = null, included: Boolean = false): Long= {

    var size: Long =0

    childList.map {child => {
      if(p == null) size += child.calculateBitSize
      else{
        if (child.equal(p)){
          if (included) size += child.calculateBitSize
          else return size
        }
        else size += child.calculateBitSize
      }
    }}
    size
  }

  //-------------------------------------------------------------------------
  override def clone : Packet =  {

    var newP = super.clone.asInstanceOf[Packet]
    newP.childList = ListBuffer[PacketItem]()
    childList.map { child => newP += child.clone }
    newP.itemType = newP.getItem(itemType)
    newP.itemSize = newP.getItem(itemSize)
    newP.queuePath = ListBuffer[String]()
    newP
  }

  //-------------------------------------------------------------------------
  def getFirstItemWithArray : PacketItem = {

    childList.map { x => if (x.isArray) return x }
    null
  }

  //-------------------------------------------------------------------------
  def contain(i:Long) : Boolean = {

    childList.map { x => if (x.id == i) return true }
    false
  }

  //-------------------------------------------------------------------------
  def setSize(item:PacketItem) = {
    itemSize = item
    byteSizeUntilItemSize = calculateByteSizePartial(itemSize, included=true)
  }
  //-------------------------------------------------------------------------
  def getHex = {
    childList.flatMap { child => child.getHex }.flatten.toArray
  }
  //-------------------------------------------------------------------------
  def setHex(data: Array[Byte], packetSize: Long) : Boolean = {

    var bytePos: Long = 0
    val arrayItem = getFirstItemWithArray

    val r = setHexUntilPacketItem(data, arrayItem, bytePos)
    if (!r._1) return false
    bytePos = r._2

    if (arrayItem != null){
      val arrayByteSize = itemSize.value
      val remainByteSize = packetSize - arrayByteSize - bytePos

      if(arrayByteSize < 0)
        return error(s"Packet '$name' with m2_id '$id'. Incorrect size setting the packet")

      if(remainByteSize < 0)
        return error(s"Packet '$name' with m2_id '$id'. Incorrect remain size setting the packet")

      if (arrayByteSize > 0) {
        arrayItem.setArrayData(data.slice(bytePos.toInt, (bytePos + arrayByteSize).toInt))
        bytePos += arrayByteSize
      }

      val pos=find(arrayItem)+1 //calculate remain packet items
      for(i <- pos to childList.size-1){
        val child = childList(i.toInt)
        if(!child.setHex(data,bytePos)) return false
        bytePos += child.byteSize
      }
    }

    true
  }

  //-------------------------------------------------------------------------
  private def setHexUntilPacketItem(l: Array[Byte]
                                    ,  p:PacketItem
                                    ,  bytePos: Long = 0
                                    ,  included: Boolean = false) : (Boolean,Long) = {

    var pos: Long = bytePos

    childList.map { child =>
      if(child == p){
        if (included){
          if(!child.setHex(l, pos)) return (false, pos)
          pos += child.byteSize
        }
        return (true,pos)
      }
      else{
        if(!child.setHex(l,pos)) return (false,pos)
        pos += child.byteSize
      }
    }

    (true,pos)
  }

  //-------------------------------------------------------------------------
  def getItem(i:Long) = childList(i.toInt)
  //-------------------------------------------------------------------------
  def getItem(i:PacketItem): PacketItem = {

    childList.map { x => if (x.equal(i)) return x}
    error(s"Packet '$name' with m2_id '$id' has not an packet item with m2_id '$i'")
    null
  }
  //-------------------------------------------------------------------------
  def find(item:PacketItem) : Long = {

    childList.zipWithIndex.foreach{ case (child,i) => if (child.id == item.id) return i}
    -1
  }
  //-------------------------------------------------------------------------
  def getHexUntilItem(p:PacketItem,  included: Boolean = false): Array[Byte] = {

    var pos = find(p)
    if(pos == -1) return null
    if(included) pos += 1
    childList.take(pos.toInt).flatMap(child => (child.getHex )).flatten.toArray
  }
  //-------------------------------------------------------------------------
  def getItemCount: Long =  childList.size
  //-------------------------------------------------------------------------
  def getLastItem: PacketItem = getItem(getItemCount -1)
  //-------------------------------------------------------------------------
  def toXmlNode : Elem= {

    val node=
      <Packet_item_list>
        {childList.map { child => child.toXmlNode}}
      </Packet_item_list>
    node
  }

  //-------------------------------------------------------------------------
  def valueXmlNode : Elem= {

    val node=
      <Packet id={id.toString} name={name}>
        <Packet_item_list>
          {childList.map { child => child.valueXmlNode}}
        </Packet_item_list>
      </Packet>
    node
  }

  //-------------------------------------------------------------------------
  def valueToString : String = xmlFormatter.format(valueXmlNode)
  //-------------------------------------------------------------------------
  def addPath(s:String)= queuePath+=s
  //-------------------------------------------------------------------------
  def getQueuePath =  queuePath.mkString(",")
  //-------------------------------------------------------------------------
  def log(longVersion: Boolean = false)  = {
    val seq = getHex
    info(s"++++++++++++++++++++++++++++++++++++++++ ")
    info(s"Packet: '$name'")
    info("|--Hex      : 0x" + getHexString(seq))
    info("|--Byte Size: " +  seq.length)
    if (queuePath.isEmpty) info("|--Path     : None")
    else info("|--Path     : " +  queuePath)
    childList.foreach{ _.log("|----", longVersion) }
    info(s"---------------------------------------- ")
  }
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //End of case class 'Packet'

//=============================================================================
// End of 'Packet.scala' file
//=============================================================================
