//=============================================================================
package com.common.compression.lz4
//=============================================================================
import scala.util.{Failure, Success, Try}
import java.io.{ByteArrayInputStream, ByteArrayOutputStream}
//=============================================================================
import com.common.compression.Compression
import net.jpountz.lz4._
//=============================================================================
//=============================================================================
case class Lz4(blockByteSize: Int = 2048) extends Compression{

  //---------------------------------------------------------------------------
  val factory: LZ4Factory = LZ4Factory.fastestInstance
  val compressor: LZ4Compressor = factory.fastCompressor
  val decompressor: LZ4FastDecompressor = factory.fastDecompressor
  //---------------------------------------------------------------------------
  def compress(seq: Array[Byte]): Option[Array[Byte]] = {

    val baos = new ByteArrayOutputStream
    Try {
      val stream  = new LZ4BlockOutputStream(baos, blockByteSize, compressor)
      stream.write(seq)
      stream.close()
      baos.close()
    }
    match {
      case Success(_) => Some(baos.toByteArray)
      case Failure(ex) => error(s"Error compressing with lz4 the input sequence. Byte size: " + seq.length + " "  + ex.toString)
        None
    }
  }
  //---------------------------------------------------------------------------
  def decompress(seq: Array[Byte], decompressByteSize: Int = 0): Option[Array[Byte]] = {

    val baos = new ByteArrayOutputStream
    Try {
      val inputStream = new ByteArrayInputStream(seq)
      val stream  = new LZ4BlockInputStream(inputStream, decompressor)
      val buffer = new Array[Byte](blockByteSize)
      var count: Int = 0
      do {
        count = stream.read(buffer)
        if (count != -1 ) baos.write(buffer, 0, count)
      } while (count != -1)

      stream.close()
      baos.close()
    }
    match {
      case Success(_) => Some(baos.toByteArray)
      case Failure(ex) => error(s"Error decompressing with lz4 the input sequence. Byte size: " + seq.length +  " "  + ex.toString)
        None
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Lz4.scala
//=============================================================================