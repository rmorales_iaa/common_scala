package com.common.compression.gzip
//=============================================================================
import java.io.{ByteArrayOutputStream}
import java.util.zip.{GZIPInputStream, GZIPOutputStream}
import scala.util.{Failure, Success, Try}
//=============================================================================
import com.common.compression.Compression
//=============================================================================
//=============================================================================
object Gzip extends Compression {
  //---------------------------------------------------------------------------
  def compress(seq: Array[Byte]): Option[Array[Byte]] = {
    val baos = new ByteArrayOutputStream
    Try {
      val gos = new GZIPOutputStream(baos)
      gos.write(seq)
      gos.close()
      baos.close()
      baos.toByteArray
    }
    match {
      case Success(_) => Some(baos.toByteArray)
      case Failure(ex) => error(s"Error compressing with gzip the input sequence. Byte size: '" + seq.length +  ex.toString)
        None
    }
  }
  //---------------------------------------------------------------------------
  def decompress(byteSeq: Array[Byte]
                 , decompressByteSize: Int=0): Option[Array[Byte]] = {
    val outputStream = new ByteArrayOutputStream()
    Try {
      val gzipInputStream = new GZIPInputStream(new java.io.ByteArrayInputStream(byteSeq))
      val buffer = new Array[Byte](2048)
      var bytesRead = gzipInputStream.read(buffer)
      while (bytesRead != -1) {
        outputStream.write(buffer, 0, bytesRead)
        bytesRead = gzipInputStream.read(buffer)
      }
      gzipInputStream.close()
      outputStream.close()
    }
    match {
      case Success(_) => Some(outputStream.toByteArray)
      case Failure(ex) => error(s"Error compressing with gzip the input sequence. Byte size: '" + byteSeq.length +  ex.toString)
        None
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
// End of 'Gzip.scala' file
//=============================================================================