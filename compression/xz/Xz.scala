package com.common.compression.xz
//=============================================================================
import java.io.{ByteArrayInputStream, ByteArrayOutputStream}
//=============================================================================
import com.common.compression.Compression
import org.tukaani.xz.{LZMA2Options, XZInputStream, XZOutputStream}
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
object Xz extends Compression{
  //---------------------------------------------------------------------------
  def compress(seq: Array[Byte]): Option[Array[Byte]] = {

    val r =  Try{
      val optionSet = new LZMA2Options
      optionSet.setPreset(9) //compressing over 32MiB
      val baos = new ByteArrayOutputStream
      val stream = new XZOutputStream(baos, optionSet)
      stream.write(seq, 0,  seq.length)
      stream.close
      baos.toByteArray
    }
    r match //call distribution.function with the parameter. It is executed inside the apply method of the Try object
    {
      case Success(v) => Some(r.get)
      case Failure(e) => {logger.error("Error compressing with xz. "+e.getMessage); None}
    }
  }
  //---------------------------------------------------------------------------
  def decompress(seq: Array[Byte]): Option[Array[Byte]] = decompress(seq, seq.length)
  //---------------------------------------------------------------------------
  def decompress(seq: Array[Byte], decompressByteSize: Int): Option[Array[Byte]] = {
    val r =  Try{
      val stream = new XZInputStream(new ByteArrayInputStream(seq))
      val buf = new Array[Byte](decompressByteSize)
      stream.read(buf, 0,  decompressByteSize)
      stream.close
      buf.take(decompressByteSize)
    }
    r match //call distribution.function with the parameter. It is executed inside the apply method of the Try object
    {
      case Success(v) => Some(r.get)
      case Failure(e) => {logger.error("Error decompressing with xz. "+e.getMessage); None}
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
// End of 'Xz.scala' file
//=============================================================================