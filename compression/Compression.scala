/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  09/Nov/2018
  * Time:  13h:03m
  * Description: None
  */
//=============================================================================
package com.common.compression
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
trait Compression  extends MyLogger {
  //---------------------------------------------------------------------------
  def compress(seq: Array[Byte]): Option[Array[Byte]]
  //---------------------------------------------------------------------------
  def decompress(seq: Array[Byte], decompressByteSize: Int = 0): Option[Array[Byte]]
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file Compression.scala
//=============================================================================
