/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  22/Nov/2020
 * Time:  11h:30m
 * Description: Adapted from: https://github.com/locationtech/jts/blob/402acb6e4d8cfb0a1de061811455564176483288/modules/core/src/main/java/org/locationtech/jts/algorithm/Orientation.java
 */
//=============================================================================
package com.common.geometry.orientation
//=============================================================================
//=============================================================================
object Orientation {
   // A value that indicates an orientation of clockwise, or a right turn.
  val CLOCKWISE: Int = -1
  val RIGHT: Int = CLOCKWISE

  //A value that indicates an orientation of counterclockwise, or a left turn.
  val COUNTERCLOCKWISE = 1
  val LEFT: Int = COUNTERCLOCKWISE

  //A value that indicates an orientation of collinear, or no turn (straight).
  val COLLINEAR = 0
  val STRAIGHT: Int = COLLINEAR
}
//=============================================================================
//End of file Orientation.scala
//=============================================================================
