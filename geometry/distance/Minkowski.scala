/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Jan/2022
 * Time:  13h:31m
 * Description: None
 */
//=============================================================================
package com.common.geometry.distance
//=============================================================================
//=============================================================================
import com.common.dataType.dataType.DataType
//=============================================================================
import scala.reflect.ClassTag
import scala.reflect.runtime.universe.{TypeTag}
//=============================================================================
case class Minkowski[T]() extends Distance[T] {
  //---------------------------------------------------------------------------
  def getDistance[Z:TypeTag:ClassTag](a: Seq[Z], b: Seq[Z]) = getDistance(a,b,Seq(1d))
  //---------------------------------------------------------------------------
  override def getDistance[Z:TypeTag:ClassTag](a: Seq[Z], b: Seq[Z], extraParameter: Seq[Double]) = {
    val aSeq = DataType.valueSeqToDoubleSeq(a)
    val bSeq = DataType.valueSeqToDoubleSeq(b)
    val p = extraParameter.head
    var sum = 0d
    (aSeq zip bSeq) foreach { case (x,y) =>
      val dif =  Math.abs(x - y)
      sum += Math.pow(dif,p)
    }
    Math.pow(sum,1/p)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Minkowski.scala
//=============================================================================
