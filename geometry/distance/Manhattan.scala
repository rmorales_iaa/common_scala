/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Jan/2022
 * Time:  13h:31m
 * Description: None
 */
//=============================================================================
package com.common.geometry.distance
//=============================================================================
//=============================================================================
import com.common.dataType.dataType.DataType
//=============================================================================
import scala.reflect.ClassTag
import scala.reflect.runtime.universe.{TypeTag}
//=============================================================================
case class Manhattan[T]() extends Distance[T] {
  //---------------------------------------------------------------------------
  def getDistance[Z:TypeTag:ClassTag](a: Seq[Z], b: Seq[Z]) = {
    val aSeq = DataType.valueSeqToDoubleSeq(a)
    val bSeq = DataType.valueSeqToDoubleSeq(b)
    ((aSeq zip bSeq) map {case (x,y) => Math.abs(x - y)}).sum
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Manhattan.scala
//=============================================================================
