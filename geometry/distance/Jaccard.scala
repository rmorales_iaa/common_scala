/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Jan/2022
 * Time:  13h:31m
 * Description: None
 */
//=============================================================================
package com.common.geometry.distance
//=============================================================================
//=============================================================================
import scala.reflect.ClassTag
import scala.reflect.runtime.universe.{TypeTag}
//=============================================================================
case class Jaccard[T]() extends Distance[T] {
  //---------------------------------------------------------------------------
  def getDistance[Z:TypeTag:ClassTag](a: Seq[Z], b: Seq[Z]) = {
    if ((a.size == 0) || (b.size == 0)) 0d
    else {
      val intersect = a.toSet.intersect(b.toSet)
      val union = a.toSet.union(b.toSet)
      intersect.size.toDouble / union.size
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Euclidean.scala
//=============================================================================
