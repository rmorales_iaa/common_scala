/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Jan/2022
 * Time:  13h:31m
 * Description: None
 */
//=============================================================================
package com.common.geometry.distance
//=============================================================================
//=============================================================================
import com.common.dataType.dataType.{DataType}
//=============================================================================
import scala.reflect.ClassTag
import scala.reflect.runtime.universe.{TypeTag}
  //=============================================================================
object Cosine {
  //---------------------------------------------------------------------------
  def getProduct(aSeq: Seq[Double], bSeq: Seq[Double]) =
    aSeq.zip(bSeq).map { case (x, y) => x * y }.sum
  //---------------------------------------------------------------------------
  def getNorm(seq: Seq[Double]) = (seq map ( v=> v * v)).sum
  //---------------------------------------------------------------------------
}
//=============================================================================
import Cosine._
case class Cosine[T]() extends Distance[T] {
  //---------------------------------------------------------------------------
  def getDistance[Z:TypeTag:ClassTag](a: Seq[Z], b: Seq[Z]) = {
    val aSeq = DataType.valueSeqToDoubleSeq(a)
    val bSeq = DataType.valueSeqToDoubleSeq(b)
    val product = aSeq.zip(bSeq).map { case (x, y) => x * y }.sum
    val norm = getNorm(aSeq) * getNorm(bSeq)
    product / norm
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Cosine.scala
//=============================================================================
