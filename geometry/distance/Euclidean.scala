/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Jan/2022
 * Time:  13h:31m
 * Description: None
 */
//=============================================================================
package com.common.geometry.distance
//=============================================================================
//=============================================================================
import com.common.dataType.dataType.DataType
//=============================================================================
import scala.reflect.ClassTag
import scala.reflect.runtime.universe.TypeTag
//=============================================================================
case class Euclidean[T]() extends Distance[T] {
  //---------------------------------------------------------------------------
  def getDistance[Z:TypeTag:ClassTag](a: Seq[Z], b: Seq[Z]) = {
    val aSeq = DataType.valueSeqToDoubleSeq(a)
    val bSeq = DataType.valueSeqToDoubleSeq(b)
    var sum = 0d
    (aSeq zip bSeq) foreach { case (x,y) =>
      val dif =  x - y
      sum += dif * dif
    }
    Math.sqrt(sum)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Euclidean.scala
//=============================================================================
