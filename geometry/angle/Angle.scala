/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  22/Nov/2020
 * Time:  11h:28m
 * Description: Adapted from : https://github.com/locationtech/jts/blob/402acb6e4d8cfb0a1de061811455564176483288/modules/core/src/main/java/org/locationtech/jts/algorithm/Angle.java#L20
 */
//=============================================================================
package com.common.geometry.angle
//=============================================================================
import com.common.geometry.orientation.Orientation
import com.common.geometry.point.{Point2D_Double}
//=============================================================================
object Angle {
  //---------------------------------------------------------------------------
  val PI_TIMES_2: Double = 2.0 * Math.PI
  val PI_OVER_2: Double = Math.PI / 2.0
  val PI_OVER_4: Double = Math.PI / 4.0
  val COUNTERCLOCKWISE: Int = Orientation.COUNTERCLOCKWISE
  val CLOCKWISE: Int = Orientation.CLOCKWISE
  val NONE: Int = Orientation.COLLINEAR
  //---------------------------------------------------------------------------
  /**
   * Returns the unoriented smallest angle between two vectors.
   * The computed angle will be in the range [0, Pi).
   *
   * @param tip1 the tip of one vector
   * @param tail the tail of each vector
   * @param tip2 the tip of the other vector
   * @return the angle between tail-tip1 and tail-tip2
   */
  def angleBetween(tip1: Point2D_Double, tail: Point2D_Double, tip2: Point2D_Double): Angle = {
    val a1 = new Angle(tail, tip1)
    val a2 = new Angle(tail, tip2)
    Angle(a1.diff(a2))
  }
  //---------------------------------------------------------------------------
  /**
   * Returns the oriented smallest angle between two vectors.
   * The computed angle will be in the range (-Pi, Pi].
   * A positive result corresponds to a counterclockwise
   * (CCW) rotation
   * from v1 to v2;
   * a negative result corresponds to a clockwise (CW) rotation;
   * a zero result corresponds to no rotation.
   *
   * @param tip1 the tip of v1
   * @param tail the tail of each vector
   * @param tip2 the tip of v2
   * @return the angle between v1 and v2, relative to v1
   */
  def angleBetweenOriented(tip1: Point2D_Double, tail: Point2D_Double, tip2: Point2D_Double): Angle = {
    val a1 = new Angle(tail, tip1)
    val a2 = new Angle(tail, tip2)
    val angDel = new Angle(a2.a - a1.a)
    // normalize, maintaining orientation
    if (angDel.a <= -Math.PI) return Angle(angDel.a + PI_TIMES_2)
    if (angDel.a > Math.PI) return Angle(angDel.a - PI_TIMES_2)
    angDel
  }
  //---------------------------------------------------------------------------
  /**
   * Computes the interior angle between two segments of a ring. The ring is
   * assumed to be oriented in a clockwise direction. The computed angle will be
   * in the range [0, 2Pi]
   * @param p0 a point of the ring
   * @param p1 the next point of the ring
   * @param p2 the next point of the ring
   * @return the interior angle based at <code>p1</code>
   */
  def interiorAngle(p0: Point2D_Double, p1: Point2D_Double, p2: Point2D_Double): Angle = {
    val anglePrev = new Angle(p1, p0)
    val angleNext = new Angle(p1, p2)
    Angle(Math.abs(angleNext.a - anglePrev.a))
  }
  //---------------------------------------------------------------------------
  def toDegrees(radians: Double): Double = (radians * 180) / Math.PI
  //---------------------------------------------------------------------------
  def toRadians(angleDegrees: Double): Double = (angleDegrees * Math.PI) / 180.0
  //---------------------------------------------------------------------------
  def getDotProd(p0: Point2D_Double, p1: Point2D_Double, p2: Point2D_Double) = {
    val dx0 = p0.x - p1.x
    val dy0 = p0.y - p1.y
    val dx1 = p2.x - p1.x
    val dy1 = p2.y - p1.y
    dx0 * dx1 + dy0 * dy1
  }
  //---------------------------------------------------------------------------
  /**
   * Tests whether the angle between p0-p1-p2 is acute.
   * An angle is acute if it is less than 90 degrees.
   * <p>
   * Note: this implementation is not precise (deterministic) for angles very close to 90 degrees.
   *
   * @param p0 an endpoint of the angle
   * @param p1 the base of the angle
   * @param p2 the other endpoint of the angle
   * @return true if the angle is acute
   */
  def isAcute(p0: Point2D_Double, p1: Point2D_Double, p2: Point2D_Double) : Boolean =
    getDotProd(p0,p1,p2) > 0
  //---------------------------------------------------------------------------
  /**
   * Tests whether the angle between p0-p1-p2 is obtuse.
   * An angle is obtuse if it is greater than 90 degrees.
   * <p>
   * Note: this implementation is not precise (deterministic) for angles very close to 90 degrees.
   *
   * @param p0 an endpoint of the angle
   * @param p1 the base of the angle
   * @param p2 the other endpoint of the angle
   * @return true if the angle is obtuse
   */
  def isObtuse(p0: Point2D_Double, p1: Point2D_Double, p2: Point2D_Double)  : Boolean =
    getDotProd(p0,p1,p2) < 0
  //---------------------------------------------------------------------------
  //An angle is right if it is equal to 90 degrees
  //Note: this implementation is not precise (deterministic) for angles very close to 90 degrees.
  def isRight(p0: Point2D_Double, p1: Point2D_Double, p2: Point2D_Double)  : Boolean =
    getDotProd(p0,p1,p2) == 0
  //---------------------------------------------------------------------------
}

//=============================================================================
import Angle._
case class Angle(a: Double) {
  //---------------------------------------------------------------------------
  /**
   * Returns the angle of the vector from p0 to p1,
   * relative to the positive X-axis.
   * The angle is normalized to be in the range [ -Pi, Pi ].
   *
   * @param p0 the initial point of the vector
   * @param p1 the terminal point of the vector
   * @return the normalized angle (in radians) that p0-p1 makes with the positive x-axis.
   */
  def this(p0: Point2D_Double, p1:Point2D_Double) = this(Math.atan2(p1.y - p0.y, p1.x - p0.x))
  //---------------------------------------------------------------------------
  /**
   * Returns the angle of the vector from (0,0) to p,
   * relative to the positive X-axis.
   * The angle is normalized to be in the range ( -Pi, Pi ].
   *
   * @param p the terminal point of the vector
   * @return the normalized angle (in radians) that p makes with the positive x-axis.
   */
  def this(p: Point2D_Double) = this(Math.atan2(p.y, p.x))
  //---------------------------------------------------------------------------
  /**
   * Computes the unoriented smallest difference between two angles.
   * The angles are assumed to be normalized to the range [-Pi, Pi].
   * The result will be in the range [0, Pi].
   *
   * @param this the angle of one vector (in [-Pi, Pi] )
   * @param ang2 the angle of the other vector (in range [-Pi, Pi] )
   * @return the angle (in radians) between the two vectors (in range [0, Pi] )
   */
  def diff(ang2: Angle): Double = {
    var delAngle = .0
    if (a < ang2.a) delAngle = ang2.a - a
    else delAngle = a - ang2.a
    if (delAngle > Math.PI) delAngle = (2 * Math.PI) - delAngle
    delAngle
  }
  //---------------------------------------------------------------------------
  /**
   * Returns whether an angle must turn clockwise or counterclockwise
   * to overlap another angle.
   *
   * @param this an angle (in radians)
   * @param ang2 an angle (in radians)
   * @return whether a1 must turn CLOCKWISE, COUNTERCLOCKWISE or NONE to
   *         overlap a2.
   */
  def getTurn(ang2: Angle): Int = {
    val crossproduct = Math.sin(ang2.a - a)
    if (crossproduct > 0) return COUNTERCLOCKWISE
    if (crossproduct < 0) return CLOCKWISE
    NONE
  }
  //---------------------------------------------------------------------------
  /**
   * Computes the normalized value of an angle, which is the
   * equivalent angle in the range ( -Pi, Pi ].
   * @return an equivalent angle in the range (-Pi, Pi]
   */
  def normalize() = {
    var newA : Double = a
    while ( {newA > Math.PI}) newA -= PI_TIMES_2
    while ( {newA <= -Math.PI}) newA += PI_TIMES_2
    Angle(newA)
  }
  //---------------------------------------------------------------------------
  /**
   * Computes the normalized positive value of an angle, which is the
   * equivalent angle in the range [ 0, 2*Pi ).
   * E.g.:
   * <ul>
   * <li>normalizePositive(0.0) = 0.0
   * <li>normalizePositive(-PI) = PI
   * <li>normalizePositive(-2PI) = 0.0
   * <li>normalizePositive(-3PI) = PI
   * <li>normalizePositive(-4PI) = 0
   * <li>normalizePositive(PI) = PI
   * <li>normalizePositive(2PI) = 0.0
   * <li>normalizePositive(3PI) = PI
   * <li>normalizePositive(4PI) = 0.0
   * </ul>
   *
   * @param this the angle to normalize, in radians
   * @return an equivalent positive angle
   */
  def normalizePositive() = {
    var newA : Double = a
    if (newA < 0.0) {
      while ( {  newA < 0.0}) newA += PI_TIMES_2
      // in case round-off error bumps the value over
      if (newA >= PI_TIMES_2) newA = 0.0
    }
    else {
      while ( { newA >= PI_TIMES_2}) newA -= PI_TIMES_2
      // in case round-off error bumps the value under
      if (newA < 0.0) newA = 0.0
    }
    Angle(newA)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file Angle.scala
//=============================================================================
