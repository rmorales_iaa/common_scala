/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  20/Feb/2020
 * Time:  21h:01m
 * Description: None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.geometry.interval
//=============================================================================
import java.util.concurrent.atomic.AtomicInteger
//=============================================================================
object IntervalOrdering extends Ordering[Interval] {
  def compare(a:Interval, b:Interval) = a compareTo b
}
//=============================================================================
object Interval {
  //---------------------------------------------------------------------------
  type IntervalDataType = Int
  //---------------------------------------------------------------------------
  private val id = new AtomicInteger(-1)
  //---------------------------------------------------------------------------
  def resetID() = id.set(-1)
  //---------------------------------------------------------------------------
  def getNewID : Int = id.addAndGet(1)
  //---------------------------------------------------------------------------
  def getCurrentID : Int = id.get
  //---------------------------------------------------------------------------
}
//=============================================================================
import Interval._
trait Interval extends Ordered[Interval] {
  //---------------------------------------------------------------------------
  val s : IntervalDataType  //star
  val e : IntervalDataType  //end
  val id: Int
  //---------------------------------------------------------------------------
  require( s <= e, s"The star of interval: $s must be greater or equal to the end of interval: $e")
  //---------------------------------------------------------------------------
  def range = e - s + 1
  //---------------------------------------------------------------------------
  def elementCount = range
  //---------------------------------------------------------------------------
  def equal(itv: Interval) : Boolean = {
    if (itv == null) return false
    if (s != itv.s) return false
    if (e != itv.e) return false
    true
  }
  //---------------------------------------------------------------------------
  def isIn(p: IntervalDataType) = (p >= s) && (p <= e)
  //---------------------------------------------------------------------------
  def fullyContains(itv: Interval) : Boolean =
    (itv.s >= s) && (itv.s <= e) && (itv.e >= s) && (itv.e <= e)
  //---------------------------------------------------------------------------
  def isContiguous(p: IntervalDataType) : Boolean = {
    ((e + 1) == p) ||   //p is just on the right
    ((p + 1) == s)      //p is just on the left
  }
  //---------------------------------------------------------------------------
  def isContiguous(itv: Interval) : Boolean = {
    ((itv.e + 1) == s) ||    //itv is just on the right
     ((e + 1) == itv.s)   //itv is just on the left
  }
  //---------------------------------------------------------------------------
  def intersects(itv: Interval) = isIn(itv.s) || isIn(itv.e) ||
                                  itv.isIn(s) || itv.isIn(e)
  //---------------------------------------------------------------------------
  override def compareTo(itv: Interval): IntervalDataType = compare(itv)
  //---------------------------------------------------------------------------
  def compare(itv: Interval): IntervalDataType = s.compare(itv.s)
  //---------------------------------------------------------------------------
  override def toString = s"[$s,$e]"
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Interval.scala
//=============================================================================