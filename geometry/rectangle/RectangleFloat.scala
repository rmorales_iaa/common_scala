//=============================================================================
package com.common.geometry.rectangle
//=============================================================================
import com.common.geometry.point.{Point2D_Float}
//=============================================================================
object RectangleFloat {
  //---------------------------------------------------------------------------
  def isValid(m: Point2D_Float, M: Point2D_Float) = (m.x <= M.x) && (m.y <= M.y)
  //---------------------------------------------------------------------------
}
//=============================================================================
//bottom left, top right
case class RectangleFloat(min: Point2D_Float, max: Point2D_Float) {
  //---------------------------------------------------------------------------
  require(RectangleFloat.isValid(min,max), s"The min point $min and the max point $max does not create a rectangle")
  //---------------------------------------------------------------------------
  val width  = max.x - min.x
  val height = max.y - min.y
  //---------------------------------------------------------------------------
  def getCenter: Point2D_Float =
    Point2D_Float((min.x + max.x) / 2, (min.y + max.y) / 2)
  //---------------------------------------------------------------------------
  def xMin = min.x
  //---------------------------------------------------------------------------
  def yMin = min.y
  //---------------------------------------------------------------------------
  def xMax = max.x
  //---------------------------------------------------------------------------
  def yMax = max.y
  //---------------------------------------------------------------------------
  def xAxisSize = max.x - min.x + 1
  //---------------------------------------------------------------------------
  def yAxisSize = max.y - min.y + 1
  //---------------------------------------------------------------------------
  def area = (max.x - min.x) * (max.y - min.y)
  //---------------------------------------------------------------------------
  def area2 = area * Math.cos(((max.y + min.y) / 2) .toRadians)
  //---------------------------------------------------------------------------
  def equal(r: RectangleFloat) = (min == r.min) && (max == r.max)
  //---------------------------------------------------------------------------
  def isIn(p: Point2D_Float) =
    p.x >= min.x && p.x <= max.x &&
    p.y >= min.y && p.y <= max.y
  //---------------------------------------------------------------------------
  def isUp(r: RectangleFloat) = min.y >= r.max.y
  //---------------------------------------------------------------------------
  def isDown(r: RectangleFloat) = !isUp(r)
  //---------------------------------------------------------------------------
  def isAtLeft(r: RectangleFloat) = max.x >= r.min.x
  //---------------------------------------------------------------------------
  def isAtRight(r: RectangleFloat) = !isAtLeft(r)
  //---------------------------------------------------------------------------
  def isContiguousX_Axis(r: RectangleFloat): Boolean =  (max.x + 1) == r.min.x || (r.max.x + 1) == min.x
  //---------------------------------------------------------------------------
  def isContiguousY_Axis(r: RectangleFloat): Boolean =  (max.y + 1) == r.min.y || (r.max.y + 1) == min.y
  //---------------------------------------------------------------------------
  def canIntersect(other: RectangleDouble): Boolean =
    (other.min.x >= min.x && other.min.x <= max.x) ||
     (other.max.x >= min.x && other.max.x <= max.x) ||
     (other.min.y >= min.y && other.min.y <= max.y) ||
     (other.max.y >= min.y && other.max.y <= max.y)
  //---------------------------------------------------------------------------
  def getIntersection(r: RectangleFloat, margin: Point2D_Float = Point2D_Float.POINT_ZERO) : Option[RectangleFloat] = {
    val minX = Math.max(min.x, r.min.x)
    val maxX = Math.min(max.x, r.max.x)
    val minY = Math.max(min.y, r.min.y)
    val maxY = Math.min(max.y, r.max.y)
    if ((maxX >= minX) && (maxY >= minY)) Some(RectangleFloat(Point2D_Float(minX,minY), Point2D_Float(maxX,maxY)))
    else {
      if (margin == Point2D_Float.POINT_ZERO) None
      else getIntersection(RectangleFloat(r.min subtractMinZero margin, r.max + margin))
    }
  }
  //---------------------------------------------------------------------------
  def intersects(r: RectangleFloat, margin: Point2D_Float = Point2D_Float.POINT_ZERO): Boolean = getIntersection(r,margin).isDefined
  //---------------------------------------------------------------------------
  def getCentroid() = Point2D_Float(min.x + width/2, min.y + height / 2)
  //---------------------------------------------------------------------------
  def getCentroidAsSequence() =  {
    val r = getCentroid()
    Seq(r.x, r.y)
  }
  //---------------------------------------------------------------------------
  def getPosSequence() = Seq(min,max,Point2D_Float(max.x,min.y),Point2D_Float(min.x,max.y))
  //---------------------------------------------------------------------------
  override def toString = s"min=$min max=$max"
  //---------------------------------------------------------------------------
  def toStringShort = s"{$min,$max}"
  //---------------------------------------------------------------------------
  def getMinDistanceToPoint(p : Point2D_Float): Float = {
    //  The area around/in the rectangle is defined in terms of
    //  several regions:
    //
    //  y
    //  |
    //  O--x
    //                        xPixMax
    //        1   |    2     |  3
    //      ======|==========|======   yPixMax
    //        4   |    0 (in)|  5
    //      ======|==========|======   yMin
    //        6   |    7     |  8
    //            xMin

    //-------------------------------------------------------------------------
    val xMin = min.x
    val yMin = min.y

    val xMax = max.x
    val yMax = max.y

    val vertexBottomLeft = min
    val vertexBottomRight = Point2D_Float(xMax, yMin)

    val vertexTopLeft = Point2D_Float(xMin, yMax)
    val vertexTopRight = max

    p.x <= xMin  match {
      case true =>   //1,4 or 6
        p.y <= yMin  match {
          case true  =>   p.getDistance(vertexBottomLeft)       //6
          case false =>  //4 or 6
            p.y >= yMax  match {
              case true  => p.getDistance(vertexTopLeft)    //1
              case false => p.getDistanceToLine(vertexBottomLeft,vertexTopLeft)    //4
            }
        }
      case false =>    //2,3,0,5,7,8
        p.x >= xMax  match {
          case true => //3,5 or 8
            p.y <= yMin match {
              case true => p.getDistance(vertexBottomRight) //8
              case false => //3,5
                p.y >= yMax match {
                  case true => p.getDistance(vertexTopRight) //3
                  case false => p.getDistanceToLine(vertexBottomRight, vertexTopRight) //5
                }
            }
          case false => //2,0,7
            p.y <= yMin match {
              case true => p.getDistanceToLine(vertexBottomLeft, vertexBottomRight) //7
              case false => //2,0
                p.y >= yMax match {
                  case true => p.getDistanceToLine(vertexTopLeft, vertexTopRight) //2
                  case false => //0
                    Math.min(
                      Math.min(p.getDistanceToLine(vertexBottomLeft, vertexBottomRight)
                        , p.getDistanceToLine(vertexTopLeft, vertexTopRight))
                      ,
                      Math.min(p.getDistanceToLine(vertexBottomLeft, vertexTopLeft)
                        , p.getDistanceToLine(vertexBottomRight, vertexTopRight))
                    )
                }
            }
        }
    }
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file RectangleFloat.scala
//=============================================================================
