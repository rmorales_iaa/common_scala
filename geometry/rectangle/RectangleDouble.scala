//=============================================================================
package com.common.geometry.rectangle
//=============================================================================
import com.common.geometry.point.Point2D_Double
//=============================================================================
object RectangleDouble {
  //---------------------------------------------------------------------------
  def isValid(m: Point2D_Double, M: Point2D_Double) = (m.x <= M.x) && (m.y <= M.y)
  //---------------------------------------------------------------------------
  def apply(x: Double, y: Double, X: Double, Y: Double): RectangleDouble =
    RectangleDouble(Point2D_Double(x, y), Point2D_Double(X, Y))

  //---------------------------------------------------------------------------
  def getIntersectionSeq(recSeq: Array[RectangleDouble]
                         , margin: Point2D_Double = Point2D_Double.POINT_ZERO): Option[RectangleDouble] =
    if (recSeq.isEmpty) None
    else
      recSeq.tail.foldLeft(Option(recSeq.head)) { (commonRecOpt, nextRec) =>
        commonRecOpt.flatMap(commonRec => commonRec.getIntersection(nextRec, margin))
      }
  //---------------------------------------------------------------------------
}
//=============================================================================
//bottom left, top right
case class RectangleDouble(min: Point2D_Double, max: Point2D_Double, id: Long = 0) {
  //---------------------------------------------------------------------------
  require(RectangleDouble.isValid(min,max), s"The min point $min and the max point $max does not create a rectangle")
  //---------------------------------------------------------------------------
  val width  = max.x - min.x
  val height = max.y - min.y
  //---------------------------------------------------------------------------
  def getCenter: Point2D_Double =
    Point2D_Double((min.x + max.x) / 2, (min.y + max.y) / 2)
  //---------------------------------------------------------------------------
  def xMin = min.x
  //---------------------------------------------------------------------------
  def yMin = min.y
  //---------------------------------------------------------------------------
  def xMax = max.x
  //---------------------------------------------------------------------------
  def yMax = max.y
  //---------------------------------------------------------------------------
  def xAxisSize = max.x - min.x + 1
  //---------------------------------------------------------------------------
  def yAxisSize = max.y - min.y + 1
  //---------------------------------------------------------------------------
  def area = (max.x - min.x) * (max.y - min.y)
  //---------------------------------------------------------------------------
  def area2 = area * Math.cos(((max.y + min.y) / 2).toRadians)
  //---------------------------------------------------------------------------
  def equal(r: RectangleDouble) = (min == r.min) && (max == r.max)
  //---------------------------------------------------------------------------
  def isIn(p: Point2D_Double) =
    p.x >= min.x && p.x <= max.x &&
      p.y >= min.y && p.y <= max.y
  //---------------------------------------------------------------------------
  def isIn(x: Double, y: Double) =
    x >= min.x && x <= max.x &&
      y >= min.y && y <= max.y
  //---------------------------------------------------------------------------
  def isUp(r: RectangleDouble) = min.y >= r.max.y
  //---------------------------------------------------------------------------
  def isDown(r: RectangleDouble) = !isUp(r)
  //---------------------------------------------------------------------------
  def isAtLeft(r: RectangleDouble) = max.x >= r.min.x
  //---------------------------------------------------------------------------
  def isAtRight(r: RectangleDouble) = !isAtLeft(r)
  //---------------------------------------------------------------------------
  def isContiguousX_Axis(r: RectangleDouble): Boolean =  (max.x + 1) == r.min.x || (r.max.x + 1) == min.x
  //---------------------------------------------------------------------------
  def isContiguousY_Axis(r: RectangleDouble): Boolean =  (max.y + 1) == r.min.y || (r.max.y + 1) == min.y

  //---------------------------------------------------------------------------
  def canIntersect(other: RectangleDouble): Boolean =
    (min.x <= other.max.x && max.x >= other.min.x) &&
    (min.y <= other.max.y && max.y >= other.min.y)
  //---------------------------------------------------------------------------
  def getIntersection(r: RectangleDouble
                      , margin: Point2D_Double = Point2D_Double.POINT_ZERO) : Option[RectangleDouble] = {
    if (!canIntersect(r)) return None

    val minX = Math.max(min.x, r.min.x)
    val maxX = Math.min(max.x, r.max.x)
    val minY = Math.max(min.y, r.min.y)
    val maxY = Math.min(max.y, r.max.y)

    if (maxX >= minX && maxY >= minY)
      Some(RectangleDouble(new Point2D_Double(minX, minY), new Point2D_Double(maxX, maxY)))
    else {
      if (margin != Point2D_Double.POINT_ZERO) {
        val expandedR = RectangleDouble(r.min.subtractMinZero(margin), r.max + margin)
        getIntersection(expandedR, Point2D_Double.POINT_ZERO)
      }
      else None
    }
  }
  //---------------------------------------------------------------------------
  def getIntersectionSeq(recSeq: Array[RectangleDouble]
                         , margin: Point2D_Double = Point2D_Double.POINT_ZERO): Option[RectangleDouble] =
    recSeq.foldLeft(Option(this)) { (commonRecOpt, nextRec) =>
      commonRecOpt.flatMap(commonRec => commonRec.getIntersection(nextRec, margin))
    }
  //---------------------------------------------------------------------------
  def intersects(r: RectangleDouble, margin: Point2D_Double = Point2D_Double.POINT_ZERO): Boolean = getIntersection(r,margin).isDefined
  //---------------------------------------------------------------------------
  def getCentroid() = new Point2D_Double(min.x + width/2, min.y + height / 2)
  //---------------------------------------------------------------------------
  def getCentroidAsSequence() =  {
    val r = getCentroid()
    Seq(r.x, r.y)
  }
  //---------------------------------------------------------------------------
  def getPosSequence() = Seq(min,max,new Point2D_Double(max.x,min.y),new Point2D_Double(min.x,max.y))
  //---------------------------------------------------------------------------
  override def toString = s"min=$min max=$max"
  //---------------------------------------------------------------------------
  def toStringShort = s"{$min,$max}"
  //---------------------------------------------------------------------------
  def getMinDistanceToPoint(p : Point2D_Double): Double = {
    //  The area around/in the rectangle is defined in terms of
    //  several regions:
    //
    //  y
    //  |
    //  O--x
    //                        xPixMax
    //        1   |    2     |  3
    //      ======|==========|======   yPixMax
    //        4   |    0 (in)|  5
    //      ======|==========|======   yMin
    //        6   |    7     |  8
    //            xMin

    //-------------------------------------------------------------------------
    val xMin = min.x
    val yMin = min.y

    val xMax = max.x
    val yMax = max.y

    val vertexBottomLeft = min
    val vertexBottomRight = new Point2D_Double(xMax, yMin)

    val vertexTopLeft = new Point2D_Double(xMin, yMax)
    val vertexTopRight = max

    p.x <= xMin  match {
      case true =>   //1,4 or 6
        p.y <= yMin  match {
          case true  =>   p.getDistance(vertexBottomLeft)       //6
          case false =>  //4 or 6
            p.y >= yMax  match {
              case true  => p.getDistance(vertexTopLeft)    //1
              case false => p.getDistanceToLine(vertexBottomLeft,vertexTopLeft)    //4
            }
        }
      case false =>    //2,3,0,5,7,8
        p.x >= xMax  match {
          case true => //3,5 or 8
            p.y <= yMin match {
              case true => p.getDistance(vertexBottomRight) //8
              case false => //3,5
                p.y >= yMax match {
                  case true => p.getDistance(vertexTopRight) //3
                  case false => p.getDistanceToLine(vertexBottomRight, vertexTopRight) //5
                }
            }
          case false => //2,0,7
            p.y <= yMin match {
              case true => p.getDistanceToLine(vertexBottomLeft, vertexBottomRight) //7
              case false => //2,0
                p.y >= yMax match {
                  case true => p.getDistanceToLine(vertexTopLeft, vertexTopRight) //2
                  case false => //0
                    Math.min(
                      Math.min(p.getDistanceToLine(vertexBottomLeft, vertexBottomRight)
                        , p.getDistanceToLine(vertexTopLeft, vertexTopRight))
                      ,
                      Math.min(p.getDistanceToLine(vertexBottomLeft, vertexTopLeft)
                        , p.getDistanceToLine(vertexBottomRight, vertexTopRight))
                    )
                }
            }
        }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RectangleDouble.scala
//=============================================================================
