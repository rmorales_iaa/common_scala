/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  22/Nov/2020
 * Time:  02h:24m
 * Description: https://en.wikipedia.org/wiki/Delaunay_triangulation
 * Adpated from: https://github.com/mdda/DelaunayScala/blob/master/src/main/scala/Delaunay.scala
 */
//=============================================================================
package com.common.geometry.spatialPartition.delauny
//=============================================================================
import com.common.geometry.point.Point2D_Double
//=============================================================================
//=============================================================================
object Delaunay {
  //---------------------------------------------------------------------------
  private val EPSILON = 0.0000001
  //---------------------------------------------------------------------------
  def calculate(measurements : List[Point2D_Double]) : Seq[(Int, Int, Int)] = {
    case class ITRIANGLE(p1:Int, p2:Int, p3:Int)
    case class IEDGE(p1:Int, p2:Int)
    //-------------------------------------------------------------------------
    case class edgeAnihilationSet(s: Set[IEDGE]) {
      def add(e: IEDGE): edgeAnihilationSet = {
        if( s.contains(e) ) {
          edgeAnihilationSet(s - e)
        }
        else {
          val e_reversed = IEDGE(e.p2, e.p1)
          if( s.contains(e_reversed) ) {
            edgeAnihilationSet(s - e_reversed)
          }
          else {
            edgeAnihilationSet(s + e)
          }
        }
      }
      def toList() = s.toList
    }
    //-------------------------------------------------------------------------
    //  Return TRUE if a point q(x,y) is inside the circumcircle made up of the points p1(x,y), p2(x,y), p3(x,y)
    //  The circumcircle centre (x,y) is returned and the radius r
    //  NOTE: A point on the edge is inside the circumcircle
    def CircumCircle( q:Point2D_Double, p1:Point2D_Double, p2:Point2D_Double, p3:Point2D_Double) : (/*inside :*/Boolean, /*center:*/Point2D_Double, /*radius:*/Float) = {
      if ( Math.abs(p1.y-p2.y) < EPSILON && Math.abs(p2.y-p3.y) < EPSILON ) {
        System.err.println("CircumCircle: Points are colinear");
        println("CircumCircle: Points are colinear *****************************")
        (false, new Point2D_Double(0,0), 0)
      }
      else {
        val mid1 = new Point2D_Double( (p1.x+p2.x)/2, (p1.y+p2.y)/2 )
        val mid2 = new Point2D_Double( (p2.x+p3.x)/2, (p2.y+p3.y)/2 )

        val c =
          if ( Math.abs(p2.y-p1.y) < EPSILON ) {
            //println("CircumCircle: p1&p2 have same y")
            val d2 = -(p3.x-p2.x) / (p3.y-p2.y)
            val xc =  mid1.x
            val yc =  d2 * (xc - mid2.x) + mid2.y
            new Point2D_Double(xc, yc)
          }
          else
            if ( Math.abs(p3.y-p2.y) < EPSILON ) {
              //println("CircumCircle: p2&p3 have same y")
              val d1 = -(p2.x-p1.x) / (p2.y-p1.y)
              val xc =  mid2.x
              val yc =  d1 * (xc - mid1.x) + mid1.y
              new Point2D_Double(xc, yc)
            }
            else {
              val d1 = -(p2.x-p1.x) / (p2.y-p1.y)
              val d2 = -(p3.x-p2.x) / (p3.y-p2.y)
              val xc =  ((d1*mid1.x - mid1.y) - (d2*mid2.x - mid2.y)) / (d1 - d2)
              val yc =  d1 * (xc - mid1.x) + mid1.y
              new Point2D_Double(xc, yc)
            }

        val rsqr = {
          val (dx, dy) = (p2.x-c.x, p2.y-c.y)  // Distance from (any) 1 point on triangle to circle center
          dx*dx + dy*dy
        }
        val qsqr = {
          val (dx, dy) = (q.x -c.x, q.y -c.y)    // Distance from query_point to circle center
          dx*dx + dy*dy
        }

        ( qsqr <= rsqr, c, Math.sqrt(rsqr).toFloat )
      }
    }
    //-------------------------------------------------------------------------
    val n_points = measurements.length
    // Find the maximum and minimum vertex bounds, to allow calculation of the bounding triangle
    val Pmin = new Point2D_Double( measurements.map(_.x).min, measurements.map(_.y).min )  // Top Left
    val Pmax = new Point2D_Double( measurements.map(_.x).max, measurements.map(_.y).max )  // Bottom Right
    val diameter = (Pmax.x - Pmin.x) max (Pmax.y - Pmin.y)
    val Pmid = new Point2D_Double( (Pmin.x + Pmax.x)/2, (Pmin.y + Pmax.y)/2 )
    //-------------------------------------------------------------------------
    /*
      Set up the supertriangle, which is a triangle which encompasses all the sample points.
      The supertriangle coordinates are added to the end of the vertex list.
      The supertriangle is the first triangle in the triangle list.
    */
    //-------------------------------------------------------------------------
    val point_list = measurements ::: List(
      new Point2D_Double( Pmid.x - 2*diameter, Pmid.y - 1*diameter),
      new Point2D_Double( Pmid.x - 0*diameter, Pmid.y + 2*diameter),
      new Point2D_Double( Pmid.x + 2*diameter, Pmid.y - 1*diameter)
    )
    val main_current_triangles   = List( ITRIANGLE(n_points+0, n_points+1, n_points+2) ) // initially containing the supertriangle
    val main_completed_triangles: List[ITRIANGLE] = Nil                                  // initially empty 
    //-------------------------------------------------------------------------
    def convert_relevant_triangles_into_new_edges(completed_triangles: List[ITRIANGLE], triangles: List[ITRIANGLE], point: Point2D_Double) =
      triangles.foldLeft( (completed_triangles: List[ITRIANGLE], List[ITRIANGLE](), edgeAnihilationSet(Set.empty[IEDGE])) ) {
        case ((completed, current, edges), triangle) => {
          // If the point 'point_being_added' lies inside the circumcircle then the three edges 
          // of that triangle are added to the edge buffer and that triangle is removed.

          // Find the coordinates of the points in this incomplete triangle
          val corner1 = point_list( triangle.p1 )
          val corner2 = point_list( triangle.p2 )
          val corner3 = point_list( triangle.p3 )

          val (inside, circle, r) = CircumCircle(point,  corner1,  corner2,  corner3)

          // have we moved too far in x to bother with this one ever again? (initial point list must be sorted for this to work)
          if (circle.x + r < point.x) {  // (false &&) to disable the 'completed' optimisation
            //printf(s"point_x=${point.x} BEYOND triangle : ${triangle} with circle=[${circle}, ${r}]\n")
            ( triangle::completed, current, edges ) // Add this triangle to the 'completed' accumulator, and don't append it on current list
          }
          else {
            if(inside) {
              //printf(s"point INSIDE triangle : ${triangle}\n")
              // Add the triangle's edge onto the edge pile, and remove the triangle
              val edges_with_triangle_added =
              edges
                .add( IEDGE(triangle.p1, triangle.p2) )
                .add( IEDGE(triangle.p2, triangle.p3) )
                .add( IEDGE(triangle.p3, triangle.p1) )
              ( completed, current, edges_with_triangle_added )
            }
            else {
              //printf(s"point outside triangle : ${triangle}\n")
              ( completed, triangle::current, edges )  // This point was not inside this triangle - just append it to the 'current' list
            }
          }
        }
      }
    //-------------------------------------------------------------------------
    def update_triangle_list_for_new_point(completed_triangles: List[ITRIANGLE], triangles: List[ITRIANGLE], point_i: Int) = {
      val (completed_triangles_updated, current_triangles_updated, edges_created) =
        convert_relevant_triangles_into_new_edges(completed_triangles, triangles, point_list(point_i))

      // Form NEW triangles for the current point, all edges arranged in clockwise order.
      val new_triangles = for ( e <- edges_created.toList ) yield ITRIANGLE( e.p1, e.p2, point_i )
      (completed_triangles_updated, new_triangles ::: current_triangles_updated)
    }
    //-------------------------------------------------------------------------
    // Go through points in x ascending order.  No need to sort the actual points, just output the point_i in correct sequence 
    // (relies on sortBy being 'stable' - so that sorting on y first will enable duplicate detection afterwards)
    val points_sorted_xy_ascending = point_list.take(n_points).zipWithIndex sortBy(_._1.y) sortBy(_._1.x) map { case (Point2D_Double(x,y), i) => i }
    val points_sorted_and_deduped =
      points_sorted_xy_ascending.foldLeft( (Nil:List[Int], -1) ) {
        case ((list, point_last), point_i) => if(point_last>=0 && point_list(point_last) == point_list(point_i)) {
          printf(s"Skipping duplicate points {${point_last},${point_i}}\n")
          ( list, point_i ) // Don't append this point to the list
        }
        else
          ( point_i::list, point_i )
      }._1.reverse // list of points is first element of tuple, and were pre-pended, so list needs reversing

    // Add each (original) point, one at a time, into the existing mesh
    val (final_completed, final_triangles) =
      points_sorted_and_deduped.foldLeft( (main_completed_triangles, main_current_triangles) ) {
        case ((completed, current), point_i) => update_triangle_list_for_new_point(completed, current, point_i)
      }

    val full_list_of_triangles = (final_completed ::: final_triangles)
    // filter out triangles with points that have point_i >= n_points (since these are part of the fake supertriangle)
    full_list_of_triangles.filterNot( t => (t.p1>=n_points || t.p2>=n_points || t.p3>=n_points)) map { t => (t.p1, t.p2, t.p3) }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Delaunay.scala
//=============================================================================