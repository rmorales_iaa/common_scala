/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  13/Jan/2021
 * Time:  16h:07m
 * Description: None
 */
//=============================================================================
package com.common.geometry.spatialPartition.voronoi
//=============================================================================
import com.common.geometry.line.Line
import com.common.geometry.point.Point2D_Double
import com.common.geometry.spatialPartition.voronoi.VoronoiDiagram.SITE_TYPE
//=============================================================================
//=============================================================================
case class CalculatedEdge(var start: Point2D_Double, left: Point2D_Double, right: Point2D_Double) {
  var end: Point2D_Double = Point2D_Double.POINT_NAN
  var neighbour : CalculatedEdge = null  //connect splitted edges (built when  arc was splitting) in order to finish the edges at the end of the algorithm
  val direction = getDirectionalVector  //direction: directional vector. Line defined from 'star' to 'end' and normal to the line from right to left site
  val line = getEdgeLine //line: used to calculate the edge positon
  //---------------------------------------------------------------------------
  def this(a: SITE_TYPE,b: SITE_TYPE, c: SITE_TYPE) =  this(Point2D_Double(a), Point2D_Double(b), Point2D_Double(c))
  //---------------------------------------------------------------------------
  def this(a: Point2D_Double, b: SITE_TYPE, c: SITE_TYPE) =  this(a, Point2D_Double(b), Point2D_Double(c))
  //---------------------------------------------------------------------------
  def hasNeighbour = neighbour != null
  //---------------------------------------------------------------------------
  def getNeighbourEnd() = neighbour.end
  //---------------------------------------------------------------------------
  def isFinished = !end.x.isNaN && !end.y.isNaN
  //---------------------------------------------------------------------------
  def getSegment = Segment(start,end)
  //---------------------------------------------------------------------------
  private def getEdgeLine()  = {
    val slope = (right.x - left.x) / (left.y - right.y)
    val l  = Line(slope, start.y - slope * start.x)
    l
  }
  //---------------------------------------------------------------------------
  private def getDirectionalVector() =
    new Point2D_Double(right.y - left.y, -(right.x - left.x)).normalize
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CalculatedEdge.scala
//=============================================================================
