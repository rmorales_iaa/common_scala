/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  18/Dec/2020
 * Time:  12h:14m
 * Description: None
 */
//=============================================================================
package com.common.geometry.spatialPartition.voronoi
//=============================================================================
import java.awt.{Color, Font}
//=============================================================================
import com.common.geometry.point.Point2D
import com.common.plot.bmpImage.BmpImagePlot
//=============================================================================
//=============================================================================
case class VoronoiPlot(
  voronoi: VoronoiDiagram
  , filename: String
  , siteRadius : Int = 1
  , siteColor: Color = Color.WHITE
  , siteFont: Option[Font] =  Some(new Font(Font.MONOSPACED, Font.PLAIN, 10)) //Set to 'None' for no text on  site
  , siteFontColor: Color = Color.YELLOW
  , siteFontOffset: Point2D = Point2D(-20,8)
  , lineWidth: Float = 0.5f
  , directixFontOffet : Int = -4
  , vertexColor: Color = Color.BLUE
  , pointColor: Color = Color.ORANGE
  , inscribedCircleColor : Color = Color.LIGHT_GRAY
  , inscribedCircleBorderColor : Color = Color.GREEN
  , segmentColor: Color = Color.MAGENTA
  , direcTrixColor: Color = Color.RED
  , parabolaColor: Color = Color.GREEN
  , intersectionWithParabolaColor: Color = Color.YELLOW
  , intersectionLineBetweenTwoParabolaColor: Color = Color.PINK
  , imageFormat: String = "png") {

      //initialize the plot
      private val plotDimensions = voronoi.getPlotDimensions
      private val offset = plotDimensions.plotPointOffset
      val plot = BmpImagePlot(plotDimensions.plotMaxX, plotDimensions.plotMaxY, offset)
      plot.restetToColor()
      plot.setLineWidth(lineWidth)
     //-----------------------------------------------------------------------
      private def plotSegment(l: Segment, c: Color = segmentColor) = {
        val start = l.start.toPoint2D()
        val end = l.end.toPoint2D
        plot.line(start,end,Some(c))
      }
      //-----------------------------------------------------------------------
      private def plotSite(p: Point2D, c: Color = siteColor) =  {
        plot.rectangleByPosAndRadius(p,siteRadius, Some(c), Some(c))
        if (siteFont.isDefined) plot.string(p.toString, p + siteFontOffset, Some(siteFontColor))
      }
      //-----------------------------------------------------------------------
      private def plotPoint(p: Point2D, c: Color) =
        plot.rectangleByPosAndRadius(p,siteRadius, innerColor = Some(c), borderColor = Some(c))
     //----------------------------------------------------------------------
     private def save(): Unit = plot.save(filename,imageFormat)
     //----------------------------------------------------------------------
     def result() = {

      //plot inscribed circle
      plot.circle(voronoi.getMaxCircle, Some(inscribedCircleColor), Some(inscribedCircleBorderColor))

      //plot a line between all sites
      val siteSeq = voronoi.siteSeq :+ voronoi.siteSeq.head
      siteSeq.sliding(2).map{ s=> new Segment(s(0),s(1))}.foreach (plotSegment(_ , c = Color.LIGHT_GRAY))

      //sites
      voronoi.siteSeq foreach ( plotSite(_) )

      //vertex seq
      voronoi.getVertexSeq() foreach (p=> plotPoint( p.toPoint2D, vertexColor) )

      //point seq
      voronoi.getPointSeq() foreach (p=> plotPoint( p.toPoint2D, pointColor) )

      //segments
      voronoi.getEdgeSeq map (edge=> plotSegment(edge.getSegment) )

      save
    }
    //-------------------------------------------------------------------------
}
//=============================================================================
//End of file VoronoiPlot.scala
//=============================================================================
