/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Jan/2021
 * Time:  14h:30m
 * Description:   Calculate by brute force, the voronoi areas corresponding to each segment2D
  //Each segment2D (source) is condiderered as a polynomy and then its infuence area (voronoi area) is calculated
  //Typical voronoi algorithm (i.e. Fortune's one) can not be applied because a source,
  //usally is not a just a single point
 */
//=============================================================================
package com.common.geometry.spatialPartition.voronoi.polygon
//=============================================================================
import com.common.hardware.cpu.CPU
import com.common.dataType.tree.kdTree.K2d_Tree
import com.common.geometry.point.Point2D
import com.common.geometry.segment.Segment1D
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.region.Region
import com.common.logger.MyLogger
import com.common.math.MyMath
import com.common.util.parallelTask.ParallelTask
//=============================================================================
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object VoronoiDiagramPolygon extends MyLogger {
  //---------------------------------------------------------------------------
  private def getClosestSourceToPoint(region: Region, kdtreeBoxHull: K2d_Tree, pixPos: Point2D) = {
    var minDistance = Double.MaxValue
    var closestSource : Segment2D = null
    kdtreeBoxHull.getKNN(pixPos.toSequence, keepAllWithMinDistanceIgnoringK = true).map(n => region.getFromMap(n.v)).foreach { seg2D =>
      val d = seg2D.getMinDistanceFromPointToConvexHull(pixPos,region.offset)
      if ((d < minDistance) || seg2D.contains(pixPos)) {
        closestSource = seg2D
        minDistance = d
      }
    }
    closestSource
  }
  //--------------------------------------------------------------------------
  private def buildResult(region: Region
                           , voronoiAreaMap: scala.collection.mutable.Map[Int, scala.collection.mutable.Map[Int, ArrayBuffer[Segment1D]]])
  : Array[(Segment2D, Segment2D)] = {
    val r = (for ((key, area) <- voronoiAreaMap) yield {
      val ySeq = area.keySet.toArray.sorted
      val xOffset = (area.values flatMap  ( _.map (_.s) )).toList.sorted.head
      val offset = Point2D(xOffset, ySeq.head)
      val seg1d_seq = ySeq map { y =>  area(y).map { s1d=> Segment1D(s1d.s - offset.x, s1d.e - offset.x) }}
      (region.getFromMap(key),Segment2D(seg1d_seq map (_.toArray), offset))
    }).toArray
    info("Voroni area count: " + r.length)
    r
  }
  //--------------------------------------------------------------------------
  private def addToVoronoiArea(pixPos: Point2D, areaMap: scala.collection.mutable.Map[Int, ArrayBuffer[Segment1D]]): Unit = {
    val py = pixPos.y
    val px = pixPos.x
    if (areaMap.contains(py)) {
      val xSeq = areaMap(py)
      xSeq.zipWithIndex foreach { case (s1d, i) =>
        val newS1d = s1d.addIfContiguous(px)
        if (newS1d.isDefined) {
          xSeq(i) = newS1d.get
          return
        }
      }
      xSeq.append(Segment1D(px))
    }
    else areaMap(py) = ArrayBuffer(Segment1D(pixPos.x))
  }
  //--------------------------------------------------------------------------
  private def processAllPixelSeq(region: Region
                                 , kdtreeBoxHull: K2d_Tree
                                 , voronoiAreaMap: scala.collection.mutable.Map[Int, scala.collection.mutable.Map[Int, ArrayBuffer[Segment1D]]]
                                 , xMin: Int
                                 , xMax: Int
                                 , yMin: Int
                                 , yMax: Int) = {
    for (y <- yMin until yMax;
         x <- xMin until xMax) {
      val pixPos = Point2D(x, y)
      val seg2D = getClosestSourceToPoint(region, kdtreeBoxHull, pixPos)
      if(seg2D != null) {
        val key = seg2D.id
        synchronized { //append point to thre voronoi area of the closest segment2D (source)
          if (voronoiAreaMap.contains(key)) addToVoronoiArea(pixPos, voronoiAreaMap(key))
          else voronoiAreaMap(key) = scala.collection.mutable.Map(pixPos.y -> ArrayBuffer(Segment1D(pixPos.x)))
        }
      }
    }
  }
  //---------------------------------------------------------------------------
  def calculateParallel(region: Region
                        , xMin : Int
                        , xMax: Int
                        , yMin : Int
                        , yMax: Int
                        , seg2d_Seq: Array[Segment2D]
                        , detailed: Boolean  = true //true: use all vertex of all sources. false: use just the enclosing box (4 vertex) for each source
                        , coreCount: Int = CPU.getCoreCount()
                       )  = {
    //-------------------------------------------------------------------------
    val voronoiAreaMap = scala.collection.mutable.Map[Int, scala.collection.mutable.Map[Int, ArrayBuffer[Segment1D]]]()
    val kdtreeBoxHull = if (detailed) region.getK2D_TreeByVertex(seg2d_Seq) else region.getK2D_TreeByByBoxHull(seg2d_Seq)
    //-------------------------------------------------------------------------
    class MyParallelTask(rowSeqSeq: Array[Array[Int]]) extends ParallelTask[Array[Int]] (
      rowSeqSeq
      , coreCount //thread cores - 2
      , isItemProcessingThreadSafe = true) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(rowSeq: Array[Int]) =
        processAllPixelSeq(region, kdtreeBoxHull, voronoiAreaMap, xMin, xMax, rowSeq.head, rowSeq.last + 1)
      //-------------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    val rowSeq = MyMath.getPartitionInt(yMin, yMax-1, coreCount)
    new MyParallelTask(rowSeq)
    buildResult(region, voronoiAreaMap)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def calculateSingle(region: Region
                      , xMin: Int
                      , xMax: Int
                      , yMin: Int
                      , yMax: Int
                      , seg2d_Seq: Array[Segment2D]
                      , detailed: Boolean = true //true: use all vertex of all sources. false: use just the enclosing box (4 vertex) for each source
                     ) = {
    //(segment2D.m2_id)->(Map[yPos,Seq[xStart,xEnd])
    //used to process the segment2D that represents the voronoi area
    val voronoiAreaMap = scala.collection.mutable.Map[Int, scala.collection.mutable.Map[Int, ArrayBuffer[Segment1D]]]()
    val kdtreeBoxHull = if (detailed) region.getK2D_TreeByVertex(seg2d_Seq) else region.getK2D_TreeByByBoxHull(seg2d_Seq)
    //------------------------------------------------------------------------
    processAllPixelSeq(region, kdtreeBoxHull, voronoiAreaMap, xMin, xMax, yMin, yMax)
    buildResult(region, voronoiAreaMap)
  }
  //---------------------------------------------------------------------------
  def synthetizeVoroiAreaSeq(name: String
                             , xSize: Int
                             , ySize: Int
                             , voronoiAreaSeq: Array[(Segment2D, Segment2D)]
                             , offset: Point2D = Point2D.POINT_ZERO
                             , format: String = "png"): Unit = {

    info(s"Synthetizing voronoi area sequence")
    val bi = new BufferedImage(xSize, ySize, BufferedImage.TYPE_INT_RGB)
    var gray = 10

    //area
    voronoiAreaSeq foreach { case (_, seg2D_VoronoiArea) =>
      //pick gray color
      val pixColor = gray + (gray << 8) + (gray << 16)
      gray = (gray + 10) % 255

      seg2D_VoronoiArea.getAbsolutePosByElement(offset) foreach { p =>
        val invertY = ySize - p.y - 1
        bi.setRGB(p.x, invertY, pixColor)
      }
    }

    //source
    voronoiAreaSeq foreach { case (seg2D, _) =>
      seg2D.getAbsolutePosByElement(offset) foreach { p =>
        val invertY = ySize - p.y - 1
        bi.setRGB(p.x, invertY, Color.MAGENTA.getRGB)
      }
    }

    val fileName = if (name.endsWith("." + format)) name else name + "." + format
    ImageIO.write(bi, format, new File(fileName))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file VoronoiDiagramPolygon.scala
//=============================================================================
