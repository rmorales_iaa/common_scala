/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  13/Jan/2021
 * Time:  15h:18m
 * Description: BAsed on:
 * https://github.com/rhololkeolke/EECS-466-Project
 * https://sites.google.com/site/pointcloudreconstruction/delaunay-triangulation-and-voronoi-diagrams
 */
//=============================================================================
package com.common.geometry.spatialPartition.voronoi
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.geometry.spatialPartition.voronoi.VoronoiDiagram.SITE_TYPE
//=============================================================================
// There are two type of events:
//  Site event is when the sweepline hits a NEW point in the input set.
//  Circle event is when the sweepline hits a location that is equidistant from 3 input sites.
//      It is called a circle event because when this occurs a circle circumscribing the 3 points will lie directly
//      on the Voronoi vertex. This event is aldo calledc edge-intersection (or arc-squeeze) events are also referred
//      to as circle events or arc-removal events.
sealed trait EventItem extends Ordered[EventItem] {
  //---------------------------------------------------------------------------
  val yAxis: Double  //used to sort events
  //---------------------------------------------------------------------------
  def getPoint = this match {
    case s: SiteEvent => Point2D_Double(s.site)
    case c: CircleEvent => c.intersection
  }
  //---------------------------------------------------------------------------
  def compare(b : EventItem) = yAxis compare b.yAxis
  //---------------------------------------------------------------------------
  override def toString() = getPoint.toString
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class SiteEvent(site: SITE_TYPE, yAxis : Double) extends EventItem {
  //---------------------------------------------------------------------------
  def this(_s: SITE_TYPE) = this(_s, _s.y)
  //---------------------------------------------------------------------------
}
//=============================================================================
sealed case class CircleEvent(intersection: Point2D_Double, yAxis : Double, squeezedArc: BeachLineNodeArc) extends EventItem
//=============================================================================
//End of file EventItem.scala
//=============================================================================
