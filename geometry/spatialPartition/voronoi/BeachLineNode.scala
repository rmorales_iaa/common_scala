/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  15/Dec/2020
 * Time:  13h:40m
 * Description: None
 */
//=============================================================================
package com.common.geometry.spatialPartition.voronoi
//=============================================================================
import scala.annotation.tailrec
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.dataType.tree.binary.printer.TreePrinter.PrintableNode
import com.common.geometry.spatialPartition.voronoi.VoronoiDiagram.SITE_TYPE
//=============================================================================
//leafs nodes: arcs of the beachLine represented by parabolas
//inner nodes: edges (intersections between parabolas) between two arcs
//An edge's right subtree represents all arcs and edges to the right of this edge (considering the x axis).
//=============================================================================
object BeachLineNode {
  //--------------------------------------------------------------
  private var idGenerator : Int = -1
  //--------------------------------------------------------------
  def getNewId = {
    idGenerator += 1
    idGenerator
  }
  //--------------------------------------------------------------
  @tailrec
  private def iterateByLeaf(n: BeachLineNode, useLeftLeaf: Boolean) : BeachLineNode =
    if (n.isInstanceOf[BeachLineNodeArc]) n
    else if (useLeftLeaf) iterateByLeaf(n.left, useLeftLeaf) else iterateByLeaf(n.right, useLeftLeaf)
  //-------------------------------------------------------------------------
  final def iterateByEdge(_n: BeachLineNode, isLeftParent: Boolean) : BeachLineNode = {
    //----------------------------------------------------------------------
    @tailrec
    def get(n: BeachLineNode): BeachLineNode = {
      if ((n == null) || (n.parent == null)) null
      else {
        if (isLeftParent) {
          if (n.parent.left != n) n.parent
          else get(n.parent)
        }
        else {
          if (n.parent.right != n) n.parent
          else get(n.parent)
        }
      }
    }
    //----------------------------------------------------------------------
    get(_n)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import BeachLineNode._
trait BeachLineNode extends PrintableNode {
  //---------------------------------------------------------------------------
  var parent: BeachLineNode = null  //parent edge
  var left:  BeachLineNode    //edge/arc on the left
  var right: BeachLineNode    //edge/arc on the right
  val id = getNewId
  //-------------------------------------------------------------------------
  override def toString = getText
  //-------------------------------------------------------------------------
  def getLeft: PrintableNode = left
  //-------------------------------------------------------------------------
  def getRight: PrintableNode = right
  //-------------------------------------------------------------------------
  //subtitute the oldChild by 'this' node at the parent
  def replaceChildOnParent(oldChild: BeachLineNode) = {
    val oldChildParent = oldChild.parent
    if (oldChildParent == null) parent = null
    else {
      if (oldChildParent.left == oldChild) oldChildParent.left = this
      else oldChildParent.right = this
      parent = oldChildParent
    }
  }
  //-------------------------------------------------------------------------
  def getLeftEdge() = iterateByEdge(this, true).asInstanceOf[BeachLineNodeEdge]
  //-------------------------------------------------------------------------
  def getRightEdge() = iterateByEdge(this, false).asInstanceOf[BeachLineNodeEdge]
  //-------------------------------------------------------------------------
  def getLeftArc() : BeachLineNodeArc = {  //first use right child and then move only using left children
    if (right == null) null
    else iterateByLeaf(right, true).asInstanceOf[BeachLineNodeArc]
  }
  //-------------------------------------------------------------------------
  def getRightArc() : BeachLineNodeArc = {  //first use left child and then move only using right children
    if (left == null) null
    else iterateByLeaf(left, false).asInstanceOf[BeachLineNodeArc]
  }
  //-------------------------------------------------------------------------
  def getText: String =  {
    //-----------------------------------------------------------------------
    def getChildID(n: BeachLineNode) = if (n == null) "-" else n.id.toString
    //-------------------------------------------------------------------------
    this match {
      case a : BeachLineNodeArc => s"arc[$id]->${a.site.toString}" + "{P" + getChildID(parent) + "}"
      case e : BeachLineNodeEdge  =>
         s"edge[$id] + {L${getChildID(left) + " P" + getChildID(parent) + " R" + getChildID(right)}}" +
         s"s${e.calculatedEdge.start}->e" + (if (e.calculatedEdge.isFinished) s"${e.calculatedEdge.end}" else "(NOT_FINISHED)") + s"->d${e.calculatedEdge.direction}"   +
         (if(e.calculatedEdge.hasNeighbour) s"s->N[${e.calculatedEdge.neighbour.start}]" else "->N[]")
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//also known as parabola. Ordered by x axis
sealed case class BeachLineNodeArc(site: SITE_TYPE
                                   , var circleEvent: CircleEvent = null
                                   , var left: BeachLineNode= null
                                   , var right: BeachLineNode = null) extends BeachLineNode
//=============================================================================
//intersections between parabolas . Ordered by x axis
//=============================================================================
sealed case class BeachLineNodeEdge( var left: BeachLineNode
                                   , var right: BeachLineNode
                                   , var calculatedEdge: CalculatedEdge) extends BeachLineNode {
  //---------------------------------------------------------------------------
  left.parent = this
  right.parent = this
  //---------------------------------------------------------------------------
  def this(s: Point2D_Double, l: BeachLineNodeArc, r: BeachLineNodeArc) =
    this(l, r, new CalculatedEdge(s,l.site,r.site))
  //---------------------------------------------------------------------------
  def this(s: Point2D_Double, rightSite: SITE_TYPE, l: BeachLineNodeArc, r: BeachLineNodeEdge) =
    this(l, r, new CalculatedEdge(s,l.site,rightSite))
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file BeachLineNode.scala
//=============================================================================
