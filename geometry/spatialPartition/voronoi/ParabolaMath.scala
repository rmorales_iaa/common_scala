/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  18/Dec/2020
 * Time:  12h:09m
 * Description: None
 */
//=============================================================================
package com.common.geometry.spatialPartition.voronoi
//=============================================================================
import com.common.geometry.point.Point2D
//=============================================================================
//=============================================================================
object ParabolaMath {
  //---------------------------------------------------------------------------
  //https://jacquesheunis.com/post/fortunes-algorithm/
  //Parabola definition : take a point (which we will call the focus) and a straight line
  // (which we will call the directrix). Now if we were to mark all of the points on the plane for which the distance to
  // the focus is the same as the distance to the closest point on the directrix, the resulting set of points would make a parabola.
  //Using maxima (http://maxima.sourceforge.net/):
  //    y_p(x,x_f,y_f,y_d) := ( (x - x_f)^2 / (2*(y_f - y_d))) + ((y_f + y_d) / 2);
  def calculateY_AxisParabola(focus: Point2D, yAxisDirectrix: Double, x:Double) = {
    val xC = x - focus.x
    ((xC * xC) / (2 * (focus.y - yAxisDirectrix))) + ((focus.y + yAxisDirectrix) / 2)
  }
  //---------------------------------------------------------------------------
  //inverse calculation of 'calculateY_AxisParabola'
  //Using maxima (http://maxima.sourceforge.net/):
  // (x-x_f)^2/(2*(y_f-y_d))+(y_f+y_d)/2-y=0
  //[x=x_f-sqrt(-y_f^2+2*y*y_f+y_d^2-2*y*y_d),x=sqrt(-y_f^2+2*y*y_f+y_d^2-2*y*y_d)+x_f]
  def calculateX_AxisParabola(focus: Point2D, yAxisDirectrix: Double, y:Double) = {
    val t = -(focus.y * focus.y) + 2 * y * focus.y + (yAxisDirectrix * yAxisDirectrix) - 2 * y * yAxisDirectrix
    (focus.x - Math.sqrt(t),Math.sqrt(t) + focus.x)
  }

  //---------------------------------------------------------------------------
  //Using maxima (http://maxima.sourceforge.net/) solve and simplify the x axis intersection point of two parabolas
  // There are two  x axis result
  // having the same directrix
  //yp_1(x,xf_1,yf_1,yd) := ( (x - xf_1)^2 / (2*(yf_1 - yd))) + ((yf_1 + yd) / 2);
  //yp_2(x,xf_2,yf_2,yd) := ( (x - xf_2)^2 / (2*(yf_2 - yd))) + ((yf_2 + yd) / 2);
  //yp_1(x,xf_1,yf_1,yd) = yp_2(x,xf_2,yf_2,yd)
  def calculateX_AxisParabolaIntersection(focus_1: Point2D, focus_2: Point2D, yAxisDirectrix: Double) = {
    val xf1 = focus_1.x
    val yf1 = focus_1.y
    val xf2 = focus_2.x
    val yf2 = focus_2.y

    val xf1_2 = xf1 * xf1
    val yf1_2 = yf1 * yf1

    val xf2_2 = xf2 * xf2
    val yf2_2 = yf2 * yf2

    val yf1_3 = yf1_2 * yf1
    val yf2_3 = yf2_2 * yf2

    val yd = yAxisDirectrix
    val yd_2 = yd * yd

    val a = 2 * xf1 * xf2

    val d_1 =
      Math.sqrt((yf1 - yd) * yf2_3 + (-2 * yf1_2 + yd * yf1 + yd_2) * yf2_2 +
        (yf1_3 + yd * yf1_2 + (-2 * yd_2 + xf2_2 - a + xf1_2) * yf1 + (-xf2_2 + a - xf1_2) * yd) *
        yf2 - yd * yf1_3 + yd_2 * yf1_2 +
        (-xf2_2 + a - xf1_2) * yd * yf1 +
        (xf2_2 - a + xf1_2) * yd_2)

    val d_2 = xf1 * yf2 - xf2 * yf1 + (xf2 - xf1) * yd
    val r_1 = (d_1  + d_2) / (yf2 - yf1)
    val r_2 = (-d_1 + d_2) / (yf2 - yf1)
    Seq(r_1,r_2)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ParabolaMaths.scala
//=============================================================================
