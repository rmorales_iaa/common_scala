/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  24/Nov/2020
 * Time:  11h:23m
 * Description:
 * A voronoi diagram is a way of dividing up a space into a set of regions (which we call cells) given a set of input
 * points (which we call sites), such that each cell contains exactly 1 site, and the points inside the cell are exactly
 * those whose nearest site is the one inside that cell.
 * This implementation is based on Furtune algorithm (https://jacquesheunis.com/post/fortunes-algorithm/)
 *  and only is applicable on 2D dimension. It is not valid for list the inner circle for sites forming u
 *  concave polygon (for that case, use 'polyLabel' or a generalized voronoi diagram)
 *
 * One of the algorithms to list the Voronoi algorithm is the Fortune's algorithm
 * Fortune's algorithm works off of the idea of moving a sweepline over the points (sites).
 * As the sweepline moves it adds and removes parabolas to what is called the beachline. The beachline is a sequence of
 * parabolas defined by a focus point and a directrix. The focus is one of the input points and the directrix is the
 * sweepline. The intersections between parabolas in the beachline define points on the Voronoi diagram edges.
 * As the sweepline moves, the parabolas change shape from a ray to wider parabolas eventually approaching a straight
 * line parallel to the sweepline. The changing shape changes the intersection locations of the parabolas in the
 * beachline so that as the sweepline moves the intersections in the beachline trace out the voronoi edges.
 * Eventually some of the parabolas are squeezed (circle event) out of existence by their neighbors leading to the end of an edge and
 * the determination of a vertex in the Voronoi diagram.
 * We consider each site in order and "grow" the cells around each site as we sweep.
 * We only need to keep track of those cells near to the sweep line that are still growing.
 * We will refer to this collection of growing cells as the "beachline".
 * The algorthm is event-based: we maintain a queue of events that need to be processed, arranged in the order that
 * they would be encountered by the sweep line
 * Implmentation based on:
 *  https://github.com/rhololkeolke/EECS-466-Project
 *  https://sites.google.com/site/pointcloudreconstruction/delaunay-triangulation-and-voronoi-diagrams
 *  http://blog.ivank.net/fortunes-algorithm-and-implementation.html
 *  https://jacquesheunis.com/post/fortunes-algorithm/
 *  https://stackoverflow.com/questions/27872964/confusion-on-delaunay-triangulation-and-largest-inscribed-circle
 */
//=============================================================================
package com.common.geometry.spatialPartition.voronoi
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
import com.common.geometry.circle.CircleDouble
import com.common.geometry.point.{Point2D, Point2D_Double}
import com.common.plot.bmpImage.PlotDimensions
//=============================================================================
//=============================================================================
object VoronoiDiagram{
  //---------------------------------------------------------------------------
  type SORTING_AXIS_TYPE = Int
  type SITE_TYPE = Point2D
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//plotBorder defines a plot with the following dimensions:
//  min_x - plotBorder.x
//  min_y - plotBorder.y
//  max_x +  plotBorder.x
//  max_y +  plotBorder.y
//  The values (min_x,min_y,max_x,max_y) are the min and max values of the input site events
//The newplot starts always at (0,0), so an offset must be calculated to place the sites
case class VoronoiDiagram(siteSeq : Array[Point2D], plotBorder: Point2D = Point2D(200,100), debug: Boolean = false) {
  //----------------------------------------------- ----------------------------
  private val beachLine = BeachLine()
  private val eventQueue = EventQueue(debug)
  private val edgeSeq = ArrayBuffer[CalculatedEdge]()
  private val vertexSeq = ArrayBuffer[Point2D_Double]()
  private val pointSeq = ArrayBuffer[Point2D_Double]()
  private var maxInscribedCircle : CircleDouble = null
  //---------------------------------------------------------------------------
  //Create the event queue of events to calculate. Order the points : from top to leftnew SiteEvent(_) )
  siteSeq.foreach{ p => eventQueue.push(new SiteEvent(p)) }
  //---------------------------------------------------------------------------
  private var plotDimensions = calculatePlotDimensions
  calculate
  //---------------------------------------------------------------------------
  def getBeachLineRoot = beachLine.getRoot
  //---------------------------------------------------------------------------
  def getVertexSeq() = vertexSeq
  //---------------------------------------------------------------------------
  def getEdgeSeq() = edgeSeq
  //---------------------------------------------------------------------------
  def getPointSeq() = pointSeq
  //---------------------------------------------------------------------------
  def getPlotDimensions() = plotDimensions
  //---------------------------------------------------------------------------
  def getMaxCircle = {
    if (maxInscribedCircle == null) maxInscribedCircle = calculateMaxInscribedCircle
    maxInscribedCircle
  }
  //---------------------------------------------------------------------------
  private def calculate() = {
    while (!eventQueue.isEmpty) {
      val event = eventQueue.pop
      if (debug) println("DEBUG " + (if(event.isInstanceOf[SiteEvent]) "SiteEvent" else "CircleEvent") + "->"+ event.getPoint)
      event match {
        case s: SiteEvent   => beachLine addArc(s.site, eventQueue, edgeSeq, pointSeq)
        case c: CircleEvent => beachLine removeEdege(c, eventQueue, edgeSeq, pointSeq)
      }
      if (debug) {
        beachLine.printVertical()
        printEdgeSeq
      }
    }

    finishEdgeSeq
    plotDimensions = calculatePlotDimensions
    calculateVertexSeq

    if (debug) {
      println("------------------------------------ Results star --------------------------------------------")
      beachLine.printVertical()
      printPointSeq
      printEdgeSeq
      printVertexSeq
      val c = getMaxCircle
      println(s"----> Max circle: $c  <----")
      println("------------------------------------ Results end ----------------------------------------------")
    }
  }
  //---------------------------------------------------------------------------
  private def	finishEdgeSeq() = {
    finishRemainAtTreeEdge(beachLine.getRoot)
    finishByNeighbour()
  }
  //---------------------------------------------------------------------------
  private def finishRemainAtTreeEdge(n : BeachLineNode) : Unit = {
    val positiveInfinite = plotDimensions.plotMaxX/2d
    val negativeInfinite = -positiveInfinite
    n match {
      case e: BeachLineNodeEdge =>
        val direction = e.calculatedEdge.direction
        val x = if (direction.x > 0) positiveInfinite else negativeInfinite
        val y = e.calculatedEdge.line.getY(x)
        e.calculatedEdge.end = new Point2D_Double(x, y)
        finishRemainAtTreeEdge(n.left)
        finishRemainAtTreeEdge(n.right)
      case _ : BeachLineNodeArc =>
    }
  }
  //---------------------------------------------------------------------------
  private def finishByNeighbour() =
    edgeSeq.foreach { edge=>
      if (edge.neighbour != null) {
        edge.start = edge.neighbour.end
        edge.neighbour = null
      }
    }
  //---------------------------------------------------------------------------
  private def calculateMaxInscribedCircle() = {
    //---------------------------------------------------------------------------
    def findMaxDistanceToSite(vertex: Point2D_Double) =
      (siteSeq map ( site=> vertex.getDistance(Point2D_Double(site)))).min
    //---------------------------------------------------------------------------
    val r =  (vertexSeq map (vertex=> (vertex, findMaxDistanceToSite(vertex)))).sortWith( _._2 > _._2).head
    val c = CircleDouble(r._1,r._2)
    c
  }
  //---------------------------------------------------------------------------
  private def calculateVertexSeq() = {
    val r =  (edgeSeq flatMap { ce =>
      Seq (if (!ce.start.oneComponentIsInfinite && !ce.start.oneComponentIsNan) Some(ce.start) else None,
           if (!ce.end.oneComponentIsInfinite && !ce.end.oneComponentIsNan) Some(ce.end) else None)
    }).flatten.toSet.toArray
    vertexSeq ++= r
  }
  //---------------------------------------------------------------------------
  private def calculatePlotDimensions() = {
    //-------------------------------------------------------------------------
    var minPos = Point2D(Int.MaxValue, Int.MaxValue)
    var macPos = Point2D(Int.MinValue, Int.MinValue)
    val templatePos = Point2D_Double(siteSeq.head) //used when not representable dimension was found
    //---------------------------------------------------------------------------
    def updatePlotMinMaxSite(p: Point2D) = {
      if (p.x < minPos.x) minPos = Point2D(p.x, minPos.y)
      if (p.y < minPos.y) minPos = Point2D(minPos.x, p.y)
      if (p.x > macPos.x) macPos = Point2D(p.x, macPos.y)
      if (p.y > macPos.y) macPos = Point2D(macPos.x, p.y)
    }
    //-------------------------------------------------------------------------
    def getRepresentablePoint(p: Point2D_Double) =
      new Point2D_Double(if (p.x.isInfinite || p.x.isNaN) templatePos.x else p.x,
        if (p.y.isInfinite || p.y.isNaN) templatePos.x else p.y).toPoint2D()
    //-------------------------------------------------------------------------
    //calculate the min and max values of all representable dimensions (not NaNm, not inifinite) of all points
    //append the site sequence
    siteSeq.foreach { p => updatePlotMinMaxSite(p) }

    //append the found lines between edge
    edgeSeq.foreach{ l=>
      updatePlotMinMaxSite(getRepresentablePoint(l.start))
      updatePlotMinMaxSite(getRepresentablePoint(l.end))
    }

    //calculate dimensions
    val plotPointRange = macPos - minPos + Point2D.POINT_ONE //one more to accomdate the full range
    val plotMaxX = plotPointRange.x + (plotBorder.x * 2) //plotMaxX : left and right border
    val plotMaxY = plotPointRange.y + (plotBorder.y * 2) //plotMaxY : top and bottom border
    val plotRange = Point2D(plotMaxX,plotMaxY) + Point2D.POINT_ONE //one more to accomdate the full range
    val centreOffset = (Point2D_Double(plotRange - plotPointRange) * 0.5).toPoint2D
    val plotPointOffset = minPos.inverse + centreOffset  //centre the points

    PlotDimensions(plotPointRange  //Plot range
      , plotPointOffset // Plot offset to centrate the points in the plot (according with borders)
      , plotMaxX
      , plotMaxY
    )
  }
  //---------------------------------------------------------------------------
  def printEdgeSeq() {
    //-------------------------------------------------------------------------
    def getString(d: Double) = {
      val sign = if (d < 0) "-" else "+"
      if (d.isNaN) sign + "nan"
      else if (d.isInfinite) sign + "inf"
      else d.toString
    }
    //-------------------------------------------------------------------------
    println("....... Edge sequence starts.......")
    edgeSeq.foreach { e =>
      val start = e.start
      val end = e.end
      val startText = s"<s(${getString(start.x)},${getString(start.y)}) -> "
      val endText = if (end.allComponentAreNan) "NOT FINISHED" else  s"e(${getString(end.x)},${getString(end.y)})"
      val directionText = s" d${e.direction}]"
      val neighbourText = if (e.hasNeighbour) s" N[${e.neighbour.start}>" else " N[]>"
      println(startText + endText + directionText + neighbourText)
    }
    println("....... Edge sequence ends.......")
  }
  //---------------------------------------------------------------------------
  def printVertexSeq() {
    //-------------------------------------------------------------------------
    println("....... Vertex sequence starts.......")
    vertexSeq.foreach { p => println(p)}
    println("....... Vertex sequence ends.......")
  }
  //---------------------------------------------------------------------------
  def printPointSeq() {
    //-------------------------------------------------------------------------
    println("....... Point sequence starts.......")
    pointSeq.foreach { p => println(p)}
    println("....... Point sequence ends.......")
  }
  //---------------------------------------------------------------------------
  def bechLinePrettyPrint() = beachLine.prettyPrint()
  //---------------------------------------------------------------------------
  def bechLinePrintVertica() = beachLine.printVertical()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file VoronoiDiagram.scala
//=============================================================================
