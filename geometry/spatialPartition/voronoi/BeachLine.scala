/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  20/Feb/2020
 * Time:  20h:28m
 * Description:
 * Beach line is composed by the cells near to the sweep line that are still growing.
 * The beachline is a sequence of parabolas defined by a focus point and a directrix.
 * The focus is one of the input points and the directrix is the sweepline. The intersections between parabolas in the
 * beachline define points on the Voronoi diagram edges.
 * The sweep line is horizontal and moves from top to bottom. For sake of simplicity, the sites (site event) are processed from left to right.
 * The beachline is an ordered collection (u binary seach tree). Arcs and the edges between them are kept ordered by the
 * x-coordinate of where they appear in the beachline.
 *
 * When sweep line moves from top to bottom, the parabolas defined in the beach line grow, squeezing the parabolas defined
 * by certain sites. At certain point those squezed parabolas are fully absorbed, and the related site is added as voronoi edge
 * The "grow" of a parabola (edge) is done in two directions : left and right.
 * The two directions are stored as "euclidean vectors" (https://en.wikipedia.org/wiki/Euclidean_vector) with a starting
 * point and other point indicating the way of the direction
 *
 * The type of trees used to store the beach line are not a formal binary search tree becuase the processing of
 * "site event" implies that the arc (node) splitted is replicated in two diferents place of the tree,
 * breaking the bianry tree order
 *
 * Implementation based on:
 *  http://blog.ivank.net/fortunes-algorithm-and-implementation.html
 *  https://sites.google.com/site/pointcloudreconstruction/delaunay-triangulation-and-voronoi-diagrams
 *  https://jacquesheunis.com/post/fortunes-algorithm/
 */

//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.geometry.spatialPartition.voronoi
//=============================================================================
import scala.annotation.tailrec
import scala.collection.mutable.ArrayBuffer
//=============================================================================
import com.common.geometry.point.{Point2D, Point2D_Double}
import com.common.geometry.spatialPartition.voronoi.ParabolaMath._
import com.common.dataType.tree.binary.printer.TreePrinter
import com.common.geometry.spatialPartition.voronoi.VoronoiDiagram.SITE_TYPE
//=============================================================================
case class Segment(start : Point2D_Double, end : Point2D_Double) {
  //---------------------------------------------------------------------------
  override def toString = s"<${start} -> ${end}>"
  //---------------------------------------------------------------------------
  def this(a: Point2D, b: Point2D) = this(Point2D_Double(a),Point2D_Double(b))
  //---------------------------------------------------------------------------
  def toPoint2dSeq = Seq(start.toPoint2D,end.toPoint2D)
  //---------------------------------------------------------------------------
  def toSeq = Seq(start,end)
  //---------------------------------------------------------------------------
}
//=============================================================================
object BeachLine {
  //---------------------------------------------------------------------------
  def getEdgeIntersection(eA: BeachLineNodeEdge, eB: BeachLineNodeEdge): Option[Point2D_Double] = {

    val x = (eB.calculatedEdge.line.c - eA.calculatedEdge.line.c) / (eA.calculatedEdge.line.a - eB.calculatedEdge.line.a)
    val y = eA.calculatedEdge.line.getY(x)
    if(x.isNaN || x.isInfinite) return None
    if(y.isNaN || x.isInfinite) return None

    val startA = eA.calculatedEdge.start
    val startB = eB.calculatedEdge.start

    val directionA = eA.calculatedEdge.direction
    val directionB = eB.calculatedEdge.direction

    if((x - startA.x) / directionA.x < 0) return None
    if((y - startA.y) / directionA.y < 0) return None

    if((x - startB.x) / directionB.x < 0) return None
    if((y - startB.y) / directionB.y < 0) return None

    Some(new Point2D_Double(x,y))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//it a typical binary seach tree using only min value of the nodes
import BeachLine._
case class BeachLine() {
  //---------------------------------------------------------------------------
  protected var root: BeachLineNode = null
  //---------------------------------------------------------------------------
  def getRoot = root
  //---------------------------------------------------------------------------
  // root is an arc and the NEW site has the save y axis
  private def addArcAsSpecialCase(newSite : SITE_TYPE, edgeStorage: ArrayBuffer[CalculatedEdge]): Boolean = {
    val rootSite = root.asInstanceOf[BeachLineNodeArc].site
    val sameY_Axis = (rootSite.y - newSite.y) == 0
    if (sameY_Axis) {
      val arcA = BeachLineNodeArc(rootSite)
      val arcB = BeachLineNodeArc(newSite)
      val start = new Point2D_Double((newSite.x + rootSite.x) / 2d, Double.PositiveInfinity) //star point withs x axis middle of (rootSite,newSite)
      val newEdge = if (newSite.x > rootSite.x) new BeachLineNodeEdge(start, arcA, arcB)
      else new BeachLineNodeEdge(start, arcB, arcA)
      root = newEdge
      edgeStorage += newEdge.calculatedEdge
      true
    }
    else false
  }

  //---------------------------------------------------------------------------
  private def removeFalseCircleEvent(eventQueue: EventQueue, a: BeachLineNodeArc) : Unit = {
    if((a != null) && (a.circleEvent != null)) {
      eventQueue.remove(a.circleEvent)
      a.circleEvent = null
    }
  }
  //---------------------------------------------------------------------------
  //also known as 'append site', 'append parabola' or 'site event'
  def addArc(newSite : SITE_TYPE
             , eventQueue: EventQueue
             , edgeStorage: ArrayBuffer[CalculatedEdge]
             , pointSeq : ArrayBuffer[Point2D_Double]) : Unit = {
    if (root == null)  root = BeachLineNodeArc(newSite)
    else{
      if (root.isInstanceOf[BeachLineNodeArc] && addArcAsSpecialCase(newSite,edgeStorage)) return  //special case: root is an arc and the NEW site has the save y axis

      //set the directrix
      val directrix = newSite.y

      //get the arc under the NEW site
      val arcToBeSplitted = findArcThatInterserctsWithX(newSite.x, directrix)
      val siteToBeSplitted = arcToBeSplitted.site

      //remove from event queue the circle event if it exists
      if (arcToBeSplitted.circleEvent != null) removeFalseCircleEvent(eventQueue,arcToBeSplitted)

      //calculate the intersection of the NEW site (vertical ray) and the 'arcToBeSplitted'
      val intersectionPoint = new Point2D_Double(newSite.x,calculateY_AxisParabola(siteToBeSplitted, directrix, newSite.x))
      pointSeq += intersectionPoint

      //create the splited arcs: just duplicate the site, keepin the newSite(that generates an arc) in middle
      //the NEW tree structure is:
      // splittedArc_1 newPArent newArc(newSiteArc) edgeRight splittedArc_2
      val splittedArc_1 = BeachLineNodeArc(siteToBeSplitted) // it grows to the left out of the intersection point
      val newSiteArc = BeachLineNodeArc(newSite)
      val splittedArc_2 = BeachLineNodeArc(siteToBeSplitted) // it grows to the right out of the intersection point

      //create two NEW edges keeping the 'newSite' in middle of the spllited arcs. Right node will be the right child os left node
      val edgeRight = new BeachLineNodeEdge(intersectionPoint, newSiteArc, splittedArc_2)
      val newParent = new BeachLineNodeEdge(intersectionPoint, newSite, splittedArc_1, edgeRight)

      //connected the edges created by the arc splitting to be finish ath the end of the algorithm
      newParent.calculatedEdge.neighbour = edgeRight.calculatedEdge

      //fix the parent of the right node
      newParent.replaceChildOnParent(arcToBeSplitted)

      //append to storage
      edgeStorage += newParent.calculatedEdge

      //update root if it was splitted
      if (arcToBeSplitted == root) root = newParent

      //check if the splitted arcs create circle envents
      val circleEventSeq = Seq(getCircleEvent(splittedArc_1, directrix),getCircleEvent(splittedArc_2, directrix)).flatten
      circleEventSeq foreach (eventQueue.push(_))
    }
  }
  //---------------------------------------------------------------------------
  //also known as 'parabola intersectiom' or 'parabola squeeze'
  def removeEdege(circleEvent : CircleEvent
                  , eventQueue: EventQueue
                  , edgeStorage: ArrayBuffer[CalculatedEdge]
                  , pointSeq : ArrayBuffer[Point2D_Double]) = {
    //---------------------------------------------------------------------------
    val squeezedArc = circleEvent.squeezedArc //arc to be removed. It is in the millde of 'leftArc' and 'rightArc'
    val leftEdge = squeezedArc.getLeftEdge
    val rightEdge = squeezedArc.getRightEdge
    val leftArc = leftEdge.getRightArc
    val rightArc = rightEdge.getLeftArc

    //remove false circle event
    removeFalseCircleEvent(eventQueue,leftArc)
    removeFalseCircleEvent(eventQueue,rightArc)

    //circumcenter between left_arc's site, given arc's site and right_arc's site
    val endPoint = circleEvent.intersection
    pointSeq += endPoint

    //fix end points
    leftEdge.calculatedEdge.end = endPoint
    rightEdge.calculatedEdge.end = endPoint

    //process a CalculatedEdge starting at 'endPoint'
    val newCalculatedEdge = new CalculatedEdge(endPoint, leftArc.site, rightArc.site)

    //append to storage
    edgeStorage += newCalculatedEdge

    // set the 'newCalculatedEdge' to the higer node (between leftEdge and leftEdge) in the tree
    val higherNode = getHigherNode(squeezedArc, leftEdge, rightEdge)
    higherNode.calculatedEdge = newCalculatedEdge

    //remove arc
    val parent = squeezedArc.parent
    val grandParent = parent.parent
    if (parent.left == squeezedArc) {
       parent.right.parent = grandParent
       if(grandParent.left  == parent) grandParent.left = parent.right
       if(grandParent.right == parent) grandParent.right = parent.right
    }
    else{
      parent.left.parent = grandParent
      if(grandParent.left  == parent) grandParent.left = parent.left
      if(grandParent.right == parent) grandParent.right = parent.left
    }

    //check if the left and rigtht arcs create circle envents
    val circleEventSeq = Seq(getCircleEvent(leftArc, circleEvent.yAxis),getCircleEvent(rightArc, circleEvent.yAxis)).flatten
    circleEventSeq foreach (eventQueue.push(_))
  }
  //---------------------------------------------------------------------------
  protected def findArcThatInterserctsWithX(x: Double, directrix: Double): BeachLineNodeArc ={
    //-------------------------------------------------------------------------
    @tailrec
    def find(n: BeachLineNode): BeachLineNodeArc =
      n match {
        case arc: BeachLineNodeArc => arc
        case edge: BeachLineNodeEdge =>
          val leftArc = edge.getRightArc()
          val rightArc= edge.getLeftArc()
          val xPosSeq = calculateX_AxisParabolaIntersection(leftArc.site, rightArc.site, directrix)
          val xIntersection = if (leftArc.site.y < rightArc.site.y) xPosSeq.max else xPosSeq.min
          if (xIntersection > x) find(n.left)
          else find(n.right)
      }
    //-------------------------------------------------------------------------
    find(root)
  }
  //---------------------------------------------------------------------------
  protected def getCircleEvent(arc: BeachLineNodeArc, directrix: Double) : Option[CircleEvent] = {
    //get and check parents
    val leftEdge = arc.getLeftEdge()
    if (leftEdge == null) return None
    val rightEdge = arc.getRightEdge()
    if (rightEdge == null) return None

    //get and check children
    val leftArc = leftEdge.getRightArc()
    if (leftArc == null) return None
    val rigArc = rightEdge.getLeftArc()
    if (rigArc == null) return None
    if (leftArc.site == rigArc.site) return None

    //check edge intersection
    val edgeIntersection = getEdgeIntersection(leftEdge,rightEdge)
    if(edgeIntersection.isEmpty) return None
    val circleCentreOffset = Point2D_Double(arc.site) - edgeIntersection.get
    val circleRadius = circleCentreOffset.magnitude
    if ((edgeIntersection.get.y - circleRadius) >= directrix) return None
    val circleEventY_Axis = edgeIntersection.get.y - circleRadius
    val circleEvent = CircleEvent(edgeIntersection.get,circleEventY_Axis, arc)
    arc.circleEvent = circleEvent
    Some(circleEvent)
  }
  //---------------------------------------------------------------------------
  final def getHigherNode(_n: BeachLineNode, leftNode: BeachLineNode, rightNode: BeachLineNode) : BeachLineNodeEdge = {
    //-------------------------------------------------------------------------
    var higerParent : BeachLineNode = null
    //-------------------------------------------------------------------------
    @tailrec
    def get(n: BeachLineNode) : Unit = {
      if (n != root) {
        val nParent = n.parent
        if(nParent == leftNode) higerParent = leftNode
        if(nParent == rightNode) higerParent = rightNode
        get(nParent)
      }
    }
    //-------------------------------------------------------------------------
    get(_n.parent)
    higerParent.asInstanceOf[BeachLineNodeEdge]
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def prettyPrint(n: BeachLineNode = root, tabSpaceCount: Int = 1): Unit = {
    println(s"------------------ Beach line starts ------------------")
    if (n == null) println("----Empty tree----")
    else TreePrinter.print(n, tabSpaceCount)
    println(s"------------------ Beach line ends ------------------")
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def printVertical(n: BeachLineNode = root): Unit = {
    println(s"------------------ Beach line starts ------------------")
    if (n == null) println("----Empty tree----")
    else TreePrinter.printVertical("",n,false)
    println(s"------------------ Beach line ends ------------------")
    //-------------------------------------------------------------------------
  }
}
//=============================================================================
//End of file BeachLine.scala
//=============================================================================
