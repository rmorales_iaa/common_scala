/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  14/Jan/2021
 * Time:  14h:12m
 * Description: AVL tree that allows duplicates. The bottom left child has the minor key.
 * Duplicates event items are ordered by x coordinate (minor to mayor)
 */
//=============================================================================
package com.common.geometry.spatialPartition.voronoi
//=============================================================================
import com.common.dataType.tree.binary.avl.AVL_TreeWithDuplicates
//=============================================================================
//=============================================================================
case class EventQueue(debug: Boolean = false) {
  //---------------------------------------------------------------------------
  private val avlTree = AVL_TreeWithDuplicates[EventItem]()
  //---------------------------------------------------------------------------
  def size = avlTree.size
  //---------------------------------------------------------------------------
  def push (e: EventItem) : Unit = avlTree += (e.yAxis,e)
  //---------------------------------------------------------------------------
  def pop() = {
    val e = avlTree.getFirstRightLeaf.get.sortWith(_.getPoint.x > _.getPoint.x).head
    avlTree -= e.yAxis
    e
  }
  //---------------------------------------------------------------------------
  def remove(e: EventItem) = {
    avlTree -= e.yAxis
    if (debug) println("FALSE CircleEvent ->" + e.asInstanceOf[CircleEvent].squeezedArc.site)
  }
  //---------------------------------------------------------------------------
  def prettyPrint(): Unit = avlTree.prettyPrint()
  //---------------------------------------------------------------------------
  def print(): Unit =
    avlTree.inorOrder().foreach { a=> println(a.mkString("("+","+")"))}
  //---------------------------------------------------------------------------
  def isEmpty() = size == 0
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file EventQueue.scala
//=============================================================================
