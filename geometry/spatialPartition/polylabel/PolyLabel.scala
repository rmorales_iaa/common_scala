/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  18/Jan/2021
 * Time:  23h:54m
 * Description: It calcalutes (by brute forcing) the centroid of the inner circle
 * of a pplynomy concave or convex
 * Based on:
 * https://github.com/mapbox/polylabel
 * https://github.com/FreshLlamanade/polylabel-java
 */
//=============================================================================
package com.common.geometry.spatialPartition.polylabel
//=============================================================================
import com.common.geometry.point

import scala.collection.mutable
//=============================================================================
import com.common.geometry.circle.CircleDouble
import com.common.geometry.point.Point2D_Double
import com.common.geometry.polygon.Polygon2D
//=============================================================================
//=============================================================================
case class PolyLabel(poly: Polygon2D, precision: Double = 0.001) {
  //----------------------------------------------------------------------------
  private val (polyMinPos,polyMaxPos,polyWidth,polyHeight) = poly.getMinMaxWidthHeightPos()
  private val cellSize = Math.min(polyWidth, polyHeight)
  private val cellHalfSize = cellSize / 2
  private val cellQueue = mutable.PriorityQueue[Cell]()
  populateCellQueue()
  val circle =  processCellQueue()
  //----------------------------------------------------------------------------
  private def populateCellQueue() = {
    val minX = polyMinPos.x
    val maxX = polyMaxPos.x
    val minY = polyMinPos.y
    val maxY = polyMaxPos.y
    var x = minX
    var y = minY

    while (x < maxX) {
      while (y < maxY) {
        cellQueue.enqueue(Cell(new Point2D_Double(x + cellHalfSize, y + cellHalfSize), cellHalfSize, poly))
        y += cellSize
      }
      x += cellSize
    }
  }
  //----------------------------------------------------------------------------
  private def processCellQueue() : CircleDouble = {
    if (cellSize == 0) return CircleDouble(polyMinPos, 0)
    var bestCell = getCentroidCell

    //special case for rectangular polygons
    val rectangularCell = Cell(new point.Point2D_Double(polyMinPos.x + polyWidth/2, polyMinPos.y + polyHeight/2), 0, poly)
    if (rectangularCell.distanceToPolygonCentroid > bestCell.distanceToPolygonCentroid) bestCell = rectangularCell

    //calculate the queue
    while(!cellQueue.isEmpty){
      val cell = cellQueue.dequeue()
      if (cell.distanceToPolygonCentroid > bestCell.distanceToPolygonCentroid) bestCell = cell

      //do not drill down further if there's no chance of a better solution
      if (cell.maxDistanceToPolygon - bestCell.distanceToPolygonCentroid > precision) {
        val halfSize2 = cell.halfSize / 2
        //split cell into 4 cells
        cellQueue.enqueue(Cell(cell.centre - halfSize2, halfSize2, poly))
        cellQueue.enqueue(Cell(new Point2D_Double(cell.centre.x + halfSize2,cell.centre.y - halfSize2), halfSize2, poly))
        cellQueue.enqueue(Cell(new Point2D_Double(cell.centre.x - halfSize2,cell.centre.y + halfSize2), halfSize2, poly))
        cellQueue.enqueue(Cell(cell.centre + halfSize2, halfSize2, poly))
      }
    }
    CircleDouble(bestCell.centre, bestCell.distanceToPolygonCentroid)
  }
  //----------------------------------------------------------------------------
  private def getCentroidCell() = {
    var x = 0d
    var y = 0d
    var area = 0d
    val seq = poly.getVertexSeq
    (seq zip (seq.last +: seq.dropRight(1))) foreach { p =>
      val a0 = p._1.x
      val a1 = p._1.y
      val b0 = p._2.x
      val b1 = p._2.y
      val diff = a0 * b1 - b0 * a1
      x +=  (a0 + b0) * diff
      y += (a1 + b1) * diff
      area += diff * 3
    }
    if (area == 0) Cell(seq(0), 0, poly)
    else Cell(new Point2D_Double(x/area, y/area), 0, poly)
  }
  //----------------------------------------------------------------------------
}
//=============================================================================
//End of file PolyLabel.scala
//=============================================================================
