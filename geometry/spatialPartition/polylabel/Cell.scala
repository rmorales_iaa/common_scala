/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  18/Jan/2021
 * Time:  23h:59m
 * Description: None
 */
//=============================================================================
package com.common.geometry.spatialPartition.polylabel
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.geometry.polygon.Polygon2D
//=============================================================================
//=============================================================================
case class Cell(centre:Point2D_Double, halfSize: Double, poly: Polygon2D) extends Comparable[Cell]{
  //---------------------------------------------------------------------------
  val distanceToPolygonCentroid = pointToPolygonDist// distance from cell center to polygon
  val maxDistanceToPolygon = distanceToPolygonCentroid + halfSize * Math.sqrt(2d) // max distance to polygon within a cell
  //---------------------------------------------------------------------------
  private def pointToPolygonDist() = {
    var inside = false
    var minDistSq = Double.MaxValue
    val xc = centre.x
    val yc = centre.y
    val seq = poly.getVertexSeq
    ((seq :+ seq.head) zip (seq.drop(1) :+ seq.head)) foreach { p =>
      val current = p._1
      val prev = p._2
      if ((current.y > yc != prev.y > yc) && (xc < (prev.x - current.x) * (yc - current.y) / (prev.y - current.y) + current.x))
        inside = !inside
      val distSq = getSquaredDistanceFromPointToSegment(current, prev)
      if (distSq < minDistSq) minDistSq = distSq
    }
    val d =  Math.sqrt(minDistSq)
    if (minDistSq == 0) 0
    else if (inside) d else -d
  }
  //---------------------------------------------------------------------------
  // get squared distance from a point to a segment
  private def getSquaredDistanceFromPointToSegment(pA: Point2D_Double, pB: Point2D_Double) = {
    val px = centre.x
    val py = centre.y
    var x = pA.x
    var y = pA.y
    var dx = pB.x - x
    var dy = pB.y - y
    if (dx != 0 || dy != 0) {
      val t = ((px - x) * dx + (py - y) * dy) / (dx * dx + dy * dy)
      if (t > 1) {
        x = pB.x
        y = pB.y
      }
      else if (t > 0) {
        x += dx * t
        y += dy * t
      }
    }
    dx = px - x
    dy = py - y
    dx * dx + dy * dy
  }
  //---------------------------------------------------------------------------
  def compareTo(b : Cell) = maxDistanceToPolygon compare b.maxDistanceToPolygon
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Cell.scala
//=============================================================================9
