/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Apr/2020
 * Time:  14h:02m
 * Description: None
 */
//=============================================================================
package com.common.geometry.triangle
//=============================================================================
//=============================================================================
import java.util.concurrent.atomic.AtomicInteger
//=============================================================================
import com.common.geometry.point.Point2D_Float
//=============================================================================
//=============================================================================
object TriangleFloat{
  //---------------------------------------------------------------------------
  private val id = new AtomicInteger(0)
  //---------------------------------------------------------------------------
  def getNewID: Int = id.addAndGet(1)
  //---------------------------------------------------------------------------
  def getCurrentID: Int = id.get
  //---------------------------------------------------------------------------
  def apply(l: List[Point2D_Float]) : TriangleFloat = this(l(0),l(1),l(2))
  //--------------------------------------------------------------------------
  def isValid(a: Point2D_Float, b: Point2D_Float, c: Point2D_Float) = getArea(a,b,c) != 0
  //---------------------------------------------------------------------------
  def getArea(a: Point2D_Float, b: Point2D_Float, c: Point2D_Float) =
    Math.abs(
     (a.x * (b.y - c.y) +
      b.x * (c.y - a.y) +
      c.x * (a.y - b.y)) / 2.0)
  //---------------------------------------------------------------------------
}
//=============================================================================
import TriangleFloat._
case class TriangleFloat(a: Point2D_Float
                         , b: Point2D_Float
                         , c: Point2D_Float
                         , id: Int = TriangleFloat.getNewID) {
  //---------------------------------------------------------------------------
  assert(isValid(a,b,c), s"The input points u=$a, b=$b c=$c does not conform a a valid triangle")
  //---------------------------------------------------------------------------
  private val abLength = ( a subAbs b).getDistance
  private val acLength = ( a subAbs c).getDistance
  private val bcLength = ( b subAbs c).getDistance
  //---------------------------------------------------------------------------
  private var invariant : TriangleInvariant = _
  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/2049582/how-to-determine-if-a-point-is-in-a-2d-triangle
  def isIn(p: Point2D_Float) : Boolean  = {
    val s = a.y * c.x - a.x * c.y + (c.y - a.y) * p.x + (a.x - c.x) * p.y
    val t = a.x * b.y - a.y * b.x + (a.y - b.y) * p.x + (b.x - a.x) * p.y
    if ((s < 0) != (t < 0)) return false
    val A = -b.y * c.x + a.y * (c.x - b.x) + a.x * (b.y - c.y) + b.x * c.y
    if (A < 0) s <= 0 && s + t >= A
    else s >= 0 && s + t <= A
  }
  //---------------------------------------------------------------------------
  //see "Astroalign: A Python module for astronomical image registration" M. Beroiz, J.B. Cabral, B. Sánchez
  def getInvariant(v: Int = -1) = {
    if (invariant == null){
      val l = Seq(abLength,acLength,bcLength).sorted
      invariant = TriangleInvariant(l(2) / l(1), l(1) / l(0), v)
    }
    invariant
  }
  //---------------------------------------------------------------------------
  def getCentroid() = Point2D_Float(Math.round((a.x + b.x + c.x) / 3.0).toInt, Math.round((a.y + b.y + c.y) / 3.0 ).toInt)
  //---------------------------------------------------------------------------
  def toArray = Array(a.toArray ++ b.toArray ++ c.toArray).flatten
  //-----------------------------------------------------------------------------------------------------------------
  def existVertex(p : Point2D_Float) = toPointSeq.indexOf(p) != -1
  //---------------------------------------------------------------------------
  //Return a vertex ordered (w,z,t) that is same after a rotation, scale or translation
  // w is the vertex defined by L1 & L2
  // z is the vertex defined by L2 & L3
  // t is the vertex defined by L3 & L1
  // and L1 < L2 < L3 are the sides of the triangle
  def getSortedVertexSeq = {
    //-----------------------------------------------------------------------------------------------------------------
    def findCommonVertex(seqA: Seq[Point2D_Float], seqB: Seq[Point2D_Float]) = {
      if (seqB.indexOf(seqA.head) != -1) seqA.head
      else seqA.last
    }
    //-----------------------------------------------------------------------------------------------------------------
    val sortedSeq = Seq((Seq(a,b),abLength), (Seq(a,c),acLength), (Seq(b,c),bcLength)).sortWith( _._2 < _._2)
    val w = findCommonVertex(sortedSeq(0)._1, sortedSeq(1)._1)
    val z = findCommonVertex(sortedSeq(1)._1, sortedSeq(2)._1)
    val t = findCommonVertex(sortedSeq(2)._1, sortedSeq(0)._1)
    Seq(w,z,t)
  }
  //---------------------------------------------------------------------------
  def toPointSeq = Array(a,b,c)
  //---------------------------------------------------------------------------
  def getMinCoordinate() = Point2D_Float.getMin(toPointSeq)
  //---------------------------------------------------------------------------
  def getMaxCoordinate() = Point2D_Float.getMax(toPointSeq)
  //---------------------------------------------------------------------------
  def getArea = TriangleFloat.getArea(a,b,c)
  //---------------------------------------------------------------------------
  override def toString = s"u($a) b($b) c($c) "
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file TriangleFloat.scala
//=============================================================================