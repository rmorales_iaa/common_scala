/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Apr/2020
 * Time:  05h:06m
 * Description: None
 */
//=============================================================================
package com.common.geometry.triangle
//=============================================================================
//=============================================================================
object TriangleInvariant {
  //---------------------------------------------------------------------------
  final val FLOAT_COMPARISION_MAX_ALLOWED_MARGIN = 0.0000001f
  //---------------------------------------------------------------------------
}
//=============================================================================
import TriangleInvariant._
import com.common.geometry.point.Point2D_Float
case class TriangleInvariant(xRatio: Float, yRatio: Float, v: Int) {
  //---------------------------------------------------------------------------
  def getAsSequence= Seq(xRatio,yRatio)
  //---------------------------------------------------------------------------
  def getAsPonint2D = Point2D_Float(xRatio,yRatio)
  //---------------------------------------------------------------------------
  def equal(t: TriangleInvariant): Boolean = {
    (Math.abs(xRatio - t.xRatio) < FLOAT_COMPARISION_MAX_ALLOWED_MARGIN) &&
    (Math.abs(yRatio - t.yRatio) < FLOAT_COMPARISION_MAX_ALLOWED_MARGIN)
  }
  //---------------------------------------------------------------------------
  def isIn(seq: Seq[TriangleInvariant]) : Boolean = {
    seq.foreach(ti => if (this equal ti) return true)
    false
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file TriangleInvariant.scala
//=============================================================================