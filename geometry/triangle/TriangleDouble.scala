/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Apr/2020
 * Time:  14h:02m
 * Description: adapted from: https://github.com/locationtech/jts/blob/master/modules/core/src/main/java/org/locationtech/jts/geom/Triangle.java
 */
//=============================================================================
package com.common.geometry.triangle
//=============================================================================
//=============================================================================
import java.util.concurrent.atomic.AtomicInteger

import com.common.geometry.angle.Angle
import com.common.geometry.circle.CircleDouble
import com.common.plot.maxima.MaximaPlot
//=============================================================================
import com.common.geometry.point.Point2D_Double
//=============================================================================
object TriangleDouble {
  //---------------------------------------------------------------------------
  private val id = new AtomicInteger(0)
  //---------------------------------------------------------------------------
  def getNewID: Int = id.addAndGet(1)
  //---------------------------------------------------------------------------
  def getCurrentID: Int = id.get
  //---------------------------------------------------------------------------
  def apply(l: List[Point2D_Double]) : TriangleDouble = this(l(0),l(1),l(2))
  //---------------------------------------------------------------------------
  def apply(a: Array[Point2D_Double]) : TriangleDouble = this(a(0),a(1),a(2))
  //--------------------------------------------------------------------------
  def isValid(a: Point2D_Double, b: Point2D_Double, c: Point2D_Double) = getArea(a,b,c) != 0
  //--------------------------------------------------------------------------
  def getArea(a: Point2D_Double, b: Point2D_Double, c: Point2D_Double) =
    Math.abs(
      (a.x * (b.y - c.y) +
       b.x * (c.y - a.y) +
       c.x * (a.y - b.y)) / 2.0)
  //---------------------------------------------------------------------------
  private case class HCoordinate(x: Double, y: Double, w: Double) {
    def this(p1: HCoordinate, p2: HCoordinate) =
      this(p1.y * p2.w - p2.y * p1.w,p2.x * p1.w - p1.x * p2.w, p1.x * p2.y - p2.x * p1.y)
  }
  //---------------------------------------------------------------------------
  def getPerpendicularBisector(a: Point2D_Double, b: Point2D_Double) = {
    val dx = b.x - a.x
    val dy = b.y - a.y

    val p1 = new Point2D_Double(a.x + dx / 2.0, a.y + dy / 2.0)
    val p1w = 1

    val p2 = new Point2D_Double(a.x - dy + dx / 2.0, a.y + dx + dy / 2.0)
    val p2w = 1

    (p1.y * p2w - p2.y * p1w,p2.x * p1w - p1.x * p2w, p1.x * p2.y - p2.x * p1.y)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
import TriangleDouble._
case class TriangleDouble(a: Point2D_Double, b: Point2D_Double, c: Point2D_Double) {
  //---------------------------------------------------------------------------
  val id = TriangleDouble.getNewID
  //---------------------------------------------------------------------------
  assert(isValid(a,b,c), s"The input points u=$a, b=$b c=$c does not conform a a valid triangle")
  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/2049582/how-to-determine-if-a-point-is-in-a-2d-triangle
  def isIn(p: Point2D_Double) : Boolean  = {
    val s = a.y * c.x - a.x * c.y + (c.y - a.y) * p.x + (a.x - c.x) * p.y
    val t = a.x * b.y - a.y * b.x + (a.y - b.y) * p.x + (b.x - a.x) * p.y
    if ((s < 0) != (t < 0)) return false
    val A = -b.y * c.x + a.y * (c.x - b.x) + a.x * (b.y - c.y) + b.x * c.y
    if (A < 0) s <= 0 && s + t >= A
    else s >= 0 && s + t <= A
  }
  //---------------------------------------------------------------------------
  def getLongestSide () =
    Array(a.getDistance(b),  b.getDistance(c), c.getDistance(a)).max
  //---------------------------------------------------------------------------
  //computes the point at which the bisector of the angle ABC cuts the segment AC.
  def angleBisector() = {
    val len0 = b.getDistance(a)
    val len2 = b.getDistance(c)
    val frac = len0 / (len0 + len2)
    val dx = c.x - a.x
    val dy = c.y - a.y
    new Point2D_Double(a.x + frac * dx, a.y + frac * dy)
  }
  //---------------------------------------------------------------------------
  //center of its inscribed circle
  def getInCentre() = {
    val len0 = b.getDistance(c)
    val len1 = a.getDistance(c)
    val len2 = a.getDistance(b)
    val circum = len0 + len1 + len2
    new Point2D_Double((len0 * a.x + len1 * b.x + len2 * c.x) / circum, (len0 * a.y + len1 * b.y + len2 * c.y) / circum)
  }
  //---------------------------------------------------------------------------
  //center of the circle that contains all the vertices of the polygon
  def getCircumCentre(): Unit = {
    //-------------------------------------------------------------------------
    def det(m00 : Double, m01 : Double, m10 : Double, m11 : Double) = m00 * m11 - m01 * m10
    //-------------------------------------------------------------------------
    val cx = c.x
    val cy = c.y
    val ax = a.x - cx
    val ay = a.y - cy
    val bx = b.x - cx
    val by = b.y - cy

    val denom = 2 * det(ax, ay, bx, by)
    val numx = det(ay, ax * ax + ay * ay, by, bx * bx + by * by)
    val numy = det(ax, ax * ax + ay * ay, bx, bx * bx + by * by)

    val ccx = cx - numx / denom
    val ccy = cy + numy / denom

    new Point2D_Double(ccx, ccy)
  }
  //---------------------------------------------------------------------------
  def getCentroid() = Point2D_Double(Math.round((a.x + b.x + c.x) / 3.0).toInt, Math.round((a.y + b.y + c.y) / 3.0 ).toInt)
  //---------------------------------------------------------------------------
  def getSemiPerimeterArea() = {
    val len0 = b.getDistance(c)
    val len1 = a.getDistance(c)
    val len2 = a.getDistance(b)
    (len0 + len1 + len2) /  2
  }
  //---------------------------------------------------------------------------
  //https://math.stackexchange.com/questions/1582061/finding-the-radius-of-a-circle-inside-of-a-triangle
  def getInCentreRadius() = getArea / getSemiPerimeterArea
  //---------------------------------------------------------------------------
  def getInCentreCircle() = CircleDouble(getInCentre, getInCentreRadius)
  //---------------------------------------------------------------------------
  def getArea = TriangleDouble.getArea(a,b,c)
  //---------------------------------------------------------------------------
  def toPointSeq = Array(a,b,c)
  //---------------------------------------------------------------------------
  def getMinCoordinate() = Point2D_Double.getMin(toPointSeq)
  //---------------------------------------------------------------------------
  def getMaxCoordinate() = Point2D_Double.getMax(toPointSeq)
  //---------------------------------------------------------------------------
  def isAcute(a: Nothing, b: Nothing, c: Nothing): Boolean = {
    if (!Angle.isAcute(a, b, c)) return false
    if (!Angle.isAcute(b, c, a)) return false
    if (!Angle.isAcute(c, a, b)) return false
    true
  }
  //---------------------------------------------------------------------------
  def isObtuse(a: Nothing, b: Nothing, c: Nothing): Boolean = {
    if (!Angle.isObtuse(a, b, c)) return false
    if (!Angle.isObtuse(b, c, a)) return false
    if (!Angle.isObtuse(c, a, b)) return false
    true
  }
  //---------------------------------------------------------------------------
  override def toString = s"u($a) b($b) c($c) "
  //---------------------------------------------------------------------------
  def getMaximaPlot2D() =
    MaximaPlot.getPlot2D_Triangle(Point2D_Double.toSeq(toPointSeq))
  //---------------------------------------------------------------------------
  def getMaximaPlot2D_TriangleInnerCircle()  =
    MaximaPlot.getPlot2D_TriangleInnerCircle(this)
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file TriangleDouble.scala
//=============================================================================