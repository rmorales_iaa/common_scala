/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Nov/2020
 * Time:  11h:54m
 * Description: None
 */
//=============================================================================
package com.common.geometry.polygon
//=============================================================================
import com.common.geometry.point.{Point2D, Point2D_Double}
import com.common.geometry.spatialPartition.delauny.Delaunay
import com.common.geometry.spatialPartition.polylabel.PolyLabel
import com.common.geometry.spatialPartition.voronoi.VoronoiDiagram
import com.common.plot.bmpImage.{BmpImagePlot, PlotDimensions}
import com.common.plot.maxima.MaximaPlot
//=============================================================================
import scala.collection.mutable.ArrayBuffer
import java.awt.Color
import java.awt.image.BufferedImage
//=============================================================================
//=============================================================================
object Polygon2D {
  //--------------------------------------------------------------------------
  //first and last point must be equal to close the polygon
  def apply(seq: Array[Point2D]) : Polygon2D = {
    val poly = Polygon2D()
    val pSeq = (seq map ( p=> Point2D_Double( p ))).toList
    poly += pSeq
    if (!poly.isClosed) poly += pSeq.head
    poly
  }
  //--------------------------------------------------------------------------
  //first and last point must be equal to close the polygon
  def apply(pSeq: List[Point2D_Double]): Polygon2D = {
    val poly = new Polygon2D()
    poly += pSeq
    if (!poly.isClosed) poly += pSeq.head
    poly
  }
  //--------------------------------------------------------------------------
  //first and last point must be equal to close the polygon
  def apply(pSeq: Array[Point2D_Double]) : Polygon2D = {
    val poly = new Polygon2D()
    poly += pSeq
    if (!poly.isClosed) poly += pSeq.head
    poly
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Polygon2D() {
  //---------------------------------------------------------------------------
  private val vertexSeq : ArrayBuffer[Point2D_Double] = ArrayBuffer[Point2D_Double]()
  //---------------------------------------------------------------------------
  def += (p: Point2D_Double) = vertexSeq += p
  //---------------------------------------------------------------------------
  def += (pseq: List[Point2D_Double]) = vertexSeq ++= pseq
  //---------------------------------------------------------------------------
  def += (pseq: Array[Point2D_Double]) = vertexSeq ++= pseq
  //---------------------------------------------------------------------------
  def getMinMaxWidthHeightPos() = {
    val (minPos,maxPos) = getMinMaxPos()
    val width = maxPos.x - minPos.x
    val height = maxPos.y - minPos.y
    (minPos,maxPos,width,height)
  }
  //---------------------------------------------------------------------------
  def isClosed = if (vertexSeq.size == 1) false else vertexSeq.head == vertexSeq.last
  //---------------------------------------------------------------------------
  def getVertexCount() = vertexSeq.length
  //---------------------------------------------------------------------------
  def getVertexSeq = vertexSeq.toArray
  //---------------------------------------------------------------------------
  def getVertexSeqAsPoint2D = (vertexSeq map ( _.toPoint2D)).toArray
  //---------------------------------------------------------------------------
  def getMinMaxPos() = {
    //-------------------------------------------------------------------------
    var minPos = Point2D_Double(Int.MaxValue, Int.MaxValue)
    var maxPos = Point2D_Double(Int.MinValue, Int.MinValue)
    //---------------------------------------------------------------------------
    def updateMinMax(p: Point2D_Double) = {
      if (p.x < minPos.x) minPos = new Point2D_Double(p.x, minPos.y)
      if (p.y < minPos.y) minPos = new Point2D_Double(minPos.x, p.y)
      if (p.x > maxPos.x) maxPos = new Point2D_Double(p.x, maxPos.y)
      if (p.y > maxPos.y) maxPos = new Point2D_Double(maxPos.x, p.y)
    }
    //---------------------------------------------------------------------------
    vertexSeq foreach( updateMinMax(_) )
    (minPos,maxPos)
    //---------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //https://www.mathopenref.com/coordpolygonarea.html
  def getArea() = {
    val n = vertexSeq.length
    var signedArea: Double = 0
    for (i <- 0 until n) {
      val p0 = vertexSeq(i)
      val p1 = vertexSeq((i + 1) % n)
      // Calculate value of A using shoelace formula
      val A = (p0.x * p1.y) - (p1.x * p0.y)
      signedArea += A
    }
    Math.abs(signedArea * 0.5)
  }
  //---------------------------------------------------------------------------
  //Points are numbered in clockwise order
  def getCentroid() = {
    val ans = new Array[Double](2)
    val n = vertexSeq.length
    var signedArea : Double = 0
    for (i <- 0 until n) {
      val p0 = vertexSeq(i)
      val p1 = vertexSeq((i + 1) % n)
      // Calculate value of A using shoelace formula
      val A = (p0.x * p1.y) - (p1.x * p0.y)
      signedArea += A
      // Calculating coordinates of centroid of polygon
      ans(0) += (p0.x + p1.x) * A
      ans(1) += (p0.y + p1.y) * A
    }
    signedArea = signedArea * 0.5
    new Point2D_Double(ans(0) / (6 * signedArea),ans(1) / (6 * signedArea))
  }
  //---------------------------------------------------------------------------
  //http://www.sunshine2k.de/coding/java/Polygon/Convex/polygon.htm
  def isConvex() : Boolean = {
    if (vertexSeq.length < 3) return false
    val n = vertexSeq.length
    var res = 0d
    for (i <- 0 until n) {
      val p0 = vertexSeq(i)
      val p1 = vertexSeq((i + 1) % n)
      val v = p1 - p0
      val u = vertexSeq((i + 2) % n)
      if (i == 0) { // in first loop direction is unknown, so save it in var 'res'polygon

        res = u.x * v.y - u.y * v.x + v.x * p0.y - v.y * p0.x
      }
      else {
        val newres = u.x * v.y - u.y * v.x + v.x * p0.y - v.y * p0.x
        if ((newres > 0 && res < 0) || (newres < 0 && res > 0)) return false
      }
    }
    true
  }
  //---------------------------------------------------------------------------
  def isConcave() : Boolean = !isConvex()
  //---------------------------------------------------------------------------
  def getTriangulation() = {
    Delaunay.calculate(vertexSeq.toList).map { t =>
      Polygon2D( Array (
          new Point2D_Double(vertexSeq(t._1).x,vertexSeq(t._1).y)
        , new Point2D_Double(vertexSeq(t._2).x,vertexSeq(t._2).y)
        , new Point2D_Double(vertexSeq(t._3).x,vertexSeq(t._3).y)
      ))
    }.toArray
  }
  //---------------------------------------------------------------------------
  def getTriangulationPlot() = {
    val polVertexSeq = Array(vertexSeq.toArray)
    val triangleVertexSeq = getTriangulation() map (t=> t.vertexSeq.toArray)
    val pointSeq = polVertexSeq ++ triangleVertexSeq
    MaximaPlot.getPlotPolynomy2D(pointSeq)
  }
  //---------------------------------------------------------------------------
  def getMaximaPlot() =
    MaximaPlot.getPlotPolynomy2D(Array(vertexSeq.toArray))
  //---------------------------------------------------------------------------
  def getVoronoiDiagram() = VoronoiDiagram(getVertexSeqAsPoint2D)
  //---------------------------------------------------------------------------
  def getMaxInnerCircle() = PolyLabel(this).circle
  //---------------------------------------------------------------------------
  def saveAsImage(
    filename: String
    , imageFormat: String = "png"
    , plotType: Int = BufferedImage.TYPE_INT_RGB
    , plotBorder: Point2D = Point2D(200,100)
    , pointColor: Color = Color.MAGENTA
    , inscribedCircleColor: Color = Color.LIGHT_GRAY
    , segmentColor: Color = Color.ORANGE) = {

    val vSeq = getVertexSeqAsPoint2D
    val plotDimensions = PlotDimensions(vSeq, plotBorder)
    val plot = BmpImagePlot(plotDimensions.plotMaxX, plotDimensions.plotMaxY, plotDimensions.plotPointOffset, plotType)

    //plot inscribed circle
    plot.circle(getMaxInnerCircle, Some(inscribedCircleColor), Some(inscribedCircleColor))

    //plot a line between all vertex
    val seq = vSeq :+ vSeq.head
    seq.sliding(2) foreach{ s=> plot.segment(s(0), s(1), Some(segmentColor), Some(segmentColor)) }

    //plot vertex
    vSeq foreach ( plot.site(_, c = pointColor) )

    //save
    plot.save(filename, imageFormat)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Polygon2D.scala
//=============================================================================
