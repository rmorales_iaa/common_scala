/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/may/2019
 * Time:  13h:26m
 * Description: two dimension points of doubles
 */
//=============================================================================
package com.common.geometry.point
//=============================================================================
object Point2D_Double {
  //---------------------------------------------------------------------------
  //Data type used to store the coordinates of the nodes.
  type CoordinateType = Double
  //---------------------------------------------------------------------------
  final val DOUBLE_COMPARISION_MAX_ALLOWED_MARGIN = 10e-10
  //---------------------------------------------------------------------------
  final val POINT_ZERO = new  Point2D_Double(0d, 0d)
  final val POINT_ONE = new Point2D_Double(1d, 1d)
  final val POINT_INVALID = new Point2D_Double(-1,-1)
  final val POINT_NAN = new Point2D_Double(Double.NaN, Double.NaN)
  //---------------------------------------------------------------------------
  def apply(x: Int, y: Int): Point2D_Double = new Point2D_Double(x.toDouble, y.toDouble)
  //---------------------------------------------------------------------------
  def apply(seq: (Double,Double)): Point2D_Double = new Point2D_Double(seq._1, seq._2)
  //---------------------------------------------------------------------------
  def apply(seq: Seq[Double]): Point2D_Double = new Point2D_Double(seq(0), seq(1))
  //---------------------------------------------------------------------------
  def apply(p: Point2D): Point2D_Double = Point2D_Double(p.x, p.y)
  //---------------------------------------------------------------------------
  def toSeq(seq : Seq[Point2D_Double]) = (seq flatMap (p=> Seq(p.x,p.y))).toArray
  //---------------------------------------------------------------------------
  def getMin(seq: Array[Point2D_Double]): Point2D_Double = {
    var minX = seq(0).x
    var minY = seq(0).y
    seq drop 1 foreach { p =>
      minX = Math.min(minX, p.x)
      minY = Math.min(minY, p.y)
    }
    new Point2D_Double(minX, minY)
  }
  //---------------------------------------------------------------------------
  def getMax(seq: Array[Point2D_Double]): Point2D_Double = {
    var maxX = seq(0).x
    var maxY = seq(0).y
    seq drop 1 foreach { p =>
      maxX = Math.max(maxX, p.x)
      maxY = Math.max(maxY, p.y)
    }
    new Point2D_Double(maxX, maxY)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import Point2D_Double._
import com.common.coordinate.conversion.Conversion.{toPreciseDegrees, toPreciseRadians}
import com.common.geometry.point.Point2D_Double.CoordinateType
case class Point2D_Double(x:CoordinateType, y: CoordinateType) {
  //---------------------------------------------------------------------------
  def this(p: Point2D_Double, xPixSize: CoordinateType, yPixSize: CoordinateType) = this(p.x + xPixSize, p.y + yPixSize)
  //--------------------------------------------------------------------------
  def + (p: Point2D_Double) = new Point2D_Double(x + p.x, y + p.y)
  //--------------------------------------------------------------------------
  def + (v:CoordinateType) = new Point2D_Double(x + v, y + v)
  //--------------------------------------------------------------------------
  def - (p: Point2D_Double) = new Point2D_Double(x - p.x, y - p.y)
  //--------------------------------------------------------------------------
  def - (v:CoordinateType) = new Point2D_Double(x - v, y - v)
  //--------------------------------------------------------------------------
  def * (v: CoordinateType) = new Point2D_Double(x * v, y * v)
  //--------------------------------------------------------------------------
  def * (p: Point2D_Double) = new Point2D_Double(x * p.x, y * p.y)
  //--------------------------------------------------------------------------
  def / (p: Point2D_Double) = new Point2D_Double(x/p.x, y/p.y)
  //--------------------------------------------------------------------------
  def / (v: CoordinateType) = new Point2D_Double(x/v, y/v)
  //--------------------------------------------------------------------------
  def abs() = new Point2D_Double(Math.abs(x), Math.abs(y))
  //--------------------------------------------------------------------------
  def == (p: Point2D_Double) = ( Math.abs(x - p.x) < DOUBLE_COMPARISION_MAX_ALLOWED_MARGIN) &&
                               ( Math.abs(y - p.y) < DOUBLE_COMPARISION_MAX_ALLOWED_MARGIN)
  //--------------------------------------------------------------------------
  def != (p: Point2D_Double) = !(this == p)
  //--------------------------------------------------------------------------
  def < (p: Point2D_Double) = getDistance() < p.getDistance()
  //--------------------------------------------------------------------------
  def <= (p: Point2D_Double) = getDistance() <= p.getDistance()
  //--------------------------------------------------------------------------
  def > (p: Point2D_Double) = getDistance() > p.getDistance()
  //--------------------------------------------------------------------------
  def >= (p: Point2D_Double) = getDistance() >= p.getDistance()
  //--------------------------------------------------------------------------
  def compare(p: Point2D_Double) = getDistance().compare(p.getDistance())
  //--------------------------------------------------------------------------
  def fixCosDec = Point2D_Double(x * Math.cos(Math.toRadians(y)), y)
  //--------------------------------------------------------------------------
  def toArcSec = Point2D_Double(x * 3600, y * 3600)
  //--------------------------------------------------------------------------
  //both 'a' and 'this' in decimal degrees
  //'this' is the reference
   def getResidualMas(other: Point2D_Double): Point2D_Double =
     (other.toArcSec - toArcSec).fixCosDec * 1000
  //--------------------------------------------------------------------------
  def add(v: CoordinateType = 1) = new Point2D_Double(x+v, y+v)
  //--------------------------------------------------------------------------
  def addX(v: CoordinateType = 1) = new Point2D_Double(x+v, y)
  //--------------------------------------------------------------------------
  def addY(v: CoordinateType = 1) = new Point2D_Double(x, y+v)
  //--------------------------------------------------------------------------
  def subAbs (p: Point2D_Double) = new Point2D_Double(Math.abs(x - p.x), Math.abs(y - p.y))
  //--------------------------------------------------------------------------
  def subX(v: CoordinateType = 1) = new Point2D_Double(x-v, y)
  //--------------------------------------------------------------------------
  def subY(v: CoordinateType = 1) = new Point2D_Double(x, y-v)
  //--------------------------------------------------------------------------
  def subtractMinZero(p: Point2D_Double) = new Point2D_Double( Math.max(0,x - p.x), Math.max(0,y - p.y) )
  //--------------------------------------------------------------------------
  def inverse = new Point2D_Double(-x, -y)
  //--------------------------------------------------------------------------
  def inverseX = new Point2D_Double(-x, y)
  //--------------------------------------------------------------------------
  def inverseY = new Point2D_Double(x, -y)
  //--------------------------------------------------------------------------
  def reverse = new Point2D_Double(y, x)
  //--------------------------------------------------------------------------
  def someNegative = (x < 0) || (y < 0)
  //--------------------------------------------------------------------------
  def allPositive = !someNegative
  //---------------------------------------------------------------------------
  def isClosedAs(p: Point2D_Double, pixelDistance: CoordinateType = 1) =
    (Math.abs(x - p.x) <= pixelDistance)  &&  (Math.abs(y - p.y) <= pixelDistance)
  //---------------------------------------------------------------------------
  def toSequence() = Seq(x,y)
  //---------------------------------------------------------------------------
  def toArray() = Array(x,y)
  //---------------------------------------------------------------------------
  def toArrayDouble() = Array(x,y)
  //---------------------------------------------------------------------------
  def toPoint2D() = Point2D(Math.round(x).toInt, Math.round(y).toInt)
  //---------------------------------------------------------------------------
  def toRadians() = Point2D_Double(toPreciseRadians(x), toPreciseRadians(y))
  //---------------------------------------------------------------------------
  def toDegrees() = Point2D_Double(toPreciseDegrees(x), toPreciseDegrees(y))
  //---------------------------------------------------------------------------
  def toPoint2D_Float() = Point2D_Float(x.toFloat, y.toFloat)
  //---------------------------------------------------------------------------
  def getDistance() = Math.sqrt( (x * x) + (y * y) )
  //---------------------------------------------------------------------------
  def getDistance(p: Point2D_Double) = {
    val a = x - p.x
    val b = y - p.y
    Math.sqrt( (a * a) + (b * b))
  }
  //---------------------------------------------------------------------------
  def getAngle(p: Point2D_Double) = Math.atan2(p.y - y , p.x - x)
  //---------------------------------------------------------------------------
  def getSlope(p: Point2D_Double) = (p.y - y) / (p.x - x)  //tangent of the angle
  //---------------------------------------------------------------------------
  override def toString = s"($x,$y)"
  //---------------------------------------------------------------------------
  def getFormattedShortString() = s"(${f"$x%.3f"}, ${f"$y%.3f"})"
  //---------------------------------------------------------------------------
  def getFormattedString() = s"(${f"$x%10.6f"},${f"$y%10.6f"})"
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
  def getDistanceToLine(p1: Point2D_Double, p2: Point2D_Double) = {
    val dx = p2.x - p1.x
    val dy = p2.y - p1.y
    Math.abs( ((dy * x) - (dx * y) + (p2.x * p1.y) - (p2.y * p1.x)) /
      Math.sqrt( (dy * dy) + (dx * dx))
    )
  }
  //---------------------------------------------------------------------------
  def oneComponentIsNan() = x.isNaN || y.isNaN
  //---------------------------------------------------------------------------
  def allComponentAreNan() = x.isNaN && y.isNaN
  //---------------------------------------------------------------------------
  def oneComponentIsInfinite() = x.isInfinite || y.isInfinite
  //---------------------------------------------------------------------------
  def allComponentAreInfinite() = x.isInfinite && y.isInfinite
  //---------------------------------------------------------------------------
  //https://github.com/caente/convex-hull/blob/master/src/main/scala/com/miguel/GrahamScanScala.scala
  def goesLeft(p0: Point2D_Double, p1: Point2D_Double): Boolean =
    (p1.x - p0.x) * (this.y - p0.y) - (this.x - p0.x) * (p1.y - p0.y) > 0
  //---------------------------------------------------------------------------
  def magnitude() = Math.sqrt((x * x)  + (y * y))
  //---------------------------------------------------------------------------
  def normalize() = {
    val m = magnitude()
    new Point2D_Double(x / m, y / m)
  }
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Point2D_Double.scala
//=============================================================================