/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/may/2019
 * Time:  13h:26m
 * Description: two dimension points of integers
 */
//=============================================================================
package com.common.geometry.point
//=============================================================================
import com.common.geometry.point.Point2D.CoordinateType
//=============================================================================
//=============================================================================
object Point2D{
  //---------------------------------------------------------------------------
  //Data type used to store the coordinates of the nodes.
  type CoordinateType = Int
  //---------------------------------------------------------------------------
  final val POINT_ZERO = Point2D(0,0)
  final val POINT_ONE =  Point2D(1,1)
  final val POINT_INVALID =  Point2D(-1,-1)
  //---------------------------------------------------------------------------
  def apply(t:(Int,Int)) : Point2D = Point2D(t._1,t._2)
  //---------------------------------------------------------------------------
  def apply(seq : Seq[Int]) : Point2D = Point2D(seq(0),seq(1))
  //---------------------------------------------------------------------------
  def apply(s: String): Point2D = {
    val seq = s
      .trim
      .drop(1)
      .dropRight(1)
      .split(",")
    Point2D(seq.head.toInt, seq.last.toInt)
  }
  //---------------------------------------------------------------------------
  def getMin(seq : Seq[Point2D]) : Point2D = {
    var minX = seq(0).x
    var minY = seq(0).y
    seq drop 1  foreach { p=>
      minX = Math.min(minX,p.x)
      minY = Math.min(minY,p.y)
    }
    Point2D(minX, minY)
  }
  //---------------------------------------------------------------------------
  def getMax(seq : Seq[Point2D]) : Point2D = {
    var maxX = seq(0).x
    var maxY = seq(0).y
    seq drop 1 foreach { p=>
      maxX = Math.max(maxX,p.x)
      maxY = Math.max(maxY,p.y)
    }
    Point2D(maxX, maxY)
  }
  //---------------------------------------------------------------------------
  def getMinMax(seq: Seq[Point2D]): (Point2D,Point2D) = {
    var maxX = seq(0).x
    var maxY = seq(0).y
    var minX = seq(0).x
    var minY = seq(0).y
    seq drop 1 foreach { p =>
      minX = Math.min(minX, p.x)
      minY = Math.min(minY, p.y)
      maxX = Math.max(maxX, p.x)
      maxY = Math.max(maxY, p.y)
    }
    (Point2D(minX, minY),Point2D(maxX, maxY))
  }
  //---------------------------------------------------------------------------
  def getAverage(seq: Seq[Point2D]) = {
    val pos = seq.size / 2
    val sortedAxisX = seq.map(_.x).sorted
    val sortedAxisY = seq.map(_.y).sorted
    Point2D(sortedAxisX(pos)
           , sortedAxisY(pos))
  }
  //---------------------------------------------------------------------------
  def toSeq(seq : Seq[Point2D]) = (seq flatMap (p=> Seq(p.x,p.y))).toArray
  //---------------------------------------------------------------------------
  //the axis partition begin from (0,0) = botton-left to top-right
  //'width' is the size in x axis. If 'width' is not a divisor of xRange, then remain submatrix at border are generated
  //'height' is the size in y axis. If 'height' is not a divisor of yRange, then remain submatrix at border are generated
  //return : partiton seq
  def getAxisPartitionSeq(imageWidth: Int
                          , imageHeight: Int
                          , subImageWidth: Int
                          , subImageHeight: Int): Array[(Point2D, Point2D)] = {

    val xMaxIndex = Math.max(0, imageWidth - 1)
    val yMaxIndex = Math.max(0, imageHeight - 1)

    require(subImageWidth > 0, s" 'width' must be greater than 0. Current value: $subImageWidth")
    require(subImageHeight > 0, s" 'height' must be greater than 0. Current value: $subImageHeight")
    require(subImageWidth <= imageWidth, s" 'width' must be <= than xRange: $subImageWidth. Current value: $subImageWidth")
    require(subImageHeight <= imageHeight, s" 'height' must be <= than yRange: $imageHeight. Current value: $subImageHeight")
    //---------------------------------------------------------------------------
    val xSliceCount = imageWidth / subImageWidth
    val widthPixelRemain = imageWidth - (xSliceCount * subImageWidth)

    val ySliceCount = imageHeight / subImageHeight
    val heightPixelRemain = imageHeight - (ySliceCount * subImageHeight)

    //build the remain subMatrix using the remain pixels in X-axis (right)
    val xSubMatrixAxisRemain =
      if (widthPixelRemain == 0) None
      else Some((Point2D(xMaxIndex - widthPixelRemain + 1, 0)
        , Point2D(xMaxIndex, yMaxIndex)))

    //build the subMatrix using the remain pixels in Y-axis (top left)
    val ySubMatrixAxisRemain =
      if (heightPixelRemain == 0) None
      else Some((Point2D(0, yMaxIndex - heightPixelRemain + 1)
        , Point2D(xMaxIndex - widthPixelRemain, yMaxIndex)))

    //main subMatrix
    val mainSubMatrixAxisSeq = (for (ySlice <- 0 until ySliceCount) yield
      for (xSlice <- 0 until xSliceCount) yield {
        val xMin = xSlice * subImageWidth
        val yMin = ySlice * subImageHeight
        (Point2D(xMin, yMin)
          , Point2D(xMin + subImageWidth - 1, yMin + subImageHeight - 1))
      }).flatten.toArray

    val remainPartitonSeq = Seq(xSubMatrixAxisRemain ++
      ySubMatrixAxisRemain).flatten.toArray
    mainSubMatrixAxisSeq ++ remainPartitonSeq
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Point2D(x: CoordinateType, y: CoordinateType) {
  //---------------------------------------------------------------------------
  def this(p: Point2D, xPixSize: CoordinateType, yPixSize: CoordinateType) = this(p.x + xPixSize, p.y + yPixSize)
  //--------------------------------------------------------------------------
  def + (p: Point2D) = Point2D(x + p.x, y + p.y)
  //--------------------------------------------------------------------------
  def + (v:CoordinateType) = new Point2D(x + v, y + v)
  //--------------------------------------------------------------------------
  def - (p: Point2D) = Point2D(x - p.x, y - p.y)
  //--------------------------------------------------------------------------
  def - (v:CoordinateType) = new Point2D(x - v, y - v)
  //--------------------------------------------------------------------------
  def * (v: CoordinateType) = Point2D(x * v, y * v)
  //--------------------------------------------------------------------------
  def * (v: Float) = Point2D(Math.round(x * v), Math.round(y * v))
  //--------------------------------------------------------------------------
  def * (p: Point2D) = Point2D(x * p.x, y * p.y)
  //--------------------------------------------------------------------------
  def / (p: Point2D) = new Point2D(x/p.x, y/p.y)
  //--------------------------------------------------------------------------
  def / (v: CoordinateType) = new Point2D(x/v, y/v)
  //--------------------------------------------------------------------------
  def abs() = Point2D(Math.abs(x), Math.abs(y))
  //--------------------------------------------------------------------------
  def == (p: Point2D) = (x == p.x) && (y == p.y)
  //--------------------------------------------------------------------------
  def equals(p: Point2D): Boolean =
    (x == p.x) && (y == p.y)
  //--------------------------------------------------------------------------
  def != (p: Point2D) = !(this == p)
  //--------------------------------------------------------------------------
  def < (p: Point2D) = getDistance() < p.getDistance()
  //--------------------------------------------------------------------------
  def <= (p: Point2D) = getDistance() <= p.getDistance()
  //--------------------------------------------------------------------------
  def > (p: Point2D) = getDistance() > p.getDistance()
  //--------------------------------------------------------------------------
  def >= (p: Point2D) = getDistance() >= p.getDistance()
  //--------------------------------------------------------------------------
  def compare(p: Point2D) = getDistance().compare(p.getDistance())
  //--------------------------------------------------------------------------
  def fixCosDec = Point2D(Math.round(x * Math.cos(Math.toRadians(y))).toInt, y)
  //--------------------------------------------------------------------------
  def toArcSec = Point2D_Double(x * 3600, y * 3600)
  //--------------------------------------------------------------------------
  //both 'a' and 'this' in decimal degrees
  //'this' is the reference
  def getResidualMas(a: Point2D_Double) = {
    val ref = toArcSec
    val aArcSec = a.toArcSec
    (aArcSec - ref).fixCosDec * 1000
  }
  //--------------------------------------------------------------------------
  def add(v: CoordinateType = 1) = Point2D(x+v, y+v)
  //--------------------------------------------------------------------------
  def addX(v: CoordinateType = 1) = Point2D(x+v, y)
  //--------------------------------------------------------------------------
  def addY(v: CoordinateType = 1) = Point2D(x, y+v)
  //--------------------------------------------------------------------------
  def subX(v: CoordinateType = 1) = Point2D(x-v, y)
  //--------------------------------------------------------------------------
  def subAbs (p: Point2D_Float) = Point2D_Float(Math.abs(x - p.x), Math.abs(y - p.y))
  //--------------------------------------------------------------------------
  def subY(v: CoordinateType = 1) = Point2D(x, y-v)
  //--------------------------------------------------------------------------
  def subtractMinZero(p: Point2D) = Point2D( Math.max(0,x - p.x), Math.max(0,y - p.y) )
  //--------------------------------------------------------------------------
  def inverse = Point2D(-x, -y)
  //--------------------------------------------------------------------------
  def inverseX = new Point2D_Double(-x, y)
  //--------------------------------------------------------------------------
  def inverseY = new Point2D_Double(x, -y)
  //--------------------------------------------------------------------------
  def reverse = Point2D(y, x)
  //--------------------------------------------------------------------------
  def someNegative = (x < 0) || (y < 0)
  //--------------------------------------------------------------------------
  def allPositive = !someNegative
  //---------------------------------------------------------------------------
  def isClosedAs(p: Point2D, pixelDistance: CoordinateType = 1) =
    (Math.abs(x - p.x) <= pixelDistance)  &&  (Math.abs(y - p.y) <= pixelDistance)
  //---------------------------------------------------------------------------
  def toSequence() = Seq(x,y)
  //---------------------------------------------------------------------------
  def toArray() = Array(x,y)
  //---------------------------------------------------------------------------
  def getDistance() = Math.sqrt( (x * x) + (y * y)).toFloat
  //---------------------------------------------------------------------------
  def getDistance(p: Point2D) ={
    val a = x - p.x
    val b = y - p.y
    Math.sqrt( (a * a) + (b * b)).toFloat
  }
  //---------------------------------------------------------------------------
  def getAngle(p: Point2D) = Math.atan2(p.y - y , p.x - x)  //same as tan-1
  //---------------------------------------------------------------------------
  def getSlope(p: Point2D) = (p.y - y.toFloat) / (p.x - x)
  //---------------------------------------------------------------------------
  override def toString = s"($x,$y)"
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
  def getDistanceToLine(p1: Point2D, p2: Point2D) = {
    val dx = p2.x - p1.x
    val dy = p2.y - p1.y
    Math.abs( dx * (p1.y - y) - (p1.x - x) * dy ).toFloat / Math.sqrt( (dx * dx) + (dy * dy) ).toFloat
  }
  //---------------------------------------------------------------------------
  //https://github.com/caente/convex-hull/blob/master/src/main/scala/com/miguel/GrahamScanScala.scala
  def goesLeft(p0: Point2D, p1: Point2D): Boolean =
    (p1.x - p0.x) * (this.y - p0.y) - (this.x - p0.x) * (p1.y - p0.y) > 0
  //---------------------------------------------------------------------------
  def magnitude() = Math.sqrt((x * x)  + (y * y))
  //---------------------------------------------------------------------------
  def normalize() = {
    val m = magnitude()
    Point2D(Math.round(x / m).toInt, Math.round(y / m).toInt)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file point.Point2D.scala
//=============================================================================