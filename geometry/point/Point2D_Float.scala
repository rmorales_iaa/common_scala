/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/may/2019
 * Time:  13h:26m
 * Description: two dimension points of floats
 */
//=============================================================================
package com.common.geometry.point
//=============================================================================
//=============================================================================
object Point2D_Float {
  //---------------------------------------------------------------------------
  //Data type used to store the coordinates of the nodes.
  type CoordinateType = Float
  //---------------------------------------------------------------------------
  final val FLOAT_COMPARISION_MAX_ALLOWED_MARGIN = 0.0000001f
  //---------------------------------------------------------------------------
  final val POINT_ZERO = Point2D_Float(0f, 0f)
  final val POINT_ONE = Point2D_Float(1f, 1f)
  final val POINT_INVALID = Point2D_Float(-1f, -1f)
  //---------------------------------------------------------------------------
  def apply(x: Double, y: Double) : Point2D_Float = Point2D_Float(x.toFloat,y.toFloat)
  //---------------------------------------------------------------------------
  def apply(x: Int, y: Int) : Point2D_Float = Point2D_Float(x.toFloat,y.toFloat)
  //---------------------------------------------------------------------------
  def apply(seq : Seq[Float]) : Point2D_Float = Point2D_Float(seq(0),seq(1))
  //---------------------------------------------------------------------------
  def apply(p: Point2D) : Point2D_Float = Point2D_Float(p.x,p.y)
  //---------------------------------------------------------------------------
  def toSeq(seq : Seq[Point2D_Float]) = (seq flatMap (p=> Seq(p.x,p.y))).toArray
  //---------------------------------------------------------------------------
  def getMin(seq : Seq[Point2D_Float]) : Point2D_Float = {
    var minX = seq(0).x
    var minY = seq(0).y
    seq drop 1  foreach { p=>
      minX = Math.min(minX,p.x)
      minY = Math.min(minY,p.y)
    }
    Point2D_Float(minX, minY)
  }
  //---------------------------------------------------------------------------
  def getMax(seq : Seq[Point2D_Float]) : Point2D_Float = {
    var maxX = seq(0).x
    var maxY = seq(0).y
    seq drop 1 foreach { p=>
      maxX = Math.max(maxX,p.x)
      maxY = Math.max(maxY,p.y)
    }
    Point2D_Float(maxX, maxY)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import Point2D_Float._
import com.common.geometry.point.Point2D_Float.CoordinateType
case class Point2D_Float(x: CoordinateType, y: CoordinateType) {
  //---------------------------------------------------------------------------
  def this(p: Point2D_Float, xPixSize: CoordinateType, yPixSize: CoordinateType) = this(p.x + xPixSize, p.y + yPixSize)
  //--------------------------------------------------------------------------
  def + (p: Point2D_Float) = Point2D_Float(x + p.x, y + p.y)
  //--------------------------------------------------------------------------
  def + (v:CoordinateType) = new Point2D_Float(x + v, y + v)
  //--------------------------------------------------------------------------
  def - (p: Point2D_Float) = Point2D_Float(x - p.x, y - p.y)
  //--------------------------------------------------------------------------
  def - (v:CoordinateType) = new Point2D_Float(x - v, y - v)
  //--------------------------------------------------------------------------
  def * (v: CoordinateType) = Point2D_Float(x * v, y * v)
  //--------------------------------------------------------------------------
  def * (p: Point2D_Float) = Point2D_Float(x * p.x, y * p.y)
  //--------------------------------------------------------------------------
  def / (p: Point2D_Float) = new Point2D_Float(x/p.x, y/p.y)
  //--------------------------------------------------------------------------
  def / (v: CoordinateType) = new Point2D_Float(x/v, y/v)
  //--------------------------------------------------------------------------
  def abs() = Point2D_Float(Math.abs(x), Math.abs(y))
  //--------------------------------------------------------------------------
  def == (p: Point2D_Float) = ( Math.abs(x - p.x) < FLOAT_COMPARISION_MAX_ALLOWED_MARGIN) &&
                               ( Math.abs(y - p.y) < FLOAT_COMPARISION_MAX_ALLOWED_MARGIN)
  //--------------------------------------------------------------------------
  def != (p: Point2D_Float) = !(this == p)
  //--------------------------------------------------------------------------
  def < (p: Point2D_Float) = getDistance() < p.getDistance()
  //--------------------------------------------------------------------------
  def <= (p: Point2D_Float) = getDistance() <= p.getDistance()
  //--------------------------------------------------------------------------
  def > (p: Point2D_Float) = getDistance() > p.getDistance()
  //--------------------------------------------------------------------------
  def >= (p: Point2D_Float) = getDistance() >= p.getDistance()
  //--------------------------------------------------------------------------
  def compare(p: Point2D_Float) = getDistance().compare(p.getDistance())
  //--------------------------------------------------------------------------
  def fixCosDec = Point2D_Float(x * Math.cos(Math.toRadians(y)), y)
  //--------------------------------------------------------------------------
  def toArcSec = Point2D_Double(x * 3600, y * 3600)
  //--------------------------------------------------------------------------
  //both 'a' and 'this' in decimal degrees
  //'this' is the reference
  def getResidualMas(a: Point2D_Double) = {
    val ref = toArcSec
    val aArcSec = a.toArcSec
    (aArcSec - ref).fixCosDec * 1000
  }
  //--------------------------------------------------------------------------
  def add(v: CoordinateType = 1) = Point2D_Float(x+v, y+v)
  //--------------------------------------------------------------------------
  def addX(v: CoordinateType = 1) = Point2D_Float(x+v, y)
  //--------------------------------------------------------------------------
  def addY(v: CoordinateType = 1) = Point2D_Float(x, y+v)
  //--------------------------------------------------------------------------
  def subAbs (p: Point2D_Float) = Point2D_Float(Math.abs(x - p.x), Math.abs(y - p.y))
  //--------------------------------------------------------------------------
  def subX(v: CoordinateType = 1) = Point2D_Float(x-v, y)
  //--------------------------------------------------------------------------
  def subY(v: CoordinateType = 1) = Point2D_Float(x, y-v)
  //--------------------------------------------------------------------------
  def subtractMinZero(p: Point2D_Float) = Point2D_Float( Math.max(0,x - p.x), Math.max(0,y - p.y) )
  //--------------------------------------------------------------------------
  def inverse = Point2D_Float(-x, -y)
  //--------------------------------------------------------------------------
  def inverseX = new Point2D_Double(-x, y)
  //--------------------------------------------------------------------------
  def inverseY = new Point2D_Double(x, -y)
  //--------------------------------------------------------------------------
  def reverse = Point2D_Float(y, x)
  //--------------------------------------------------------------------------
  def someNegative = (x < 0) || (y < 0)
  //--------------------------------------------------------------------------
  def allPositive = !someNegative
  //---------------------------------------------------------------------------
  def isClosedAs(p: Point2D_Float, pixelDistance: CoordinateType = 1) =
    (Math.abs(x - p.x) <= pixelDistance)  &&  (Math.abs(y - p.y) <= pixelDistance)
  //---------------------------------------------------------------------------
  def toSequence() = Seq(x,y)
  //---------------------------------------------------------------------------
  def toArray() = Array(x,y)
  //---------------------------------------------------------------------------
  def toArrayDouble() = Array(x.toDouble,y.toDouble)
  //---------------------------------------------------------------------------
  def toPoint2D() = Point2D(Math.round(x), Math.round(y))
  //---------------------------------------------------------------------------
  def toPointDouble2D() = new Point2D_Double(x, y)
  //---------------------------------------------------------------------------
  def getDistance() = Math.sqrt( (x * x) + (y * y)).toFloat
  //---------------------------------------------------------------------------
  def getDistance(p: Point2D_Float) ={
    val a = x - p.x
    val b = y - p.y
    Math.sqrt( (a * a) + (b * b)).toFloat
  }
  //---------------------------------------------------------------------------
  def getAngle(p: Point2D_Float) = Math.atan2(p.y - y , p.x - x)
  //---------------------------------------------------------------------------
  def getSlope(p: Point2D_Float) = (p.y - y.toFloat) / (p.x - x)
  //---------------------------------------------------------------------------
  override def toString = s"($x,$y)"
  //---------------------------------------------------------------------------
  def getFormattedShortString() = s"(${f"$x%.3f"}, ${f"$y%.3f"})"
  //---------------------------------------------------------------------------
  def getFormattedString() = s"(${f"$x%10.6f"},${f"$y%10.6f"})"
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
  def getDistanceToLine(p1: Point2D_Float, p2: Point2D_Float) = {
    val dx = p2.x - p1.x
    val dy = p2.y - p1.y
    Math.abs( ((dy * x) - (dx * y) + (p2.x * p1.y) - (p2.y * p1.x)) /
      Math.sqrt( (dy * dy) + (dx * dx))
    ).toFloat
  }
  //---------------------------------------------------------------------------
  def isNan() = x.isNaN || y.isNaN
  //---------------------------------------------------------------------------
  //https://github.com/caente/convex-hull/blob/master/src/main/scala/com/miguel/GrahamScanScala.scala
  def goesLeft(p0: Point2D_Float, p1: Point2D_Float): Boolean =
    (p1.x - p0.x) * (this.y - p0.y) - (this.x - p0.x) * (p1.y - p0.y) > 0
  //---------------------------------------------------------------------------
  def magnitude() = Math.sqrt((x * x)  + (y * y))
  //---------------------------------------------------------------------------
  def normalize() = {
    val m = magnitude()
    Point2D_Float((x / m).toFloat, (y / m).toFloat)
  }
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Point2D_Float.scala
//=============================================================================