/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  23/Jan/2021
 * Time:  10h:46m
 * Description: None
 */
//=============================================================================
package com.common.geometry.hull
//=============================================================================
import com.common.geometry.point.{Point2D, Point2D_Double}
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object ConvexHull {
  //---------------------------------------------------------------------------
  //https://github.com/caente/convex-hull/blob/master/src/main/scala/com/miguel/GrahamScanScala.scala
  //https://jamesadam.me/2013/11/21/grahams-scan-in-scala/
  //https://www.geeksforgeeks.org/convex-hull-set-2-graham-scan/
  //https://www.gaussianos.com/una-interesante-introduccion-a-la-geometria-computacional/
  //O(n log(n))
  def grahamScan(pointSeq: List[Point2D_Double]) = {
    //instead of using PUSH and POP, foldRight
    def addToHull(hull: List[Point2D_Double], new_point: Point2D_Double): List[Point2D_Double] =
      new_point :: hull.foldRight(List.empty[Point2D_Double]) {
        case (p1, currentHull@(p0 :: _)) => if (new_point.goesLeft(p0, p1)) p1 :: currentHull else currentHull
        case (p, currentHull) => p :: currentHull
      }
    val min = pointSeq.minBy(_.y)
    //adding the min to close the polygon
    min :: pointSeq.sortBy(point =>
      Math.atan2(point.y - min.y, point.x - min.x)).foldLeft(List.empty[Point2D_Double])(addToHull)
  }
  //---------------------------------------------------------------------------
  //https://gotorecursive.wordpress.com/2014/05/28/grahams-scan-from-imperative-to-functional/
  def grahamScanPoint2D(pointSeq: List[Point2D]) = {
    //-------------------------------------------------------------------------
    def addToHull(hull: List[Point2D], new_point: Point2D): List[Point2D] =
      new_point :: hull.foldRight(List.empty[Point2D]) {
        case (p1, currentHull@p0 :: _) => if (new_point.goesLeft(p0, p1)) p1 :: currentHull else currentHull
        case (p, currentHull) => p :: currentHull
      }
    //-------------------------------------------------------------------------
    val min = pointSeq.minBy(_.y)
    min :: pointSeq.sortBy(point =>
      Math.atan2(point.y - min.y, point.x - min.x)).foldLeft(List.empty[Point2D])(addToHull)
  }
  //---------------------------------------------------------------------------
  //https://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain#Scala
  def monotone(pointSeq: List[Point2D_Double]) = {
    //-------------------------------------------------------------------------
    // 2D cross product of OA and OB vectors, i.e. z-component of their 3D cross product.
    // Returns a positive value, if OAB makes a counter-clockwise turn,
    // negative for clockwise turn, and zero if the points are collinear.
    def cross(o: (Double, Double), a: (Double, Double), b: (Double, Double)): Double =
      (a._1 - o._1) * (b._2 - o._2) - (a._2 - o._2) * (b._1 - o._1)
    //-------------------------------------------------------------------------
    if (pointSeq.size == 1) List(pointSeq.head,pointSeq.head)
    else {
      val pSeq = (pointSeq map (p => (p.x, p.y)))

      val sortedPoints = pSeq.sorted
      // Build the lower hull
      val lower = ArrayBuffer[(Double, Double)]()
      for(i <- sortedPoints) {
        while (lower.length >= 2 && cross(lower(lower.length - 2), lower(lower.length - 1), i) <= 0)
          lower -= lower.last
        lower += i
      }

      // Build the upper hull
      val upper = ArrayBuffer[(Double, Double)]()
      for(i <- sortedPoints.reverse){
        while(upper.size >= 2 && cross(upper(upper.length - 2), upper(upper.length - 1) , i) <= 0)
          upper -= upper.last
        upper += i
      }

      // Last point of each list is omitted because it is repeated at the beginning of the other list.
      lower -= lower.last
      upper -= upper.last

      // Concatenation of the lower and upper hulls gives the convex hull
      ((lower ++= upper) map ( p=> new Point2D_Double(p._1, p._2))).toList
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file ConvexHull.scala
//=============================================================================
