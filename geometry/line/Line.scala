/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/Jan/2021
 * Time:  13h:55m
 * Description: Euclidean line
 * BAsed on:  http://www.softwareandfinance.com/Turbo_C/Intersection_Two_lines_EndPoints.html
 */
//=============================================================================
package com.common.geometry.line
//=============================================================================
import com.common.geometry.point.{Point2D, Point2D_Double}
//=============================================================================
//=============================================================================
object Line {
  //---------------------------------------------------------------------------u
  def apply(a: Point2D, b: Point2D, s: Point2D_Double) : Line = {
    val slope = (b.x - a.x.toDouble) / (a.y - b.y)
    Line(slope,  s.y - slope * s.x)
  }
  //-----------------------------------------------------------------------
  def buildWithPointAndDirection(p: Point2D_Double, d: Point2D_Double) = {
    val slope = d.y / d.x
    val intercept = p.y - ((d.y * p.x) / d.x)
    Line(slope, intercept)
  }
  //-----------------------------------------------------------------------
  def buildWithTwoPoints(a: Point2D_Double, b: Point2D_Double) = {
    val d = b - a
    val slope = d.y / d.x
    val intercept = a.y - (slope * a.x)
    Line(slope,intercept)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//it represents the line: f(x) = y = ax + c    //u = slope and c = intercept
//vertical line (i.e. x = 3) can not be represented
case class Line(a : Double, c : Double) {
  //---------------------------------------------------------------------------
  def getY(x: Double) = a * x + c
  //---------------------------------------------------------------------------
  def getIntersection(l2: Line) = {
    val x = (l2.c - c) / (a - l2.a)
    new Point2D_Double(x, getY(x))
  }
  //---------------------------------------------------------------------------
  def existIntersection(l2: Line) = !getIntersection(l2).allComponentAreNan
  //---------------------------------------------------------------------------
  override def toString() = s"y = {$a}x + $c"
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Line.scala
//=============================================================================
