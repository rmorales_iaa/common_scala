/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  25/Oct/2020
 * Time:  15h:09m
 * Description: None
 */
//=============================================================================
package com.common.geometry.grid
//=============================================================================
import com.common.geometry.point.Point2D

import scala.collection.mutable.ArrayBuffer
//=============================================================================
object GridCircleOverlap {
  //---------------------------------------------------------------------------
  //https://github.com/astropy/photutils/blob/master/photutils/geometry/circular_overlap.pyx
  //Area of overlap between a circle and a pixel grid. The circle is centered on the origin
  //return  2-d array of shape (nx, ny) giving the fraction of the overlap.
  def calculate(
      xmin: Double    //grid xMin
    , xmax: Double   //grid xPixMax
    , ymin: Double   //grid yMin
    , ymax: Double   //grid yPixMax
    , nx : Int       //grid x dimension
    , ny : Int      //grid y dimension
    , r : Double     // circle radius
    , use_exact : Boolean // true calculates exact overlap, false uses `subpixel` number of subpixels to list the overlap.
    , subpixels : Int // Each pixel resampled by this factor in each dimension, thus each pixel is divided into `subpixels ** 2` subpixels.
    )  = {

    //define output array
    val frac = ArrayBuffer.fill(ny, nx)(0d)

    //find the width of each element in x and y
    val dx = (xmax - xmin) / nx
    val dy = (ymax - ymin) / ny

    //Find the radius of a single pixel
    val pixel_radius = 0.5 * Math.sqrt(dx * dx + dy * dy)

    //Define bounding box
    val bxmin = -r - 0.5 * dx
    val bxmax = +r + 0.5 * dx
    val bymin = -r - 0.5 * dy
    val bymax = +r + 0.5 * dy

    for (i<- 0 until nx) {

      val pxmin = xmin + i * dx //lower end of pixel
      val pxcen = pxmin + dx * 0.5
      val pxmax = pxmin + dx //upper end of pixel

      if (pxmax > bxmin && pxmin < bxmax) {
        for (j <- 0 until ny) {
          val pymin = ymin + j * dy
          val pycen = pymin + dy * 0.5
          val pymax = pymin + dy
          if ((pymax > bymin) && (pymin < bymax)) {

            // Distance from circle center to pixel center.
            val d = Math.sqrt(pxcen * pxcen + pycen * pycen)

            // If pixel center is "well within" circle, count full pixel.
            if (d < r - pixel_radius) frac(j)(i) = 1d
            else if (d < r + pixel_radius) { //If pixel center is "close" to circle border, find overlap.

              //either do exact calculation or use subpixel sampling
              if (use_exact)
                frac(j)(i) = circular_overlap_single_exact(pxmin, pymin, pxmax, pymax, r) / (dx * dy)
              else
                frac(j)(i) = circular_overlap_single_subpixel(pxmin, pymin, pxmax, pymax, r, subpixels)
            }
          }
        }
      }
      // Otherwise, it is fully outside circle. No action needed.
    }
    frac
  }
  //---------------------------------------------------------------------------
  //Return the fraction of overlap between a circle and a single pixel with given extent, using a sub-pixel sampling method
  private def circular_overlap_single_subpixel(
     x0 : Double
    , y0 : Double
    , x1: Double
    , y1: Double
    , r : Double
    , subpixels : Int) : Double = {

    val dx = (x1 - x0) / subpixels
    val dy = (y1 - y0) / subpixels
    val r_squared = r * r

    var x = x0 - 0.5 * dx
    var y = 0d
    var frac : Double = 0 // Accumulator
    for (_ <- 0 until subpixels) {
      x += dx
      y = y0 - 0.5 * dy
      for (_ <- 0 until subpixels) {
        y += dy
        if ((x * x + y * y) < r_squared)
          frac += 1d
      }
    }
    frac / (subpixels * subpixels)
  }
  //---------------------------------------------------------------------------
  //Area of overlap of a rectangle and a circle
  private def circular_overlap_single_exact(
    xmin : Double
    , ymin : Double
    , xmax : Double
    , ymax : Double
    , r : Double) : Double = {

    if (0 <= xmin) {
      if (0 <= ymin) circular_overlap_core(xmin, ymin, xmax, ymax, r)
      else {
        if (0 >= ymax) circular_overlap_core(-ymax, xmin, -ymin, xmax, r)
        else circular_overlap_single_exact(xmin, ymin, xmax, 0, r) +
             circular_overlap_single_exact(xmin, 0, xmax, ymax, r)
      }
    }
    else {
      if (0 >= xmax) {
        if (0 <= ymin) circular_overlap_core(-xmax, ymin, -xmin, ymax, r)
        else {
          if (0 >= ymax) circular_overlap_core(-xmax, -ymax, -xmin, -ymin, r)
          else circular_overlap_single_exact(xmin, ymin, xmax, 0, r) +
               circular_overlap_single_exact(xmin, 0, xmax, ymax, r)
        }
      }
      else {
        if (0 <= ymin) return circular_overlap_single_exact(xmin, ymin, 0, ymax, r) +
          circular_overlap_single_exact(0, ymin, xmax, ymax, r)
        if (0 >= ymax)
          circular_overlap_single_exact(xmin, ymin, 0, ymax, r) +
            circular_overlap_single_exact(0, ymin, xmax, ymax, r)
        else
          circular_overlap_single_exact(xmin, ymin, 0, 0, r) +
            circular_overlap_single_exact(0, ymin, xmax, 0, r) +
            circular_overlap_single_exact(xmin, 0, 0, ymax, r) +
            circular_overlap_single_exact(0, 0, xmax, ymax, r)
      }
    }
  }
  //---------------------------------------------------------------------------
  //https://github.com/astropy/photutils/blob/master/photutils/geometry/core.pyx
  /*
    In some of the geometrical functions, we have to take the sqrt of a number
    and we know that the number should be >= 0. However, in some cases the
    value is e.g. -1e-10, but we want to treat it as zero, which is what
    this distribution.function does.

    Note that this does **not** check whether negative values are close or not
    to zero, so this should be used only in cases where the value is expected
    to be positive on paper.
    */
  private def floor_sqrt(x:Double) : Double = {
    if (x > 0) Math.sqrt(x)
    else 0
  }
  //---------------------------------------------------------------------------
  private def circular_overlap_core(
    xmin : Double
    , ymin : Double
    , xmax : Double
    , ymax : Double
    , r : Double) : Double = {

    var area = 0d
    if ((xmin * xmin + ymin * ymin) > (r * r)) area = 0
    else {
      if ((xmax * xmax + ymax * ymax) < (r * r)) area = (xmax - xmin) * (ymax - ymin)
      else {
        area = 0
        val d1 = floor_sqrt(xmax * xmax + ymin * ymin)
        val d2 = floor_sqrt(xmin * xmin + ymax * ymax)
        if ((d1 < r) && (d2 < r)) {
          val (x1,y1) = (floor_sqrt(r * r - ymax * ymax), ymax)
          val (x2,y2) = (xmax, floor_sqrt(r * r - xmax * xmax))
          area = ((xmax - xmin) * (ymax - ymin)) -
            area_triangle(x1, y1, x2, y2, xmax, ymax) +
            area_arc(x1, y1, x2, y2, r)
        }
        else{
          if (d1 < r) {
            val (x1,y1) = (xmin, floor_sqrt(r * r - xmin * xmin))
            val (x2,y2) = (xmax, floor_sqrt(r * r - xmax * xmax))
            area = area_arc(x1, y1, x2, y2, r) +
              area_triangle(x1, y1, x1, ymin, xmax, ymin) +
              area_triangle(x1, y1, x2, ymin, x2, y2)
          }
          else{
            if (d2 < r){
              val (x1,y1) = (floor_sqrt(r * r - ymin * ymin),ymin)
              val (x2,y2) = (floor_sqrt(r * r - ymax * ymax),ymax)
              area = area_arc(x1, y1, x2, y2, r) +
                area_triangle(x1, y1, xmin, y1, xmin, ymax) +
                area_triangle(x1, y1, xmin, y2, x2, y2)
            }
            else{
              val (x1,y1) = (floor_sqrt(r * r - ymin * ymin),ymin)
              val (x2,y2) = (xmin, floor_sqrt(r * r - xmin * xmin))
              area = area_arc(x1, y1, x2, y2, r) +
                area_triangle(x1, y1, x2, y2, xmin, ymin)
            }
          }
        }
      }
    }
    area
  }
  //---------------------------------------------------------------------------
  //The Euclidean distance between the two points
  private def distance(x1: Double, y1: Double, x2: Double, y2: Double): Double =
    Math.sqrt(((x2 - x1) *  (x2 - x1)) + ((y2 - y1) * (y2 - y1)))
  //---------------------------------------------------------------------------
  //Area of a circle arc with radius r between points (x1, y1) and (x2, y2).
  private def area_arc(x1 : Double, y1: Double, x2: Double, y2: Double, r : Double) = {
    val a = distance(x1, y1, x2, y2)
    val theta = 2 * Math.asin(0.5 * a / r)
    0.5 * r * r * (theta - Math.sin(theta))
  }
  //---------------------------------------------------------------------------
  //Area of a triangle defined by three vertices.
  private def area_triangle(x1 : Double,y1 : Double,x2 : Double,y2 : Double,x3 : Double,y3 : Double) =
    0.5 * Math.abs(x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2))

  //---------------------------------------------------------------------------
  //https://github.com/astropy/photutils/blob/main/photutils/aperture/bounding_box.py
  private def getOverlapSlices(xmin: Int
                       , xmax: Int
                       , ymin: Int
                       , ymax: Int
                       , imageDimension:Point2D) = {
    val slicesLarge = (Point2D(Math.max(ymin, 0), Math.min(ymax, imageDimension.y)),
                       Point2D(Math.max(xmin, 0), Math.min(xmax,  imageDimension.x)))
    val slicesSmall  = (Point2D(Math.max(-ymin, 0),Math.min(ymax - ymin, imageDimension.y - ymin)),
                       Point2D(Math.max(-xmin, 0), Math.min(xmax - xmin, imageDimension.x - xmin)))
    (slicesLarge,slicesSmall)
  }

  //---------------------------------------------------------------------------
  //https://github.com/astropy/photutils/blob/main/photutils/aperture/mask.py
  //_get_overlap_cutouts
  //calcualte pix mask, get sub image dimensions and reshape the weights to fit with subamtrix
  def reshape(xmin: Int
               , xmax: Int
               , ymin: Int
               , ymax: Int
               , imageDimension: Point2D
               , weightMatrix: ArrayBuffer[ArrayBuffer[Double]]) = {
    val (sliceLarge, sliceSmall) = getOverlapSlices(
        xmin
      , xmax
      , ymin
      , ymax
      , imageDimension)

    val yRangeSlice = sliceSmall._1
    val xRangeSlice = sliceSmall._2

    val weightMatrixReshaped = ArrayBuffer[Double]()
    val pixMax = (for (y <- yRangeSlice.x until yRangeSlice.y;
                       x <-xRangeSlice.x until xRangeSlice.y) yield {
      val weight = weightMatrix(y)(x)
      weightMatrixReshaped += weight
      if(weightMatrix(y)(x) > 0) true //good pixels
      else false

    }).toArray
    (weightMatrixReshaped, sliceLarge, pixMax)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file GridCircleOverlap.scala
//=============================================================================
