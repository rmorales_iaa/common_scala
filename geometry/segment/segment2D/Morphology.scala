//=============================================================================
package com.common.geometry.segment.segment2D
//=============================================================================
import com.common.dataType.pixelDataType.PixelDataType.{PIXEL_DATA_TYPE, PIXEL_ZERO_VALUE}
import com.common.geometry.point.{Point2D, Point2D_Double}
import com.common.image.estimator.ellipse.{Ellipse, EllipseFit}
import com.common.image.estimator.ellipse.EllipseFit.{ELLIPSE_DIRECT_FIT, ELLIPSE_MY_FIT}
//=============================================================================
import scala.util.{Failure, Success, Try}
//=============================================================================
object Morphology {
  //---------------------------------------------------------------------------
  private def getEllipseEstimation(perimeterPosSeq: Array[Point2D]) : Option[Ellipse] = {
    var r: Option[Ellipse] = None
    val pointSeq = perimeterPosSeq map (Point2D_Double(_))
    Try {
      r = EllipseFit.fit(pointSeq, algorithm = ELLIPSE_DIRECT_FIT)
    }
    match {
      case Success(_) =>
      case Failure(_) =>
        r = EllipseFit.fit(pointSeq, algorithm = ELLIPSE_MY_FIT)
    }
    r
  }
  //---------------------------------------------------------------------------
  //regarding to the rectangle hull
  private def getPixelInPercentage(_source: Segment2D): Int = {
    val source = _source.toMatrix2D().toSegment2D_ByDetection().get
    val xAxis = source.getX_AxisAbsolute()
    val yAxis = source.getY_AxisAbsolute()
    var pixInCount = 0
    for(y<-yAxis.s to yAxis.e;
        x<-xAxis.s to xAxis.e) {
      if (source.isInAbsoluteCoordinate(x,y)) pixInCount += 1
    }
    val totalPixel = xAxis.range * yAxis.range
    if (pixInCount == 0) 0 else Math.round((pixInCount / totalPixel.toDouble) * 100).toInt
  }
  //---------------------------------------------------------------------------
  def calculate(source: Segment2D) = {
    //---------------------------------------------------------------------------
    def isDataZero(d: Array[PIXEL_DATA_TYPE]) =
      d forall( _ == PIXEL_ZERO_VALUE)
    //---------------------------------------------------------------------------
    val (hasUniqueRow,hasUniqueCol) = {
      val m = source.toMatrix2D()
      val rowSeq = m.getAsRowSeq()
      val colSeq = m.getAsColSeq()
      ((rowSeq map (row => if(isDataZero(row)) 0 else 1)).sum <= 1,
        (colSeq map (col => if(isDataZero(col)) 0 else 1)).sum <= 1)
    }

    val polygon = source.getPolygon()
    val perimeterPosSeq = polygon.getVertexSeq map(_.toPoint2D)
    val ellipseOpt = getEllipseEstimation(perimeterPosSeq)
    val perimeter = (perimeterPosSeq :+ perimeterPosSeq.head).sliding(2).map(t=> t(0).getDistance(t(1))).sum
    val rowCount = source.getRowCount()
    val colCount = source.getX_Axis.range
    val proportion = rowCount / colCount.toFloat
    Morphology(source.getElementCount           //area
      , perimeter                               //perimeter
      , if (ellipseOpt.isEmpty) -1 else ellipseOpt.get.eccentricity  //eccentricity
      , hasUniqueRow
      , hasUniqueCol
      , getPixelInPercentage(source)
      , proportion)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Morphology(area: Long
                      , perimeter: Double
                      , eccentricity: Double
                      , hasUniqueRow: Boolean = false
                      , hasUniqueCol: Boolean = false
                      , pixelInPercentage: Int //regarding to rectangle hull
                      , sizeProportion: Float) //regardind x size and y size
//=============================================================================
//End of file Segment2D.scala
//=============================================================================
