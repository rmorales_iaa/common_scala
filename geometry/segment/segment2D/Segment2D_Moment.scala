/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  27/Oct/2020
 * Time:  12h:42m
 * Description:
 * https://en.wikipedia.org/wiki/Image_moment
 * https://github.com/accord-net/java/blob/master/Catalano.Image/src/Catalano/Imaging/Tools/ImageMoments.java
 */

//=============================================================================
package com.common.geometry.moment
//=============================================================================
//=============================================================================
import com.common.geometry.segment.segment2D.Segment2D
//=============================================================================
//=============================================================================
case class Segment2D_Moment(s: Segment2D) extends Moment {
  //---------------------------------------------------------------------------
  //(x,y) <- (xPos,yPos)
  private val posMap = (s.rowSeq.zipWithIndex flatMap { case (row,y) => row.zipWithIndex.map{ case (s,x)=>
    val xPosSeq = s.getPosSeq map (_.toDouble)
    val yPosSeq = Array.fill[Double](s.elementCount)(y)
    (x,y) -> ((xPosSeq, yPosSeq))
  }}).toMap
  //---------------------------------------------------------------------------
  def getRawMoment(px: Int, qy: Int): Double = {
    var m = 0d
    (px,qy) match {
      case (0, 0) => m += s.getFlux()

      case (1, 0) =>
        s.rowSeq.zipWithIndex foreach { case (row, y) => row.zipWithIndex.foreach { case (s, x) =>
          val (xPos, _) = posMap((x, y))
          m += s.multiplyAndSum(xPos)
        }
        }

      case (0, 1) =>
        s.rowSeq.zipWithIndex foreach { case (row, y) => row.zipWithIndex.foreach { case (s, x) =>
          val (_, yPos) = posMap((x, y))
          m += s.multiplyAndSum(yPos)
        }
        }

      case (1, 1) =>
        s.rowSeq.zipWithIndex foreach { case (row, y) => row.zipWithIndex.foreach { case (s, x) =>
          val (xPos, _) = posMap((x, y))
          val (_, yPos) = posMap((x, y))
          val xy = (xPos, yPos).zipped.map(_ * _)
          m += s.multiplyAndSum(xy.toArray)
        }
        }

      case (0, 2) =>
        s.rowSeq.zipWithIndex foreach { case (row, y) => row.zipWithIndex.foreach { case (s, x) =>
          val (_, yPos) = posMap((x, y))
          val y2 = (yPos, yPos).zipped.map(_ * _)
          m += s.multiplyAndSum(y2.toArray)
        }
        }

      case (2, 0) =>
        s.rowSeq.zipWithIndex foreach { case (row, y) => row.zipWithIndex.foreach { case (s, x) =>
          val (xPos, _) = posMap((x, y))
          val x2 = (xPos, xPos).zipped.map(_ * _)
          m += s.multiplyAndSum(x2.toArray)
        }
        }

      case (1, 2) =>
        s.rowSeq.zipWithIndex foreach { case (row, y) => row.zipWithIndex.foreach { case (s, x) =>
          val (xPos, yPos) = posMap((x, y))
          val y2 = (yPos, yPos).zipped.map(_ * _)
          val xy2 = (xPos, y2).zipped.map(_ * _)
          m += s.multiplyAndSum(xy2.toArray)
        }
        }

      case (2, 1) =>
        s.rowSeq.zipWithIndex foreach { case (row, y) => row.zipWithIndex.foreach { case (s, x) =>
          val (xPos, yPos) = posMap((x, y))
          val x2 = (xPos, xPos).zipped.map(_ * _)
          val x2y = (x2, yPos).zipped.map(_ * _)
          m += s.multiplyAndSum(x2y.toArray)
        }
        }

      case (2, 2) =>
        s.rowSeq.zipWithIndex foreach { case (row, y) => row.zipWithIndex.foreach { case (s, x) =>
          val (xPos, yPos) = posMap((x, y))
          val x2 = (xPos, xPos).zipped.map(_ * _)
          val y2 = (yPos, yPos).zipped.map(_ * _)
          val x2y2 = (x2, y2).zipped.map(_ * _)
          m += s.multiplyAndSum(x2y2.toArray)
        }
        }

      case _ =>
        s.rowSeq.zipWithIndex foreach { case (row, y) => row.zipWithIndex.foreach { case (s, x) =>
          val (xPos, yPos) = posMap((x, y))

          var xPow = Array.fill[Double](xPos.length)(1)
          for (_ <- 0 until px)
            xPow = (xPow, xPos).zipped.map(_ * _).toArray

          var yPow = Array.fill[Double](yPos.length)(1)
          for (_ <- 0 until qy)
            yPow = (yPow, yPos).zipped.map(_ * _).toArray

          val xPow_yPow = (xPow, yPow).zipped.map(_ * _)
          m += s.multiplyAndSum(xPow_yPow.toArray)
        }
        }
    }
    m
  }
  //---------------------------------------------------------------------------
  def getCentralMomentGeneric(px: Int, qy: Int): Double = {
    val centreOfMass = getCentreOfMass
    val centreX = centreOfMass.x
    val centreY = centreOfMass.y
    var mc = 0d
    s.rowSeq.zipWithIndex foreach { case (row,y) => row.zipWithIndex.foreach { case (s,x)=>
      val (xPos, yPos) = posMap((x, y))

      var xPow = xPos map (_ - centreX)
      for (_ <- 1 until px)
        xPow = (xPow, xPos).zipped.map(_ * _).toArray

      var yPow = if (qy == 0) Array.fill[Double](yPos.length)(1) else yPos map (_ - centreY)
      for (_ <- 1 until qy)
        yPow = (yPow, yPos).zipped.map(_ * _).toArray

      val xPow_yPow = (xPow, yPow).zipped.map(_ * _)
      mc += s.multiplyAndSum(xPow_yPow.toArray)
    }}
    mc
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Source2D_Moment.scala
//=============================================================================
