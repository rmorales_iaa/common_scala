/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Jan/2022
 * Time:  19h:36m
 * Description: None
 */
//=============================================================================
package com.common.geometry.segment.segment2D.deblend
//=============================================================================
import com.common.dataType.pixelDataType.PixelDataType.PIXEL_DATA_TYPE
import com.common.geometry.segment.segment2D.Segment2D
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object ComponentTree  {
  //--------------------------------------------------------------------------
  type ValueType = Segment2D
  //---------------------------------------------------------------------------
}
//=============================================================================
import ComponentTree._
//=============================================================================
case class PairChildParent(child:Segment2D, parent: Segment2D)
//=============================================================================
case class Level(index: Int, thereshold: PIXEL_DATA_TYPE, pairSeq: Array[PairChildParent]) {
  //---------------------------------------------------------------------------
  private var isSplitted: Boolean = false
  //---------------------------------------------------------------------------
  override def toString() = s"$index[$isSplitted,$thereshold,${pairSeq.length}]->${pairSeq.map(t=> t.toString()).mkString("{",",","}")}"
  //---------------------------------------------------------------------------
  def setIsSplitted(b: Boolean) = isSplitted = b
  //---------------------------------------------------------------------------
  def hasASplit() = isSplitted
  //---------------------------------------------------------------------------
  def size = pairSeq.size
  //---------------------------------------------------------------------------
  def getPixelSet() =
    pairSeq.flatMap(_.child.getAbsolutePosWithPixelValue()).toSet
  //---------------------------------------------------------------------------
  def getChild(i: Int) = pairSeq(i).child
  //---------------------------------------------------------------------------
  def getChildSeq() = pairSeq map (_.child)
  //---------------------------------------------------------------------------
  def findPairWithParent(parent: ValueType) =
    pairSeq.find(pair => pair.child.offset == parent.offset).get
  //---------------------------------------------------------------------------
  def print() =  {
    println(s"@--------level $thereshold starts -----------")
    pairSeq.foreach( _.child.prettyPrint())
    println(s"@--------level $thereshold ends -----------")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
case class ComponentTree() {
  //---------------------------------------------------------------------------
  private val levelSeq = ArrayBuffer[Level]()
  //---------------------------------------------------------------------------
  def getLastLevel() = levelSeq.last
  //---------------------------------------------------------------------------
  def getLastLevelSourceSeq() = levelSeq.last.getChildSeq
  //---------------------------------------------------------------------------
  def getMainSource() = levelSeq.head.pairSeq.head.child
  //---------------------------------------------------------------------------
  def getLevel(i: Int) = levelSeq(i)
  //---------------------------------------------------------------------------
  def isEmpty() = levelSeq.size <= 1
  //---------------------------------------------------------------------------
  def getLevelSeq(reverseLevel: Boolean = true) = if (reverseLevel) levelSeq.reverse.toArray else levelSeq.toArray
  //---------------------------------------------------------------------------
  def addFirstLevel(threshold: PIXEL_DATA_TYPE, value: ValueType) =
    levelSeq += Level(0, threshold, Array(PairChildParent(value,null)))
  //---------------------------------------------------------------------------
  def add(threshold: PIXEL_DATA_TYPE, valueSeq: Array[(ValueType,Array[ValueType])]) : Unit= {
    val lastLevel = levelSeq.last
    val newPairChildSeq =
      valueSeq flatMap { case (parent,children) =>
      if (children.isEmpty) Array(lastLevel.findPairWithParent(parent))
      else children.map(child=> PairChildParent(child,parent))
    }
    lastLevel.setIsSplitted(newPairChildSeq.length > lastLevel.size)
    levelSeq += Level(lastLevel.index + 1, threshold, newPairChildSeq)
  }
  //---------------------------------------------------------------------------
  def print() = levelSeq.foreach( _.print)
  //---------------------------------------------------------------------------
}
//=============================================================================

//=============================================================================
//End of file ComponentTree.scala
//=============================================================================
