/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Jan/2022
 * Time:  12h:39m
 * Description: None
 */
//=============================================================================
package com.common.geometry.segment.segment2D.deblend
//=============================================================================
import com.common.configuration.MyConf
import com.common.dataType.pixelDataType.PixelDataType.PIXEL_DATA_TYPE
import com.common.geometry.segment.segment2D.deblend.algorithm.CosmosAlgorithm
import com.common.geometry.segment.segment2D.Segment2D
import com.common.logger.MyLogger
//=============================================================================
object Deblend extends MyLogger {
  //---------------------------------------------------------------------------
  private val DEBLEND_SAMPLING_N_THERESHOLD     = MyConf.c.getInt("Deblending.COSMOS.samplingLevel")
  private val DEBLEND_SAMPLING_EXPONENTIAL_STEP = MyConf.c.getBoolean("Deblending.COSMOS.samplingExponentialStep")
  private val MAX_ALLOWED_SOURCE_PROPORTION     = MyConf.c.getFloat("SourceDetection.maxAllowedSourceProportion")
  //---------------------------------------------------------------------------
  //based on: "The CosmosIntensityEstimator system for crowed-field analysis of digitized photographic plate scans" S.M. Bear and H.T. MacGillivray
  //based on: Sextractor deblending algorithm sextractor deblending algorithm
  //it slices (sample) the source in 1D/2D using several theresholds
  def reThereshold(source: Segment2D
                   , backgroundEstimation: PIXEL_DATA_TYPE
                   , algorithm: CosmosAlgorithm
                   , imageFWHM: Double
                  ): Array[Segment2D] = {
    val data = source.getData().sorted
    val minValue = data.head
    val peakValue = data.last.toDouble
    //--------------------------------------------------------------------------
    val componentTree = ComponentTree()
    componentTree.addFirstLevel(minValue,source)

    //iterate form to threshold to bottom applying it to the sources at each level
    for(i <-1 until DEBLEND_SAMPLING_N_THERESHOLD) {
      //calculate the NEW threshold to apply
      val newThereshold =
        if (DEBLEND_SAMPLING_EXPONENTIAL_STEP) minValue * Math.pow(peakValue/minValue, i.toDouble / DEBLEND_SAMPLING_N_THERESHOLD)
        else minValue + (peakValue - minValue) * ( i.toDouble / DEBLEND_SAMPLING_N_THERESHOLD )
      val thereshold = Math.round(newThereshold)

      val pairSeq =
        componentTree.getLastLevel().getChildSeq().map { parent =>
        val childSeq = parent.toMatrix2D().findSource(thereshold).toSegment2D_seq()
        (parent,childSeq)
      }
      componentTree.add(thereshold, pairSeq)
    }

    //the last level of the component tree holds the sources that stores the sources with max local pixel value
    //Those sources are calculated attenting to the sample rate DEBLEND_SAMPLING_N_THERESHOLD and DEBLEND_SAMPLING_EXPONENTIAL_STEP
    if (componentTree.isEmpty()) Array(source)
    else algorithm.deblend(componentTree, backgroundEstimation, imageFWHM)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Deblend.scala
//=============================================================================
