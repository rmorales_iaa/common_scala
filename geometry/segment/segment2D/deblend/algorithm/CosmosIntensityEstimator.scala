/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Jan/2022
 * Time:  11h:06m
 * Description: Algorithm for source deblending
 * "The Cosmos system for crowded-field analysis of digitized photographic plate scans"
   S. M. Beard, H. T. MacGillivray, Peter Thanisch
   October 1990 Monthly Notices of the Royal Astronomical Society 247:311
 */
//=============================================================================
package com.common.geometry.segment.segment2D.deblend.algorithm
//=============================================================================
import com.common.dataType.pixelDataType.PixelDataType.{PIXEL_DATA_TYPE}
import com.common.geometry.point.{Point2D, Point2D_Double}
import com.common.geometry.segment.segment2D.Segment2D
//=============================================================================
//=============================================================================
case class CosmosIntensityEstimator() extends CosmosAlgorithm {
  //---------------------------------------------------------------------------
  def getIntensityAtPosition(source: Segment2D
                             , thereshold: PIXEL_DATA_TYPE
                             , pixelPosition: Point2D
                             , pixelIntensity: PIXEL_DATA_TYPE
                             , Isky: PIXEL_DATA_TYPE
                             , imageFWHM: Double) = {
    val In = thereshold.toDouble       //faintPixelIntensity of threshold level
    val Imax = source.getData().max.toDouble //peak faintPixelIntensity within object
    val objectCentroid = source.getRectangle().getCentroid()
    val distance = objectCentroid - Point2D_Double(pixelPosition)
    val r2 = (distance.x + distance.x) + (distance.y + distance.y)
    val B = (- Math.log((In -Isky) / (Imax - Isky))) / r2

    Isky + ((Imax - Isky) * Math.exp(-B * r2))  //paper:equation 7
  }
  //---------------------------------------------------------------------------
  def getIntensityAtPositionOriginal(source: Segment2D
                             , thereshold: PIXEL_DATA_TYPE
                             , pixelPosition: Point2D
                             , pixelIntensity: PIXEL_DATA_TYPE
                             , Isky: PIXEL_DATA_TYPE
                             , imageFWHM: Double) = {
    val An = source.getElementCount //number of pixels having faintPixelIntensity greater that In
    val In = thereshold.toDouble       //faintPixelIntensity of threshold level
    val Imax = source.getData().max.toDouble //peak faintPixelIntensity within object
    val M = An / Math.log( (In - Isky) / (Imax - Isky)) //paper:equation 4. slope of the best-fitting straight line
    val B = -Math.PI / M  //paper:equation 5. it represents the (-1/2*stdDev^2) in the formulae of https://en.wikipedia.org/wiki/Gaussian_function

    val objectCentroid = source.getRectangle().getCentroid()
    val distance = objectCentroid - Point2D_Double(pixelPosition)
    val r2 = (distance.x + distance.x) + (distance.y + distance.y)

    Isky + ((Imax - Isky) * Math.exp(-B * r2))  //paper:equation 7
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file CosmosIntensityEstimator.scala
//=============================================================================