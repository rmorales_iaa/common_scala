/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Jan/2022
 * Time:  11h:06m
 * Description: Algorithm for source deblending
 * Adaptation of CosmosIntensityEstimator deblending algorithm using bell shape fit for estimating the contribution of a source in a pixel position:
 * "The Cosmos system for crowded-field analysis of digitized photographic plate scans"
   S. M. Beard, H. T. MacGillivray, Peter Thanisch
   October 1990 Monthly Notices of the Royal Astronomical Society 247:311
 */
//=============================================================================
package com.common.geometry.segment.segment2D.deblend.algorithm
//=============================================================================
import com.common.dataType.pixelDataType.PixelDataType.PIXEL_DATA_TYPE
import com.common.geometry.point.{Point2D, Point2D_Double}
import com.common.geometry.segment.segment2D.Segment2D
//=============================================================================
//=============================================================================
//it calculates the intensity contribution of a source regarding a pixel position
case class MyIntensityEstimator() extends CosmosAlgorithm {
  //---------------------------------------------------------------------------
  def getIntensityAtPosition(source: Segment2D
                             , thereshold: PIXEL_DATA_TYPE
                             , pixelPosition: Point2D
                             , pixelIntensity: PIXEL_DATA_TYPE
                             , Isky: PIXEL_DATA_TYPE
                             , imageFWHM: Double) =
//    getIntensityAtPosition1D(source,pixelPosition,Isky)
    //getIntensityAtPosition2d(source,pixelPosition,pixelIntensity,Isky, imageFWHM)
    getIntensityAtSimplier(source,pixelPosition)
  //---------------------------------------------------------------------------
  //using a gaussian 1d
  def getIntensityAtSimplier(source: Segment2D, pixelPosition: Point2D) =
    source.getRectangle().getCentroid().getDistance(Point2D_Double(pixelPosition))
  //---------------------------------------------------------------------------
  //using a gaussian 1d
  def getIntensityAtPosition1D(source: Segment2D
                               , pixelPosition: Point2D
                               , pixelIntensity: PIXEL_DATA_TYPE
                               , Isky: PIXEL_DATA_TYPE
                               , imageFWHM: Double) = {
    val Imax = source.getData().max.toDouble //peak faintPixelIntensity within object
    //do not estimate the B parameter, just use the fwhm to list the 'c' parameter
    // from wikipedia "c (the standard deviation, sometimes called the Gaussian RMS width) controls the width of the "bell"
    val c = imageFWHM / 2.35482 //see https://en.wikipedia.org/wiki/Gaussian_function
    val B = 1 / (2 * c * c)  //see https://en.wikipedia.org/wiki/Gaussian_function

    val objectCentroid = source.getRectangle().getCentroid()
    val distance = objectCentroid - Point2D_Double(pixelPosition)
    val r2 = distance.x + distance.x  //paper:equation 6

    Isky + ((Imax - Isky) * Math.exp(-B * r2))  //paper:equation 7
  }
  //---------------------------------------------------------------------------
  //using a gaussian 2d
  def getIntensityAtPosition2d(source: Segment2D
                               , pixelPosition: Point2D
                               , pixelIntensity: PIXEL_DATA_TYPE
                               , Isky: PIXEL_DATA_TYPE
                               , imageFWHM: Double) = {
    val Imax = source.getData().max.toDouble //peak faintPixelIntensity within object
    val objectCentroid = source.getRectangle().getCentroid()
    val distance = objectCentroid - Point2D_Double(pixelPosition)


    //do not estimate the B parameter, just use the fwhm to calculate the 'c' parameter
    // from wikipedia "c (the standard deviation, sometimes called the Gaussian RMS width) controls the width of the "bell"
    val c = imageFWHM / 2.35482 //see https://en.wikipedia.org/wiki/Gaussian_function
    val stdv_x = c
    val stdv_y = stdv_x

    Isky + ((Imax - Isky) * Math.exp( -1 *
      (((distance.x * distance.x) / (stdv_x * stdv_x * 2)) +
        (distance.y * distance.y) / (stdv_y * stdv_y * 2))))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file MyIntensityEstimator.scala
//=============================================================================