/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  01/Feb/2022
 * Time:  10h:48m
 * Description: None
 */
//=============================================================================
package com.common.geometry.segment.segment2D.deblend.algorithm
//=============================================================================
import com.common.configuration.MyConf
import com.common.dataType.pixelDataType.PixelDataType.{PIXEL_DATA_TYPE}
import com.common.geometry.point.Point2D
import com.common.geometry.segment.segment2D.Segment2D
import com.common.geometry.segment.segment2D.deblend.{ComponentTree}

import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object CosmosAlgorithm {
  //---------------------------------------------------------------------------
  private val DEBLEND_COSMOS_MIN_ALLOWED_CONTRIBUTION_PERCENTAGE = MyConf.c.getInt("Deblending.COSMOS.minAllowedIntensityPercentage")
  //---------------------------------------------------------------------------
}
//=============================================================================
import CosmosAlgorithm._
trait CosmosAlgorithm {
  //---------------------------------------------------------------------------
  def getIntensityAtPosition(source: Segment2D
                           , thereshold: PIXEL_DATA_TYPE
                           , pixelPosition: Point2D
                           , pixelIntensity: PIXEL_DATA_TYPE
                           , Isky: PIXEL_DATA_TYPE
                           , imageFWHM: Double) : Double
  //---------------------------------------------------------------------------
  def deblend(componentTree: ComponentTree, Isky: PIXEL_DATA_TYPE, imageFWHM: Double) : Array[Segment2D] = {
    if (componentTree.isEmpty) return Array(componentTree.getMainSource())
    val levelWithSplitSeq = componentTree.getLevelSeq(reverseLevel = true)  //sort levels, where the first one has the top threshold level (storing the local maxima values)
        .filter( _.hasASplit())              //avoid the levels without a split
    if (levelWithSplitSeq.isEmpty) return Array(componentTree.getMainSource()) //no splits (normal case in a perfect gaussian or in an undersample of thereshold range)

    //theorically these are the sources with local maxima according to the sample that generated the component tree
    val sourceSeedSeq = ArrayBuffer[Segment2D]() ++= componentTree.getLastLevel().getChildSeq()

    //used to not calculate pixel twice
    val processedPixelMap = scala.collection.mutable.Map[Point2D,PIXEL_DATA_TYPE]()

    //calculate the position of the pixels used in the seedSource seeds
    val gaussianSeedPixelSet = (sourceSeedSeq flatMap (_.getAbsolutePosWithPixelValue().toSet)).toSet

    //append to used pixels
    gaussianSeedPixelSet.foreach(p=> if (!processedPixelMap.contains(p._1)) processedPixelMap(p._1) = p._2 )

    //calculate all levels with a split
    levelWithSplitSeq.foreach { level =>

      //get the faint pixels (using CosmosIntensityEstimator terminology): pixels of the level that generated the split)
      val levelPixelSeq = level.getChildSeq flatMap (_.getAbsolutePosWithPixelValue())

      //redistribute the faint pixels in the seedSource seeds
      //evaluate how the sources of the level contribute to each faint pixel and augment each seedSource seed proportionally
      levelPixelSeq.foreach { case (faintPixelPosition, faintPixelIntensity) =>
        if (!processedPixelMap.contains(faintPixelPosition))  {
          processedPixelMap(faintPixelPosition) = faintPixelIntensity
          assigFaintPixel(sourceSeedSeq
            , level.thereshold
            , faintPixelPosition
            , faintPixelIntensity
            , Isky
            , imageFWHM)
        }
      }
    }
    sourceSeedSeq.toArray
  }

  //---------------------------------------------------------------------------
  private def assigFaintPixel (sourceSeedSeq: ArrayBuffer[Segment2D]
                               , levelThereshold: PIXEL_DATA_TYPE
                               , faintPixelPosition: Point2D
                               , faintPixelIntensity: PIXEL_DATA_TYPE
                               , Isky: PIXEL_DATA_TYPE
                               , imageFWHM: Double) : Unit = {

    val estimatedPixelIntensityMap = scala.collection.mutable.Map[Segment2D, Double]() //(seedSource , estimated pixel intensity)
    sourceSeedSeq.foreach { source =>
      val estimatedPixelIntensity = getIntensityAtPosition(source, levelThereshold, faintPixelPosition, faintPixelIntensity, Isky, imageFWHM)
      estimatedPixelIntensityMap(source) = estimatedPixelIntensity
    }
    val seedSource = estimatedPixelIntensityMap.toList.sortBy(_._2).head._1
    val newSource = seedSource.updateValue(faintPixelPosition, faintPixelIntensity)

    //update the seed source
    sourceSeedSeq(sourceSeedSeq.indexOf(seedSource)) = newSource

  }
  //---------------------------------------------------------------------------
  private def assigFaintPixelOriginal (sourceSeedSeq: ArrayBuffer[Segment2D]
                              , levelThereshold: PIXEL_DATA_TYPE
                              , faintPixelPosition: Point2D
                              , faintPixelIntensity: PIXEL_DATA_TYPE
                              , Isky: PIXEL_DATA_TYPE
                              , imageFWHM: Double) : Unit = {

    val estimatedPixelIntensityMap = scala.collection.mutable.Map[Segment2D,Double]() //(seedSource , estimated pixel intensity)
    sourceSeedSeq.foreach { source =>

      //calculate the estimated faintPixel intensity (pixel value) depending to the distance to the centre of the seedSource
      // and a model the seedSource intensity distribution (in the papre a Gaussian 1D of the profile of the seedSource)
      val estimatedPixelIntensity = getIntensityAtPosition(source, levelThereshold, faintPixelPosition, faintPixelIntensity, Isky, imageFWHM) //paper:equation 4, 5, 6 and 7

      //calculate the percentage of the pixel contribution regarding the actual pixel value
      val contributionPercentage = (faintPixelIntensity / estimatedPixelIntensity) * 100

      //ignore pixel contribution less than a certain min allowed percentage (paper:sectionName c)
      if (contributionPercentage > DEBLEND_COSMOS_MIN_ALLOWED_CONTRIBUTION_PERCENTAGE)
        estimatedPixelIntensityMap(source) = estimatedPixelIntensity
    }

    //paper: equation 8
    val allSourceContributionSum = estimatedPixelIntensityMap.values.sum
    estimatedPixelIntensityMap.map { case (seedSource, estimatedPixelIntensity) =>
      val contributionProbability = estimatedPixelIntensity / allSourceContributionSum

      //in the paper, problable there is a mistake. the should be (to takie into account the background):
      (faintPixelIntensity + Isky)  * contributionProbability
      //but in any case use the originla equation
      //val pixelIntesityAtSource = (faintPixelIntensity + Isky) * contributionProbability //modified from paper: adding '+ Isky'
      val pixelIntesityAtSource = faintPixelIntensity * contributionProbability //modified from paper: adding '+ Isky'
      if (pixelIntesityAtSource > levelThereshold) {

        //ignore pixels with a intensity below the detection threshold (paper:sectionName e)
        //append this pixel to the seedSource and to its parent
        val newSource = seedSource.updateValue(faintPixelPosition, pixelIntesityAtSource)

        //update the seed source
        sourceSeedSeq(sourceSeedSeq.indexOf(seedSource)) = newSource
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CosmosAlgorithm.scala
//=============================================================================
