//=============================================================================
package com.common.geometry.segment.segment2D
//=============================================================================
import java.awt.{Color, Font}
import java.awt.image.BufferedImage
import java.io.{BufferedWriter, File, FileWriter}
import java.util.concurrent.atomic.AtomicInteger
import javax.imageio.ImageIO
import scala.collection.mutable.ArrayBuffer
//=============================================================================
import com.common.image.estimator.centroid.Centroid.SOURCE_CENTROID_ALGORITHM
import com.common.dataType.pixelDataType.PixelDataType._
import com.common.geometry.matrix.matrix2D.Matrix2D
import com.common.geometry.segment.Segment1D
import com.common.util.util.Util
import com.common.geometry.point.Point2D
import com.common.geometry.rectangle.Rectangle
import com.common.dataType.tree.binary.interval.AVLI_Tree
import com.common.geometry.polygon.Polygon2D
import com.common.geometry.circle.CircleDouble
import com.common.coordinate.conversion.Conversion
import com.common.image.estimator.bellShape2D.BellShape2D_Fit
import com.common.util.path.Path
import com.common.geometry.point.Point2D_Double
import com.common.geometry.affineTransformation.AffineTransformation2D
import com.common.image.estimator.centroid.Centroid
import com.common.image.estimator.centroid.Centroid.SOURCE_CENTROID_ALGORITHM_CENTROID_NONE
import com.common.image.estimator.bellShape2D.simple.Gaussian2D_Simple
import com.common.math.regression.Regression
//=============================================================================
//=============================================================================
object Segment2D {
  //---------------------------------------------------------------------------
  type Source2D = Segment2D
  //---------------------------------------------------------------------------
  //https://www.w3schools.com/colors/colors_picker.asp
  //https://www.rapidtables.com/web/color/RGB_Color.html
  //https://html-color.codes/image
  val COLOR_PALETTE = IndexedSeq(
    0xff0000, 0x008B8B, 0x7FFFD4, 0xffbf00, 0xffff00, 0xbfff00, 0x80ff00, 0x40ff00
    , 0x00ff80, 0x00ffbf, 0x00bfff, 0x0080ff, 0x0040ff, 0x0000ff, 0x4000ff, 0x8000ff
    , 0xbf00ff, 0xff00ff, 0xff0080, 0xff0040, 0x800000, 0x808000, 0x008080, 0x000080
    , 0xFF7F50, 0xCD5C5C, 0xB8860B, 0x90EE90, 0x3CB371, 0xff4000, 0xff8000, 0x00BFFF
    , 0x808000, 0xbdb76b, 0x9acd32, 0x6b8e23, 0xe3f988, 0xd1e189, 0xa4c639, 0x8db600
    , 0xe8f48c, 0xffff66, 0xfada5e, 0xdfff00, 0xeeff1b, 0xffd800, 0xe6e200, 0xeeff1b
    , 0xffc0cb, 0xff69b4, 0xdb7093, 0xff1493, 0xffe4e1, 0xfddde6, 0xffb6c1, 0xf28dcd
    , 0xffa07a, 0xff4500, 0xdc143c, 0xfd5e53, 0xfe6f5e, 0xe97451, 0xff355e, 0xfe2712
  )
  //---------------------------------------------------------------------------
  private final val COLOR_RGB_WHITE   = 0xFFFFFF
  //---------------------------------------------------------------------------
  //pretty print
  private final val PRETTY_PRINT_VALUE_PAD_SIZE = 5
  private final val PRETTY_PRINT_VALUE_CELL_SEPARATOR = "|"
  private final val PRETTY_PRINT_ROW_HEADER =  "->|"
  private final val PRETTY_PRINT_ROW_VALUE_PAD_SIZE = 4
  private final val PRETTY_PRINT_COL_VALUE_PAD_SIZE = 4
  //---------------------------------------------------------------------------
  private val SERIALIZATION_MULTIPLE_SEGMENT_1D_DIVIDER = "!"
  private val SERIALIZATION_MULTIPLE_SEGMENT_2D_DIVIDER = "%"
  private val SERIALIZATION_ROW_DIVIDER = "#"
  //---------------------------------------------------------------------------
  def getCentroidCsvLineHeader(sep: String = "\t") =
    s"centroid_x$sep" +
      s"centroid_y"
  //---------------------------------------------------------------------------
  def getCsvLineHeader(sep: String = "\t") =
    s"source_id$sep" +
      s"centroid_x$sep" +
      s"centroid_y$sep" +
      s"centroid_ra$sep" +
      s"centroid_dec$sep" +
      s"centroid_ra_hms$sep" +
      s"centroid_dec_dms$sep" +
      s"pix_count$sep" +
      s"flux$sep" +
      s"fwhm$sep" +
      s"eccentricity$sep" +
      s"pixInPercentage"
  //---------------------------------------------------------------------------
  private val idGenerator = new AtomicInteger(-1)
  //---------------------------------------------------------------------------
  def resetID() = idGenerator.set(-1)
  //---------------------------------------------------------------------------
  def getNewID: Int = idGenerator.addAndGet(1)
  //---------------------------------------------------------------------------
  def apply(data: ArrayBuffer[ArrayBuffer[PIXEL_DATA_TYPE]], offset: Point2D) =
    Matrix2D(data,offset).toSegment2D()
  //---------------------------------------------------------------------------
  def apply(data: Array[Array[PIXEL_DATA_TYPE]], offset: Point2D) =
    Matrix2D(data, offset).toSegment2D()
  //---------------------------------------------------------------------------
  def deserialization(s: String) = {
    val split = s.split(SERIALIZATION_MULTIPLE_SEGMENT_2D_DIVIDER)
    val rowSeq = split(0).split(SERIALIZATION_ROW_DIVIDER) map (
      _.split(SERIALIZATION_MULTIPLE_SEGMENT_1D_DIVIDER) map (
        Segment1D.deserialization(_) ))
    val offsetSeq = split(1).split(",")
    val offset = Point2D(offsetSeq(0).toInt, offsetSeq(1).toInt)
    val id = split(2).toInt
    Segment2D(rowSeq, offset, id)
  }
  //---------------------------------------------------------------------------
  def saveAsImage(bi: BufferedImage, name: String, format: String = "png"): Unit =
    ImageIO.write(bi, format, new File(name + "." + format))
  //---------------------------------------------------------------------------
  def xAxisIntersects(segSeq: Array[Segment1D],seg: Segment1D) =
    segSeq flatMap {seg1D => if (seg1D intersects seg) Some(seg1D) else None}
  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
  def getDistanceFromPointToSegment(p: Point2D_Double, p1: Point2D_Double, p2: Point2D_Double) = {
    val A = p.x - p1.x
    val B = p.y - p1.y
    val C = p2.x - p1.x
    val D = p2.y - p1.y
    val dot = A * C + B * D
    val len_sq = C * C + D * D
    val param = if (len_sq != 0) (dot / len_sq).toDouble else -1d //in case of 0 length line
    val (xx,yy) = param match {
      case x if x < 0 => (p1.x,p1.y)
      case x if x > 1 => (p2.x,p2.y)
      case _          => (p1.x + param * C, p2.y + param * D)
    }
    val dx = p.x - xx
    val dy = p.y - yy
    Math.sqrt(dx * dx + dy * dy)
  }
  //----------------------------------------------------------------------------
  def synthesizeImageWithColor(sourceSeq: Array[Segment2D]
                               , xSize: Int
                               , ySize: Int
                               , blackAndWhite : Boolean = false
                               , pixZero: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE
                               , userPixColor : Option[Int] = Some(0xFFFFFF) //set to None for color random color in each source
                               , centroidColor: Option[Int] = None //set to None when no centroid must be show
                               , font: Option[Font] = Some(new Font(Font.MONOSPACED, Font.PLAIN, 10)) //Set to 'None' for no text on source
                               , fontColor: Color = Color.YELLOW
                               , fontOffsetY: Int = 1
                               , dict: scala.collection.mutable.Map[Long, String] = scala.collection.mutable.Map[Long, String]()
                               , differentColorSource: scala.collection.mutable.Map[Long, Color] = scala.collection.mutable.Map[Long, Color]()
                               , arrowDiffMap: Map[Int, Point2D] = Map[Int, Point2D]()
                               , pixelTransformation: Option[(Option[Seq[Double]],Option[Seq[Double]])] = None
                               , inverYAxis: Boolean = true
                               , additionalOffset: Point2D = Point2D.POINT_ZERO): BufferedImage = {
    //---------------------------------------------------------------------------
    val bmpImage = new BufferedImage(xSize, ySize, BufferedImage.TYPE_INT_RGB)
    val g2d = bmpImage.createGraphics()
    val bmpMinX   = bmpImage.getMinX
    val bmpMinY   = bmpImage.getMinY
    val bmpWidth  = bmpImage.getWidth
    val bmpHeight = bmpImage.getHeight
    //---------------------------------------------------------------------------
    def plotPixel(x: Int, y: Int, v: Int) = {

      if (x >= bmpMinX &&
        x < (bmpMinX + bmpWidth) &&
        y >= bmpMinY &&
        y < (bmpMinY + bmpHeight))
        bmpImage.setRGB(x, y, v)
    }
    //---------------------------------------------------------------------------
    def getColor(source: Source2D, sourceIndex: Int) = {
      if (differentColorSource.contains(source.id)) {
        val c  = differentColorSource(source.id)
        g2d.setColor(c)
        c.getRGB
      }
      else {
        g2d.setColor(fontColor)
        if (blackAndWhite) COLOR_RGB_WHITE
        else if (userPixColor.isDefined) userPixColor.get else COLOR_PALETTE(sourceIndex % COLOR_PALETTE.length)
      }
    }
    //---------------------------------------------------------------------------
    def applyTransformationX(x: Int) =
      if(pixelTransformation.isEmpty || pixelTransformation.get._1.isEmpty) x
      else Math.round(Regression.evaluatePolynomy(pixelTransformation.get._1.get,x)).toInt
    //---------------------------------------------------------------------------
    def applyTransformationY(y: Int) =
      if(pixelTransformation.isEmpty || pixelTransformation.get._2.isEmpty) y
      else Math.round(Regression.evaluatePolynomy(pixelTransformation.get._2.get,y)).toInt

    //---------------------------------------------------------------------------
    //https://stackoverflow.com/questions/2027613/how-to-draw-a-directed-arrow-line-in-java
    def drawArrowLine(x1: Int, y1: Int, x2: Int, y2: Int, d: Int, h: Int) = {
      val dx = x2 - x1
      val dy = y2 - y1
      val D = Math.sqrt(dx * dx + dy * dy)
      var xm: Double = D - d
      var xn: Double = xm
      var ym: Double = h
      var yn: Double = -h
      var x = .0d
      val sin: Double = dy / D
      val cos: Double = dx / D

      x = xm * cos - ym * sin + x1
      ym = xm * sin + ym * cos + y1
      xm = x

      x = xn * cos - yn * sin + x1
      yn = xn * sin + yn * cos + y1
      xn = x

      val xpoints = Array(applyTransformationX(x2), applyTransformationX(xm.toInt), applyTransformationX(xn.toInt))
      val ypoints = Array(applyTransformationY(y2), applyTransformationY(ym.toInt), applyTransformationY(yn.toInt))

      g2d.drawLine(applyTransformationX(x1), applyTransformationY(y1), applyTransformationX(x2), applyTransformationY(y2))
      g2d.fillPolygon(xpoints, ypoints, 3)
    }
    //---------------------------------------------------------------------------
    if (font.isDefined) {
      g2d.setFont(font.get)
      g2d.setColor(fontColor)
    }

    //init bmp
    for (x <- 0 until xSize;
         y <- 0 until ySize)
      bmpImage.setRGB(x, y, PIXEL_DATA_TYPE_TO_INT(pixZero))

    //render all pix of the source
    sourceSeq.zipWithIndex.foreach { case (source, sourceIndex) =>

      val offset = source.offset + additionalOffset

      //render all pixels of the source
      for ((row, index) <- source.rowSeq.zipWithIndex) {
        for (seg1D <- row) {
          val y = index
          for (x <- seg1D.s to seg1D.e) {
            if (seg1D(x - seg1D.s) != PIXEL_ZERO_VALUE) {
              val finalX = x + offset.x - 1 //bmp origin is in (0,0)
              var finalY = y + offset.y - 1 //bmp origin is in (0,0)
              finalY = if (inverYAxis) ySize -  finalY else finalY
              val pixColor = getColor(source, sourceIndex)
              plotPixel(finalX, finalY, pixColor)
            }
          }
        }
      }

      //centroid
      if (centroidColor.isDefined) {
        val centroid = source.getCentroid - Point2D_Double.POINT_ONE //bmp origin is in (0,0)
        val finalX = Math.round(centroid.x).toInt
        val finalY = if (inverYAxis) (ySize - Math.round(centroid.y) - 1).toInt else (Math.round(centroid.y) - 1).toInt
        plotPixel(finalX, finalY, centroidColor.get)

        //arrow diff
        if (arrowDiffMap.size > 0) {
          val endPoint = arrowDiffMap(source.id)
          val endPointFinalX = endPoint.x
          val endPointFinalY = if (inverYAxis) ySize - endPoint.y - 1 else endPoint.y - 1
          drawArrowLine(finalX, finalY, endPointFinalX, endPointFinalY, 8, 8)
        }
      }

      //font
      if (font.isDefined)  {
        val centroid =  source.getCentroid() - Point2D_Double.POINT_ONE //bmp origin is in (0,0)
        val finalX = Math.round(centroid.x)
        val finalY = if (inverYAxis) ySize - Math.round(centroid.y) else Math.round(centroid.y)
        val message = source.id.toString + (if (dict.contains(source.id)) "->" + dict(source.id) else "")
        g2d.drawString(message, finalX.toInt, (finalY-fontOffsetY).toInt)
      }
    }
    bmpImage
  }
  //-------------------------------------------------------------------------
  def synthesizeValueSeq(seg2D_Seq: Array[Segment2D]
                         , xSize: Int
                         , ySize: Int
                         , mainOffset : Point2D = Point2D.POINT_ZERO
                         , invertY_axis : Boolean = false) : Array[PIXEL_DATA_TYPE]  = {

    val m = Array.ofDim[PIXEL_DATA_TYPE](ySize, xSize)

    //init pix
    for (x <- 0 until xSize;
         y <- 0 until ySize)
      m(y)(x) = PIXEL_ZERO_VALUE

    //fill pix
    seg2D_Seq.zipWithIndex.foreach { case (seg2D, seg2D_Index) =>
      val offset = seg2D.offset - mainOffset
      for ((row ,index) <- seg2D.rowSeq.zipWithIndex) {
        for (seg1D <- row){
          val y = index + offset.y
          for(x <- seg1D.s to seg1D.e){
            val finalY =  if (invertY_axis) ySize - 1 - y else y
            m(finalY)(x + offset.x) = seg1D(x - seg1D.s)
          }
        }
      }
    }
    m.toIndexedSeq.flatten.toArray
  }
  //-------------------------------------------------------------------------
  def saveAsCsv(segSeq: Array[Segment2D]
                , csvFileName: String
                , imageOffset: Point2D = Point2D.POINT_ZERO
                , fileExtension: String = ".tsv"
                , sep: String ="\t"): Unit = {
    val includeAbsPosition = imageOffset != Point2D.POINT_ZERO
    val absPosHeader = if (includeAbsPosition) sep + Segment2D.getCentroidCsvLineHeader(sep) else ""
    val name = if (csvFileName.endsWith(fileExtension)) csvFileName else csvFileName + fileExtension
    val bw = new BufferedWriter(new FileWriter(new File(name)))
    bw.write("sequence" + sep + getCsvLineHeader(sep) + absPosHeader +   "\n")
    if (segSeq.isEmpty)  bw.write("#None unknown source detected\n")
    else{
      segSeq.zipWithIndex.foreach { case (seg2D, i) =>
        val line =
          f"$i%010d" +  sep +         //sequence
            seg2D.getAsCsvLine(sep) +
            (if (includeAbsPosition) sep + seg2D.getPosAsCsvLineWithOffset(imageOffset,sep) else "")
        bw.write(line + "\n")
      }
    }
    bw.close()
  }

  //-------------------------------------------------------------------------
  def saveAsCsvWithData(segSeq: Array[Segment2D]
                        , csvFileName: String
                        , imageOffset: Point2D = Point2D.POINT_ZERO
                        , fileExtension: String = ".tsv"
                        , sep: String = "\t"): Unit = {
    val includeAbsPosition = imageOffset != Point2D.POINT_ZERO
    val absPosHeader = if (includeAbsPosition) sep + Segment2D.getCentroidCsvLineHeader(sep) else ""
    val name = if (csvFileName.endsWith(fileExtension)) csvFileName else csvFileName + fileExtension
    val bw = new BufferedWriter(new FileWriter(new File(name)))
    bw.write("sequence" + sep + getCsvLineHeader(sep) + absPosHeader + s"${sep}source_data" + "\n")
    if (segSeq.isEmpty) bw.write("#None unknown source detected\n")
    else {
      segSeq.zipWithIndex.foreach { case (source, i) =>
        val line =
          f"$i%010d" + sep + //sequence
            source.getAsCsvLine(sep) +
            (if (includeAbsPosition) sep + source.getPosAsCsvLineWithOffset(imageOffset, sep) else "") + sep +
            source.serialization()
        bw.write(line + "\n")
      }
    }
    bw.close()
  }
  //---------------------------------------------------------------------------
  def merge(sourceSeq: Array[Segment2D]
            , sourceSizeRestriction: Option[(Int, Int)] = None): Array[Segment2D] = {
    if (sourceSeq.size < 2) sourceSeq
    else sourceSeq.head.merge(sourceSeq.drop(1),sourceSizeRestriction)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import Segment2D._
case class Segment2D(var rowSeq: Array[Array[Segment1D]]
                     , var offset : Point2D = Point2D.POINT_ZERO
                     , var id: Int = Segment2D.getNewID) {
  //---------------------------------------------------------------------------
  //centroid
  private var centroid  = Point2D_Double.POINT_NAN //origin at (1,1)
  private var lowResolutionCentroid = true
  private var centroidAlgorithm: SOURCE_CENTROID_ALGORITHM = SOURCE_CENTROID_ALGORITHM_CENTROID_NONE
  private var centroidRaDec = Point2D_Double(-1,-1)

  //flux
  private var flux: Double = -1
  private var snr = -1d //signal to noise ratio

  //geometry and models
  private var bellShapeFit : Option[BellShape2D_Fit] = None   //fwhm
  private var morphology: Option[Morphology] = None
  private var polygon : Polygon2D = null
  private var xAxisMap : Map[Int, Segment1D] = null
  private var xAxisTree: AVLI_Tree = null
  private var yAxisMap : Map[Int, Array[Segment1D]] = null
  private var surroundRectangle : Rectangle = null

  //additional info
  private var extraInfo: Int = -1
  private var catalogSourceID: Long = -1
  //---------------------------------------------------------------------------
  override def toString() = s"{$id} o:$offset s:$getElementCount"
  //---------------------------------------------------------------------------
  def hasLowResolutionCentroid = lowResolutionCentroid
  //---------------------------------------------------------------------------
  def setLowResolutionCentroid(b: Boolean) = lowResolutionCentroid = b
  //---------------------------------------------------------------------------
  def setCentroidAlgorithm(b: SOURCE_CENTROID_ALGORITHM) = centroidAlgorithm = b
  //---------------------------------------------------------------------------
  def getCentroidAlgorithm() = centroidAlgorithm
  //---------------------------------------------------------------------------
  def getElementCount() = rowSeq.flatten.map(_.getNonZeroValueCount).sum
  //---------------------------------------------------------------------------
  def getPixCount() = getElementCount
  //---------------------------------------------------------------------------
  def getFlux(force: Boolean = false) = {
    if (flux == -1 || force) flux = rowSeq.flatten.map(_.getFlux).sum
    flux
  }
  //---------------------------------------------------------------------------
  def contains(p: Point2D) : Boolean = {
    if (!isInY_AxisAbsolute(p.y)) return false
    if (!isInX_AxisAbsolute(p.x)) return false
    true
  }
  //---------------------------------------------------------------------------
  def isInY(y: Int) = (y >= 0) && (y <= (rowSeq.length - 1))
  //---------------------------------------------------------------------------
  def isInY_AxisAbsolute(y: Int) = (y >= offset.y) && (y <= (offset.y + rowSeq.length - 1))
  //---------------------------------------------------------------------------
  def isInX(x: Int) = {
    val axis = getX_Axis()
    (x >= axis.s) &&  (x <= axis.e)
  }
  //---------------------------------------------------------------------------
  def isInX_AxisAbsolute(x: Int) = {
    val axis = getX_AxisAbsolute()
    (x >= axis.s) &&  (x <= axis.e)
  }
  //---------------------------------------------------------------------------
  def isInAbsolute(pos: Point2D) = isInX_AxisAbsolute(pos.x) && isInY_AxisAbsolute(pos.y)
  //---------------------------------------------------------------------------
  def isInAbsolute(x: Int, y: Int) = isInX_AxisAbsolute(x) && isInY_AxisAbsolute(y)
  //---------------------------------------------------------------------------
  def isIn(x: Int, y: Int) = isInX(x) && isInY(y)
  //---------------------------------------------------------------------------
  def isInAbsoluteCoordinate(x:Int, y:Int) : Boolean = {
    if(!isInAbsolute(x,y)) return false
    else {
      rowSeq(y-offset.y).foreach{seg1D => if (seg1D.isIn(x - offset.x)) return true}
      false
    }
  }
  //---------------------------------------------------------------------------
  private def updateStorage() = {
    xAxisMap  = (for (t<-rowSeq.flatten) yield t.id->t).toMap
    xAxisTree = AVLI_Tree(rowSeq.flatten)
    yAxisMap  = (for ((t,i)<-rowSeq.zipWithIndex) yield i->t).toMap
  }
  //---------------------------------------------------------------------------
  def getX_AxisMap() = {
    if (xAxisMap == null) xAxisMap = (for (t<-rowSeq.flatten) yield t.id->t).toMap
    xAxisMap
  }
  //---------------------------------------------------------------------------
  def getX_AxisTree() = {
    if (xAxisTree == null) xAxisTree = AVLI_Tree(rowSeq.flatten)
    xAxisTree
  }
  //---------------------------------------------------------------------------
  def getY_AxisMap() = {
    if (yAxisMap == null) yAxisMap = (for ((t,i)<-rowSeq.zipWithIndex) yield i->t).toMap
    yAxisMap
  }
  //---------------------------------------------------------------------------
  //insert 'seg1D' after pos
  def insertY_Axis(y : Int, seg1D: Segment1D, pos: Int) = {
    val (l,m,r) = if (pos == -1) (Array(seg1D),Array[Segment1D](),rowSeq(y))
    else (rowSeq(y).slice(0,pos+1), Array(seg1D),rowSeq(y).slice(pos+1,rowSeq.length-1))
    val newArray = Array(l ++ m ++ r).filter(!_.isEmpty)
    rowSeq = if (y == 0) newArray ++ rowSeq.slice(1, rowSeq.length)
    else rowSeq.slice(0,y) ++ newArray ++ rowSeq.slice(y+1, rowSeq.length)

    val xAxisCorrection = Math.max(0, getX_Axis.s - seg1D.s)
    offset = Point2D(offset.x - xAxisCorrection, offset.y)
    updateStorage
  }
  //---------------------------------------------------------------------------
  def appendY_Axis(seg1D: Segment1D, y:Int) = {
    val before = rowSeq.slice(0,y)
    val newArray = Array(rowSeq(y) ++ Array(seg1D))
    val after = rowSeq.slice(y+1,rowSeq.length)
    rowSeq = before ++ newArray ++ after

    val xAxisCorrection = Math.max(0, getX_Axis.s - seg1D.s)
    offset = Point2D(offset.x - xAxisCorrection, offset.y)
    updateStorage
  }
  //---------------------------------------------------------------------------
  def appendY_axis(seg1D: Segment1D, stackAtEnd: Boolean = false) = {
    if (stackAtEnd && !rowSeq.isEmpty) {
      val newSeq = rowSeq.last ++ Array(seg1D)
      rowSeq = rowSeq.dropRight(1) :+ newSeq
    }
    else rowSeq = rowSeq:+ Array(seg1D)

    val xAxisCorrection = Math.max(0, getX_Axis.s - seg1D.s)
    offset = Point2D(offset.x - xAxisCorrection, offset.y)
    updateStorage
  }
  //---------------------------------------------------------------------------
  def preprendY_axis(seg1D: Segment1D, stackAtEnd: Boolean = false) = {
    if (stackAtEnd && !rowSeq.isEmpty){
      val newSeq = rowSeq.head ++ Array(seg1D)
      rowSeq = newSeq +: rowSeq.drop(1)
    }
    else rowSeq = Array(seg1D) +: rowSeq

    val xAxisCorrection = Math.max(0, getX_Axis.s - seg1D.s)
    offset = Point2D(offset.x - xAxisCorrection, Math.max(0,offset.y-1))
    updateStorage
  }
  //---------------------------------------------------------------------------
  def apply(i: Int) = rowSeq(i)
  //---------------------------------------------------------------------------
  def apply(_x: Int, y: Int, isAbsolute: Boolean = false) : Option[PIXEL_DATA_TYPE] = {
    val x = if (isAbsolute) _x - offset.x else _x
    rowSeq(y) foreach {seg => if (seg.isIn(x)) return Some(seg(x -seg.s))}
    None
  }
  //---------------------------------------------------------------------------
  def getZeroIfNotExistPos(_x: Int, y: Int, isAbsolute: Boolean = false) : PIXEL_DATA_TYPE = {
    val r = this(_x,y,isAbsolute)
    if(r.isEmpty) PIXEL_ZERO_VALUE
    else r.get
  }
  //---------------------------------------------------------------------------
  def getRowAbsolute(i: Int) = {
    val index = i - offset.y
    if ((index < 0) || (index >= rowSeq.length)) None
    else Some(rowSeq(index))
  }
  //---------------------------------------------------------------------------
  def setOffset(o: Point2D) = offset = o
  //---------------------------------------------------------------------------
  def findY_Pos(seg1D: Segment1D, absolute: Boolean = true): Int = {
    for ((row,i) <- rowSeq.zipWithIndex)
      for (seg <- row) if (seg == seg1D) return if (absolute) i + offset.y else i
    -1
  }
  //---------------------------------------------------------------------------
  def findY_Pos(id: Int, absolute: Boolean): Int = {
    for ((row,i) <- rowSeq.zipWithIndex)
      for (seg <- row) if (seg.id == id) return if (absolute) i + offset.y else i
    -1
  }
  //---------------------------------------------------------------------------
  def findX_Pos(y: Int,id: Int, absolute: Boolean): Int = {
    for ((seg1D,i) <- rowSeq(y).zipWithIndex)
      if (seg1D.id == id)  return if (absolute) i + offset.x else i
    -1
  }
  //---------------------------------------------------------------------------
  //first x pos where 'seg1D' starts after the end of one of the segments
  def findX_FirstPosBiggerSegment(y: Int, seg1D: Segment1D): Int = {
    if (!isInY(y)) -2
    else {
      rowSeq(y).reverse.zipWithIndex.foreach { case (seg,i)=>
        if(seg.e < seg1D.s) return  rowSeq.length - 1 - (i + offset.x)
      }
      -1
    }
  }
  //---------------------------------------------------------------------------
  def yAxisRowAt(y: Int) = getY_AxisMap()(y)
  //---------------------------------------------------------------------------
  //(x,y,segment)
  def getX_AxisIntersection(seg: Segment1D, absolute: Boolean = true) = {
    for (n <- getX_AxisTree().getIntersection(seg)) yield {
      val id = n.v
      val r = getX_AxisMap()(id)
      val y = findY_Pos(id, false)
      val x = findX_Pos(y, id, absolute)
      if (absolute) (x + offset.x, y + offset.y, r) else (x, y, r)
    }
  }
  //---------------------------------------------------------------------------
  def xAxisIntersects(seg: Segment1D) : Boolean =
    getX_AxisTree().getIntersection(seg).length > 0

  //---------------------------------------------------------------------------
  def merge(otherSeq: Array[Segment2D]
            , sourceSizeRestriction: Option[(Int, Int)] = None): Array[Segment2D] = {

    val xAxis = getX_AxisAbsolute()
    val yAxis = getY_AxisAbsolute()

    var xMin = xAxis.s
    var xMax = xAxis.e

    var yMin = yAxis.s
    var yMax = yAxis.e

    otherSeq.foreach { other =>
      val xAxis = other.getX_AxisAbsolute()
      val yAxis = other.getY_AxisAbsolute()
      xMin = Math.min(xMin ,xAxis.s)
      xMax = Math.max(xMax, xAxis.e)

      yMin = Math.min(yMin, yAxis.s)
      yMax = Math.max(yMax, yAxis.e)
    }

    //build a matrix with all sources
    val xRange = xMax-xMin+1
    val yRange = yMax-yMin+1
    val mergedSourceSeq = ArrayBuffer.fill(xRange * yRange)(PIXEL_ZERO_VALUE)
    val mergedOffset = Point2D(xMin,yMin)
    (otherSeq :+ this)
      .foreach { source=>
      source.getPosSeq(absolutePos = false).foreach { case (x, y, pix) =>
        val ofsset = source.offset - mergedOffset
        val pos = ((y + ofsset.y) * xRange) + (x + ofsset.x)
        mergedSourceSeq(pos) = pix
      }
    }
    //detect the new sources
    val mergedMatrix = Matrix2D(xRange
                               , yRange
                               , mergedSourceSeq.toArray
                               , mergedOffset)
    mergedMatrix.findSource(noiseTide = PIXEL_ZERO_VALUE
                            , sourceSizeRestriction = sourceSizeRestriction).toSegment2D_seq()
  }
  //---------------------------------------------------------------------------
  def getY_Axis() = Segment1D(0, rowSeq.length-1, Array(), id)
  //---------------------------------------------------------------------------
  def getX_Axis() = {
    val r = (for (row <- rowSeq) yield
      for (seg <- row)  yield
        (seg.s, seg.e)).flatten
    val (sSeq,eSeq) = r.unzip
    Segment1D(sSeq.min,eSeq.max, Array())
  }
  //---------------------------------------------------------------------------
  def getY_AxisAbsolute() = {
    val seg = getY_Axis
    Segment1D(seg.s + offset.y, seg.e + offset.y, seg.d)
  }
  //---------------------------------------------------------------------------
  def getX_AxisAbsolute() = {
    val seg = getX_Axis
    Segment1D(seg.s + offset.x, seg.e + offset.x, seg.d)
  }
  //---------------------------------------------------------------------------
  def getSeqSegment1D() = rowSeq.flatten
  //---------------------------------------------------------------------------
  def getRowCount() = rowSeq.length
  //---------------------------------------------------------------------------
  def getColCount() = getX_Axis.range
  //---------------------------------------------------------------------------
  def getCountSegment1D() = (for(row<-rowSeq) yield row.length).sum
  //---------------------------------------------------------------------------
  def getIsRowAllZeroes(rowIndex: Int) = {
    rowSeq(rowIndex).forall( _.d.forall( _ == PIXEL_ZERO_VALUE) )
  }
  //---------------------------------------------------------------------------
  def getOffsetFirstPixNoZeroe() : Point2D = {
    //-------------------------------------------------------------------------
    //find first row empty
    def firsRowNoEmpty(): Int = {
      for(rowIndex<-0 until rowSeq.length) if(!getIsRowAllZeroes(rowIndex)) return rowIndex
      -1
    }
    //-------------------------------------------------------------------------
    val rowIndex = firsRowNoEmpty()
    rowSeq(rowIndex).head.d.zipWithIndex.foreach( t=> if (t._1 != PIXEL_ZERO_VALUE) return Point2D(offset.x + t._2, offset.y + rowIndex) )
    offset
  }
  //---------------------------------------------------------------------------
  def equal(sseg: Segment2D) : Boolean = {
    if (sseg == null) return false
    if (offset != sseg.offset) return false
    if (getCountSegment1D != sseg.getCountSegment1D) return false
    if (rowSeq.length != sseg.rowSeq.length) return false
    for (t <- getSeqSegment1D zip sseg.getSeqSegment1D)
      if (t._1 != t._2) return false
    true
  }
  //---------------------------------------------------------------------------
  //get surround rectangle in relative coordinates
  def getRectangleRelativeCoordinates() = {
    val xAxis = getX_Axis
    val yAxis = getY_Axis
    Rectangle( Point2D(xAxis.s, yAxis.s), Point2D(xAxis.e, yAxis.e) )
  }
  //---------------------------------------------------------------------------
  //get surround rectangle in absolute coordinates
  def getRectangle() = {
    if (surroundRectangle == null) {
      val xAxis = getX_Axis()
      val yAxis = getY_Axis()
      surroundRectangle = Rectangle(Point2D(offset.x, offset.y),
                                    Point2D(xAxis.e + offset.x, yAxis.e + offset.y) )
    }
    surroundRectangle
  }
  //---------------------------------------------------------------------------
  def getCentroid(): Point2D_Double = {
    if (centroid.oneComponentIsNan()) Centroid.calculateCentroidByAlgorithm(this)
    centroid
  }
  //---------------------------------------------------------------------------
  def getCentroidWithAft(aft: AffineTransformation2D): Point2D_Double =
    aft(getCentroid())
  //---------------------------------------------------------------------------
  def recalculateCentroid() = Centroid.calculateCentroidByAlgorithm(this)
  //---------------------------------------------------------------------------
  def setCentroidRaw(c: Point2D_Double) = centroid = c
  //---------------------------------------------------------------------------
  def getCentroidRaDec() = centroidRaDec
  //---------------------------------------------------------------------------
  def setCentroidRaDec(p:Point2D_Double) = centroidRaDec = p
  //---------------------------------------------------------------------------
  def toMatrix2D(fillValue: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE) = {
    val xRange = getX_Axis.range
    val data =
      (for (row <- rowSeq)  yield {
        val outputRow = ArrayBuffer.fill(xRange)(fillValue)
        for (seg <- row)
          for (x <- seg.s to seg.e)
            outputRow(x) = seg(x - seg.s)
        outputRow.toArray
      }).flatten
    Matrix2D(xRange, getY_Axis.range, data, offset,"converted")
  }

  //---------------------------------------------------------------------------
  def getCroppedCentredMaxPixel(pixRadius: Int = 1
                                , fillValue: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE) = {

    val m = toMatrix2D(fillValue)
    val (_, _, _, pixMaxPosSeq) = m.findMinMax()
    val (idx, idy) = {
      val c = centroid.toPoint2D() - offset
      if (pixMaxPosSeq.length > 1) {
        val closedTocentrePos = pixMaxPosSeq.map (p=> (p,p.getDistance(c))).sortWith( _._2 < _._2).head._1
        (closedTocentrePos.x, closedTocentrePos.y)
      }
      else (pixMaxPosSeq(0).x, pixMaxPosSeq(0).y)
    }

    val posMax = Point2D(idx,idy)

    val minPos = Point2D(Math.max(0, posMax.x - pixRadius), Math.max(0, posMax.y - pixRadius))
    val maxPos = Point2D(Math.min(m.xMax-1, posMax.x + pixRadius), Math.min(m.yMax-1, posMax.y + pixRadius))

    val centredSumatrix = m.getSubMatrixWithPadding(minPos
                                                    , maxPos
                                                    , paddingValue = fillValue)
    val seg = centredSumatrix.toSegment2D_ByDetection(fillValue).get
    val gas = Gaussian2D_Simple.fit(centredSumatrix, Some(pixRadius))
    seg.id = id
    seg.centroid = gas.centroid
    seg.setBellShapeFit(gas)
    seg
  }
  //---------------------------------------------------------------------------
  def transpose() = {
    val xAxisTree = AVLI_Tree(rowSeq.flatten)
    val segment1D_Map = (for (t<-rowSeq.flatten) yield t.id->t).toMap
    val xAxis = getX_Axis
    val segSeq =
      for(x<- xAxis.s to xAxis.e) yield {
        val yPosIdSeq = for (itv<- xAxisTree.getIntersection(Segment1D(x))) yield (findY_Pos(segment1D_Map(itv.v),absolute=false),itv.v)
        val r = Util.getCoalescentIntIntervalSeq(yPosIdSeq)
        for (row <- r) yield {
          val (ySeq, segIdSeq) = row.unzip
          val d = for (segId <- segIdSeq) yield {
            val seg = segment1D_Map(segId)
            seg(x - seg.s)
          }
          Segment1D(ySeq.head, ySeq.last, d)
        }
      }
    Segment2D(segSeq.toArray, Point2D(offset.y,offset.x))
  }
  //---------------------------------------------------------------------------
  def saveAsText(name: String): Unit = toMatrix2D().saveAsCoordinateSeq(name)
  //---------------------------------------------------------------------------
  def saveAsTextStacking(bw: BufferedWriter
                         , o: Point2D = offset
                         , predicate: Option[PIXEL_DATA_TYPE => Boolean] = None): Unit =
    toMatrix2D().saveAsCoordinateSeqStacking(bw,o, predicate)
  //---------------------------------------------------------------------------
  def getXY_IntersectionAtRow(seg1D: Segment1D, y: Int) = {
    val segSeq = this(y)
    if(segSeq.isEmpty) None
    else {
      val r = (for (s<-segSeq) yield {
        if (s intersects seg1D) Some(Segment1D(s.s, s.e, Array(), id))
        else None
      }).flatten
      Some(r)
    }
  }
  //--------------------------------------------------------------------------
  def getIntersection(segB: Segment2D, storeData: Boolean = false): Option[Segment2D]  = {
    //check if box hull isInForbiddenArea
    val recIntersection = getRectangle().getIntersection(segB.getRectangle()).getOrElse(return None)
    val recIntersectionX_Axis = Segment1D(recIntersection.min.x, recIntersection.max.x)
    //------------------------------------------------------------------------
    case class partialResult(o: Point2D, s: Segment1D)
    val storageMap = scala.collection.mutable.Map[Int, ArrayBuffer[partialResult]]()
    //------------------------------------------------------------------------
    def addToStorage(y: Int, o: Point2D, s: Segment1D): Unit = {
      if (storageMap.contains(y)) storageMap(y).append(partialResult(o,s))
      else storageMap(y) = ArrayBuffer(partialResult(o,s))
    }
    //-------------------------------------------------------------------------
    def builResult() = {
      if (storageMap.isEmpty)  None
      else{
        val sortedSeqY = storageMap.keySet.toArray.sorted
        var o: Point2D = Point2D(Int.MaxValue,Int.MaxValue)
        storageMap.values.flatten map { pr =>  o = Point2D(Math.min(o.x, pr.o.x), Math.min(o.y, pr.o.y)) }
        val maxY = sortedSeqY.last
        val seg1dSeq = (for(y<- 0 to maxY) yield {
          if (storageMap.contains(y)) (storageMap(y) map { r => Segment1D(r.s, -o.x) }).toArray
          else Array[Segment1D]()   //empty row
        }).toArray
        Some(Segment2D(seg1dSeq, o))
      }
    }
    //-------------------------------------------------------------------------
    for(y<- recIntersection.min.y to recIntersection.max.y) {
      val rowA = getRowAbsolute(y)
      val rowB = segB.getRowAbsolute(y)
      if (rowA.isDefined && rowB.isDefined) {
        rowA.get foreach { sa =>
          rowB.get foreach { sb =>
            val subA = Segment1D(sa, offset.x).getIntersection(recIntersectionX_Axis, true)
            val subB = Segment1D(sb, segB.offset.x).getIntersection(recIntersectionX_Axis,true)
            if (subA.isDefined && subB.isDefined) {
              val its = subA.get.getIntersection(subB.get,storeData)
              if (its.isDefined) addToStorage(y - recIntersection.min.y, Point2D(its.get.s, y), its.get)
            }
          }
        }
      }
    }
    builResult
  }
  //---------------------------------------------------------------------------
  def getSizeMinMaxElementCount(seg2D: Segment2D) = {
    val a = getElementCount
    val b = seg2D.getElementCount
    if (a < b) (a, b) else (b, a)
  }
  //---------------------------------------------------------------------------
  def getSizeMinMaxFlux(seg2D: Segment2D) = {
    val a = flux
    val b = seg2D.flux
    if (a < b) (a, b) else (b, a)
  }
  //---------------------------------------------------------------------------
  def getIntersectionPercentageVariation(seg2D: Segment2D): Float = {
    val its = getIntersection(seg2D)
    if (its.isEmpty)  return 100f
    val a = its.get.getElementCount
    val b = Math.min(getElementCount,seg2D.getElementCount)
    val (min,max) = if (a < b) (a,b) else (b,a)
    100 - ((min / max.toFloat) * 100)
  }
  //---------------------------------------------------------------------------
  def getSizePercentageVariation(seg2D: Segment2D): Float = {
    val (min,max) = getSizeMinMaxElementCount(seg2D)
    if (Math.abs(min-max) <= 2) return 0 //useful with small ones
    100 - ((min / max.toFloat) * 100)
  }

  //---------------------------------------------------------------------------
  def getFluxPercentageVariation(seg2D: Segment2D): Float = {
    val (min, max) = getSizeMinMaxFlux(seg2D)
    (100 - ((min / max.toFloat) * 100)).toFloat
  }
  //---------------------------------------------------------------------------
  def isSameShapeAs(seg2D_B: Segment2D) : Boolean = {  //ignore the offset and data value, just the structure of segment1D
    if (seg2D_B.getElementCount != getElementCount) return false
    if (seg2D_B.rowSeq.length != rowSeq.length) return false
    for(i<- 0 until rowSeq.length) {
      val rowA =  rowSeq(i)
      val rowB =  seg2D_B.rowSeq(i)
      if (rowA.length != rowB.length) return false
      for(k<- 0 until rowA.length) {
        val sA = rowA(k)
        val sB = rowB(k)
        if (sA.s != sB.s) return false
        if (sA.e != sB.e) return false
      }
    }
    true
  }
  //---------------------------------------------------------------------------
  def serializationData() =
    (rowSeq map ( _.map( _.serialization).mkString(Segment2D.SERIALIZATION_MULTIPLE_SEGMENT_1D_DIVIDER) ))
      .mkString(Segment2D.SERIALIZATION_ROW_DIVIDER)
  //---------------------------------------------------------------------------
  def serialization() = {
    serializationData + Segment2D.SERIALIZATION_MULTIPLE_SEGMENT_2D_DIVIDER +
      s"${offset.x},${offset.y}" + Segment2D.SERIALIZATION_MULTIPLE_SEGMENT_2D_DIVIDER +
      s"$id"
  }
  //---------------------------------------------------------------------------
  def getPosAsCsvLineWithOffset(o: Point2D, sep: String ="\t") = {
    val centroid = getCentroid + Point2D_Double(o)
    s"${centroid.x}" + sep + //centroid.x
      s"${centroid.y}"       //centroid.y
  }
  //---------------------------------------------------------------------------
  def getPosAsCsvLine(sep: String ="\t") : String =
    getPosAsCsvLine(getCentroid(), sep)
  //---------------------------------------------------------------------------
  def getPosAsCsvLine(centroid: Point2D_Double, sep: String) : String = {
    val centroidRaDec = getCentroidRaDec()
    val ra_hms = Conversion.DD_to_HMS_WithDivider(centroidRaDec.x,divider = ":")
    val dec_dms = Conversion.DD_to_DMS_WithDivider(centroidRaDec.y,divider = ":")
    s"${centroid.x}" + sep +          //centroid.x
      s"${centroid.y}" + sep +         //centroid.y
      s"${centroidRaDec.x}"  + sep +   //centroid_ra
      s"${centroidRaDec.y}"  + sep +   //centroid_dec
      s"$ra_hms"  + sep +              //centroid_ra_hms
      s"$dec_dms"  + sep               //centroid_dec_dms
  }
  //---------------------------------------------------------------------------
  def getAsCsvLine(sep: String ="\t") = {
    f"$id%010d" + sep +              //source_id
      getPosAsCsvLine(sep) +
      s"$getElementCount" + sep +      //pix_count
      s"${getFlux()}" + sep +          //flux
      s"$getFwhm" + sep +              //fwhm
      s"${getMorphology.eccentricity}" + sep + //eccentricity
      s"${getMorphology.pixelInPercentage}"  //pixel in percentage
  }
  //---------------------------------------------------------------------------
  def hasExtraInfo = extraInfo != -1
  //---------------------------------------------------------------------------
  def getExtraInfo = extraInfo
  //---------------------------------------------------------------------------
  def setExtraInfo(o: Int) = extraInfo = o
  //---------------------------------------------------------------------------
  def hasCatalogSourceID = catalogSourceID != -1
  //---------------------------------------------------------------------------
  def getCatalogSourceID = catalogSourceID
  //---------------------------------------------------------------------------
  def setCatalogSourceID(o: Long) = catalogSourceID = o
  //---------------------------------------------------------------------------
  def getEdge(fillValue: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE) : Segment2D = {
    //-------------------------------------------------------------------------
    val xMax = getX_Axis.range
    val yMax = getY_Axis.range

    val xMaxIdex = xMax - 1
    val yMaxIdex = yMax - 1
    //-------------------------------------------------------------------------
    def hasHoleBelow(x: Int, y: Int): Boolean = {
      if (y == 0) return true
      if (getZeroIfNotExistPos(x,y-1) == PIXEL_ZERO_VALUE) true
      else false
    }
    //-------------------------------------------------------------------------
    def hasHoleAbove(x: Int, y: Int): Boolean = {
      if (y == yMaxIdex) return true
      if (getZeroIfNotExistPos(x,y+1) == PIXEL_ZERO_VALUE) true
      else false
    }
    //-------------------------------------------------------------------------
    def hasHoleOnLeft(x: Int, y: Int): Boolean = {
      if (x == 0) return true
      if (getZeroIfNotExistPos(x-1,y) == PIXEL_ZERO_VALUE) true
      else false
    }
    //-------------------------------------------------------------------------
    def hasHoleOnRight(x: Int, y: Int): Boolean = {
      if (x == xMaxIdex) return true
      if (getZeroIfNotExistPos(x+1,y) == PIXEL_ZERO_VALUE) true
      else false
    }
    //-------------------------------------------------------------------------
    val edgeSeq = ArrayBuffer[Point2D]()
    for (y <- 0 to yMaxIdex;
         x <- 0 to xMaxIdex)
      if (getZeroIfNotExistPos(x, y) != PIXEL_ZERO_VALUE)
        if (hasHoleBelow(x, y)) edgeSeq += Point2D(x, y)
        else if (hasHoleAbove(x, y)) edgeSeq += Point2D(x, y)
        else if (hasHoleOnLeft(x, y)) edgeSeq += Point2D(x, y)
        else if (hasHoleOnRight(x, y)) edgeSeq += Point2D(x, y)

    //create an empty matrix and fill it with the edge positions
    val data = ArrayBuffer.fill[PIXEL_DATA_TYPE](xMax * yMax) (PIXEL_ZERO_VALUE)
    edgeSeq foreach { pos=>
      data( (pos.y * xMax) + pos.x) = this(pos.x, pos.y).get
    }
    val resultM = Matrix2D(xMax,yMax,data.toArray,offset).toSegment2D_ByDetection(fillValue).get
    if (resultM.getElementCount == 0)
      Segment2D(Array(Array[Segment1D](Segment1D(0,0,Array(PIXEL_ZERO_VALUE)))),offset)  //just a single pixel with zero value
    else Matrix2D(xMax,yMax,data.toArray,offset).toSegment2D_ByDetection(fillValue).get
  }
  //---------------------------------------------------------------------------
  def getBinMap() = {
    val xAxis = getX_Axis
    val yAxis = getY_Axis
    val xMax = xAxis.range
    val d = ArrayBuffer.fill[PIXEL_DATA_TYPE](xMax * yAxis.range)(0)
    //---------------------------------------------------------------------------
    rowSeq.zipWithIndex foreach  { case (row,y) =>
      for(x<- xAxis.s to xAxis.e) {
        if(!row.isEmpty)
          row foreach (s1D => if(s1D.isIn(x)) d( (y * xMax) + x) = 1) //get affine_transformation element without checking
      }
    }
    Matrix2D(xAxis.range, yAxis.range, d.toArray, offset)
  }
  //---------------------------------------------------------------------------
  def getData() = (rowSeq.flatten map (_.d)).flatten
  //---------------------------------------------------------------------------
  def getMinValue() = getData().min
  //---------------------------------------------------------------------------
  def getMaxValue() = getData().max
  //---------------------------------------------------------------------------
  def getPosByElement(o: Point2D = Point2D.POINT_ZERO) =
    rowSeq.zipWithIndex flatMap { case (row,y) =>
      row flatMap { s1D=> for(x<-s1D.s to s1D.e) yield Point2D(x,y) + o} }
  //---------------------------------------------------------------------------
  def getPosByElementWithPixelValue(o: Point2D = Point2D.POINT_ZERO) =
    rowSeq.zipWithIndex flatMap { case (row,y) =>
      row flatMap { s1D=> for(x<-s1D.s to s1D.e) yield (Point2D(x,y) + o, s1D(x-s1D.s)) }}
  //---------------------------------------------------------------------------
  def getAbsolutePosByElement(o: Point2D = Point2D.POINT_ZERO) = {
    val oX = offset.x + o.x
    val oY = offset.y + o.y
    rowSeq.zipWithIndex flatMap { case (row,y) =>
      row flatMap { s1D=> for(x<-s1D.s to s1D.e) yield Point2D(oX + x,oY + y)} }
  }
  //---------------------------------------------------------------------------
  def getAbsolutePosWithPixelValue(o: Point2D = Point2D.POINT_ZERO) = {
    val oX = offset.x + o.x
    val oY = offset.y + o.y
    rowSeq.zipWithIndex flatMap { case (row,y) =>
      row flatMap { s1D=> for(x<-s1D.s to s1D.e) yield (Point2D(oX + x,oY + y), s1D(x-s1D.s)) }}
  }
  //---------------------------------------------------------------------------
  def getMinMax() = {
    val xAxis = getX_Axis
    val yAxis = getY_Axis
    (Point2D(xAxis.s,yAxis.s), Point2D(xAxis.e,yAxis.e))
  }
  //---------------------------------------------------------------------------
  def getAbsoluteMinMax(o: Point2D = Point2D.POINT_ZERO) ={
    val xAxis = getX_AxisAbsolute()
    val yAxis = getY_AxisAbsolute()
    (Point2D(xAxis.s,yAxis.s) + o, Point2D(xAxis.e,yAxis.e) + o)
  }
  //---------------------------------------------------------------------------
  def getMaxY_Absolute = rowSeq.length -1 + offset.y
  //-------------------------------------------------------------------------
  def getMinMaxPosSeq() = {
    val d = getData()
    val (min,max) = (d.min, d.max)
    val minPosSeq = ArrayBuffer[Point2D]()
    val maxPosSeq = ArrayBuffer[Point2D]()
    rowSeq.zipWithIndex foreach  { case (row,y) => row foreach {
      s1D=> for(x<-s1D.s to s1D.e) {
        if (s1D(x - s1D.s) == min) minPosSeq += Point2D(x,y)
        if (s1D(x - s1D.s) == max) maxPosSeq += Point2D(x,y)
      }
    }}
    (minPosSeq.toArray, maxPosSeq.toArray)
  }
  //-------------------------------------------------------------------------
  def getAbsoluteMinMaxPosSeq() = {
    val (minPosSeq, maxPosSeq) = getMinMaxPosSeq
    (minPosSeq map (_ + offset), maxPosSeq  map (_ + offset))
  }
  //---------------------------------------------------------------------------
  def getMorphology() = {
    if (!morphology.isDefined) morphology = Some(Morphology.calculate(this))
    morphology.get
  }
  //---------------------------------------------------------------------------
  def getFwhm() = if (bellShapeFit.isDefined) bellShapeFit.get.fwhm else -1
  //---------------------------------------------------------------------------
  def setBellShapeFit(g:BellShape2D_Fit) = bellShapeFit = Some(g)
  //---------------------------------------------------------------------------
  def getBellShapeFit() = bellShapeFit
  //---------------------------------------------------------------------------
  def setSNR(_snr: Double) = snr = _snr
  //---------------------------------------------------------------------------
  def getSNR() = snr
  //---------------------------------------------------------------------------
  def getPolygon(o: Point2D = Point2D.POINT_ZERO) = {
    if (polygon == null) polygon = Polygon2D(getEdge().getAbsolutePosByElement(o))
    polygon
  }
  //---------------------------------------------------------------------------
  def getBoxHull(o: Point2D = Point2D.POINT_ZERO) = {
    val (min,max) = getAbsoluteMinMax(o)
    Rectangle(min,max)
  }
  //---------------------------------------------------------------------------
  def getMinDistanceFromPointToConvexHull(p: Point2D, o: Point2D = Point2D.POINT_ZERO) = {
    var minDistance : Double = Double.MaxValue
    val hull = getPolygon().getVertexSeqAsPoint2D
    hull.sliding(2).foreach { s =>
      val d = getDistanceFromPointToSegment(Point2D_Double(p),Point2D_Double(s(0)),Point2D_Double(s(1)))
      if (d < minDistance) minDistance = d
    }
    minDistance
  }
  //---------------------------------------------------------------------------
  def getMinDistanceFromPoint(p: Point2D, o: Point2D = Point2D.POINT_ZERO) = {
    var minDistance : Double = Double.MaxValue
    val hull = getPolygon().getVertexSeqAsPoint2D
    hull.sliding(2).foreach { s =>
      val d = Segment2D.getDistanceFromPointToSegment(Point2D_Double(p),Point2D_Double(s(0)),Point2D_Double(s(1)))
      if (d < minDistance) minDistance = d
    }
    minDistance
  }
  //---------------------------------------------------------------------------
  def getMaxInnerCircleCentred(centroid: Point2D_Double) : Option[CircleDouble] = {
    if (centroid.oneComponentIsNan()) return None
    var minDistance : Double = Double.MaxValue
    val hull = getPolygon().getVertexSeqAsPoint2D
    hull.foreach { p =>
      val d = centroid.getDistance(Point2D_Double(p))
      if (d < minDistance) minDistance = d
    }
    Some(CircleDouble(centroid, minDistance))
  }
  //---------------------------------------------------------------------------
  //it can not be centred on the source
  def getMaxInnerCircle() = getPolygon().getMaxInnerCircle()
  //---------------------------------------------------------------------------
  def hasHole() : Boolean = {
    rowSeq foreach { row => if (row.length > 1) return true}
    false
  }
  //---------------------------------------------------------------------------
  def round(minValue: PIXEL_DATA_TYPE
            , minCount: Int = 2
            , fillValue: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE): Option[Source2D] = {

    if (minCount == 0) return Some(this)

    val roundedSource = toMatrix2D()
      .round(minValue, minCount, fillValue)
      .toSegment2D_ByDetection(fillValue).getOrElse(return None)

    //update id
    roundedSource.id = id

    //update centroid
    roundedSource.recalculateCentroid()

    Some(roundedSource)
  }
  //---------------------------------------------------------------------------
  def getPosSeq() : Array[(Int, Int)] = getPosSeq(false) map (t=> (t._1, t._2))
  //---------------------------------------------------------------------------
  def getPosSeqWithValues() = getPosSeq(false)
  //---------------------------------------------------------------------------
  def getAbsolutePosSeq() = getPosSeq(true) map (t=> (t._1, t._2))
  //---------------------------------------------------------------------------
  private def getPosSeq(absolutePos: Boolean): Array[(Int, Int, PIXEL_DATA_TYPE)] = {
    val o = if (absolutePos) offset else Point2D.POINT_ZERO
    rowSeq.zipWithIndex.flatMap { case (row,yPos) => row.map { s =>
      s.getPosSeqWithValues().map {t => (o.x + t._1, o.y + yPos, t._2)}
    }}.flatten
  }
  //---------------------------------------------------------------------------
  def getAbsolutePosSeqWithValues() = getPosSeq(true)
  //---------------------------------------------------------------------------
  //it expands the matrix it is needed
  def updateValue(position: Point2D
                  , value: PIXEL_DATA_TYPE
                  , fillValue: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE) : Segment2D = {
    val m = toMatrix2D()
    val pos = position - offset
    if (isInAbsolute(position)) {
      val i = m.getFlatPos(pos)
      m.updateSingleValue(i,value).toSegment2D()
    }
    else {
      val expandedM = m.expand(pos,fillValue)
      val i = expandedM.getFlatPos(position - expandedM.offset)
      expandedM.updateSingleValue(i,value).toSegment2D()
    }
  }
  //---------------------------------------------------------------------------
  def getProfile(topProfile: Boolean, absolutePos: Boolean, fillValue: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE) = {
    //-------------------------------------------------------------------------
    def findPos(seq: Array[PIXEL_DATA_TYPE], value: PIXEL_DATA_TYPE) : (PIXEL_DATA_TYPE,PIXEL_DATA_TYPE) = {
      seq.zipWithIndex.foreach { case (v,y) => if (v == value) return (y, v) }
      (PIXEL_ZERO_VALUE,PIXEL_ZERO_VALUE)
    }
    //-------------------------------------------------------------------------
    val o = if (absolutePos) offset else Point2D.POINT_ZERO
    val m = toMatrix2D(fillValue)
    val r  = m.getAsColSeq().zipWithIndex.map { case (col,x) =>
      val sortedData = col.sorted.filter( _ != fillValue).sorted
      val r = if (topProfile) findPos(col, sortedData.last) else findPos(col, sortedData.head)
      (x + o.x, r._1 + o.y, r._2)
    }
    r
  }
  //---------------------------------------------------------------------------
  def profileToCSV(csvFileName: String
                   , topProfile: Boolean
                   , absolutePos: Boolean
                   , sep: String = "\t"
                   , fillValue: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE) = {
    val bw = new BufferedWriter(new FileWriter(new File(csvFileName)))
    bw.write(s"#x${sep}y${sep}z\n")
    getProfile(topProfile, absolutePos, fillValue) foreach { t=> bw.write(s"${t._1}$sep${t._2}$sep${t._3}\n")}
    bw.close()
  }
  //---------------------------------------------------------------------------
  def profileToGnuPlot(baseDir: String
                       , topProfile: Boolean
                       , absolutePos: Boolean
                       , fillValue: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE) = {
    val gnuFileName = baseDir + s"/source_profile_$id.gnu"
    val csvFileName = baseDir + s"/source_profile_$id.csv"

    val bw = new BufferedWriter(new FileWriter(new File(gnuFileName)))
    val s = s"""set xlabel "x pix"
               |set ylabel "y pix"
               |plot "${Path.getCurrentPath()}$csvFileName" i 1:3 with lines
               |pause -1""".stripMargin
    bw.write(s"$s\n")
    bw.close()
    profileToCSV(csvFileName, topProfile, absolutePos, " ", fillValue)
  }
  //---------------------------------------------------------------------------
  def toCSV(dataFileName: String, sep: String = "\t", absolutePos: Boolean) = {
    val bw = new BufferedWriter(new FileWriter(new File(dataFileName)))
    bw.write(s"#x${sep}y${sep}z\n")
    if (absolutePos)
      getAbsolutePosSeqWithValues foreach { t=> bw.write(s"${t._1}$sep${t._2}$sep${t._3}\n")}
    else
      getPosSeqWithValues foreach { t=> bw.write(s"${t._1}$sep${t._2}$sep${t._3}\n")}
    bw.close()
  }
  //---------------------------------------------------------------------------
  def toGnuPlot(baseDir: String, absolutePos: Boolean) = {
    val gnuFileName = baseDir + s"/source_$id.gnu"
    val csvFileName = baseDir + s"/source_$id.csv"

    val bw = new BufferedWriter(new FileWriter(new File(gnuFileName)))
    val s = s"""set xlabel "x pix"
               |set ylabel "y pix"
               |set zlabel "z intensity"
               |set hidden3d
               |set dgrid3d 50,50
               |set pm3d at b
               |set contour
               |set grid ztics
               |splot "${Path.getCurrentPath()}$csvFileName" i 1:2:3 with lines
               |pause -1""".stripMargin
    bw.write(s"$s\n")
    bw.close()
    toCSV(csvFileName," ", absolutePos)
  }
  //---------------------------------------------------------------------------
  def prettyPrint(reverse : Boolean = true, o: Point2D = offset): Unit = {
    //-------------------------------------------------------------------------
    val xAxis = getX_Axis
    //-------------------------------------------------------------------------
    def printColIndicator(xAxis: Segment1D, xOffset: Int = 0): Unit = {
      print("|")

      for (i <- 0 until xAxis.range) {
        val colIndex = s"%0${PRETTY_PRINT_COL_VALUE_PAD_SIZE}d".format(i + xOffset)
        val padSize = (PRETTY_PRINT_VALUE_PAD_SIZE - colIndex.length) / 2
        print(" " * padSize)
        print(colIndex)
        print(" " * (padSize + 1))
        print("|")
      }
    }
    //-------------------------------------------------------------------------
    def printRow(row: Array[Segment1D], rowIndex: Int) : Unit = {
      //fill the line with empty values
      val lineSeq  = ArrayBuffer.fill(xAxis.range)((" " * PRETTY_PRINT_VALUE_PAD_SIZE) + PRETTY_PRINT_VALUE_CELL_SEPARATOR)
      row foreach { case seg =>
        for(x <- seg.s to seg.e)
          lineSeq(x-xAxis.s) = s"%${PRETTY_PRINT_ROW_VALUE_PAD_SIZE + 1}d".format(seg(x-seg.s).toLong) + PRETTY_PRINT_VALUE_CELL_SEPARATOR
      }
      println(lineSeq.mkString("") + "  " + s"%0${PRETTY_PRINT_ROW_VALUE_PAD_SIZE}d".format(rowIndex) )
    }
    //-------------------------------------------------------------------------
    println(s"---------- Segment2D with m2_id: $id starts ----------")
    println(s" ALL POSITIONS ARE BASED TO ORIGIN (0,0)")
    val maxRowCharSize =  PRETTY_PRINT_ROW_HEADER.length + PRETTY_PRINT_ROW_VALUE_PAD_SIZE +
      (xAxis.range * (PRETTY_PRINT_VALUE_PAD_SIZE +1))

    println(s"Source: m2_id: $id centroid: ${centroid.getFormattedShortString()} With offset: $o  source pix count: $getElementCount segment count: $getRowCount fwhm: ${getFwhm()}")
    println("")
    println("yAbs")
    print("_" * maxRowCharSize)
    print("  yRel")
    println
    val seq = if (reverse) rowSeq.reverseIterator else rowSeq.iterator
    seq.zipWithIndex foreach { case (row,index) =>
      val rowBaseIndex = getRowCount - index - 1
      print( s"%0${PRETTY_PRINT_ROW_VALUE_PAD_SIZE}d".format(rowBaseIndex + o.y) + "->|")
      if (reverse) printRow(row,getRowCount - index - 1)
      else printRow(row,index)
    }

    //end line
    for( _ <- 0 until maxRowCharSize)
      print(0x203e.toChar)
    println

    //column indicator
    print("xAbs")
    print(" " * (PRETTY_PRINT_ROW_HEADER.length-1))
    printColIndicator(xAxis, o.x + xAxis.s)

    println("")
    print("xRel")
    print(" " * (PRETTY_PRINT_ROW_HEADER.length-1))
    printColIndicator(xAxis)

    println
    println
    println(s"---------- Segment2D with m2_id: $id ends ----------")
  }
  //---------------------------------------------------------------------------
  def printScalaCodeDefinition(maxSizeFormatValue : Int = 5): Unit = toMatrix2D().printScalaCodeDefinition(maxSizeFormatValue)
  //---------------------------------------------------------------------------
  def printScalaCodeDefinition() = toMatrix2D().printScalaCodeDefinition()
  //---------------------------------------------------------------------------
  def printRustCodeDefinition() = toMatrix2D().printRustCodeDefinition()
  //---------------------------------------------------------------------------
  //(pos,maxValue,maxX,maxY)
  def findMaxPositionAndValue(): (Matrix2D,Point2D, PIXEL_DATA_TYPE) = {
    val m = toMatrix2D()
    val (_, max, _, posSeqMax)  = m.findMinMax()
    val maxPos = Point2D.getAverage(posSeqMax)
    (m,maxPos,max)
  }
  //---------------------------------------------------------------------------
  //(fixed fwhm,fwhm was updated)
  def isValidFWHM(fwhm: Double): Boolean = {
    if (fwhm < 0) return false
    if (fwhm.isNaN) return false
    if (fwhm.isInfinity) return false

    val fwhmX = getX_Axis().range
    val fwhmY = getY_Axis().range
    val maxFWHM = Math.max(fwhmX, fwhmY)
    if (fwhm > maxFWHM) false
    else true
  }
  //---------------------------------------------------------------------------
  def calculateSimpleFwhmEstimation() = {

    val (m,maxPos,maxValue) = findMaxPositionAndValue()
    val halfMaxValue = maxValue / 2.0d

    val columnAtHalfMax =
      rowSeq(maxPos.y)
        .flatMap(seg=> seg.getPosSeqWithValues())
        .indexWhere( p=> p._2 >= halfMaxValue)

    val rowAtHalfMax = (m.getAsColSeq()(maxPos.x) zip Range(0,m.yMax))
                        .indexWhere(_._1 >= halfMaxValue)

    var fwhmX = math.abs(maxPos.x - columnAtHalfMax).toDouble
    if (fwhmX == 0) fwhmX = 0.5d

    var fwhmY = math.abs(maxPos.y - rowAtHalfMax).toDouble
    if (fwhmY == 0) fwhmY = 0.5d

    Math.max(fwhmX,fwhmY)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Segment2D.scala
//=============================================================================
