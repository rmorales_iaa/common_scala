//=============================================================================
package com.common.geometry.segment
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
import com.common.dataType.pixelDataType.PixelDataType._
import com.common.geometry.interval.Interval
import com.common.geometry.interval.Interval.IntervalDataType
//=============================================================================
//=============================================================================
object Segment1D {
  //---------------------------------------------------------------------------
  private val SERIALIZATION_DIVIDER = ":"
  private val SERIALIZATION_DATA_DIVIDER = ","
  //---------------------------------------------------------------------------
  def deserialization(s: String) = {
    val split = s.split(SERIALIZATION_DIVIDER)
    val data = split(2).split(",") map (v => v.toDouble)
    Segment1D(split(0).toInt
             , split(1).toInt
             , data
             , split(3).toInt)
  }
  //---------------------------------------------------------------------------
  def apply(s: Segment1D, o: Int) : Segment1D = Segment1D(s.s+o, s.e+o, s.d)
  //---------------------------------------------------------------------------
  def apply(p: Int) : Segment1D = Segment1D(p, p, Array())
  //---------------------------------------------------------------------------
  def apply(d: Array[PIXEL_DATA_TYPE]) : Segment1D = Segment1D(0, d.length-1, d)
  //---------------------------------------------------------------------------
  def getMax(seq: Array[Segment1D]) = {
    var minS = Int.MaxValue
    var maxE = Int.MinValue
    seq foreach { s =>
      minS = Math.min(minS,s.s)
      maxE = Math.max(maxE,s.e)
    }
    Segment1D(minS, maxE)
  }
  //---------------------------------------------------------------------------
  //return the sequence of contiguous segments regarding the the one at index starIndex'
  def getFirstContiguousSeq(seq: Array[Segment1D], starIndex : Int = 0) : Array[Segment1D] = {
    val contiguousSeq = ArrayBuffer[Segment1D]()
    var lastContiguousIndex = -1
    for(x<- starIndex until seq.length-1)
      if (seq(x) isContiguous seq(x+1)) {
        contiguousSeq += seq(x)
        lastContiguousIndex = x+1
      }
      else {
        if (lastContiguousIndex != -1)  contiguousSeq += seq(lastContiguousIndex)
        return contiguousSeq.toArray
      }
    if (lastContiguousIndex != -1)  contiguousSeq += seq(lastContiguousIndex)
    contiguousSeq.toArray
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import Segment1D._
//star,end, data, m2_id
case class Segment1D(s : IntervalDataType
                     , e : IntervalDataType
                     , d: Array[PIXEL_DATA_TYPE] = Array()
                     , id : Int = Interval.getNewID) extends Interval {
  //---------------------------------------------------------------------------
  private var flag = 0
  //---------------------------------------------------------------------------
  if(d.length > 0)
    require((e-s+1) == d.length, s"Range of segment: ${(e-s+1)} must match with data size: ${d.length}")
  //---------------------------------------------------------------------------
  def apply(x:Int) : PIXEL_DATA_TYPE = if (d.isEmpty) -1 else d(x) //get affine_transformation element without checking
  //---------------------------------------------------------------------------
  def getFlux = d.sum
  //---------------------------------------------------------------------------
  def getFlag = flag
  //---------------------------------------------------------------------------
  def size() = e-s+1
  //---------------------------------------------------------------------------
  def setFlag(f: Int) = flag = f
  //---------------------------------------------------------------------------
  def == (seg: Segment1D) : Boolean = {
    if (d.length != seg.d.length) return false
    (d zip seg.d) forall( t=> t._1 == t._2 )
  }
  //---------------------------------------------------------------------------
  def !=(seg: Segment1D)  = !(this == seg)
  //---------------------------------------------------------------------------
  def - (o: Int) = Segment1D(s-o, e-o, d)
  //---------------------------------------------------------------------------
  def findPos(v: PIXEL_DATA_TYPE) = d.indexOf(v)

  //---------------------------------------------------------------------------
  def addIfContiguous(p: Int, v: PIXEL_DATA_TYPE): Option[Segment1D]  = {
    if ((e + 1 ) == p) Some(Segment1D(s, p, d ++ Array(v), id))   //x is just on the right. Keep the segment is
    else
    if ((p + 1) == s) Some(Segment1D(p, e, Array(v) ++ d, id))   //x is just on the left. Keep the segment is
    else None
  }
  //---------------------------------------------------------------------------
  def addIfContiguous(p: Int): Option[Segment1D] = {
    if ((e + 1) == p) Some(Segment1D(s, p)) //x is just on the right. Keep the segment is
    else if ((p + 1) == s) Some(Segment1D(p, e)) //x is just on the left. Keep the segment is
    else None
  }

  //---------------------------------------------------------------------------
  def getIntersection(seg: Segment1D
                      , storeData: Boolean = false
                      , keepID: Boolean = false
                      , keepFlag: Boolean = false
                      , keepIDInput: Boolean = false
                      , keepIDInputFlag: Boolean = false): Option[Segment1D] = {
    if (!(this intersects seg)) return None
    val start_max = if (s >  seg.s)  s else seg.s
    val end_min = if (e <  seg.e)  e else seg.e
    if (end_min < start_max) None
    else {
      val newD =  if(!storeData) Array[PIXEL_DATA_TYPE]() else d.slice(start_max - s, (end_min + 1) - s)
      val newID = if (keepID) id else if (keepIDInput) seg.id else Interval.getNewID
      val newFlag = if (keepFlag) flag else if (keepIDInputFlag) seg.flag else 0
      val r = Segment1D(start_max, end_min, newD, newID)
      r.setFlag(newFlag)
      Some(r)
    }
  }
  //---------------------------------------------------------------------------
  def prettyPrint(pixValuePadSize: Int, pixValueSeparator : String): Unit = {
    d foreach { v =>
      val padSize = pixValuePadSize - v.toString.size
      if (padSize > 0)  print("0" * padSize)
      print(v + pixValueSeparator)
    }
  }
  //---------------------------------------------------------------------------
  override def toString = super.toString + s"=>{${d.mkString(",")}}"
  //---------------------------------------------------------------------------
  def serialization() = {
     s"$s" + Segment1D.SERIALIZATION_DIVIDER +
     s"$e" + Segment1D.SERIALIZATION_DIVIDER +
     d.mkString(SERIALIZATION_DATA_DIVIDER) + Segment1D.SERIALIZATION_DIVIDER +
     s"$id"
  }
  //---------------------------------------------------------------------------
  def multiplyAndSum(scale: Array[Double]) = (d,scale).zipped.map( _ * _ ).sum
  //---------------------------------------------------------------------------
  def getPosSeq() = (for (x<-s to e) yield x).toArray
  //---------------------------------------------------------------------------
  def getPosSeqWithValues() = (for (x<-s to e) yield (x,d(x-s))).toArray
  //---------------------------------------------------------------------------
  def getNonZeroValueCount = d.filter( _ != PIXEL_ZERO_VALUE).size
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================