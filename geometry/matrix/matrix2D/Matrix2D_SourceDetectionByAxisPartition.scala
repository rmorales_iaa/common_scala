
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  20/Oct/2023
 * Time:  12h:52m
 * Description: None
 */
//=============================================================================
package com.common.geometry.matrix.matrix2D
//=============================================================================
import com.common.dataType.pixelDataType.PixelDataType.PIXEL_DATA_TYPE
import com.common.geometry.point.Point2D
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.mask.Mask
import com.common.logger.MyLogger
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object Matrix2DSourceDetectionByAxisPartition extends MyLogger {
  //---------------------------------------------------------------------------
  def findSource(m: Matrix2D
                 , partitionWidth: Int
                 , partitionHeight: Int
                 , noiseTide: PIXEL_DATA_TYPE
                 , maskSeq: Array[Mask] = Array[Mask]()
                 , sourceSizeRestriction: Option[(Int, Int)] = None
                 , verbose: Boolean = false): Array[Segment2D] = {
    //-------------------------------------------------------------------------
    val axisPartitionSeq = Point2D.getAxisPartitionSeq(m.xMax, m.yMax, partitionWidth, partitionHeight)
    val compiledSourceSeq = ArrayBuffer[Segment2D]()
    //-------------------------------------------------------------------------

    //find sources in the sequence of partitions
    axisPartitionSeq.foreach { t =>
      val subMatrix = m.getSubMatrix(t._1, t._2)

      axisPartitionFindSourceSeq(subMatrix
                                 , noiseTide
                                 , maskSeq
                                 , compiledSourceSeq)

      if (verbose)
        info(s"SubImage '${subMatrix.id}' min pos: '${subMatrix.minPosIndexAbso}' max pos: '${subMatrix.maxPosIndexAbso}' compiled source seq: '${compiledSourceSeq.length}'")
    }

    if(verbose)
       info("Detected sources before merging count: " + compiledSourceSeq.length)

    val finalSourceSeq = mergeSourceSeq(compiledSourceSeq.toArray, sourceSizeRestriction)  //now apply the source restrictions to merged sources

    if(verbose)
       info(s"Final detected source count '${finalSourceSeq.length}'")
    finalSourceSeq
  }
  //-------------------------------------------------------------------------
  def axisPartitionFindSourceSeq(m: Matrix2D
                                 , noiseTide: PIXEL_DATA_TYPE
                                 , maskSeq: Array[Mask] = Array[Mask]()
                                 , compiledSourceSeq: ArrayBuffer[Segment2D]
                                 , verbose: Boolean = false) = {
    val sourceSeq = m.findSource(
      noiseTide
      , maskSeq
      , sourceSizeRestriction = None //disable restrictions to not affect the source merge
      , useGloblalId = true).toSegment2D_seq()

    if (verbose) info(s"SubImage '${m.id}' min pos: '${m.minPosIndexAbso}' max pos: '${m.maxPosIndexAbso}' before apply restrictions, found source count: '${sourceSeq.length}'")

    sourceSeq.foreach { source =>
      val sourceX_Axis = source.getX_AxisAbsolute()
      val sourceY_Axis = source.getY_AxisAbsolute()

      //check if source is at the border
      if (m.isAtBorderAbso(sourceX_Axis.s, sourceY_Axis.s) ||
          m.isAtBorderAbso(sourceX_Axis.e, sourceY_Axis.e)) { //avoid source restriction check
        source.setExtraInfo(m.id)
        compiledSourceSeq += source
      }
      else compiledSourceSeq += source
    }
  }
  //-------------------------------------------------------------------------
  def mergeSourceSeq(_compiledSourceSeq: Array[Segment2D]
                      , sourceSizeRestriction: Option[(Int, Int)] = None): Array[Segment2D] = {

    //compiled sources
    val compiledSourceSeq = ArrayBuffer[Segment2D]()

    //split by sources detected at border
    val sourceSeqAtBorder    = ArrayBuffer[Segment2D]()
    val sourceSeqNotAtBorder = ArrayBuffer[Segment2D]()

    _compiledSourceSeq.foreach { source =>
      if (source.hasExtraInfo) sourceSeqAtBorder += source
      else sourceSeqNotAtBorder += source
    }
    //add to final result the sources not at the border
    compiledSourceSeq ++= sourceSeqNotAtBorder

    //add to final result the merged sources at the border
    compiledSourceSeq ++= Segment2D.merge(sourceSeqAtBorder.toArray
                                          , sourceSizeRestriction)

    //final result applying the source restriction
    if (sourceSizeRestriction.isDefined)
      compiledSourceSeq.toArray.filter { source =>
        sourcePassRestrictions(source, sourceSizeRestriction.get)
      }
    else compiledSourceSeq.toArray
  }
  //---------------------------------------------------------------------------
  private def sourcePassRestrictions(source: Segment2D
                                     , sourceSizeRestriction: (Int, Int)): Boolean = {
    val sourcePixCount = source.getPixCount()
    (sourcePixCount >= sourceSizeRestriction._1) &&
    (sourcePixCount <= sourceSizeRestriction._2)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Matrix2DSourceDetectionByAxisPartition.scala
//=============================================================================