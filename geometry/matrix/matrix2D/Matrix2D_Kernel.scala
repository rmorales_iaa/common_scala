/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  30/Mar/2023
 * Time:  11h:26m
 * Description: None
 */
//=============================================================================
package com.common.geometry.matrix.matrix2D
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
case class Matrix2D_Kernel(xMax : Int //range [0,xMax-1]
                           , yMax : Int //range [0,yMax-1]
                           , data: Array[Double]
                           , anchorPosX:Int
                           , anchorPosY:Int) {
  //---------------------------------------------------------------------------
  val xMaxIndex = Math.max(0, xMax - 1)
  val yMaxIndex = Math.max(0, yMax - 1)

  val colCountLeftAnchor   = Math.max(0,anchorPosX)
  val colCountRightAnchor  = xMax - 1 - anchorPosX

  val rowCountTopAnchor    = yMax - 1 - anchorPosY
  val rowCountBottomAnchor = Math.max(0,anchorPosY)
  //---------------------------------------------------------------------------
  require(data != null, "Data must be not null")
  require(xMax > 0, "'xRange > 0'")
  require(yMax > 0, "'yRange > 0'")
  require(xMax * yMax == data.size, s"'(xRange * yRange == data.size) => ${xMax * yMax} != ${data.size} '")
  require(anchorPosX >= 0 && anchorPosX < xMax , s"anchorPosX:$anchorPosX must be between: [0,$xMax]")
  require(anchorPosY >= 0 && anchorPosY < yMax , s"anchorPosY:$anchorPosY must be between: [0,$yMax]")
  //---------------------------------------------------------------------------
  def apply(x: Int, y: Int) = data((y * xMax) + x) //get  element without checking

  //---------------------------------------------------------------------------
  def invertAllRow = {
    val r = (for {y <- yMaxIndex to 0 by -1}
      yield {
        val start = y * xMax
        data.slice(start, start + xMax)
      }).flatten
    Matrix2D_Kernel(xMax, yMax, r.toArray,anchorPosX,anchorPosY)
  }
  //---------------------------------------------------------------------------
  def convolve(m:Matrix2D) = {
    //-------------------------------------------------------------------------
    var mx = 0
    var my = 0
    val convolvedData = ArrayBuffer[Double]()
    while(my < m.yMax) {
      while(mx < m.xMax) {
        //convolve a single value of m

        //build the submatrix of m that will be convolved
        //setting to none the values out of the image
        val mStartCol = mx - colCountLeftAnchor
        val mEndCol   = mx + colCountRightAnchor
        val mStartRow = my - rowCountBottomAnchor
        val mEndRow   = my + rowCountTopAnchor
        val subMatrixData = ArrayBuffer.fill[Double](xMax * yMax)(0d)

        var kernelPos = 0
        for (subY <- mStartRow to mEndRow;
             subX <- mStartCol to mEndCol){
          if (m.isIn(subX, subY)) subMatrixData(kernelPos) = m(subX,subY)
          kernelPos += 1
        }

        //convolve the submatrix
        var acc = 0D
        for (kx <- 0 until xMax;
             ky <- 0 until yMax) yield {
          val pos = (ky * xMax) + kx
          acc += (data(pos) * subMatrixData(pos))
        }
        convolvedData += acc
        mx += 1
      }
      mx = 0
      my += 1
    }
    //build the result matrix
    Matrix2D(
        m.xMax
      , m.yMax
      , convolvedData.toArray
      , m.offset
      , name = s"Colvolved from: '${m.name}'"
    )
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def toMatrix2D = Matrix2D(xMax, yMax,data)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Matrix2D_Kernel.scala
//=============================================================================
