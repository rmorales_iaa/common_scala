/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  11/Oct/2023
 * Time:  14h:03m
 * Description: None
 */
package com.common.geometry.matrix.matrix2D
//=============================================================================
import com.common.util.path.Path
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object GnuPlot {
  //---------------------------------------------------------------------------
  private val OUTPUT_ROOT_PATH = "output/plot3D"
  //---------------------------------------------------------------------------
  private def normalizeNameGnuPlotName(s: String) = {
    val r = s.replaceAll("_", """\\\\_""")
    if (r.startsWith("'")) r else "'" + r + "'"
  }
  //---------------------------------------------------------------------------
  private def generateCsv(m: Matrix2D,csvFileName: String) = {
    val bw = new BufferedWriter(new FileWriter(new File(csvFileName)))
    for (y <- 0 until m.yMax; x <- 0 until m.xMax) {
      bw.write(s"$x,$y,${m(x,y)}\n")
    }
    bw.close()
  }
  //---------------------------------------------------------------------------
  def build(m: Matrix2D, path: String) = {

    val name = Path.getOnlyFilenameNoExtension(m.name)
    val d = Path.ensureDirectoryExist(path)
    val csvFileName     = s"$d/$name.csv"
    val gnuplotFileName = s"$d/$name.gnuplot"
    generateCsv(m,csvFileName)

    val s =
      s"""
  #start of gnu script
  #--------------------------------------
  set datafile separator ","
  set encoding utf8
  #--------------------------------------
  #variables
  my_title="{/:Bold Object and source: ${normalizeNameGnuPlotName(name)}}"
  my_x_axis_label='x pix pos'
  my_y_axis_label='y pix pos'
  my_z_axis_label='intensity'
  #--------------------------------------
  my_data_file="$csvFileName"
  #--------------------------------------
  #fonts
  my_axis_ticks_font = "Courier-New,9"
  my_axis_title_font = "Times-Roman:Bold,12"
  #--------------------------------------
  #colors
  #background
  my_plot_background_color = "#b0afaa"
  #--------------------------------------
  #title
  set title sprintf("{/Arial:Bold=14 %s}", my_title)
  #--------------------------------------
  #plot background color
  set object 1 rectangle from screen 0,0 to screen 1,1 fillcolor rgb my_plot_background_color behind
  #--------------------------------------
  set xlabel my_x_axis_label
  set ylabel my_y_axis_label
  #--------------------------------------
  set   autoscale                        # scale axes automatically
  unset log                              # remove any log-scaling
  set xtic auto                          # set xtics automatically
  set ytic auto                          # set ytics automatically
  set ztic auto                          # set ytics automatically
  set grid
  #--------------------------------------
  #no fit log file
  set fit quiet
  set fit logfile '/dev/null'
  #--------------------------------------
  #axis
  set xtics font my_axis_ticks_font
  set ytics font my_axis_ticks_font

  set xtic auto
  set ytic auto
  set ztic auto

  set xlabel my_x_axis_label font my_axis_title_font
  set ylabel my_y_axis_label font my_axis_title_font
  set zlabel my_z_axis_label font my_axis_title_font

  set hidden3d
  set dgrid3d 20,20
  #--------------------------------------
  splot my_data_file using 1:2:3 with lines lc rgb 'black' title 'Intensity map'
  #--------------------------------------
  pause -1
  #--------------------------------------
  #end of gnu script"""

    val fileName = gnuplotFileName
    val bw = new BufferedWriter(new FileWriter(new File(fileName)))
    bw.write(s)
    bw.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file GnuPlot.scala
//=============================================================================