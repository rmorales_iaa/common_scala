/** It implements a Matrix two dimensions stored aas a linear array of integers
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.geometry.matrix.matrix2D
//=============================================================================
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.{BufferedWriter, File, FileWriter}
import java.util.concurrent.atomic.AtomicInteger
import javax.imageio.ImageIO
import scala.annotation.tailrec
import scala.collection.mutable.ArrayBuffer
//=============================================================================
import com.common.util.file.MyFile
import com.common.dataType.pixelDataType.PixelDataType
import com.common.dataType.pixelDataType.PixelDataType._
import com.common.image.estimator.bellShape2D.BellShape2D_Fit
import com.common.image.estimator.bellShape2D.BellShape2D_Fit._
import com.common.image.estimator.bellShape2D.gaussian.{Gaussian2D_Elliptical, Gaussian2D_EllipticalRotated}
import com.common.image.estimator.bellShape2D.moffat.{Moffat2D_Elliptical, Moffat2D_EllipticalRotated}
import com.common.image.estimator.bellShape2D.normal.{Normal2D_Elliptical, Normal2D_EllipticalRotated}
import com.common.image.estimator.bellShape2D.simple.{Gaussian2D_CentralRowCol, Gaussian2D_Simple}
import com.common.image.myImage.MyImage.SIMPLE_FWHM_DISCARD_CRITERIA
import com.common.logger.MyLogger
import com.common.geometry.point.Point2D_Double
import com.common.geometry.point.{Point2D, Point2D_Float}
import com.common.geometry.rectangle.Rectangle
import com.common.geometry.segment.Segment1D
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.region.Region
import com.common.math.MyMath
import com.common.util.path.Path
import com.common.geometry.affineTransformation.AffineTransformation2D
import com.common.image.mask.Mask
import com.common.stat.StatDescriptive
import zscale._
//=============================================================================
//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================
object Matrix2D {
  //---------------------------------------------------------------------------
  private val id = new AtomicInteger(-1)
  //---------------------------------------------------------------------------
  def getNewID : Int = id.addAndGet(1)
  //---------------------------------------------------------------------------
  //pretty print
  private final val PRETTY_PRINT_VALUE_PAD_SIZE = 5
  private final val PRETTY_PRINT_VALUE_CELL_SEPARATOR = "|"
  private final val PRETTY_PRINT_ROW_HEADER =  "->|"
  private final val PRETTY_PRINT_ROW_VALUE_PAD_SIZE = 4
  private final val PRETTY_PRINT_COL_VALUE_PAD_SIZE = 4
  //---------------------------------------------------------------------------
  def apply(data: ArrayBuffer[ArrayBuffer[PIXEL_DATA_TYPE]], offset: Point2D) : Matrix2D =
    Matrix2D(data(0).length, data.length, data.flatten.toArray, offset)
  //---------------------------------------------------------------------------
  def apply(data: Array[Array[PIXEL_DATA_TYPE]], offset: Point2D) : Matrix2D =
    Matrix2D(data(0).length, data.length, data.flatten, offset)
  //---------------------------------------------------------------------------
  def getCoalescentIntervalSeq(v: ArrayBuffer[(Int,PIXEL_DATA_TYPE)]): Array[Array[(Int,PIXEL_DATA_TYPE)]] = {
    //-------------------------------------------------------------------------
    val r = ArrayBuffer[Array[(Int,PIXEL_DATA_TYPE)]]()
    //-------------------------------------------------------------------------
    def getFirstCoalescence(seq: ArrayBuffer[(Int,PIXEL_DATA_TYPE)]): ArrayBuffer[(Int,PIXEL_DATA_TYPE)] = {
      var endIndex = -1
      if (seq.length == 0) return ArrayBuffer()
      if (seq.length == 1) return seq.slice(0,1)
      for (v<-seq.sliding(2)) {
        if ((v(0)._1 + 1) == v(1)._1) endIndex +=1
        else {
          if (endIndex == -1) return seq.slice(0,1)
          else return seq.slice(0,endIndex+2)
        }
      }
      seq.slice(0,endIndex+2)
    }
    //-------------------------------------------------------------------------
    var sortedV = v.sortWith( _._1 < _._1)
    while(true){
      val g = getFirstCoalescence(sortedV)
      r += g.toArray
      sortedV = sortedV.drop(g.length)
      if (sortedV.length == 0) return r.toArray
    }
    r.toArray
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def joinImageFromPartition(mSeq: Array[Matrix2D], xAxisSize: Int, yAxisPSize: Int): Matrix2D = {
    val xAxisPartitionSize = mSeq(0).xMax
    val yAxisPartitionSize = mSeq(0).yMax
    val colCount = xAxisSize / xAxisPartitionSize
    val rowMatrixSeq = (mSeq grouped colCount).toArray
    val data = (rowMatrixSeq flatMap { rowMatrix =>
      for(row<-0 until yAxisPartitionSize) yield {
        rowMatrix flatMap (_.data.drop(row * xAxisPartitionSize).take(xAxisPartitionSize))}}).flatten
    Matrix2D(xAxisSize, yAxisPSize, data)
  }
  //---------------------------------------------------------------------------
  def joinImageFromPartition(mSeqSeq: Array[Array[Matrix2D]], xAxisSize: Int, yAxisPSize: Int): Array[Matrix2D] = {
    val stackCount = mSeqSeq(0).length
    val joinedImgSeq = for(imgIndex <-0  until stackCount) yield mSeqSeq map (_(imgIndex))
    (joinedImgSeq map (joinImageFromPartition(_,xAxisSize,yAxisPSize))).toArray
  }

  //---------------------------------------------------------------------------
  def surroundWithRectanglePixPos(bi: BufferedImage
                                  , seg2D_Seq: Array[Segment2D]
                                  , offsetX: Int
                                  , offsetY: Int
                                  , borderPixColor: Int = Color.RED.getRGB
                                  , borderWidthSize: Int = 2
                                  , distanceToBorder: Int = 2): Unit =
    surroundWithRectangle(bi
                          , seg2D_Seq
                          , borderPixColor
                          , Point2D(offsetX, offsetY)
                          , borderWidthSize
                          , distanceToBorder)
  //---------------------------------------------------------------------------
  def surroundWithRectangle(bi: BufferedImage
                            , seg2D_Seq:Array[Segment2D]
                            , borderPixColor: Int  = Color.RED.getRGB
                            , offset : Point2D = Point2D.POINT_ZERO
                            , borderWidthSize: Int = 2
                            , distanceToBorder: Int  = 2) {
    //-------------------------------------------------------------------------
    val distance = Point2D(distanceToBorder, distanceToBorder)
    val xMax = bi.getWidth
    val xMaxIndex = xMax-1
    val yMaxIndex = bi.getHeight-1
    //---------------------------------------------------------------------------
    def isIn(p: Point2D) =  (p.x >= 0 && p.x <= xMaxIndex) &&
      (p.y >= 0 && p.y <= yMaxIndex)
    //-------------------------------------------------------------------------
    def fillRectangle(minPos: Point2D, maxPos: Point2D): Unit = {
      val xRange = Range(minPos.x, maxPos.x+1)
      val xMinMax = Seq(minPos.x, maxPos.x)
      val yRange = Range(minPos.y, maxPos.y)
      val yMinMax = Seq(minPos.y, maxPos.y)

      for(x<-xRange; y<-yMinMax)
        if (isIn(Point2D(x,y))) bi.setRGB(x, yMaxIndex - y, borderPixColor)

      for(x<-xMinMax; y<-yRange)
        if (isIn(Point2D(x,y))) bi.setRGB(x, yMaxIndex - y, borderPixColor)
    }
    //-------------------------------------------------------------------------
    def surround(seg2D: Segment2D, color: Int): Unit = {
      val mM = seg2D.getMinMax()
      val minPos = mM._1 - distance + seg2D.offset + offset
      val maxPos = mM._2 + distance + seg2D.offset + offset
      for( i<- 0 until borderWidthSize ) {
        val d = Point2D(i,i)
        fillRectangle(minPos - d, maxPos + d)
      }
    }
    //-------------------------------------------------------------------------
    seg2D_Seq foreach { surround(_, borderPixColor)}
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def getMedianSeq(seq: Array[Matrix2D]) = {
    val xMax = seq(0).xMax
    val yMax = seq(0).yMax
    val newData = for(y<-0 until yMax; x<-0 until xMax) yield {
      val pixSeq = seq.map(m=> m(x,y))
      val sortedPixSeq = pixSeq.sortWith(_ < _)
      val median = if (pixSeq.size % 2 == 1) sortedPixSeq(sortedPixSeq.size / 2)
      else {
        val posRight = pixSeq.size / 2
        val posLeft = posRight - 1
        (sortedPixSeq(posRight) + sortedPixSeq(posLeft)) / 2.0d
      }
      median
    }
    Matrix2D(xMax, yMax, newData.toArray)
  }

  //---------------------------------------------------------------------------
  def fitBellShape(img: Matrix2D
                   , algorithm: BELL_SHAPE_2D_FIT_ALGORITHM = NORMAL_2D_ELLIPTICAL_ROTATED_FIT)
  : Option[BellShape2D_Fit] = {

    algorithm match {
      case GAUSSIAN_2D_SIMPLE_FIT                    => Some(Gaussian2D_Simple.fit(img))
      case GAUSSIAN_2D_SIMPLE_CENTRAL_ROW_COL_FIT    => Some(Gaussian2D_CentralRowCol.fit(img))

      case GAUSSIAN_2D_ELLIPTICAL_FIT                => Gaussian2D_Elliptical.fit(img)
      case GAUSSIAN_2D_ELLIPTICAL_ROTATED_FIT        => Gaussian2D_EllipticalRotated.fit(img)

      case NORMAL_2D_ELLIPTICAL                      => Normal2D_Elliptical.fit(img)
      case NORMAL_2D_ELLIPTICAL_ROTATED_FIT          => Normal2D_EllipticalRotated.fit(img)

      case MOFFAT_ELLIPTICAL_2D_FIT                  => Moffat2D_Elliptical.fit(img)
      case MOFFAT_ELLIPTICAL_ROTATED_FIT             => Moffat2D_EllipticalRotated.fit(img)
    }
  }
  //---------------------------------------------------------------------------
  def fitGaussian2D(m: Matrix2D
                    , sourceSeq: Array[Segment2D]
                    , samplingRegion: Point2D = Point2D(0,0) //extra pix size around the source
                    , gaussian2D_EstimationAlgorithm: BELL_SHAPE_2D_FIT_ALGORITHM = MOFFAT_ELLIPTICAL_ROTATED_FIT)
  : Array[Option[BellShape2D_Fit]]= {
    sourceSeq.map { source =>

      val rec = source.getRectangle()
      val subMatrix = m.getSubMatrixWithPadding(rec.min - samplingRegion, rec.max + samplingRegion)
      val gasFitted = fitBellShape(subMatrix, gaussian2D_EstimationAlgorithm)
      if (gasFitted.isEmpty) Some(Gaussian2D_Simple.fit(subMatrix))
      else {
        //check wrong fitting
        val simpleFwhm = Gaussian2D_Simple.fit(subMatrix)
        val fwhmFitted = gasFitted.get.fwhm
        val diff = Math.abs(simpleFwhm.fwhm - fwhmFitted)
        if (diff > simpleFwhm.fwhm * SIMPLE_FWHM_DISCARD_CRITERIA) Some(simpleFwhm)
        else gasFitted
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import Matrix2D._
case class Matrix2D(xMax : Int //range [0,xMax-1]
                    , yMax : Int //range [0,yMax-1]
                    , data: Array[PIXEL_DATA_TYPE]
                    , var offset: Point2D = Point2D.POINT_ZERO
                    , name: String= "None"
                    , id : Int = Matrix2D.getNewID) extends MyLogger {
  //---------------------------------------------------------------------------
  require(data != null, "Data must be not null")
  if ((xMax != 0) || (yMax != 0) || (!data.isEmpty)) {
    require(xMax > 0, "'xRange > 0'")
    require(yMax > 0, "'yRange > 0'")
    require(xMax * yMax == data.size, s"'(xRange * yRange == data.size) => ${xMax * yMax} != ${data.size} '")
  }
  //---------------------------------------------------------------------------
  val xMaxIndex = Math.max(0, xMax - 1)
  val yMaxIndex = Math.max(0, yMax - 1)

  val minPosIndexAbso = offset
  val maxPosIndexAbso = Point2D(xMaxIndex,yMaxIndex) + offset

  val lastIndex = data.length - 1
  val itemCount = data.length
  //---------------------------------------------------------------------------
  var bScale = 1D
  var bZero = 0D
  //---------------------------------------------------------------------------
  def this() = this(0, 0, Array(), Point2D.POINT_ZERO, "None")
  //---------------------------------------------------------------------------
  def this(m: Matrix2D) = this(m.xMax, m.yMax, m.data, m.offset, m.name)
  //---------------------------------------------------------------------------
  def this(m: Matrix2D, n: String) = this(m.xMax, m.yMax, m.data, m.offset, n)
  //---------------------------------------------------------------------------
  def this(m: Matrix2D, o: Point2D, n: String) = this(m.xMax, m.yMax, m.data, o, n)
  //---------------------------------------------------------------------------
  def equal(m2: Matrix2D): Boolean = {
    if (xMax != m2.xMax) return false
    if (yMax != m2.yMax) return false
    if (offset != m2.offset) return false
    for (t <- data zip m2.data)
      if (t._1 != t._2) return false
    true
  }
  //---------------------------------------------------------------------------
  def apply(x: Int, y: Int) = data((y * xMax) + x) //get  element without checking
  //---------------------------------------------------------------------------
  def apply(p: Point2D) = data((p.y * xMax) + p.x) //get element without checking
  //---------------------------------------------------------------------------
  def getFlatPos(x: Int, y: Int) = (y * xMax) + x
  //---------------------------------------------------------------------------
  def getFlatPos(p: Point2D) = (p.y * xMax) + p.x
  //---------------------------------------------------------------------------
  def get2D_Pos(p: Int) = Point2D(p % xMax, p / xMax)
  //---------------------------------------------------------------------------
  def getItemCount() = xMax * yMax
  //---------------------------------------------------------------------------
  def getPixSize() = getItemCount()
  //---------------------------------------------------------------------------
  def getDimension() = Point2D(xMax, yMax)
  //---------------------------------------------------------------------------
  def getByteSizeNoHeader() = xMax * yMax * getPixelByteSize
  //---------------------------------------------------------------------------
  def getMaxPos = Point2D(xMaxIndex, yMaxIndex)
  //---------------------------------------------------------------------------
  def getMaxPosAbs = getMaxPos + offset
  //---------------------------------------------------------------------------
  def getMinPosAbs = offset
  //---------------------------------------------------------------------------
  def xAxisIsIn(x: Int) = x >= 0 && x <= xMaxIndex
  //---------------------------------------------------------------------------
  def yAxisIsIn(y: Int) = y >= 0 && y <= yMaxIndex
  //---------------------------------------------------------------------------
  def isIn(x: Int, y: Int): Boolean = (x >= 0 && x <= xMaxIndex) && (y >= 0 && y <= yMaxIndex)
  //---------------------------------------------------------------------------
  def isIn(p: Point2D): Boolean = isIn(p.x, p.y)
  //---------------------------------------------------------------------------
  def isInAbs(p: Point2D) = (p.x >= (0 + offset.x) && p.x <= (xMaxIndex + offset.x)) &&
    (p.y >= (0 + offset.y) && p.y <= (yMaxIndex + offset.y))
  //---------------------------------------------------------------------------
  def isInBorder(x: Int, y: Int) = (x == 0) || (y == 0) || (x == xMaxIndex) || (y == yMaxIndex)

  //---------------------------------------------------------------------------
  def isAtBorderAbso(x: Int, y: Int) = (x == minPosIndexAbso.x) || (y == minPosIndexAbso.y) ||
                                       (x == maxPosIndexAbso.x) || (y == maxPosIndexAbso.y)
  //---------------------------------------------------------------------------
  def isInCorner(x: Int, y: Int) = ((x == 0) && (y == 0)) || //bottom left
    ((x == xMaxIndex) || (y == yMaxIndex)) || //top right
    ((x == xMaxIndex) || (y == 0)) || //bottom right
    ((x == 0) || (y == yMaxIndex)) //top left

  //---------------------------------------------------------------------------
  def getIfExist(x: Int, y: Int): Option[PIXEL_DATA_TYPE] = {
    if (!isIn(x, y)) None
    else Some(this (x, y))
  }
  //---------------------------------------------------------------------------
  def getIfExistAndNoZero(x: Int, y: Int): Option[PIXEL_DATA_TYPE] = {
    if (!isIn(x, y)) None
    else {
      val v = this (x, y)
      if (v != 0) Some(v) else None
    }
  }
  //---------------------------------------------------------------------------
  def get(x: Int, y: Int) = { //get affine_transformation element with checking
    require(x >= 0 && x <= xMaxIndex, s"'Invalid x value : $x'")
    require(y >= 0 && x <= yMaxIndex, s"'Invalid y value : $y'")
    data((y * xMax) + x)
  }
  //---------------------------------------------------------------------------
  def getAsRowSeq() = data.grouped(xMax).toArray

  //---------------------------------------------------------------------------
  def getAsColSeq() =  {
    val d = (for (i <- 0 until xMax) yield data.drop(i).sliding(1, xMax).toArray.flatten).flatten.toArray
    val m = Matrix2D(yMax, xMax, d, offset.reverse, name)
    m.getAsRowSeq
  }
  //---------------------------------------------------------------------------
  def toRawMatrix = data.grouped(xMax).toArray
  //---------------------------------------------------------------------------
  //(min, max, posSeqMin, posSeqMax)
  def findMinMax(): (PIXEL_DATA_TYPE, PIXEL_DATA_TYPE, Array[Point2D], Array[Point2D]) = {
    val max = data.max
    val min = data.min
    val posSeqMax = ArrayBuffer[Point2D]()
    val posSeqMin = ArrayBuffer[Point2D]()
    data.zipWithIndex.foreach { case (v, i) =>
      if (v == max) posSeqMax += get2D_Pos(i)
      if (v == min) posSeqMin += get2D_Pos(i)
    }
    (min, max, posSeqMin.toArray, posSeqMax.toArray)
  }

  //---------------------------------------------------------------------------
  //(xMin,yMin)  (xMax, yMax)
  def getSubMatrixWithPaddingPixPos(xMin: Int
                                    , yMin: Int
                                    , xMax: Int
                                    , yMax: Int
                                    , paddingValue: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE
                                    , pixValueOffset: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE
                                    , defaultPixValueWhenZeroOrNegative: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE): Matrix2D =

    getSubMatrixWithPadding(Point2D(xMin,yMin)
                            , Point2D(xMax,yMax)
                            , paddingValue
                            , pixValueOffset
                            , defaultPixValueWhenZeroOrNegative)
  //---------------------------------------------------------------------------
  //(xMin,yMin)  (xMax, yMax)
  def getSubMatrixWithPadding(m: Point2D
                              , M: Point2D
                              , paddingValue: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE
                              , pixValueOffset: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE
                              , defaultPixValueWhenZeroOrNegative: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE): Matrix2D = {

    assert(M.x >= m.x, s"The minor value of x range: ${m.x} must be less or equal than the max value: ${M.x} ")
    assert(M.y >= m.y, s"The minor value of x range: ${m.y} must be less or equal than the max value: ${M.y} ")

    if (isIn(m) && isIn(M)) getSubMatrix(m, M)
    else { //some  additional row and col are required
      val xSize = Math.abs(M.x - m.x + 1)
      val ySize = Math.abs(M.y - m.y + 1)
      val newData = ArrayBuffer.fill[PIXEL_DATA_TYPE](xSize * ySize)(paddingValue)

      val xItv = Segment1D(offset.x, offset.x + xMaxIndex)
      val yItv = Segment1D(offset.y, offset.y + yMaxIndex)

      val newOffset = Point2D(Math.min(xMaxIndex, offset.x + m.x), Math.min(yMaxIndex, offset.y + m.y))
      val xNewItv = Segment1D(newOffset.x, Math.min(xMaxIndex, newOffset.x + xSize - 1))
      val yNewItv = Segment1D(newOffset.y, Math.min(yMaxIndex, newOffset.y + ySize - 1))

      val xIts = xNewItv.getIntersection(xItv).get
      val yIts = yNewItv.getIntersection(yItv).get

      for (x <- xIts.s to xIts.e;
           y <- yIts.s to yIts.e) {
        val o = Point2D(x, y) - newOffset
        val pixValue = this(x - offset.x, y - offset.y) - pixValueOffset
        newData((o.y * xSize) + o.x) = if (pixValue <= 0) defaultPixValueWhenZeroOrNegative else pixValue
      }
      Matrix2D(xSize, ySize, newData.toArray, offset + m)
    }
  }

  //---------------------------------------------------------------------------
  def getSubMatrixWithSize(min: Point2D
                           , width: Int
                           , height: Int) =
    getSubMatrix(min.x, min.y, min.x + width, min.y + height)

  //---------------------------------------------------------------------------
  //In relative positions (no offset considered)
  //(xMin,yMin)  (xMax, yMax)
  def getSubMatrix(m: Point2D, M: Point2D): Matrix2D =
      getSubMatrix(m.x, m.y, M.x, M.y, name)
  //---------------------------------------------------------------------------
  //(xMin,yMin)  (xMax, yMax)
  def getSubMatrix(xm: Int
                   , ym: Int
                   , xM: Int
                   , yM: Int
                   , resultName: String = ""
                   , addInverseOffset: Boolean = false): Matrix2D = {

    require(xm >= 0 && xm <= xMaxIndex, s"'Invalid xMin: $xm")
    require(xM >= 0 && xM <= xMaxIndex, s"'Invalid xMax: $xM")

    require(ym >= 0 && ym <= yMaxIndex, s"'Invalid yMin: $ym")
    require(yM >= 0 && yM <= yMaxIndex, s"'Invalid yMin: $yM")

    require(xM >= xm, s"'xMin >= xMax: $xM >= $xm'")
    require(yM >= ym, s"'yMin >= yMax: $yM >= $ym'")

    val width = xM - xm + 1
    val height = yM - ym + 1

    val r =
      (for {y <- ym to yM} yield {
        val start = (y * xMax) + xm
        data.slice(start, start + width)
      }).flatten

    val o = if (addInverseOffset) offset.inverse + Point2D(xm, ym) else offset + Point2D(xm, ym)
    val m = Matrix2D(width, height, r.toArray, o, resultName)
    m.bScale = bScale
    m.bZero = bZero
    m
  }

  //---------------------------------------------------------------------------
  //(x,y) is the center of the box
  //'recX_Size' is the size in x axis from the center to the rectangle (center itself is not considered in the size)
  //'recY_Size' is the size in y axis from the center to the rectangle (center itself is not considered in the size)
  def getCentralSubMatrix(x: Int
                          , y: Int
                          , recX_Size: Int
                          , recY_Size: Int
                          , resultName: String = "None") = {

    val xMin = Math.max(0, x - recX_Size)
    val xMax = Math.min(xMaxIndex, x + recX_Size)

    val yMin = Math.max(0, y - recY_Size)
    val yMax = Math.min(yMaxIndex, y + recY_Size)

    getSubMatrix(xMin, yMin, xMax, yMax, resultName)
  }

  //---------------------------------------------------------------------------
  //(x,y) is the center of the box
  //squareSize is the  size from the center to one edge (center itself is not considered in the size)
  def getSquaredSubMatrix(x: Int, y: Int, squareSize: Int, resultName: String = "None") =
    getCentralSubMatrix(x, y, squareSize, squareSize, resultName)

  //---------------------------------------------------------------------------
  //  1   2    3       4        5
  //(min, max, median, average, variance)
  def getStats() = {
    val n = data.size.toDouble
    val average = data.sum / n
    val variance = data.map(v => Math.pow(average - v, 2)).sum / n - 1
    val sorted = data.sortWith(_ < _)
    val median =
      if (data.size % 2 == 1) sorted(sorted.size / 2).toDouble
      else {
        val posRight = data.size / 2
        val posLeft = posRight - 1
        (sorted(posRight) + sorted(posLeft)) / 2.0d
      }
    (sorted.head, sorted.last, median, average, variance)
  }
  //---------------------------------------------------------------------------
  def convolve(kernel: Matrix2D_Kernel): Matrix2D = kernel.convolve(this)
  //---------------------------------------------------------------------------
  //'recX_Size' is the size in x axis from the center to the edge (center itself is not considered in the size)
  //'recY_Size' is the size in y axis from the center to the edge (center itself is not considered in the size)
  def filterWithRectangle(recX_Size: Int
                          , recY_Size: Int
                          , filter: Matrix2D => PIXEL_DATA_TYPE
                          , resultName: String = "None"): Matrix2D = {

    require(recX_Size > 0, s" 'recX_Size' must be greater than 0. Current value: $recX_Size")
    require(recY_Size > 0, s" 'recY_Size' must be greater than 0. Current value: $recY_Size")
    require(recX_Size < xMax, s" 'recX_Size' must be less than xRange: $xMax. Current value: $recX_Size")
    require(recY_Size < yMax, s" 'recY_Size' must be less than yRange: $yMax. Current value: $recY_Size")

    val r = for (y <- 0 until yMax;
                 x <- 0 until xMax) yield
      filter(getCentralSubMatrix(x, y, recX_Size, recY_Size))
    Matrix2D(xMax, yMax, r.toArray, offset, resultName)
  }

  //---------------------------------------------------------------------------
  def applyPredicate(f: PIXEL_DATA_TYPE => PIXEL_DATA_TYPE
                     , resultName: String = "None") =
    Matrix2D(xMax, yMax, data.map(f(_)), offset, resultName)

  //---------------------------------------------------------------------------
  //return : (mainSubMatrixArray, remainSubMatrixX_axis, remainSubMatrixY_axis)
  def getPartitionSeq(width: Int
                      , height: Int) =
    Point2D.getAxisPartitionSeq(xMax,yMax,width,height).map { t =>
       getSubMatrix(t._1, t._2)
    }
  //---------------------------------------------------------------------------
  def invertAllRow = {
    val r = (for {y <- yMaxIndex to 0 by -1}
      yield {
        val start = y * xMax
        data.slice(start, start + xMax)
      }).flatten
    Matrix2D(xMax, yMax, r.toArray, offset, name + "_row_inverted")
  }

  //---------------------------------------------------------------------------
  private def getPercentile(p: Int, seq: IndexedSeq[PIXEL_DATA_TYPE], isSortedSeq: Boolean): PIXEL_DATA_TYPE = {
    require(0 <= p && p <= 100) // some value requirements
    require(seq.nonEmpty) // more value requirements
    val sortedSeq = if (isSortedSeq) seq else seq.sorted
    val k = Math.ceil((seq.length - 1) * (p / 100.toDouble)).toInt
    sortedSeq(k)
  }

  //-----------------------------------------------------------------------
  def getNormalizationWithZscale() = {
    val floatSeq = data.map(_.toDouble)
    val zLow = Array[Double](-1)
    val zHigh = Array[Double](-1)
    val zMax = Array[Double](-1)
    Zscale.calculate(floatSeq, floatSeq.length, zLow, zHigh, zMax)
    (zLow(0),zHigh(0))
  }

  //-----------------------------------------------------------------------
  def convertToBufferedImage(usezScale: Boolean = true
                             , minPercentile: Int = 2 //when not zscales is being used
                             , maxPercentile: Int = 98 //when not zscales is being used
                             , invertY_Axis: Boolean = true) = {
    val (minPix, maxPix) =
      if (usezScale) getNormalizationWithZscale
      else {
        val valueList = data.sorted
        (getPercentile(minPercentile, valueList, isSortedSeq = true),
          getPercentile(maxPercentile, valueList, isSortedSeq = true))
      }

    //-----------------------------------------------------------------------
    def getScaledRGB(pixValue: PIXEL_DATA_TYPE) = {
      val r = MyMath.rescale(pixValue, minPix, maxPix, 0, 255).toInt
      val b = if (r >= 255) 255.toByte else if (r < 0) 0.toByte else r.toByte
      b + (b << 8) + (b << 16)
    }

    //-----------------------------------------------------------------------
    val bmpImage = new BufferedImage(xMax, yMax, BufferedImage.TYPE_INT_RGB)
    val rowList = data.grouped(xMax).toIndexedSeq
    for (x <- 0 until xMaxIndex;
         y <- 0 until yMaxIndex)
      if (invertY_Axis) bmpImage.setRGB(x, yMaxIndex - y, getScaledRGB(rowList(y)(x)))
      else bmpImage.setRGB(x, y, getScaledRGB(rowList(y)(x)))
    bmpImage
  }
  //---------------------------------------------------------------------------
  def surroundWithRectangle(seg2D_Seq: Array[Segment2D]
                            , borderPixColor: Int = Color.WHITE.getRGB
                            , parentOffset: Point2D = Point2D.POINT_ZERO
                            , borderWidthSize: Int = 2
                            , distanceToBorder: Int = 2
                            , borderPixColorVeryFirstOne: Option[Int] = None) = {
    //-------------------------------------------------------------------------
    val distance = Point2D(distanceToBorder, distanceToBorder)
    val r = ArrayBuffer[PIXEL_DATA_TYPE]()

    //-------------------------------------------------------------------------
    def fillRectangle(minPos: Point2D, maxPos: Point2D, color: Int): Unit = {

      val xRange = Range(minPos.x, maxPos.x + 1)
      val xMinMax = Seq(minPos.x, maxPos.x)

      val yRange = Range(minPos.y, maxPos.y)
      val yMinMax = Seq(minPos.y, maxPos.y)

      for (x <- xRange; y <- yMinMax)
        if (isIn(Point2D(x, y))) r(getFlatPos(x, y)) = color

      for (x <- xMinMax; y <- yRange)
        if (isIn(Point2D(x, y))) r(getFlatPos(x, y)) = color
    }

    //-------------------------------------------------------------------------
    def surround(seg2D: Segment2D, color: Int): Unit = {
      val mM = seg2D.getMinMax()
      val minPos = mM._1 - distance + seg2D.offset + parentOffset
      val maxPos = mM._2 + distance + seg2D.offset + parentOffset
      for (i <- 0 until borderWidthSize) {
        val d = Point2D(i, i)
        fillRectangle(minPos - d, maxPos + d, color)
      }
    }
    //-------------------------------------------------------------------------
    //set border in a mutable storage
    r ++= data
    seg2D_Seq.zipWithIndex foreach { case (seg2D, i) =>
      surround(seg2D, if (i == 0) borderPixColorVeryFirstOne.getOrElse(borderPixColor) else borderPixColor)
    }
    Matrix2D(xMax, yMax, r.toArray, offset, "copy_of_" + name, id)
  }

  //---------------------------------------------------------------------------
  def saveAsImage(name: String
                  , scale: Boolean = false
                  , minPercentile: Int = 2
                  , maxPercentile: Int = 98
                  , format: String = "png"): Unit = {
    //---------------------------------------------------------------------------
    var minPix: Double = 0
    var maxPix: Double = 0
    if (scale) {
      val sortedPixSeq = data.sortWith(_ < _) map (_.toDouble)
      minPix = MyMath.getIntPercentileDouble(minPercentile, sortedPixSeq)
      maxPix = MyMath.getIntPercentileDouble(maxPercentile, sortedPixSeq)
    }

    //-----------------------------------------------------------------------
    def getScaledRGB(pixValue: PIXEL_DATA_TYPE) = {
      val r = MyMath.rescale(pixValue, minPix, maxPix, 0, 255).toInt
      val b = if (r >= 255) 255.toByte else if (r < 0) 0.toByte else r.toByte
      b + (b << 8) + (b << 16)
    }

    //-----------------------------------------------------------------------
    val bi = new BufferedImage(xMax, yMax, BufferedImage.TYPE_INT_RGB)
    //fill pix
    var k = 0
    for (y <- 0 until yMax;
         x <- 0 until xMax) {
      val pix = if (scale) getScaledRGB(data(k)) else PIXEL_DATA_TYPE_TO_INT(data(k))
      bi.setRGB(x, yMax - y - 1, pix) //inverted y axis
      k += 1
    }
    ImageIO.write(bi, format, new File(name + "." + format))
  }

  //---------------------------------------------------------------------------
  //Root mean square
  def getRMS() = {
    val squaredSum = (for (v <- data) yield v * v.toFloat).sum
    Math.sqrt(squaredSum / getItemCount)
  }

  //---------------------------------------------------------------------------
  def getAverage() = data.sum.toFloat / getItemCount - 1 //is not a poblation

  //---------------------------------------------------------------------------
  private def joinByCol(m: Matrix2D, isFirstCol: Boolean): Matrix2D = {
    assert(yMaxIndex == m.yMaxIndex, s"Can not join the both Matrix2D. $yMaxIndex != ${m.yMaxIndex}")
    val rowSeq = data.grouped(xMax).toArray
    val mRowSeq = m.data.grouped(m.xMax).toArray
    val d = rowSeq.zipWithIndex flatMap { case (row, i) =>
      if (isFirstCol) mRowSeq(i) ++ row else row ++ mRowSeq(i)
    }
    val o = if (isFirstCol) Point2D(offset.x - m.xMax, offset.y)
    else offset
    Matrix2D(xMax + m.xMax, yMax, d, o)
  }

  //---------------------------------------------------------------------------
  private def joinByRow(m: Matrix2D, isFirstRow: Boolean): Matrix2D = {
    assert(xMaxIndex == m.xMaxIndex, s"Can not join the both Matrix2D. $xMaxIndex != ${m.xMaxIndex}")
    val rowSeq = data.grouped(xMax).toArray
    val mRowSeq = m.data.grouped(m.xMax).toArray
    val d = if (isFirstRow) (mRowSeq ++ rowSeq).flatten else (rowSeq ++ mRowSeq).flatten
    val o = if (isFirstRow) Point2D(offset.x, offset.y - m.yMax)
    else offset
    Matrix2D(xMax, yMax + m.yMax, d, o)
  }

  //---------------------------------------------------------------------------
  def joinByFirstCol(m: Matrix2D) = joinByCol(m, true)

  //---------------------------------------------------------------------------
  def joinByLastCol(m: Matrix2D) = joinByCol(m, false)

  //---------------------------------------------------------------------------
  def joinByFirstRow(m: Matrix2D) = joinByRow(m, true)

  //---------------------------------------------------------------------------
  def joinByLastRow(m: Matrix2D) = joinByRow(m, false)

  //---------------------------------------------------------------------------
  def cropLeft(v: Int) = getSubMatrixWithSize(Point2D(v, 0), xMaxIndex - v, yMaxIndex)

  //---------------------------------------------------------------------------
  def skipColumnFromLeft(v: Int) = cropLeft(v)

  //---------------------------------------------------------------------------
  def cropRight(v: Int) = getSubMatrixWithSize(Point2D(0, 0), xMaxIndex - v, yMaxIndex)

  //---------------------------------------------------------------------------
  def skipColumnFromRight(v: Int) = cropRight(v)

  //---------------------------------------------------------------------------
  def cropTop(v: Int) = getSubMatrixWithSize(Point2D(0, 0), xMaxIndex, yMaxIndex - v)

  //---------------------------------------------------------------------------
  def skipRowsFromTop(v: Int) = cropTop(v)

  //---------------------------------------------------------------------------
  def cropBottom(v: Int) = getSubMatrixWithSize(Point2D(0, v), xMaxIndex, yMaxIndex - v)

  //---------------------------------------------------------------------------
  def skipRowsFromBottom(v: Int) = cropBottom(v)

  //---------------------------------------------------------------------------
  private def moveToLeftRight(v: Int, isLeft: Boolean): Matrix2D = {
    if (v == 0) return this
    val t = Math.min(v, xMax)
    val padding = Matrix2D(t, yMax, Array.fill[PIXEL_DATA_TYPE](t * yMax)(PIXEL_ZERO_VALUE))
    if (t >= xMax) padding
    else if (isLeft) cropLeft(t).joinByLastCol(padding)
    else cropRight(v).joinByFirstCol(padding)
  }

  //---------------------------------------------------------------------------
  private def moveTopBottom(v: Int, isTop: Boolean): Matrix2D = {
    if (v == 0) return this
    val t = Math.min(v, yMax)
    val padding = Matrix2D(xMax, t, Array.fill[PIXEL_DATA_TYPE](t * xMax)(PIXEL_ZERO_VALUE))
    if (t >= yMax) padding
    else if (isTop) cropTop(v).joinByFirstRow(padding)
    else cropBottom(t).joinByLastRow(padding)
  }

  //---------------------------------------------------------------------------
  def moveToLeft(v: Int): Matrix2D = moveToLeftRight(v, true)

  //---------------------------------------------------------------------------
  def moveToRight(v: Int): Matrix2D = moveToLeftRight(v, false)

  //---------------------------------------------------------------------------
  def moveToTop(v: Int): Matrix2D = moveTopBottom(v, true)

  //---------------------------------------------------------------------------
  def moveToBottom(v: Int): Matrix2D = moveTopBottom(v, false)

  //---------------------------------------------------------------------------
  def applyTranslation(p: Point2D, name: String, addInvertedOffset: Boolean = false) = {
    val t1 = if (p.x < 0) moveToLeft(-p.x) else moveToRight(p.x)
    val t2 = if (p.y < 0) t1.moveToBottom(-p.y) else t1.moveToTop(p.y)
    val o = if (addInvertedOffset) p.inverse + offset else p + offset
    new Matrix2D(t2, o, name)
  }

  //---------------------------------------------------------------------------
  def addRow(n: Int, addAtTop: Boolean, fillValue: PIXEL_DATA_TYPE = PixelDataType.PIXEL_ZERO_VALUE) = {
    val rowSeqToAdd = (for (_ <- 0 until n) yield Array.fill[PIXEL_DATA_TYPE](xMax)(fillValue)).flatten.toArray
    val rowSeq = getAsRowSeq().flatten
    var o = offset
    val newData =
      if (addAtTop) rowSeq ++ rowSeqToAdd
      else {
        o = Point2D(offset.x, offset.y - n)
        rowSeqToAdd ++ rowSeq
      }
    Matrix2D(xMax, yMax + n, newData, o)
  }

  //---------------------------------------------------------------------------
  def addColumn(n: Int, addAtRight: Boolean, fillValue: PIXEL_DATA_TYPE = PixelDataType.PIXEL_ZERO_VALUE) = {
    var o = offset
    val newData = getAsRowSeq() flatMap { row =>
      val colSeqToAdd = Array.fill[PIXEL_DATA_TYPE](n)(fillValue)
      if (addAtRight) row ++ colSeqToAdd
      else colSeqToAdd ++ row
    }
    if (!addAtRight) o = Point2D(offset.x - n, offset.y)
    Matrix2D(xMax + n, yMax, newData, o)
  }

  //---------------------------------------------------------------------------
  def expand(p: Point2D, fillValue: PIXEL_DATA_TYPE = PixelDataType.PIXEL_ZERO_VALUE) = {
    if (isIn(p)) this
    else {
      val t1 = if (p.x < 0) addColumn(-p.x, addAtRight = false, fillValue) else addColumn(p.x, addAtRight = true, fillValue)
      if (p.y < 0) t1.addRow(-p.y, addAtTop = false, fillValue) else t1.addRow(p.y, addAtTop = true, fillValue)
    }
  }

  //---------------------------------------------------------------------------
  def updateSingleValue(i: Int, value: PIXEL_DATA_TYPE) =
    Matrix2D(xMax, yMax, data.take(i) ++ Array(value) ++ data.drop(i + 1), offset)

  //---------------------------------------------------------------------------
  def fillRectangle(p: Point2D, v: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE): Matrix2D = {
    if (p == Point2D.POINT_ZERO) return this
    val rx = if (p.x == 0) None
    else {
      Some(
        if (p.x > 0) Rectangle(Point2D(0, 0), Point2D(p.x, yMaxIndex)) //left
        else Rectangle(Point2D(xMaxIndex + p.x + 1, 0), Point2D(xMaxIndex, yMaxIndex)))
    } //right

    val ry = if (p.y == 0) None
    else {
      Some(
        if (p.y > 0) Rectangle(Point2D(0, 0), Point2D(xMaxIndex, p.y)) //bottom
        else Rectangle(Point2D(0, yMaxIndex + p.y + 1), Point2D(xMaxIndex, yMaxIndex)))
    } //top
  //---------------------------------------------------------------------------
  def transpose() = {
    val newData = ArrayBuffer.fill[PIXEL_DATA_TYPE](xMax * yMax)(PIXEL_ZERO_VALUE)
    var k = 0
    for (y <- 0 until yMax;
         x <- 0 until xMax) {
      val newX = yMaxIndex - y
      val newY = xMaxIndex - x
      newData((newY * yMax) + newX) = data(k)
      k += 1
    }
    Matrix2D(yMax, xMax, newData.toArray, offset.reverse, name)
  }
  //---------------------------------------------------------------------------
  def flipCol() = {
    val newData = ArrayBuffer.fill[PIXEL_DATA_TYPE](xMax * yMax)(PIXEL_ZERO_VALUE)
    var k = 0
    for (y <- 0 until yMax;
         x <- 0 until xMax) {
      val newX = xMaxIndex - x
      val newY = y
      newData((newY * xMax) + newX) = data(k)
      k += 1
    }
    Matrix2D(xMax, yMax, newData.toArray, offset, name)
  }

  //---------------------------------------------------------------------------
  def flipRow() = {
    val newData = ArrayBuffer.fill[PIXEL_DATA_TYPE](xMax * yMax)(PIXEL_ZERO_VALUE)
    var k = 0
    for (y <- 0 until yMax;
         x <- 0 until xMax) {
      val newX = x
      val newY = yMaxIndex - y
      newData((newY * xMax) + newX) = data(k)
      k += 1
    }
    Matrix2D(xMax, yMax, newData.toArray, offset, name)
  }
    val r = if (rx.isDefined) fillRectangle(rx.get, v) else this
    if (ry.isDefined) r.fillRectangle(ry.get, v) else r
  }

  //---------------------------------------------------------------------------
  def fillRectangle(r: Rectangle, v: PIXEL_DATA_TYPE) = {
    val newData = ArrayBuffer[PIXEL_DATA_TYPE]()
    newData ++= data
    for (y <- r.min.y to r.max.y;
         x <- r.min.x to r.max.x) {
      val pos = (y * xMax) + x
      if ((pos >= 0) && (pos <= lastIndex)) newData(pos) = v
    }
    Matrix2D(xMax, yMax, newData.toArray, offset, name)
  }

  //---------------------------------------------------------------------------
  def toSegment2D(id: Option[Int] = None): Segment2D = {
    if (id.isDefined) Segment2D((data.grouped(xMax) map { v => Array(Segment1D(v)) }).toArray, offset, id.get)
    else Segment2D((data.grouped(xMax) map { v => Array(Segment1D(v)) }).toArray, offset)
  }

  //---------------------------------------------------------------------------
  def toSegment2D_ByDetection(v: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE): Option[Segment2D] = {
    val source = findSource(v)
    if (source.getItemCount == 0) None
    else Some(findSource(v).toSegment2D_seq().head)
  }
  //---------------------------------------------------------------------------
  def findSource(noiseTide: PIXEL_DATA_TYPE
                , maskSeq: Array[Mask] = Array[Mask]()
                , sourceSizeRestriction: Option[(Int, Int)] = None
                , useGloblalId: Boolean = false): Region = {
    val sourceSeq = Matrix2D_SourceDetection(
          noiseTide
        , maskSeq
        , sourceSizeRestriction
        , useGloblalId
        ).findSource(this)

    val region = Region(MyFile.removeFileExtension(Path.getOnlyFilename(name)), offset = offset)
    sourceSeq foreach (region.addToStorage(_))
    region
  }

  //---------------------------------------------------------------------------
  def findSourceByAxisPartition(noiseTide: PIXEL_DATA_TYPE
                               , maskSeq: Array[Mask] = Array[Mask]()
                               , sourceSizeRestriction: Option[(Int, Int)] = None
                               , partitionWidth: Int = 256
                               , partitionHeight: Int = 256) = {
    val sourceSeq = Matrix2DSourceDetectionByAxisPartition.findSource(
      m = this
      , partitionWidth
      , partitionHeight
      , noiseTide
      , maskSeq: Array[Mask]
      , sourceSizeRestriction)

    val region = Region(MyFile.removeFileExtension(Path.getOnlyFilename(name)), offset = offset)
    sourceSeq foreach (region.addToStorage(_))
    region.toSegment2D_seq()
  }
  //---------------------------------------------------------------------------
  def findBorder(v: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE) = {
    //-------------------------------------------------------------------------
    def isColBorder(colIndex: Int): Boolean = {
      for (y <- 0 to yMaxIndex) if (this (colIndex, y) != v) return false
      true
    }

    //-------------------------------------------------------------------------
    def isRowBorder(rowIndex: Int): Boolean = {
      for (x <- 0 to xMaxIndex) if (this (x, rowIndex) != v) return false
      true
    }

    //-------------------------------------------------------------------------
    @tailrec def findColBorder(acc: Int, colIndex: Int, decrease: Boolean): Int = {
      val count = if ((colIndex < 0) || (colIndex > xMaxIndex)) 0
      else if (isColBorder(colIndex)) 1 else 0
      if (count == 0) acc
      else findColBorder(count + acc, if (decrease) colIndex - 1 else colIndex + 1, decrease)
    }

    //-------------------------------------------------------------------------
    @tailrec def findRowBorder(acc: Int, rowIndex: Int, decrease: Boolean): Int = {
      val count = if ((rowIndex < 0) || (rowIndex > yMaxIndex)) 0
      else if (isRowBorder(rowIndex)) 1 else 0
      if (count == 0) acc
      else findRowBorder(count + acc, if (decrease) rowIndex - 1 else rowIndex + 1, decrease)
    }

    //-------------------------------------------------------------------------
    val leftBorder = findColBorder(0, 0, false)
    val rightBorder = findColBorder(0, xMaxIndex, true)
    val topBorder = findRowBorder(0, yMaxIndex, true) //top is the last row
    val bottomBorder = findRowBorder(0, 0, false) //bottom is the first row
    (leftBorder, rightBorder, topBorder, bottomBorder)
  }
  //---------------------------------------------------------------------------
  def saveAsCoordinateSeq(name: String, o: Point2D = offset): Unit = {
    val file = new File(name)
    val bw = new BufferedWriter(new FileWriter(file))
    for (y <- 0 until yMax;
         x <- 0 until xMax)
      bw.write(s"{${x + o.x} ${y + o.y}} ${this (x, y)}\n")
    bw.close()
  }
  //---------------------------------------------------------------------------
  def saveAsCoordinateSeqStacking(bw: BufferedWriter
                                  , o: Point2D = offset
                                  , predicate: Option[PIXEL_DATA_TYPE => Boolean]): Unit = {
    if (predicate.isDefined) {
      for (y <- 0 until yMax;
           x <- 0 until xMax) {
        val v = this (x, y)
        if (predicate.get(v)) bw.write(s"{${x + o.x} ${y + o.y}} ${this (x, y)}\n")
      }
    }
    else {
      for (y <- 0 until yMax;
           x <- 0 until xMax)
        bw.write(s"{${x + o.x} ${y + o.y}} ${this (x, y)}\n")
    }
  }

  //---------------------------------------------------------------------------
  //append m to this
  def +(m: Matrix2D) =
    Matrix2D(xMax, yMax, (data zip m.data) map (t => t._1 + t._2), name = "added")

  //---------------------------------------------------------------------------
  //subtract m to this
  def -(m: Matrix2D) =
    Matrix2D(xMax, yMax, (data zip m.data) map (t => t._1 - t._2), name = "subtracted")

  //---------------------------------------------------------------------------
  //divide this between m
  def /(m: Matrix2D) =
    Matrix2D(xMax, yMax, (data zip m.data) map (t => t._1 / t._2), name = "divided")

  //---------------------------------------------------------------------------
  def rescale(newMin: Double, newMax: Double): Matrix2D = {
    val sorted = data.sortWith(_ < _)
    val min = sorted.min
    val max = sorted.max
    Matrix2D(xMax
      , yMax
      , data.map(p => MyMath.rescale(p, min, max, newMin, newMax)))
  }

  //---------------------------------------------------------------------------
  def getProportionalRowPixel(x: Double, y: Double): Double = {
    if (x == xMaxIndex) this (xMaxIndex, yMaxIndex)
    else {
      val xi = x.toInt
      val yi = y.toInt
      val fractionalPart = x - xi
      (this (xi, yi) * (1 - fractionalPart)) + (fractionalPart * this (xi + 1, yi))
    }
  }

  //---------------------------------------------------------------------------
  def getProportionalColPixel(x: Double, y: Double): Double = {
    if (y == yMaxIndex) this (xMaxIndex, yMaxIndex)
    else {
      val xi = x.toInt
      val yi = y.toInt
      val fractionalPart = y - yi
      (this (xi, yi) * (1 - fractionalPart)) + (fractionalPart * this (xi, yi + 1))
    }
  }

  //---------------------------------------------------------------------------
  def getProportionalCol(x: Double, y: Double) = for (i <- 0 to yMaxIndex) yield getProportionalRowPixel(x, i)

  //---------------------------------------------------------------------------
  def getProportionalRow(x: Double, y: Double) = for (i <- 0 to xMaxIndex) yield getProportionalColPixel(i, y)

  //---------------------------------------------------------------------------
  def getAverageCol(colA: Int, colB: Int) =
    for (i <- 0 to yMaxIndex)
      yield (this (colA, i) + this (colB, i)) / 2d

  //---------------------------------------------------------------------------
  def getAverageRow(rowA: Int, rowB: Int) =
    for (i <- 0 to xMaxIndex)
      yield (this (i, rowA) + this (i, rowB)) / 2d

  //---------------------------------------------------------------------------
  def getMinMaxPosSeq() = {
    val min = data.min
    val max = data.max
    val minPosSeq = ArrayBuffer[Point2D]()
    val maxPosSeq = ArrayBuffer[Point2D]()
    data.zipWithIndex.foreach { case (d, i) =>
      if (d == min) minPosSeq += get2D_Pos(i)
      if (d == max) maxPosSeq += get2D_Pos(i)
    }
    (minPosSeq.toArray, maxPosSeq.toArray)
  }

  //---------------------------------------------------------------------------
  def getAbsoluteMinMaxPosSeq() = {
    val (minPosSeq, maxPosSeq) = getMinMaxPosSeq
    (minPosSeq map (_ + offset), maxPosSeq map (_ + offset))
  }

  //---------------------------------------------------------------------------
  def getNormalizationSumOne() = {
    val normalizedValue = 1d / data.sum
    for (y <- 0 until yMax; x <- 0 until xMax) yield this (x, y) * normalizedValue
  }

  //---------------------------------------------------------------------------
  def getNormalizationByMax() = {
    val normalizedValue = getStats()._2.toDouble
    for (y <- 0 until yMax; x <- 0 until xMax) yield this (x, y) / normalizedValue
  }

  //---------------------------------------------------------------------------
  def getNormalizationByMedian() = {
    val normalizedValue = getStats()._3
    for (y <- 0 until yMax; x <- 0 until xMax) yield this (x, y) / normalizedValue
  }

  //---------------------------------------------------------------------------
  def applyAffineTransformation(atf: AffineTransformation2D
                                , newName: String
                                , backgroundValue: PIXEL_DATA_TYPE
                                , addInvertedOffset: Boolean = false
                                , integralInterpolationType: Int = 0) = {
    val newImgPixSeq = ArrayBuffer.fill[PIXEL_DATA_TYPE](itemCount)(backgroundValue)
    for (y <- 0 until yMaxIndex; x <- 0 until xMaxIndex) {
      integralInterpolationType match {
        case 0 => //no interpolation. Direct conversion
          val newPosition = atf(Point2D_Double(x, y)).toPoint2D
          if (isIn(newPosition))
            newImgPixSeq((newPosition.y * xMax) + newPosition.x) = data((y * xMax) + x)

        //process all pixels in the projection area in the output image,
        // obtaining the corresponding pixel in the input image applying the
        // inverse of the affine transformation
        case 1 => //nearest pixel
          val outputPixPos = Point2D(x, y)
          val inverseAtf = atf.getInverse
          val inputPixPos = inverseAtf(outputPixPos).toPoint2D()
          if (isIn(inputPixPos))
            newImgPixSeq((outputPixPos.y * xMax) + outputPixPos.x) = data((inputPixPos.y * xMax) + inputPixPos.x)
      }
    }
    val newOffset = atf(Point2D_Float(0f, 0f)).toPoint2D
    val o = if (addInvertedOffset) newOffset.inverse + offset else newOffset + offset
    Matrix2D(xMax, yMax, newImgPixSeq.toArray, o, newName)
  }

  //---------------------------------------------------------------------------
  def applyAffineTransformationOnDimensions(atf: AffineTransformation2D) = {
    val min = atf(Point2D_Double(0, 0))
    val max = atf(Point2D_Double(xMaxIndex, yMaxIndex))
    val (x1, x2) = (Math.round(min.x), Math.round(max.x))
    val (y1, y2) = (Math.round(min.y), Math.round(max.y))

    val xAxisNew = Segment1D(Math.min(x1, x2).toInt, Math.max(x1, x2).toInt)
    val yAxisNew = Segment1D(Math.min(y1, y2).toInt, Math.max(y1, y2).toInt)

    val x = Segment1D(0, xMaxIndex) getIntersection xAxisNew
    val y = Segment1D(0, yMaxIndex) getIntersection yAxisNew

    val width = if (x.isDefined) x.get.range else 0
    val height = if (y.isDefined) y.get.range else 0

    (min, width, height)
  }

  //---------------------------------------------------------------------------
  def getConnectionCount(x: Int, y: Int, minValue: PIXEL_DATA_TYPE): Int = {
    var count = 0
    //4-connected
    if (isIn(x - 1, y) && (this (x - 1, y) > minValue)) count += 1
    if (isIn(x + 1, y) && (this (x + 1, y) > minValue)) count += 1
    if (isIn(x, y - 1) && (this (x, y - 1) > minValue)) count += 1
    if (isIn(x, y + 1) && (this (x, y + 1) > minValue)) count += 1

    //8-connected
    if (isIn(x - 1, y - 1) && (this (x - 1, y - 1) > minValue)) count += 1
    if (isIn(x + 1, y + 1) && (this (x + 1, y + 1) > minValue)) count += 1
    if (isIn(x + 1, y - 1) && (this (x + 1, y - 1) > minValue)) count += 1
    if (isIn(x - 1, y + 1) && (this (x - 1, y + 1) > minValue)) count += 1

    count
  }

  //-------------------------------------------------------------------------
  private def rawRound(minValue: PIXEL_DATA_TYPE, minNeighbourConnectedPixelCount: Int = 3, fillValue: PIXEL_DATA_TYPE = PIXEL_ZERO_VALUE) = {
    val newData = (for (y <- 0 to yMaxIndex; x <- 0 to xMaxIndex) yield {
      if (getConnectionCount(x, y, minValue) >= minNeighbourConnectedPixelCount) this (x, y)
      else fillValue
    }).toArray
    Matrix2D(xMax, yMax, newData, offset)
  }

  //---------------------------------------------------------------------------
  def round(minValue: PIXEL_DATA_TYPE
            , minNeighbourConnectedPixelCount: Int = 3
            , fillValue: PIXEL_DATA_TYPE = PIXEL_INVALID_VALUE) = {
    //-------------------------------------------------------------------------
    var prevMatrix = this
    var currentMatrix = rawRound(minValue, minNeighbourConnectedPixelCount, fillValue)
    var prevMatrixElementCount = prevMatrix.data.filter(_ > minValue).size
    var currentMatrixElementCount = currentMatrix.data.filter(_ > minValue).size
    while (prevMatrixElementCount != currentMatrixElementCount) {
      prevMatrix = currentMatrix
      currentMatrix = prevMatrix.rawRound(minValue, minNeighbourConnectedPixelCount, fillValue)
      prevMatrixElementCount = prevMatrix.data.filter(_ > minValue).size
      currentMatrixElementCount = currentMatrix.data.filter(_ > minValue).size
    }
    currentMatrix //keep empty col and rows to do not modify the geometry and the ocffset
  }

  //---------------------------------------------------------------------------
  //apply a sigma clipping and substitute the discard values by the average
  def normalize(sigmaScale: Double): Matrix2D = {
    if (sigmaScale == 0) this
    else {
      val (remainValueSeq, discardedValueSeq) = StatDescriptive.sigmaClippingDoubleGetDiscarded(data, sigmaScale = sigmaScale)
      val average = StatDescriptive.getWithDouble(remainValueSeq).average
      val normalizedData = ArrayBuffer[PIXEL_DATA_TYPE](data: _*)
      discardedValueSeq.foreach { t => normalizedData(t._2) = average }
      Matrix2D(xMax
        , yMax
        , normalizedData.toArray
        , offset
        , name)
    }
  }
  //---------------------------------------------------------------------------
  def transpose() = {
    val newData = ArrayBuffer.fill[PIXEL_DATA_TYPE](xMax * yMax)(PIXEL_ZERO_VALUE)
    var k = 0
    for (y <- 0 until yMax;
         x <- 0 until xMax) {
      val newX = yMaxIndex - y
      val newY = xMaxIndex - x
      newData((newY * yMax) + newX) = data(k)
      k += 1
    }
    Matrix2D(yMax, xMax, newData.toArray, offset.reverse, name)
  }
  //---------------------------------------------------------------------------
  def flipCol() = {
    val newData = ArrayBuffer.fill[PIXEL_DATA_TYPE](xMax * yMax)(PIXEL_ZERO_VALUE)
    var k = 0
    for (y <- 0 until yMax;
         x <- 0 until xMax) {
      val newX = xMaxIndex - x
      val newY = y
      newData((newY * xMax) + newX) = data(k)
      k += 1
    }
    Matrix2D(xMax, yMax, newData.toArray, offset, name)
  }

  //---------------------------------------------------------------------------
  def flipRow() = {
    val newData = ArrayBuffer.fill[PIXEL_DATA_TYPE](xMax * yMax)(PIXEL_ZERO_VALUE)
    var k = 0
    for (y <- 0 until yMax;
         x <- 0 until xMax) {
      val newX = x
      val newY = yMaxIndex - y
      newData((newY * xMax) + newX) = data(k)
      k += 1
    }
    Matrix2D(xMax, yMax, newData.toArray, offset, name)
  }

  //---------------------------------------------------------------------------
  def verticalFlip() = {
    val rowSeq = getAsRowSeq().reverse
    Matrix2D(yMax, xMax, rowSeq.flatten, offset, s"vertical flip of:'$name'")
  }
  //---------------------------------------------------------------------------
  def horizontalFlip() = {
    val newData = ArrayBuffer.fill(yMax, xMax)(PIXEL_ZERO_VALUE)
    for (y <- 0 until yMax;
         x <- 0 until xMax) {
      newData(y)(x) = this (xMaxIndex - x, y)
    }
    Matrix2D(yMax, xMax, newData.flatten.toArray, offset, s"horizontal flip of:'$name'")
  }
  //---------------------------------------------------------------------------
  def rotation90(clockwise: Boolean, invertY: Boolean = false) = {

    val newData = ArrayBuffer.fill[PIXEL_DATA_TYPE](xMax * yMax)(PIXEL_ZERO_VALUE)
    var k = 0
    for (y <- 0 until yMax;
         x <- 0 until xMax) {
      val newX = if (clockwise) y else yMaxIndex - y
      val newY =  if (invertY) x else xMaxIndex - x
      newData((newY * yMax) + newX) = data(k)
      k += 1
    }
    Matrix2D(yMax, xMax, newData.toArray, offset.reverse, name)
  }
  //---------------------------------------------------------------------------
  def rotation180(clockwise: Boolean) = {
    val newData = ArrayBuffer.fill[PIXEL_DATA_TYPE](xMax * yMax)(PIXEL_ZERO_VALUE)
    var k = 0
    for (y <- 0 until yMax;
         x <- 0 until xMax) {
      val newX = if (clockwise) xMaxIndex - x else x
      val newY = if (clockwise) yMaxIndex - y else y
      newData((newY * xMax) + newX) = data(k)
      k += 1
    }
    Matrix2D(xMax, yMax, newData.toArray, offset, name)
  }
  //---------------------------------------------------------------------------
  def rotation270(clockwise: Boolean) = {
    val newData = ArrayBuffer.fill[PIXEL_DATA_TYPE](xMax * yMax)(PIXEL_ZERO_VALUE)
    var k = 0
    for (y <- 0 until yMax;
         x <- 0 until xMax) {
      val newX = yMaxIndex - y
      val newY = if (clockwise) x else xMaxIndex - x
      newData((newY * yMax) + newX) = data(k)
      k += 1
    }
    Matrix2D(yMax, xMax, newData.toArray, offset.reverse, name)
  }

  //---------------------------------------------------------------------------
  def getRobustStats() = {
    val maxValue = data.max
    val minValue = data.min
    val robustData = data.filter { v => (v > minValue) && (v < maxValue) }
    StatDescriptive.getWithDouble(robustData)
  }
  //---------------------------------------------------------------------------
  def normalizeHistogram(normalizedValue: PIXEL_DATA_TYPE): Matrix2D = {
    val minPixel = data.min
    val maxPixel = data.max
    val scaleFactor = normalizedValue / (maxPixel - minPixel)
    val newData = data.map(pixel => (pixel - minPixel) * scaleFactor)
    Matrix2D(xMax, yMax, newData, offset, name, id)
  }
  //---------------------------------------------------------------------------
  def printScalaCodeDefinition(maxSizeFormatValue : Int = 5): Unit ={
    val header = s"val m = Matrix2D($xMax,$yMax,Array("
    val tail = s" ), offset = Point2D(${offset.x},${offset.y})).invertAllRow\n"

    println(header)
    for (y <- yMax-1 to 0 by -1) {
      val row = for (x <- 0 until xMax) yield {
        val v = Math.round(this(x, y))
        s" %${maxSizeFormatValue}d".format(v)
      }
      val rowEnd = if (y != 0) "," else ""
      println( row.mkString(",") + rowEnd)
    }
    println(tail)
  }
  //---------------------------------------------------------------------------
  def printRustCodeDefinition(maxSizeFormatValue: Int = 5): Unit = {
    val header = s"let m = Matrix2D::new($xMax, $yMax, vec!["
    val tail = s"], Point2D::new(${offset.x}, ${offset.y})).invert_all_row();"

    println(header)

    for (y <- yMax-1 to 0 by -1) {
      val row = for (x <- 0 until xMax) yield {
        val v = Math.round(this(x, y))
        s"%${maxSizeFormatValue}d".format(v)
      }

      val rowEnd = if (y != 0) "," else ""
      println(row.mkString(", ") + rowEnd)
    }

    println(tail)
  }
  //---------------------------------------------------------------------------
  private def prettyPrintRow(row: IndexedSeq[PIXEL_DATA_TYPE]
                             , pixValuePadSize: Int
                             , pixValueSeparator: String
                             , convertToInt: Boolean): Unit = {
    row foreach { v =>
      val vAsString = if(convertToInt) Math.round(v).toString else v.toString
      val padSize = pixValuePadSize - vAsString.size
      if (padSize > 0)  print(" " * padSize)
      print(vAsString + pixValueSeparator)
    }
  }
  //---------------------------------------------------------------------------
  def prettyPrint(reverse : Boolean = true
                  , o: Point2D = offset
                  , convertToInt: Boolean = true): Unit = {
    //-------------------------------------------------------------------------
    def printColIndicator(xOffset: Int = 0): Unit ={
      print(" " * (PRETTY_PRINT_ROW_HEADER.length + PRETTY_PRINT_ROW_VALUE_PAD_SIZE - 1))
      print("|")
      for (i<- 0 until xMax){
        val colIndex = s"%0${PRETTY_PRINT_COL_VALUE_PAD_SIZE}d".format(i+xOffset)
        val padSize = (PRETTY_PRINT_VALUE_PAD_SIZE - colIndex.length) / 2
        print(" " * padSize)
        print(colIndex)
        print(" " * (padSize + 1))
        print("|")
      }
    }
    //-------------------------------------------------------------------------
    // pix values with pad + separator +  row column indicator
    val maxRowCharSize = PRETTY_PRINT_ROW_HEADER.length + PRETTY_PRINT_ROW_VALUE_PAD_SIZE  +  (xMax * (PRETTY_PRINT_VALUE_PAD_SIZE +1))

    //start line
    println(s"----------- Matrix2D: '$name' with offset: $o starts -----------")
    println(s" ALL POSITIONS ARE BASED TO ORIGIN (0,0)")
    if (o.y > 0) println("yAbs")
    print("_" * maxRowCharSize)
    if (o.y > 0) print("  yRel")
    println

    val seq = if (reverse) data.grouped(xMax).toList.reverseIterator.zipWithIndex else data.grouped(xMax).toList.zipWithIndex
    seq foreach { case (row,index) =>

      //print row star
      val rowIndex = s"%0${PRETTY_PRINT_ROW_VALUE_PAD_SIZE}d".format(yMax + o.y - index - 1)
      print(rowIndex + "->|")

      //print the row
      prettyPrintRow(row
                     , PRETTY_PRINT_VALUE_PAD_SIZE
                     , PRETTY_PRINT_VALUE_CELL_SEPARATOR
                     , convertToInt)

      if (o.y > 0)  print("  " + s"%0${PRETTY_PRINT_ROW_VALUE_PAD_SIZE}d".format(yMax - index - 1))

      //print row separator
      println
    }

    //end of row
    for( _ <-0 until maxRowCharSize)
      print(0x203e.toChar)
    println

    //offset column indicator
    if (o.x > 0) {
      printColIndicator()
      print("xRel")
      println
    }

    //column indicator
    printColIndicator(o.x)
    if (o.x > 0) print("xAbs")
    println
    println
    println(s"----------- Matrix2D: '$name' with offset: $o ends -------------")
  }
  //---------------------------------------------------------------------------
  def plot3D(path: String) = GnuPlot.build(this, path)
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
