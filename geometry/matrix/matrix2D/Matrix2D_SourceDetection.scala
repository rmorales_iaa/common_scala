/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  17/Apr/2023
 * Time:  09h:29m
 * Description: Source detection inspired in
 * "An Algorithm for the Real Time Analysis of Digitised Image" R.K. Lutz 1980. The Computer Journal 23 262:269"
 */
//=============================================================================
package com.common.geometry.matrix.matrix2D
//=============================================================================
import com.common.dataType.pixelDataType.PixelDataType.PIXEL_DATA_TYPE
import com.common.geometry.point.Point2D
import com.common.geometry.segment.Segment1D
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.mask.Mask
import com.common.logger.MyLogger
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object Matrix2D_SourceDetection {
  //---------------------------------------------------------------------------
  type Pixel = (Point2D, PIXEL_DATA_TYPE)
  //---------------------------------------------------------------------------
  case class Source(id: Int
                    , map: scala.collection.mutable.LinkedHashMap[Point2D, PIXEL_DATA_TYPE]) {
    //-------------------------------------------------------------------------
    private var minCoord: Point2D = Point2D(Int.MaxValue,Int.MaxValue)
    private var maxCoord: Point2D = Point2D(Int.MinValue,Int.MinValue)
    //-------------------------------------------------------------------------
    map.keys.foreach(updateMinMaxCoordinates(_))
    //-------------------------------------------------------------------------
    private def updateMinMaxCoordinates(point: Point2D): Unit = {
      minCoord = Point2D(Math.min(minCoord.x, point.x), Math.min(minCoord.y, point.y))
      maxCoord = Point2D(Math.max(maxCoord.x, point.x), Math.max(maxCoord.y, point.y))
    }
    //-------------------------------------------------------------------------
    def addPixel(pixel: Pixel): Unit = {
      map(pixel._1) = pixel._2
      updateMinMaxCoordinates(pixel._1)
    }
    //-------------------------------------------------------------------------
    def canStore(position: Point2D): Boolean = {
      //the min and max coordiantes are augmented to consider the pixel connectivity
      if (position.x < (minCoord.x - 1) || position.x > (maxCoord.x + 1)) return false
      if (position.y < (minCoord.y - 1) || position.y > (maxCoord.y + 1)) return false
      true
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  final val PIXEL_CONNECTIVITY_EIGHT = 8.toByte
  //---------------------------------------------------------------------------
  private def groupByConsecutiveX(a: Array[Pixel]) = {
    a.foldLeft(List.empty[Array[Pixel]]) { case (acc, n) =>
      if (acc.isEmpty) List(Array(n))
      else {
        if ((acc.last.last._1.x + 1) == n._1.x) {
          val last = acc.last
          acc.dropRight(1) :+ (last :+ n)
        }
        else acc :+ Array(n)
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import Matrix2D_SourceDetection._
case class Matrix2D_SourceDetection(noiseTide: PIXEL_DATA_TYPE //a valid pixel of an object must have a value greater than 'noiseTide'
                                    , maskSeq: Array[Mask] = Array[Mask]()
                                    , sourceSizeRestriction: Option[(Int, Int)] = None
                                    , useGloblalId: Boolean = false
                                    , pixelConnectivity: Byte = Matrix2D_SourceDetection.PIXEL_CONNECTIVITY_EIGHT
                                    , verbose: Boolean = false) extends MyLogger {
  //---------------------------------------------------------------------------
  private var maxCol = 0
  private var mainOffset = Point2D.POINT_INVALID
  private var col = 0
  private var row = 0
  private val rowScan = ArrayBuffer[Pixel]() //pixels above noiseTide in the current roe
  private val sourceMap = scala.collection.mutable.LinkedHashMap[Int, Source]() //detected sources
  private var localSegmentID = 0
  private val notActiveSourceMap = scala.collection.mutable.LinkedHashMap[Int, Boolean]()
  private val completedSourceSeq = ArrayBuffer[Segment2D]()
  //--------------------------------------------------------------------------
  def findSource(m: Matrix2D): Array[Segment2D] = {
    //------------------------------------------------------------------------
    maxCol = m.xMax
    mainOffset = m.offset
    //------------------------------------------------------------------------
    //create the pixel map with values above noiseTide
    m.data.foreach { pixelValue =>
      if (pixelValue > noiseTide) {
        val pixelPos = Point2D(col, row)
        if(!Mask.isInForbiddenArea(maskSeq, pixelPos + m.offset))
          rowScan += ((pixelPos, pixelValue))
      }

      //update pos
      col += 1
      if (col == maxCol) { //a complete row is processed
        col = 0
        row += 1
        if (!rowScan.isEmpty){
          processRowScan()
          rowScan.clear()
        }
        if (verbose) debug(s"row-> ${row -1} completed sources: ${completedSourceSeq.size}")
      }
    }

    //consider current sources as complete source
    buildSourceSeq(sourceMap.values.toArray)

    //free not used memory
    rowScan.clear()
    sourceMap.clear()
    notActiveSourceMap.clear()

    completedSourceSeq.toArray
  }
  //---------------------------------------------------------------------------
  private def processRowScan() = {
    rowScan.foreach { case pixel =>
      if (sourceMap.isEmpty) addNewSource(pixel)
      else addPixelToSource(pixel)
    }
  }
  //---------------------------------------------------------------------------
  private def addPixelToSource(pixel: Pixel): Unit = {

    //all current sources are not active, they will actived on merge
    notActiveSourceMap.clear()
    notActiveSourceMap ++= sourceMap.keys.toArray.map( id=> (id,false) )

    //process current sources
    val sourcesConnectedByPixel = sourceMap.values.flatMap { source =>
      if (isPixelConnectedToSource(pixel, source)) Some(source)
      else None
    }.toArray

    //check the sources connected by the pixel and merge (connect) them
    if (sourcesConnectedByPixel.isEmpty) {addNewSource(pixel); return}
    if (sourcesConnectedByPixel.length == 1) {sourcesConnectedByPixel.head.addPixel(pixel);return}

    mergeSourceSeq(pixel,sourcesConnectedByPixel)  //merged sources are remove from the actives one

    //get sources that are not active, so they are completed
    val completedSourceSeq = notActiveSourceMap.keys.flatMap { id=>
      val source = sourceMap(id)
      //avoid the case when pixel at the top row of the source are in the same row but does not connect
      //In that case, the source is completed only if are in a different row
      if (pixel._1.y > (source.map.last._1.y + 1)) {
        sourceMap.remove(id)
        Some(source)  //completed
      }
      else None
    }
    //build the complete sources
    if (completedSourceSeq.size > 0)
     buildSourceSeq(completedSourceSeq.toArray)
  }
  //------------------------------------------------------------------------
  private def mergeSourceSeq(pixel: Pixel, connetedSourceByPixel: Array[Source]): Unit = {

    val map = scala.collection.mutable.LinkedHashMap[Point2D, PIXEL_DATA_TYPE]()
    connetedSourceByPixel.foreach { source =>
      sourceMap.remove(source.id)
      notActiveSourceMap.remove(source.id)   //merged sources are remove from the not actives
      source.map.foreach { case (pixelPos, pixelValue) =>
        map(pixelPos) = pixelValue
      }
    }
    val id = getNewSourceID()
    val source = Source(id, map)
    source.addPixel(pixel)
    sourceMap += ((id, source))
  }
  //---------------------------------------------------------------------------
  private def addNewSource(pixel: Pixel): Unit = {
    val map = scala.collection.mutable.LinkedHashMap[Point2D, PIXEL_DATA_TYPE]()
    map(pixel._1) = pixel._2
    val id = getNewSourceID()
    sourceMap += ((id, Source(id, map)))
  }
  //------------------------------------------------------------------------
  private def isPixelConnectedToSource(pixel: Pixel, source: Source): Boolean = {

    if (!source.canStore(pixel._1)) return false
    val pixelPos = pixel._1
    val map = source.map

    //row scan is processed from left to right
    if (map.contains(pixelPos.addX(-1)) ||
        map.contains(pixelPos.addY(-1))
    ) return true

    if (pixelConnectivity == PIXEL_CONNECTIVITY_EIGHT) {
      if (map.contains(pixelPos + Point2D(0, 1)) ||
          map.contains(pixelPos + Point2D(0, -1))  ||
          map.contains(pixelPos + Point2D(1, 1))  ||
          map.contains(pixelPos + Point2D(-1, -1))  ||
          map.contains(pixelPos + Point2D(1, 0)) ||
          map.contains(pixelPos + Point2D(-1, 0))  ||
          map.contains(pixelPos + Point2D(1, -1))  ||
          map.contains(pixelPos + Point2D(-1, 1))
      ) return true
    }
    false
  }
  //---------------------------------------------------------------------------
  private def getNewSourceID() =
    if (useGloblalId) Segment2D.getNewID
    else {
      localSegmentID += 1
      localSegmentID
    }
  //---------------------------------------------------------------------------
  private def buildSegmentSeq(pixelRowSeq: Array[(Int,Array[Pixel])]
                              ,colOffset: Int) = {
    pixelRowSeq.map { case (_, pixelRow) =>

      val consecutivePixelSeq = groupByConsecutiveX(pixelRow.sortWith(_._1.x < _._1.x))

      consecutivePixelSeq.map { case t =>

        val s = t.head._1.x - colOffset
        val e = s + t.length - 1
        val d = t.map(_._2)

        Segment1D(s, e, d, getNewSourceID())
      }.toArray
    }
  }

  //---------------------------------------------------------------------------
  private def buildSource(source:Source) = {
    val pixelRowSeq = source.map.toArray
      .groupBy(_._1.y)
      .toArray
      .sortWith(_._1 < _._1) //row 0 first

    val colOffset = source.map.keys.toArray.map(_.x).min
    val rowOffset = pixelRowSeq.head._1

    Segment2D(buildSegmentSeq(pixelRowSeq, colOffset)
      , mainOffset + Point2D(colOffset, rowOffset)
      , source.id)
  }
  //---------------------------------------------------------------------------
  private def buildSourceSeq(sourceSeq: Array[Source]): Unit = {
    val newCompletedSourceSeq = sourceSeq.flatMap { source =>
      if (sourcePassRestrictions(source)) Some(buildSource(source))
      else None
    }
    completedSourceSeq ++= newCompletedSourceSeq
  }
  //---------------------------------------------------------------------------
  private def sourcePassRestrictions(source: Source): Boolean = {
    if (sourceSizeRestriction.isDefined) {
      val sourcePixCount = source.map.size
      if ((sourcePixCount < sourceSizeRestriction.get._1) ||
          (sourcePixCount > sourceSizeRestriction.get._2)) {
        if (verbose) warning(s"discarded source because do not pass the restrictions. Source id '${source.id}' pixSize: '$sourcePixCount' first pix: '${source.map.head}' last pix: '${source.map.last}'")
        return false
      }
    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SourceDetection.scala
//=============================================================================
