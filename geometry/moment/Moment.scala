/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  27/Oct/2020
 * Time:  12h:42m
 * Description:
 * https://en.wikipedia.org/wiki/Image_moment
 * https://github.com/accord-net/java/blob/master/Catalano.Image/src/Catalano/Imaging/Tools/ImageMoments.java
 * "Shape Analysis & Measurement" by Miachael Wirth
 */

//=============================================================================
package com.common.geometry.moment
//=============================================================================
//=============================================================================
import com.common.geometry.point.Point2D_Double
//=============================================================================
trait Moment {
  //---------------------------------------------------------------------------
  def getRawMoment(px: Int, qy: Int): Double
  def getCentralMomentGeneric(px: Int, qy: Int): Double
  //---------------------------------------------------------------------------
  // moments
  private var rawMoment_0_0 : Option[Double] = None
  private var rawMoment_1_0 : Option[Double] = None
  private var rawMoment_0_1 : Option[Double] = None
  private var rawMoment_1_1 : Option[Double] = None
  private var rawMoment_0_2 : Option[Double] = None
  private var rawMoment_2_0 : Option[Double] = None
  private var rawMoment_1_2 : Option[Double] = None
  private var rawMoment_2_1 : Option[Double] = None
  private var rawMoment_2_2 : Option[Double] = None

  //central moments
  private var centralMoment_1_1: Option[Double] = None
  private var centralMoment_2_0: Option[Double] = None
  private var centralMoment_0_2: Option[Double] = None
  private var centralMoment_2_1: Option[Double] = None
  private var centralMoment_1_2: Option[Double] = None

  //normalised central moment
  private var normalisedCentralMoment_1_1: Option[Double] = None
  //---------------------------------------------------------------------------
  def getRawMoment_0_0() = {
    if (rawMoment_0_0.isEmpty) rawMoment_0_0 = Some(getRawMoment(0, 0))
    rawMoment_0_0.get
  }
  //---------------------------------------------------------------------------
  def getRawMoment_1_0() = {
    if (rawMoment_1_0.isEmpty) rawMoment_1_0 = Some(getRawMoment(1, 0))
    rawMoment_1_0.get
  }
  //---------------------------------------------------------------------------
  def getRawMoment_0_1() = {
    if (rawMoment_0_1.isEmpty) rawMoment_0_1 = Some(getRawMoment(0, 1))
    rawMoment_0_1.get
  }
  //---------------------------------------------------------------------------
  def getRawMoment_1_1() = {
    if (rawMoment_1_1.isEmpty) rawMoment_1_1 = Some(getRawMoment(1, 1))
    rawMoment_1_1.get
  }
  //---------------------------------------------------------------------------
  def getRawMoment_0_2() = {
    if (rawMoment_0_2.isEmpty) rawMoment_0_2 = Some(getRawMoment(0, 2))
    rawMoment_0_2.get
  }
  //---------------------------------------------------------------------------
  def getRawMoment_2_0() = {
    if (rawMoment_2_0.isEmpty) rawMoment_2_0 = Some(getRawMoment(2, 0))
    rawMoment_2_0.get
  }
  //---------------------------------------------------------------------------
  def getRawMoment_1_2() = {
    if (rawMoment_1_2.isEmpty) rawMoment_1_2 = Some(getRawMoment(1, 2))
    rawMoment_1_2.get
  }
  //---------------------------------------------------------------------------
  def getRawMoment_2_1() = {
    if (rawMoment_2_1.isEmpty) rawMoment_2_1 = Some(getRawMoment(2, 1))
    rawMoment_2_1.get
  }
  //---------------------------------------------------------------------------
  def getRawMoment_2_2() = {
    if (rawMoment_2_2.isEmpty) rawMoment_2_2 = Some(getRawMoment(2, 2))
    rawMoment_2_2.get
  }
  //---------------------------------------------------------------------------
  def getCentralMoment_1_1() = {
    if (centralMoment_1_1.isEmpty) centralMoment_1_1 = Some(getCentralMoment(1, 1))
    centralMoment_1_1.get
  }
  //---------------------------------------------------------------------------
  def getCentralMoment_0_2() = {
    if (centralMoment_0_2.isEmpty) centralMoment_0_2 = Some(getCentralMoment(0, 2))
    centralMoment_0_2.get
  }
  //---------------------------------------------------------------------------
  def getCentralMoment_2_0() = {
    if (centralMoment_2_0.isEmpty) centralMoment_2_0 = Some(getCentralMoment(2, 0))
    centralMoment_2_0.get
  }
  //---------------------------------------------------------------------------
  def geCentralMoment_2_1() = {
    if (centralMoment_2_1.isEmpty) centralMoment_2_1 = Some(getCentralMoment(2, 1))
    centralMoment_2_1.get
  }
  //---------------------------------------------------------------------------
  def getCentralMoment_1_2() = {
    if (centralMoment_1_2.isEmpty) centralMoment_1_2 = Some(getCentralMoment(1, 2))
    centralMoment_1_2.get
  }
  //---------------------------------------------------------------------------
  def getNormalisedCentralMoment_1_1() = {
    if (normalisedCentralMoment_1_1.isEmpty) normalisedCentralMoment_1_1 = Some(getNormalizedCentralMoment(1, 1))
    normalisedCentralMoment_1_1.get
  }
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Image_moment
  def getCentralMoment(px: Int, qy: Int): Double = {
    val centreOfMass = getCentreOfMass
    val centreX = centreOfMass.x
    val centreY = centreOfMass.y
    (px,qy) match {
      case  (0,0) => getRawMoment_0_0
      case  (0,1) => 0
      case  (1,0) => 0
      case  (1,1) => getRawMoment_1_1 - (centreX * getRawMoment_0_1)
      case  (2,0) => getRawMoment_2_0 - (centreX * getRawMoment_1_0)
      case  (0,2) => getRawMoment_0_2 - (centreY * getRawMoment_0_1)
      case  (2,1) => getRawMoment_2_1 - (2 * centreX * getRawMoment_1_1) - (centreY * getRawMoment_2_0) + (2 * centreX * centreX * getRawMoment_0_1)
      case  (1,2) => getRawMoment_1_2 - (2 * centreY * getRawMoment_1_1) - (centreX * getRawMoment_0_2) + (2 * centreY * centreY * getRawMoment_1_0)
      case  _ => getCentralMomentGeneric(px, qy)
    }
  }
  //---------------------------------------------------------------------------
  def getNormalizedCentralMoment(px: Int, qy: Int): Double = {
    val gama = ((px + qy) / 2) + 1
    val m00gama = Math.pow(getCentralMoment(0, 0), gama)
    getCentralMoment(px, qy) / m00gama
  }
  //---------------------------------------------------------------------------
  def getCentreOfMass() = new Point2D_Double(getRawMoment_1_0 / getRawMoment_0_0, getRawMoment_0_1 / getRawMoment_0_0)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Moment.scala
//=============================================================================
