/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  27/Oct/2020
 * Time:  12h:42m
 * Description:
 * https://en.wikipedia.org/wiki/Image_moment
 * https://github.com/accord-net/java/blob/master/Catalano.Image/src/Catalano/Imaging/Tools/ImageMoments.java
 */

//=============================================================================
package com.common.geometry.moment
//=============================================================================
//=============================================================================
import com.common.dataType.pixelDataType.PixelDataType.PIXEL_DATA_TYPE
//=============================================================================
//=============================================================================
case class ImageMoment(data : Array[Array[PIXEL_DATA_TYPE]]) extends Moment {
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Image_moment
  //---------------------------------------------------------------------------
  def getRawMoment(px: Int, qy: Int): Double = {
    var m = 0d
    (px,qy) match {
      case  (0,0) => for (y<-0 until data.length; x<-0 until data(y).length) m += data(y)(x)
      case  (1,0) => for (y<-0 until data.length; x<-0 until data(y).length) m += x * data(y)(x)
      case  (0,1) => for (y<-0 until data.length; x<-0 until data(y).length) m += y * data(y)(x)
      case  (1,1) => for (y<-0 until data.length; x<-0 until data(y).length) m += x * y * data(y)(x)
      case  (0,2) => for (y<-0 until data.length; x<-0 until data(y).length) m += y * y * data(y)(x)
      case  (2,0) => for (y<-0 until data.length; x<-0 until data(y).length) m += x * x * data(y)(x)
      case  (1,2) => for (y<-0 until data.length; x<-0 until data(y).length) m += x * y * y * data(y)(x)
      case  (2,1) => for (y<-0 until data.length; x<-0 until data(y).length) m += x * x * y * data(y)(x)
      case  (2,2) => for (y<-0 until data.length; x<-0 until data(y).length) m += x * x * y * y * data(y)(x)
      case      _ => for (y<-0 until data.length; x<-0 until data(y).length) m += Math.pow(x, px) * Math.pow(y,qy) * data(y) (x)
    }
    m
  }
  //---------------------------------------------------------------------------
  def getCentralMomentGeneric(px: Int, qy: Int): Double = {
    val centreOfMass = getCentreOfMass
    val centreX = centreOfMass.x
    val centreY = centreOfMass.y
    var mc = 0d
    for (y<-0 until data.length; x<-0 until data(y).length)
      mc += Math.pow(x-centreX, px) * Math.pow(y-centreY, qy) * data(y)(x)
    mc
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ImageMoment.scala
//=============================================================================
