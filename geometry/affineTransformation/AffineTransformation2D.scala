/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  11/May/2020
 * Time:  00h:34m
 * Description: https://docs.oracle.com/javase/7/docs/api/java/awt/geom/AffineTransform.html
 * [ x']   [  m00  m01  m02  ] [ x ]   [ m00x + m01y + m02 ]
 * [ y'] = [  m10  m11  m12  ] [ y ] = [ m10x + m11y + m12 ]
 * [ 1 ]   [   0    0    1   ] [ 1 ]   [         1         ]
 */
//=============================================================================
package com.common.geometry.affineTransformation
//=============================================================================
import Jama.Matrix
import com.common.stat.{StatDescriptive}
import org.apache.commons.math3.linear.{LUDecomposition, MatrixUtils}
import scala.collection.mutable.ArrayBuffer
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.geometry.point.Point2D_Float
import com.common.geometry.point.Point2D
//=============================================================================
//=============================================================================
object AffineTransformation2D {
  //---------------------------------------------------------------------------
  private val order = 3
  //---------------------------------------------------------------------------
  final val AFFINE_TRANSFORMATION_2D_INVALID = AffineTransformation2D(Array(Double.NaN, Double.NaN, Double.NaN
                                                                          , Double.NaN, Double.NaN, Double.NaN))
  //---------------------------------------------------------------------------
  final val AFFINE_TRANSFORMATION_2D_UNIT  = AffineTransformation2D(Array(1, 0, 0
                                                                        , 0, 1, 0)) //unit transformation
  //---------------------------------------------------------------------------
  def apply(): AffineTransformation2D = AffineTransformation2D(Array(1, 0, 0
                                                                   , 0, 1, 0)) //unit transformation
  //---------------------------------------------------------------------------
  def buildScaledAft(sx: Double, sy: Double) =
    AffineTransformation2D(Array(sx, 0, 0
                               , 0, sy, 0)) //unit transformation
  //---------------------------------------------------------------------------
  def buildTranslatedAft(tx: Double, ty: Double) =
    AffineTransformation2D(Array(1, 0, tx
                               , 0, 1, ty)) //unit transformation
  //---------------------------------------------------------------------------
  def buildRotatedAftRad(alfaRad: Double) = {
    val sin = Math.sin(alfaRad)
    val cos = Math.cos(alfaRad)
    AffineTransformation2D(Array(cos, -sin, 0
                               , sin, cos, 0))
  }
  //---------------------------------------------------------------------------
  def buildRotatedAftDeg(alfaDeg: Double) =
    buildRotatedAftRad(alfaDeg.toRadians)

  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/21270892/generate-affinetransform-from-3-points
  //same as used in opencv
  def LutDecomposition(seqA: Array[Point2D_Float], seqB: Array[Point2D_Float]): Option[AffineTransformation2D] = {
    val oldMatrix = MatrixUtils.createRealMatrix(Array(seqA map (_.x.toDouble), seqA map (_.y.toDouble) , Array(1d, 1d, 1d)))
    val newMatrix = MatrixUtils.createRealMatrix(Array(seqB map (_.x.toDouble), seqB map (_.y.toDouble) , Array(1d, 1d, 1d)))
    val inverseOld = new LUDecomposition(oldMatrix).getSolver.getInverse
    val transformationMatrix = newMatrix.multiply(inverseOld)
    val valueSeq = (for(x<-0 to 1; y<-0 to 2) yield transformationMatrix.getEntry(x, y)).toArray
    Some(AffineTransformation2D(valueSeq))
  }
  //---------------------------------------------------------------------------
  def LutDecomposition(seqA: Array[Point2D_Double], seqB: Array[Point2D_Double]): Option[AffineTransformation2D] = {
    val oldMatrix = MatrixUtils.createRealMatrix(Array(seqA map (_.x), seqA map (_.y) , Array(1d, 1d, 1d)))
    val newMatrix = MatrixUtils.createRealMatrix(Array(seqB map (_.x), seqB map (_.y) , Array(1d, 1d, 1d)))
    val inverseOld = new LUDecomposition(oldMatrix).getSolver.getInverse
    val transformationMatrix = newMatrix.multiply(inverseOld)
    val valueSeq = (for (x <- 0 to 1; y <- 0 to 2) yield transformationMatrix.getEntry(x, y)).toArray
    Some(AffineTransformation2D(valueSeq))
  }

  //---------------------------------------------------------------------------
  def LutDecomposition(seqA_x: Array[Double]
                       , seqA_y: Array[Double]
                       , seqB_x:  Array[Double]
                       , seqB_y: Array[Double]): Option[AffineTransformation2D] = {
    try {
      val n = seqA_x.length
      if (n != seqA_y.length || n != seqB_x.length || n != seqB_y.length) {
        throw new IllegalArgumentException("Input arrays must have the same length")
      }

      val oldMatrix = MatrixUtils.createRealMatrix(Array(seqA_x, seqA_y, Array.fill(n)(1.0)))
      val newMatrix = MatrixUtils.createRealMatrix(Array(seqB_x, seqB_y, Array.fill(n)(1.0)))

      val luDecomposition = new LUDecomposition(oldMatrix)
      if (!luDecomposition.getSolver.isNonSingular) return None

      val inverseOld = luDecomposition.getSolver.getInverse
      val transformationMatrix = newMatrix.multiply(inverseOld)
      val valueSeq = (for (x <- 0 to 1; y <- 0 to 2) yield transformationMatrix.getEntry(x, y)).toArray

      Some(AffineTransformation2D(valueSeq))
    } catch {
      case _: Exception => None
    }
  }
  //---------------------------------------------------------------------------
  //https://github.com/Andrew-Tuen/umeyama_java/blob/master/umeyama_jama.java
  //"Least-squares estimate of transformation parameters between two point patterns", Shinji Umeyama, PAMI 1991, :DOI:`10.1109/34.88573`
  //Same algorithm as used in python :'_geometric.umeyama'
  //Alternative implementation: https://github.com/pbloem/Lilian/blob/master/Lilian/src/main/java/org/lilian/data/real/Maps.java
  //calculate tha affine transformation to transform seqA to seqB
  def umeyana2D(seqA: Array[Point2D_Float]
                , seqB: Array[Point2D_Float]): Option[AffineTransformation2D] = {

    val aft = umeyana(seqA map (_.toArrayDouble)
                    , seqB map (_.toArrayDouble), estimate_scale = true)
    if (aft.isEmpty) return None
    Some(AffineTransformation2D(aft.get.dropRight(3)))  //last 3 values are always 0
  }

  //---------------------------------------------------------------------------
  //calculate tha affine transformation to transform seqA to seqB
  def umeyana2D(seqA: Array[Point2D_Double]
                , seqB: Array[Point2D_Double]): Option[AffineTransformation2D] = {
    val aft = umeyana(seqA map (_.toArray())
                     , seqB map (_.toArray),estimate_scale = true)
    if (aft.isEmpty) return None
    Some(AffineTransformation2D(aft.get.dropRight(3))) //last 3 values are always 0
  }
  //---------------------------------------------------------------------------
  //calculate tha affine transformation to transform seqA to seqB
  def umeyana2D(seqA: Array[Array[Double]]
                , seqB: Array[Array[Double]]): Option[AffineTransformation2D] = {
    val aft = umeyana(seqA, seqB, estimate_scale = true)
    if (aft.isEmpty) return None
    Some(AffineTransformation2D(aft.get.dropRight(3))) //last 3 values are always 0
  }
  //---------------------------------------------------------------------------
  //calculate tha affine transformation to transform dst to src
  private def umeyana(src: Array[Array[Double]]
                      , dst: Array[Array[Double]]
                      , estimate_scale: Boolean): Option[Array[Double]] = {

    if(src.isEmpty) return None
    if(dst.isEmpty) return None

    val num = src.length
    val dim = src(0).length
    //compute mean of src and dst
    val src_mean = new Array[Double](dim)
    val dst_mean = new Array[Double](dim)
    for (i <- 0 until dim) {
      src_mean(i) = 0
      dst_mean(i) = 0
    }
    for (i <- 0 until num) {
      for (j <- 0 until dim) {
        src_mean(j) += src(i)(j)
        dst_mean(j) += dst(i)(j)
      }
    }
    for (i <- 0 until dim) {
      src_mean(i) /= num
      dst_mean(i) /= num
    }
    // Subtract mean from src and dst.
    val src_demean_f = ArrayBuffer.fill[Double](num, dim)(0)
    val dst_demean_f = ArrayBuffer.fill[Double](num, dim)(0)
    for (i <- 0 until num) {
      for (j <- 0 until dim) {
        src_demean_f(i)(j) = src(i)(j) - src_mean(j)
        dst_demean_f(i)(j) = dst(i)(j) - dst_mean(j)
      }
    }
    // Eq. (38).
    val src_demean = new Matrix(src_demean_f.toArray map (_.toArray))
    val dst_demean = new Matrix(dst_demean_f.toArray map (_.toArray))
    val A = dst_demean.transpose.times(src_demean).times(1.0 / num.toDouble)
    //Eq. (39).
    val d = ArrayBuffer.fill[Double](dim, dim)(0)
    for (i <- 0 until dim) {
      for (j <- 0 until dim) {
        d(i)(j) = 0
        if (i == j) d(i)(j) = 1
      }
    }
    if (A.det < 0) d(dim - 1)(dim - 1) = -1
    val T = ArrayBuffer.fill[Double](dim+1, dim+1) (0)
    var S = A.svd.getS
    var V = A.svd.getV
    val U = A.svd.getU
    val temp_S = ArrayBuffer.fill[Double](1, dim)(0)
    for (i <- 0 until dim)
      temp_S(0)(i) = S.get(i, i)
    S = new Matrix(temp_S.toArray map (_.toArray))
    V = V.transpose
    //Eq. (40) and (43).
    val rank = A.rank
    if (rank == 0) {
      for (i <- 0 to dim)
        for (j <- 0 to dim)
          T(i)(j) = -1
      return Some((T.toArray map (_.toArray)).flatten)
    }
    else if (rank == dim - 1) if (U.det * V.det > 0) {
      val tempt = U.times(V)
      for (i <- 0 until dim)
        for (j <- 0 until dim)
          T(i)(j) = tempt.get(i, j)
    }
    else {
      val s = d(dim - 1)(dim - 1)
      d(dim - 1)(dim - 1) = -1
      val tempd = new Matrix(d.toArray map (_.toArray))
      val tempt = U.times(tempd.times(V))
      for (i <- 0 until dim)
        for (j <- 0 until dim)
          T(i)(j) = tempt.get(i, j)
      d(dim - 1)(dim - 1) = s
    }
    else {
      val tempd = new Matrix(d.toArray map (_.toArray))
      val tempt = U.times(tempd.times(V))
      for (i <- 0 until dim) {
        for (j <- 0 until dim)
          T(i)(j) = tempt.get(i, j)
      }
    }
    // Eq. (41) and (42).
    var scale = 1.0
    if (estimate_scale) {
      val temp_mean = new Array[Double](dim)
      val temp_var = new Array[Double](dim)
      val temp_d = ArrayBuffer.fill[Double](dim, 1)(0)
      for (i <- 0 until dim) {
        temp_mean(i) = 0
        temp_var(i) = 0
        temp_d(i)(0) = d(i)(i)
      }
      for (i <- 0 until num)
        for (j <- 0 until dim)
          temp_mean(j) += src_demean_f(i)(j)
      for (i <- 0 until dim)
        temp_mean(i) /= num
      for (i <- 0 until num)
        for (j <- 0 until dim)
          temp_var(j) += (src_demean_f(i)(j) - temp_mean(j)) * (src_demean_f(i)(j) - temp_mean(j))
      for (i <- 0 until dim)
        temp_var(i) /= num
      val td = new Matrix(temp_d.toArray map (_.toArray))
      val t2 = S.times(td).get(0, 0)
      var t1 : Double = 0
      for (i <- 0 until dim)
        t1 += temp_var(i)
      scale = 1.0 / t1 * t2
    }
    val temp_dst_mean_f = ArrayBuffer.fill[Double](dim, 1)(0)
    val temp_src_mean_f = ArrayBuffer.fill[Double](dim,1)(0)
    for (i <- 0 until dim) {
      temp_dst_mean_f(i)(0) = dst_mean(i)
      temp_src_mean_f(i)(0) = src_mean(i)
    }
    val temp_dst_mean = new Matrix(temp_dst_mean_f.toArray map (_.toArray))
    val temp_src_mean = new Matrix(temp_src_mean_f.toArray map (_.toArray))
    val temp_T_f =  ArrayBuffer.fill[Double](dim, dim)(0)
    for (i <- 0 until dim)
      for (j <- 0 until dim)
        temp_T_f(i)(j) = T(i)(j)
    val temp_T = new Matrix(temp_T_f.toArray map (_.toArray))
    val TT = temp_dst_mean.minus(temp_T.times(temp_src_mean).times(scale))
    for (i <- 0 until dim) {
      T(i)(dim) = TT.get(i, 0)
      for (j <- 0 until dim)
        T(i)(j) *= scale
    }
    Some((T.toArray map (_.toArray)).flatten)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import AffineTransformation2D._
case class AffineTransformation2D(coeffSeq: Array[Double]) {
  //---------------------------------------------------------------------------
  def isValid =
    coeffSeq.forall( ! _.isNaN ) &&
      coeffSeq.forall( _ != -1d )
  //---------------------------------------------------------------------------
  def isEqual(other: AffineTransformation2D) =
    (coeffSeq zip other.coeffSeq).forall{ case (a,b) => a == b }
  //---------------------------------------------------------------------------
  def isUnit = isEqual(AFFINE_TRANSFORMATION_2D_UNIT)
  //---------------------------------------------------------------------------
  def isNotUnit = !isUnit
  //---------------------------------------------------------------------------
  def getInverse() = {
    val m = new Matrix((coeffSeq ++ Array(0d,0d,1d)).grouped(AffineTransformation2D.order).toArray)
    AffineTransformation2D(m.inverse.getArray.flatten.dropRight(3))
  }
  //---------------------------------------------------------------------------
  def apply(p: Point2D)  =
    Point2D_Float(((coeffSeq(0) * p.x) + (coeffSeq(1) * p.y) + coeffSeq(2)).toFloat ,
      ((coeffSeq(3) * p.x) + (coeffSeq(4) * p.y) + coeffSeq(5)).toFloat)
  //---------------------------------------------------------------------------
  def apply(p: Point2D_Float)  =
    Point2D_Float(((coeffSeq(0) * p.x) + (coeffSeq(1) * p.y) + coeffSeq(2)).toFloat ,
      ((coeffSeq(3) * p.x) + (coeffSeq(4) * p.y) + coeffSeq(5)).toFloat)
  //---------------------------------------------------------------------------
  def apply(p: Point2D_Double)  =
    Point2D_Double((coeffSeq(0) * p.x) + (coeffSeq(1) * p.y) + coeffSeq(2) ,
      (coeffSeq(3) * p.x) + (coeffSeq(4) * p.y) + coeffSeq(5))
  //---------------------------------------------------------------------------
  def getOnlyTranslation = Array(coeffSeq(2),coeffSeq(5))
  //---------------------------------------------------------------------------
  //https://rosettacode.org/wiki/Matrix_multiplication#Scala
  def compose(atfB : AffineTransformation2D)  = {
    val lastRow = Array(0d,0d,1d) //last row is missing for this representation of affine transformation, so append it
    val extA = (this.coeffSeq ++ lastRow).grouped(3).toArray
    val extB = (atfB.coeffSeq ++ lastRow).grouped(3).toArray
    val r = for (row <- extA)
      yield for(col <- extB.transpose)
        yield row zip col map Function.tupled(_*_) reduceLeft (_+_)
    AffineTransformation2D(r.flatten.dropRight(3))
  }
  //---------------------------------------------------------------------------
  override def toString: String = coeffSeq.mkString("{", "," ,"}")
  //---------------------------------------------------------------------------
  def toShortString: String =
    (coeffSeq map { v =>  s"${f"$v%4.2f"}" }).mkString("{", "," ,"}")
  //---------------------------------------------------------------------------
  def onlyTranslationToShortString: String =
    (getOnlyTranslation map { v => s"${f"$v%4.2f"}" }).mkString("{", ",", "}")
  //---------------------------------------------------------------------------
  def getAsCsvLine(sep: String) =  (coeffSeq map (_.toString)).mkString(sep)
  //---------------------------------------------------------------------------
  def calculateResidualStdev(inputPointSeq: Array[Point2D_Double]
                             , expectedPointSeq: Array[Point2D_Double]
                             , aft: AffineTransformation2D) = {
    val residualSeq = (inputPointSeq zip expectedPointSeq).map{case (i,e) => aft(this(i)).getDistance(e)}
    StatDescriptive.getWithDouble(residualSeq).stdDev
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file AffineTransformation2D.scala
//=============================================================================