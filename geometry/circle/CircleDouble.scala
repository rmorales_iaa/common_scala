/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  15/Apr/2020
 * Time:  21h:30m
 * Description: None
 */
//=============================================================================
package com.common.geometry.circle
//=============================================================================
import com.common.geometry.point.{Point2D, Point2D_Double}
import com.common.geometry.rectangle.RectangleDouble
//=============================================================================
//=============================================================================
case class CircleDouble(centre: Point2D_Double, radius: Double) {
  //--------------------------------------------------------------------------
  def this(centre: Point2D, radius: Double) = this(Point2D_Double(centre.x, centre.y), radius)
  //--------------------------------------------------------------------------
  def isIn(p: Point2D_Double) = centre.getDistance(p) <= radius
  //--------------------------------------------------------------------------
  def isIn(p: Point2D) = centre.getDistance(Point2D_Double(p.x,p.y)) <= radius
  //--------------------------------------------------------------------------
  def getSurroundRectangle() = RectangleDouble(new Point2D_Double(centre.x - radius,centre.y - radius) ,
                                               new Point2D_Double(centre.x + radius,centre.y + radius))
  //--------------------------------------------------------------------------
  def getMin() =  new Point2D_Double(centre.x - radius,centre.y - radius)
  //--------------------------------------------------------------------------
  def getMax() =  new Point2D_Double(centre.x + radius,centre.y + radius)
  //--------------------------------------------------------------------------
  override def toString = s"centre:$centre radius: $radius"
  //--------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Circle.scala
//=============================================================================