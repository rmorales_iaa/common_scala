/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  15/Apr/2020
 * Time:  21h:30m
 * Description: None
 */
//=============================================================================
package com.common.geometry.circle
//=============================================================================
import com.common.geometry.point.{Point2D, Point2D_Float}
import com.common.geometry.rectangle.RectangleFloat
//=============================================================================
//=============================================================================
case class CircleFloat(centre: Point2D_Float, radius: Float) {
  //--------------------------------------------------------------------------
  def this(centre: Point2D, radius: Float) = this(Point2D_Float(centre.x, centre.y), radius)
  //---------------------------------------------------------------------------
  def isIn(p: Point2D_Float) = centre.getDistance(p) <= radius
  //---------------------------------------------------------------------------
  def isIn(p: Point2D) = centre.getDistance(Point2D_Float(p.x,p.y)) <= radius
  //---------------------------------------------------------------------------
  def getSurroundRectangle() = RectangleFloat(Point2D_Float(centre.x - radius,centre.y - radius) ,
                                              Point2D_Float(centre.x + radius,centre.y + radius))
  //---------------------------------------------------------------------------
  def getMin() =  Point2D_Float(centre.x - radius,centre.y - radius)
  //---------------------------------------------------------------------------
  def getMax() =  Point2D_Float(centre.x + radius,centre.y + radius)
  //--------------------------------------------------------------------------
  override def toString = s"centre:$centre radius: $radius"
  //--------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file CircleFloat.scala
//=============================================================================