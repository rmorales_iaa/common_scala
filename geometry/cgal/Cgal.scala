// The Computational Geometry Algorithms Library
//https://www.cgal.org/
//=============================================================================
package com.common.geometry.utils
//=============================================================================
import com.common.util.native.MyNativeLibrary
//=============================================================================
object Cgal extends MyNativeLibrary {
  //---------------------------------------------------------------------------
  myLoad
  //---------------------------------------------------------------------------
  def myLoad = load("AbsolutePath.nativeLibrary.path"
    , "AbsolutePath.cgal.library")
  //---------------------------------------------------------------------------
  @native def ellipseFit(point_seq: Array[Double], point_count: Long, fit: Array[Double]): Unit
  //---------------------------------------------------------------------------
}
//=============================================================================