//=============================================================================
//File: clientServerSide.scala
//=============================================================================
/** It implements a TCP client on the server side
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.tcp.server

//=============================================================================
// System import sectionName
//=============================================================================
import java.net.InetSocketAddress

import akka.actor.{ActorRef, Props}
import akka.io.Tcp._
import akka.util.ByteString
//=============================================================================
// User import sectionName
//=============================================================================

//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================

object ClientServerSide {
  
  //http://doc.akka.io/docs/akka/2.5.0/scala/actors.html/  (Recommended Practices)
  //-------------------------------------------------------------------------
  def props(server: ActorRef      
  ,  serverSocket: InetSocketAddress
  ,  userRead:Option[ByteString=>Unit] = None)    
    :  Props = Props(classOf[Client],server,serverSocket,userRead)
      
  //-------------------------------------------------------------------------  
}

//=============================================================================
//=============================================================================

class ClientServerSide(client: ActorRef      
  ,  clientSocket: InetSocketAddress
  ,  userRead:Option[ByteString=>Unit]) extends TcpCommon("clientServerSide",isClient=true,userRead=userRead) {

  //-------------------------------------------------------------------------
  // Local import
  //-------------------------------------------------------------------------
  import context.{become}
  
  //-------------------------------------------------------------------------
  // Code starts
  //------------------------------------------------------------------------- 
  
  //-------------------------------------------------------------------------
  //https://github.com/akka/akka/blob/v2.5.0/akka-docs/rst/scala/code/docs/io/ReadBackPressure.scala
  override def preStart: Unit = {
    
    context watch client // sign death pact: this actor terminates when connection breaks
    
    peerSocket = clientSocket
    connectionList.add(client) 
    client ! ResumeReading    
    
    info(s"Client on server side created to manage client connection:" + peerSocket.toString)
  }

  //-------------------------------------------------------------------------
  
  def receive = {
    
    case Write(data,_)        => client ! Write(data)
    case Received(data)       => { super.read(data); client ! ResumeReading}
    case                    _ => become(workingState)
  }
  

  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //End of case class 'ClientServerSide'

//=============================================================================
// End of file 'clientServerSide.scala' file
//=============================================================================
