//=============================================================================
//File: server.scala
//=============================================================================
/** It implements a TCP server
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.tcp.server

//=============================================================================
// System import sectionName
//=============================================================================
import java.net.InetSocketAddress

import akka.actor.{ActorRef, Props}
import akka.io.Tcp._
import akka.util.ByteString

//=============================================================================
// User import sectionName
//=============================================================================

//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================

object Server {

  //https://github.com/akka/akka/blob/v2.5.0/akka-docs/rst/scala/code/docs/io/ReadBackPressure.scala
  //http://doc.akka.io/docs/akka/2.5.0/scala/actors.html/  (Recommended Practices)
  //-------------------------------------------------------------------------

  def props(clientClass: Class[_]
    ,  name: String                                                                         
    ,  port: Int
    ,  maxCLientCount: Int
    ,  maxBufferStorageSize: Long = 100000000L
    ,  userRead:Option[ByteString=>Unit] = None
    ,  userServerConnectedClient:Option[()=>Unit] = None
    ,  userWriteFailed:Option[()=>Unit] = None
    ,  userAck:Option[()=>Unit] = None
    ,  userClosed:Option[()=>Unit] = None
    ,  userUnknowMessage:Option[()=>Unit] = None)
      :  Props = Props(classOf[Server],clientClass
                       ,name,port,maxCLientCount,maxBufferStorageSize
                       ,userRead,userServerConnectedClient,userWriteFailed,userAck,userClosed,userUnknowMessage)
                                                                                  
  //-------------------------------------------------------------------------  
}

//=============================================================================
//=============================================================================
class Server(clientClass: Class[_]  //class used to manage the client
  ,  name: String                                                                         
  ,  port: Int
  ,  maxCLientCount: Int
  ,  maxBufferStorageSize: Long
  ,  userRead:Option[ByteString=>Unit] 
  ,  userServerConnectedClient:Option[()=>Unit] 
  ,  userWriteFailed:Option[()=>Unit]
  ,  userAck:Option[()=>Unit] 
  ,  userClosed:Option[()=>Unit] 
  ,  userUnknowMessage:Option[()=>Unit])  
    extends TcpCommon(name,"localhost",port,false,maxBufferStorageSize
                      ,userRead,userWriteFailed,userAck,userClosed,userUnknowMessage) {
      
  //-------------------------------------------------------------------------
  // Local import
  //-------------------------------------------------------------------------  
  import context.become
  
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
  
  if (peerSocket != null) tcpManager ! Bind(self, peerSocket, pullMode = true)
  
  //-------------------------------------------------------------------------  
  def receive = {
    
    case MyStartState => become(bindingState)
    case MyWorking => become(workingState)
  }
    
  //-------------------------------------------------------------------------
  def bindingState: Receive = {   
    
    case b @ Bound(localAddress) => myBound(localAddress)
    case CommandFailed(b: Bind)  => myBindError(b)
  }
  
  //-------------------------------------------------------------------------
  def workingState(listener: ActorRef): Receive = {
    
      case c @ Connected(remote, local) => myConnected(listener,remote,local)
      case e: Any => super.workingState(e)
  }
     
  //-------------------------------------------------------------------------
  def myBound(localAdress: InetSocketAddress) {

    info(s"Server '$name' is ready to accept incoming clients on: "+localAdress)
        
    become(workingState(sender))
    initialized = true
    
    sender ! ResumeAccepting(batchSize = 1) // ready to accept a NEW client
  }
    
  //-------------------------------------------------------------------------
  def myConnected(listener: ActorRef
  ,  localAdress:  InetSocketAddress
  ,  remoteAdress:  InetSocketAddress) {

    if (connectionList.size >= maxCLientCount){
      warning(s"Socket server '$name' client '$remoteAdress' will not accepted due to max clients allowed limit")
      listener ! Abort
      return
    }
     
    val client = context.actorOf(Props(clientClass, sender, remoteAdress, userRead))
    connectionList.add(client) //append to storage
    info(s"Socket server '$name' has added a NEW client. Client count: " +connectionList.size)
    if (connectionList.size == maxCLientCount)
      warning(s"Socket server '$name' max client count reached, NEW clients will not allowed")

    //remain half-open when the remote side closed its writing end allowing to send byteSeq before fully closing the connection
    sender ! Register(client, keepOpenOnPeerClosed = true) //manage itself the byteSeq received
    listener ! ResumeAccepting(batchSize = 1)  // ready to accept a NEW client
    if (userServerConnectedClient.isDefined) userServerConnectedClient.get()
    else info(s"'$name' client connected")
  }  
   
  //-------------------------------------------------------------------------
  def myBindError(b: Bind) {
       
    error(s"Socket server '$name'. Command failed. Probable port '$port' already in use")
    error("Bind local address "+b.localAddress)
  }
  
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //End of case class 'Server'

//=============================================================================
// End of 'server.scala' file
//=============================================================================
