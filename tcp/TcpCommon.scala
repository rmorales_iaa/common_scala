//=============================================================================
//File: tcpCommon.scala
//=============================================================================
/** It implements a commonHenosis activities of TCP
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.tcp

//=============================================================================
// System import sectionName
//=============================================================================
import akka.actor.{Actor, ActorRef, ActorSystem, SupervisorStrategy}
import akka.io.{IO, Tcp}
import akka.util.ByteString
import java.net.{InetSocketAddress, ServerSocket}
import java.util.concurrent.ConcurrentLinkedQueue

import Tcp._
//=============================================================================
// User import sectionName
//=============================================================================

//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================
//http://doc.akka.io/docs/akka/current/scala/io-tcp.html
//Sample: https://github.com/akka/akka/blob/v2.5.0/akka-docs/rst/scala/code/docs/io/EchoServer.scala
//A connection or peer connection) has a read and a write side.
//options: Tcp.Write (up to 2GB of byteSeq transmitted), ACK-based (Data + Object is sent, Object is returned if successful, on other case is sent Tcp.NoAck)
//         .Pull reading. Writer suspend of write until reader actor signals with a ResumeReading message that it is ready
//Only one actor will write at the same time on the same connection (risk of no consistent result)

  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
object TcpCommon extends MyLogger {
  
  //-------------------------------------------------------------------------
  // Constants
  //-------------------------------------------------------------------------
  
  //Common server and client messages
  final case object Ack extends Event

  //states
  final case object MyStartState extends Event
  final case object MyWorking extends Event

  var actorSystem : ActorSystem = null
  //-------------------------------------------------------------------------
  // Variable declaration
  //-------------------------------------------------------------------------

  //-------------------------------------------------------------------------
  def createSocket(address: String, port: Int) : InetSocketAddress = {

    try { new InetSocketAddress(address, port) }
    catch {
      case e: Exception => exception(e, s"Can not process the socket with parameters '$address' and '$port'")
      return null}
  }

  //-------------------------------------------------------------------------
  def isLocalPortInUse(port: Int) : Boolean = {
    try {
        new ServerSocket(port).close
        return false
    }
    catch {
     case e: Exception =>  exception(e, s"Port $port is use")
    }
  }
  //-------------------------------------------------------------------------
}

//=============================================================================
//=============================================================================

abstract class TcpCommon(name: String
                         , address: String =""
                         , port: Int       =1
                         , isClient:  Boolean
                         , maxBufferStorageSize: Long = 100000000L
                         , userRead:Option[ByteString=>Unit] = None
                         , userWriteFailed:Option[()=>Unit] = None
                         , userAck:Option[()=>Unit] = None
                         , userClosed:Option[()=>Unit] = None
                         , userUnknownMessage:Option[()=>Unit] = None) extends Actor with MyLogger{

  //-------------------------------------------------------------------------
  // Local import
  //-------------------------------------------------------------------------
  import TcpCommon._

  //-------------------------------------------------------------------------
  // Variable declaration
  //-------------------------------------------------------------------------

  //flags
  protected var initialized = false
  protected var isClosed = true

  //stats
  private var readByte: Long = 0L
  private var writeByte: Long = 0L

  //socket
  protected var peerSocket = if (address.isEmpty) null else createSocket(address,port)
  protected val connectionList = new ConcurrentLinkedQueue[ActorRef]
  protected var tcpManager:  ActorRef = null

  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
  def init() : Boolean = {
    try {
      tcpManager =  IO(Tcp)(actorSystem)
      info("IO TCP actor manager system initialized")
    }
    catch {
      case e: Exception => exception(e, "Error initializating the IO TCP actor manager system")
        false
    }
  }


  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------

  //-------------------------------------------------------------------------
  override def preStart(): Unit = self ! MyStartState  //switch state

  //-------------------------------------------------------------------------
  // there is not recovery for broken connections
  override val supervisorStrategy = SupervisorStrategy.stoppingStrategy

  //-------------------------------------------------------------------------
  override def postStop(): Unit = {
    warning(s"'$name' read byte count :$readByte")
    warning(s"'$name' write byte count:$writeByte")
    isClosed = true
  }

  //-------------------------------------------------------------------------
  def workingState: Receive = {

      //received
      case Received(data)          => { read(data) }
      case Ack                     => acknowledge

      //command
      case Write(data, _)          => write(data)
      case Close                   => close
      case ConfirmedClose          => confirmedClose
      case Abort                   => abort

      //command error
      case CommandFailed(w: Write) => writeFailed

      //closing messages
      case Closed                  => close
      case ConfirmedClosed         => confirmedClosed
      case Aborted                 => aborted
      case PeerClosed              => peerClosed
      case ErrorClosed             => errorClosed

      case m : Message             => unKnownMessage(m)
  }

  //-------------------------------------------------------------------------
  def myRead(data: ByteString){  //can be override by user

    if (userRead.isDefined) userRead.get(data)
    else warning(s"'$name' TcpCommon byteSeq received: "+ data.toString())
  }

  //-------------------------------------------------------------------------
  def myWriteFailed(){  //can be override by user

    if (userWriteFailed.isDefined) userWriteFailed.get()
    else info(s"'$name' TcpCommon write failed")
  }

  //-------------------------------------------------------------------------
  def myAck(){  //can be override by user

    if (userAck.isDefined) userAck.get()
    else { //too much logging
      //info(s"'$name' TcpCommon ACK received")
    }
  }

  //-------------------------------------------------------------------------
  def myClosed(){  //can be override by user

    if (userClosed.isDefined) userClosed.get()
    else info(s"'$name' TcpCommon closed")
  }

  //-------------------------------------------------------------------------
  def myUnknownMessage(m: Message){  //can be override by user

    if (userUnknownMessage.isDefined) userUnknownMessage.get()
    else info(s"'$name' TcpCommon unknown message"+m.toString)
  }

  //-------------------------------------------------------------------------
  def isInitialized = initialized

  //-------------------------------------------------------------------------
  protected def read(data: ByteString){

    readByte += data.size //stats
    myRead(data) //user call
  }

  //-------------------------------------------------------------------------
  private def acknowledge() {
    myAck
  }

  //-------------------------------------------------------------------------
  private def write(data: ByteString) {   //command

    val it=connectionList.iterator
    while(it.hasNext)
      it.next ! Write(data, Ack)
    
    writeByte+=data.size
  }
  
  //-------------------------------------------------------------------------
  private def close() {   //command
   
    myIterate(Close)      
    info(s"'$name' '$name' TcpCommon has send close event")
  }
  
  //-------------------------------------------------------------------------
  private def confirmedClose() { //command
    
    myIterate(ConfirmedClose)  
    info(s"'$name' TcpCommon has send confirmed closed event")
  }

  //-------------------------------------------------------------------------
  private def abort() {  //command
    
    myIterate(Abort)             
    info(s"'$name' TcpCommon has send abort event")
  }
  
  //-------------------------------------------------------------------------
  private def closed() {
    
    context stop self
    info(s"'$name' TcpCommon has flush writes and close its end point and the server has close its endpoint")
    myClosed //user call
  }
  
  //-------------------------------------------------------------------------
  private def confirmedClosed() {
        
    context stop self
    info(s"'$name' TcpCommon both, server and client have flush writes and closed the endpoint")
    myClosed //user call
  }
  
  //-------------------------------------------------------------------------
  private def aborted() {
    
    context stop self
    info(s"'$name' TcpCommon has closed the endpoint ASAP without flush writes and the server has close its endpoint")
    myClosed //user call
  }
  
  //-------------------------------------------------------------------------
  private def peerClosed() {
            
    context stop self
    info(s"'$name' TcpCommon has closed its endpoint")
    myClosed //user call
  }
  
  //-------------------------------------------------------------------------
  private def errorClosed() {
            
    context stop self
    info(s"'$name' TcpCommon an error has closed the connection")
    myClosed //user call
  }
  
  //-------------------------------------------------------------------------
  private def unKnownMessage(m: Message) {
       
    context stop self
    if (m!=null) info(s"'$name' unknown message "+m.toString())
    else info(s"'$name' TcpCommon unknown message")
    myClosed //user call
  }
 
  //-------------------------------------------------------------------------
  private def writeFailed() {
   
    info(s"'$name' TcpCommon operating system buffer is full")
    myWriteFailed
  }
  
  //-------------------------------------------------------------------------  
  private def myIterate(m: Message) {
    
    val it=connectionList.iterator
    while(it.hasNext)
      it.next ! m
  }
    
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //End object 'TcpCommon'

//=============================================================================
// End of 'TcpCommon.scala' file
//=============================================================================
