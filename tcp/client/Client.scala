//=============================================================================
//File: client.scala
//=============================================================================
/** It implements a TCP client
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.tcp.client
//=============================================================================
// System import sectionName
//=============================================================================
import akka.actor.Props
import akka.io.Inet.SocketOption
import akka.io.Tcp
import akka.io.Tcp._
import akka.util.ByteString

//=============================================================================
// User import sectionName
//=============================================================================

//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================

object Client {
  //http://stackoverflow.com/questions/33747858/akka-tcp-client-how-can-i-send-a-message-over-tcp-using-akka-actor
  //http://doc.akka.io/docs/akka/2.5.0/scala/actors.html/  (Recommended Practices)
  //-------------------------------------------------------------------------
  def props(name: String
  ,  address: String
  ,  port: Int 
  ,  timeBetweenReconnectionsInS: Int
  ,  maxBufferStorageSize: Long = 100000000L
  ,  socketKeepAlive : Boolean = true
  ,  socketOOBIOnline : Boolean = true
  ,  socketNoDelay : Boolean = false
  ,  socketReuseAddress : Boolean = false
  ,  socketReceiveBufferize : Int = (65 * 1024)
  ,  socketSendBufferSize : Int = (65 * 1024)
  ,  userRead:Option[ByteString=>Unit] = None
  ,  userClientConnectedServer:Option[()=>Unit] = None
  ,  userWriteFailed:Option[()=>Unit] = None
  ,  userAck:Option[()=>Unit] = None
  ,  userClosed:Option[()=>Unit] = None
  ,  userUnknowMessage:Option[()=>Unit] = None)
    :  Props = Props(classOf[Client]
                     ,name, address, port, timeBetweenReconnectionsInS, maxBufferStorageSize
                     ,socketKeepAlive, socketOOBIOnline, socketNoDelay, socketReuseAddress, socketReceiveBufferize, socketSendBufferSize
                     ,userRead, userClientConnectedServer, userWriteFailed, userAck, userClosed, userUnknowMessage)
      
  //-------------------------------------------------------------------------  
}

//=============================================================================
//=============================================================================

class Client(name: String
  ,  address: String
  ,  port: Int 
  ,  timeBetweenReconnectionsInS: Int 
  ,  maxBufferStorageSize: Long 
  ,  socketKeepAlive : Boolean
  ,  socketOOBIOnline : Boolean
  ,  socketNoDelay : Boolean
  ,  socketReuseAddress : Boolean
  ,  socketReceiveBufferize : Int 
  ,  socketSendBufferSize : Int
  ,  userRead:Option[ByteString=>Unit]
  ,  userClientConnectedServer:Option[()=>Unit]
  ,  userWriteFailed:Option[()=>Unit]
  ,  userAck:Option[()=>Unit]
  ,  userClosed:Option[()=>Unit]
  ,  userUnknowMessage:Option[()=>Unit]) 
    extends TcpCommon(name
                      , address
                      , port
                      ,true
                      , maxBufferStorageSize
                      , userRead
                      , userWriteFailed
                      , userAck
                      , userClosed
                      , userUnknowMessage) with MyLogger{
  //-------------------------------------------------------------------------
  // Local import
  //-------------------------------------------------------------------------  
  import context.become
  
  //-------------------------------------------------------------------------
  // Code starts
  //------------------------------------------------------------------------- 

  init

  //http://stackoverflow.com/questions/3192940/best-socket-options-for-client-and-sever-that-continuously-transfer-data 
  private val socketOption = List[SocketOption](
    SO.KeepAlive(socketKeepAlive),
    SO.OOBInline(socketOOBIOnline),
    SO.TcpNoDelay(socketNoDelay),
    SO.ReuseAddress(socketReuseAddress),
    SO.ReceiveBufferSize(socketReceiveBufferize),
    SO.SendBufferSize(socketSendBufferSize))
      
    //connect with the server
    if (peerSocket != null){
      info(s"Socket client '$name' connecting with server '$address' at port '$port'")
      tcpManager ! Connect(peerSocket, options=socketOption) //NEW actor ins push reading mode
      //tcpManager ! Connect(peerSocket, options=socketOption)//, pullMode = true) //NEW actor in pull mode
  }
  //-------------------------------------------------------------------------  
  def receive = {

    case Received(data)       => { super.read(data) ; sender ! ResumeReading}
    case MyStartState         => become(connectingState)
    case MyWorking            => become(workingState)
  }
  //-------------------------------------------------------------------------  
  def connectingState: Receive = {   
    
    case c @ Connected(remote, local) => myConnected(c) //user call
    case CommandFailed(_: Connect)    => myConnectionError
  }
  //-------------------------------------------------------------------------
  def myConnected(c: Tcp.Connected) {
    
    warning(s"Socket client '$name' connected with server '$address' at port '$port'")
    warning(s"Socket client '$name' address local/remote'" + c.toString)
    
    connectionList.add(sender) //append to storage
    sender ! Register(self)  //activate the connection. Itself will manage byteSeq of connection
    sender ! ResumeReading //allow reading to peer    
    become(workingState)
       
    initialized = true
    isClosed = false
    
    if (userClientConnectedServer.isDefined) userClientConnectedServer.get()
    else info(s"'$name' connected to server")
    info(s"Socket client '$name' has requested its twoImages on server to receive byteSeq")
  }
  //-------------------------------------------------------------------------
  def myConnectionError() {
      
    warning(s"Socket client '$name' error connecting with server '$address' at port '$port', reconnecting in '$timeBetweenReconnectionsInS's")
    Thread.sleep(timeBetweenReconnectionsInS * 1000)
    warning(s"Socket client '$name' reconnecting with server '$address' at port '$port'")
    tcpManager ! Connect(peerSocket,options=socketOption)
    
    become(connectingState)
  }
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //End of case class 'Client'

//=============================================================================
// End of 'client.scala' file
//=============================================================================
