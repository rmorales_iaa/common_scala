
package spice.basic;

/**
<p>Class Ellipsoid represents tri-axial ellipsoids in three-dimensional,
Euclidean space.

<h3> Disclaimer </h3>

<pre>
   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY THE
   CALIFORNIA INSTITUTE OF TECHNOLOGY (CALTECH) UNDER A U.S.
   GOVERNMENT CONTRACT WITH THE NATIONAL AERONAUTICS AND SPACE
   ADMINISTRATION (NASA). THE SOFTWARE IS TECHNOLOGY AND SOFTWARE
   PUBLICLY AVAILABLE UNDER U.S. EXPORT LAWS AND IS PROVIDED "AS-IS"
   TO THE RECIPIENT WITHOUT WARRANTY OF ANY KIND, INCLUDING ANY
   WARRANTIES OF PERFORMANCE OR MERCHANTABILITY OR FITNESS FOR A
   PARTICULAR USE OR PURPOSE (AS SET FORTH IN UNITED STATES UCC
   SECTIONS 2312-2313) OR FOR ANY PURPOSE WHATSOEVER, FOR THE
   SOFTWARE AND RELATED MATERIALS, HOWEVER USED.

   IN NO EVENT SHALL CALTECH, ITS JET PROPULSION LABORATORY, OR NASA
   BE LIABLE FOR ANY DAMAGES AND/OR COSTS, INCLUDING, BUT NOT
   LIMITED TO, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
   INCLUDING ECONOMIC DAMAGE OR INJURY TO PROPERTY AND LOST PROFITS,
   REGARDLESS OF WHETHER CALTECH, JPL, OR NASA BE ADVISED, HAVE
   REASON TO KNOW, OR, IN FACT, SHALL KNOW OF THE POSSIBILITY.

   RECIPIENT BEARS ALL RISK RELATING TO QUALITY AND PERFORMANCE OF
   THE SOFTWARE AND ANY RELATED MATERIALS, AND AGREES TO INDEMNIFY
   CALTECH AND NASA FOR ALL THIRD-PARTY CLAIMS RESULTING FROM THE
   ACTIONS OF RECIPIENT IN THE USE OF THE SOFTWARE.
</pre>


<h3> Required_Reading </h3>


<h3> Keywords </h3>


<h3> Parameters </h3>


<h3> Exceptions </h3>


<h3> Particulars </h3>

   <pre>

   JNISpice Ellipsoids are centered at the origin and have semi-axes
   aligned with the x, y, and z coordinate axes.
   JNISpice Ellipsoids are required to have positive semi-axis lengths.

   To find the closest point on an Ellipsoid to a given point,
   see class {@link spice.basic.EllipsoidPointNearPoint}.

   To find the closest point on an Ellipsoid to a given line,
   see class {@link spice.basic.EllipsoidLineNearPoint}.

   To find the intercept on an Ellipsoid of a given Ray,
   see class {@link spice.basic.RayEllipsoidIntercept}.

   </pre>

<h3> Examples </h3>



<h3> Version </h3>

<h3> Version 1.0.1 01-JAN-2022 (NJB) </h3>

Updated markup to work with javadoc version 8.*.

<p>Added methods normalToPoint and directionToPoint.

<p>Removed method toRectangular.

<h3> Version 1.1.0 13-JUL-2017 (EDW) </h3>

Added toRectangular method using parametric description.

<h3>Version 1.0.0 28-NOV-2009 (NJB)</h3>

*/
public class Ellipsoid extends Object
   {


   //
   // Fields
   //
   private double[]            radii;


   //
   // Constructors
   //

   /**
   <p>
   No-arguments constructor. This constructor generates a unit sphere.
   </p>

   <h3>I/O</h3>


   <h3> Particulars </h3>


   <h3> Exceptions </h3>

   <h3> Examples </h3>


    */
   public Ellipsoid()
      {
      radii    = new double[3];

      radii[0] = 1.0;
      radii[1] = 1.0;
      radii[2] = 1.0;
      }


   /**
   <p>
   Copy constructor. This constructor creates a deep copy.
   </p>

   <h3>I/O</h3>


   <h3> Particulars </h3>


   <h3> Exceptions </h3>


   <h3> Examples </h3>


   */
   public Ellipsoid( Ellipsoid ell )
      {
      radii = new double[3];

      for ( int i = 0;  i < 3;  i++ )
         {
         this.radii[i] = ell.radii[i];
         }
      }


   /**
   <p>
   Construct an ellipsoid from three semi-axis lengths.
   </p>

   <h3>I/O</h3>


   <h3> Particulars </h3>


   <h3> Exceptions </h3>


   <h3> Examples </h3>


   */
   public Ellipsoid( double         a,
                     double         b,
                     double         c  )

      throws SpiceException
   {

      String     msg;

      radii    = new double[3];

      radii[0] = a;
      radii[1] = b;
      radii[2] = c;


      for ( int i = 0;  i < 3;  i++ )
      {
         if ( radii[i] <= 0.0 )
         {
            SpiceException exc = SpiceErrorException.create(

               "Ellipsoid",
               "SPICE(VALUEOUTOFRANGE)",

               "Ellipsoid radii must be positive, but " +
               "matchRadiusMas " + i + " is " + radii[i]            );

            throw exc;
         }
      }
   }


   //
   // Methods
   //

   /**
   <p>
   Get radii of this Ellipsoid.
   </p>

   <h3>I/O</h3>


   <h3> Particulars </h3>

   <pre>
   None.
   </pre>

   <h3> Exceptions </h3>


   <h3> Examples </h3>

   */
   public double[] getRadii()
   {
      double[]   retArray = new double[3];

      System.arraycopy ( this.radii, 0, retArray, 0, 3 );

      return ( retArray );
   }


   /**
   <p>
   Find the unit outward surface normal at a specified point
   on this Ellipsoid's surface.
   </p>

   <p>For important details concerning this module's function, please
   refer to the CSPICE routine
   <u href="../../../../doc/html/cspice/surfnm_c.html">surfnm_c</u>.
   </p>


   <h3>I/O</h3>


   <h3> Particulars </h3>

   <pre>
   None.
   </pre>

   <h3> Exceptions </h3>


   <h3> Examples </h3>

   */
   public Vector3 getNormal( Vector3 point ) throws SpiceException
   {
      double[] normal = CSPICE.surfnm( radii[0],
                                       radii[1],
                                       radii[2],
                                       point.toArray() );

      return (  new Vector3( normal )  );
   }



   /**
   <p>
   Find the limb of this Ellipsoid, as seen from a given viewing
   location.
   </p>

   <p>For important details concerning this module's function, please
   refer to the CSPICE routine
   <u href="../../../../doc/html/cspice/edlimb_c.html">edlimb_c</u>.
   </p>


   <h3>I/O</h3>


   <h3> Particulars </h3>

   <pre>
   None.
   </pre>

   <h3> Exceptions </h3>


   <h3> Examples </h3>

   */

   public Ellipse getLimb( Vector3  viewpt ) throws SpiceException
   {
      double[] limbArray = CSPICE.edlimb( radii[0],
                                          radii[1],
                                          radii[2],
                                          viewpt.toArray() );

      return ( new Ellipse( limbArray )  );
   }



   /**
   <p>
   Find the surface point on this Ellipsoid at which the outward normal
   vector is parallel to a specified vector.
   </p>
   */
   public Vector3 normalToPoint( Vector3 normal ) throws SpiceException
   {
      return new Vector3(  CSPICE.ednmpt( radii[0],
                                          radii[1],
                                          radii[2],
                                          normal.toArray() )  );
   }


   /**
   <p>
   Find the surface point on this Ellipsoid lying on the ray emanating from
   the origin and parallel to a specified vector.
   </p>
   */
   public Vector3 directionToPoint( Vector3 dir ) throws SpiceException
   {
      return new Vector3(  CSPICE.edpnt( dir.toArray(),
                                         radii[0],
                                         radii[1],
                                         radii[2]     )  );
   }




   /**
   <p>
   Display an Ellipsoid as a string; override Object's toString() method.
   </p>

   <h3>I/O</h3>


   <h3> Particulars </h3>

   <pre>
   None.
   </pre>

   <h3> Exceptions </h3>


   <h3> Examples </h3>

   */
   public String toString()
      {
      String endl = System.getProperty( "line.separator" );

      String str  = "Ellipsoid Radii:" + endl + ( new Vector3(radii) );

      return ( str );
      }




   }
