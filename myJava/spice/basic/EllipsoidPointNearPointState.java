
package spice.basic;

import spice.basic.CSPICE;

/**
Class EllipsoidPointNearPointState represents the result of an
Ellipsoid-Point near point state computation. This computation
finds the state of the nearest point on a given ellipsoid to
the position component of a specified state vector.

<p>Below we'll call the position component of the input state vector the
"view point."

<p>The view point may be inside the ellipsoid.

<p> An EllipsoidPointNearPointState instance consists of

<ol>

<li> A {@link Vector6} instance representing the state of the near
point on the ellipsoid. </li>
<li> A double array representing the altitude and derivative with
respect to time of the view point relative to the ellipsoid</li>
<li> A "found" flag indicating whether a valid near point velocity was
computed.

</ol>

<p> This is a low-level, "pure geometric" class having
functionality analogous to that provided
by the CSPICE routine dnearp_c.


<h2>Version and Date</h2>

<h3>Version 1.0.0 14-SEP-2021 (NJB)</h3>

*/
public class EllipsoidPointNearPointState
{
   //
   // Fields
   //
   private Vector6             nearPointState;
   private double[]            altitudeState = new double[2];
   private boolean             found;

   //
   // Constructors
   //

   /**
   Construct an EllipsoidPointNearPointState from an Ellipsoid and
   a specified state.

   <h2>Inputs</h2>
   <pre>
   ellipsoid      represents a tri-axial ellipsoid having its x, y, and z
                  axes aligned respectively with the x, y, and z axes of
                  the reference frame relative to which the input view point
                  state is expressed.

                  Below we'll refer to the semi-axis lengths as `u`, `b`,
                  and `c`.

                  Units of the semi-axis lengths must be consistent with the
                  distance unit of the input view point state.


   state          is a 6-vector giving the position and velocity of some
                  object, which we'll call the "view point," in the
                  body-fixed coordinates of the ellipsoid.

                  The first three components of `state' are the view point's
                  x, y, and z position components; the view point's x, y,
                  and z velocity components follow.

                  In body-fixed coordinates, the semi-axes of the ellipsoid
                  are aligned with the x, y, and z-axes of the coordinate
                  system.
   </pre>

   <h2>Outputs</h2>

   <pre>
   The EllipsoidPointNearPointState instance created by this constructor
   contains a {@link Vector6} member representing the near point's state,
   which we'll call `dnear`, and a boolean value indicating whether a state
   was computed; we'll call this value `found`. These are described below.
   Also see the Inputs sectionName above for descriptions of the view point
   state's components.

   dnear       is the state of the nearest point to the view point on
               the surface of the input ellipsoid. The first
               three components of `stx' are the near point's x, y,
               and z position components; the near point's x, y,
               and z velocity components follow.

               `dnear' is specified relative to the same reference
               frame as are `state'.

               `dnear' is defined if and only if the velocity of the
               near point is computable, as indicated by the instance
               member `found'.

               The position unit of `dnear' are the same as those of
               `state' and `u', `b', and `c'. The time unit are the same
               as those of `state'.


   found       is a logical flag indicating whether `dnear' is
               defined. `found' is true if and only if the velocity
               of the near point is computable. Note that in some cases
               the near point may computable while the velocity is not.

   </pre>


   <h2>Examples</h2>

   <p>
      The numerical results shown for these examples may differ across
      platforms. The results depend on the SPICE kernels used as
      input, the compiler and supporting libraries, and the machine
      specific arithmetic implementation.

   <ol>

   <li><pre>
   Suppose you wish to compute the velocity of the ground track
   of a satellite as it passes over a location on Mars and that
   the moment of passage has been previously determined. (We
   assume that the spacecraft is close enough to the surface that
   light time corrections do not matter.)
   </pre>

   <p>  Use the meta-kernel shown below to load the required SPICE
        kernels.

   <pre>

   KPL/MK

   File: EllipsoidPointNearPointStateEx1.tm

   This meta-kernel is intended to support operation of SPICE
   example programs. The kernels shown here should not be
   assumed to contain adequate or correct versions of dataByteSeq
   required by SPICE-based user applications.

   In order for an application to use this meta-kernel, the
   kernels referenced here must be present in the user's
   current working directory.

   The names and contents of the kernels referenced
   by this meta-kernel are as follows:

      File name                        Contents
      ---------                        --------
      pck00010.tpc                     Planet orientation and
                                       radii
      naif0012.tls                     Leapseconds
      de430.bsp                        Planetary ephemeris
      mar097.bsp                       Mars satellite ephemeris
      mro_psp4_ssd_mro95a.bsp          MRO ephemeris

   \begindata

      KERNELS_TO_LOAD = ( 'pck00010.tpc',
                          'naif0012.tls',
                          'de430.bsp',
                          'mar097.bsp',
                          'mro_psp4_ssd_mro95a.bsp' )

   \begintext

      End of meta-kernel
   </pre>

   <p> Example code begins here.
<pre>{@code
   //
   // Program EllipsoidPointNearPointStateEx1
   //
   import spice.basic.*;

   //
   // Compute the velocity of the ground track of u
   // satellite as it passes over a location on Mars.
   //
   class EllipsoidPointNearPointStateEx1
   {
      //
      // Load SPICE shared library.
      //
      static{ System.loadLibrary( "JNISpice" ); }


      public static void main( String[] args )

         throws SpiceException
      {
         //
         // Local constants
         //
         final String META  = "EllipsoidPointNearPointStateEx1.tm";

         try
         {
            //
            // Load kernel files via the meta-kernel.
            //
            KernelDatabase.load( META );

            //
            // Convert the TDB input time string to seconds past
            // J2000, TDB.
            //
            TDBTime et = NEW TDBTime( "2007 SEP 30 00:00:00 TDB" );

            //
            // First get the semi-axis lengths of the body.
            //
            Body target = NEW Body( "MARS" );

            double[] radii = target.getValues( "RADII" );

            double a = radii[0];
            double b = radii[1];
            double c = radii[2];

            //
            // Get the geometric state of the spacecraft with
            // respect to the target in the body-fixed reference frame
            // at `et' and compute the state of the sub-spacecraft point.
            //
            AberrationCorrection abcorr = NEW AberrationCorrection( "NONE" );
            Body                 obsrvr = NEW Body( "MRO" );
            ReferenceFrame       fixref = NEW ReferenceFrame( "IAU_MARS" );

            StateVector sv = NEW StateVector( obsrvr, et,    fixref,
                                              abcorr, target        );

            Ellipsoid ell = NEW Ellipsoid( u, b, c );

            EllipsoidPointNearPointState npState =
               NEW EllipsoidPointNearPointState( ell, sv );

            if ( npState.wasFound() )
            {
               //
               // `dnear` contains the state of the subspacecraft point.
               //
               Vector6  dnear  = npState.getState();
               Vector3  vel    = dnear.getVector3(1);
               double[] velArr = vel.toArray();

               System.out.printf( "Ground-track velocity (km/s): " +
                                  "%9.6f %9.6f %9.6f%n",
                                  velArr[0], velArr[1], velArr[2] );

               System.out.printf( "Ground-track speed    (km/s): " +
                                  "%9.6f%n", vel.norm()  );
            }
            else
            {
               System.out.printf( "DNEAR is degenerate.%n" );
            }
         }
         catch ( SpiceException exc )
         {
            exc.printStackTrace();
         }
      }
   }

}</pre>

   <p> When this program was executed on a Mac/Intel/cc/64-bit/Java 1.8
   platform, the output was:
<pre>

   Ground-track velocity (km/s):  0.505252  1.986553 -2.475506
   Ground-track speed    (km/s):  3.214001

</pre>
   </li>



   <li><pre>
   Suppose you wish to compute the one-way doppler shift of u
   radar mounted on board a spacecraft as it passes over some
   region. Moreover, assume that for your purposes it is
   sufficient to neglect effects of atmosphere, topography and
   antenna pattern for the sake of this computation.
   </pre>

   <p>Use the meta-kernel of the first example program to load the
   required SPICE kernels.

   <p> Example code begins here.

<pre>{@code
   //
   // Program EllipsoidPointNearPointStateEx2
   //
   import spice.basic.*;

   //
   // Compute the one-way doppler shift of a radar mounted
   // on board a spacecraft as it passes over some region.
   //
   class EllipsoidPointNearPointStateEx2
   {
      //
      // Load SPICE shared library.
      //
      static{ System.loadLibrary( "JNISpice" ); }


      public static void main( String[] args )

         throws SpiceException
      {
         //
         // Local constants
         //

         //
         // Use the meta-kernel from the first example.
         //
         final String META  = "EllipsoidPointNearPointStateEx1.tm";

         //
         // Define the central frequency of the radar,
         // in megahertz.
         //
         final double RCFRQ = 20.0;

         try
         {
            //
            // Load kernel files via the meta-kernel.
            //
            KernelDatabase.load( META );

            //
            // Convert the TDB input time string to seconds past
            // J2000, TDB.
            //
            TDBTime et = NEW TDBTime( "2007 SEP 30 00:00:00 TDB" );

            //
            // First get the semi-axis lengths of the body.
            //
            Body target = NEW Body( "MARS" );

            double[] radii = target.getValues( "RADII" );

            double a = radii[0];
            double b = radii[1];
            double c = radii[2];

            //
            // Get the geometric state of the spacecraft with
            // respect to the target in the body-fixed reference frame
            // at `et' and compute the state of the sub-spacecraft point.
            //
            AberrationCorrection abcorr = NEW AberrationCorrection( "NONE" );
            Body                 obsrvr = NEW Body( "MRO" );
            ReferenceFrame       fixref = NEW ReferenceFrame( "IAU_MARS" );

            StateVector sv = NEW StateVector( obsrvr, et,    fixref,
                                              abcorr, target        );

            Ellipsoid ell = NEW Ellipsoid( u, b, c );

            EllipsoidPointNearPointState npState =
               NEW EllipsoidPointNearPointState( ell, sv );

            if ( npState.wasFound() )
            {
               //
               // The change in frequency is given by multiplying `shift'
               // times the carrier frequency.
               //
               double[] altState = npState.getAltitudeState();

               double shift = ( altState[1] / PhysicalConstants.CLIGHT );

               System.out.printf( "Central frequency (MHz): %19.16f%n",
                                  RCFRQ );
               System.out.printf( "Doppler shift     (MHz): %19.16f%n",
                                  RCFRQ * shift );
            }
            else
            {
               System.out.printf( "DNEAR is degenerate.%n" );
            }
         }
         catch ( SpiceException exc )
         {
            exc.printStackTrace();
         }
      }
   }

}</pre>

   <p> When this program was executed on a Mac/Intel/cc/64-bit/java 1.8
   platform, the output was:
<pre>

   Central frequency (MHz): 20.0000000000000000
   Doppler shift     (MHz): -0.0000005500991159

</pre>

   </ol>

   */
   public EllipsoidPointNearPointState ( Ellipsoid   ellipsoid,
                                         Vector6     state     )
      throws SpiceException

   {
      super();

      double[]  radii      = ellipsoid.getRadii();
      double[]  v          = new double[6];
      boolean[] foundArray = new boolean[1];

      CSPICE.dnearp( state.toArray(),
                     radii[0],
                     radii[1],
                     radii[2],
                     v,
                     altitudeState,
                     foundArray       );

      found = foundArray[0];

      if ( found )
      {
         nearPointState = new Vector6( v );
      }

   }



   //
   // Methods
   //

   /**
   Indicate whether a result was found.
   */
   public boolean wasFound()
   {
      return found;
   }


   /**
   Fetch the state of near point on the Ellipsoid. The first three components
   of the state are the near point's position; the last three components are
   the near point's velocity.

   <p>Position and time unit of the state are those of the state of the
   view point.

   <p>The result is available only if wasFound returns true.
   */
   public Vector6 getState()

      throws PointNotFoundException, SpiceException
   {
      if ( !found )
      {
         throwNotFoundExc();
      }

      return nearPointState;
   }


   /**
   Fetch the altitude and derivative with respect to time of the altitude
   of the view point relative to the Ellipsoid.

   <p>Position and time unit of the altitude's state are those of the
   state of the view point.

   <p>The result is available only if wasFound returns true.
   */
   public double[] getAltitudeState()

      throws PointNotFoundException, SpiceException
   {
      if ( !found )
      {
         throwNotFoundExc();
      }

      double[] retArray = new double[2];
      System.arraycopy( altitudeState, 0, retArray, 0, 2 );

      return ( retArray );
   }


   //
   // Package private methods
   //
   void throwNotFoundExc()

      throws PointNotFoundException, SpiceException

   {
      String msg = "Velocity of near point is undefined.";

      PointNotFoundException exc

          = PointNotFoundException.create( this.getClass().toString(), (msg) );

      throw( exc );
   }
}
