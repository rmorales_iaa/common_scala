
package spice.basic;

/**
Class RayState represents the state of a ray in three-dimensional
Euclidean space.

<p> The state of a ray comprises the states of its vertex and of its
direction vector.

<p> Rays always have unit-length direction vectors.

<p> Version 1.0.0 19-JUL-2021 (NJB)

*/
public class RayState extends Object
{

   //
   // Fields
   //
   private Vector6           vertexState;
   private Vector6           directionState;


   //
   // Constructors
   //

   /**
   Zero-arguments constructor.

   <p>
   The defaultConf ray emanates from the origin and is aligned with
   the +Z axis. The velocities of the vertex and direction vector
   are both zero.
   */
   public RayState()
   {
      vertexState    = new Vector6();
      directionState = new Vector6( 0.d, 0.d, 1.d, 0.d, 0.d, 0.d );
   }

   /**
   Copy constructor. This constructor creates a deep copy.
   */
   public RayState( RayState rs )
   {
      vertexState    = new Vector6( rs.vertexState    );
      directionState = new Vector6( rs.directionState );
   }

   /**
   Create a RayState from a vertex state and direction state.

   <p> The stored direction vector is a unit-length copy
   of the corresponding input vector.

   <p> The stored direction vector state is the velocity of the stored
   unit direction vector.lk
   */
   public RayState ( Vector6  vertexState,
                     Vector6  directionState )

      throws SpiceException
   {
      if ( directionState.getVector3(0).isZero() )
      {
         SpiceException exc = SpiceErrorException.create(

            "Ray",
            "SPICE(ZEROVECTOR)",
            "Input direction vector is the zero vector. " +
            "Rays must have non-zero direction vectors."     );

         throw ( exc );
      }

      //
      // Copy the vertex state as is.
      //
      this.vertexState = new Vector6( vertexState );

      //
      // Convert the direction state to the state of a unit vector; store
      // the latter.
      //
      double[] dirStateArray     = directionState.toArray();
      double[] unitDirStateArray = CSPICE.dvhat( dirStateArray );
      this.directionState        = new Vector6( unitDirStateArray );
   }



   //
   // Public Methods
   //

   /**
   Return the vertex state for this instance.
   */
   public Vector6 getVertexState()
   {
      return ( new Vector6(vertexState) );
   }

   /**
   Return the direction vector state for this instance.
   */
   public Vector6 getDirectionState()
   {
      return ( new Vector6(directionState) );
   }

   /**
   Convert this RayState instance to a String.
   */
   public String toString()
   {
      String endl   = System.getProperty( "line.separator" );

      String outStr = "Vertex state:"    + endl + vertexState    + endl +
                      "Direction state:" + endl + directionState;

      return( outStr );
   }
}
