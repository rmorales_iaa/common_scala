
package spice.basic;

import java.util.Arrays;

/**
Class AZELTargetState computes the state of a target as seen from an
observer at a fixed position in a specified body-fixed,
body-centered topocentric reference frame. The target position and
velocity are expressed in the AZ/EL coordinate system associated
with a topocentric frame centered at the observer.

<h2> Files </h2>
<pre>
   Appropriate kernels must be loaded by the calling program before
   methods of this class are called.

   The following dataByteSeq are required:

   -  SPK dataByteSeq: ephemeris dataByteSeq for the observer center and target
      must be loaded. If aberration corrections are used, the
      states of the observer center and target relative to the
      solar system barycenter must be calculable from the
      available ephemeris dataByteSeq. Typically ephemeris dataByteSeq are made
      available by loading one or more SPK files using
      {@link KernelDatabase#load }.

   -  Shape and orientation dataByteSeq: if the computation method is
      specified as "Ellipsoid," triaxial radii for the center body
      must be loaded into the kernel pool. Typically this is done by
      loading a text PCK file via KernelDatabase.load. Additionally,
      rotation dataByteSeq for the body-fixed, body-centered frame associated
      with the observer's center of motion must be loaded. These may be
      provided in a text or binary PCK file. In some cases these
      dataByteSeq may be provided by a CK file.

   The following dataByteSeq may be required:

   -  Frame dataByteSeq: if a frame definition not built into SPICE is
      required, for example to convert the observer-target state
      to the body-fixed body-centered frame, that definition
      must be available in the kernel pool. Typically frame
      definitions are supplied by loading a frame kernel using
      KernelDatabase.load.

   -  Additional kernels: if a CK frame is used in this constructor's
      state computation, then at least one CK and corresponding SCLK
      kernel is required. If dynamic frames are used, additional
      SPK, PCK, CK, or SCLK kernels may be required.

   In all cases, kernel dataByteSeq are normally loaded once per program
   run, NOT every time this constructor is called.
</pre>

<h2> Particulars </h2>
<pre>
   The AZELTargetState instance created by the principal
   constructor of this class contains the azimuth/elevation
   coordinates of a target as seen from an observer whose
   trajectory is not provided by SPK files.

   Observers supported by the principal constructor must have constant
   position with respect to a specified center of motion, expressed in u
   caller-specified reference frame. The state of the center of
   motion relative to the target must be computable using
   loaded SPK dataByteSeq.

   The principal constructor is suitable for computing the
   azimuth/elevation coordinates, and their derivatives, of
   target ephemeris objects, as seen from landmarks on the surface
   of an extended object, in cases where no SPK dataByteSeq are available
   for those landmarks.

   The azimuth/elevation coordinates are defined with respect to
   the observer's local topocentric reference frame. This frame is
   generally defined as follows:

   -  the +Z axis is aligned with the surface normal outward
      vector at the observer's location;

   -  the +X axis is aligned with the component of the +Z axis
      of the body-fixed reference frame orthogonal to the
      outward normal vector, i.e. the +X axis points towards
      the body's North pole;

   -  the +Y axis completes the right-handed system.

   For observers located on the +Z axis of the body-fixed frame
   designated by `obsref', the following definition of the local
   topocentric reference frame is used by this constructor:

   -  the +Z axis is aligned with the surface normal outward
      vector at the observer's location;

   -  the +X axis aligned with the +X axis of the body-fixed
      reference frame;

   -  the +Y axis completes the right-handed system.

   In both cases, the origin of the local topocentric frame is
   the observer's location.
</pre>

<h2> Version and Date </h2>

<h3> Version 1.0.0 10-SEP-2021 (NJB)</h3>
*/
public class AZELTargetState extends Vector6
{

   //
   // Instance variables
   //
   TDBDuration  lt;
   boolean      azccw;
   boolean      elplsz;


   //
   // Constructors
   //

   /**
   Compute the state of a target as seen from a stationary observer in
   a specified reference frame. The target state is expressed in the
   AZ/EL coordinate system associated with a topocentric frame
   centered at the observer.

   <p> Units are radians, km, radians/second, and km/second.

   <h2> Inputs </h2>
   <pre>
   `method` is a short string providing parameters defining the
   computation method to be used to obtain the surface
   normal vector that defines the local zenith. The only value
   currently supported is ELLIPSOID.

   `target` is the body whose azimuth and elevation are sought.

   `t` is the observation time.

   `abcorr` specifies the aberration corrections to be applied to the
   observer-target position vector.

   `azccw` is a boolean flag that is true when azimuth is positive in
   the counterclockwise direction.

   `elplsz` is a boolean flag that is true when elevation is positive in
   the +Z direction.

   `obspos` is the position of the observer relative to its center of
   motion. Units are km.

   `obsctr` is the center of motion of the observer.

   `obsref` is a body-fixed, body-centered reference frame
   in which `obspos` is expressed. This frame must be centered at `obsctr`.
   </pre>


   <h2> Outputs </h2>

   The dataByteSeq members of the AZELTargetState instance created by
   this constructor are described below.

   <pre>{@code
   Below, we use abbreviations for the names of the dataByteSeq members of
   an AZELTargetState instance. The correspondence of these abbreviations
   to the dataByteSeq member names, and the methods that return these dataByteSeq
   members, are as follows:

      Data member            Abbreviation   Access method
      --------------------   ------------   -------------
      <AZ/EL target state>   azlsta         <instance>, also getAZELState()
      azccw                  azccw          getAzimuthCounterclockwise()
      elplsz                 elplsz         getElevationPlusZ()
      lt                     lt             getLightTime()
   }</pre>
   <pre>
   Class AZELTargetState is derived from class {@link Vector6},
   and the AZ/EL target state itself is stored in the dataByteSeq member
   of the base class. For consistency, the method getAZELState() is
   provided, but it is never necessary to call this method.

   Descriptions of the dataByteSeq members follow.

      azlsta      is a state vector representing the position and
                  velocity of the target relative to the specified
                  observer, corrected for the specified aberrations
                  and expressed in azimuth/elevation coordinates. The
                  first three components of `azlsta' represent the fluxPerSecondRange,
                  azimuth and elevation of the target's position; the
                  last three components form the corresponding velocity
                  vector:

                     azlsta = ( r, az, el, dr/dt, daz/dt, del/dt )

                  The position component of `azlsta' points from the
                  observer's location at `t' to the aberration-corrected
                  location of the target. Note that the sense of the
                  position vector is independent of the direction of
                  radiation travel implied by the aberration correction.

                  The velocity component of `azlsta' is the derivative with
                  respect to time of the position component of `azlsta'.

                  Azimuth, elevation and its derivatives are measured with
                  respect to the axes of the local topocentric reference
                  frame. See the -Particulars sectionName for the definition
                  of this reference frame.

                  The azimuth is the angle between the projection onto the
                  local topocentric principal (X-Y) plane of the vector
                  from the observer's position to the target and the
                  +X axis of the reference frame. The azimuth is zero on
                  the +X axis.

                  The elevation is the angle between the vector from the
                  observer's position to the target and the local
                  topocentric principal plane. The elevation is zero on
                  the plane.

                  Units are km for `r', radians for `az' and `el', km/sec for
                  dr/dt, and radians/sec for daz/dt and del/dt. The fluxPerSecondRange
                  of `az' is [0, 2*pi] and the fluxPerSecondRange of `el' is [-pi/2, pi/2].

                  The way azimuth and elevation are measured depend
                  respectively on the value of the logical flags `azccw' and
                  `elplsz'. See the description of these input arguments for
                  details.

      lt          is the one-way light time between the observer and
                  target in seconds. If the target state is corrected
                  for aberrations, then `lt' is the one-way light time
                  between the observer and the light time corrected
                  target location.
   </pre>

   <h2> Examples </h2>

         The numerical results shown for this example may differ across
         platforms. The results depend on the SPICE kernels used as
         input, the compiler and supporting libraries, and the machine
         specific arithmetic implementation.

         <ol>

         <li><pre>
   Find the azimuth/elevation state of Venus as seen from the
   DSS-14 station at a given epoch first using the position of
   the station given as a vector in the ITRF93 frame and then
   using the dataByteSeq provided by a loaded SPK file for the DSS-14
   station.

   Task description
   ================

   In this example, we will obtain the apparent state of Venus as
   seen from DSS-14 station in the DSS-14 topocentric reference
   frame. For this computation, we'll use the DSS-14 station's
   location given as a vector in the ITRF93 frame.

   Then we will compute same apparent state using {@link StateVector} to
   obtain a Cartesian state vector, after which we will transform
   the vector coordinates to azimuth, elevation and fluxPerSecondRange and
   their derivatives using {@link AZELCoordinates#AZELCoordinates} and
   {@link AZELCoordinates#getRecAZELJacobian}.

   In order to introduce the usage of the logical flags `azccw'
   and `elplsz', we will request the azimuth to be measured
   clockwise and the elevation positive towards the +Z
   axis of the DSS-14_TOPO reference frame.

   Results from the two computations will not agree exactly
   because of time-dependent differences in the orientation,
   relative to the ITRF93 frame, of the topocentric frame centered
   at DSS-14. This orientation varies with time due to movement of
   the station, which is affected by tectonic plate motion. The
   computation using {@link AZELTargetState#AZELTargetState} evaluates
   the orientation of this frame using the station location at the
   observation epoch, while the StateVector computation uses the
   orientation provided by the station frame kernel. The latter is
   fixed and is derived from the station location at an epoch specified
   in the documentation of that kernel.
   </pre>

      <p>  Use the meta-kernel shown below to load the required SPICE
           kernels.

      <pre>

   KPL/MK

   File name: AZELTargetStateEx1.tm

   This meta-kernel is intended to support operation of SPICE
   example programs. The kernels shown here should not be
   assumed to contain adequate or correct versions of dataByteSeq
   required by SPICE-based user applications.

   In order for an application to use this meta-kernel, the
   kernels referenced here must be present in the user's
   current working directory.

   The names and contents of the kernels referenced
   by this meta-kernel are as follows:

      File name                        Contents
      ---------                        --------
      de430.bsp                        Planetary ephemeris
      naif0011.tls                     Leapseconds
      pck00010.tpc                     Planetary constants
      earth_720101_070426.bpc          Earth historical
                                          binary PCK
      earthstns_itrf93_050714.bsp      DSN station SPK
      earth_topo_050714.tf             DSN station FK

   \begindata

   KERNELS_TO_LOAD = ( 'de430.bsp',
                       'naif0011.tls',
                       'pck00010.tpc',
                       'earth_720101_070426.bpc',
                       'earthstns_itrf93_050714.bsp',
                       'earth_topo_050714.tf'         )

   \begintext

   End of meta-kernel
   </pre>

      <p> Example code begins here.
   <pre>{@code
   //
   // Program AZELTargetStateEx1
   //
   import spice.basic.*;
   import static spice.basic.AngularUnits.*;

   //
   //
   //
   class AZELTargetStateEx1
   {
      //
      // Load SPICE shared library.
      //
      static{ System.loadLibrary( "JNISpice" ); }


      public static void main( String[] args )

         throws SpiceException
      {
         //
         // Local constants
         //
         final String                      META  = "AZELTargetStateEx1.tm";

         //
         // Local variables
         //

         try
         {
            //
            // Load SPICE kernels.
            //
            KernelDatabase.load( META );

            //
            // Convert the observation time to seconds past J2000 TDB.
            //
            String  obstim = "2003 Jan 01 00:00:00 TDB";
            TDBTime et     = NEW TDBTime( obstim );

            //
            // Set the method, target, center of motion of the observer,
            // frame of observer position, and aberration corrections.
            //
            String               method = "ELLIPSOID";
            Body                 target = NEW Body( "VENUS" );
            Body                 obsctr = NEW Body( "EARTH" );
            ReferenceFrame       obsref = NEW ReferenceFrame( "ITRF93" );
            AberrationCorrection abcorr = NEW AberrationCorrection( "CN+S" );

            //
            // Set the position of DSS-14 relative to the earth's
            // center at the observation epoch, expressed in the
            // ITRF93 reference frame. Values come from the
            // earth station SPK specified in the meta-kernel.
            //
            // The actual station velocity is non-zero due
            // to tectonic plate motion; we ignore the motion
            // in this example.
            //
            Vector3 obspos = NEW Vector3( -2353.621419700,
                                          -4641.341471700,
                                           3677.052317800  );

            //
            // We want the azimuth/elevation coordinates to be
            // measured with the azimuth increasing clockwise and the
            // elevation positive towards +Z axis of the local
            // topocentric reference frame
            //
            boolean azccw  = false;
            boolean elplsz = true;

            AZELTargetState azelState = NEW AZELTargetState(
                method, target, et,     abcorr, azccw,
                elplsz, obspos, obsctr, obsref        );

            //
            // In order to check the contents of `azelState`
            // we are going to compute the same azimuth/elevation state
            // using the position of DSS-14 and its local topocentric
            // reference frame "DSS-14_TOPO" from the kernel pool.
            //
            Body           obs = NEW Body( "DSS-14" );
            ReferenceFrame ref = NEW ReferenceFrame( "DSS-14_TOPO" );

            //
            // Compute the observer-target state.
            //
            StateVector state = NEW StateVector( target, et, ref,
                                                 abcorr, obs      );

            //
            // Convert the position to azimuth/elevation coordinates.
            //
            AZELCoordinates azlCoords = NEW AZELCoordinates(
                                        state.getPosition(), azccw, elplsz );

            //
            // Convert velocity to azimuth/elevation coordinates.
            //
            Matrix33 jacobi = AZELCoordinates.getRecAZELJacobian(
                                 state.getPosition(), azccw, elplsz );

            Vector3 azlvel = jacobi.mxv( state.getVelocity() );

            //
            // Convert outputs to arrays and sclars of primitive types
            // for display.
            //
            double[] azlStateArr = azelState.toArray();
            double[] azlVelArr   = azlvel.toArray();
            double   az          = azlCoords.getAzimuth();
            double   el          = azlCoords.getElevation();
            double   r           = azlCoords.getRange();

            System.out.printf( "%n" );
            System.out.printf( "AZ/EL coordinates " +
                               "(from AZELTargetState):%n" );
            System.out.printf( "%n" );
            System.out.printf( "   Range     (km)         =  %19.8f%n",
                               azlStateArr[0] );
            System.out.printf( "   Azimuth   (deg)        =  %19.8f%n",
                                         azlStateArr[1] * DPR );
            System.out.printf( "   Elevation (deg)        =  %19.8f%n",
                                         azlStateArr[2] * DPR );
            System.out.printf( "%n" );
            System.out.printf( "AZ/EL coordinates (using kernels):%n" );
            System.out.printf( "%n" );
            System.out.printf( "   Range     (km)         =  %19.8f%n", r );
            System.out.printf( "   Azimuth   (deg)        =  %19.8f%n",
                               az * DPR );
            System.out.printf( "   Elevation (deg)        =  %19.8f%n",
                               el * DPR );
            System.out.printf( "%n" );
            System.out.printf( "AZ/EL velocity (from AZELTargetState):%n" );
            System.out.printf( "%n" );
            System.out.printf( "   d Range/dt    (km/s)   =  %19.8f%n",
                               azlStateArr[3] );
            System.out.printf( "   d Azimuth/dt  (deg/s)  =  %19.8f%n",
                               azlStateArr[4] * DPR );
            System.out.printf( "   d Elevation/dt (deg/s) =  %19.8f%n",
                               azlStateArr[5] * DPR );
            System.out.printf( "%n" );
            System.out.printf( "AZ/EL velocity (using kernels):%n" );
            System.out.printf( "%n" );
            System.out.printf( "   d Range/dt     (km/s)  =  %19.8f%n",
                               azlVelArr[0] );
            System.out.printf( "   d Azimuth/dt   (deg/s) =  %19.8f%n",
                               azlVelArr[1] * DPR );
            System.out.printf( "   d Elevation/dt (deg/s) =  %19.8f%n",
                               azlVelArr[2] * DPR );
            System.out.printf( "%n" );
         }
         catch ( SpiceException exc )
         {
            exc.printStackTrace();
         }
      }
   }
   }</pre>

      <p> When this program was executed on a Mac/Intel/cc/64-bit/Java 1.8
      platform, the output was:

   <pre>{@code
   AZ/EL coordinates (from AZELTargetState):

      Range     (km)         =    89344802.82679011
      Azimuth   (deg)        =         269.04481881
      Elevation (deg)        =         -25.63088321

   AZ/EL coordinates (using kernels):

      Range     (km)         =    89344802.82679011
      Azimuth   (deg)        =         269.04481846
      Elevation (deg)        =         -25.63088278

   AZ/EL velocity (from AZELTargetState):

      d Range/dt    (km/s)   =          13.41734176
      d Azimuth/dt  (deg/s)  =           0.00238599
      d Elevation/dt (deg/s) =          -0.00339644

   AZ/EL velocity (using kernels):

      d Range/dt     (km/s)  =          13.41734176
      d Azimuth/dt   (deg/s) =           0.00238599
      d Elevation/dt (deg/s) =          -0.00339644
   }</pre>

      </li>
      </ol>





   */
   public AZELTargetState( String                method,
                           Body                  target,
                           Time                  t,
                           AberrationCorrection  abcorr,
                           boolean               azccw,
                           boolean               elplsz,
                           Vector3               obspos,
                           Body                  obsctr,
                           ReferenceFrame        obsref  )

      throws SpiceException

   {
      super();

      this.azccw  = azccw;
      this.elplsz = elplsz;

      double[]    stateArray = new double[6];
      double[]    ltArray    = new double[1];

      CSPICE.azlcpo( method,           target.getName(), t.getTDBSeconds(),
                     abcorr.getName(), azccw,            elplsz,
                     obspos.toArray(), obsctr.getName(), obsref.getName(),
                     stateArray,       ltArray                             );

      System.arraycopy( stateArray, 0, v, 0, 6 );
      lt = new TDBDuration( ltArray[0] );
   }

   /**
   No-arguments constructor.
   */
   public AZELTargetState()
   {
      super();
      lt     = new TDBDuration();
      azccw  = false;
      elplsz = false;
   }

   /**
   Copy constructor.
   */
   public AZELTargetState( AZELTargetState state )
   {
      super();

      this.azccw  = state.azccw;
      this.elplsz = state.elplsz;

      System.arraycopy( state.v, 0, v, 0, 6 );

      lt = state.lt;
   }

   //
   // Methods
   //

   /**
   Get target state. Position coordinates are Range/AZ/EL. Velocity
   components are dRange/dt, dAZ/dt, dEL/dt, expressed in km/s and
   radians/s.
   */
   public Vector6 getAZELState()
   {
      return this;
   }

   /**
   Get target-observer light time. The time is expressed as seconds in
   the TDB time system.
   */
   public TDBDuration getLightTime()
   {
      return lt;
   }

   /**
   Get Range, AZ, EL, and angular sense parameters as an AZELCoordinates
   instance.
   */
   public AZELCoordinates getAZELCoordinates()

      throws SpiceException
   {
      return new AZELCoordinates( v[0], v[1], v[2], azccw, elplsz );
   }

   /**
   Get Range, AZ, and EL rates as a 3-D vector. Velocity
   components are dRange/dt, dAZ/dt, dEL/dt, expressed in km/s and
   radians/s.
   */
   public Vector3 getAZELVelocity()

      throws SpiceException
   {
      return new Vector3( this.getVector3(1) );
   }

   /**
   Return flag which is true when azimuth is positive in the
   counterclockwise sense.
   */
   public boolean getAzimuthCounterclockwise()
   {
      return ( azccw );
   }

   /**
   Return flag which is true when elevation is positive in the +Z sense.
   */
   public boolean getElevationPlusZ()
   {
      return ( elplsz );
   }
}
