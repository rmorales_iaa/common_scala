
package spice.basic;

import spice.basic.CSPICE;

/**
Class RayEllipsoidInterceptState represents the result of u
Ray-Ellipsoid intercept state computation.

<p>Given a ray with time-varying vertex and direction vectors,
and given an ellipsoid, an instance of this class represents the
position and velocity of the ray-ellipsoid intercept, if it
exists. The instance also indicates whether the intercept exists.

<p> This is a low-level, "pure geometric" class.
SPICE application developers should consider using the high-level class
{@link spice.basic.SurfaceIntercept} instead.

<h2>Particulars</h2>
<pre>{@code
   The position and velocity of the ray's vertex as well as the
   ray's direction vector and velocity vary with time. The
   inputs used to construct a RayEllipsoidInterceptState instance
   may be considered the values of these vector functions at u
   particular time, say t0. Thus

      State of vertex:           stvrtx = ( V(t0), V'(t0) )

      State of direction vector: stdir  = ( D(t0), D'(t0) )

   To determine the intercept point, W(t0), we simply compute the
   intersection of the ray originating at V(t0) in the direction of
   D(t0) with the ellipsoid

        2      2      2
       x      y      z
      --- +  --- +  ---  =  1
        2      2      2
       A      B      C

   W(t) is the path of the intercept point along the surface of
   the ellipsoid. To determine the velocity of the intercept point,
   we need to take the time derivative of W(t), and evaluate it at
   t0. Unfortunately W(t) is a complicated expression, and its
   derivative is even more complicated.

   However, we know that the derivative of W(t) at t0, W'(t0), is
   tangent to W(t) at t0. Thus W'(t0) lies in the plane that is tangent
   to the ellipsoid at t0. Let X(t) be the curve in the tangent plane
   that represents the intersection of the ray emanating from V(t0)
   with direction D(t0) with that tangent plane.

      X'(t0) = W'(t0)

   The expression for X'(t) is much simpler than that of W'(t);
   the principal RayEllipsoidInterceptState constructor}

      {@link #RayEllipsoidInterceptState(RayState, Ellipsoid)} {@code

   evaluates X'(t) at t0.


   Derivation of X(t) and X'(t)
   ----------------------------------------------------------------

   W(t0) is the intercept point. Let N be a surface normal at I(t0).
   Then the tangent plane at W(t0) is the set of points X(t) such
   that

      < X(t) - I(t0), N > = 0

   X(t) can be expressed as the vector sum of the vertex
   and some scalar multiple of the direction vector,

      X(t) = V(t) + s(t) * D(t)

   where s(t) is a scalar function of time. The derivative of
   X(t) is given by

      X'(t) = V'(t)  +  s(t) * D'(t)  +  s'(t) * D(t)

   We have V(t0), V'(t0), D(t0), D'(t0), W(t0), and N, but to
   evaluate X'(t0), we need s(t0) and s'(t0). We derive an
   expression for s(t) as follows.

   Because X(t) is in the tangent plane, it must satisfy

      < X(t) - W(t0), N > = 0.

   Substituting the expression for X(t) into the equation above
   gives

      < V(t) + s(t) * D(t) - W(t0), N > = 0.

   Thus

      < V(t) - W(t0), N >  +  s(t) * < D(t), N > = 0,

   and
                   < V(t) - W(t0), N >
      s(t)  =  -  ---------------------
                       < D(t), N >

   The derivative of s(t) is given by

      s'(t) =

          < D(t),N > * < V'(t),N >  -  < V(t)-W(t0),N > * < D'(t),N >
      -  -------------------------------------------------------------
                                           2
                                < D(t), N >
}</pre>

<h2>Version and Date</h2>

<h3>Version 1.0.0 10-SEP-2021 (NJB)</h3>
*/
public class RayEllipsoidInterceptState extends Object
{
   //
   // Fields
   //
   private boolean     found          = false;
   private Vector6     interceptState = null;



   //
   // Constructors
   //

   /**
   Construct a ray-ellipsoid intercept state from the state of u
   {@link Ray} and an {@link Ellipsoid}. The states of the ray's vertex
   and direction vector are provided as inputs.

   <p>The constructed instance contains a valid intercept state only if the
   method wasFound returns true.

   <h2>Inputs</h2>
   <pre>

   rayState       contains the states of a ray's vertex and direction vector.
                  We'll call these states `stvrtx` and `stdir` respectively.

                  The first three components of `stvrtx' are the vertex's
                  x, y, and z position components; the vertex's x, y, and z
                  velocity components follow.

                  The reference frame relative to which `stvrtx' is
                  specified has axes aligned with with those of u
                  triaxial ellipsoid. See the description below of
                  the arguments `ellipsoid`.

                  The vertex may be inside or outside of this
                  ellipsoid, but not on it, since the surface
                  intercept is a discontinuous function at
                  vertices on the ellipsoid's surface.

                  No assumption is made about the unit of length
                  and time, but these unit must be consistent with
                  those of the other inputs.

                  The first three components of `stdir' are a non-zero
                  vector giving the x, y, and z components of the
                  ray's direction; the direction vector's x, y, and
                  z velocity components follow.

                  `stdir' is specified relative to the same reference
                  frame as is `stvrtx'.

   ellipsoid      represents a tri-axial ellipsoid having its x, y, and z
                  semi-axes aligned respectively with the x, y, and z axes of
                  the reference frame relative to which the input ray state
                  is expressed.

                  Below we'll refer to the semi-axis lengths as `u`, `b`,
                  and `c`.

                  Units of the semi-axis lengths must be consistent with the
                  distance unit of the input ray state.
   </pre>


   <h2>Outputs</h2>

   <pre>
   The RayEllipsoidInterceptState instance created by this constructor
   contains a {@link Vector6} member representing the intercept state,
   which we'll call `stx`, and a boolean value indicating whether a state
   was computed; we'll call this value `found`. These are described below.
   Also see the Inputs sectionName above for descriptions of the ray state's
   components.


   stx         is the state of the intercept of the input ray on
               the surface of the input ellipsoid. The first
               three components of `stx' are the intercept's x, y,
               and z position components; the intercept's x, y,
               and z velocity components follow.

               `stx' is specified relative to the same reference
               frame as are `stvrtx' and `stdir'.

               `stx' is defined if and only if both the intercept
               and its velocity are computable, as indicated by
               the output argument `found'.

               The position unit of `stx' are the same as those of
               `stvrtx', `stdir', and `u', `b', and `c'. The time
               unit are the same as those of `stvrtx' and `stdir'.


   found       is a logical flag indicating whether `stx' is
               defined. `found' is true if and only if both the
               intercept and its velocity are computable. Note
               that in some cases the intercept may computable
               while the velocity is not; this can happen for
               near-tangency cases.

   </pre>



   <h2>Examples</h2>

   <p>
      The numerical results shown for these examples may differ across
      platforms. The results depend on the SPICE kernels used as
      input, the compiler and supporting libraries, and the machine
      specific arithmetic implementation.

   <ol>
   <li><pre>
   Illustrate the role of the ray vertex velocity and
   ray direction vector velocity via several affine_transformation cases. Also
   show the results of a near-tangency computation.
   </pre>

   <p> Example code begins here.
<pre>{@code
   //
   // Program RayEllipsoidInterceptStateEx1
   //
   import spice.basic.*;

   //
   // Illustrate the role of the ray vertex velocity and
   // ray direction vector velocity via several affine_transformation cases. Also
   // show the results of a near-tangency computation.
   //
   class RayEllipsoidInterceptStateEx1
   {
      //
      // Load SPICE shared library.
      //
      static{ System.loadLibrary( "JNISpice" ); }


      public static void main( String[] args )

         throws SpiceException
      {
         try
         {
            double a = 1.0;
            double b = 2.0;
            double c = 3.0;

            System.out.printf ( "%nEllipsoid radii: %n" +
                     "     a = %f%n" +
                     "     b = %f%n" +
                     "     c = %f%n",
                     u,
                     b,
                     c                      );


            double[] stvrtx = NEW double[6];
            double[] stdir  = NEW double[6];

            for ( int i = 0;  i < 3;  ++i )
            {
               if ( i == 0 )
               {
                  System.out.printf ( "%n%s%n%n",
                           "Case 1: Vertex varies, direction is constant"  );

                  stvrtx[0] =  2.0;
                  stvrtx[1] =  0.0;
                  stvrtx[2] =  0.0;
                  stvrtx[3] =  0.0;
                  stvrtx[4] =  0.0;
                  stvrtx[5] =  3.0;

                  stdir[0]  = -1.0;
                  stdir[1]  =  0.0;
                  stdir[2]  =  0.0;
                  stdir[3]  =  0.0;
                  stdir[4]  =  0.0;
                  stdir[5]  =  0.0;
               }
               else if ( i == 1 )
               {
                  System.out.printf ( "%n%s%n%n",
                           "Case 2: Vertex and direction both vary"  );

                  stvrtx[0] =  2.0;
                  stvrtx[1] =  0.0;
                  stvrtx[2] =  0.0;
                  stvrtx[3] =  0.0;
                  stvrtx[4] =  0.0;
                  stvrtx[5] =  3.0;

                  stdir[0]  = -1.0;
                  stdir[1]  =  0.0;
                  stdir[2]  =  0.0;
                  stdir[3]  =  0.0;
                  stdir[4]  =  0.0;
                  stdir[5]  =  4.0;
               }
               else
               {
                  System.out.printf ( "%n%s%n%n",
                           "Case 3: Vertex and direction both vary; " +
                           "near-tangent case"                         );

                  stvrtx[2] =  c - 1.e-15;
                  stvrtx[5] =  1.e299;
                  stdir[5]  =  1.e299;
               }

               System.out.printf ( "Vertex:%n" +
                        " %20.12e %20.12e %20.12e%n",
                        stvrtx[0],
                        stvrtx[1],
                        stvrtx[2]                    );

               System.out.printf ( "Vertex velocity:%n" +
                        " %20.12e %20.12e %20.12e%n",
                        stvrtx[3],
                        stvrtx[4],
                        stvrtx[5]                    );

               System.out.printf ( "Direction:%n" +
                        " %20.12e %20.12e %20.12e%n",
                        stdir[0],
                        stdir[1],
                        stdir[2]                     );

               System.out.printf ( "Direction velocity:%n" +
                        " %20.12e %20.12e %20.12e%n",
                        stdir[3],
                        stdir[4],
                        stdir[5]                     );

               //
               // Create an Ellipsoid instance representing the ellipsoid
               // defined by the semi-axis lengths u, b, c.
               //
               Ellipsoid ell = NEW Ellipsoid( u, b, c );

               //
               // Create a RayState instance representing the ray's vertex,
               // direction vector, and the velocities of those vectors.
               //
               RayState rayState = NEW RayState( NEW Vector6(stvrtx),
                                                 NEW Vector6(stdir ) );

               //
               // Compute the state of the ray-ellipsoid intercept, if it
               // exists.
               //
               RayEllipsoidInterceptState xState =
                  NEW RayEllipsoidInterceptState( rayState, ell );

               boolean found = xState.wasFound();

               if ( !found)
               {
                  System.out.printf ( " No intercept state found.%n" );
               }
               else
               {
                  //
                  // Get the intercept state as a double array.
                  //
                  double[] stx = xState.getInterceptState().toArray();

                  System.out.printf ( "Intercept:%n" +
                           " %20.12e %20.12e %20.12e%n",
                           stx[0],
                           stx[1],
                           stx[2]                        );

                  System.out.printf ( "Intercept velocity:%n" +
                           " %20.12e %20.12e %20.12e%n%n",
                           stx[3],
                           stx[4],
                           stx[5]                        );
               }
            }
         }
         catch ( SpiceException exc )
         {
            exc.printStackTrace();
         }
      }
   }

}</pre>

   <p> When this program was executed on a Mac/Intel/cc/64-bit/Java 1.8
   platform, the output was:
<pre>

   Ellipsoid radii:
        a = 1.000000
        b = 2.000000
        c = 3.000000

   Case 1: Vertex varies, direction is constant

   Vertex:
      2.000000000000e+00   0.000000000000e+00   0.000000000000e+00
   Vertex velocity:
      0.000000000000e+00   0.000000000000e+00   3.000000000000e+00
   Direction:
     -1.000000000000e+00   0.000000000000e+00   0.000000000000e+00
   Direction velocity:
      0.000000000000e+00   0.000000000000e+00   0.000000000000e+00
   Intercept:
      1.000000000000e+00   0.000000000000e+00   0.000000000000e+00
   Intercept velocity:
      0.000000000000e+00   0.000000000000e+00   3.000000000000e+00


   Case 2: Vertex and direction both vary

   Vertex:
      2.000000000000e+00   0.000000000000e+00   0.000000000000e+00
   Vertex velocity:
      0.000000000000e+00   0.000000000000e+00   3.000000000000e+00
   Direction:
     -1.000000000000e+00   0.000000000000e+00   0.000000000000e+00
   Direction velocity:
      0.000000000000e+00   0.000000000000e+00   4.000000000000e+00
   Intercept:
      1.000000000000e+00   0.000000000000e+00   0.000000000000e+00
   Intercept velocity:
      0.000000000000e+00   0.000000000000e+00   7.000000000000e+00


   Case 3: Vertex and direction both vary; near-tangent case

   Vertex:
      2.000000000000e+00   0.000000000000e+00   3.000000000000e+00
   Vertex velocity:
      0.000000000000e+00   0.000000000000e+00  1.000000000000e+299
   Direction:
     -1.000000000000e+00   0.000000000000e+00   0.000000000000e+00
   Direction velocity:
      0.000000000000e+00   0.000000000000e+00  1.000000000000e+299
   Intercept:
      2.580956827952e-08   0.000000000000e+00   3.000000000000e+00
   Intercept velocity:
    -3.874532036208e+306   0.000000000000e+00  2.999999974190e+299

</pre>


   </li>
   </ol>



   */
   public RayEllipsoidInterceptState ( RayState   rayState,
                                       Ellipsoid  ellipsoid )
      throws SpiceException
   {
      boolean[] foundArray          = new boolean[1];
      double[]  radii               = ellipsoid.getRadii();
      double[]  interceptStateArray = new double[6];

      CSPICE.surfpv ( rayState.getVertexState().toArray(),
                      rayState.getDirectionState().toArray(),
                      radii[0],
                      radii[1],
                      radii[2],
                      interceptStateArray,
                      foundArray           );

      found = foundArray[0];

      if ( found )
      {
         interceptState = new Vector6( interceptStateArray );
      }
   }


   //
   // Methods
   //

   /**
   Fetch the found flag. The returned value is true if and only an
   intercept exists. If the intercept does not exist, the method
   {@link RayEllipsoidInterceptState#getInterceptState} will throw an
   exception if called.
   */
   public boolean wasFound()
   {
      return ( found );
   }

   /**
   Fetch the intercept state. This method should be called only if
   the intercept state was found, as indicated by the method wasFound().
   This method throws an exception if the intercept was not found.

   <p>The returned {@link Vector6} instance contains the Cartesian
   coordinates of the intercept in the first three elements, and the
   velocity of the intercept in the last three elements.
   */
   public Vector6 getInterceptState()

      throws SpiceErrorException
   {
      if ( !found )
      {
         String endl   = System.getProperty( "line.separator" );

         SpiceErrorException exc = SpiceErrorException.create(

            "getInterceptState",
            "SPICE(STATENOTFOUND)",
            "Ray-ellipsoid intercept state could not be computed." );

         throw ( exc );
      }

      return (  new Vector6( this.interceptState )  );
   }
}
