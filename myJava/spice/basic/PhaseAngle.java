
package spice.basic;

import java.util.Arrays;

/**
Class PhaseAngle computes the apparent phase angle for a target, observer,
illuminator set of ephemeris objects.


   <h2> Files </h2>

   <pre>
   Appropriate kernels must be loaded by the calling program before
   methods of this class are called.

   The following dataByteSeq are required:

   -  SPK dataByteSeq: ephemeris dataByteSeq for the observer, illuminator, and
      target must be loaded. If aberration corrections are used,
      the states of the ephemeris bodies relative to the solar
      system barycenter must be calculable from the available
      ephemeris dataByteSeq. Typically ephemeris dataByteSeq are made
      available by loading one or more SPK files using FURNSH.

   The following dataByteSeq may be required:

   -  Frame dataByteSeq: if a frame definition not built into SPICE is
      required, that definition must be available in the kernel
      pool. Typically frame definitions are supplied by loading u
      frame kernel using FURNSH.

   -  Orientation dataByteSeq: if a CK based frame is used in this routine's
      state computation, then at least one CK and corresponding SCLK
      kernel is required. If dynamic frames are used, additional
      SPK, PCK, CK, or SCLK kernels may be required.

   In all cases, kernel dataByteSeq are normally loaded once per program
   run, NOT every time this routine is called.
   </pre>

<h2> Particulars </h2>

<pre>{@code
   The phase angle is defined using the location of the bodies (if
   point objects) or the centers of the bodies (if finite bodies).

                     illmn     obsrvr
     illmn as seen      ^       /
     from target at     |      /
     et - lt.           |     /
                       >|..../< phase angle
                        |   /
                      . |  /
                    .   | /
                   .    |v        target as seen from obsrvr
             sep   .  target      at et
                    .  /
                      /
                     v

      pi    = sep + phase
      phase = pi - sep

}</pre>

<h2> Version and Date </h2>

<h3> Version 1.0.0 10-SEP-2021 (NJB) (EDW)</h3>
*/

public class PhaseAngle extends Object
{
   //
   // Static methods
   //

   /**
   Compute the angle between an observer and another ephemeris object,
   which typically acts as an illumination source, as measured from u
   target ephemeris object.

   <h2> Inputs   </h2>

   <pre>
   `t` is the observation time.

   `target` is the central body from which the phase angle is measured.

   `abcorr` specifies the aberration corrections to be applied to the
   observer-target position vector and the position of the illumination
   source as seen from the target center. Aberration corrections are
   restricted to reception cases.

   `illumSource` is an ephemeris object which typically acts as an
   illumination source, but which may be any ephemeris object distinct from
   the target and observer. The position of the illumination source is one
   vertex defining the observer-target-illumination source angle.

   `observer` is the observer; the input time applies to the observer time
   at the observer. The position of the observer at `t` is one vertex
   defining the observer-target-illumination source angle.
   </pre>

   <h2> Outputs </h2>

   The function returns the optionally light-time corrected phase
   angle between `target' and `illmn' as observed from `obsrvr'.

   <p>Units are radians. The fluxPerSecondRange of the phase angle is [0, pi].

   <h2> Examples </h2>

      The numerical results shown for this example may differ across
      platforms. The results depend on the SPICE kernels used as
      input, the compiler and supporting libraries, and the machine
      specific arithmetic implementation.

      <ol>

      <li><p>
   Determine the time intervals from December 1, 2006 UTC to
   January 31, 2007 UTC for which the sun-moon-earth configuration
   phase angle satisfies the relation conditions with respect to u
   reference value of .57598845 radians (the phase angle at
   January 1, 2007 00:00:00.000 UTC, 33.001707 degrees). Also
   determine the time intervals corresponding to the local maximum and
   minimum phase angles, and the absolute maximum and minimum phase
   angles during the search interval. The configuration defines the
   sun as the illuminator, the moon as the target, and the earth as
   the observer.

   <p>  Use the meta-kernel shown below to load the required SPICE
        kernels.

   <pre>
   KPL/MK

   File name: PhaseAngleEx1.tm

   This meta-kernel is intended to support operation of SPICE
   example programs. The kernels shown here should not be
   assumed to contain adequate or correct versions of dataByteSeq
   required by SPICE-based user applications.

   In order for an application to use this meta-kernel, the
   kernels referenced here must be present in the user's
   current working directory.

   The names and contents of the kernels referenced
   by this meta-kernel are as follows:

      File name                     Contents
      ---------                     --------
      de421.bsp                     Planetary ephemeris
      pck00009.tpc                  Planet orientation and
                                    radii
      naif0009.tls                  Leapseconds

   \begindata

      KERNELS_TO_LOAD = ( 'de421.bsp',
                          'pck00009.tpc',
                          'naif0009.tls'  )

   \begintext

   End of meta-kernel
   </pre>

   <p> Example code begins here.

   <pre>{@code
   //
   // Program PhaseAngleEx1
   //
   import spice.basic.*;
   import static spice.basic.TimeConstants.*;

   class PhaseAngleEx1
   {
      //
      // Load SPICE shared library.
      //
      static{ System.loadLibrary( "JNISpice" ); }


      public static void main( String[] args )

         throws SpiceException
      {
         //
         // Local constants
         //
         final int                         MAXWIN = 5000;

         final String                      META   = "PhaseAngleEx1.tm";
         final String                      TIMFMT = "YYYY MON DD HR:MN:SC.###";

         //
         // Local variables
         //
         String[]                          relate = { "=",
                                                      "<",
                                                      ">",
                                                      "LOCMIN",
                                                      "ABSMIN",
                                                      "LOCMAX",
                                                      "ABSMAX" };
         try
         {
            //
            // Load kernels.
            //
            KernelDatabase.load( META );

            //
            // Set initial values of phase angle computation inputs.
            //
            AberrationCorrection abcorr = NEW AberrationCorrection( "LT+S" );

            Body target = NEW Body( "moon"  );
            Body illmn  = NEW Body( "sun"   );
            Body obsrvr = NEW Body( "earth" );

            //
            // Create time windows.
            //
            SpiceWindow cnfine = NEW SpiceWindow();
            SpiceWindow result = null;

            //
            // Store the time bounds of our search interval in
            // the confinement window.
            //
            double et0 = NEW TDBTime( "2006 DEC 01" ).getTDBSeconds();
            double et1 = NEW TDBTime( "2007 JAN 31" ).getTDBSeconds();

            cnfine.insert( et0, et1 );

            //
            // Define the search.
            //
            GFPhaseAngleSearch search =
               NEW GFPhaseAngleSearch( target, illmn, abcorr, obsrvr );

            //
            // Search using a step size of 1 day (in unit of seconds).
            // The reference value is 0.57598845 radians. We're not using
            // the adjustment feature, so we set `adjust` to zero.
            //
            double step   = SPD;
            double refval = 0.57598845;
            double adjust = 0.0;

            int ncase = relate.length;

            for ( int j = 0;  j < ncase;  ++j )
            {
               System.out.printf ( "Relation condition: %s%n",  relate[j] );

               //
               // Set the search constraint.
               //
               GFConstraint constraint = null;

               if (    ( relate[j].indexOf( "MAX" )  !=  -1 )
                    || ( relate[j].indexOf( "MIN" )  !=  -1 )  )
               {
                  //
                  // This is an extremum constraint.
                  //
                  constraint =
                     GFConstraint.createExtremumConstraint( relate[j] );
               }
               else
               {
                  //
                  // This is an relational constraint.
                  //
                  constraint = GFConstraint.createReferenceConstraint(
                                  relate[j], refval );
               }

               //
               // Run the search over the confinement window,
               // using the selected constraint and step size.
               // Indicate the maximum number of workspace
               // intervals to use.
               //
               result = search.run( cnfine, constraint, step, MAXWIN );

               //
               // Display the results.
               //
               if ( result.card() == 0 )
               {
                  System.out.println ( "Result window is empty." );
               }
               else
               {
                  for ( int i = 0;  i < result.card();  ++i )
                  {
                     //
                     // Fetch the endpoints of the Ith interval
                     // of the result window.
                     //
                     double[] intrvl = result.getInterval(i);

                     TDBTime  start  = NEW TDBTime( intrvl[0] );
                     TDBTime  stop   = NEW TDBTime( intrvl[1] );

                     double   phase  = PhaseAngle.getAngle(
                        start, target, illmn, obsrvr, abcorr );

                     String begstr = start.toString( TIMFMT );

                     System.out.printf ( "Start time = %s %16.9f%n",
                                         begstr, phase );

                     phase  = PhaseAngle.getAngle(
                        stop, target, illmn, obsrvr, abcorr );

                     String endstr = stop.toString( TIMFMT );

                     System.out.printf ( "Stop time  = %s %16.9f%n",
                                         endstr, phase );
                  }
                  System.out.printf( "%n" );
               }
            }
         }
         catch ( SpiceException exc )
         {
            exc.printStackTrace();
         }
      }
   }
   }</pre>

   <p> When this program was executed on a Mac/Intel/cc/64-bit/Java 1.8
   platform, the output was:

   <pre>{@code
   Relation condition: =
   Start time = 2006 DEC 02 13:31:34.414      0.575988450
   Stop time  = 2006 DEC 02 13:31:34.414      0.575988450
   Start time = 2006 DEC 07 14:07:55.470      0.575988450
   Stop time  = 2006 DEC 07 14:07:55.470      0.575988450
   Start time = 2006 DEC 31 23:59:59.997      0.575988450
   Stop time  = 2006 DEC 31 23:59:59.997      0.575988450
   Start time = 2007 JAN 06 08:16:25.512      0.575988450
   Stop time  = 2007 JAN 06 08:16:25.512      0.575988450
   Start time = 2007 JAN 30 11:41:32.557      0.575988450
   Stop time  = 2007 JAN 30 11:41:32.557      0.575988450

   Relation condition: <
   Start time = 2006 DEC 02 13:31:34.414      0.575988450
   Stop time  = 2006 DEC 07 14:07:55.470      0.575988450
   Start time = 2006 DEC 31 23:59:59.997      0.575988450
   Stop time  = 2007 JAN 06 08:16:25.512      0.575988450
   Start time = 2007 JAN 30 11:41:32.557      0.575988450
   Stop time  = 2007 JAN 31 00:00:00.000      0.468279091

   Relation condition: >
   Start time = 2006 DEC 01 00:00:00.000      0.940714974
   Stop time  = 2006 DEC 02 13:31:34.414      0.575988450
   Start time = 2006 DEC 07 14:07:55.470      0.575988450
   Stop time  = 2006 DEC 31 23:59:59.997      0.575988450
   Start time = 2007 JAN 06 08:16:25.512      0.575988450
   Stop time  = 2007 JAN 30 11:41:32.557      0.575988450

   Relation condition: LOCMIN
   Start time = 2006 DEC 05 00:16:50.317      0.086121423
   Stop time  = 2006 DEC 05 00:16:50.317      0.086121423
   Start time = 2007 JAN 03 14:18:31.977      0.079899769
   Stop time  = 2007 JAN 03 14:18:31.977      0.079899769

   Relation condition: ABSMIN
   Start time = 2007 JAN 03 14:18:31.977      0.079899769
   Stop time  = 2007 JAN 03 14:18:31.977      0.079899769

   Relation condition: LOCMAX
   Start time = 2006 DEC 20 14:09:10.392      3.055062862
   Stop time  = 2006 DEC 20 14:09:10.392      3.055062862
   Start time = 2007 JAN 19 04:27:54.600      3.074603891
   Stop time  = 2007 JAN 19 04:27:54.600      3.074603891

   Relation condition: ABSMAX
   Start time = 2007 JAN 19 04:27:54.600      3.074603891
   Stop time  = 2007 JAN 19 04:27:54.600      3.074603891
   }</pre>

   </li>
   </ol>

   */
   public static double getAngle( Time                  t,
                                  Body                  target,
                                  Body                  illumSource,
                                  Body                  observer,
                                  AberrationCorrection  abcorr      )

      throws SpiceException
   {
      return CSPICE.phaseq ( t.getTDBSeconds(),
                             target.getName(),
                             illumSource.getName(),
                             observer.getName(),
                             abcorr.getName()      );
   }
}
