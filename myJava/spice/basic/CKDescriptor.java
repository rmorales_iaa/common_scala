
package spice.basic;


/**
Class CKDescriptor represents CK segment descriptors
and declares CK segment descriptor constants.

<h3> Version 1.0.0 20-AUG-2021 (NJB) </h3>

*/
public class CKDescriptor
{

   //
   // Public fields
   //

   //
   //    Segment descriptor parameters
   //
   //
   //       The CK segment descriptor's double precision component layout is:
   //
   //          +--------------------------+
   //          | Segment start time, TDB  |
   //          +--------------------------+
   //          | Segment stop time, TDB   |
   //          +--------------------------+
   //
   //
   //       The CK segment descriptor's integer component layout is:
   //
   //          +--------------------------+
   //          | CK frame class ID        |
   //          +--------------------------+
   //          | Base frame ID            |
   //          +--------------------------+
   //          | Data type                |
   //          +--------------------------+
   //          | Angular velocity flag    |
   //          +--------------------------+
   //          | Segment begin  address   |
   //          +--------------------------+
   //          | Segment end address      |
   //          +--------------------------+

   //
   //    Parameters defining offsets for segment descriptor elements
   //    follow. The offsets are 0-based.
   //
   //

   /**
   Index in double precision component of start time:
   */
   public static final int                  STRIDX = 0;

   /**
   Index in double precision component of stop time:
   */
   public static final int                  STPIDX = STRIDX + 1;

   /**
   Index in integer component of CK frame class ID:
   */
   public static final int                  CKFIDX = 0;

   /**
   Index in integer component of frame ID of base frame:
   */
   public static final int                  BASIDX = CKFIDX + 1;

   /**
   Index in integer component of dataByteSeq type code:
   */
   public static final int                  TYPIDX = BASIDX + 1;

   /**
   Index in integer component of angular velocity flag:
   */
   public static final int                  AVFIDX = TYPIDX + 1;

   /**
   Index in integer component of DAF begin address:
   */
   public static final int                  BADIDX = AVFIDX + 1;

   /**
   Index in integer component of DAF end address:
   */
   public static final int                  EADIDX = BADIDX + 1;

   //
   // Protected fields
   //

   //
   // Constants
   //

   //
   // Double precision descriptor array size:
   //
   public final static int                  CK_DESCR_SIZE = 5;

   //
   // Double precision descriptor component size:
   //
   public final static int                  ND = 2;

   //
   // Integer descriptor component size:
   //
   public final static int                  NI = 6;

   //
   // Instance variables
   //
   double[]  dc;
   int[]     ic;

   //
   // Constructors
   //

   /**
   Construct a CK descriptor instance from an array of doubles
   representing a segment descriptor.
   */
   public CKDescriptor ( double[] descrArray )

      throws SpiceErrorException
   {
      int size = descrArray.length;

      if ( size != CK_DESCR_SIZE )
      {
         throw SpiceErrorException.create(
            "CKDescriptor.CKDescriptor",
            "SPICE(BADARRAYSIZE)",
            "Input CK descriptor array size must be " +
            CK_DESCR_SIZE +
            "but was " +
            size );
      }

      //
      // Unpack array.
      //
      dc = new double[ ND ];
      ic = new int   [ NI ];

      CSPICE.dafus( descrArray, ND, NI, dc, ic );
   }


   /**
   Construct a CK descriptor instance from double and integer component
   arrays.
   */
   public CKDescriptor ( double[] dc,
                         int[]    ic  )

      throws SpiceErrorException
   {
      int dSize = dc.length;

      if ( dSize != ND )
      {
         throw SpiceErrorException.create(
            "CKDescriptor.CKDescriptor",
            "SPICE(BADARRAYSIZE)",
            "Input CK double component array size must be " +
            ND +
            "but was " +
            dSize );
      }

      int iSize = ic.length;

      if ( iSize != NI )
      {
         throw SpiceErrorException.create(
            "CKDescriptor.CKDescriptor",
            "SPICE(BADARRAYSIZE)",
            "Input CK integer component array size must be " +
            NI +
            "but was " +
            iSize );
      }

      //
      // Copy descriptor components.
      //
      this.dc = new double[ ND ];
      this.ic = new int   [ NI ];

      System.arraycopy ( dc, 0, this.dc, 0, ND );
      System.arraycopy ( ic, 0, this.ic, 0, NI );
   }

   /**
   No-arguments constructor.
   */
   public CKDescriptor()
   {
      dc = new double[ ND ];
      ic = new int   [ NI ];
   }

   /**
   Copy constructor. This constructor creates a deep copy.
   */
   public CKDescriptor( CKDescriptor descr )
   {
      dc = new double[ ND ];
      ic = new int   [ NI ];

      System.arraycopy ( descr.dc, 0, this.dc, 0, ND );
      System.arraycopy ( descr.ic, 0, this.ic, 0, NI );
   }



   //
   // Instance Methods
   //


   /**
   Get double precision component.
   */
   public double[] getDoubleComponent()
   {
      double[] retval = new double[ ND ];

      System.arraycopy( dc, 0, retval, 0, ND );

      return retval;
   }

   /**
   Get integer component.
   */
   public int[] getIntComponent()
   {
      int[] retval = new int[ NI ];

      System.arraycopy( ic, 0, retval, 0, NI );

      return retval;
   }


   /**
   Extract CK descriptor contents into a double array.
   */
   public double[] toArray()

      throws SpiceErrorException
   {
      double[] retArray = CSPICE.dafps( dc, ic );

      return( retArray );
   }
}
