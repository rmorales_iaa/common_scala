
package spice.basic;
import java.util.Arrays;


/**
Class DASFileSummary provides access to summary parameters of a loaded DAS
file.

<p> This class enables user applications to obtain the summary without
re-reading the DAS file's file record.

<pre>

    DAS file summary members
    ========================

    A DAS file summary contains the following members:

       - File handle
       - Number of reserved records
       - Number of reserved characters
       - Number of comment records
       - Number of comment characters
       - Fortran record number of the first free record
       - Last logical addresses of character, double precision, and
         integer types, in that order.
       - Fortran record numbers of the last cluster directories
         of character, double precision, and  integer types, in that
         order.
       - Indices within cluster directory records of the last cluster
         directories of character, double precision, and  integer types, in that
         order.

</pre>

<p> See the DAS Required Reading for details concerning the DAS file format.

<p> Version 1.0.0 27-OCT-2021 (NJB)

*/


public class DASFileSummary
{

   //
   // Public fields
   //

   /**
   Index of in last address, last record, and last word arrays of
   parameters applicable to character DAS dataByteSeq:
   */
   public static final int CIDX = DAS.CIDX;

   /**
   Index of in last address, last record, and last word arrays of
   parameters applicable to double precision DAS dataByteSeq:
   */
   public static final int DIDX = DAS.DIDX;

   /**
   Index of in last address, last record, and last word arrays of
   parameters applicable to integer DAS dataByteSeq:
   */
   public static final int IIDX = DAS.IIDX;



   //
   // Instance variables
   //
   int                handle;
   int                numReservedRecords;
   int                numReservedCharacters;
   int                numCommentRecords;
   int                numCommentCharacters;
   int                firstFreeRecord;
   int[]              lastLogicalAddresses;
   int[]              lastDirectoryRecords;
   int[]              lastDirectoryWords;


   //
   // Constructors
   //

   /**
   Construct a CK descriptor instance from a DAS handle.
   */
   public DASFileSummary ( int handle )

      throws SpiceErrorException
   {
      this.handle = handle;

      lastLogicalAddresses = new int[3];
      lastDirectoryRecords = new int[3];
      lastDirectoryWords   = new int[3];

      int[]      nresvrArr = new int[1];
      int[]      nresvcArr = new int[1];
      int[]      ncomrArr  = new int[1];
      int[]      ncomcArr  = new int[1];
      int[]      freeArr   = new int[1];

      CSPICE.dashfs( handle,
                     nresvrArr,
                     nresvcArr,
                     ncomrArr,
                     ncomcArr,
                     freeArr,
                     lastLogicalAddresses,
                     lastDirectoryRecords,
                     lastDirectoryWords    );

      numReservedRecords    = nresvrArr[0];
      numReservedCharacters = nresvrArr[0];
      numCommentRecords     = ncomrArr[0];
      numCommentCharacters  = ncomcArr[0];
      firstFreeRecord       = freeArr[0];
   }


   /**
   No-arguments constructor.
   */
   public DASFileSummary()
   {
      lastLogicalAddresses = new int[3];
      lastDirectoryRecords = new int[3];
      lastDirectoryWords   = new int[3];
   }

   /**
   Copy constructor. This constructor creates a deep copy.
   */
   public DASFileSummary( DASFileSummary sum )
   {
      this.handle                = sum.handle;
      this.numReservedRecords    = sum.numReservedRecords;
      this.numReservedCharacters = sum.numReservedCharacters;
      this.numCommentRecords     = sum.numCommentRecords;
      this.numCommentCharacters  = sum.numCommentCharacters;
      this.firstFreeRecord       = sum.firstFreeRecord;

      lastLogicalAddresses = new int[3];
      lastDirectoryRecords = new int[3];
      lastDirectoryWords   = new int[3];

      System.arraycopy ( sum.lastLogicalAddresses,  0,
                         this.lastLogicalAddresses, 0, 3 );

      System.arraycopy ( sum.lastDirectoryRecords,  0,
                         this.lastDirectoryRecords, 0, 3 );

      System.arraycopy ( sum.lastDirectoryWords,    0,
                         this.lastDirectoryWords,   0, 3 );
   }

   /**
   Construct a DASFileSummary instance from user-supplied inputs.
   */
   public DASFileSummary( int     handle,
                          int     numReservedRecords,
                          int     numReservedCharacters,
                          int     numCommentRecords,
                          int     numCommentCharacters,
                          int     firstFreeRecord,
                          int[]   lastLogicalAddresses,
                          int[]   lastDirectoryRecords,
                          int[]   lastDirectoryWords    )
   {
      this.handle                = handle;
      this.numReservedRecords    = numReservedRecords;
      this.numReservedCharacters = numReservedCharacters;
      this.numCommentRecords     = numCommentRecords;
      this.numCommentCharacters  = numCommentCharacters;
      this.firstFreeRecord       = firstFreeRecord;

      this.lastLogicalAddresses = new int[3];
      this.lastDirectoryRecords = new int[3];
      this.lastDirectoryWords   = new int[3];

      System.arraycopy ( lastLogicalAddresses,      0,
                         this.lastLogicalAddresses, 0, 3 );

      System.arraycopy ( lastDirectoryRecords,      0,
                         this.lastDirectoryRecords, 0, 3 );

      System.arraycopy ( lastDirectoryWords,        0,
                         this.lastDirectoryWords,   0, 3 );
   }


   //
   // Instance Methods
   //


   /**
   Get DAS file handle.
   */
   public int getHandle()
   {
      return handle;
   }

   /**
   Get number of reserved records.
   */
   public int getNumReservedRecords()
   {
      return numReservedRecords;
   }

   /**
   Get number of reserved characters.
   */
   public int getNumReservedCharacters()
   {
      return numReservedCharacters;
   }

   /**
   Get number of comment records.
   */
   public int getNumCommentRecords()
   {
      return numCommentRecords;
   }

   /**
   Get number of comment characters.
   */
   public int getNumCommentCharacters()
   {
      return numCommentCharacters;
   }

   /**
   Get Fortran record number of first free record.
   */
   public int getFirstFreeRecord()
   {
      return firstFreeRecord;
   }

   /**
   Get last logical addresses of each DAS component.

   <pre>
   The order of the addresses in the output array is

      Character
      Double precision
      Integer

   These elements may be referenced using the public constants

      CIDX
      DIDX
      IIDX

   </pre>
   */
   public int[] getLastLogicalAddresses()
   {
      int[] retval = new int[ 3 ];

      System.arraycopy( lastLogicalAddresses, 0, retval, 0, 3 );

      return retval;
   }


   /**
   Get last cluster directory record numbers of each DAS component.

   <pre>
   The order of the record numbers in the output array is

      Character
      Double precision
      Integer

   These elements may be referenced using the public constants

      CIDX
      DIDX
      IIDX

   </pre>
   */
   public int[] getLastDirectoryRecords()
   {
      int[] retval = new int[ 3 ];

      System.arraycopy( lastDirectoryRecords, 0, retval, 0, 3 );

      return retval;
   }


   /**
   Get last cluster directory word indices of each DAS component.

   <pre>
   The order of the word indices in the output array is

      Character
      Double precision
      Integer

   These elements may be referenced using the public constants

      CIDX
      DIDX
      IIDX

   </pre>
   */
   public int[] getLastDirectoryWords()
   {
      int[] retval = new int[ 3 ];

      System.arraycopy( lastDirectoryWords, 0, retval, 0, 3 );

      return retval;
   }


   /**
   Test two DAS file summaries for equality.
   */
   public boolean equals( Object obj )
   {
      if ( obj == null )
      {
         return false;
      }

      if ( this.getClass() != obj.getClass() )
      {
         return false;
      }

      DASFileSummary sum = (DASFileSummary)obj;

      return (    ( this.handle                == sum.handle                )
               && ( this.numReservedRecords    == sum.numReservedRecords    )
               && ( this.numReservedCharacters == sum.numReservedCharacters )
               && ( this.numCommentRecords     == sum.numCommentRecords     )
               && ( this.numCommentCharacters  == sum.numCommentCharacters  )
               && ( this.firstFreeRecord       == sum.firstFreeRecord       )
               && ( Arrays.equals(
                    this.lastLogicalAddresses, sum.lastLogicalAddresses )   )
               && ( Arrays.equals(
                    this.lastDirectoryRecords, sum.lastDirectoryRecords )   )
               && ( Arrays.equals(
                    this.lastDirectoryWords,   sum.lastDirectoryWords   )   )
             );
   }
}
