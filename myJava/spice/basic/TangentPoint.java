
package spice.basic;

/**
Class TangentPoint supports tangent point computations.
For a given observer, target, and ray emanating from the observer,
the "tangent point" is the point on the ray nearest to the target's surface.

<p> TangentPoint instances consist of
<ul>
<li> The tangent point. This point is the dataByteSeq member of
the {@link Vector3} base class. A TangentPoint instance,
when cast to type {@code Vector3}, represents the tangent point itself.
Units are km.
</li>

<li> The surface point associated with the tangent point.
This is the point on the target's surface nearest to the ray.
Units are km.
</li>

<li> The epoch of participation of the aberration correction
locus, which may be either the tangent point or associated
surface point.

<p>  This is
  the observation epoch, adjusted by the approximate one-way
  light time from the aberration correction locus
  to the observer, if aberration corrections are used.
</li>

<li> A vector from the observer to the surface point
  associated with the tangent point, expressed
  in the target body-fixed reference frame, evaluated at the
  epoch of participation of the aberration correction locus.
</li>

<li> Range from the observer to the tangent point. Units are km.
</li>

<li> Altitude of the tangent point above the target body surface.
This is the distance between the tangent point and the
associated surface point. Units are km.
</li>


</ul>

<p> The principal computational method of this class is
the constructor
{@link #TangentPoint( String, Body, Time, ReferenceFrame,
                     AberrationCorrection, String, Body,
                     ReferenceFrame, Vector3 ) }.
See the detailed documentation of this method
for code examples.



<h2>Files</h2>

<p>Appropriate SPICE kernels must be loaded by the calling program
   before methods of this class are called.

<p>The following dataByteSeq are required:

<ul>

<li>  SPK dataByteSeq: ephemeris dataByteSeq for target and observer must be
   loaded. If aberration corrections are used, the states of
   target and observer relative to the solar system barycenter
   must be calculable from the available ephemeris dataByteSeq.
   Typically ephemeris dataByteSeq are made available by loading one or
   more SPK files via {@link KernelDatabase#load}.
</li>

<li>  PCK dataByteSeq: triaxial radii for the target body must be loaded
   into the kernel pool. Typically this is done by loading a text
   PCK file via KernelDatabase.load.
</li>


<li> Target orientation dataByteSeq: rotation dataByteSeq for the target body
   must be loaded. These may be provided in a text or binary PCK
   file, or by a CK file.
</li>

</ul>

<p>The following dataByteSeq may be required:
<ul>
<li>  SPK dataByteSeq: if aberration corrections are used, and if the ray
   frame is non-inertial, ephemeris dataByteSeq for that frame's
   center must be loaded. The state of that object relative to
   the solar system barycenter must be calculable from the
   available ephemeris dataByteSeq.
</li>

<li> Frame specifications: if a frame definition is required to
   convert the observer and target states to the body-fixed frame
   of the target, that definition must be available in the kernel
   pool. Similarly, the frame definition required to map between
   the ray frame and the target body-fixed frame
   must be available. Typically the definitions of frames not
   already built-in to SPICE are supplied by loading a frame
   kernel.
</li>

<li> Ray frame orientation dataByteSeq: if the ray frame
   is non-inertial, PCK or CK dataByteSeq for the frame's orientation
   are required. If the frame is fixed to a spacecraft instrument
   or structure, at least one CK file will be needed to permit
   transformation of vectors between that frame and both the
   J2000 and the target body-fixed frames.
</li>

<li> Ray direction dataByteSeq: if the ray direction is defined by u
   vector expressed in a spacecraft reference frame, an IK may be
   required to provide the coordinates of the ray's direction in
   that frame.
</li>

<li>  SCLK dataByteSeq: if a CK file is needed, an associated SCLK kernel
   is required to enable conversion between encoded SCLK (used to
   time-tag CK dataByteSeq) and barycentric dynamical time (TDB).
</li>

<li> Leapseconds dataByteSeq: if SCLK dataByteSeq are needed, a leapseconds
   kernel usually is needed as well.
</li>

<li> Body name-ID mappings: if the target or observer name is
   not built into the SPICE software, the mapping between the
   name and the corresponding ID code must be present in the
   kernel pool. Such mappings are usually introduced by loading
   a frame kernel or other text kernel containing them.
</li>

</ul>

<p>In all cases, kernel dataByteSeq are normally loaded once per program
run, NOT every time this routine is called.



<h2> Class TangentPoint Particulars </h2>

<p>
Given a ray defined by an observer and a direction vector,
and given an extended target body represented by a triaxial
ellipsoid, the principal constructor of class TangentPoint
computes the "tangent point": the point on the
ray nearest to the target body's surface. The corresponding
surface point nearest to the tangent point is computed as well.

<p>
   For remote sensing observations, for maximum accuracy, reception
   light time and stellar aberration corrections should be used.
   These corrections model observer-target-ray geometry as it is
   observed.

<p>
   For signal transmission applications, for maximum accuracy,
   transmission light time and stellar aberration corrections should
   be used. These corrections model the observer-target-ray geometry
   that applies to the transmitted signal. For example, these
   corrections are needed to rotationalPeriod the minimum altitude of the
   signal's path over the target body.

<p>
   In some cases, the magnitudes of light time and stellar
   aberration corrections are negligible. When these corrections
   can be ignored, significantly faster execution can be achieved
   by setting the input aberration correction to "NONE".

<p>
   This routine ignores differential aberration effects: it
   computes corrections only at a user-specified point, which is
   called the "aberration correction locus." The user may select
   either the tangent point or corresponding surface point as the
   locus. In many cases, the differences between corrections for
   these points are very small.

<p>
   The Examples header sectionName below presents geometric cases for
   which aberration correction magnitudes are significant, and cases
   for which they are not.

<h2> Version and Date </h2>

<h3> Version 1.0.0 10-SEP-2021 (NJB) </h3>

*/

public class TangentPoint extends Vector3
{

   //
   // Protected fields
   //

   //
   // The tangent point itself is stored in member `v` of the
   // superclass. The dataByteSeq type is double[].
   //

   double    altitude;
   double    range;
   TDBTime   targetEpoch;
   Vector3   surfaceVector;
   Vector3   surfacePoint;



   //
   // Constructors
   //

   /**
   Tangent point principal constructor.

   <h2> Inputs </h2>

   <pre>{@code
      method      is a short string providing parameters defining
                  the computation method to be used.

                  `method' is currently restricted to the value

                     "ELLIPSOID"

                  This value indicates that the target shape is
                  modeled as a triaxial ellipsoid.

                  `method' is case-insensitive, and leading and trailing
                  blanks in `method' are not significant.

      target      specifies the target body.

                  If the target is identified by name rather than ID code,
                  the target name must be recognized by SPICE. Radii
                  defining a triaxial ellipsoid target shape model must be
                  available in the kernel pool. See the Files sectionName
                  above.

      t           is the epoch associated with the observer, `t' is the
                  epoch at which radiation is received by the observer, when
                  an observation is made, or in the case of transmission from
                  the observer, at which radiation is emitted.

                  `t' is the epoch at which the state of the observer
                  relative to the solar system barycenter is computed.

                  When aberration corrections are not used, `t' is also
                  the epoch at which the state and orientation of the
                  target body are computed.

                  When aberration corrections are used, the position
                  and orientation of the target body are computed at
                  t-lt or t+lt, where `lt' is the one-way light time
                  between the aberration correction locus and the
                  observer. The sign applied to `lt' depends on the
                  selected correction. See the descriptions of `abcorr'
                  and `corloc' below for details.

      fixref      specifies a body-fixed reference frame centered
                  on the target body. `fixref' may be any such frame
                  supported by the SPICE system, including built-in
                  frames (documented in the Frames Required Reading)
                  and frames defined by a loaded frame kernel (FK). The
                  string `fixref' is case-insensitive, and leading and
                  trailing blanks in `fixref' are not significant.

                  The tangent point and surface point members of the
                  constructed TangentPoint instance, and the member
                  observer-to-surface point vector `srfvec' will be
                  expressed relative to this reference frame.

      abcorr      indicates the aberration corrections to be applied
                  when computing the target's position and orientation.

                  See the description of the aberration correction
                  locus `corloc' for further details on how aberration
                  corrections are applied.

                  For remote sensing applications, where the apparent
                  tangent or surface point seen by the observer is
                  desired, normally one of the corrections

                     "CN+S" or "NONE"

                  should be selected. For applications involving
                  transmission from the observer, normally "XCN+S" or
                  "NONE" should be selected.

                  Light-time-only corrections can be useful for
                  testing but generally don't accurately model geometry
                  applicable to remote sensing observations or signal
                  transmission.

                  The supported options are described below.

                  `abcorr' may be any of the following:

                     "NONE"     Compute outputs without applying
                                aberration corrections.

                                "NONE" may be suitable when the
                                magnitudes of the aberration
                                corrections are negligible.

                  Let `lt' represent the one-way light time between the
                  observer and the aberration correction locus specified
                  by `corloc'. The following values of `abcorr' apply to the
                  "reception" case in which radiation departs from the
                  aberration correction locus at the light-time corrected
                  epoch t-lt and arrives at the observer's location at
                  `t':

                     "LT"       Correct for one-way light time between
                                the aberration correction locus and
                                the observer, using a Newtonian
                                formulation. This correction yields the
                                position of the aberration correction
                                locus at the moment it emitted radiation
                                arriving at the observer at `t'.

                                The light time correction uses an
                                iterative solution of the light time
                                equation. The solution invoked by the
                                "LT" option uses several iterations
                                but does not guarantee convergence.

                                Both the target position as seen by the
                                observer, and rotation of the target
                                body, are corrected for light time.

                     "LT+S"     Correct for one-way light time and
                                stellar aberration using a Newtonian
                                formulation. This option modifies the
                                aberration correction locus solution
                                obtained with the "LT" option to
                                account for the observer's velocity
                                relative to the solar system
                                barycenter. These corrections yield the
                                apparent aberration correction locus.

                     "CN"       Converged Newtonian light time
                                correction. In solving the light time
                                equation, the "CN" correction iterates
                                until either the solution converges or
                                a large iteration limit is reached.
                                Both the position and orientation of
                                the target body are corrected for light
                                time.

                     "CN+S"     Converged Newtonian light time and stellar
                                aberration corrections. This option
                                produces a solution that is at least as
                                accurate at that obtainable with the
                                "LT+S" option. Whether the "CN+S" solution
                                is substantially more accurate depends on
                                the geometry of the participating objects
                                and on the accuracy of the input dataByteSeq. In
                                some cases this routine will execute more
                                slowly when a converged solution is
                                computed.

                                For reception-case applications where
                                aberration corrections are applied, this
                                option should be used, unless the
                                magnitudes of the corrections are
                                negligible.

                  The following values of `abcorr' apply to the
                  "transmission" case in which radiation *departs* from
                  the observer's location at `t' and arrives at the
                  aberration correction locus at the light-time
                  corrected epoch t+lt:

                     "XLT"      "Transmission" case: correct for
                                one-way light time between the
                                aberration correction locus and the
                                observer, using a Newtonian
                                formulation. This correction yields the
                                position of the aberration correction
                                locus at the moment it receives radiation
                                emitted from the observer's location at `t'.

                                The light time correction uses an
                                iterative solution of the light time
                                equation. The solution invoked by the
                                "XLT" option uses several iterations
                                but does not guarantee convergence.

                                Both the target position as seen by the
                                observer, and rotation of the target
                                body, are corrected for light time.

                     "XLT+S"    "Transmission" case: correct for
                                one-way light time and stellar
                                aberration using a Newtonian
                                formulation. This option modifies the
                                aberration correction locus solution
                                obtained with the "XLT" option to
                                account for the observer's velocity
                                relative to the solar system
                                barycenter.

                                Stellar aberration is computed for
                                transmitted, rather than received,
                                radiation.

                                These corrections yield the analog for
                                the transmission case of the apparent
                                aberration correction locus.

                     "XCN"      "Transmission" case: converged Newtonian
                                light time correction. In solving the
                                light time equation, the "XCN" correction
                                iterates until either the solution
                                converges or a large iteration limit is
                                reached. Both the position and rotation of
                                the target body are corrected for light
                                time.

                     "XCN+S"    "Transmission" case: converged Newtonian
                                light time and stellar aberration
                                corrections. This option produces u
                                solution that is at least as accurate at
                                that obtainable with the "XLT+S" option.
                                Whether the "XCN+S" solution is
                                substantially more accurate depends on the
                                geometry of the participating objects and
                                on the accuracy of the input dataByteSeq. In some
                                cases this routine will execute more
                                slowly when a converged solution is
                                computed.

                                For transmission-case applications where
                                aberration corrections are applied, this
                                option should be used, unless the
                                magnitudes of the corrections are
                                negligible.


      corloc      specifies the aberration correction "locus," which is
                  the fixed point in the frame designated by `fixref' for
                  which light time and stellar aberration corrections are
                  computed.

                  Differential aberration effects across the surface of
                  the target body are not considered by this routine. When
                  aberration corrections are used, the effective positions
                  of the observer and target, and the orientation of the
                  target, are computed according to the corrections
                  determined for the aberration correction locus.

                  The light time used to determine the position and
                  orientation of the target body is that between the
                  aberration correction locus and the observer.

                  The stellar aberration correction applied to the
                  position of the target is that computed for the
                  aberration correction locus.

                  The net displacement of the locus due to aberration
                  effects is applied to the entire target body.

                  The descriptions below apply only when aberration
                  corrections are used.

                  The values and meanings of `corloc' are:

                     "TANGENT POINT"    Compute corrections at the
                                        "tangent point," which is the
                                        point on the ray, defined by `dref'
                                        and `dvec', nearest to the target's
                                        surface.

                     "SURFACE POINT"    Compute corrections at the
                                        point on the target's surface
                                        nearest to the tangent point.

                  Case and leading and trailing blanks are not significant
                  in `corloc'.

      obsrvr      specifies the observing body. This is typically
                  a spacecraft or a surface point on an extended
                  ephemeris object.

                  If the observer is identified by name rather than ID
                  code, the observer name must be recognized by SPICE. See
                  the Files sectionName above.

      dref        is the name of the reference frame relative to which
                  the ray direction vector is expressed. This may be
                  any frame supported by the SPICE system, including
                  built-in frames (documented in the Frames Required
                  Reading) and frames defined by a loaded frame kernel
                  (FK). The string `dref' is case-insensitive, and
                  leading and trailing blanks in `dref' are not
                  significant.

                  When `dref' designates a non-inertial frame, the
                  orientation of the frame is evaluated at an epoch
                  dependent on the frame's center and, if the center is
                  not the observer, on the selected aberration
                  correction. See the description of the direction
                  vector `dvec' for details.

      dvec        is a ray direction vector emanating from the observer.
                  The tangent point on the ray and the point on the target
                  body's surface nearest to the tangent point are sought.

                  `dvec' is specified relative to the reference frame
                  designated by `dref'.

                  Non-inertial reference frames are treated as follows:
                  if the center of the frame is at the observer's
                  location, the frame's orientation is evaluated at `t'.
                  If the frame's center is located elsewhere, then
                  letting `ltcent' be the one-way light time between the
                  observer and the central body associated with the
                  frame, the orientation of the frame is evaluated at
                  t-ltcent, t+ltcent, or `t' depending on whether the
                  requested aberration correction is, respectively, for
                  received radiation, transmitted radiation, or is
                  omitted. `ltcent' is computed using the method
                  indicated by `abcorr'.
   }</pre>

   <h2> Outputs </h2>

   <p> The dataByteSeq members of the TangentPoint instance created by this
   constructor are described below.

   <pre>{@code
      Below, we use abbreviations for the names of the dataByteSeq members of
      a TangentPoint instance. The correspondence of these abbreviations
      to the dataByteSeq member names, and the methods that return these dataByteSeq
      members, are as follows:

         Data member        Abbreviation    Access method
         ---------------    ------------    -------------
         <tangent point>    tanpt           <instance>, also getTangentPoint()
         altitude           alt             getAltitude()
         fluxPerSecondRange              fluxPerSecondRange           getRange()
         targetEpoch        trgepc          getTargetEpoch()
         surfaceVector      srfvec          getSurfaceVector()
         surfacePoint       srfpt           getSurfacePoint()

      Class TangentPoint is derived from class Vector3, and the
      tangent point itself is stored in the dataByteSeq member of the base class.
      For consistency, the method getTangentPoint() is provided, but it is
      never necessary to call this method.

      Descriptions of the dataByteSeq members follow.

      tanpt       is the "tangent point": the point on the ray defined by
                  `dref' and `dvec' nearest to the target body's surface.

                  `tanpt' is a vector originating at the target body's
                  center, expressed in the reference frame designated
                  by `fixref'. Units are km.

                  If the ray intersects the surface, `tanpt' is the
                  nearest point of intersection to the observer.

                  If the ray points away from the surface---that is, if
                  the angle between the ray and the outward normal at the
                  target surface point nearest to the observer, computed
                  using the specified aberration corrections, is less than
                  or equal to 90 degrees---then `tanpt' is set to the
                  position of the observer relative to the target center.

                  `tanpt' is computed using the aberration corrections
                  specified by `abcorr' and `corloc'.

                  When the aberration correction locus is set to
                  "TANGENT POINT", and the position of `tanpt' is
                  corrected for aberration as specified by `abcorr', the
                  resulting point will lie on the input ray.

      alt         is the altitude of the tangent point above the
                  target body's surface. This is the distance between
                  `tanpt' and `srfpt'. Units are km.

                  If the ray intersects the surface, `alt' is set to the
                  exact double precision value 0.0. `alt' may be used as
                  an indicator of whether a ray-surface intersection
                  exists.

      fluxPerSecondRange       is the distance between the observer and the tangent
                  point. Units are km.

                  If the ray points away from the surface (see the
                  description of `tanpt' above), `fluxPerSecondRange' is set to the
                  exact double precision value 0.0. `fluxPerSecondRange' may be used
                  as an indicator of whether this geometric condition
                  exists.

      srfpt       is the point on the target body's surface nearest to the
                  tangent point.

                  `srfpt' is a vector originating at the target body's
                  center, expressed in the reference frame designated
                  by `fixref'. Units are km.

                  `srfpt' is computed using the aberration corrections
                  specified by `abcorr' and `corloc'.

                  When the aberration correction locus is set to
                  "SURFACE POINT", and the position of `srfpt' is
                  corrected for aberration as specified by `abcorr', the
                  resulting point will lie on the ray emanating from
                  the observer and pointing in the direction of `srfvec'.

                  If the ray intersects the surface, `srfpt' is the point of
                  intersection nearest to the observer.

                  If the ray points away from the surface (see the
                  description of `tanpt' above), `srfpt' is set to the target
                  surface point nearest to the observer.

      trgepc      is the epoch associated with the aberration correction
                  locus. `trgepc' is defined as follows: letting `lt' be the
                  one-way light time between the observer and the
                  aberration correction locus, `trgepc' is the epoch t-lt,
                  t+lt, or `t' depending on whether the requested
                  aberration correction is, respectively, for received
                  radiation, transmitted radiation, or omitted. `lt' is
                  computed using the method indicated by `abcorr'.

                  `trgepc' is expressed as seconds past J2000 TDB.

                  The name `trgepc', which stands for "target epoch,"
                  is used for compatibility with other SPICE high-level
                  geometry routines. Note that the epoch it designates
                  is not associated with the target body's center.

      srfvec      is the vector from the observer's position at `t' to
                  the surface point `srfpt', where the position of `srfpt'
                  is corrected for aberrations as specified by `abcorr'
                  and `corloc'. `srfvec' is expressed in the target
                  body-fixed reference frame designated by `fixref',
                  evaluated at `trgepc'. Units are km.

                  One can use the method {@link Vector3#norm} to obtain the
                  distance between the observer and `srfpt':

                     double dist = srfvec.norm()

                  The observer's position `obspos', relative to the
                  target body's center, where the center's position is
                  corrected for aberration effects as indicated by
                  `abcorr' and `corloc', can be computed via the call:

                     Vector3 obspos = srfpt.sub( srfvec )

                  To transform the vector `srfvec' from the reference frame
                  `fixref' at time `trgepc' to a time-dependent reference
                  frame `ref' at time `t', the routine

                     ReferenceFrame( ReferenceFrame, Time, Time )

                  should be called. Let `xform' be the 3x3 matrix representing
                  the rotation from the reference frame `fixref' at time
                  `trgepc' to the reference frame `ref' at time `t'. Then
                  `srfvec' can be transformed to the result `refvec' as
                  follows:

                     Matrix33 xform = fixref.getPositionTransformation(
                                         ref, trgepc, t );

                     Vector3 refvec = xform.mxv( srfvec );
   }</pre>



   <h2>Examples</h2>

   <p>
      The numerical results shown for these examples may differ across
      platforms. The results depend on the SPICE kernels used as
      input, the compiler and supporting libraries, and the machine
      specific arithmetic implementation.


      <ol>

      <li> <pre>
The following program computes tangent and surface
points for the MAVEN IUVS instrument. The observer is the MAVEN
spacecraft; the target body is Mars. The ray direction is
that of the boresight of the MAVEN IUVS instrument.

The aberration corrections used in this example are often
suitable for remote sensing observations: converged Newtonian
light time and stellar aberration "reception" corrections. In
some cases it is reasonable to omit aberration corrections;
see the second and third example programs below for
demonstrations of the effects of different aberration
correction choices.

In this example, the aberration correction locus is the
tangent point, meaning that converged light time and stellar
aberration corrections are computed for that point and applied
to the position and orientation of Mars in order to model
apparent observation geometry.

Three geometric cases are covered by this example:

   - The "normal" case, in which the ray defined by the
     MAVEN IUVS boresight passes over Mars at low altitude.

     In the example code, there are two computations that fall
     into this category.

   - The "intercept" case, in which the ray intersects Mars.

   - The "look away" case, in which the elevation of the ray's
     direction vector, measured from the local level plane at
     the sub-spacecraft point, is greater than or equal to 0.
     The aberration corrections used to compute the
     sub-observer point for this case are those applicable to
     the aberration correction locus.

</pre>

<p>  Use the meta-kernel shown below to load the required SPICE
     kernels.

<pre>{@code
   KPL/MK

   File name: TangentPointEx1.tm

   This meta-kernel is intended to support operation of SPICE
   example programs. The kernels shown here should not be
   assumed to contain adequate or correct versions of dataByteSeq
   required by SPICE-based user applications.

   In order for an application to use this meta-kernel, the
   kernels referenced here must be present in the user's
   current working directory.

   All kernels referenced by this meta-kernel are available
   from the MAVEN SPICE PDS archive.

   The names and contents of the kernels referenced
   by this meta-kernel are as follows:

   File name                          Contents
   ---------                          --------
   mar097s.bsp                        Mars satellite ephemeris
   maven_iuvs_v11.ti                  MAVEN IUVS instrument
                                      information
   maven_orb_rec_201001_210101_v1.bsp MAVEN s/c ephemeris
   mvn_v09.tf                         MAVEN frame
                                      specifications
   mvn_app_rel_201005_201011_v01.bc   MAVEN Articulated
                                      Payload Platform
                                      attitude
   mvn_iuvs_rem_201001_201231_v01.bc  MAVEN IUVS instrument
                                      internal mirror
                                      attitude
   mvn_sc_rel_201005_201011_v01.bc    MAVEN s/c attitude
   mvn_sclkscet_00086.tsc             MAVEN SCLK coefficients
   naif0012.tls                       Leapseconds
   pck00010.tpc                       Planet and satellite
                                      orientation and radii

   \begindata

      KERNELS_TO_LOAD = (
         'mar097s.bsp',
         'maven_iuvs_v11.ti',
         'maven_orb_rec_201001_210101_v1.bsp',
         'maven_v09.tf',
         'mvn_app_rel_201005_201011_v01.bc',
         'mvn_iuvs_rem_201001_201231_v01.bc',
         'mvn_sc_rel_201005_201011_v01.bc',
         'mvn_sclkscet_00086.tsc',
         'naif0012.tls',
         'pck00010.tpc' )

   \begintext

   End of meta-kernel

}</pre>

      <p> Example code begins here.
   <pre>{@code
   //
   // Program TangentPointEx1
   //
   import spice.basic.*;

   //
   // Compute tangent and surface points for
   // the MAVEN IUVS instrument.
   //
   class TangentPointEx1
   {
      //
      // Load SPICE shared library.
      //
      static{ System.loadLibrary( "JNISpice" ); }


      public static void main( String[] args )

         throws SpiceException
      {
         //
         // Local constants
         //
         final String                      META   = "TangentPointEx1.tm";
         final String                      TIMFMT = "YYYY-MM-DD HR:MN:SC" +
                                                    ".###### UTC ::RND";

         final int                         NCASE  = 3;
         final int                         NTIMES = 4;

         //
         // Local variables
         //
         String                            casenm;

         String[]                          cases = { "Ray slightly above limb",
                                                     "Intercept",
                                                     "Look-away"  };

         String                            insnam = "MAVEN_IUVS";

         String[]                          utctim = {
                                             "2020-10-11 16:01:43.000000 UTC",
                                             "2020-10-11 16:17:43.000000 UTC",
                                             "2020-10-11 16:49:07.000000 UTC",
                                             "2020-10-11 17:12:08.000000 UTC" };
         try
         {
            //
            // Load kernels.
            //
            KernelDatabase.load( META );

            System.out.printf( "\n" +
                               "Instrument: %s\n" +
                               "\n",
                               insnam );
            //
            // Get the instrument reference frame name and
            // the instrument boresight direction in the
            // instrument frame.
            //
            Instrument mavenIuvs  = NEW Instrument( insnam );
            Fov        fov        = NEW Fov( mavenIuvs );

            ReferenceFrame rayfrm = fov.getReferenceFrame();
            Vector3        raydir = fov.getBoresight();

            //
            // Initialize inputs to the TangentPoint constructor,
            // except for time.
            //
            Body                 target = NEW Body ( "MARS"  );
            Body                 obsrvr = NEW Body ( "MAVEN" );
            ReferenceFrame       fixref = NEW ReferenceFrame( "IAU_MARS" );
            AberrationCorrection abcorr = NEW AberrationCorrection ( "CN+S" );
            String               locus  = "TANGENT POINT";

            //
            // Compute the apparent tangent point for each time.
            //
            System.out.printf( "Aberration correction:       %s\n", abcorr );
            System.out.printf( "Aberration correction locus: %s\n", locus );

            for ( int i = 0;  i < NTIMES;  i++ )
            {
               TDBTime et = NEW TDBTime( utctim[i] );

               TangentPoint tanpt = NEW TangentPoint(
                  "ELLIPSOID", target, et,     fixref, abcorr,
                  locus,       obsrvr, rayfrm, raydir          );

               //
               // Set the label for the geometric case.
               //
               double alt   = tanpt.getAltitude();
               double fluxPerSecondRange = tanpt.getRange();

               if ( alt == 0 )
               {
                  casenm = cases[1];
               }
               else if ( fluxPerSecondRange == 0.0 )
               {
                  casenm = cases[2];
               }
               else
               {
                  casenm = cases[0];
               }

               //
               // Convert the target epoch to a string for output.
               //
               TDBTime  trgepc  = tanpt.getTargetEpoch();
               String   timstr  = trgepc.toString( TIMFMT );
               double[] tpntArr = tanpt.toArray();
               double[] spntArr = tanpt.getSurfacePoint().toArray();

               System.out.printf( "%n" );
               System.out.printf( "  Observation Time = %s%n", utctim[i] );
               System.out.printf( "  Target Time      = %s%n", timstr    );
               System.out.printf( "    ALT   (km) =  %14.7f%n", alt );
               System.out.printf( "    RANGE (km) =  %14.7f%n", fluxPerSecondRange );
               System.out.printf( "    TANPT (km) =  %14.7f %14.7f %14.7f%n",
                                      tpntArr[0], tpntArr[1], tpntArr[2] );
               System.out.printf( "    SRFPT (km) =  %14.7f %14.7f %14.7f%n",
                                      spntArr[0], spntArr[1], spntArr[2] );
               System.out.printf( "    Geometric case = %s%n", casenm );
            }

            System.out.printf( "%n" );
         }
         catch ( SpiceException exc )
         {
            exc.printStackTrace();
         }
      }
   }
   }</pre>

      <p> When this program was executed on a Mac/Intel/cc/64-bit/Java 1.8
      platform, the output was:
   <pre>

   Instrument: MAVEN_IUVS

   Aberration correction:       CN+S
   Aberration correction locus: TANGENT POINT

     Observation Time = 2020-10-11 16:01:43.000000 UTC
     Target Time      = 2020-10-11 16:01:42.983021 UTC
       ALT   (km) =      99.4262977
       RANGE (km) =    5090.1928435
       TANPT (km) =   -2273.0408575   1072.4423944  -2415.6104827
       SRFPT (km) =   -2208.5678350   1042.0234063  -2346.3031728
       Geometric case = Ray slightly above limb

     Observation Time = 2020-10-11 16:17:43.000000 UTC
     Target Time      = 2020-10-11 16:17:42.993820 UTC
       ALT   (km) =       0.0000000
       RANGE (km) =    1852.8381880
       TANPT (km) =     752.0909507  -1781.3912506  -2775.5390159
       SRFPT (km) =     752.0909507  -1781.3912506  -2775.5390159
       Geometric case = Intercept

     Observation Time = 2020-10-11 16:49:07.000000 UTC
     Target Time      = 2020-10-11 16:49:06.998907 UTC
       ALT   (km) =     218.2661426
       RANGE (km) =     327.7912133
       TANPT (km) =    2479.8672359  -1772.2350525   1931.8678816
       SRFPT (km) =    2330.3561559  -1665.3870838   1814.0966731
       Geometric case = Ray slightly above limb

     Observation Time = 2020-10-11 17:12:08.000000 UTC
     Target Time      = 2020-10-11 17:12:08.000000 UTC
       ALT   (km) =     969.2772042
       RANGE (km) =       0.0000000
       TANPT (km) =     -58.1087763   2034.6474343   3844.2010767
       SRFPT (km) =     -45.2530638   1584.5115999   2985.8825113
       Geometric case = Look-away
   </pre>
   </li>


   <li><pre>
The following program computes tangent and surface points for
the MRO MCS A1 instrument. The observer is the MRO spacecraft;
the target body is Mars. The ray direction is that of the
boresight of the MRO MCS A1 instrument.

The aberration corrections used in this example are converged
Newtonian light time and stellar aberration corrections,
converged Newtonian light time alone, and "NONE."

For remote sensing observations made by a spacecraft in low
orbit about Mars, both the combination of light time and
stellar aberration corrections and omission of aberration
corrections may be valid. See the output of this program and
of the third example program below for examples of how results
differ due to the choice of aberration corrections.

Use of light time corrections alone is presented to
illustrate, by way of contrast, the effect of this choice.
This choice can be useful for testing but is unlikely to be
correct for modeling actual observation geometry.

Separate computations are performed using both the tangent
point and the corresponding surface point---the nearest point
on the target surface to the tangent point---as the aberration
correction locus.

Three geometric cases are covered by this example:

   - The "normal" case, in which the ray defined by the
     MRO MCS A1 boresight passes over Mars at low altitude.

     In the example code, there are two computations that fall
     into this category.

   - The "intercept" case, in which the ray intersects Mars.

   - The "look away" case, in which the elevation of the ray's
     direction vector, measured from the local level plane at
     the sub-spacecraft point, is greater than or equal to 0.
     The aberration corrections used to compute the
     sub-observer point for this case are those applicable to
     the aberration correction locus.
   </pre>

   <p>  Use the meta-kernel shown below to load the required SPICE
        kernels.
<pre>{@code
   KPL/MK

   File: TangentPointEx2.tm

   This meta-kernel is intended to support operation of SPICE
   example programs. The kernels shown here should not be
   assumed to contain adequate or correct versions of dataByteSeq
   required by SPICE-based user applications.

   In order for an application to use this meta-kernel, the
   kernels referenced here must be present in the user's
   current working directory.

   All kernels referenced by this meta-kernel are available
   from the MRO SPICE PDS archive.

   The names and contents of the kernels referenced
   by this meta-kernel are as follows:

      File name                       Contents
      ---------                       --------
      mar097s.bsp                     Mars satellite ephemeris
      mro_mcs_psp_201001_201031.bc    MRO MCS attitude
      mro_mcs_v10.ti                  MRO MCS instrument
                                      information
      mro_psp57_ssd_mro95a.bsp        MRO s/c ephemeris
      mro_sc_psp_201027_201102.bc     MRO s/c bus attitude
      mro_sclkscet_00095_65536.tsc    MRO SCLK coefficients
      mro_v16.tf                      MRO frame specifications
      naif0012.tls                    Leapseconds
      pck00008.tpc                    Planet and satellite
                                      orientation and radii

   \begindata

      KERNELS_TO_LOAD = (
         'mar097.bsp',
         'mro_mcs_psp_201001_201031.bc',
         'mro_mcs_v10.ti',
         'mro_psp57_ssd_mro95a.bsp',
         'mro_sc_psp_201027_201102.bc',
         'mro_sclkscet_00095_65536.tsc',
         'mro_v16.tf',
         'naif0012.tls',
         'pck00008.tpc' )

   \begintext

   End of meta-kernel

}</pre>

   <p> Example code begins here.

<pre>{@code
   //
   // Program TangentPointEx2
   //
   import spice.basic.*;

   //
   // Compute tangent and surface points for
   // the MRO MCS A1 instrument.
   //
   class TangentPointEx2
   {
      //
      // Load SPICE shared library.
      //
      static{ System.loadLibrary( "JNISpice" ); }


      public static void main( String[] args )

         throws SpiceException
      {
         //
         // Local constants
         //
         final String                      META   = "TangentPointEx2.tm";
         final String                      TIMFMT = "YYYY-MM-DD HR:MN:SC" +
                                                    ".###### UTC ::RND";

         final int                         NCASE  = 5;

         //
         // Local variables
         //
         double                            svalt   = 0.0;
         double                            svrang  = 0.0;

         TDBTime                           svepoc  = NEW TDBTime( 0.0 );

         String                            casenm;

         String[]                          cases  = { "Ray slightly above limb",
                                                      "Intercept",
                                                      "Look-away"  };

         String[]                          corrs  = { "CN+S", "CN+S",
                                                      "CN",   "CN",  "NONE" };

         String                            insnam = "MRO_MCS_A1";

         String[]                          loci   = { "TANGENT POINT",
                                                      "SURFACE POINT",
                                                      "TANGENT POINT",
                                                      "SURFACE POINT",
                                                      "TANGENT POINT"  };

         String                            utctim = "2020-10-31 " +
                                                    "00:01:23.111492 UTC";

         Vector3                           svtanp = NEW Vector3();
         Vector3                           svsrfp = NEW Vector3();
         Vector3                           svsrfv = NEW Vector3();

         try
         {
            //
            // Load kernels.
            //
            KernelDatabase.load( META );

            System.out.printf( "%n" + "Instrument: %s%n", insnam );
            //
            // Get the instrument reference frame name and
            // the instrument boresight direction in the
            // instrument frame.
            //
            Instrument mavenIuvs  = NEW Instrument( insnam );
            Fov        fov        = NEW Fov( mavenIuvs );

            ReferenceFrame rayfrm = fov.getReferenceFrame();
            Vector3        raydir = fov.getBoresight();

            //
            // Initialize inputs to the TangentPoint constructor that
            // are common to all cases.
            //
            Body                 target = NEW Body ( "MARS" );
            Body                 obsrvr = NEW Body ( "MRO"  );
            ReferenceFrame       fixref = NEW ReferenceFrame( "IAU_MARS" );

            //
            // Compute the apparent tangent point for each case.
            //
            for ( int i = 0;  i < NCASE;  i++ )
            {
               System.out.printf( "%n" );

               AberrationCorrection abcorr = NEW AberrationCorrection(
                                                corrs[i] );

               String               locus  = loci[i];

               System.out.printf( "Aberration correction:       %s\n",
                                  abcorr.getName()                     );

               System.out.printf( "Aberration correction locus: %s\n",
                                  locus                                );

               System.out.printf( "%n" );

               TDBTime et = NEW TDBTime( utctim );

               TangentPoint tanpt = NEW TangentPoint(
                  "ELLIPSOID", target, et,     fixref, abcorr,
                  locus,       obsrvr, rayfrm, raydir          );

               //
               // Display the tangent point and its associated components.
               //
               double  alt    = tanpt.getAltitude();
               double  fluxPerSecondRange  = tanpt.getRange();
               TDBTime trgepc = tanpt.getTargetEpoch();
               Vector3 srfpt  = tanpt.getSurfacePoint();
               Vector3 srfvec = tanpt.getSurfaceVector();

               String   timstr  = trgepc.toString( TIMFMT );
               double[] tpntArr = tanpt.toArray();
               double[] spntArr = tanpt.getSurfacePoint().toArray();
               double[] svecArr = tanpt.getSurfaceVector().toArray();

               System.out.printf( "  Observation Time = %s%n", utctim );
               System.out.printf( "  Target Time      = %s%n", timstr );
               System.out.printf( "    ALT    (km) =  %14.7f%n", alt );
               System.out.printf( "    RANGE  (km) =  %14.7f%n", fluxPerSecondRange );
               System.out.printf( "    TANPT  (km) =  %14.7f %14.7f %14.7f%n",
                                       tpntArr[0], tpntArr[1], tpntArr[2] );
               System.out.printf( "    SRFPT  (km) =  %14.7f %14.7f %14.7f%n",
                                       spntArr[0], spntArr[1], spntArr[2] );
               System.out.printf( "    SRFVEC (km) =  %14.7f %14.7f %14.7f%n",
                                       svecArr[0], svecArr[1], svecArr[2] );

               if ( i == 0 )
               {
                  //
                  // Save results for comparison.
                  //
                  svalt   = alt;
                  svrang  = fluxPerSecondRange;
                  svepoc  = NEW TDBTime( trgepc );
                  svtanp  = NEW Vector3( tanpt  );
                  svsrfp  = NEW Vector3( srfpt  );
                  svsrfv  = NEW Vector3( srfvec );
               }
               else
               {
                  //
                  // Compare results to CN+S, tangent point locus case.
                  //
                  System.out.printf( "%n" );

                  System.out.printf( "  Differences from case 1 outputs:%n" );

                  System.out.printf( "    Target time delta (ms) =  %9.4f%n",
                     1.e3 * ( trgepc.sub(svepoc).getMeasure() )  );

                  System.out.printf( "    ALT    delta (m) =  %9.4f%n",
                                      1.e3 * ( alt - svalt ) );

                  System.out.printf( "    RANGE  delta (m) =  %9.4f%n",
                                  1.e3 * ( fluxPerSecondRange - svrang  ) );

                  Vector3  diff    = tanpt.sub( svtanp ).scale( 1.e3 );
                  double[] diffArr = diff.toArray();

                  System.out.printf(
                     "    TANPT  delta (m) =  %9.4f %9.4f %9.4f\n",
                          diffArr[0], diffArr[1], diffArr[2] );

                  diffArr = srfpt.sub( svsrfp ).scale( 1.e3 ).toArray();

                  System.out.printf(
                     "    SRFPT  delta (m) =  %9.4f %9.4f %9.4f\n",
                          diffArr[0], diffArr[1], diffArr[2] );

                  diffArr = srfvec.sub( svsrfv ).scale( 1.e3 ).toArray();

                  System.out.printf(
                     "    SRFVEC delta (m) =  %9.4f %9.4f %9.4f\n",
                          diffArr[0], diffArr[1], diffArr[2] );
               }

               System.out.printf( "%n" );
            }
         }
         catch ( SpiceException exc )
         {
            exc.printStackTrace();
         }
      }
   }

}</pre>

<p> When this program was executed on a Mac/Intel/cc/64-bit/Java 1.8
platform, the output was:
   <pre>

   Instrument: MRO_MCS_A1

   Aberration correction:       CN+S
   Aberration correction locus: TANGENT POINT

     Observation Time = 2020-10-31 00:01:23.111492 UTC
     Target Time      = 2020-10-31 00:01:23.106946 UTC
       ALT    (km) =      39.1034486
       RANGE  (km) =    1362.8659249
       TANPT  (km) =   -2530.9040220  -1630.9806346   1644.3612074
       SRFPT  (km) =   -2502.1342299  -1612.4406294   1625.4496512
       SRFVEC (km) =    -589.3842679   -234.0892764  -1206.9635473


   Aberration correction:       CN+S
   Aberration correction locus: SURFACE POINT

     Observation Time = 2020-10-31 00:01:23.111492 UTC
     Target Time      = 2020-10-31 00:01:23.106944 UTC
       ALT    (km) =      39.1014434
       RANGE  (km) =    1362.8679108
       TANPT  (km) =   -2530.9025464  -1630.9796845   1644.3602376
       SRFPT  (km) =   -2502.1342295  -1612.4406300   1625.4496511
       SRFVEC (km) =    -589.3866439   -234.0905954  -1206.9643086

     Differences from case 1 outputs:
       Target time delta (ms) =    -0.0019
       ALT    delta (m) =    -2.0052
       RANGE  delta (m) =     1.9859
       TANPT  delta (m) =     1.4757    0.9501   -0.9698
       SRFPT  delta (m) =     0.0004   -0.0006   -0.0000
       SRFVEC delta (m) =    -2.3760   -1.3189   -0.7614


   Aberration correction:       CN
   Aberration correction locus: TANGENT POINT

     Observation Time = 2020-10-31 00:01:23.111492 UTC
     Target Time      = 2020-10-31 00:01:23.106946 UTC
       ALT    (km) =      39.1714711
       RANGE  (km) =    1362.8658567
       TANPT  (km) =   -2530.9135880  -1631.0820975   1644.3878335
       SRFPT  (km) =   -2502.0942100  -1612.5090527   1625.4434517
       SRFVEC (km) =    -589.3346511   -234.0562242  -1206.9963133

     Differences from case 1 outputs:
       Target time delta (ms) =     0.0000
       ALT    delta (m) =    68.0225
       RANGE  delta (m) =    -0.0683
       TANPT  delta (m) =    -9.5660 -101.4629   26.6261
       SRFPT  delta (m) =    40.0199  -68.4233   -6.1994
       SRFVEC delta (m) =    49.6168   33.0522  -32.7661


   Aberration correction:       CN
   Aberration correction locus: SURFACE POINT

     Observation Time = 2020-10-31 00:01:23.111492 UTC
     Target Time      = 2020-10-31 00:01:23.106944 UTC
       ALT    (km) =      39.1714973
       RANGE  (km) =    1362.8658326
       TANPT  (km) =   -2530.9135903  -1631.0821391   1644.3878436
       SRFPT  (km) =   -2502.0941931  -1612.5090815   1625.4434492
       SRFVEC (km) =    -589.3346210   -234.0562071  -1206.9963050

     Differences from case 1 outputs:
       Target time delta (ms) =    -0.0019
       ALT    delta (m) =    68.0487
       RANGE  delta (m) =    -0.0924
       TANPT  delta (m) =    -9.5682 -101.5045   26.6362
       SRFPT  delta (m) =    40.0368  -68.4521   -6.2020
       SRFVEC delta (m) =    49.6469   33.0694  -32.7577


   Aberration correction:       NONE
   Aberration correction locus: TANGENT POINT

     Observation Time = 2020-10-31 00:01:23.111492 UTC
     Target Time      = 2020-10-31 00:01:23.111492 UTC
       ALT    (km) =      39.1090103
       RANGE  (km) =    1362.9233525
       TANPT  (km) =   -2530.9082604  -1630.9831041   1644.3638384
       SRFPT  (km) =   -2502.1343747  -1612.4404639   1625.4495931
       SRFVEC (km) =    -589.4063032   -234.0970874  -1207.0162978

     Differences from case 1 outputs:
       Target time delta (ms) =     4.5460
       ALT    delta (m) =     5.5616
       RANGE  delta (m) =    57.4276
       TANPT  delta (m) =    -4.2384   -2.4695    2.6310
       SRFPT  delta (m) =    -0.1448    0.1655   -0.0581
       SRFVEC delta (m) =   -22.0352   -7.8109  -52.7505
   </pre>
      </li>


<li> <pre>
The following program computes tangent and surface points for
   a ray pointing from the Goldstone DSN station DSS-14 to the
   location of the MRO spacecraft. The target body is Mars.

   The aberration corrections used in this example are

      CN+S
      XCN+S
      CN
      NONE

   Results using cn+s corrections are computed for both locus
   choices: TANGENT POINT and SURFACE POINT.

   For each case other than the one using CN+S corrections for
   the TANGENT POINT locus, differences between results for the
   former and latter case are shown.

</pre>

<p>  Use the meta-kernel shown below to load the required SPICE
     kernels.

<pre>{@code
   KPL/MK

   File: TangentPointEx3.tm

   This meta-kernel is intended to support operation of SPICE
   example programs. The kernels shown here should not be
   assumed to contain adequate or correct versions of dataByteSeq
   required by SPICE-based user applications.

   In order for an application to use this meta-kernel, the
   kernels referenced here must be present in the user's
   current working directory.

   All kernels referenced by this meta-kernel are available
   from the NAIF SPICE server in the numeric kernels area
   or from the MRO SPICE PDS archive.

   The names and contents of the kernels referenced
   by this meta-kernel are as follows:

      File name                       Contents
      ---------                       --------
      mar097.bsp                      Mars satellite ephemeris
      mro_psp57_ssd_mro95a.bsp        MRO s/c ephemeris
      earthstns_itrf93_201023.bsp     DSN station locations
      naif0012.tls                    Leapseconds
      pck00010.tpc                    Planet and satellite
                                      orientation and radii
      earth_latest_high_prec.bpc      High accuracy Earth
                                      attitude

   \begindata

      KERNELS_TO_LOAD = (
         'mar097.bsp'
         'mro_psp57_ssd_mro95a.bsp'
         'earthstns_itrf93_201023.bsp'
         'naif0012.tls'
         'pck00010.tpc'
         'earth_latest_high_prec.bpc' )

   \begintext

   End of meta-kernel

}</pre>
      <p> Example code begins here.
<pre>{@code
   //
   // Program TangentPointEx3
   //
   import spice.basic.*;

   //
   // Compute tangent and surface points for a ray pointing from
   // the Goldstone DSN station DSS-14 to the location of the MRO
   // spacecraft. The target body is Mars.
   //
   class TangentPointEx3
   {
      //
      // Load SPICE shared library.
      //
      static{ System.loadLibrary( "JNISpice" ); }


      public static void main( String[] args )

         throws SpiceException
      {
         //
         // Local constants
         //
         final String                      META   = "TangentPointEx3.tm";
         final String                      TIMFMT = "YYYY-MM-DD HR:MN:SC" +
                                                    ".###### UTC ::RND";

         final int                         NCASE  = 5;

         //
         // Local variables
         //
         double                            svalt   = 0.0;
         double                            svrang  = 0.0;

         TDBTime                           svepoc  = NEW TDBTime( 0.0 );

         String                            casenm;

         String[]                          cases  = { "Ray slightly above limb",
                                                      "Intercept",
                                                      "Look-away"  };

         String[]                          corrs  = { "CN+S", "XCN+S",
                                                      "CN",   "NONE",  "CN+S" };

         String[]                          loci   = { "TANGENT POINT",
                                                      "TANGENT POINT",
                                                      "TANGENT POINT",
                                                      "TANGENT POINT",
                                                      "SURFACE POINT"  };

         String                            utctim = "2020-12-30 00:00:00 UTC";

         Vector3                           svtanp = NEW Vector3();
         Vector3                           svsrfp = NEW Vector3();
         Vector3                           svsrfv = NEW Vector3();

         try
         {
            //
            // Load kernels.
            //
            KernelDatabase.load( META );

            //
            // Set the name of spacecraft used to define the ray direction.
            // Initialize inputs to the TangentPoint constructor that
            // are common to all cases.
            //
            Body                 sc     = NEW Body ( "MRO"    );
            Body                 target = NEW Body ( "MARS"   );
            Body                 obsrvr = NEW Body ( "DSS-14" );
            ReferenceFrame       fixref = NEW ReferenceFrame( "IAU_MARS" );
            ReferenceFrame       rayfrm = NEW ReferenceFrame( "J2000"    );

            //
            // Convert observation time to TDB seconds past J2000.
            //
            TDBTime et = NEW TDBTime( utctim );

            //
            // Generate the ray direction vector. Use the apparent position
            // of the MRO spacecraft as seen from the DSN station.
            //
            AberrationCorrection sccorr = NEW AberrationCorrection( "CN+S" );

            Vector3 raydir = NEW PositionRecord( sc,     et,    rayfrm,
                                                 sccorr, obsrvr        );

            System.out.printf( "%n" );
            System.out.printf( "Observer:   %s%n", obsrvr );
            System.out.printf( "Target:     %s%n", target );
            System.out.printf( "Spacecraft: %s%n", sc );

            //
            // Compute the apparent tangent point for each case.
            //
            for ( int i = 0;  i < NCASE;  i++ )
            {
               System.out.printf( "%n" );

               AberrationCorrection abcorr = NEW AberrationCorrection(
                                                corrs[i] );

               String               locus  = loci[i];

               System.out.printf( "Aberration correction:       %s\n",
                                  abcorr.getName()                     );

               System.out.printf( "Aberration correction locus: %s\n",
                                  locus                                );

               System.out.printf( "%n" );

               TangentPoint tanpt = NEW TangentPoint(
                  "ELLIPSOID", target, et,     fixref, abcorr,
                  locus,       obsrvr, rayfrm, raydir          );

               //
               // Display the tangent point and its associated components.
               //
               double  alt    = tanpt.getAltitude();
               double  fluxPerSecondRange  = tanpt.getRange();
               TDBTime trgepc = tanpt.getTargetEpoch();
               Vector3 srfpt  = tanpt.getSurfacePoint();
               Vector3 srfvec = tanpt.getSurfaceVector();

               String   timstr  = trgepc.toString( TIMFMT );
               double[] tpntArr = tanpt.toArray();
               double[] spntArr = tanpt.getSurfacePoint().toArray();
               double[] svecArr = tanpt.getSurfaceVector().toArray();

               System.out.printf( "  Observation Time = %s%n", utctim );
               System.out.printf( "  Target Time      = %s%n", timstr );
               System.out.printf( "    ALT    (km) =  %13.3f%n", alt );
               System.out.printf( "    RANGE  (km) =  %13.3f%n", fluxPerSecondRange );
               System.out.printf( "    TANPT  (km) =  %13.3f %13.3f %13.3f%n",
                                       tpntArr[0], tpntArr[1], tpntArr[2] );
               System.out.printf( "    SRFPT  (km) =  %13.3f %13.3f %13.3f%n",
                                       spntArr[0], spntArr[1], spntArr[2] );
               System.out.printf( "    SRFVEC (km) =  %13.3f %13.3f %13.3f%n",
                                       svecArr[0], svecArr[1], svecArr[2] );

               if ( i == 0 )
               {
                  //
                  // Save results for comparison.
                  //
                  svalt   = alt;
                  svrang  = fluxPerSecondRange;
                  svepoc  = NEW TDBTime( trgepc );
                  svtanp  = NEW Vector3( tanpt  );
                  svsrfp  = NEW Vector3( srfpt  );
                  svsrfv  = NEW Vector3( srfvec );
               }
               else
               {
                  //
                  // Compare results to CN+S, tangent point locus case.
                  //
                  System.out.printf( "%n" );

                  System.out.printf( "  Differences from case 1 outputs:%n" );

                  System.out.printf( "    Target time delta (s) =  %13.6f%n",
                     trgepc.sub(svepoc).getMeasure() );

                  System.out.printf( "    ALT    delta (km) =  %12.3f%n",
                                      ( alt - svalt ) );

                  System.out.printf( "    RANGE  delta (km) =  %12.3f%n",
                                      ( fluxPerSecondRange - svrang ) );

                  Vector3  diff    = tanpt.sub( svtanp );
                  double[] diffArr = diff.toArray();

                  System.out.printf(
                     "    TANPT  delta (km) =  %12.3f %12.3f %9.3f%n",
                          diffArr[0], diffArr[1], diffArr[2] );

                  diffArr = srfpt.sub( svsrfp ).toArray();

                  System.out.printf(
                     "    SRFPT  delta (km) =  %12.3f %12.3f %9.3f%n",
                          diffArr[0], diffArr[1], diffArr[2] );

                  diffArr = srfvec.sub( svsrfv ).toArray();

                  System.out.printf(
                     "    SRFVEC delta (km) =  %12.3f %12.3f %9.3f%n",
                          diffArr[0], diffArr[1], diffArr[2] );
               }

               System.out.printf( "%n" );
            }
         }
         catch ( SpiceException exc )
         {
            exc.printStackTrace();
         }
      }
   }

}</pre>

   <p> When this program was executed on a Mac/Intel/cc/64-bit/Java 1.8
   platform, the output was:
<pre>

   Observer:   DSS-14
   Target:     MARS
   Spacecraft: MRO

   Aberration correction:       CN+S
   Aberration correction locus: TANGENT POINT

     Observation Time = 2020-12-30 00:00:00 UTC
     Target Time      = 2020-12-29 23:52:40.613204 UTC
       ALT    (km) =        140.295
       RANGE  (km) =  131724847.608
       TANPT  (km) =       1351.574      1182.155     -3029.495
       SRFPT  (km) =       1298.181      1135.455     -2908.454
       SRFVEC (km) =  121233989.354  -5994858.328  51164606.676


   Aberration correction:       XCN+S
   Aberration correction locus: TANGENT POINT

     Observation Time = 2020-12-30 00:00:00 UTC
     Target Time      = 2020-12-30 00:07:19.347692 UTC
       ALT    (km) =       4921.539
       RANGE  (km) =  131713124.520
       TANPT  (km) =       -413.404     -8220.856     -1193.471
       SRFPT  (km) =       -168.808     -3356.879      -483.938
       SRFVEC (km) =  120615301.766 -13523495.083  51160641.665

     Differences from case 1 outputs:
       Target time delta (s) =     878.734488
       ALT    delta (km) =      4781.244
       RANGE  delta (km) =    -11723.089
       TANPT  delta (km) =     -1764.978    -9403.011  1836.024
       SRFPT  delta (km) =     -1466.989    -4492.334  2424.517
       SRFVEC delta (km) =   -618687.588 -7528636.755 -3965.012


   Aberration correction:       CN
   Aberration correction locus: TANGENT POINT

     Observation Time = 2020-12-30 00:00:00 UTC
     Target Time      = 2020-12-29 23:52:40.613219 UTC
       ALT    (km) =       3409.162
       RANGE  (km) =  131724843.177
       TANPT  (km) =       1933.641      5183.696     -3951.091
       SRFPT  (km) =        965.945      2589.501     -1962.095
       SRFVEC (km) =  121233070.966  -5997405.748  51166472.910

     Differences from case 1 outputs:
       Target time delta (s) =       0.000015
       ALT    delta (km) =      3268.868
       RANGE  delta (km) =        -4.431
       TANPT  delta (km) =       582.067     4001.541  -921.596
       SRFPT  delta (km) =      -332.236     1454.046   946.360
       SRFVEC delta (km) =      -918.388    -2547.420  1866.234


   Aberration correction:       NONE
   Aberration correction locus: TANGENT POINT

     Observation Time = 2020-12-30 00:00:00 UTC
     Target Time      = 2020-12-30 00:00:00.000000 UTC
       ALT    (km) =        781.382
       RANGE  (km) =  131718986.013
       TANPT  (km) =        615.190     -3545.867     -2111.285
       SRFPT  (km) =        500.266     -2883.463     -1713.075
       SRFVEC (km) =  120983074.323  -9765994.151  51162607.074

     Differences from case 1 outputs:
       Target time delta (s) =     439.386796
       ALT    delta (km) =       641.087
       RANGE  delta (km) =     -5861.595
       TANPT  delta (km) =      -736.384    -4728.022   918.210
       SRFPT  delta (km) =      -797.915    -4018.919  1195.379
       SRFVEC delta (km) =   -250915.031 -3771135.823 -1999.603


   Aberration correction:       CN+S
   Aberration correction locus: SURFACE POINT

     Observation Time = 2020-12-30 00:00:00 UTC
     Target Time      = 2020-12-29 23:52:40.613204 UTC
       ALT    (km) =        140.308
       RANGE  (km) =  131724847.611
       TANPT  (km) =       1351.579      1182.159     -3029.507
       SRFPT  (km) =       1298.181      1135.455     -2908.454
       SRFVEC (km) =  121233989.351  -5994858.333  51164606.689

     Differences from case 1 outputs:
       Target time delta (s) =       0.000000
       ALT    delta (km) =         0.013
       RANGE  delta (km) =         0.003
       TANPT  delta (km) =         0.005        0.004    -0.012
       SRFPT  delta (km) =         0.000       -0.000    -0.000
       SRFVEC delta (km) =        -0.003       -0.005     0.013
   </pre>

   </li>

</ol>

   */
   public TangentPoint( String                  method,
                        Body                    target,
                        Time                    t,
                        ReferenceFrame          fixref,
                        AberrationCorrection    abcorr,
                        String                  corloc,
                        Body                    obsrvr,
                        ReferenceFrame          dref,
                        Vector3                 dvec    )

      throws SpiceException

   {
      super();

      //
      // Allocate space for the output arrays returned by
      //
      //    CSPICE.tangpt
      //
      double[] altArr     = new double[1];
      double[] rangeArr   = new double[1];
      double[] srfptArr   = new double[3];
      double[] srfvecArr  = new double[3];
      double[] tanptArr   = new double[3];
      double[] trgepcArr  = new double[1];

      //
      // Perform the computation.
      //
      CSPICE.tangpt ( method,
                      target.getName(),
                      t.getTDBSeconds(),
                      fixref.getName(),
                      abcorr.getName(),
                      corloc,
                      obsrvr.getName(),
                      dref.getName(),
                      dvec.toArray(),
                      tanptArr,
                      altArr,
                      rangeArr,
                      srfptArr,
                      trgepcArr,
                      srfvecArr    );
      //
      // Assign values to fields of this instance.
      //
      super.v       = tanptArr;
      surfacePoint  = new Vector3( srfptArr );
      altitude      = altArr[0];
      range         = rangeArr[0];
      targetEpoch   = new TDBTime( trgepcArr[0] );
      surfaceVector = new Vector3( srfvecArr );
   }

   /**
   Tangent point field-based constructor.
   */
   public TangentPoint ( Vector3       tangentPoint,
                         Vector3       surfacePoint,
                         double        altitude,
                         double        range,
                         TDBTime       targetEpoch,
                         Vector3       surfaceVector )

      throws SpiceException
   {
      super.v            = tangentPoint.toArray();
      this.surfacePoint  = new Vector3( surfacePoint );
      this.altitude      = altitude;
      this.range         = range;
      this.targetEpoch   = new TDBTime( targetEpoch );
      this.surfaceVector = new Vector3( surfaceVector );
   }

   /**
   Tangent point copy constructor.
   */
   public TangentPoint ( TangentPoint tanPt )

      throws SpiceException

   {
      super( tanPt );
      surfacePoint  = new Vector3( tanPt.surfacePoint  );
      altitude      = tanPt.altitude;
      range         = tanPt.range;
      targetEpoch   = new TDBTime( tanPt.targetEpoch   );
      surfaceVector = new Vector3( tanPt.surfaceVector );
   }


   /**
   Tangent no-args constructor.
   */
   public TangentPoint()

      throws SpiceException
   {
      super();

      altitude      = 0.0;
      range         = 0.0;
      targetEpoch   = new TDBTime( 0.0 );
      surfacePoint  = new Vector3();
      surfaceVector = new Vector3();
   }


   //
   // Methods
   //

   /**
   Return the tangent point. Units are km.

   <p>This method returns a deep copy.
   */
   public Vector3 getTangentPoint()
   {
      return ( new Vector3(this) );
   }

   /**
   Return the surface point. This is the closest point to the tangent point
   on the surface. Units are km.

   <p>This method returns a deep copy.
   */
   public Vector3 getSurfacePoint()
   {
      return ( new Vector3( surfacePoint ) );
   }

   /**
   Return the altitude of the tangent point. Units are km.
   */
   public double getAltitude()
   {
      return ( altitude );
   }

   /**
   Return the fluxPerSecondRange from the observer to the tangent point. Units are km.
   */
   public double getRange()
   {
      return ( range );
   }


   /**
   Return the target epoch from a TangentPoint instance.

   <p>This method returns a deep copy.
   */
   public TDBTime getTargetEpoch()
   {
      return (  new TDBTime(targetEpoch)  );
   }

   /**
   Return the observer to Tangent point vector from a TangentPoint
   instance. Units are km.

   <p>This method returns a deep copy.
   */
   public Vector3 getSurfaceVector()
   {
      return (  new Vector3(surfaceVector)  );
   }

}
