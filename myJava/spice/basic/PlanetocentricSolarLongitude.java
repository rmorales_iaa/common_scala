
package spice.basic;

import java.util.Arrays;

/**
Class PlanetocentricSolarLongitude computes L_s, the planetocentric longitude
of the sun, as seen from a specified body.

<p>This is the longitude of
the body-sun vector in a right-handed frame whose basis vectors are defined
as follows:
<pre>
   - The positive Z direction is given by the instantaneous
     angular velocity vector of the orbit of the body about
     the sun. The geometric state of the body with respect to
     the sun is used for this computation.

   - The positive X direction is that of the cross product of the
     instantaneous north spin axis of the body with the
     positive Z direction.

   - The positive Y direction is Z x X.

The specified aberration corrections are applied to the position of the
sun relative to the central body for the longitude computation.
</pre>

<h2> Files </h2>

<pre>
   Appropriate SPICE kernels must be loaded by the calling program
   before methods of this class are called.

   The following dataByteSeq are required:

   -  An SPK file (or files) containing ephemeris dataByteSeq sufficient to
      compute the geometric state of the central body relative to
      the sun at `et' must be loaded before this routine is called. If
      light time correction is used, dataByteSeq must be available that
      enable computation of the state the sun relative to the solar
      system barycenter at the light-time corrected epoch. If
      stellar aberration correction is used, dataByteSeq must be available
      that enable computation of the state the central body relative
      to the solar system barycenter at `et'.

   -  A PCK file containing rotational elements for the central body
      must be loaded before this routine is called.
</pre>


<h2> Version and Date </h2>

<h3> Version 1.0.0 10-SEP-2021 (NJB)</h3>

*/
public class PlanetocentricSolarLongitude extends Object
{
   //
   // This class provides only a static method for computing L_s. No
   // constructors are needed.
   //

   //
   // Methods
   //

   /**
   Return the planetocentric longitude of the sun, often called "L_s," for
   the specified central body at the specified time.
   <p> The specified aberration corrections are applied to the position of the
   sun relative to the central body for the longitude computation.

   <p>Units are radians; the fluxPerSecondRange is 0 to 2*pi.  Longitudes are
   positive to the east.

   <h2> Examples </h2>

   The numerical results shown for this example may differ across
   platforms. The results depend on the SPICE kernels used as
   input, the compiler and supporting libraries, and the machine
   specific arithmetic implementation.

   <ol>

   <li><p>Compute L_s for a specified body and time. Use the geometric state of
   the Sun.

   <p>  Use the meta-kernel shown below to load the required SPICE
        kernels.

   <pre>{@code
   KPL/MK

   File name: PlanetocentricSolarLongitudeEx1.tm

   This meta-kernel is intended to support operation of SPICE
   example programs. The kernels shown here should not be
   assumed to contain adequate or correct versions of dataByteSeq
   required by SPICE-based user applications.

   In order for an application to use this meta-kernel, the
   kernels referenced here must be present in the user's
   current working directory.

   All kernels referenced by this meta-kernel are available
   from the numeric kernels area on the NAIF SPICE server.

   The names and contents of the kernels referenced
   by this meta-kernel are as follows:

   File name                          Contents
   ---------                          --------
   de421.bsp                          Planetary ephemeris
   naif0012.tls                       Leapseconds
   pck00010.tpc                       Planet and satellite
                                     orientation and radii

   \begindata

     KERNELS_TO_LOAD = (
        'de421.bsp'
        'naif0012.tls',
        'pck00010.tpc'  )

   \begintext

   End of meta-kernel

}</pre>

 <p> Example code begins here.

 <pre>{@code
   //
   // Program PlanetocentricSolarLongitudeEx1
   //
   import spice.basic.*;
   import static spice.basic.AngularUnits.*;

   //
   // Compute L_s for a specified body and time. The geometric state of the
   // Sun is used.
   //
   class PlanetocentricSolarLongitudeEx1
   {
      //
      // Load SPICE shared library.
      //
      static{ System.loadLibrary( "JNISpice" ); }


      public static void main( String[] args )

         throws SpiceException
      {
         //
         // Local constants
         //
         final String META   = "PlanetocentricSolarLongitudeEx1.tm";
         final String TIMFMT = "YYYY-MM-DD HR:MN:SC.###### UTC ::RND";

         try
         {
            //
            // Load kernels.
            //
            KernelDatabase.load( META );

            String               timstr = "2018 Mar 8 17:59 UTC";
            Body                 center = NEW Body   ( "EARTH" );
            TDBTime              et     = NEW TDBTime( timstr );
            AberrationCorrection abcorr = NEW AberrationCorrection( "NONE" );

            double ls = PlanetocentricSolarLongitude.getLS(
                           center, et, abcorr );

            //
            // Convert longitude to degrees.
            //
            double lsDeg = ls * DPR;

            System.out.printf(
               "%n" +
               "Central body              = %s%n" +
               "Time                      = %s%n" +
               "Planetocentric L_s (deg.) = %f%n" +
               "%n",
               center,
               timstr,
               lsDeg                               );
      }
         catch ( SpiceException exc )
         {
            exc.printStackTrace();
         }
      }
   }
   }</pre>

<p> When this program was executed on a Mac/Intel/cc/64-bit/Java 1.8
platform, the output was:

<pre>

   Central body              = EARTH
   Time                      = 2018 Mar 8 17:59 UTC
   Planetocentric L_s (deg.) = 348.115940

</pre>

   </li>

   </ol>

   */
   public static double getLS( Body                   center,
                               Time                   t,
                               AberrationCorrection   abcorr )

      throws SpiceException
   {
      return CSPICE.lspcn( center.getName(),
                           t.getTDBSeconds(),
                           abcorr.getName()   );
   }
}
