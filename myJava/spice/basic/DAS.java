
package spice.basic;

import java.util.ArrayList;
import spice.basic.CSPICE;

/**
Class DAS supports creation of and low-level read
operations on DAS files.

<p>
This class supports DAS comment area read access.

<p>
See the subclass
{@link spice.basic.DSK} for methods used to write that type of file.

<p>
Normal read access of DSK files
requires that these files be loaded via
{@link spice.basic.KernelDatabase#load(java.lang.String)}. This method
plays the role of the routine FURNSH in SPICELIB.

<h3>Examples</h3>
TBD
<p>
The numerical results shown for this example may differ across
platforms. The results depend on the SPICE kernels used as
input, the compiler and supporting libraries, and the machine
specific arithmetic implementation.



<h3>Author_and_Version </h3>

<h3> Version 2.0.0 20-AUG-2021 (NJB) </h3>
<pre>
Added API methods

   addDoubleData
   addIntData
   getFileSummary
   getLastLogicalAddresses
   lowLevelClose
   openNew
   openScratch
   readDoubleData
   readIntData
   updateDoubleData
   updateIntData
   writeBufferedRecords

and utility method

   checkWritable

Added public fields

   CIDX
   DIDX
   IIDX

to simplify access to 3-element arrays returned by some DAS methods.
These arrays contain values applicable to the respective character,
double precision and integer logical arrays of a DAS file.

Bug fix: method openForWrite now sets the writable member to true.

Changed calls to checkAccess to calls to checkWriteAccess
in methods addComments and deleteComments.
</pre>

<p>Updated markup to work with javadoc version 8.*.


<h3> Version 1.0.0 10-DEC-2016 (NJB) </h3>


*/

public class DAS extends Object
{

   //
   // Public Fields
   //

   /**
   Index of character items in 3-element arrays returned by methods
   such as getLastLogicalAddresses:
   */
   public static final int CIDX = 0;

   /**
   Index of double precision items in 3-element arrays returned by methods
   such as getLastLogicalAddresses:
   */
   public static final int DIDX = 1;

   /**
   Index of integer items in 3-element arrays returned by methods
   such as getLastLogicalAddresses:
   */
   public static final int IIDX = 2;

   //
   // Protected Fields
   //
   protected String        fileName;
   protected int           handle;
   protected boolean       readable;
   protected boolean       writable;


   //
   // Nested classes
   //

   //
   // Class FileRecord centralizes file record fetch
   // actions.
   //
   private class FileRecord
   {
      //
      // Fields
      //
      String   idword;
      String   ifname;
      int      nresvr;
      int      nresvc;
      int      ncomr;
      int      ncomc;

      //
      // Constructors
      //
      private FileRecord ( DAS das )

         throws SpiceException
      {
         //
         // Local variables
         //
         String[] idwordArray = new String[1];
         String[] ifnameArray = new String[1];
         int[]    nresvrArray = new int[1];
         int[]    nresvcArray = new int[1];
         int[]    ncomrArray  = new int[1];
         int[]    ncomcArray  = new int[1];

         //
         // Fetch file record parameters from the DAS file.
         //
         CSPICE.dasrfr( das.handle,  idwordArray, ifnameArray,
                        nresvrArray, nresvcArray, ncomrArray,  ncomcArray );

         idword = idwordArray[0];
         ifname = ifnameArray[0];
         nresvr = nresvrArray[0];
         nresvc = nresvcArray[0];
         ncomr  = ncomrArray[0];
         ncomc  = ncomcArray[0];
      }
   }

   //
   // Constructors
   //

   /**
   Construct a DAS instance representing a file.
   The file need not exist.

   <p> User applications will not need to call this
   constructor directly. See the methods {@link #openForRead}
   and {@link #openForWrite}.
   */
   protected DAS ( String fileName )
   {
      this.fileName    = fileName;
      handle           = 0;
      readable         = false;
      writable         = false;
   }


   /**
   Construct a DAS instance from handle of an open DAS file.
   */
   public DAS( int  handle )

      throws SpiceException
   {
      //
      // Map the input handle to a file name. If the
      // handle is invalid (for example, if it's stale),
      // an exception will be thrown.
      //
      this.fileName = CSPICE.dashfn( handle );
      this.handle   = handle;

      //
      // Any DAS file that has a valid handle is readable.
      //
      readable = true;

      //
      // Handles of writable DAS files are negative.
      //
      writable = (handle < 0);
   }


   /**
   Copy constructor. This constructor creates a deep copy.
   */
   public DAS ( DAS das )
   {
      this.fileName    = new String(das.fileName);
      handle           = das.handle;
      readable         = das.readable;
      writable         = das.writable;
   }


   /**
   No-args constructor.
   */
   public DAS ()
   {
   }


   //
   // Static Methods
   //




   /**
   Open a DAS file for read access.
   */
   public static DAS openForRead( String fileName )

      throws SpiceException
   {
      //
      // Create a NEW DAS.
      //
      DAS d = new DAS( fileName );

      d.handle   = CSPICE.dasopr( fileName );

      d.readable = true;

      return ( d );
   }


   /**
   Open a DAS file for write access.
   */
   public static DAS openForWrite( String fileName )

      throws SpiceException
   {

      //
      // Create a NEW DAS.
      //
      DAS d = new DAS( fileName );

      d.handle   = CSPICE.dasopw( fileName );

      d.readable = true;
      d.writable = true;

      return ( d );
   }

   /**
   Open a NEW DAS file.
   <pre>{@code

   The ID word of the file will be

      DAS/<fileType>

   where fileType may be up to four characters.

   The internal file name may be up to 60 characters.
   }
   </pre>
   */
   public static DAS openNew( String fileName,
                              String fileType,
                              String internalFileName,
                              int    nComRecords      )

      throws SpiceException
   {
      //
      // Create a NEW DAS.
      //
      DAS d = new DAS( fileName );

      d.handle = CSPICE.dasonw( fileName,         fileType,
                                internalFileName, nComRecords );

      d.writable = true;
      d.readable = true;

      return ( d );
   }


   /**
   Open a scratch DAS file for write access.
   */
   public static DAS openScratch()

      throws SpiceException
   {
      //
      // Create a scratch DAS file using class CSPICE. We'll need the
      // scratch file to exist in order to get its file name.
      //
      int h = CSPICE.dasops();

      //
      // The returned file name could be empty. This is allowed.
      //
      String fileName = CSPICE.dashfn( h );

      //
      // Now we can create a DAS instance.
      //
      DAS d = new DAS( fileName );

      d.handle   = h;
      d.readable = true;
      d.writable = true;

      return ( d );
   }


   //
   // Instance Methods
   //

   /**
   Add character dataByteSeq.

   <p>The dataByteSeq are written as a two-dimensional array of bytes. This allows
   storage and retrieval of arbitrary bit patterns.

   <pre>
   `n`       is the total number of bytes to be written. `n` may be less than
             the capacity of the subset of the input array specified by
             `bpos`, `epos`, and the number of rows of `dataByteSeq`.

   `bpos`,
   `epos`    are 0-based begin and end byte indices that define a sub-fluxPerSecondRange
             of the of bytes in the input array. This routine writes bytes
             from the specified sub-fluxPerSecondRange of the rows of `dataByteSeq` to the
             specified DAS file.

   `dataByteSeq`    is a two-dimensional byte array, a portion of which is to be
             added to the specified DAS file. The first `n' bytes of the
             sub-ranges of rows

                dataByteSeq[i][bpos:epos],    i = 0, ...

             are appended to the character dataByteSeq in the file. The order of
             bytes in the input array rows is considered to increase from
             left to right within each element of `dataByteSeq', and to increase
             with the indices of the elements of `dataByteSeq'.
   </pre>
   */
   public void addCharacterData( int        n,
                                 int        bpos,
                                 int        epos,
                                 byte[][]   data )

      throws SpiceException
   {
      checkWriteAccess();

      CSPICE.dasadc( this.handle, n, bpos, epos, data );
   }

   /**
   Add double precision dataByteSeq.
   */
   public void addDoubleData( double[] data )

      throws SpiceException
   {
      checkWriteAccess();

      CSPICE.dasadd( this.handle, data );
   }


   /**
   Add integer dataByteSeq.
   */
   public void addIntData( int[] data )

      throws SpiceException
   {
      checkWriteAccess();

      CSPICE.dasadi( this.handle, data );
   }



   /**
   Read character dataByteSeq from a fluxPerSecondRange of DAS character addresses.

   <p>The dataByteSeq are read as a two-dimensional array of bytes. This allows
   storage and retrieval of arbitrary bit patterns.

   <p>The DAS addresses `first` and `last` bound the fluxPerSecondRange of values to be
   read. DAS addresses are 1-based.

   <p>For each row in the array `cdata`, dataByteSeq are read into only the bytes
   in positions `bpos` through `epos`. These bounds are 0-based.
   */
   public void readCharacterData( int       first,
                                  int       last,
                                  int       bpos,
                                  int       epos,
                                  byte[][]  cdata  )

      throws SpiceException
   {
      checkAccess();


      CSPICE.dasrdc( this.handle, first, last, bpos, epos, cdata );
   }



   /**
   Read double precision dataByteSeq.
   */
   public double[] readDoubleData( int  first,
                                   int  last   )

      throws SpiceException
   {
      checkAccess();

      return CSPICE.dasrdd( this.handle, first, last );
   }

   /**
   Read integer dataByteSeq.
   */
   public int[] readIntData( int  first,
                             int  last   )

      throws SpiceException
   {
      checkAccess();

      return CSPICE.dasrdi( this.handle, first, last );
   }

   /**
   Update character dataByteSeq in a fluxPerSecondRange of DAS logical addresses.

   <p>The dataByteSeq are to be written are stored as a two-dimensional array of
   bytes. This allows storage and retrieval of arbitrary bit patterns.

   <p>The DAS addresses `first` and `last` bound the fluxPerSecondRange of values to be
   read. DAS addresses are 1-based.

   <p>For each row in the array `cdata`, dataByteSeq are read written only from
   the bytes in positions `bpos` through `epos`. These bounds are 0-based.
   */
   public void updateCharacterData( int       first,
                                    int       last,
                                    int       bpos,
                                    int       epos,
                                    byte[][]  cdata  )
      throws SpiceException
   {
      checkWriteAccess();

      CSPICE.dasudc( this.handle, first, last, bpos, epos, cdata );
   }



   /**
   Update double precision dataByteSeq.
   */
   public void updateDoubleData( int       first,
                                 int       last,
                                 double[]  data  )

      throws SpiceException
   {
      checkWriteAccess();

      CSPICE.dasudd( this.handle, first, last, data );
   }

   /**
   Update integer dataByteSeq.
   */
   public void updateIntData( int    first,
                              int    last,
                              int[]  data  )

      throws SpiceException
   {
      checkWriteAccess();

      CSPICE.dasudi( this.handle, first, last, data );
   }




   /**
   Get file handle.
   */
   public int getHandle()

      throws SpiceException
   {
      checkAccess();

      return( handle );
   }

   /**
   Return the file name.
   */
   public String getFileName()

      throws SpiceException
   {
      if ( fileName == null )
      {
         SpiceException exc

           = new SpiceException ( "File name is null." );

         throw( exc );
      }

      return( fileName );
   }

   /**
   Get last logical addresses of each dataByteSeq type.

   <p> The output array contains the last logical addresses of the character,
   double precision, and integer logical arrays, in that order.
   */
   public int[] getLastLogicalAddresses()

      throws SpiceErrorException
   {
      return CSPICE.daslla( handle );
   }



   /**
   Indicate whether a DAS file is readable.

   <p> A DAS file is readable if it has been opened
   for read OR write access.
   */
   public boolean isReadable()
   {
      return( readable );
   }

   /**
   Indicate whether a DAS file is writable.
   */
   public boolean isWritable()
   {
      return( writable );
   }

   /**
   Get the file summary of a DAS file.

   <p> The file must be open for read or write access.
   */
   public DASFileSummary getFileSummary()

      throws SpiceErrorException
   {
      return new DASFileSummary( this.handle );
   }


   /**
   Close a specified DAS file, thereby freeing resources.
   */
   public void close()

      throws SpiceException
   {
      CSPICE.dascls( handle );

      //
      // This DAS is no longer readable or writable.
      //

      readable = false;
      writable = false;
   }


   /**
   Write to a specified writable DAS file all buffered records associated with
   that file.
   */
   public void writeBufferedRecords()

      throws SpiceException
   {
      CSPICE.daswbr( handle );
   }


   /**
   Close a specified DAS file without writing buffered records or
   segregating the file.

   <p> Note that, in most cases, the caller must write buffered records
   before calling this method. Leaving records buffered after the DAS file
   is closed may cause errors later, since the DAS subsystem may attempt to
   write those records in order to make room in its buffers.
   */
   public void lowLevelClose()

      throws SpiceException
   {
      CSPICE.dasllc( handle );

      //
      // This DAS is no longer readable or writable.
      //
      readable = false;
      writable = false;
   }



   /**
   Append comments to the comment area of a DAS file.
   */
   public void addComments( String[] commentBuffer )

      throws SpiceException
   {
      checkWriteAccess();

      CSPICE.dasac( handle, commentBuffer );
   }



   /**
   Delete comments from a DAS file.
   */
   public void deleteComments()

      throws SpiceException
   {
      checkWriteAccess();

      CSPICE.dasdc( handle );
   }



   /**
   Read comments from an existing DAS file.
   */
   public String[] readComments( int  lineLength )

      throws SpiceException
   {
      //
      // Local constants
      //
      final int NLINES = 20;

      checkAccess();


      //
      // We'll accumulate comments from the DAS file in an ArrayList
      // of Strings. This allows us to process up a lsst of
      // arbitrary size.
      //
      ArrayList<String> alist  = new ArrayList<String>( NLINES );



      int[]     n      = new int[1];
      String[]  combuf = new String[NLINES];
      boolean[] done   = new boolean[1];

      //
      // Fetch comments into a buffer of size NLINES.
      // Continue until `done' indicates there are no
      // more comments to fetch.
      //

      CSPICE.dasec ( handle,
                     NLINES,
                     lineLength,
                     n,
                     combuf,
                     done       );

      for ( int i = 0;  i < n[0];  i++ )
      {
         alist.add( combuf[i] );
      }

      while ( !done[0] )
      {
         CSPICE.dasec ( handle,
                        NLINES,
                        lineLength,
                        n,
                        combuf,
                        done       );

         for ( int i = 0;  i < n[0];  i++ )
         {
            alist.add( combuf[i] );
         }
      }


      //
      // Extract the comments from the ArrayList into
      // an array of Strings.
      //
      String[] retbuf = alist.toArray( new String[0] );


      return ( retbuf );
   }


   /**
   Get the internal file name from a DAS file.

   <p> This method initializes the internal file name
   field of the DAS instance and returns a deep copy
   of the name.
   */
   public String getInternalFileName()

      throws SpiceException
   {
      checkAccess();

      //
      // Create a FileRecord instance for this file.
      //
      FileRecord fr = new FileRecord( this );

      return (  new String( fr.ifname )  );
   }



   /**
   Get the number of comment records in a DAS file.
   */
   public int getCommentRecordCount()

      throws SpiceException
   {
      FileRecord fr = new FileRecord( this );

      return ( fr.ncomr );
   }


   /**
   Get the number of comment characters in a DAS file.
   */
   public int getCommentCharacterCount()

      throws SpiceException
   {
      FileRecord fr = new FileRecord( this );

      return ( fr.ncomc );
   }


   /**
   Helper method for diagnosing improper file access.
   This method centralizes error handling for cases
   where access to a closed file is requested.
   */
   private void checkAccess()

      throws SpiceErrorException
   {
      if (  (!readable)  &&  (!writable)  )
      {
         String excMsg;

         excMsg = String.format ( " File %s is closed. The file " +
                                  "must be opened for read or "   +
                                  "write access in order to "     +
                                  "perform the requested "        +
                                  "operation",  fileName            );

         SpiceErrorException exc

            = SpiceErrorException.create ( "DAS.checkAccess()",
                                           "SPICE(DASFILECLOSED)",
                                           excMsg                 );

         throw ( exc );
      }
   }

   /**
   Helper method for checking write access.
   */
   private void checkWriteAccess()

      throws SpiceErrorException
   {
      if ( !writable )
      {
         String excMsg;

         excMsg = String.format ( " File %s is not writable. The file " +
                                  "must be opened for write access in " +
                                  "order to perform the requested "     +
                                  "operation",  fileName                 );

         SpiceErrorException exc

            = SpiceErrorException.create ( "DAS.checkWriteAccess()",
                                           "SPICE(DASNOTWRITABLE)",
                                           excMsg                  );
         throw ( exc );
      }
   }
}
