

package spice.basic;


/**
Class CK provides methods for writing, summarizing,
and conducting low-level read operations on CK files.

<p>
To make CK dataByteSeq available to programs for frame
transformations, use the method
{@link spice.basic.KernelDatabase#load(java.lang.String)}.


<h3>Version 2.0.0 22-SEP-2021 (NJB)</h3>

<p> Added methods

<pre>
   getRecordArrayType02
   getRecordArrayType03
   getRecordCountType02
   getRecordCountType02
</pre>

<p>Deleted unnecessary assignment in method openNew.

<p>Updated markup to work with javadoc version 8.*.


<h3> Version 1.0.0 04-JAN-2010 (NJB) </h3>
*/
public class CK extends DAF
{

   //
   // Private methods
   //

   /**
   Construct a CK instance representing a file.
   The file need not exist.
   */


   private CK ( String fileName )
   {
      super ( fileName );
   }


   /**
   Open a NEW CK file. The file must not exist prior
   to this method call.
   */
   private void openNew( String         internalFileName,
                         int            nCommentChars )

      throws SpiceException
   {
      this.handle = CSPICE.ckopn ( this.fileName,
                                   internalFileName,
                                   nCommentChars     );

      this.internalFileName = internalFileName;
      ND                    =  2;
      NI                    =  6;
      readable              = false;
      writable              = true;
   }


   //
   // Static methods
   //

   /**
   Create a NEW CK file. The file must not exist prior
   to this method call.
   */
   public static CK openNew ( String         fileName,
                              String         internalFileName,
                              int            nCommentChars     )
      throws SpiceException
   {
      CK ck = new CK( fileName );

      ck.openNew ( internalFileName, nCommentChars );

      return ( ck );
   }


   /**
   Open an existing CK file for read access.
   */
   public static CK openForRead( String fileName )

      throws SpiceException
   {
      CK ck = new CK( fileName );

      DAF daf = DAF.openForRead( fileName );

      ck.handle           = daf.getHandle();
      ck.internalFileName = daf.getInternalFileName();
      ck.ND               = daf.getND();
      ck.NI               = daf.getNI();
      ck.readable         = true;
      ck.writable         = false;

      return ( ck );
   }




   /**
   Open an existing CK for write access.

   <p> Note that a CK cannot be opened for write access
   if it has already been opened for read access.
   */
   public static CK openForWrite( String fileName )

      throws SpiceException
   {
      //
      // The following variables are declared as 1-dimensional
      // arrays so that they can serve as output arguments
      // in a call to CSPICE.dafrfr:
      //
      String[]          ifname = new String[1];
      int[]             fward  = new int[1];
      int[]             bward  = new int[1];
      int[]             free   = new int[1];
      int[]             nd     = new int[1];
      int[]             ni     = new int[1];

      //
      // Create a NEW CK instance.
      //
      CK ck = new CK( fileName );

      //
      // If the file is already open for read access, the following
      // call will fail.
      //
      ck.handle = CSPICE.dafopw( fileName );

      //
      // Obtain the DAF descriptor parameters ND and NI,
      // since these will be used for descriptor unpacking.
      //
      CSPICE.dafrfr ( ck.handle, nd, ni, ifname, fward, bward, free );

      ck.ND               = nd[0];
      ck.NI               = ni[0];

      ck.internalFileName = ifname[0];

      ck.writable         = true;

      return ( ck );
   }



   //
   // Constructors
   //




   //
   // Instance methods
   //

   /**
   Obtain a set of ID codes of objects for which a CK file
   contains dataByteSeq.
   */
   public int[] getInstruments()

      throws SpiceException
   {
      int   size       = this.countSegments();

      int[] initialSet = new int[0];

      int[] objectSet = CSPICE.ckobj( fileName, size, initialSet );

      return ( objectSet );
   }


   /**
   Obtain a set of ID codes of objects for which a CK file
   contains dataByteSeq, merged with an existing set of ID codes.

   <p> The result is returned in a NEW set.
   */
   public int[] getInstruments( int[] initialSet )

      throws SpiceException
   {
      int   size      = this.countSegments();

      int[] objectSet = CSPICE.ckobj( fileName, size + initialSet.length,
                                      initialSet );

      return ( objectSet );
   }

   /**
   Obtain a SpiceWindow representing the time coverage provided by
   this CK for a given instrument.
   */
   public SpiceWindow getCoverage( Instrument    instrument,
                                   boolean       needav,
                                   String        level,
                                   SCLKDuration  tol,
                                   TimeSystem    timsys,
                                   int           nintvls    )
      throws SpiceException
   {
      //
      // Validate tolerance: the SCLK associated with the
      // tolerance must match that associated with the
      // instrument.
      //

      if (     instrument.getSCLK().getIDCode()
               !=  tol.getSCLK().getIDCode()     )
      {
         String msg = "SCLK associated with Instrument " +
                      instrument.getIDCode() + " is " +
                      instrument.getSCLK().getIDCode() + " " +
                      "while SCLK associated with the input " +
                      "tolerance is " + tol.getSCLK().getIDCode();

         SpiceException exc = SpiceErrorException.create(

            "CK.getCoverage", "SPICE(CLOCKMISMATCH)", msg );

         throw ( exc );
      }

      double[] initialWindow = new double[0];

      double[] coverArray = CSPICE.ckcov( fileName,
                                          instrument.getIDCode(),
                                          needav,
                                          level,
                                          tol.getMeasure(),
                                          timsys.toString(),
                                          2*nintvls,
                                          initialWindow       );

      SpiceWindow cover = new SpiceWindow( coverArray );

      return ( cover );
   }



   /**
   Obtain a SpiceWindow representing the time coverage provided by
   this CK for a given instrument; return the union of this window
   with a pre-existing coverage window.

   <p> Note that the resulting window will be meaningful only if
   the input window is compatible with the requested coverage
   representation for this CK instance: specifically, the instruments,
   coverage levels, tolerances, time systems, and "need angular velocity"
   flags must match.
   */
   public SpiceWindow getCoverage( Instrument    instrument,
                                   boolean       needav,
                                   String        level,
                                   SCLKDuration  tol,
                                   TimeSystem    timsys,
                                   SpiceWindow   cover,
                                   int           nintvls    )
      throws SpiceException
   {
      //
      // Validate tolerance: the SCLK associated with the
      // tolerance must match that associated with the
      // instrument.
      //

      if (     instrument.getSCLK().getIDCode()
               !=  tol.getSCLK().getIDCode()     )
      {
         String msg = "SCLK associated with Instrument " +
                      instrument.getIDCode() + " is " +
                      instrument.getSCLK().getIDCode() + " " +
                      "while SCLK associated with the input " +
                      "tolerance is " + tol.getSCLK().getIDCode();
         SpiceException exc = SpiceErrorException.create(

            "CK.getCoverage", "SPICE(CLOCKMISMATCH)", msg );

         throw ( exc );
      }

      double[] initialWindow = cover.toArray();

      double[] coverArray = CSPICE.ckcov( fileName,
                                          instrument.getIDCode(),
                                          needav,
                                          level,
                                          tol.getMeasure(),
                                          timsys.toString(),
                                          2*nintvls,
                                          initialWindow       );

      SpiceWindow outCover = new SpiceWindow( coverArray );

      return ( outCover );
   }



   /**
   Write a type 2 segment to a CK file.
   */
   public void writeType02Segment ( Time               first,
                                    Time               last,
                                    Instrument         inst,
                                    ReferenceFrame     frame,
                                    String             segid,
                                    Time[]             startTimes,
                                    Time[]             stopTimes,
                                    SpiceQuaternion[]  quats,
                                    Vector3[]          avvs,
                                    double[]           rates     )


      throws SpiceException
   {
      SCLK             clock       = inst.getSCLK();

      int              nrec        = startTimes.length;

      int              to;

      double           firstSCLK;
      double           lastSCLK;

      double[]         avArray     = new double[3 * nrec];
      double[]         quatArray   = new double[4 * nrec];
      double[]         startArray  = new double[nrec];
      double[]         stopArray   = new double[nrec];


      //
      // Create a one-dimensional quaternion array, since this is
      // what CSPICE.ckw02 expects.
      //
      to = 0;

      for ( int i = 0;  i < nrec;  i++ )
      {
         System.arraycopy ( quats[i].toArray(), 0, quatArray, to, 4 );

         to += 4;
      }

      //
      // Same deal for angular velocity.
      //
      to = 0;

      for ( int i = 0;  i < nrec;  i++ )
      {
         System.arraycopy ( avvs[i].toArray(), 0, avArray, to, 3 );

         to += 3;
      }


      //
      // Copy the interval start and stop tick values to respective arrays.
      //
      for ( int i = 0;  i < nrec;  i++ )
      {
         startArray[i] = ( new SCLKTime(clock, startTimes[i]) ).
                               getContinuousTicks();
         stopArray [i] = ( new SCLKTime(clock, stopTimes [i]) ).
                               getContinuousTicks();
      }

      firstSCLK = ( new SCLKTime(clock, first) ).getContinuousTicks();
      lastSCLK  = ( new SCLKTime(clock, last ) ).getContinuousTicks();

      CSPICE.ckw02 ( this.handle,
                     firstSCLK,
                     lastSCLK,
                     inst.getIDCode(),
                     frame.getName(),
                     segid,
                     nrec,
                     startArray,
                     stopArray,
                     quatArray,
                     avArray,
                     rates            );
   }






   /**
   Write a type 3 segment to a CK file.
   */
   public void writeType03Segment ( Time               first,
                                    Time               last,
                                    Instrument         inst,
                                    ReferenceFrame     frame,
                                    boolean            avflag,
                                    String             segid,
                                    Time[]             timeTags,
                                    SpiceQuaternion[]  quats,
                                    Vector3[]          avvs,
                                    Time[]             startTimes   )


      throws SpiceException
   {
      SCLK             clock       = inst.getSCLK();

      int              nrec        = timeTags.length;
      int              nints       = startTimes.length;

      int              to;

      double           firstSCLK;
      double           lastSCLK;

      double[]         avArray     = new double[3 * nrec];
      double[]         quatArray   = new double[4 * nrec];
      double[]         sclkdpArray = new double[nrec];
      double[]         startArray  = new double[nints];

      //
      // Create a one-dimensional quaternion array, since this is
      // what CSPICE.ckw03 expects.
      //
      to = 0;

      for ( int i = 0;  i < nrec;  i++ )
      {
         System.arraycopy ( quats[i].toArray(), 0, quatArray, to, 4 );

         to += 4;
      }

      //
      // Same deal for angular velocity.
      //
      if ( avflag )
      {
         to = 0;

         for ( int i = 0;  i < nrec;  i++ )
         {
            System.arraycopy ( avvs[i].toArray(), 0, avArray, to, 3 );

            to += 3;
         }
      }


      //
      // Copy the tick values to an array.
      //
      for ( int i = 0;  i < nrec;  i++ )
      {
         sclkdpArray[i] = ( new SCLKTime(clock, timeTags[i]) ).
                                getContinuousTicks();
      }

      //
      // Copy the interval start times to an array.
      //
      for ( int i = 0;  i < nints;  i++ )
      {
         startArray[i] = ( new SCLKTime(clock, startTimes[i]) ).
                               getContinuousTicks();
      }

      firstSCLK = ( new SCLKTime(clock, first) ).getContinuousTicks();
      lastSCLK  = ( new SCLKTime(clock, last ) ).getContinuousTicks();

      CSPICE.ckw03 ( this.handle,
                     firstSCLK,
                     lastSCLK,
                     inst.getIDCode(),
                     frame.getName(),
                     avflag,
                     segid,
                     nrec,
                     sclkdpArray,
                     quatArray,
                     avArray,
                     nints,
                     startArray    );
   }


   /**
   Count the segments in a CK file.
   */
   public int countSegments()

      throws SpiceException
   {
      boolean found;
      int     n = 0;


      this.beginForwardSearch();

      found = this.findNextArray();

      while ( found )
      {
         ++n;

         found = this.findNextArray();
      }

      return ( n );
   }


   /**
   Get record count from a specified type 02 segment.
   */
   public int getRecordCountType02 ( CKDescriptor   descr )

      throws SpiceErrorException
   {
      return CSPICE.cknr02( handle, descr.toArray() );
   }

   /**
   Get record count from a specified type 03 segment.
   */
   public int getRecordCountType03 ( CKDescriptor   descr )

      throws SpiceErrorException
   {
      return CSPICE.cknr03( handle, descr.toArray() );
   }


   /**
   Get specified record from a type 02 segment.

   The contents of the record are as follows:

      record[ 0 ] = start SCLK time of interval
      record[ 1 ] = end SCLK time of interval
      record[ 2 ] = TDB seconds per tick rate

      record[ 3 ] = q0
      record[ 4 ] = q1
      record[ 5 ] = q2
      record[ 6 ] = q3

      record[ 7 ] = av0
      record[ 8 ] = av1
      record[ 9 ] = av2

   Angular velocity has unit of radians/second.

   */
   public double[] getRecordArrayType02 ( CKDescriptor   descr,
                                          int            recno )

      throws SpiceErrorException
   {
      return CSPICE.ckgr02( handle, descr.toArray(), recno );
   }


   /**
   Get specified record from a type 03 segment.

   The contents of the record are as follows:

      record[ 0 ] = clkout

      record[ 1 ] = q0
      record[ 2 ] = q1
      record[ 3 ] = q2
      record[ 4 ] = q3

      record[ 5 ] = av0  |
      record[ 6 ] = av1  |-- returned optionally
      record[ 7 ] = av2  |

   The angular velocity elements of the returned array are undefined if the
   segment doesn't contain angular velocity dataByteSeq.

   Angular velocity has unit of radians/second.

   */
   public double[] getRecordArrayType03 ( CKDescriptor   descr,
                                          int            recno )

      throws SpiceErrorException
   {
      return CSPICE.ckgr03( handle, descr.toArray(), recno );
   }


}
