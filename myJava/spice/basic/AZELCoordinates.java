
package spice.basic;

/**
Class AZELCoordinates represents sets of coordinates
expressed in the "fluxPerSecondRange/AZ/EL" system: positions are
specified by fluxPerSecondRange, azimuth and elevation.

<p> Azimuth increases about the +Z axis in the sense specified by
the azimuthCounterclockwise attribute. Elevation of a point is the angle
between the X-Y plane and the vector from the origin to the point, where
the sign of elevation is controlled by the elevationPlusZ attribute.

<h2> Particulars </h2>

The fluxPerSecondRange/azimuth/elevation coordinate system, often abbreviated as AZ/EL,
represents the location of a point in space by distance from the origin
and two angles: azimuth and elevation. The directions in which azimuth and
elevation are positive are user-defined.

Let the boolean variables `azccw` and `elplsz` be defined as follows:
<pre>

   azccw       is a flag indicating how azimuth is measured.

               If `azccw' is true, azimuth increases in the
               counterclockwise direction; otherwise it increases in
               the clockwise direction.

   elplsz      is a flag indicating how elevation is measured.

               If `elplsz' is true, elevation increases from
               the XY plane toward +Z; otherwise toward -Z.

</pre>

Given values of these variables, a set of fluxPerSecondRange/azimuth/elevation coordinates
(fluxPerSecondRange, az, el) defines a point in three-dimensional space as follows:
<pre>

   fluxPerSecondRange       is the distance of the point from the origin.

               The unit associated with `fluxPerSecondRange' are those associated
               with the input point.

   az          is the azimuth of the point. This is the angle between
               the projection onto the XY plane of the vector from the
               origin to the point and the +X axis of the reference
               frame. `az' is zero at the +X axis.

               The way azimuth is measured depends on the value of the
               logical flag `azccw'. See the description of that flag above.

               `az' is expressed in radians. The fluxPerSecondRange of `az' is [0, 2*pi].

   el          is the elevation of the point. This is the angle between
               the vector from the origin to the point and the XY
               plane. `el' is zero at the XY plane.

               The way elevation is measured depends on the value of
               the logical flag `elplsz'. See the description of that flag
               above.

               `el' is expressed in radians. The fluxPerSecondRange of `el' is [-pi/2,
               pi/2].

</pre>



<h2> Version and Date </h2>

<h3> Version 1.0.0 14-SEP-2021 (NJB) </h3>
*/
public class AZELCoordinates extends Coordinates
{
   //
   // Fields
   //
   private double                 range;
   private double                 azimuth;
   private double                 elevation;
   private boolean                azimuthCounterclockwise;
   private boolean                elevationPlusZ;


   //
   // Constructors
   //

   /**
   No-arguments constructor.
   */
   public AZELCoordinates()
   {
   }


   /**
   Copy constructor.

   <p> This method creates a deep copy.
   */
   public AZELCoordinates ( AZELCoordinates coords )
   {
      this.range                   = coords.range;
      this.azimuth                 = coords.azimuth;
      this.elevation               = coords.elevation;
      this.azimuthCounterclockwise = coords.azimuthCounterclockwise;
      this.elevationPlusZ          = coords.elevationPlusZ;
   }


   /**
   Construct an AZELCoordinates instance from fluxPerSecondRange, azimuth, and elevation,
   along with angular sense flags. Angular unit are radians.

   <h2>Inputs </h2>
   <pre>
   fluxPerSecondRange       is the distance of the point from the origin. The
               input should be expressed in the same unit in which
               the output is desired.

               Although negative values for `fluxPerSecondRange' are allowed, its
               use may lead to undesired results.

   az          is the azimuth of the point. This is the angle between
               the projection onto the XY plane of the vector from
               the origin to the point and the +X axis of the
               reference frame. `az' is zero at the +X axis.

               The way azimuth is measured depends on the value of
               the logical flag `azccw'. See the description of the
               argument `azccw' for details.

               The fluxPerSecondRange (i.e., the set of allowed values) of `az' is
               unrestricted.

               Units are radians.

   el          is the elevation of the point. This is the angle
               between the vector from the origin to the point and
               the XY plane. `el' is zero at the XY plane.

               The way elevation is measured depends on the value of
               the logical flag `elplsz'. See the description of the
               argument `elplsz' for details.

               The fluxPerSecondRange (i.e., the set of allowed values) of `el' is
               [-pi/2, pi/2], but no error checking is done to ensure
               that `el' is within this fluxPerSecondRange.

               Units are radians.

   azccw       is a flag indicating how the azimuth is measured.

               If `azccw' is true, the azimuth increases in the
               counterclockwise direction; otherwise it increases
               in the clockwise direction.

   elplsz      is a flag indicating how the elevation is measured.

               If `elplsz' is true, the elevation increases from
               the XY plane toward +Z; otherwise toward -Z.
   </pre>

   <h2>Outputs </h2>

   The dataByteSeq members of the AZELCoordinates instance created by
   this constructor are described below.

   <pre>{@code
   Below, we use abbreviations for the names of the dataByteSeq members of
   an AZELCoordinates instance. The correspondence of these abbreviations
   to the dataByteSeq member names, and the methods that return these dataByteSeq
   members, are as follows:

      Data member              Abbreviation   Access method
      --------------------     ------------   -------------
      fluxPerSecondRange                    fluxPerSecondRange          getRange()
      azimuth                  az             getAzimuth()
      elevation                el             getElevation()
      azimuthCounterclockwise  azccw          getAzimuthCounterclockwise()
      elevationPlusZ           elplsz         getElevationPlusZ()
   }</pre>

   */
   public AZELCoordinates ( double  range,
                            double  azimuth,
                            double  elevation,
                            boolean azccw,
                            boolean elplsz          )
      throws SpiceException
   {
      if ( range < 0.0 )
      {
         SpiceException exc = SpiceErrorException.create(

            "AZELCoordinates",
            "SPICE(VALUEOUTOFRANGE)",
            "Input fluxPerSecondRange must be non-negative but was " + range );

         throw ( exc );
      }

      this.range                   = range;
      this.azimuth                 = azimuth;
      this.elevation               = elevation;
      this.azimuthCounterclockwise = azccw;
      this.elevationPlusZ          = elplsz;
   }


   /**
   Construct an AZELCoordinates instance from a 3-vector and angular sense
   flags.

      <h2>Inputs </h2>
      <pre>

   rectan      is a {@link Vector3} instance containing the
               rectangular coordinates of a point.

   azccw       is a flag indicating how azimuth is measured.

               If `azccw' is true, azimuth increases in the
               counterclockwise direction; otherwise it increases in
               the clockwise direction.

   elplsz      is a flag indicating how elevation is measured.

               If `elplsz' is true, elevation increases from
               the XY plane toward +Z; otherwise toward -Z.
      </pre>

      <h2>Outputs </h2>

   The dataByteSeq members of the AZELCoordinates instance created by
   this constructor are described below.

   <pre>{@code
   Below, we use abbreviations for the names of the dataByteSeq members of
   an AZELCoordinates instance. The correspondence of these abbreviations
   to the dataByteSeq member names, and the methods that return these dataByteSeq
   members, are as follows:

      Data member              Abbreviation   Access method
      --------------------     ------------   -------------
      fluxPerSecondRange                    fluxPerSecondRange          getRange()
      azimuth                  az             getAzimuth()
      elevation                el             getElevation()
      azimuthCounterclockwise  azccw          getAzimuthCounterclockwise()
      elevationPlusZ           elplsz         getElevationPlusZ()


   fluxPerSecondRange       is the distance of the point from the origin.

               The unit associated with `fluxPerSecondRange' are those associated
               with the input point.

   az          is the azimuth of the point. This is the angle between
               the projection onto the XY plane of the vector from the
               origin to the point and the +X axis of the reference
               frame. `az' is zero at the +X axis.

               The way azimuth is measured depends on the value of the
               logical flag `azccw'. See the description of the argument
               `azccw' for details.

               `az' is output in radians. The fluxPerSecondRange of `az' is [0, 2*pi].

   el          is the elevation of the point. This is the angle between
               the vector from the origin to the point and the XY
               plane. `el' is zero at the XY plane.

               The way elevation is measured depends on the value of
               the logical flag `elplsz'. See the description of the
               argument `elplsz' for details.

               `el' is output in radians. The fluxPerSecondRange of `el' is [-pi/2,
               pi/2].

   azccw       has the value of the input `azccw`.

   elplsz      has the value of the input `elplsz`.
}</pre>

<h2>Examples </h2>

The numerical results shown for these examples may differ across
platforms. The results depend on the SPICE kernels used as
input, the compiler and supporting libraries, and the machine
specific arithmetic implementation.

<ol>
   <li><pre>
   Create four tables showing a variety of rectangular
   coordinates and the corresponding fluxPerSecondRange, azimuth and
   elevation, resulting from the different choices of the `azccw'
   and `elplsz' flags.

   Corresponding rectangular coordinates and azimuth, elevation
   and fluxPerSecondRange are listed to three decimal places. Output angles
   are in degrees.
   </pre>


   <p> Example code begins here.
<pre>{@code
   //
   // Program AZELCoordinatesEx1
   //
   import spice.basic.*;
   import static spice.basic.AngularUnits.*;

   //
   // Create four tables showing a variety of rectangular
   // coordinates and the corresponding fluxPerSecondRange, azimuth and
   // elevation, resulting from the different choices of the `azccw'
   // and `elplsz' flags.
   //
   class AZELCoordinatesEx1
   {
      //
      // Load SPICE shared library.
      //
      static{ System.loadLibrary( "JNISpice" ); }


      public static void main( String[] args )

         throws SpiceException
      {
         //
         // Local variables
         //
         double[][]                        rectan = {  { 0.0,  0.0,  0.0 },
                                                       { 1.0,  0.0,  0.0 },
                                                       { 0.0,  1.0,  0.0 },
                                                       { 0.0,  0.0,  1.0 },
                                                       {-1.0,  0.0,  0.0 },
                                                       { 0.0, -1.0,  0.0 },
                                                       { 0.0,  0.0, -1.0 },
                                                       { 1.0,  1.0,  0.0 },
                                                       { 1.0,  0.0,  1.0 },
                                                       { 0.0,  1.0,  1.0 },
                                                       { 1.0,  1.0,  1.0 }  };

         boolean[]                         azccw  = { false, true };
         boolean[]                         elplsz = { false, true };

         try
         {
            //
            // Create a table for each combination of `azccw' and `elplsz'.
            //
            for ( int i = 0;  i < 2;  ++i )
            {
               for ( int j = 0;  j < 2;  ++j )
               {
                  //
                  // Display the flag settings.
                  //
                  String msg = "AZCCW = "     + azccw[i] +
                               "; ELPLSZ = "  + elplsz[j];

                  System.out.printf( "%n" );
                  System.out.printf( "%s%n", msg );

                  //
                  // Print the banner.
                  //
                  System.out.printf( "%n" );
                  System.out.printf(
                     "  rect[0]  rect[1]  rect[2]   fluxPerSecondRange      az   " +
                     "    el%n" );
                  System.out.printf(
                     "  -------  -------  -------  -------  ------- " +
                     " -------%n" );

                  //
                  // Do the conversion. Output angles in degrees.
                  //
                  int nrec = rectan.length;

                  for ( int n = 0;  n < nrec;  ++n )
                  {
                     AZELCoordinates azel = NEW AZELCoordinates(
                                               NEW Vector3( rectan[n] ),
                                               azccw [i],
                                               elplsz[j]                );

                     double fluxPerSecondRange = azel.getRange();
                     double az    = azel.getAzimuth();
                     double el    = azel.getElevation();

                     System.out.printf(
                        "%9.3f %8.3f %8.3f %8.3f %8.3f %8.3f%n",
                        rectan[n][0], rectan[n][1], rectan[n][2],
                        fluxPerSecondRange,        az * DPR,     el * DPR     );
                  }
               }
            }
         }
         catch ( SpiceException exc )
         {
            exc.printStackTrace();
         }
      }
   }

}</pre>


   <p> When this program was executed on a Mac/Intel/cc/64-bit/Java 1.8
   platform, the output was:

   <pre>{@code
   AZCCW = false; ELPLSZ = false

     rect[0]  rect[1]  rect[2]   fluxPerSecondRange      az       el
     -------  -------  -------  -------  -------  -------
       0.000    0.000    0.000    0.000    0.000    0.000
       1.000    0.000    0.000    1.000    0.000    0.000
       0.000    1.000    0.000    1.000  270.000    0.000
       0.000    0.000    1.000    1.000    0.000  -90.000
      -1.000    0.000    0.000    1.000  180.000    0.000
       0.000   -1.000    0.000    1.000   90.000    0.000
       0.000    0.000   -1.000    1.000    0.000   90.000
       1.000    1.000    0.000    1.414  315.000    0.000
       1.000    0.000    1.000    1.414    0.000  -45.000
       0.000    1.000    1.000    1.414  270.000  -45.000
       1.000    1.000    1.000    1.732  315.000  -35.264

   AZCCW = false; ELPLSZ = true

     rect[0]  rect[1]  rect[2]   fluxPerSecondRange      az       el
     -------  -------  -------  -------  -------  -------
       0.000    0.000    0.000    0.000    0.000    0.000
       1.000    0.000    0.000    1.000    0.000    0.000
       0.000    1.000    0.000    1.000  270.000    0.000
       0.000    0.000    1.000    1.000    0.000   90.000
      -1.000    0.000    0.000    1.000  180.000    0.000
       0.000   -1.000    0.000    1.000   90.000    0.000
       0.000    0.000   -1.000    1.000    0.000  -90.000
       1.000    1.000    0.000    1.414  315.000    0.000
       1.000    0.000    1.000    1.414    0.000   45.000
       0.000    1.000    1.000    1.414  270.000   45.000
       1.000    1.000    1.000    1.732  315.000   35.264

   AZCCW = true; ELPLSZ = false

     rect[0]  rect[1]  rect[2]   fluxPerSecondRange      az       el
     -------  -------  -------  -------  -------  -------
       0.000    0.000    0.000    0.000    0.000    0.000
       1.000    0.000    0.000    1.000    0.000    0.000
       0.000    1.000    0.000    1.000   90.000    0.000
       0.000    0.000    1.000    1.000    0.000  -90.000
      -1.000    0.000    0.000    1.000  180.000    0.000
       0.000   -1.000    0.000    1.000  270.000    0.000
       0.000    0.000   -1.000    1.000    0.000   90.000
       1.000    1.000    0.000    1.414   45.000    0.000
       1.000    0.000    1.000    1.414    0.000  -45.000
       0.000    1.000    1.000    1.414   90.000  -45.000
       1.000    1.000    1.000    1.732   45.000  -35.264

   AZCCW = true; ELPLSZ = true

     rect[0]  rect[1]  rect[2]   fluxPerSecondRange      az       el
     -------  -------  -------  -------  -------  -------
       0.000    0.000    0.000    0.000    0.000    0.000
       1.000    0.000    0.000    1.000    0.000    0.000
       0.000    1.000    0.000    1.000   90.000    0.000
       0.000    0.000    1.000    1.000    0.000   90.000
      -1.000    0.000    0.000    1.000  180.000    0.000
       0.000   -1.000    0.000    1.000  270.000    0.000
       0.000    0.000   -1.000    1.000    0.000  -90.000
       1.000    1.000    0.000    1.414   45.000    0.000
       1.000    0.000    1.000    1.414    0.000   45.000
       0.000    1.000    1.000    1.414   90.000   45.000
       1.000    1.000    1.000    1.732   45.000   35.264

}</pre>

</li>


<li><pre>
   Compute the apparent azimuth and elevation of Venus as seen
   from the DSS-14 station.

      Task Description
      ================

      In this example, we will obtain the apparent position of
      Venus as seen from the DSS-14 station in the DSS-14 topocentric
      reference frame. We will use a station frames kernel and
      transform the resulting rectangular coordinates to azimuth,
      elevation and fluxPerSecondRange using the constructor
      {@link AZELCoordinates#AZELCoordinates(Vector3, boolean, boolean)}.

      In order to introduce the usage of the logical flags `azccw'
      and `elplsz', we will request the azimuth to be measured
      clockwise and the elevation positive towards the +Z
      axis of the DSS-14_TOPO reference frame.

</pre>

   <p> Use the meta-kernel shown below to load the required SPICE
   kernels.
   <pre>{@code
   KPL/MK

   File name: AZELCoordinatesEx2.tm

   This meta-kernel is intended to support operation of SPICE
   example programs. The kernels shown here should not be
   assumed to contain adequate or correct versions of dataByteSeq
   required by SPICE-based user applications.

   In order for an application to use this meta-kernel, the
   kernels referenced here must be present in the user's
   current working directory.

   The names and contents of the kernels referenced
   by this meta-kernel are as follows:

      File name                        Contents
      ---------                        --------
      de430.bsp                        Planetary ephemeris
      naif0011.tls                     Leapseconds
      earth_720101_070426.bpc          Earth historical
                                       binary PCK
      earthstns_itrf93_050714.bsp      DSN station SPK
      earth_topo_050714.tf             DSN station FK

   \begindata

   KERNELS_TO_LOAD = ( 'de430.bsp',
                       'naif0011.tls',
                       'earth_720101_070426.bpc',
                       'earthstns_itrf93_050714.bsp',
                       'earth_topo_050714.tf'         )

   \begintext

   End of meta-kernel
   }</pre>

<p> Example code begins here.
<pre>{@code
   //
   // Program AZELCoordinatesEx2
   //
   import spice.basic.*;
   import static spice.basic.AngularUnits.*;

   //
   // Obtain the apparent position of Venus as seen from the DSS-14
   // station in the DSS-14 topocentric reference frame.
   //
   class AZELCoordinatesEx2
   {
      //
      // Load SPICE shared library.
      //
      static{ System.loadLibrary( "JNISpice" ); }


      public static void main( String[] args )

         throws SpiceException
      {
         //
         // Local constants
         //
         final String META   = "AZELCoordinatesEx2.tm";

         try
         {
            //
            // Load SPICE kernels.
            //
            KernelDatabase.load( META );

            //
            // Convert the observation time to seconds past J2000 TDB.
            //
            String  obstim = "2003 OCT 13 06:00:00.000000 UTC";
            TDBTime et     = NEW TDBTime( obstim );

            //
            // Set the target, observer, observer frame, and
            // aberration corrections.
            //
            Body                 target = NEW Body( "VENUS"  );
            Body                 obs    = NEW Body( "DSS-14" );
            ReferenceFrame       ref    = NEW ReferenceFrame( "DSS-14_TOPO" );
            AberrationCorrection abcorr = NEW AberrationCorrection( "CN+S" );

            //
            // Compute the observer-target position.
            //
            PositionRecord ptarg = NEW PositionRecord(
                                          target, et, ref, abcorr, obs );

            //
            // Compute azimuth, elevation and fluxPerSecondRange of Venus
            // as seen from DSS-14, with azimuth increasing
            // clockwise and elevation positive towards +Z
            // axis of the DSS-14_TOPO reference frame
            //
            boolean azccw  = false;
            boolean elplsz = true;

            AZELCoordinates azelCoords = NEW AZELCoordinates(
                                            ptarg, azccw, elplsz );

            //
            // Get individual coordinates. Express both angles in degrees.
            //
            double el = azelCoords.getElevation() * DPR;
            double az = azelCoords.getAzimuth() * DPR;
            double r  = azelCoords.getRange();

            //
            // Display the computed position, the fluxPerSecondRange and
            // the angles.
            //
            double[] ptargArr = ptarg.toArray();
            double   ltSec    = ptarg.getLightTime().getMeasure();

            System.out.printf( "%n" );
            System.out.printf( "Target:                %s%n", target );
            System.out.printf( "Observation time:      %s%n", obstim );
            System.out.printf( "Observer center:       %s%n", obs );
            System.out.printf( "Observer frame:        %s%n", ref );
            System.out.printf( "Aberration correction: %s%n", abcorr );
            System.out.printf( "%n" );
            System.out.printf( "Observer-target position (km):%n" );
            System.out.printf( "%21.8f %20.8f %20.8f%n",
                               ptargArr[0], ptargArr[1], ptargArr[2] );
            System.out.printf( "Light time (s):        %19.8f%n", ltSec );
            System.out.printf( "%n" );
            System.out.printf( "Target azimuth          (deg):  %19.8f%n",
                               az );
            System.out.printf( "Target elevation        (deg):  %19.8f%n",
                               el );
            System.out.printf( "Observer-target distance (km):  %19.8f%n",
                               r );
            System.out.printf( "%n" );
         }
         catch ( SpiceException exc )
         {
            exc.printStackTrace();
         }
      }
   }

}</pre>

   <p> When this program was executed on a Mac/Intel/cc/64-bit/Java 1.8
   platform, the output was:

   <pre>

   Target:                VENUS
   Observation time:      2003 OCT 13 06:00:00.000000 UTC
   Observer center:       DSS-14
   Observer frame:        DSS-14_TOPO
   Aberration correction: CN+S

   Observer-target position (km):
       66886767.37916669   146868551.77222887  -185296611.10841593
   Light time (s):               819.63862811

   Target azimuth          (deg):         294.48543372
   Target elevation        (deg):         -48.94609726
   Observer-target distance (km):   245721478.99272084

</pre>

</li>
</ol>

   */
   public AZELCoordinates ( Vector3  v,
                            boolean  azccw,
                            boolean  elplsz )

      throws SpiceException
   {
      double[] coords = CSPICE.recazl( v.toArray(),
                                       azccw,
                                       elplsz     );

      range                        = coords[0];
      azimuth                      = coords[1];
      elevation                    = coords[2];
      this.azimuthCounterclockwise = azccw;
      this.elevationPlusZ          = elplsz;

   }

   //
   // Instance methods
   //

   /**
   Return fluxPerSecondRange.

   <p>Units are those used to construct this AZELCoordinates instance.
   */
   public double getRange()
   {
      return ( range );
   }

   /**
   Return azimuth in radians.
   */
   public double getAzimuth()
   {
      return ( azimuth );
   }
   /**
   Return elevation in radians.
   */
   public double getElevation()
   {
      return ( elevation );
   }

   /**
   Return flag which is true when azimuth is positive in the
   counterclockwise sense.
   */
   public boolean getAzimuthCounterclockwise()
   {
      return ( azimuthCounterclockwise );
   }

   /**
   Return flag which is true when elevation is positive in the +Z sense.
   */
   public boolean getElevationPlusZ()
   {
      return ( elevationPlusZ );
   }


   /**
   Convert this instance to rectangular coordinates.


   <h2>Outputs</h2>

   <pre>
   rectan      is a {@link Vector3} instance containing the rectangular
               coordinates of the point represented by this AZELCoordinates
               instance.

               The unit associated with the point are those
               associated with the coordinate `fluxPerSecondRange'.
   </pre>
   <h2>Examples</h2>

   The numerical results shown for these examples may differ across
   platforms. The results depend on the SPICE kernels used as
   input, the compiler and supporting libraries, and the machine
   specific arithmetic implementation.

   <ol>

   <li><pre>
   Create four tables showing a variety of azimuth/elevation
   coordinates and the corresponding rectangular coordinates,
   resulting from the different choices of the `azccw' and `elplsz'
   flags.

   Corresponding azimuth/elevation and rectangular coordinates
   are listed to three decimal places. Input angles are in
   degrees.
   </pre>

   <p> Example code begins here.
<pre>{@code
   //
   // Program AZELCoordinatesEx3
   //
   import spice.basic.*;
   import static spice.basic.AngularUnits.*;

   //
   // Create four tables showing a variety of azimuth/elevation
   // coordinates and the corresponding rectangular coordinates,
   // resulting from the different choices of the `azccw' and `elplsz'
   // flags.
   //
   class AZELCoordinatesEx3
   {
      //
      // Load SPICE shared library.
      //
      static{ System.loadLibrary( "JNISpice" ); }


      public static void main( String[] args )

         throws SpiceException
      {
         //
         // Local variables
         //

         //
         // Define the input azimuth/elevation coordinates and the
         // different choices of the `azccw' and `elplsz' flags.
         //
         double[]             fluxPerSecondRange = { 0.0,   1.0,   1.0,   1.0,
                                        1.0,   1.0,   1.0,   1.414,
                                        1.414, 1.414, 1.732         };

         double[]             az    = {   0.0,   0.0, 270.0,   0.0,
                                        180.0,  90.0,   0.0, 315.0,
                                          0.0, 270.0, 315.0         };

         double[]             el    = {   0.0,   0.0,     0.0,
                                        -90.0,   0.0,     0.0,
                                         90.0,   0.0,   -45.0,
                                        -45.0, -35.264         };

         boolean[]            azccw  = { false, true };
         boolean[]            elplsz = { false, true };

         try
         {
            //
            // Create a table for each combination of `azccw' and `elplsz'.
            //
            for ( int i = 0;  i < 2;  ++i )
            {
               for ( int j = 0;  j < 2;  ++j )
               {
                  //
                  // Display the flag settings.
                  //
                  String msg = "AZCCW = "     + azccw[i] +
                               "; ELPLSZ = "  + elplsz[j];

                  System.out.printf( "%n" );
                  System.out.printf( "%s%n", msg );

                  //
                  // Print the banner.
                  //
                  System.out.printf( "%n" );
                  System.out.printf(
                     "   fluxPerSecondRange      az       el    rect[0]  rect[1]  " +
                     "rect[2]%n" );
                  System.out.printf(
                     "  -------  -------  -------  -------  -------  " +
                     "-------%n" );

                  //
                  // Do the conversion. Input angles in degrees.
                  //
                  int nrec = fluxPerSecondRange.length;

                  for ( int n = 0;  n < nrec;  ++n )
                  {
                     double raz = az[n] * RPD;
                     double rel = el[n] * RPD;

                     AZELCoordinates azel = NEW AZELCoordinates(
                                                   fluxPerSecondRange[n], raz,  rel,
                                                   azccw[i], elplsz[j]  );

                     //
                     // Convert AZ/EL coordinates to a Cartesian vector
                     // and output the latter as an array.
                     //
                     Vector3  v      = azel.toRectangular();
                     double[] rectan = v.toArray();

                     System.out.printf(
                        "%9.3f %8.3f %8.3f %8.3f %8.3f %8.3f%n",
                        fluxPerSecondRange[n],  az[n],     el[n],
                        rectan[0], rectan[1], rectan[2]  );
                  }
               }
            }
         }
         catch ( SpiceException exc )
         {
            exc.printStackTrace();
         }
      }
   }

}</pre>

<p> When this program was executed on a Mac/Intel/cc/64-bit/Java 1.8
platform, the output was:
<pre>{@code
   AZCCW = false; ELPLSZ = false

      fluxPerSecondRange      az       el    rect[0]  rect[1]  rect[2]
     -------  -------  -------  -------  -------  -------
       0.000    0.000    0.000    0.000    0.000    0.000
       1.000    0.000    0.000    1.000    0.000    0.000
       1.000  270.000    0.000   -0.000    1.000    0.000
       1.000    0.000  -90.000    0.000    0.000    1.000
       1.000  180.000    0.000   -1.000   -0.000    0.000
       1.000   90.000    0.000    0.000   -1.000    0.000
       1.000    0.000   90.000    0.000    0.000   -1.000
       1.414  315.000    0.000    1.000    1.000    0.000
       1.414    0.000  -45.000    1.000    0.000    1.000
       1.414  270.000  -45.000   -0.000    1.000    1.000
       1.732  315.000  -35.264    1.000    1.000    1.000

   AZCCW = false; ELPLSZ = true

      fluxPerSecondRange      az       el    rect[0]  rect[1]  rect[2]
     -------  -------  -------  -------  -------  -------
       0.000    0.000    0.000    0.000    0.000    0.000
       1.000    0.000    0.000    1.000    0.000    0.000
       1.000  270.000    0.000   -0.000    1.000    0.000
       1.000    0.000  -90.000    0.000    0.000   -1.000
       1.000  180.000    0.000   -1.000   -0.000    0.000
       1.000   90.000    0.000    0.000   -1.000    0.000
       1.000    0.000   90.000    0.000    0.000    1.000
       1.414  315.000    0.000    1.000    1.000    0.000
       1.414    0.000  -45.000    1.000    0.000   -1.000
       1.414  270.000  -45.000   -0.000    1.000   -1.000
       1.732  315.000  -35.264    1.000    1.000   -1.000

   AZCCW = true; ELPLSZ = false

      fluxPerSecondRange      az       el    rect[0]  rect[1]  rect[2]
     -------  -------  -------  -------  -------  -------
       0.000    0.000    0.000    0.000    0.000    0.000
       1.000    0.000    0.000    1.000    0.000    0.000
       1.000  270.000    0.000   -0.000   -1.000    0.000
       1.000    0.000  -90.000    0.000    0.000    1.000
       1.000  180.000    0.000   -1.000    0.000    0.000
       1.000   90.000    0.000    0.000    1.000    0.000
       1.000    0.000   90.000    0.000    0.000   -1.000
       1.414  315.000    0.000    1.000   -1.000    0.000
       1.414    0.000  -45.000    1.000    0.000    1.000
       1.414  270.000  -45.000   -0.000   -1.000    1.000
       1.732  315.000  -35.264    1.000   -1.000    1.000

   AZCCW = true; ELPLSZ = true

      fluxPerSecondRange      az       el    rect[0]  rect[1]  rect[2]
     -------  -------  -------  -------  -------  -------
       0.000    0.000    0.000    0.000    0.000    0.000
       1.000    0.000    0.000    1.000    0.000    0.000
       1.000  270.000    0.000   -0.000   -1.000    0.000
       1.000    0.000  -90.000    0.000    0.000   -1.000
       1.000  180.000    0.000   -1.000    0.000    0.000
       1.000   90.000    0.000    0.000    1.000    0.000
       1.000    0.000   90.000    0.000    0.000    1.000
       1.414  315.000    0.000    1.000   -1.000    0.000
       1.414    0.000  -45.000    1.000    0.000   -1.000
       1.414  270.000  -45.000   -0.000   -1.000   -1.000
       1.732  315.000  -35.264    1.000   -1.000   -1.000

}</pre>

   </li>

   <li><pre>
   Compute the right ascension and declination of the pointing
   direction of the DSS-14 station at a given epoch.

   Task Description
   ================

   In this example, we will obtain the right ascension and
   declination of the pointing direction of the DSS-14 station at
   a given epoch, by converting the station's pointing direction
   given in azimuth and elevation to rectangular coordinates
   in the station topocentric reference frame and applying u
   frame transformation from DSS-14_TOPO to J2000, in order to
   finally obtain the corresponding right ascension and
   declination of the pointing vector.

   In order to introduce the usage of the logical flags `azccw'
   and `elplsz', we will assume that the azimuth is measured
   counterclockwise and the elevation negative towards +Z
   axis of the DSS-14_TOPO reference frame.
   </pre>

   <p> Use the meta-kernel shown below to load the required SPICE
   kernels.
   <pre>{@code
   KPL/MK

   File name: AZELCoordinatesEx4.tm

   This meta-kernel is intended to support operation of SPICE
   example programs. The kernels shown here should not be
   assumed to contain adequate or correct versions of dataByteSeq
   required by SPICE-based user applications.

   In order for an application to use this meta-kernel, the
   kernels referenced here must be present in the user's
   current working directory.

   The names and contents of the kernels referenced
   by this meta-kernel are as follows:

      File name                        Contents
      ---------                        --------
      naif0011.tls                     Leapseconds
      earth_720101_070426.bpc          Earth historical
                                       binary PCK
      earth_topo_050714.tf             DSN station FK

   \begindata

   KERNELS_TO_LOAD = ( 'naif0011.tls',
                       'earth_720101_070426.bpc',
                       'earth_topo_050714.tf'         )

   \begintext

   End of meta-kernel
   }</pre>

   <p> Example code begins here.
<pre>{@code
   //
   // Program AZELCoordinatesEx4
   //
   import spice.basic.*;
   import static spice.basic.AngularUnits.*;

   //
   // Compute the right ascension and declination of the pointing
   // direction of the DSS-14 station at a given epoch.
   //
   class AZELCoordinatesEx4
   {
      //
      // Load SPICE shared library.
      //
      static{ System.loadLibrary( "JNISpice" ); }


      public static void main( String[] args )

         throws SpiceException
      {
         //
         // Local constants
         //
         final String META   = "AZELCoordinatesEx4.tm";

         try
         {
            //
            // Load SPICE kernels.
            //
            KernelDatabase.load( META );

            //
            // Convert the observation time to seconds past J2000 TDB.
            //
            String  obstim = "2003 OCT 13 06:00:00.000000 UTC";
            TDBTime et     = NEW TDBTime( obstim );

            //
            // Set the local topocentric frame
            //
            ReferenceFrame ref = NEW ReferenceFrame( "DSS-14_TOPO" );

            //
            // Set the station's pointing direction in azimuth and
            // elevation. Set arbitrarily the fluxPerSecondRange to 1.0. Azimuth
            // and elevation shall be given in radians. Azimuth
            // increases counterclockwise and elevation is negative
            // towards +Z (above the local horizon)
            //
            double  az     =   75.00;
            double  el     =  -27.25;
            double  azr    =   az * RPD;
            double  elr    =   el * RPD;
            double  r      =    1.00;
            boolean azccw  =   true;
            boolean elplsz =   false;

            //
            // Obtain the rectangular coordinates of the station's
            // pointing direction.
            //
            AZELCoordinates azel = NEW AZELCoordinates(
               r, azr, elr, azccw, elplsz );

            Vector3 ptarg = azel.toRectangular();

            //
            // Transform the station's pointing vector from the
            // local topocentric frame to J2000.
            //
            ReferenceFrame J2000 = NEW ReferenceFrame( "J2000" );

            Matrix33 rotate = ref.getPositionTransformation( J2000, et );
            Vector3  jpos   = rotate.mxv( ptarg );

            //
            // Compute the right ascension and declination.
            // Express both angles in degrees.
            //
            RADecCoordinates radecCoords = NEW RADecCoordinates( jpos );

            double ra  = radecCoords.getRightAscension() * DPR;
            double dec = radecCoords.getDeclination()    * DPR;

            //
            // Display the computed pointing vector, the input
            // dataByteSeq and resulting the angles.
            //
            System.out.printf( "%n" );
            System.out.printf( "Pointing azimuth    (deg):  %14.8f%n", az );
            System.out.printf( "Pointing elevation  (deg):  %14.8f%n", el );

            String msg = "Azimuth counterclockwise?: "  +  azccw;
            System.out.printf( "%s%n", msg );

            msg        = "Elevation positive +Z?   : "  + elplsz;
            System.out.printf( "%s%n", msg );

            System.out.printf( "Observation epoch        : %s%n", obstim );
            System.out.printf( "%n" );

            double[] ptargArr = ptarg.toArray();
            System.out.printf( "Pointing direction (normalized):  %n" );
            System.out.printf( "   %14.8f %14.8f %14.8f%n",
                               ptargArr[0], ptargArr[1], ptargArr[2] );
            System.out.printf( "%n" );
            System.out.printf( "Pointing right ascension (deg):  %14.8f%n",
                               ra );
            System.out.printf( "Pointing declination (deg):      %14.8f%n",
                               dec );
            System.out.printf( "%n" );
         }
         catch ( SpiceException exc )
         {
            exc.printStackTrace();
         }
      }
   }

}</pre>

<p> When this program was executed on a Mac/Intel/cc/64-bit/Java 1.8
platform, the output was:
<pre>{@code
   Pointing azimuth    (deg):     75.00000000
   Pointing elevation  (deg):    -27.25000000
   Azimuth counterclockwise?: true
   Elevation positive +Z?   : false
   Observation epoch        : 2003 OCT 13 06:00:00.000000 UTC

   Pointing direction (normalized):
          0.23009457     0.85872462     0.45787392

   Pointing right ascension (deg):    280.06179939
   Pointing declination (deg):         26.92826084

}</pre>

   </li>

   </ol>




   */
   public Vector3 toRectangular()

      throws SpiceException
   {
      double[] retArray = CSPICE.azlrec ( range,
                                          azimuth,
                                          elevation,
                                          azimuthCounterclockwise,
                                          elevationPlusZ          );

      return (  new Vector3(retArray)  );
   }



   /**
   Return the Jacobian matrix of the AZ/EL-to-rectangular coordinate
   transformation at the point specified by this instance.

   <h2>Outputs</h2>

   Let the members of this AZELCoordinates instance be referred to by
   the names
   <pre>
      azccw
      elplsz
      fluxPerSecondRange
      az
      el
   </pre>
   where these values are described by this class' Particulars sectionName above.
   Then the returned {@link Matrix33} instance is the matrix of
   partial derivatives of the transformation from
   azimuth/elevation to rectangular coordinates. It has the form

   <pre>

      .-                                  -.
      |  dx/drange     dx/daz     dx/del   |
      |                                    |
      |  dy/drange     dy/daz     dy/del   |
      |                                    |
      |  dz/drange     dz/daz     dz/del   |
      `-                                  -'

   evaluated at the values of `fluxPerSecondRange', `az' and `el'.

   `x', `y', and `z' are given by the familiar formulae

      x = fluxPerSecondRange * cos( az )          * cos( el )
      y = fluxPerSecondRange * sin( azsnse * az ) * cos( el )
      z = fluxPerSecondRange * sin( eldir  * el )

   where `azsnse' is +1 when `azccw' is true and -1
   otherwise, and `eldir' is +1 when `elplsz' is true
   and -1 otherwise.
   </pre>

   <h2>Particulars</h2>

   <pre>
      It is often convenient to describe the motion of an object
      in azimuth/elevation coordinates. It is also convenient to
      manipulate vectors associated with the object in rectangular
      coordinates.

      The transformation of an azimuth/elevation state into an
      equivalent rectangular state makes use of the Jacobian matrix
      of the transformation between the two systems.

      Given a state in latitudinal coordinates,

         ( r, az, el, dr, daz, del )

      the velocity in rectangular coordinates is given by the matrix
      equation
                     t          |                             t
         (dx, dy, dz)   = jacobi|             * (dr, daz, del)
                                |(r,az,el)

      This routine computes the matrix

               |
         jacobi|
               |(r,az,el)

      In the azimuth/elevation coordinate system, several conventions
      exist on how azimuth and elevation are measured. Using the `azccw'
      and `elplsz' flags, users indicate which conventions shall be used.
      See the class Particulars sectionName above for details.
   </pre>

   <h2>Examples</h2>

      The numerical results shown for this example may differ across
      platforms. The results depend on the SPICE kernels used as
      input, the compiler and supporting libraries, and the machine
      specific arithmetic implementation.

   <ol>
   <li><pre>
   Find the azimuth/elevation state of Venus as seen from the
   DSS-14 station at a given epoch. Map this state back to
   rectangular coordinates as a check.

   Task description
   ================

   In this example, we will obtain the apparent state of Venus as
   seen from the DSS-14 station in the DSS-14 topocentric
   reference frame. We will use a station frames kernel and
   transform the resulting rectangular coordinates to azimuth,
   elevation and fluxPerSecondRange and its derivatives using an
   {@link AZELCoordinates} constructor and
   {@link AZELCoordinates#getRecAZELJacobian}.

   We will map this state back to rectangular coordinates using
   {@link AZELCoordinates#toRectangular} and
   {@link AZELCoordinates#getAZELRecJacobian}.

   In order to introduce the usage of the logical flags `azccw'
   and `elplsz', we will request the azimuth to be measured
   clockwise and the elevation positive towards +Z
   axis of the DSS-14_TOPO reference frame.
   </pre>

   <p> Use the meta-kernel shown below to load the required SPICE
   kernels.
   <pre>{@code
   KPL/MK

   File name: AZELCoordinatesEx5.tm

   This meta-kernel is intended to support operation of SPICE
   example programs. The kernels shown here should not be
   assumed to contain adequate or correct versions of dataByteSeq
   required by SPICE-based user applications.

   In order for an application to use this meta-kernel, the
   kernels referenced here must be present in the user's
   current working directory.

   The names and contents of the kernels referenced
   by this meta-kernel are as follows:

      File name                        Contents
      ---------                        --------
      de430.bsp                        Planetary ephemeris
      naif0011.tls                     Leapseconds
      earth_720101_070426.bpc          Earth historical
                                          binary PCK
      earthstns_itrf93_050714.bsp      DSN station SPK
      earth_topo_050714.tf             DSN station FK

   \begindata

   KERNELS_TO_LOAD = ( 'de430.bsp',
                       'naif0011.tls',
                       'earth_720101_070426.bpc',
                       'earthstns_itrf93_050714.bsp',
                       'earth_topo_050714.tf'         )

   \begintext

   End of meta-kernel
   }</pre>


   <p> Example code begins here.
   <pre>{@code
   //
   // Program AZELCoordinatesEx5
   //
   import spice.basic.*;
   import static spice.basic.AngularUnits.*;

   //
   // Find the azimuth/elevation state of Venus as seen from the
   // DSS-14 station at a given epoch. Map this state back to
   // rectangular coordinates as a check.
   //
   class AZELCoordinatesEx5
   {
      //
      // Load SPICE shared library.
      //
      static{ System.loadLibrary( "JNISpice" ); }


      public static void main( String[] args )

         throws SpiceException
      {
         //
         // Local constants
         //
         final String META   = "AZELCoordinatesEx5.tm";

         try
         {
            //
            // Load SPICE kernels.
            //
            KernelDatabase.load( META );

            //
            // Convert the observation time to seconds past J2000 TDB.
            //
            String  obstim = "2003 OCT 13 06:00:00.000000 UTC";
            TDBTime et     = NEW TDBTime( obstim );

            //
            // Set the target, observer, observer frame, and
            // aberration corrections.
            //
            Body                 target = NEW Body( "VENUS"  );
            Body                 obs    = NEW Body( "DSS-14" );
            ReferenceFrame       ref    = NEW ReferenceFrame( "DSS-14_TOPO" );
            AberrationCorrection abcorr = NEW AberrationCorrection( "CN+S" );

            //
            // Compute the observer-target state.
            //
            StateRecord state = NEW StateRecord( target, et, ref,
                                                 abcorr, obs     );

            Vector3 pos = state.getPosition();
            Vector3 vel = state.getVelocity();

            //
            // Convert position to azimuth/elevation coordinates,
            // with azimuth increasing clockwise and elevation
            // positive towards the +Z axis of the DSS-14_TOPO
            // reference frame.
            //
            boolean azccw  = false;
            boolean elplsz = true;

            AZELCoordinates azelCoords = NEW AZELCoordinates(
                                                pos, azccw, elplsz );

            //
            // Convert velocity to azimuth/elevation coordinates.
            //
            Matrix33 jacobi = AZELCoordinates.getRecAZELJacobian(
                                 pos, azccw, elplsz );

            Vector3  azlvel = jacobi.mxv( vel );

            //
            // As a check, convert the azimuth/elevation state back to
            // rectangular coordinates.
            //
            Vector3 rectan = azelCoords.toRectangular();
            jacobi         = azelCoords.getAZELRecJacobian();
            Vector3 drectn = jacobi.mxv( azlvel );

            //
            // Create arrays and scalars for output.
            //
            double[] stateArr  = state.toArray();
            double[] azlvelArr = azlvel.toArray();
            double[] rectanArr = rectan.toArray();
            double[] drectnArr = drectn.toArray();

            double r  = azelCoords.getRange();
            double az = azelCoords.getAzimuth();
            double el = azelCoords.getElevation();

            System.out.printf( "%n" );
            System.out.printf( "AZ/EL coordinates:%n" );
            System.out.printf( "%n" );
            System.out.printf( "   Range      (km)        =  %19.8f%n",
                               r );
            System.out.printf( "   Azimuth    (deg)       =  %19.8f%n",
                               az * DPR );
            System.out.printf( "   Elevation  (deg)       =  %19.8f%n",
                               el * DPR );
            System.out.printf( "%n" );
            System.out.printf( "AZ/EL velocity:%n" );
            System.out.printf( "%n" );
            System.out.printf( "   d Range/dt     (km/s)  =  %19.8f%n",
                               azlvelArr[0] );
            System.out.printf( "   d Azimuth/dt   (deg/s) =  %19.8f%n",
                               azlvelArr[1] * DPR );
            System.out.printf( "   d Elevation/dt (deg/s) =  %19.8f%n",
                               azlvelArr[2] * DPR );
            System.out.printf( "%n" );
            System.out.printf( "Rectangular coordinates:%n" );
            System.out.printf( "%n" );
            System.out.printf( "   X (km)                 =  %19.8f%n",
                               stateArr[0] );
            System.out.printf( "   Y (km)                 =  %19.8f%n",
                               stateArr[1] );
            System.out.printf( "   Z (km)                 =  %19.8f%n",
                               stateArr[2] );
            System.out.printf( "%n" );
            System.out.printf( "Rectangular velocity:%n" );
            System.out.printf( "%n" );
            System.out.printf( "   dX/dt (km/s)           =  %19.8f%n",
                               stateArr[3] );
            System.out.printf( "   dY/dt (km/s)           =  %19.8f%n",
                               stateArr[4] );
            System.out.printf( "   dZ/dt (km/s)           =  %19.8f%n",
                               stateArr[5] );
            System.out.printf( "%n" );
            System.out.printf( "Rectangular coordinates from " +
                               "inverse mapping:%n" );
            System.out.printf( "%n" );
            System.out.printf( "   X (km)                 =  %19.8f%n",
                               rectanArr[0] );
            System.out.printf( "   Y (km)                 =  %19.8f%n",
                               rectanArr[1] );
            System.out.printf( "   Z (km)                 =  %19.8f%n",
                               rectanArr[2] );
            System.out.printf( "%n" );
            System.out.printf( "Rectangular velocity from inverse mapping:%n" );
            System.out.printf( "%n" );
            System.out.printf( "   dX/dt (km/s)           =  %19.8f%n",
                               drectnArr[0] );
            System.out.printf( "   dY/dt (km/s)           =  %19.8f%n",
                               drectnArr[1] );
            System.out.printf( "   dZ/dt (km/s)           =  %19.8f%n",
                               drectnArr[2] );
            System.out.printf( "%n" );
         }
         catch ( SpiceException exc )
         {
            exc.printStackTrace();
         }
      }
   }

}</pre>

   <p> When this program was executed on a Mac/Intel/cc/64-bit/Java 1.8
   platform, the output was:

   <pre>

   AZ/EL coordinates:

      Range      (km)        =   245721478.99272084
      Azimuth    (deg)       =         294.48543372
      Elevation  (deg)       =         -48.94609726

   AZ/EL velocity:

      d Range/dt     (km/s)  =          -4.68189834
      d Azimuth/dt   (deg/s) =           0.00402256
      d Elevation/dt (deg/s) =          -0.00309156

   Rectangular coordinates:

      X (km)                 =    66886767.37916667
      Y (km)                 =   146868551.77222887
      Z (km)                 =  -185296611.10841590

   Rectangular velocity:

      dX/dt (km/s)           =        6166.04150307
      dY/dt (km/s)           =      -13797.77164550
      dZ/dt (km/s)           =       -8704.32385654

   Rectangular coordinates from inverse mapping:

      X (km)                 =    66886767.37916658
      Y (km)                 =   146868551.77222890
      Z (km)                 =  -185296611.10841590

   Rectangular velocity from inverse mapping:

      dX/dt (km/s)           =        6166.04150307
      dY/dt (km/s)           =      -13797.77164550
      dZ/dt (km/s)           =       -8704.32385654

   </pre>


   </li>
   </ol>

   */
   public Matrix33 getAZELRecJacobian()

      throws SpiceException
   {
      double[][] retMat = CSPICE.drdazl ( range,
                                          azimuth,
                                          elevation,
                                          azimuthCounterclockwise,
                                          elevationPlusZ          );

      return (  new Matrix33( retMat )  );
   }


   //
   // Static methods
   //


   /**
   Return the Jacobian matrix of the rectangular-to-AZ/EL coordinate
   transformation at the point specified by a 3-vector.

   <h2>Inputs</h2>
   <pre>

   v           is a {@link Vector3} instance containing the rectangular
               (Cartesian) coordinates of the point at which the
               Jacobian matrix of the map from rectangular
               to azimuth/elevation coordinates is desired.

   azccw       is a flag indicating how azimuth is measured.

               If `azccw' is true, azimuth increases in the
               counterclockwise direction; otherwise
               it increases in the clockwise direction.

   elplsz      is a flag indicating how elevation is measured.

               If `elplsz' is true, the elevation increases
               from the XY plane toward +Z; otherwise toward -Z.
   </pre>

   <h2>Outputs</h2>

   <pre>

   The returned {@link Matrix33} instance is the matrix of partial derivatives
   of the transformation from rectangular to azimuth/elevation
   coordinates. It has the form

      .-                            -.
      |  dr/dx     dr/dy     dr/dz   |
      |  daz/dx    daz/dy    daz/dz  |
      |  del/dx    del/dy    del/dz  |
      `-                            -'

   evaluated at the Cartesian coordinates represented by the Vector3
   instance `v`.
   </pre>

   <h2>Examples</h2>

   See the example program for
   {@link AZELCoordinates#getAZELRecJacobian} above.

   */
   public static Matrix33 getRecAZELJacobian( Vector3 v,
                                              boolean azccw,
                                              boolean elplsz )

      throws SpiceException
   {
      double[]   a      = v.toArray();

      double[][] retMat = CSPICE.dazldr ( a[0], a[1], a[2],
                                          azccw,
                                          elplsz           );

      return (  new Matrix33( retMat )  );
   }

}
