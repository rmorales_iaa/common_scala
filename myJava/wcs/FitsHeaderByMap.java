package wcs;
//-----------------------------------------------------------------------------
import java.util.Map;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
public class FitsHeaderByMap implements WCSKeywordProvider {
	//-------------------------------------------------------------------------
	private Map<String, String> map;
	private static MyGetValue myGet = new MyGetValue();
	private Boolean logQuery = false;
	//-------------------------------------------------------------------------
	public FitsHeaderByMap(Map <String, String> _map){
		map = _map;
	}
	//-------------------------------------------------------------------------
    @Override
    public final boolean findKey(final String key) {
    	if (logQuery) System.out.println("key->" + key + " value->" + map.containsKey(key));  
       return map.containsKey(key);
    }

    //-------------------------------------------------------------------------
    @Override
    public final String getStringValue(final String key) {
    	if (logQuery) System.out.println("key->" + key + " value->" + map.get(key));
    	String s = map.get(key);
    	if (s != null) return s.replace("'", "");
        else return null;
    }

    //-------------------------------------------------------------------------
    @Override
    public final String getStringValue(final String key, final String defaultValue) {
    	if (logQuery) System.out.println("key->" + key + " value->" + map.get(key));  
    	if (!map.containsKey(key)) return defaultValue;
        return map.get(key).replace("'", "");
    }

    //-------------------------------------------------------------------------
    @Override
    public final double getDoubleValue(final String key) {
    	if (logQuery) System.out.println("key->" + key + " value->" + myGet.get(Double.class, 0.d, map.get(key)));
        return myGet.get(Double.class, 0.d, map.get(key));  
    }

    //-------------------------------------------------------------------------
    @Override
    public final double getDoubleValue(final String key, final double defaultValue) {
    	if (logQuery) System.out.println("key->" + key + " value->" + myGet.get(Double.class, defaultValue, map.get(key)));       
        return myGet.get(Double.class, defaultValue, map.get(key));       
    }

    //-------------------------------------------------------------------------
    @Override
    public final float getFloatValue(final String key) {
    	if (logQuery) System.out.println("key->" + key + " value->" + myGet.get(Float.class, 0.f, map.get(key)));  
    	return myGet.get(Float.class, 0.f, map.get(key));  
    }

    //-------------------------------------------------------------------------
    @Override
    public final float getFloatValue(final String key, final float defaultValue) {
    	if (logQuery) System.out.println("key->" + key + " value->" + myGet.get(Float.class, defaultValue, map.get(key))); 
    	return myGet.get(Float.class, defaultValue, map.get(key));  
    }

    //-------------------------------------------------------------------------
    @Override
    public final int getIntValue(final String key) {
    	if (logQuery) System.out.println("key->" + key + " value->" + myGet.get(Integer.class, 0, map.get(key)));
    	return myGet.get(Integer.class, 0, map.get(key));
    }

    //-------------------------------------------------------------------------
    @Override
    public final int getIntValue(final String key, final int defaultValue) {
    	if (logQuery) System.out.println("key->" + key + " value->" + myGet.get(Integer.class, defaultValue, map.get(key)));
    	return myGet.get(Integer.class, defaultValue, map.get(key));
    }
    //-------------------------------------------------------------------------    
}
//-----------------------------------------------------------------------------
