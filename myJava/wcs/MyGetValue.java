package wcs;
//-----------------------------------------------------------------------------
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.regex.Pattern;

//-----------------------------------------------------------------------------
//adapted from nom.tam.fits.HeaderCard
//-----------------------------------------------------------------------------
public class MyGetValue {

	//-------------------------------------------------------------------------	
	private static final Pattern DBLSCI_REGEX = Pattern.compile("[+-]?(?=\\d*[.dD])(?=\\.?\\d)\\d*\\.?\\d*(?:[dD][+-]?\\d+)?");	
	//-------------------------------------------------------------------------
    public Boolean getBooleanValue(Boolean defaultValue, String value) {
        if ("T".equals(value)) {
            return Boolean.TRUE;
        } else if ("F".equals(value)) {
            return Boolean.FALSE;
        }
        return defaultValue;
    }
    //-------------------------------------------------------------------------
    public <T> T get(Class<T> clazz, T defaultValue, String value) {
        if (String.class.isAssignableFrom(clazz)) {
            return clazz.cast(value);
        } else if (value == null || value.isEmpty()) {
            return defaultValue;
        } else if (Boolean.class.isAssignableFrom(clazz)) {
            return clazz.cast(getBooleanValue((Boolean) defaultValue, value));
        }

        // Convert the Double Scientific Notation specified by FITS to pure IEEE.
        if (DBLSCI_REGEX.matcher(value).find()) {
            value = value.replace('d', 'e');
            value = value.replace('D', 'E');
        }

        BigDecimal parsedValue;
        try {
            parsedValue = new BigDecimal(value);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
        if (Integer.class.isAssignableFrom(clazz)) {
            return clazz.cast(parsedValue.intValueExact());
        } else if (Long.class.isAssignableFrom(clazz)) {
            return clazz.cast(parsedValue.longValueExact());
        } else if (Double.class.isAssignableFrom(clazz)) {
            return clazz.cast(parsedValue.doubleValue());
        } else if (Float.class.isAssignableFrom(clazz)) {
            return clazz.cast(parsedValue.floatValue());
        } else if (BigDecimal.class.isAssignableFrom(clazz)) {
            return clazz.cast(parsedValue);
        } else if (BigInteger.class.isAssignableFrom(clazz)) {
            return clazz.cast(parsedValue.toBigIntegerExact());
        } else {
            throw new IllegalArgumentException("unsupported class " + clazz);
        }
    }
    //-------------------------------------------------------------------------	
	
}
//-----------------------------------------------------------------------------
