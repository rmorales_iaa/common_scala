package wcs;

public abstract interface Coordinates {

    /** Returns the name of the coordinate system as a string. */
    public String getCoordinateSystemName();

    /**
     * Return the coordinates as a string.
     */
    public String toString();

    /**
     * Return the distance between this position and the given one in
     * the standard unit of the coordinate system being used (arcmin
     * for WorldCoords, pixels for ImageCoords, ...).
     *
     * @param pos The other point.
     *
     * @return The distance to the given point.
     */
    public double dist(Coordinates pos);

    /** return the X coordinate as a double */
    public double getX();

    /** return the Y coordinate as a double */
    public double getY();
}