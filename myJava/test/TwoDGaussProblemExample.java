/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

package test;

import org.apache.commons.math3.fitting.leastsquares.LeastSquaresBuilder;
import org.apache.commons.math3.fitting.leastsquares.LeastSquaresOptimizer;
import org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer;


/**
 * This is the test program for two dimensional Gaussian distribution.function fitting.
 * @author araiyoshiyuki
 * @date 07/01/2015
 */
public class TwoDGaussProblemExample {

    public void run() {

        /*
        //entry the dataByteSeq (5x5)
        double[] inputdata = {
                0  ,12 ,25 ,12 ,0  ,
                12 ,89 ,153,89 ,12 ,
                25 ,153,255,153,25 ,
                12 ,89 ,153,89 ,12 ,
                0  ,12 ,25 ,12 ,0  ,
        };
        int x_width = 5; // width of x*/

        double[] inputdata = {
                9096,  9604,  9578,  9484,  8961,
                9220,  9960, 10006,  9787,  9308,
                9316,  9741,  9877,  9777,  9426,
                9183,  9471,  9689,  9154,  9195,
                8856,  9134,  9125,  8884,  8782
        };
        int x_width = 5; // width of x


        //set initial parameters
        double[] newStart = {
                10006.0,
                1.9964963727654785,
                2.0233276786551526,
                1.1235471333138825,
                1.1235471333138825,
                9316.0
        };

        //construct two-dimensional Gaussian distribution.function
        test.TwoDGaussianFunction tdgf = new test.TwoDGaussianFunction(x_width,inputdata.length);

        //prepare construction of LeastSquresProblem by builder
        LeastSquaresBuilder lsb = new LeastSquaresBuilder();

        //set model distribution.function and its jacobian
        lsb.model(tdgf.retMVF(), tdgf.retMMF());

        //set target dataByteSeq
        lsb.target(inputdata);


        lsb.start(newStart);
        //set upper limit of evaluation time
        lsb.maxEvaluations(1000);
        //set upper limit of iteration time
        lsb.maxIterations(100);

        LevenbergMarquardtOptimizer lmo = new LevenbergMarquardtOptimizer();
        try{
            //do LevenbergMarquardt optimization
            LeastSquaresOptimizer.Optimum lsoo = lmo.optimize(lsb.build());

            //get optimized parameters
            final double[] optimalValues = lsoo.getPoint().toArray();
            //output dataByteSeq
            System.out.println("v0: " + optimalValues[0]);
            System.out.println("v1: " + optimalValues[1]);
            System.out.println("v2: " + optimalValues[2]);
            System.out.println("v3: " + optimalValues[3]);
            System.out.println("v4: " + optimalValues[4]);
            System.out.println("v5: " + optimalValues[5]);
            System.out.println("Iteration number: "+lsoo.getIterations());
            System.out.println("Evaluation number: "+lsoo.getEvaluations());
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}