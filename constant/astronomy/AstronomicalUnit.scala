/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  21/Jun/2021 
 * Time:  20h:08m
 * Description: None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.constant.astronomy
//=============================================================================
//=============================================================================
object AstronomicalUnit {
  //---------------------------------------------------------------------------
  final val JD_2000_EPOCH = 2451545d
  //---------------------------------------------------------------------------
  final val JD_PER_CENTURY    = 36525d
  final val JD_PER_MILLENNIUM = JD_PER_CENTURY * 10
  //---------------------------------------------------------------------------
  final val PRECISE_SIN_OBLIQ_J2000 = 0.397777155931913701597179975942380896684
  final val PRECISE_COS_OBLIQ_J2000 = 0.917482062069181825744000384639406458043
  //---------------------------------------------------------------------------
  final val LIGHT_SPEED_KM_S = 299792.458D //km/s. Source: jpl horizons
  //---------------------------------------------------------------------------
  final val ASTRONOMICAL_UNIT_M       =     149597870700d //meters. Source: jpl horizons
  final val ASTRONOMICAL_UNIT_KM      = ASTRONOMICAL_UNIT_M / 1000    //kilometers. Source: jpl horizons
  final val ASTRONOMICAL_UNIT_PER_DAY =  86400D * LIGHT_SPEED_KM_S / ASTRONOMICAL_UNIT_KM
  //---------------------------------------------------------------------------
  //source AST library: http://starlink.eao.hawaii.edu/starlink/AST
  final val PRECISE_DEGREES_TO_RADIANS: Double = 0.017453292519943295769236907684886127134428718885417
  final val PRECISE_RADIANS_TO_DEGREES: Double = 57.295779513082320876798154814105170332405472466564
  //---------------------------------------------------------------------------
  final val PRECISE_PI           = 3.141592653589793238462643383279502884197169399375105
  final val PRECISE_TWO_PI       = PRECISE_PI + PRECISE_PI
  final val PRECISE_HALF_PI      = 1.5707963267948966192313216916397514420985846996876

  final val PRECISE_ANGLE_TO_RAD = PRECISE_PI / 180d
  //---------------------------------------------------------------------------
  final val PRECISE_SQRT_2 = 1.4142135623730950488016887242096980785696718753769480731766797
  //---------------------------------------------------------------------------
  final val GAUSS_K = 0.01720209895
  final val SOLAR_GM = GAUSS_K * GAUSS_K
  //---------------------------------------------------------------------------
  final val DBL_EPSILON      = Math.ulp(1.0)
  //---------------------------------------------------------------------------
  final val EARTH_MOON_RATIO = 81.30056
  //---------------------------------------------------------------------------
  val PLANET_RELATIVE_MASS: Array[Double] = Array(
    1.0
    , 1.660136795271931e-007                    // Mercury
    , 2.447838339664545e-006                    // Venus
    , 3.003489596331057e-006                    // Earth
    , 3.227151445053866e-007                    // Mars
    , 0.0009547919384243268                     // Jupiter
    , 0.0002858859806661309                     // Saturn
    , 4.366244043351564e-005                    // Uranus
    , 5.151389020466116e-005                    // Neptune
    , 7.396449704142013e-009                    // Pluto
    , 3.003489596331057e-006 / EARTH_MOON_RATIO // Moon
    , 4.7622e-10                                //Ceres
    , 1.0775e-10                                //Pallas
    , 1.3412e-10                                //Vesta
  )
  final val MERCURY_AU_RADIUS  = 2439.4 / ASTRONOMICAL_UNIT_KM
  final val VENUS_AU_RADIUS    = 6051.0 / ASTRONOMICAL_UNIT_KM
  final val EARTH_AU_RADIUS    = 6378.140 / ASTRONOMICAL_UNIT_KM
  final val MARS_AU_RADIUS     = 3397.0 / ASTRONOMICAL_UNIT_KM
  final val JUPITER_AU_RADIUS  = 71492.0 / ASTRONOMICAL_UNIT_KM
  final val SATURN_AU_RADIUS   = 60330.0 / ASTRONOMICAL_UNIT_KM
  final val URANUS_AU_RADIUS   = 25559.0 / ASTRONOMICAL_UNIT_KM
  final val NEPTUNE_AU_RADIUS  = 25225.0 / ASTRONOMICAL_UNIT_KM
  final val PLUTO_AU_RADIUS    = 1500.0 / ASTRONOMICAL_UNIT_KM
  final val MOON_AU_RADIUS     = 1748.2 / ASTRONOMICAL_UNIT_KM

  final val PLANET_RADIUS = Seq(
    MERCURY_AU_RADIUS
    , VENUS_AU_RADIUS
    , EARTH_AU_RADIUS
    , MARS_AU_RADIUS
    , JUPITER_AU_RADIUS
    , SATURN_AU_RADIUS
    , URANUS_AU_RADIUS
    , NEPTUNE_AU_RADIUS
    , PLUTO_AU_RADIUS
    , MOON_AU_RADIUS
  )
  //---------------------------------------------------------------------------
  //https://github.com/Bill-Gray/lunar/blob/master/delta_t.cpp#L113
  val DELTA_T_TABLE: Array[Short] = Array(
    12400, 11500, 10600, 9800, 9100, 8500, 7900, /*  1620-1632 */
    7400, 7000, 6500, 6200, 5800, 5500, 5300, /*  1634-1646 */
    5000, 4800, 4600, 4400, 4200, 4000, 3700, /*  1648-1660 */
    3500, 3300, 3100, 2800, 2600, 2400, 2200, /*  1662-1674 */
    2000, 1800, 1600, 1400, 1300, 1200, 1100, /*  1676-1688 */
    1000, 900, 900, 900, 900, 900, 900, /*  1690-1702 */
    900, 900, 1000, 1000, 1000, 1000, 1000, /*  1704-1716 */
    1100, 1100, 1100, 1100, 1100, 1100, 1100, /*  1718-1730 */
    1100, 1200, 1200, 1200, 1200, 1200, 1300, /*  1732-1744 */
    1300, 1300, 1300, 1400, 1400, 1400, 1500, /*  1746-1758 */
    1500, 1500, 1500, 1600, 1600, 1600, 1600, /*  1760-1772 */
    1600, 1700, 1700, 1700, 1700, 1700, 1700, /*  1774-1786 */
    1700, 1700, 1600, 1600, 1500, 1400, 1370, /*  1788-1800 */
    1310, 1270, 1250, 1250, 1250, 1250, 1250, /*  1802-1814 */
    1250, 1230, 1200, 1140, 1060, 960, 860, /*  1816-1828 */
    750, 660, 600, 570, 560, 570, 590, /*  1830-1842 */
    620, 650, 680, 710, 730, 750, 770, /*  1844-1856 */
    780, 790, 750, 640, 540, 290, 160, /*  1858-1870 */
    -100, -270, -360, -470, -540, -520, -550, /*  1872-1884 */
    -560, -580, -590, -620, -640, -610, -470, /*  1886-1898 */
    -270, 0, 260, 540, 770, 1050, 1340, /*  1900-1912 */
    1600, 1820, 2020, 2120, 2240, 2350, 2390, /*  1914-1926 */
    2430, 2400, 2390, 2390, 2370, 2400, 2430, /*  1928-1940 */
    2530, 2620, 2730, 2820, 2910, 3000, 3070, /*  1942-1954 */
    3140, 3220, 3310, 3400, 3500, 3650, 3830, /*  1956-1968 */
    4020, 4220, 4450, 4650, 4850, 5050, 5220, /*  1970-1982 */
    5380, 5490, 5580, /*  1984-1988 */
    5686, /* 1990  1 1    56.8554     .3286        -24.6714 */
    5831, /* 1992  1 1    58.3093    -.1253        -26.1253 */
    5998, /* 1994  1 1    59.9847     .1993        -27.8007 */
    6163, /* 1996  1 1    61.6287     .5553        -29.4447 */
    6297, /* 1998  1 1    62.9659     .2181        -30.7819 */
    6383, /* 2000  1 1    63.8285268  .3554732     -31.6445268 */
    6430, /* 2002  1 1    64.2998152 -.1158152     -32.1158152 */
    6457, /* 2004  1 1    64.5736400 -.3896400     -32.3896400 */
    6485, /* 2006  1 1    64.8452                              */
    6546, /* 2008  1 1:   65.4574                              */
    6607, /* 2010  1 1:   66.0699                              */
    6660, /* 2012  1 1:   66.6030                              */
    6728, /* 2014  1 1:   67.2810                              */
    6810, /* 2016  1 1:   68.1024                              */
    6897, /* 2018  1 1:   68.9677                              */
    6936, /* 2020  1 1:   UT1 - UTC = -0.1772554; 69.3612554   */
    6929, /* 2022  1 1:   UT1 - UTC = -0.1104988; 69.2944988   */
    6918  /* 2024  1 1:   UT1 - UTC =  0.0087688; 69.1752068    */
  )
  //---------------------------------------------------------------------------
  //https://github.com/Bill-Gray/lunar/blob/master/mpc_code.cpp
  /* Given an ellipse with semimajor axis a,  semiminor axis b,  centered
  at the origin,  and an arbitrary point (x, y),  point_to_ellipse() will
  compute the closest distance between that point and the ellipse,  and
  the angle to the ellipse.

     This is an exact method from _Explanatory Supplement to the Astronomical
  Almanac_, pgs 206-207,  in turn from K. M. Borkowski (1989), "Accurate
  Algorithms to Transform Geocentric to Geodetic Coordinates",  _Bulletin
  Geodesique_ 63, no. 1, 50-56,  modified slightly to handle the possibilities
  of negative x and/or y.  It is also described at

  http://www.astro.uni.torun.pl/~kb/Papers/ASS/Geod-ASS.htm

  This reduces the problem to finding the roots of a quartic polynomial,
  but does so in a form that is somewhat straightforward,  with unit
  leading and trailing coefficients and a zero quadratic coefficient.

  References are to the _Explanatory Supplement_ and then the above URL.
  For example,  the equation for 'e' is given at 4.22-12 in the ES and
  as equation (6) at the above URL.

  The same point-to-ellipse problem can come up in computing MOIDs, which
  is why the function is not of type static : it's used in moid.cpp. */
 //(latitude in radians, altitude). Multiply the altitude by EARTH_MINOR_AXIS_M to get meters
  def pointToEllipse(a: Double
                     , b: Double
                     , x: Double
                     , y: Double): (Double, Double) = {
    val fy = math.abs(y)
    val fx = math.abs(x)
    var lat = 0.0
    var dist = 0.0

    if (x == 0.0) {
      lat = PRECISE_HALF_PI
      dist = fy - a
    } else {
      val cSquared = a * a - b * b
      val e = (b * fy - cSquared) / (a * fx)      // 4.22-12/6
      val f = (b * fy + cSquared) / (a * fx)      // 4.22-13/7
      val p = (4.0 / 3.0) * (e * f + 1.0)         // 4.22-14/9
      val q = 2.0 * (e * e - f * f)               // 4.22-15/10
      val d = p * p * p + q * q                   // 4.22-16/12
      var v = 0.0
      var g = 0.0
      var t = 0.0

      if (d >= 0.0) {
        val sqrtD = math.sqrt(d)
        v = math.cbrt(sqrtD - q) - math.cbrt(sqrtD + q)     // 4.22-17/11a
      } else {
        val sqp = math.sqrt(-p)
        val tempAng = math.acos(q / (sqp * p))
        v = 2.0 * sqp * math.cos(tempAng / 3.0)        // 11b
      }

      g = (math.sqrt(e * e + v) + e) * 0.5             // 4.22-18/14
      t = math.sqrt(g * g + (f - v * g) / (2.0 * g - e)) - g // 4.22-19/13
      lat = math.atan2(a * (1.0 - t * t), 2.0 * b * t) // 4.22-20/15a

      dist = (fx - a * t) * math.cos(lat) + (fy - b) * math.sin(lat) // 4.22-21/15b
    }

    if (x < 0.0) lat = PRECISE_PI - lat
    if (y < 0.0) lat = -lat

    (lat, dist)
  }
  //---------------------------------------------------------------------------
  //https://github.com/Bill-Gray/lunar/blob/master/miscell.cpp
  /* Greenwich sidereal time from UT.  Based on Meeus' _Astronomical
   Algorithms_,  pp 87-88 (2nd edition).  Note that UT0 should be
   used,  the version that reflects the earth's current rotational
   state.  (UTC comes close,  but leap seconds are inserted so that
   each second can be equal in length,  rather than "stretching" each
   second as the earth slows down.  The difference is kept within one
   second,  and can be ignored for many purposes.)   *//* Greenwich sidereal time from UT.  Based on Meeus' _Astronomical
   Algorithms_,  pp 87-88 (2nd edition).  Note that UT0 should be
   used,  the version that reflects the earth's current rotational
   state.  (UTC comes close,  but leap seconds are inserted so that
   each second can be equal in length,  rather than "stretching" each
   second as the earth slows down.  The difference is kept within one
   second,  and can be ignored for many purposes.)   */

  def greenSiderealTime(jdUt: Double): Double = {
    var jdUtAdjusted = jdUt - JD_2000_EPOCH  // set relative to 2000.0
    val tCen = jdUtAdjusted / JD_PER_CENTURY   // convert to Julian centuries
    val baseT = math.floor(jdUtAdjusted)
    jdUtAdjusted -= baseT

    val rval = 280.46061837 +
      360.98564736629 * jdUtAdjusted +
      0.98564736629 * baseT +
      tCen * tCen * (3.87933e-4 - tCen / 38710000.0)

    // Convert to radians (multiply by Pi / 180)
    rval * PRECISE_DEGREES_TO_RADIANS
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file AstronomicalUnit.scala
//=============================================================================
