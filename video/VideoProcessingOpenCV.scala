/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Nov/2018
 * Time:  14h:38m
 * Description: None
 */
//=============================================================================
package com.common.video
//=============================================================================
import org.bytedeco.javacv.{FFmpegFrameGrabber, Frame}
//=============================================================================
import org.bytedeco.ffmpeg.global.avutil
//=============================================================================
case class VideoProcessingOpenCV(s: String, userProcess: Frame => Boolean) extends MyLogger {
  //-----------------------------------------------------------------------
  private val startMs = System.currentTimeMillis
  //-----------------------------------------------------------------------
  private val frameGrabber = new FFmpegFrameGrabber(s)
  private var frameCount: Int = 0
  private var frameWidth : Int = 0
  private var frameHeight: Int = 0
  //-----------------------------------------------------------------------
  def getFrameWidth = frameWidth
  //-----------------------------------------------------------------------
  def getFrameHeight = frameHeight
  //-----------------------------------------------------------------------
  def process(): Boolean = {
    //-----------------------------------------------------------------------
    def printInfo(s: String): Unit = {

      val videoMetadata = frameGrabber.getVideoMetadata

      info(s"Video file           : $s")
      info(s"Creation time        : ${videoMetadata.get("creation_time")}")
      info(s"Codec                : ${videoMetadata.get("encoder")}")
      info(s"Frame count          : $frameCount")
      info(s"Duration in s        : ${frameGrabber.getLengthInTime / 1000000f}")
      info(s"Frame rate           : ${frameGrabber.getFrameRate}")
      info(s"Frame pix size (WxH) : $frameWidth x $frameHeight")
    }
    //-----------------------------------------------------------------------
    //processing vars
    var frame: Frame = null
    //-----------------------------------------------------------------------
    frameGrabber.start
    frameCount  = frameGrabber.getLengthInFrames
    frameWidth  = frameGrabber.getImageWidth
    frameHeight = frameGrabber.getImageHeight

    printInfo(s)

    for (_ <- 0 until frameCount) {
      frame = frameGrabber.grabImage
      if ((frame == null) || !userProcess(frame) ){
        logger.info("Video processing ends. Elapsed time: " + Time.getFormattedElapseTimeFromStart(startMs))
        return false
      }
    }
    //logger.info("Video processing ends. Elapsed time: " + Time.getFormattedElapseTimeFromStart(startMs))
    println("Video processing ends. Elapsed time: " + Time.getFormattedElapseTimeFromStart(startMs))
    frameGrabber.release
    frameGrabber.stop
    true
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//=============================================================================
//End of file VideoProcessingOpenCV.scala
//=============================================================================