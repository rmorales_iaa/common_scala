//=============================================================================
//File: control.scala
//=============================================================================
/** It implements NEW control structures for commonHenosis. See object declaration
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.control
//=============================================================================
// System import sectionName
//=============================================================================
import scala.util.{Failure, Success, Try}
import scala.language.reflectiveCalls

//=============================================================================
// User import sectionName
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================
/** Useful control structures */
object Control extends MyLogger {

  //-------------------------------------------------------------------------
  // class variable
  //-------------------------------------------------------------------------

  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
  
  //-------------------------------------------------------------------------
  /** Uses and closes and object
   * @param resource is a object with type A that must has defined the method 'def close(): Unit'
   * @param f a distribution.function that transforms type A in type B
   * @return Object of type B when success or Failure in other case
   */
  //-------------------------------------------------------------------------
  def withClose[A <: { def close(): Unit }, B](resource: A)(f: A => B): Try[B] = {
    Try(f(resource)) match { //call distribution.function with the parameter. It is executed inside the apply method of the Try object
      case Success(v) =>
        resource.close //run close method on the parameter in any case
        Success(v)

      case Failure(e) =>
        resource.close//run close method on the parameter in any case
        error(e.toString)
        error("Error in 'Control.withClose' method. Function: '"+f.toString()+"' parameter: '"+resource+"'")
        Failure(e)
    }
  }
  
  //-------------------------------------------------------------------------
  def withDisconnect[A <: { def disconnect(): Unit }, B](resource: A)(f: A => B): Try[B] = {

    Try(f(resource)) match { //call distribution.function with the parameter. It is executed inside the apply method of the Try object
      case Success(v) =>
        resource.disconnect //run disconnect method on the parameter in any case
        Success(v)

      case Failure(e) =>
        resource.disconnect //run disconnect method on the parameter in any case
        error(e.toString)
        error("Error in 'Control.withDisconnect' method. Function: '"+f.toString()+"' parameter: '"+resource+"'")
        Failure(e)
    }
  }
  //-------------------------------------------------------------------------
} //End of object 'control'

//=============================================================================
// End of 'control' file
//=============================================================================
