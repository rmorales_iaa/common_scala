/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  11/Jan/2023 
 * Time:  11h:23m
 * Description: None
 */
//=============================================================================
package com.common.csv
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
import java.util.concurrent.atomic.AtomicLong
import scala.util.{Failure, Success, Try}
//=============================================================================
import java.io.{BufferedWriter, FileWriter}
//=============================================================================
object CsvWrite {
  //---------------------------------------------------------------------------
  def getCharOrDefault(s: String, default: Char = ' ') =
    if (s.isEmpty || s.trim.isEmpty || s.trim == "null") default else s.head
  //-------------------------------------------------------------------------
  def getBooleanOrDefault(s: String, default: Boolean = false): Boolean = {
    if (s.isEmpty || s.trim.isEmpty || s.trim == "null") return default
    if (s.toLowerCase() == "t") true
    else if (s.toLowerCase() == "1") true
    else if (s.toLowerCase() == "true") true
    else false
  }
  //-------------------------------------------------------------------------
  def getByteOrDefault(s: String, default: Byte = Byte.MinValue): Byte = {
    if (s.isEmpty || s.trim.isEmpty || s.trim == "null") return default
    try {
      s.trim.toByte
    }
    catch {
      case _: Exception => default
    }
  }
  //-------------------------------------------------------------------------
  def getShortOrDefault(s: String, default: Short = Short.MinValue): Short = {
    if (s.isEmpty || s.trim.isEmpty || s.trim == "null") return default
    try {
      s.trim.toShort
    }
    catch {
      case _: Exception => default
    }
  }
  //-------------------------------------------------------------------------
  def getIntOrDefault(s: String, default: Int = Int.MinValue): Int = {
    if (s.isEmpty || s.trim.isEmpty || s.trim == "null") return default
    try {
      s.trim.toInt
    }
    catch {
      case _: Exception => default
    }
  }
  //-------------------------------------------------------------------------
  def getLongOrDefault(s: String, default: Long = Long.MinValue): Long = {
    if (s.isEmpty || s.trim.isEmpty || s.trim == "null") return default
    try {
      s.trim.toLong
    }
    catch {
      case _: Exception => default
    }
  }
  //-------------------------------------------------------------------------
  def getFloatOrDefault(s: String, default: Float = Float.NaN): Float = {
    if (s.isEmpty || s.trim.isEmpty || s.trim == "null") return default
    try {
      s.trim.toFloat
    }
    catch {
      case _: Exception => default
    }
  }
  //-------------------------------------------------------------------------
  def getDoubleOrDefault(s: String, default: Double = Double.NaN): Double = {
    if (s.isEmpty || s.trim.isEmpty || s.trim == "null") return default
    try {
      s.trim.toDouble
    }
    catch {
      case _: Exception => default
    }
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
case class CsvWrite(fileName: String
                    , headerRow: CsvRow = CsvRow() //indicated the column order in the row
                    , appendWrite: Boolean = false
                    , immediatelyWrite: Boolean = false
                    , itemDivider:String = "\t"
                    , lineSeparator: String = System.getProperty("line.separator","\n"))
      extends MyLogger {
  //---------------------------------------------------------------------------
  private var writer : BufferedWriter = _
  private val columnNameSeq = headerRow.getColNameSeq()
  val writtenRowCount = new AtomicLong(-1)
  private var isClosed = false
  //---------------------------------------------------------------------------
  private def write(s: String): Boolean = {
    Try {
      synchronized {
        writer.write(s)
        if (immediatelyWrite) flush()
      }
    }
    match {
      case Success(_) => true
      case Failure(e: Exception) =>
        exception(e, s"CsvWrite: Error writting in CSV file '$fileName'")
    }
  }
  //-------------------------------------------------------------------------
  private def writeLine(s: String): Boolean = write(s + lineSeparator)
  //-------------------------------------------------------------------------
  def close(verbose: Boolean = true): Boolean = {
    if (writer == null) {
      synchronized{writer = new BufferedWriter(new FileWriter(fileName, appendWrite))}
      if (verbose) warning(s"CsvWrite: Csv file:'$fileName' is empty")
      writeHeader()
    }
    //at least write header
    Try {
      synchronized { writer.close(); isClosed = true }
    }
    match {
      case Success(_) => true
      case Failure(e: Exception) =>
        exception(e, s"CSV write: error closing CSV file '$fileName'")
    }
  }

  //-------------------------------------------------------------------------
  private def flush(): Unit = synchronized { writer.flush() }
  //---------------------------------------------------------------------------
  def writeAndClose(row: CsvRow): Unit = writeAndClose(Seq(row))
  //---------------------------------------------------------------------------
  def writeAndClose(rowSeq: Seq[CsvRow]): Unit = {
    //write content
    append(rowSeq)

    //close file
    close()
  }
  //---------------------------------------------------------------------------
  def append(rowSeq: Seq[CsvRow]): Unit = {
    if (writtenRowCount.get() == -1) {
      synchronized {
        //create file
        writer = new BufferedWriter(new FileWriter(fileName, appendWrite))

        //write header
        if (!appendWrite) writeHeader()
      }
    }
    rowSeq foreach { row =>
      writeLine(row.getAsString(columnNameSeq, itemDivider))
      writtenRowCount.getAndAdd(1)
    }
  }
  //---------------------------------------------------------------------------
  def append(row: CsvRow): Unit = append(Seq(row))
  //---------------------------------------------------------------------------
  private def writeHeader() =
  if (!columnNameSeq.isEmpty) writeLine(columnNameSeq.mkString(itemDivider))
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CsvFile.scala
//=============================================================================
