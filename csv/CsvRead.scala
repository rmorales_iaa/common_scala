/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  11/Jan/2023 
 * Time:  11h:23m
 * Description: None
 */
//=============================================================================
package com.common.csv
//=============================================================================
//=============================================================================
import com.common.csv.CsvDataType.CSV_DATA_TYPE_NONE
import com.common.logger.MyLogger
import com.common.util.file.MyFile
//=============================================================================
import java.io.{BufferedReader, FileReader}
import scala.collection.mutable.ArrayBuffer
//=============================================================================
object CsvRead extends MyLogger {
  //---------------------------------------------------------------------------
  private final val SCALA_RESERVED_WORDS = Set("abstract", "case", "catch", "class", "def", "do", "else", "extends", "false", "final", "finally", "for", "if", "implicit", "import", "lazy", "match", "new", "null", "object", "override", "package", "private", "protected", "return", "sealed", "super", "this", "throw", "trait", "try", "true", "type", "val", "var", "while", "with", "yield")
  //---------------------------------------------------------------------------
  //adapted from chat gpt
  def normalizeAsScalaVariableName(s:String): String= {
    val regex = "[^a-zA-Z0-9_]".r
    val step1 = regex.replaceAllIn(s, "")
    val step2 = if (step1.headOption.exists(_.isDigit)) "_" + step1 else step1
    val step3 = step2.replaceAll("\\s+", "_")
    val r = step3.toLowerCase
    SCALA_RESERVED_WORDS.foreach { word=>
      if (r.startsWith(word)) return "_" + r
    }
    r
  }
  //---------------------------------------------------------------------------
  def apply(other: CsvRead): CsvRead = {
    val newCsv = CsvRead(other.fileName
      , other.hasHeader
      , other.commentLinePrefix
      , other.itemDivider)
    newCsv.setColNameSeq(other.getColNameSeq)
    other.rowSeq foreach (newCsv + _)
    newCsv
  }
  //---------------------------------------------------------------------------
  def readCsv(csvFileName: String
             , hasHeader: Boolean  = true
             , itemDivider:String = "\t"
             , verbose: Boolean = false) = {

    val csvRead = CsvRead(csvFileName, hasHeader, itemDivider = itemDivider)

    if (verbose) info(s"Loading csv '$csvFileName'")
    if (!MyFile.fileExist(csvFileName))
      warning(s"CSV '$csvFileName' does not exists, assuming 0 rows")
    else {
      csvRead.read()
      if (verbose) info(s"Loaded csv '$csvFileName' with ${csvRead.getRowSeq.length} rows")
    }
    csvRead
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class CsvRead(fileName: String
                   , hasHeader: Boolean  = true
                   , commentLinePrefix: Seq[String] = Seq()
                   , itemDivider:String = "\t"
                   , skipFirstLineCount: Int = 0)
  extends MyLogger {
  //---------------------------------------------------------------------------
  private var reader : BufferedReader = _
  private var readRowCount = 0L
  private val rowSeq = ArrayBuffer[CsvRow]()
  private var colNameSeq: Seq[String] = null
  private var minRowCharSize = Long.MaxValue
  private var maxRowCharSize = Long.MinValue
  //---------------------------------------------------------------------------
  def apply(name: String) = rowSeq map ( row => row(name) )
  //---------------------------------------------------------------------------
  def getRowSeq = rowSeq.toArray
  //---------------------------------------------------------------------------
  def getRowCount = rowSeq.size
  //---------------------------------------------------------------------------
  def getRowMinSize = minRowCharSize
  //---------------------------------------------------------------------------
  def getRowMaxSize = maxRowCharSize
  //---------------------------------------------------------------------------
  def getRowSeq(columnName: String, value: String) =
    rowSeq filter ( row=> row(columnName) == value)
  //---------------------------------------------------------------------------
  def getCol(columnName: String) =
    rowSeq map ( row=> row(columnName) )
  //---------------------------------------------------------------------------
  def getColSeq(columnNameSeq: Seq[String]) =
    rowSeq map (row => columnNameSeq map (name=> row(name)) )
  //---------------------------------------------------------------------------
  def getUniqueValueSeq(columnName: String) =
    (rowSeq map (row => row(columnName))).toSet
  //---------------------------------------------------------------------------
  def +(_row: CsvRow) = rowSeq ++= Seq(_row)
  //---------------------------------------------------------------------------
  def + (_rowSeq: Seq[CsvRow])  = rowSeq ++= _rowSeq
  //---------------------------------------------------------------------------
  def hasRows = !rowSeq.isEmpty
  //---------------------------------------------------------------------------
  def isEmpty = rowSeq.isEmpty
  //---------------------------------------------------------------------------
  def getColNameSeq = colNameSeq
  //---------------------------------------------------------------------------
  def getColNameSize = colNameSeq.size
  //---------------------------------------------------------------------------
  def read(verbose: Boolean = false): Boolean = {

    if (!MyFile.fileExist(fileName)) return error(s"CsvRead. The csv file: '$fileName' does not exist")
    reader = new BufferedReader(new FileReader(fileName))
    //-------------------------------------------------------------------------
    def isCommentLine(line: String): Boolean = {
      commentLinePrefix foreach { prefix => if (line.startsWith(prefix)) return true }
      false
    }
    //-------------------------------------------------------------------------
    def addToRow(itemSeq: Array[String]
                 , lineCount: Long
                 , lineCharSize: Long
                 , verbose: Boolean): Boolean = {

      readRowCount += 1

      //check size
      val emptyColCount = colNameSeq.size - itemSeq.size
      if (itemSeq.size != colNameSeq.size) {
        if (verbose)
        warning(s"CsvRead. File: '$fileName' Can not load fully the line: $lineCount . Expecting: ${colNameSeq.size} " +
          s"columns but found: ${itemSeq.size} ${if (emptyColCount == 0) "" else s"assuming: $emptyColCount empty columns"}")
      }

      //build col
      val emptyColSeq = Array.fill[String](emptyColCount)("")
      val row = CsvRow()
      (colNameSeq zip (itemSeq ++ emptyColSeq)) map { case (colName, colValue) => row + CsvItem(colName, colValue)}
      rowSeq += row

      //stats
      if (lineCharSize < minRowCharSize) minRowCharSize = lineCharSize
      if (lineCharSize > maxRowCharSize) maxRowCharSize = lineCharSize

      true
    }
    //-------------------------------------------------------------------------
    //read the lines of the input file
    var lineCount = 0L

    //very first skip
    for(_<- 0 until skipFirstLineCount) {
      reader.readLine()
      lineCount += 1
    }

    reader.lines().forEach { line =>
      lineCount += 1
      val itemSeq = line.split(itemDivider)
      if (!isCommentLine(line)) {
        if (!line.trim.isEmpty) {
          if (colNameSeq == null) {
            colNameSeq =
              if (hasHeader) itemSeq
              else for (i <- 0 to itemSeq.size) yield f"col_$i%04d"
            if(!hasHeader) addToRow(itemSeq, lineCount, line.size, verbose)
          }
          else addToRow(itemSeq, lineCount, line.size, verbose)
        }
      }
    }
    reader.close()
    if (verbose) info(s"CsvRead. Readed: $readRowCount rows from file: '$fileName'")
    true
  }
  //---------------------------------------------------------------------------
  //join this with B using the fields: 'joinColNameLeft' and 'joinColNameRight'
  def join(csvRight: CsvRead
           , joinColNameLeft: String
           , joinColNameRight: String
           , verbose: Boolean = true) = {

    val leftMap   = CsvRow.getMapByColName(rowSeq.toSeq,joinColNameLeft)
    val rightMap  = CsvRow.getMapByColName(csvRight.rowSeq.toSeq,joinColNameRight)
    val newRowSeq = ArrayBuffer[CsvRow]()

    leftMap.foreach { case (leftRowColValue,leftRowSeq) =>
      if (rightMap.contains(leftRowColValue)) {
        rightMap(leftRowColValue).foreach { rightRow =>
          leftRowSeq.foreach { leftRow =>
            val newRow = CsvRow(leftRow)
            newRow.join(rightRow, csvRight.getColNameSeq)
            newRowSeq += newRow
          }
        }
      }
      else {
        if (verbose) warning(s"CsvRead. The join field in left side: '$joinColNameLeft' has not match with right side: '$joinColNameRight' at value: '$leftRowColValue' ")
      }
    }

    val newCsv = CsvRead(fileName, hasHeader, commentLinePrefix)
    if (newRowSeq.isEmpty) warning(s"CsvRead. No matched rows between: '$fileName' and '${csvRight.fileName}'")
    else newCsv + newRowSeq.toSeq
    newCsv.setColNameSeq(colNameSeq ++ csvRight.colNameSeq)
    newCsv
  }
  //---------------------------------------------------------------------------
  def setColNameSeq(seq: Seq[String]) = colNameSeq = seq
  //---------------------------------------------------------------------------
  def removeDuplicated(columnNameSeq: Seq[String]): Unit = {
    if (columnNameSeq.isEmpty) {
      warning("CsvRead. No columns specified for removing duplicates.")
      return
    }

    val itemDivider = "|"
    val uniqueMap = scala.collection.mutable.Map[String,Boolean]()
    val uniqueRowSeq = ArrayBuffer[CsvRow]()
    rowSeq.foreach { row =>
      val id = row.getAsString(columnNameSeq, itemDivider)
      if (!uniqueMap.contains(id)) {
        uniqueRowSeq += row
        uniqueMap(id) = true
      }
    }
    rowSeq.clear()
    rowSeq ++= uniqueRowSeq
  }
  //---------------------------------------------------------------------------
  def renameColumn(oldColNameSeq: Seq[String], newColNameSeq: Seq[String]) = {

    rowSeq foreach { row =>
      (oldColNameSeq zip newColNameSeq) map { case (oldColName, newColName) =>
        row.renameColumn(oldColName, newColName)
      }
    }

    colNameSeq = colNameSeq.map { colName =>
      if (oldColNameSeq.contains(colName)) newColNameSeq(oldColNameSeq.indexOf(colName))
      else colName
    }
  }
  //---------------------------------------------------------------------------
  def removeColumn(columnNameSeq: Seq[String]): Unit =
    columnNameSeq foreach (removeColumn(_))
  //---------------------------------------------------------------------------
  def removeColumn(columnName : String) : Unit = {
    colNameSeq = colNameSeq.diff(Seq(columnName))
    rowSeq.map { row => row.removeColumn(columnName) }
  }
  //---------------------------------------------------------------------------
  def save(outputCsvName : String) =
    if (rowSeq.isEmpty) warning(s"Csv file: '$outputCsvName' has no rows")
    else CsvWrite(outputCsvName, headerRow = rowSeq.head).writeAndClose(rowSeq.toArray)
  //---------------------------------------------------------------------------
  def save(outputCsvName: String, appendWrite: Boolean) =
    CsvWrite(outputCsvName, headerRow = rowSeq.head, appendWrite).writeAndClose(rowSeq.toArray)
  //---------------------------------------------------------------------------
  def filterRow(key:String, value:String): Seq[CsvRow] =
    filterRow(Seq(key), Seq(value))
  //---------------------------------------------------------------------------
  def filterRow(keySeq: Seq[String],valueSeq: Seq[String]): Seq[CsvRow] =
    rowSeq.filter { row =>
      if (keySeq.forall(row.contains(_))) {
        val storedValueSeq = keySeq map { row(_) }
        (storedValueSeq zip valueSeq).forall { case (a,b) => a == b }
      }
      else false
    }.toSeq
  //---------------------------------------------------------------------------
  def guessFormat() =
    colNameSeq map { colName=>
      val dataTypeSeq = getUniqueValueSeq(colName) map ( CsvDataType.guessDataType(_))
      val headDataType = dataTypeSeq.head
      if (dataTypeSeq forall( _ == headDataType)) (colName,headDataType)
      else (colName,CSV_DATA_TYPE_NONE)
    }
  //---------------------------------------------------------------------------
  def compareStringCol(csvOther: CsvRead
                       , columnName: String
                       , limitResult: Option[Int] = None): Unit = {
    val thisColAValueSeq = this.getCol(columnName).toSet
    val otherColAValueSeq = csvOther.getCol(columnName).toSet

    val onlyInThis = thisColAValueSeq.diff(otherColAValueSeq)
    val onlyInOther = otherColAValueSeq.diff(thisColAValueSeq)

    if (thisColAValueSeq.size != otherColAValueSeq.size)
      println(s"Number of unique rows mismatch: '${this.fileName}' = '${thisColAValueSeq.size}' and '${csvOther.fileName}' = '${otherColAValueSeq.size}'")
    else
      println(s"Both CSV have the same number of unique rows")

    if (onlyInThis.isEmpty && onlyInOther.isEmpty) {
      println(s"No differences found in column $columnName between the two CSV files.")
    } else {
      if (onlyInThis.nonEmpty) {
        println(s"'${onlyInThis.size}' values in column $columnName ONLY in ${this.fileName}:")
        if (limitResult.isDefined){
          println(s"  Only showing:${limitResult.get} results")
          onlyInThis.take(limitResult.get).foreach(s=> println("    " + s))
        }
        else onlyInThis.foreach(println)
      }
      if (onlyInOther.nonEmpty) {
        println(s"'${onlyInOther.size}' values in column $columnName ONLY in ${csvOther.fileName}:")
        if (limitResult.isDefined) {
          println(s"  Only showing:${limitResult.get} results")
          onlyInOther.take(limitResult.get).foreach(s=> println("   " + s))
        } else onlyInOther.foreach(println)
      }
    }
  }

  //---------------------------------------------------------------------------
  def compareDoubleCol(csvOther: CsvRead
                       , columnName: String
                       , limitResult: Option[Int] = None): Unit = {
    val thisColAValueSeq = this.getCol(columnName).map(_.toDouble).sorted
    val otherColAValueSeq = csvOther.getCol(columnName).map(_.toDouble).sorted
    //-------------------------------------------------------------------------
    def existValue(a: ArrayBuffer[Double], v: Double) =
      a.exists{ p=> Math.abs(p-v) < 10E-9}
    //-------------------------------------------------------------------------

    val onlyInThis = thisColAValueSeq.filter( v=> existValue(otherColAValueSeq, v)).sorted
    val onlyInOther = otherColAValueSeq.filter( v=> existValue(thisColAValueSeq, v)).sorted

    if (thisColAValueSeq.size != otherColAValueSeq.size)
      println(s"Number of unique rows mismatch: '${this.fileName}' = '${thisColAValueSeq.size}' and '${csvOther.fileName}' = '${otherColAValueSeq.size}'")
    else
      println(s"Both CSV have the same number of unique rows")

    if (onlyInThis.isEmpty && onlyInOther.isEmpty) {
      println(s"No differences found in column $columnName between the two CSV files.")
    } else {
      if (onlyInThis.nonEmpty) {
        println(s"'${onlyInThis.size}' values in column $columnName ONLY in ${this.fileName}:")
        if (limitResult.isDefined) {
          println(s"  Only showing:${limitResult.get} results")
          onlyInThis.take(limitResult.get).foreach(s => println("    " + s))
        }
        else onlyInThis.foreach(println)
      }
      if (onlyInOther.nonEmpty) {
        println(s"'${onlyInOther.size}' values in column $columnName ONLY in ${csvOther.fileName}:")
        if (limitResult.isDefined) {
          println(s"  Only showing:${limitResult.get} results")
          onlyInOther.take(limitResult.get).foreach(s => println("   " + s))
        } else onlyInOther.foreach(println)
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CsvFile.scala
//=============================================================================
