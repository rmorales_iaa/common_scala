/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  24/Mar/2023 
 * Time:  17h:57m
 * Description: None
 */
//=============================================================================
package com.common.csv
//=============================================================================
import com.common.util.util.Util
//=============================================================================
//=============================================================================
object CsvDataType {
  //---------------------------------------------------------------------------
  def guessDataType(s: String, floatByDefault: Boolean = false): CsvDataType[_] = {
    //-------------------------------------------------------------------------
    if (Util.isByte(s))    return CSV_DATA_TYPE_BYTE
    if (Util.isShort(s))   return CSV_DATA_TYPE_SHORT
    if (Util.isInteger(s)) return CSV_DATA_TYPE_INT
    if (Util.isLong(s))    return CSV_DATA_TYPE_LONG
    if (Util.isFloat(s))   return CSV_DATA_TYPE_FLOAT
    if (Util.isDouble(s))  return CSV_DATA_TYPE_DOUBLE
    if (s.size == 1) CSV_DATA_TYPE_CHAR
    else CSV_DATA_TYPE_STRING
    //-------------------------------------------------------------------------
  }
  //===========================================================================
  trait CsvDataType[T] {
    //-------------------------------------------------------------------------
    val scalaDataTypeName: String
    val order: Byte //less value is worst
    //-------------------------------------------------------------------------
  }
  //===========================================================================
  //Singleton Objects
  val CSV_DATA_TYPE_CHAR   = CsvDataTypeChar()
  val CSV_DATA_TYPE_STRING = CsvDataTypeString()
  val CSV_DATA_TYPE_BYTE   = CsvDataTypeByte()
  val CSV_DATA_TYPE_SHORT  = CsvDataTypeShort()
  val CSV_DATA_TYPE_INT    = CsvDataTypeInt()
  val CSV_DATA_TYPE_LONG   = CsvDataTypeLong()
  val CSV_DATA_TYPE_FLOAT  = CsvDataTypeFloat()
  val CSV_DATA_TYPE_DOUBLE = CsvDataTypeDouble()
  val CSV_DATA_TYPE_NONE   = CsvDataTypeNone()
  //---------------------------------------------------------------------------
  sealed case class CsvDataTypeNone() extends CsvDataType[String] {val scalaDataTypeName="String";val order=0}
  //---------------------------------------------------------------------------
  sealed case class CsvDataTypeChar() extends CsvDataType[Char] {val scalaDataTypeName="Char";val order=1}
  //---------------------------------------------------------------------------
  sealed case class CsvDataTypeByte() extends CsvDataType[Byte] {val scalaDataTypeName="Byte";val order=2}
  //---------------------------------------------------------------------------
  sealed case class CsvDataTypeShort() extends CsvDataType[Short] {val scalaDataTypeName="Short";val order=3}
  //---------------------------------------------------------------------------
  sealed case class CsvDataTypeInt() extends CsvDataType[Int] {val scalaDataTypeName="Int";val order=4}
  //---------------------------------------------------------------------------
  sealed case class CsvDataTypeLong() extends CsvDataType[Long] {val scalaDataTypeName="Long";val order=5}
  //---------------------------------------------------------------------------
  sealed case class CsvDataTypeFloat() extends CsvDataType[Float] {val scalaDataTypeName="Float";val order=6}
  //---------------------------------------------------------------------------
  sealed case class CsvDataTypeDouble() extends CsvDataType[Double] {val scalaDataTypeName="Double";val order=7}
  //---------------------------------------------------------------------------
  sealed case class CsvDataTypeString() extends CsvDataType[String] {val scalaDataTypeName = "String";val order=8}
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CsvDataType.scala
//=============================================================================
