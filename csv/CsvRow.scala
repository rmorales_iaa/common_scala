/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  11/Jan/2023 
 * Time:  12h:37m
 * Description: None
 */
//=============================================================================
package com.common.csv
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object CsvRow {
  //---------------------------------------------------------------------------
  def apply(map: Map[String,String]): CsvRow = {
    val row = CsvRow()
    for( (name,value) <-map) {row + CsvItem(name, value)}
    row
  }
  //---------------------------------------------------------------------------
  def apply(csvRow: CsvRow): CsvRow = {
    val newRow = CsvRow()
    csvRow.getItemSeq foreach (newRow + _)
    newRow
  }
  //---------------------------------------------------------------------------
  def apply(colNameSeq: Seq[String]): CsvRow = {
    val newRow = CsvRow()
    colNameSeq foreach ( newRow + CsvItem(_))
    newRow
  }
  //---------------------------------------------------------------------------
  def apply(item: CsvItem): CsvRow = {
    val newRow = CsvRow()
    newRow + item
    newRow
  }
  //---------------------------------------------------------------------------
  def buildWithPair(seq: Seq[(String, String)]): CsvRow = {
    val row = CsvRow()
    for ((name, value) <- seq)
      row + CsvItem(name, value)
    row
  }
  //---------------------------------------------------------------------------
  def build(valueSeq: Seq[String]): CsvRow = {
    val newRow = CsvRow()
    valueSeq.zipWithIndex.foreach {case (value,i) =>
      newRow + CsvItem(s"col_$i",value)
    }
    newRow
  }
  //---------------------------------------------------------------------------
  def getMapByColName(rowSeq: Seq[CsvRow], rowName: String) = {
    val map = scala.collection.mutable.Map[String,ArrayBuffer[CsvRow]]()
    rowSeq.foreach { row =>
      if (row.contains(rowName)) {
        if (!map.contains(row(rowName))) map(row(rowName)) = ArrayBuffer[CsvRow]()
        map(row(rowName)) += row
      }
    }
    map
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class CsvRow() {
  //---------------------------------------------------------------------------
  private val map = scala.collection.mutable.LinkedHashMap[String,CsvItem]()
  //---------------------------------------------------------------------------
  def apply(name: String) = map(name).value
  //-------------------------------------------------------------------------
  def getItemSeq = map.values.toSeq
  //-------------------------------------------------------------------------
  def getColNameSeq() = map.values.map { csvItem => csvItem.name }.toSeq
  //-------------------------------------------------------------------------
  def size = map.size
  //-------------------------------------------------------------------------
  def contains(rowName: String) = map.contains(rowName)
  //-------------------------------------------------------------------------
  def getAsString(columnNameSeq: Seq[String] = Seq()
                  , itemDivider:String) = {
    if (columnNameSeq.isEmpty) {
      if (itemDivider == "\\t") map.toSeq.map( _._2.value).mkString("\t")
      else map.toSeq.map( _._2.value).mkString(itemDivider)
    }
    else
      (columnNameSeq map { columnName =>
        if (map.contains(columnName)) map(columnName).value
        else ""
      }).mkString(itemDivider)
  }
  //---------------------------------------------------------------------------
  def + (item: CsvItem): Unit = map += ((item.name,item))
  //---------------------------------------------------------------------------
  def + (name:String, item: CsvItem): Unit = map += ((name,item))
  //---------------------------------------------------------------------------
  def + (itemSeq: Seq[CsvItem]): Unit = itemSeq foreach { this + _ }
  //---------------------------------------------------------------------------
  def getColName(prefix: String): Option[String] = {
    map.keySet.foreach { s =>
      if (s.startsWith(prefix)) return Some(s)
      else None
    }
    None
  }
  //---------------------------------------------------------------------------
  def getString(name: String) = map(name).value
  //---------------------------------------------------------------------------
  def getStringOrDefault(name: String, default: String) = {
    if (map.contains(name)) {
      val v = map(name).value
      if (v.isEmpty) default
      else v
    }
    else default
  }
  //---------------------------------------------------------------------------
  def getByte(name: String) = map(name).value.toByte
  //---------------------------------------------------------------------------
  def getShort(name: String) = map(name).value.toShort
  //---------------------------------------------------------------------------
  def getInt(name: String) = map(name).value.toInt

  def getIntOrDefault(name: String, default: Int) = {
    if (map.contains(name)) {
      val v =  map(name).value
      if (v.isEmpty) default
      else v.toInt
    }
    else default
  }
  //---------------------------------------------------------------------------
  def getLong(name: String) = map(name).value.toLong

  def getLongOrDefault(name: String, default: Long) =
    if (map.contains(name)) {
      val v = map(name).value
      if (v.isEmpty) default
      else v.toLong
    }
    else default
  //---------------------------------------------------------------------------
  def getFloat(name: String) = map(name).value.toFloat

  def getFloatOrDefault(name: String, default: Float) =
    if (map.contains(name)) {
      val v = map(name).value
      if (v.isEmpty) default
      else v.toFloat
    }
    else default
  //---------------------------------------------------------------------------
  def getDouble(name: String) = map(name).value.toDouble
  def getDoubleOrDefault(name: String, default: Double) =
    if (map.contains(name)) {
      val v = map(name).value
      if (v.isEmpty) default
      else v.toDouble
    }
    else default
  //---------------------------------------------------------------------------
  def join(row: CsvRow) = { row.getItemSeq.foreach { this + _  } }
  //---------------------------------------------------------------------------
  def join(row: CsvRow, colNameSeq:Seq[String]) = {
    row.getItemSeq.zipWithIndex.foreach { case (item,i) =>
      this + (colNameSeq(i),item)
    }
  }
  //---------------------------------------------------------------------------
  def renameColumn(oldColName: String, newColName:String) = {
    val mapSeq = map.toSeq map { case (name,item) =>
      if(name == oldColName) (newColName,CsvItem(newColName,item.value))
      else (name,item)
    }
    map.clear()
    mapSeq foreach { case (name,item) => map(name) = item }
  }
  //---------------------------------------------------------------------------
  def removeColumn(columnName: String) = map.remove(columnName)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CsvRow.scala
//=============================================================================
