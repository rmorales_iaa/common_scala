//=============================================================================
//File: StatItemLong.scala
//=============================================================================
/** It implements a basic item oldStat with long values
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    23 Aptil 2017
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.stat

import com.common.configuration.MyConf

//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================
case class StatDescriptiveInt(count: Long
                           , average : Double
                           , variance : Double
                           , stdDev : Double
                           , mode: Int
                           , median : Double
                           , min : Int
                           , max : Int
                           , skewness_1 : Double
                           , skewness_2 : Double) {
  //---------------------------------------------------------------------------
  override def toString(): String = toString("")
  //---------------------------------------------------------------------------
  def toString(prefix:String) = {
    s"$prefix count: $count \n" +
    s"$prefix average   : $average \n" +
    s"$prefix variance  : $variance \n" +
    s"$prefix stdDev    : $stdDev \n" +
    s"$prefix mode      : $mode \n" +
    s"$prefix median    : $median \n" +
    s"$prefix min       : $min \n" +
    s"$prefix max       : $max \n" +
    s"$prefix skewness_1: $skewness_1 \n" +
    s"$prefix skewness_2: $skewness_2"
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class SimpleStatDescriptive(count: Long
                                 , average : Double
                                 , variance : Double
                                 , stdDev : Double
                                 , median : Double
                                 , min: Double
                                 , max: Double
                                 , rms: Double) {
  //---------------------------------------------------------------------------
  override def toString() = toString("")
  //---------------------------------------------------------------------------
  def toString(prefix:String) =
    s"$prefix count     : $count \n" +
    s"$prefix average   : $average \n" +
    s"$prefix variance  : $variance \n" +
    s"$prefix stdDev    : $stdDev \n" +
    s"$prefix median    : $median \n" +
    s"$prefix min       : $min \n" +
    s"$prefix max       : $max"
  //---------------------------------------------------------------------------
}
//=============================================================================
object StatDescriptive {
  //---------------------------------------------------------------------------
  private def SIGMA_CLIPPING_MAX_ITERATION         = MyConf.c.getInt("stats.sigmaClippingDefaultMaxIteration")
  //---------------------------------------------------------------------------
  //(count, average, variance, standard deviation, mode, median, first skewness coefficient, second skewness coefficient)
  def getWithInt(data: Array[Int], isPoblation: Boolean = true) = {

    val n  = data.size.toDouble
    val average = data.sum / n
    val variance = data.map( v=> Math.pow(average - v, 2) ).sum / (if (isPoblation) n else if (n == 1) 1 else n-1)
    val stdDev = Math.sqrt(variance)

    val histogram = data.groupBy(x => x).mapValues(_.size).toSeq
    val max = histogram.map(_._2).max
    val mode = histogram.filter(_._2 == max).map(_._1).head

    val sortedSeq = data.sortWith(_ < _)
    val median = if (data.size % 2 == 1) sortedSeq(sortedSeq.size / 2).toDouble
    else {
      val posRight = data.size / 2
      val posLeft = posRight - 1
      (sortedSeq(posRight) + sortedSeq(posLeft)) / 2.0d
    }
    val skewness_1 = (average - mode) / stdDev
    val skewness_2 = 3 * (average - median) / stdDev

    StatDescriptiveInt(data.length, average, variance, stdDev, mode, median, sortedSeq.head, sortedSeq.last, skewness_1, skewness_2)
  }
  //---------------------------------------------------------------------------
  //(count, average, variance, standard deviation, median)
  def getWithLong(data: Array[Long], isPoblation: Boolean = true) = {
    val n  = data.size.toDouble
    val average = data.sum / n
    val variance = data.map( v=> Math.pow(average - v, 2) ).sum / (if (isPoblation) n else if (n == 1) 1 else n-1)
    val stdDev = Math.sqrt(variance)

    val sortedSeq = data.sortWith(_ < _)
    val median = if (data.size % 2 == 1) sortedSeq(sortedSeq.size / 2).toDouble
    else {
      val posRight = data.size / 2
      val posLeft = posRight - 1
      (sortedSeq(posRight) + sortedSeq(posLeft)) / 2.0d
    }

    val (lower, upper) = data.sorted.splitAt(n.toInt / 2)
    if (data.size % 2 == 0) (lower.last + upper.head) / 2 else upper.head

    val rms = Math.sqrt(data.map(v => (v - median) * (v - median)).sum / n)

    SimpleStatDescriptive(data.length
                          , average
                          , variance
                          , stdDev
                          , median
                          , sortedSeq.head
                          , sortedSeq.last
                          , rms)
  }
  //---------------------------------------------------------------------------
  //(count, average, variance, standard deviation, mode, median)
  def getWithFloat(data: Array[Float], isPopulation: Boolean = false) =
    getWithDouble(data map (_.toDouble), isPopulation)
  //---------------------------------------------------------------------------
  def getWithDouble(_data: Array[Double], isPopulation: Boolean = true): SimpleStatDescriptive = {

    val data = _data.filter(!_.isNaN)
    if (data.isEmpty) return SimpleStatDescriptive(0,Double.NaN,Double.NaN,Double.NaN, Double.NaN,Double.NaN,Double.NaN,Double.NaN)
    val n  = data.size.toDouble
    val average = data.sum / n
    val variance = data.map( v=> Math.pow(average - v, 2) ).sum / (if (isPopulation) n else if (n == 1) 1 else n-1)
    val stdDev = Math.sqrt(variance)

    val sortedSeq = data.sortWith(_ < _)
    val median = if (data.size % 2 == 1) sortedSeq(sortedSeq.size / 2)
    else {
      val posRight = data.size / 2
      val posLeft = posRight - 1
      (sortedSeq(posRight) + sortedSeq(posLeft)) / 2.0d
    }
    val (lower, upper) = data.sorted.splitAt(n.toInt / 2)
    if (data.size % 2 == 0) (lower.last + upper.head) / 2 else upper.head

    val rms = Math.sqrt(data.map(v => (v - median) *  (v - median) ).sum / n)

    SimpleStatDescriptive(data.length
                          , average
                          , variance
                          , stdDev
                          , median
                          , sortedSeq.head
                          , sortedSeq.last
                          , rms)
  }
  //---------------------------------------------------------------------------
  //Root mean square
  def getRMS_Int(data: Array[Int]) = {
    val squaredSum = (for (v<-data) yield v * v.toFloat).sum
    Math.sqrt(squaredSum / data.length)
  }
  //---------------------------------------------------------------------------
  //https://www.gnu.org/software/gnuastro/manual/html_node/Sigma-clipping.html
  def sigmaClippingDouble(data: Array[Double]
                        , iteration: Int = SIGMA_CLIPPING_MAX_ITERATION
                        , sigmaScale: Double  = 3
                        , useCentralValueAsMedian: Boolean = true) : Array[Double] = {
    var d = data
    var oldStat : SimpleStatDescriptive = null
    for(_<-0 until iteration) {
      if (d.isEmpty) return d
      val newStat = getWithDouble(d)
      val (median,stdDev) = (newStat.median, newStat.stdDev)
      val centralValue = if (useCentralValueAsMedian) median else stdDev
      val a = centralValue - (sigmaScale * stdDev)
      val b = centralValue + (sigmaScale * stdDev)
      val (minAllowedValue,maxAllowedValue) = if (a < b) (a,b) else (b,a)
      d = d.filter( v=>  (v <= maxAllowedValue) && (v >= minAllowedValue))
      if ((oldStat != null) && (d.length == oldStat.count)) return  d  //same amount of items
      oldStat = newStat
    }
    d
  }
  //---------------------------------------------------------------------------
  //https://www.gnu.org/software/gnuastro/manual/html_node/Sigma-clipping.html
  private def sigmaClippingLong(data: Array[Long]
                                , iteration: Int = SIGMA_CLIPPING_MAX_ITERATION
                                , sigmaScale: Double  = 3
                                , useCentralValueAsMedian: Boolean = true)  : Array[Long] = {
    var d = data
    var oldStat : SimpleStatDescriptive = null
    for(_<-0 until iteration) {
      if (d.isEmpty) return d
      val newStat = getWithLong(d)
      val (median,stdDev) = (newStat.median, newStat.stdDev)
      val centralValue = if (useCentralValueAsMedian) median else stdDev
      val a = centralValue - (sigmaScale * stdDev)
      val b = centralValue + (sigmaScale * stdDev)
      val (minAllowedValue,maxAllowedValue) = if (a < b) (a,b) else (b,a)
      d = d.filter( v=>  (v <= maxAllowedValue) && (v >= minAllowedValue))
      if ((oldStat != null) && (d.length == oldStat.count)) return  d  //same amount of items
      oldStat = newStat
    }
    d
  }
  //---------------------------------------------------------------------------
  def sigmaClippingMedianShort(data: Array[Short], iteration: Int = SIGMA_CLIPPING_MAX_ITERATION, sigmaScale: Double = 3d) =
    sigmaClippingLong(data map (_.toLong), iteration, sigmaScale)
  //---------------------------------------------------------------------------
  def sigmaClippingMedianLong(data: Array[Long], iteration: Int = SIGMA_CLIPPING_MAX_ITERATION, sigmaScale: Double = 3d) =
    sigmaClippingLong(data, iteration, sigmaScale)
  //---------------------------------------------------------------------------
  def sigmaClippingMedianInt(data: Array[Int], iteration: Int = SIGMA_CLIPPING_MAX_ITERATION, sigmaScale: Double = 3d) =
    sigmaClippingLong(data map (_.toLong), iteration, sigmaScale)
  //---------------------------------------------------------------------------
  def sigmaClippingStdDevInt(data: Array[Int], iteration: Int = SIGMA_CLIPPING_MAX_ITERATION, sigmaScale: Double = 3d) =
    sigmaClippingLong(data map (_.toLong), iteration, sigmaScale, useCentralValueAsMedian = false)
  //---------------------------------------------------------------------------
  def sigmaClippingStdDevFloat(data: Array[Float], iteration: Int = SIGMA_CLIPPING_MAX_ITERATION, sigmaScale: Double = 3d) =
    sigmaClippingDouble(data map (_.toDouble), iteration, sigmaScale, useCentralValueAsMedian = false) map (_.toFloat)
  //---------------------------------------------------------------------------
  def sigmaClippingMedianDouble(data: Array[Double], iteration: Int = SIGMA_CLIPPING_MAX_ITERATION, sigmaScale: Double = 3d) =
    sigmaClippingDouble(data, iteration, sigmaScale)
  //---------------------------------------------------------------------------
  def sigmaClippingMedianFloat(data: Array[Float], iteration: Int = SIGMA_CLIPPING_MAX_ITERATION, sigmaScale: Double = 3d) =
    sigmaClippingDouble(data map (_.toDouble), iteration, sigmaScale) map (_.toFloat)
  //---------------------------------------------------------------------------
  def sigmaClippingStdDevDouble(data: Array[Double], iteration: Int = SIGMA_CLIPPING_MAX_ITERATION, sigmaScale: Double = 3d) =
    sigmaClippingDouble(data, iteration, sigmaScale, useCentralValueAsMedian = false)
  //---------------------------------------------------------------------------
  //(remainValueSeq,discardedValueSeq(data,pos))
  def sigmaClippingDoubleGetDiscarded(data: Array[Double]
                                      , iteration: Int = SIGMA_CLIPPING_MAX_ITERATION
                                      , sigmaScale: Double = 3
                                      , useCentralValueAsMedian: Boolean = true) = {

    val dataPair = data.zipWithIndex map { case (d, i) => (d,i) }
    val dataFiltered = StatDescriptive.sigmaClippingDoubleWithPair(dataPair
                                                                   , iteration
                                                                   , sigmaScale
                                                                   , useCentralValueAsMedian)
    val remainIndexSeq = dataFiltered.map(_._2).toSet
    val discardedValueSeq = Array.range(0, data.length)
      .toSet
      .diff(remainIndexSeq) map ( index=> (dataPair(index)._1,index) )

    (dataFiltered map (_._1), discardedValueSeq)
  }
  //---------------------------------------------------------------------------
  //https://www.gnu.org/software/gnuastro/manual/html_node/Sigma-clipping.html
  def sigmaClippingDoubleWithPair[T] (data: Array[(Double,T)]
                                      , iteration: Int = SIGMA_CLIPPING_MAX_ITERATION
                                      , sigmaScale: Double  = 3
                                      , useCentralValueAsMedian: Boolean = true) : Array[(Double,T)] = {
    var d = data.filter(!_._1.isNaN)
    var oldStat : SimpleStatDescriptive = null
    for(_<-0 until iteration) {
      if (d.isEmpty) return d
      val newStat = getWithDouble(d map (_._1))
      val (median,stdDev) = (newStat.median, newStat.stdDev)
      val centralValue = if (useCentralValueAsMedian) median else stdDev
      val a = centralValue - (sigmaScale * stdDev)
      val b = centralValue + (sigmaScale * stdDev)
      val (minAllowedValue,maxAllowedValue) = if (a < b) (a,b) else (b,a)
      d = d.filter( v=> (v._1 <= maxAllowedValue) && (v._1 >= minAllowedValue))
      if ((oldStat != null) && (d.length == oldStat.count)) return  d  //same amount of items
      oldStat = newStat
    }
    d
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
// End of 'StatDescriptive.scala' file
//=============================================================================
