/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Oct/2022
 * Time:  13h:06m
 * Description: None
 */
//=============================================================================
package com.common.stream.remote
//=============================================================================
import com.common.logger.MyLogger
import com.common.stream.MyOutputStream
import org.apache.hadoop.fs.{FSDataOutputStream, FileSystem, Path}
//=============================================================================
import java.io.{DataOutputStream, File, FileOutputStream}
//=============================================================================
//=============================================================================
case class RemoteOutputStream(fileSystem: FileSystem, name: String) extends MyOutputStream
  with MyLogger {
  //---------------------------------------------------------------------------
  type StreamType = FSDataOutputStream
  //---------------------------------------------------------------------------
  val stream: StreamType = fileSystem.create(new Path(name))
  //---------------------------------------------------------------------------
  def close: Boolean = myClose[StreamType](stream)
  //---------------------------------------------------------------------------
  def write(data: Seq[Byte], closeIt: Boolean): Boolean =
    myWrite[StreamType](stream, data, closeIt)
  //---------------------------------------------------------------------------
  def writeLine(s: String, closeIt: Boolean): Boolean =
    myWrite[StreamType](stream, (s + "\n").getBytes, closeIt)
  //---------------------------------------------------------------------------
  def flush[A <: RequiredMethodType]: Boolean = myFlush[StreamType](stream)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RemoteOutputStream.scala
//=============================================================================
