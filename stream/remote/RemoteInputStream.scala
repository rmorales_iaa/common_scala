/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Oct/2022
 * Time:  13h:06m
 * Description: None
 */
//=============================================================================
package com.common.stream.remote
//=============================================================================
import com.common.stream.{MyInputStream}
import org.apache.hadoop.fs.{FSDataInputStream, FileSystem, Path}
import scala.util.{Try,Success,Failure}
//=============================================================================
//=============================================================================
case class RemoteInputStream(fileSystem: FileSystem
                             , name: String) extends MyInputStream {
  //---------------------------------------------------------------------------
  type StreamType = FSDataInputStream
  //---------------------------------------------------------------------------
  private val path = new Path(name)
  private val stream = fileSystem.open(path)
  remainByteCount = getByteSize
  //---------------------------------------------------------------------------
  def getByteSize: Long = fileSystem.getContentSummary(new Path(name)).getLength
  //---------------------------------------------------------------------------
  def close: Boolean = myClose[StreamType](stream)
  //---------------------------------------------------------------------------
  def read(byteSize: Int): (Boolean, Seq[Byte]) = multipleRead[StreamType](stream, byteSize)
  //---------------------------------------------------------------------------
  def skip(byteSize: Int): Boolean = mySkip[StreamType](stream, byteSize)
  //---------------------------------------------------------------------------
  def resetMark(): Boolean =
    Try {
      stream.reset()
      remainByteCount = totalByteSize
      streamPointer = 0
    } match {
      case Success(_) => true
      case Failure(ex) =>
        error(s"Error resetting the stream: $name " + ex.toString)
        false
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RemoteInputStream.scala
//=============================================================================
