/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  10/Aug/2018
  * Time:  17h:15m
  * Description: None
  */
//=============================================================================
package com.common.stream.remote
//=============================================================================
import com.common.stream.MyInputStream
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.io.compress.{CodecPool, CompressionCodec, CompressionInputStream}
//=============================================================================
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
case class RemoteCompressedInputStream(
  fileSystem: FileSystem
  , path: Path
  , codec: CompressionCodec
  , name: String) extends MyInputStream {
  //---------------------------------------------------------------------------
  type StreamType = CompressionInputStream
  //---------------------------------------------------------------------------
  private val f = fileSystem.open(path)
  private val decompressor = CodecPool.getDecompressor(codec)
  private val stream = codec.createInputStream(f, decompressor)
  remainByteCount = getByteSize
  //---------------------------------------------------------------------------
  def getByteSize: Long = fileSystem.getFileStatus(path).getLen
  //---------------------------------------------------------------------------
  def close: Boolean = {
    val r = myClose[StreamType](stream)
    if (decompressor != null) CodecPool.returnDecompressor(decompressor)
    r
  }
  //---------------------------------------------------------------------------
  def read(byteSize: Int): (Boolean, Seq[Byte]) = singleRead[StreamType](stream, byteSize)
  //---------------------------------------------------------------------------
  def skip(byteSize: Int): Boolean = {

    if (byteSize == 0) return true
    if (!canRead(byteSize)) return false
    remainByteCount -= byteSize
    var r: Boolean = false
    Try {
      r = stream.skip(byteSize) == byteSize
    } match {
      case Success(_) => r
      case Failure(ex) =>
        error(
          s"Error skipping $byteSize bytes from stream: $name " + ex.toString)
        false
    }
  }

  //---------------------------------------------------------------------------
  def resetMark: Boolean =
    Try {
      stream.reset()
    } match {
      case Success(_) => true
      case Failure(ex) =>
        error(s"Error resetting the stream: $name " + ex.toString)
        false
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file RemoteCompressedStream
//=============================================================================