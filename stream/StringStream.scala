/*
 * Rafael Morales (rmorales@iaa.es)
 * Instituto de Astrofísisca de Andalucía - CSIC
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  13/abr/2018
  * Time:  13h:13m
  * Description: None
  */
//=============================================================================
package com.common.stream

//=============================================================================
import java.io.{ByteArrayInputStream, ByteArrayOutputStream, DataInputStream}
import scala.util.{Failure, Success, Try}
//=============================================================================

//=============================================================================
object StringStream {}
//=============================================================================
case class StringStream(s: List[Seq[Byte]], name: String = "StringStream")
    extends MyInputStream {
  //---------------------------------------------------------------------------
  type StreamType = DataInputStream
  //---------------------------------------------------------------------------
  private val size: Long = s.map(_.length).sum
  remainByteCount = size
  //---------------------------------------------------------------------------
  private val out = new ByteArrayOutputStream
  s.foreach(t => out.write(t.toArray))
  private val stream = new DataInputStream(new ByteArrayInputStream(out.toByteArray))
  stream.mark(0)
  out.close()
  //---------------------------------------------------------------------------
  def getByteSize: Long = size
  //---------------------------------------------------------------------------
  def close: Boolean = myClose[StreamType](stream)
  //---------------------------------------------------------------------------
  def read(byteSize: Int): (Boolean, Seq[Byte]) =  singleRead[StreamType](stream, byteSize)
  //---------------------------------------------------------------------------
  def skip(byteSize: Int): Boolean = mySkip[StreamType](stream, byteSize)
  //---------------------------------------------------------------------------
  def resetMark: Boolean =
    Try {
      stream.reset()
    } match {
      case Success(_) => true
      case Failure(ex) =>
        error(s"Error resetting the stream: $name " + ex.toString)
        false
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file StringStream.scala
//=============================================================================
