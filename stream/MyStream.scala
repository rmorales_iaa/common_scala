/*
 * Rafael Morales (rmorales@iaa.es)
 * Instituto de Astrofísisca de Andalucía - CSIC
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  05/abr/2018
  * Time:  14h:12m
  * Description: It manages local files or HDFS files
  */
//=============================================================================
package com.common.stream
//=============================================================================
import scala.language.reflectiveCalls
import scala.util.{Failure, Success, Try}
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
trait MyStream extends Serializable with MyLogger {
  //---------------------------------------------------------------------------
  val name: String
  //---------------------------------------------------------------------------
  var streamPointer: Long = 0
  //---------------------------------------------------------------------------
  var closed: Boolean = false
  //---------------------------------------------------------------------------
  type CloseRequiredType = { def close(): Unit } //structural type definition
  //---------------------------------------------------------------------------
  protected def myClose[A <: CloseRequiredType](stream: A): Boolean = {
    if (closed) return true
    if (stream == null) return true
    Try(stream.close()) match {
      case Success(_) =>
        closed = true
        true
      case Failure(ex) =>
        error(s"Error closing stream: '$name' " + ex.toString)
        false
    }
  }
  //---------------------------------------------------------------------------
  def close: Boolean
  //---------------------------------------------------------------------------
  def isClosed: Boolean = closed
  //---------------------------------------------------------------------------
  def getProcessedDataByteCount = streamPointer
  //---------------------------------------------------------------------------
  def getProcessedDataIndex = streamPointer - 1
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MyStream
// =============================================================================
