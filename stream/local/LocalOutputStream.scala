/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Oct/2022
 * Time:  13h:06m
 * Description: None
 */
//=============================================================================
package com.common.stream.local
//=============================================================================
import com.common.logger.MyLogger
import com.common.stream.MyOutputStream
//=============================================================================
import java.io.{DataOutputStream, File, FileOutputStream}
//=============================================================================
//=============================================================================
object LocalOutputStream {
  //---------------------------------------------------------------------------
  private val LINE_SEPARATOR = System.lineSeparator()
  //---------------------------------------------------------------------------
}
//=============================================================================
import LocalOutputStream._
case class LocalOutputStream(name: String, append: Boolean = false) extends MyOutputStream with MyLogger {
  //---------------------------------------------------------------------------
  type StreamType = DataOutputStream
  //---------------------------------------------------------------------------
  val stream = new StreamType(new FileOutputStream(new File(name),append))
  //---------------------------------------------------------------------------
  def close: Boolean = myClose[StreamType](stream)
  //---------------------------------------------------------------------------
  def write(data: Seq[Byte], closeIt: Boolean): Boolean  =
    myWrite[StreamType](stream, data, closeIt)
  //---------------------------------------------------------------------------
  def writeLine(s: String, closeIt: Boolean = false): Boolean = {
    if (s.isEmpty) true
    else myWrite[StreamType](stream, (s + LINE_SEPARATOR).getBytes, closeIt)
  }
  //---------------------------------------------------------------------------
  def writeLine(s: List[String]): Boolean = {
    s.foreach { t =>
      if (!writeLine(t)) return false
    }
    true
  }
  //---------------------------------------------------------------------------
  def flush[A <: RequiredMethodType]: Boolean = myFlush[StreamType](stream)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file LocalOutputStream.scala
//=============================================================================
