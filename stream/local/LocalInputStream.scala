/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Oct/2022
 * Time:  13h:06m
 * Description: None
 */
//=============================================================================
package com.common.stream.local
//=============================================================================
import com.common.stream.MyInputStream
//=============================================================================
import java.io.{BufferedInputStream, DataInputStream, File, FileInputStream}
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
case class LocalInputStream(name: String) extends MyInputStream {
  //---------------------------------------------------------------------------
  type StreamType = DataInputStream
  //---------------------------------------------------------------------------
  private val stream = new DataInputStream(
    new BufferedInputStream(
      new FileInputStream(name)))
  //---------------------------------------------------------------------------
  def getByteSize(): Long = new File(name).length
  //---------------------------------------------------------------------------
  def close(): Boolean = myClose[StreamType](stream)
  //---------------------------------------------------------------------------
  def read(byteSize: Int): (Boolean, Seq[Byte]) = multipleRead[StreamType](stream, byteSize)
  //---------------------------------------------------------------------------
  def skip(byteSize: Int): Boolean = mySkip[StreamType](stream, byteSize)
  //---------------------------------------------------------------------------
  def resetMark(): Boolean =
    Try {
      stream.reset()
      remainByteCount = totalByteSize
      streamPointer = 0
    }
    match {
      case Success(_) => true
      case Failure(ex) =>
        error(s"Error resetting the stream: '$name' " + ex.toString)
        false
   }
  //---------------------------------------------------------------------------
  def setMark(pos: Int = 0) = {
    Try {
      stream.mark(pos)
    }
    match {
      case Success(_) => true
      case Failure(ex) =>
        error(s"Error setting mark to position '$pos' " + ex.toString)
        false
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file LocalInputStream.scala
//=============================================================================
