/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  05/Oct/2022 
 * Time:  13h:42m
 * Description: None
 */
//=============================================================================
package com.common.stream
//=============================================================================
//=============================================================================
import scala.util.{Failure, Success, Try}
import scala.language.reflectiveCalls

//=============================================================================
//=============================================================================
trait MyOutputStream extends MyStream {
  //---------------------------------------------------------------------------
  type RequiredMethodType = {
    def write(arr: Array[Byte]): Unit
    def close(): Unit
    def flush(): Unit
  } //structural type definition
  //---------------------------------------------------------------------------
  def write(data: Seq[Byte], closeIt: Boolean ): Boolean
  //---------------------------------------------------------------------------
  def writeLine(s: String, closeIt: Boolean = false): Boolean
  //---------------------------------------------------------------------------
  def write(data: List[Seq[Byte]], closeIt: Boolean = false): Boolean = {
    data.foreach(d => if (!write(d,closeIt)) return false)
    true
  }
  //---------------------------------------------------------------------------
  protected def myWrite[A <: RequiredMethodType](stream: A,
                                                 data: List[Seq[Byte]],
                                                 closeIt: Boolean): Boolean = {
    data.foreach( d=> if (!myWrite(stream,d,closeIt)) return false)
    true
  }
  //---------------------------------------------------------------------------
  protected def myWrite[A <: RequiredMethodType](stream: A,
                                                 data: Seq[Byte],
                                                 closeIt: Boolean = false): Boolean = {
    Try { stream.write(data.toArray)}
    match {
      case Success(_) =>
        streamPointer += data.length
      case Failure(ex) =>
        error(s"Error writing into the stream: $name " + ex.toString)
        return false
    }
    if (closeIt)
      myClose[A](stream)
    true
  }
  //---------------------------------------------------------------------------
  def myFlush[A <: RequiredMethodType](stream: A): Boolean = {
    Try {
      stream.flush()
    } match {
      case Success(_) => return true
      case Failure(ex) =>
        error(s"Error flushing the stream: $name " + ex.toString)
        return false
    }
    true
  }
  //---------------------------------------------------------------------------
  def getWritePosition: Long = streamPointer
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MyOutputStream.scala
//=============================================================================
