/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  05/Oct/2022 
 * Time:  13h:41m
 * Description: None
 */
//=============================================================================
package com.common.stream
//=============================================================================
//=============================================================================
//=============================================================================
import scala.util.{Failure, Success, Try}
import scala.language.reflectiveCalls
//=============================================================================
//=============================================================================
trait MyInputStream extends MyStream {
  //---------------------------------------------------------------------------
  protected var totalByteSize = getByteSize()
  protected var remainByteCount = totalByteSize
  //---------------------------------------------------------------------------
  type SingleReadRequiredType     = {def read(arr: Array[Byte]): Int} //structural type definition
  type MultipleReadRequiredType   = {def read(arr: Array[Byte], off: Int, len: Int): Int} //structural type definition
  type SkipIntRequiredType        = {def skipBytes(size: Int): Int} //structural type definition
  //---------------------------------------------------------------------------
  def resetMark(): Boolean
  //---------------------------------------------------------------------------
  def skip(byteSize: Int): Boolean
  //---------------------------------------------------------------------------
  def getByteSize(): Long
  //---------------------------------------------------------------------------
  def read(byteSize: Int): (Boolean, Seq[Byte])
  //---------------------------------------------------------------------------
  def setReadPos(pos: Int): Boolean = { //absolute position
    Try {
      skip(pos) //it reset the mark
    }
    match {
      case Success(_) =>
      case Failure(ex) =>
        error(s"Error moving to read position '$pos' " + ex.toString)
        return false
    }
    true
  }
  //---------------------------------------------------------------------------
  def longRead(byteSize: Long
               , bufferByteSize: Int = Int.MaxValue/100): (Boolean, Seq[Byte]) = {
    if(byteSize == 0 ) return (true, Seq())
    if(byteSize < 0 ) return (false, Seq())
    val iterations = (byteSize / bufferByteSize).toInt
    val remain = (byteSize % bufferByteSize).toInt
    val r = (for( _ <- 1 to iterations) yield read(bufferByteSize)) :+ read(remain)
    r.foreach{ t => if (!t._1)  return (false, Seq())}
    (true,r.flatMap(  _._2))
  }

  //---------------------------------------------------------------------------
  def longSkip(byteSize: Long
               , bufferByteSize: Int = Int.MaxValue/100): Boolean = {
    if (byteSize == 0) return true
    if (byteSize < 0) return false
    val iterations = (byteSize / bufferByteSize).toInt
    val remain = (byteSize % bufferByteSize).toInt
    val r = (for (_ <- 1 to iterations) yield skip(bufferByteSize)) :+ skip(remain)
    r.foreach { t => if (!t) return false }
    true
  }
  //---------------------------------------------------------------------------
  protected def singleRead[A <: SingleReadRequiredType](stream: A,byteSize: Int): (Boolean, Seq[Byte]) = {

    var d: Array[Byte] = null
    var bufferWasFullyRead = false

    if (!canRead(byteSize)) return (false, d)
    streamPointer += byteSize
    remainByteCount -= byteSize
    Try {
      d = new Array[Byte](byteSize)
      val readCount = stream.read(d)
      bufferWasFullyRead = readCount == byteSize
      if (!bufferWasFullyRead)
        error(s"Error partial reading from file: '$name' . Expecting to read: $byteSize but read only $readCount. Missing " + (byteSize - readCount) + " bytes")
    } match {
      case Success(_) =>
        (bufferWasFullyRead, d)
      case Failure(ex) =>
        error(s"Error reading from file: '$name' " + ex.toString)
        (false, d)
    }
  }
  //---------------------------------------------------------------------------
  protected def multipleRead[A <: MultipleReadRequiredType](stream: A, byteSize: Int): (Boolean, Seq[Byte]) = {
    var d: Array[Byte] = null
    var bufferWasFullyRead = false

    if (!canRead(byteSize)) return (false, d)

    streamPointer += byteSize
    remainByteCount -= byteSize

    var offset: Int = 0
    d = new Array[Byte](byteSize)
    Try {
      do {
        val byteCountToRead = byteSize-offset
        val readCount = stream.read(d,offset,byteCountToRead)
        bufferWasFullyRead = readCount == byteCountToRead
        if (!bufferWasFullyRead) offset += readCount
      } while(!bufferWasFullyRead)
    } match {
      case Success(_) =>
        (bufferWasFullyRead, d)
      case Failure(ex) =>
        error(s"Error reading from file: '$name' " + ex.toString)
        (false, d)
    }
  }
  //---------------------------------------------------------------------------
  protected def mySkip[A <: SkipIntRequiredType](stream: A,
                                                 byteSize: Int): Boolean = {
    if (byteSize == 0) return true
    if (!canRead(byteSize)) return false
    streamPointer += byteSize
    remainByteCount -= byteSize
    var r: Boolean = false
    Try {
      r = stream.skipBytes(byteSize) == byteSize
    } match {
      case Success(_) => r
      case Failure(ex) =>
        error(
          s"Error skipping $byteSize bytes from stream: $name " + ex.toString)
        false
    }
  }
  //---------------------------------------------------------------------------
  def canRead(byteSize: Long): Boolean =
    byteSize match {
      case 0 => true
      case _ if byteSize < 0 => false
      case _ => remainByteCount >= byteSize
    }
  //---------------------------------------------------------------------------
  def getRemainByteCount: Long = remainByteCount
  //---------------------------------------------------------------------------
  def getReadPosition: Long = streamPointer
  //---------------------------------------------------------------------------
  def isEnfOfFile = remainByteCount == 0
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MyInputStream.scala
//=============================================================================
