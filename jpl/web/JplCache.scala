
/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Oct/2023
 * Time:  17h:30m
 * Description: None
 */
//=============================================================================
package com.common.jpl.web
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.path.Path
//=============================================================================
import scala.io.Source
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object JplCache {
  //---------------------------------------------------------------------------
  type JPL_TIME_POS_MAP = scala.collection.mutable.Map[String, Point2D_Double]
  type JPL_ORBIT_TIME_POS = scala.collection.mutable.Map[String, JPL_TIME_POS_MAP]
  type JPL_CACHE = scala.collection.mutable.Map[String, JPL_ORBIT_TIME_POS]
  //---------------------------------------------------------------------------
  private val CACHE_PATH = "output/jpl_web_query_cache/"
  private val TIME_CACHE_FILE_NAME = "time_pos_cache"
  //---------------------------------------------------------------------------
  private val itemSeparator = ","
  //---------------------------------------------------------------------------
}
//=============================================================================
import JplCache._
case class JplCache() extends MyLogger {
  //---------------------------------------------------------------------------
  private val jplCache: JPL_CACHE = scala.collection.mutable.Map[String, JPL_ORBIT_TIME_POS]()
  //---------------------------------------------------------------------------
  def load(objectName: String): Unit = {
    if (jplCache.contains(objectName)) return
    Path.ensureDirectoryExist(s"$CACHE_PATH/$objectName")
    val jplOrbitPathSeq = Path.getSubDirectoryList(s"$CACHE_PATH/$objectName")
    val jplOrbitTimePosMap = scala.collection.mutable.Map[String, JPL_TIME_POS_MAP]()
    jplOrbitPathSeq.foreach { jplOrbitPath =>

      val jplTimePosMap = scala.collection.mutable.Map[String, Point2D_Double]()
      val fileName = jplOrbitPath + s"/$TIME_CACHE_FILE_NAME"
      if (!MyFile.fileExist(fileName)) return
      val bufferedSource = Source.fromFile(jplOrbitPath + s"/$TIME_CACHE_FILE_NAME")
      for (line <- bufferedSource.getLines) {
        val lineSplit = line.trim.split(itemSeparator)
        val timeStamp = lineSplit(0).trim
        val pos = Point2D_Double(lineSplit(1).toDouble
          , lineSplit(2).toDouble)
        jplTimePosMap(timeStamp) = pos
      }
      bufferedSource.close
      jplOrbitTimePosMap(jplOrbitPath.getName) = jplTimePosMap
    }
    jplCache(objectName) = jplOrbitTimePosMap
  }
  //---------------------------------------------------------------------------
  private def reset() = {
    jplCache.clear()
    Path.resetDirectory(CACHE_PATH)
  }
  //---------------------------------------------------------------------------
  def getPos(objectName: String
             , orbit: String
             , timeStamp: String): Option[Point2D_Double] = {
    if (!jplCache.contains(objectName)) return None
    if (!jplCache(objectName).contains(orbit)) return None
    if (!jplCache(objectName)(orbit).contains(timeStamp)) return None
    Some(jplCache(objectName)(orbit)(timeStamp))
  }
  //---------------------------------------------------------------------------
  def add(objectName: String
          , orbit: String
          , time: String
          , pos: Point2D_Double) = {

    //store in memory
    if (!jplCache.contains(objectName)) jplCache(objectName) = scala.collection.mutable.Map[String, JPL_TIME_POS_MAP]()
    if (!jplCache(objectName).contains(orbit)) jplCache(objectName)(orbit) = scala.collection.mutable.Map[String, Point2D_Double]()
    jplCache(objectName)(orbit)(time) = pos

    //store in file
    val dir =  s"$CACHE_PATH/$objectName/$orbit/"
    Path.ensureDirectoryExist(dir)
    val fileName = s"$dir/$TIME_CACHE_FILE_NAME"

    val bw = new BufferedWriter(new FileWriter(new File(fileName), true)) //open for append
    bw.write(s"$time$itemSeparator${pos.x}$itemSeparator${pos.y}\n")
    bw.close
    info(s"JPL web query cache. Added entry to: '$fileName'")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file JplCache.scala
//=============================================================================