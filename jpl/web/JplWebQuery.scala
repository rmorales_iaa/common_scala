/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Oct/2023
 * Time:  16h:41m
 * Description: None
 */
//=============================================================================
package com.common.jpl.web
//=============================================================================
import com.common.configuration.MyConf
import com.common.geometry.point.Point2D_Double
import com.common.logger.MyLogger
import scalaj.http.{Http, HttpResponse}
//=============================================================================
//=============================================================================
object JplWebQuery extends MyLogger {
  //---------------------------------------------------------------------------
  private val verbose = MyConf.c.getInt("Verbosity.verbose") == 1
  //---------------------------------------------------------------------------
  private final val JPL_RESPOSE_MAGIC_TOKEN = "$$SOE"
  //---------------------------------------------------------------------------
  private final val cache = JplCache()
  //---------------------------------------------------------------------------
  private final val URL_ROOT            = "https://ssd.jpl.nasa.gov/api/horizons.api?"
  private final val connectionTimeoutMs = 10 * 1000
  private final val readTimeoutMs       = 30 * 1000
  //---------------------------------------------------------------------------
  //require completion
  private final val URL_PARAMETER_COMMAND      = "COMMAND="
  private final val URL_PARAMETTER_CENTER      = "CENTER="
  private final val URL_PARAMETTER_TLIST       = "TLIST="

  //constants
  private final val URL_PARAMETER_TEXT         = "format=text"

  private final val URL_PARAMETTER_OBJECT_DATA = "OBJ_DATA='YES'"
  private final val URL_PARAMETTER_MAKE_EPHEM  = "MAKE_EPHEM='YES'"
  private final val URL_PARAMETTER_EPHEM_TYPE  = "EPHEM_TYPE='OBSERVER'"
  private final val URL_PARAMETTER_TLIST_TYPE  = "TLIST_TYPE='CAL'"

  private final val URL_PARAMETTER_QUANTITIES  = "QUANTITIES='1'"
  private final val URL_PARAMETTER_EXTRA_PREC  = "EXTRA_PREC='YES'"
  private final val URL_PARAMETTER_ANG_FORMAT  = "ANG_FORMAT='DEG'"
  //---------------------------------------------------------------------------
  private final val URL_CONSTANT_SEQ = Seq(
      URL_PARAMETER_TEXT
    , URL_PARAMETTER_OBJECT_DATA
    , URL_PARAMETTER_MAKE_EPHEM
    , URL_PARAMETTER_EPHEM_TYPE
    , URL_PARAMETTER_TLIST_TYPE
    , URL_PARAMETTER_QUANTITIES
    , URL_PARAMETTER_EXTRA_PREC
    , URL_PARAMETTER_ANG_FORMAT
  )
  //---------------------------------------------------------------------------
  private def getCommand(objectName:String)      =  s"$URL_PARAMETER_COMMAND'DES=$objectName'"
  private def getCenter(observatoryCode: String) =  s"$URL_PARAMETTER_CENTER'$observatoryCode'"
  private def getTimeSeq(timeSeq:Seq[String])    =  s"$URL_PARAMETTER_TLIST${timeSeq.mkString("'",", ","'")}"
  //---------------------------------------------------------------------------
  private def buildUrl(objectName:String
                       , observatoryCode: String
                       , timeSeq:Seq[String]) =
    URL_ROOT +
      (URL_CONSTANT_SEQ ++
      Seq(getCommand(objectName)
      , getCenter(observatoryCode)
      , getTimeSeq(timeSeq))).mkString("&")
  //---------------------------------------------------------------------------
  private def httpRequest(objectName: String
                          , observatoryCode: String
                          , timeStamp: String): Option[String] = {
    val url = buildUrl(objectName,observatoryCode,Seq(timeStamp))
    info(s"Querying JPL $url")
    val result: HttpResponse[String] = Http(url)
      .charset("UTF-8")
      .timeout(connectionTimeoutMs, readTimeoutMs)
      .asString
    if (result.code != 200) return {
      error(s"Error requesting url from JPL Horizons API. Error code: ${result.body}")
      error(s"url:$url")
      error(s"Error message:${result.headers}")
      None
    }
    if (result.headers.isEmpty) None
    else Some(result.body)
  }
  //---------------------------------------------------------------------------
  private def parseResponse(response: String) = {
    val lines = response.split('\n')
    val start = lines.indexWhere(_.contains(JPL_RESPOSE_MAGIC_TOKEN)) + 1
    val end = start+1
    val split = lines.slice(start, end)
                 .mkString("\n")
                 .trim
                 .split(" ")
                 .filter(!_.isEmpty)

    Point2D_Double(split(split.length-2).trim.toDouble, split.last.trim.toDouble)
  }
  //---------------------------------------------------------------------------
  private def normalizeTime(t: String) =
    t.replaceAll("T", " ")
  //---------------------------------------------------------------------------
  def getPos(objectName: String
             , normalizedName: String
             , orbit: String
             , observatoryCode: String
             , timeSeq: Array[String]): Array[Point2D_Double] = {

    synchronized {
      cache.load(normalizedName)
      timeSeq.map { timeStamp =>
        val pos = cache.getPos(
            normalizedName
          , orbit
          , timeStamp)
        if (pos.isDefined) {
          if(verbose) info(s"Used JPL cache for query oj object '$normalizedName' orbit '$orbit' observatory code: '$observatoryCode' timeStamp '$timeStamp'")
          pos.get
        }
        else {
          val response = httpRequest(
            objectName
            , observatoryCode
            , normalizeTime(timeStamp)).getOrElse(return Array())
          val pos = parseResponse(response)
          cache.add(normalizedName, orbit, timeStamp, pos)
          pos
        }
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file JplWebQuery.scala
//=============================================================================