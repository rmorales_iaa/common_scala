/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  13/Sep/2021
 * Time:  01h:50m
 * Description: Compressed database: wget ftp://ssd.jpl.nasa.gov/pub/xfr/dastcom5.zip
 * update script: /home/rafa/images/tools/spice/horizons/xfr/dastcom5/dat/update.sh
 */
//=============================================================================
package com.common.jpl.horizons.db
//=============================================================================
import com.common.database.mongoDB.Helpers.DocumentObservable
import com.common.database.mongoDB.MongoDB
//=============================================================================
import org.mongodb.scala.model.Collation
import com.mongodb.client.model.CollationStrength
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
import org.mongodb.scala.model.Filters.{equal}
//=============================================================================
object HorizonsDB {
  //---------------------------------------------------------------------------
  val collationCaseInsensitive = Collation
    .builder()
    .locale("en")
    .collationStrength(CollationStrength.PRIMARY).build
  //---------------------------------------------------------------------------
  private val cacheNameMap = scala.collection.mutable.Map[String,Seq[Document]]()
  //---------------------------------------------------------------------------
}
//=============================================================================
import HorizonsDB._
case class HorizonsDB(databaseName: String
                      , collectionName : String) extends MongoDB {
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = init()
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  connected = true
  //---------------------------------------------------------------------------
  def existByName(name: String) =
    collection.find(equal("name", name))
      .collation(collationCaseInsensitive)
      .resultCount > 0
  //---------------------------------------------------------------------------
  private def getByName(name: String) = {
    if (cacheNameMap.contains(name)) cacheNameMap(name)
    else {
      collection.find(equal("name", name))
        .collation(collationCaseInsensitive)
        .results()
    }
  }
  //---------------------------------------------------------------------------
  def getByAlternateName(name: String) = {
    if (cacheNameMap.contains(name)) cacheNameMap(name)
    else {
      collection.find(equal("alternateName", name))
        .collation(collationCaseInsensitive)
        .projection(MongoDB.getProjectionColList(Seq("name", "spk_id", "alternateName","_id")))
        .results()
    }
  }
  //---------------------------------------------------------------------------
  //(spk_id,name,mpc_id)
  private def getByAnyName(name: String) = {
    if (cacheNameMap.contains(name)) cacheNameMap(name)
    else {
      val docSeq = getByName(name) ++ getByAlternateName(name)
      if (!docSeq.isEmpty) cacheNameMap(name) = docSeq
      docSeq
    }
  }
  //---------------------------------------------------------------------------
  //(spk_id,name,mpc_id)
  def getByAnyName(name: String, verbose: Boolean = true): Option[(Long, String, Long)] = {
    var docSec: Seq[Document] = getByAnyName(name)
    if (docSec.isEmpty) {
      docSec = getByAnyName(name.replaceAll("_", " "))
      if (docSec.isEmpty) {
        if (verbose) error(s"The mpo name: '$name' was not found in the horizons database")
        return None
      }
    }
    val doc = docSec.head  //get only the first match
    Some((  doc("spk_id").asInt64().getValue
          , doc("name").asString().getValue
          , doc("_id").asInt64().getValue))
  }
  //---------------------------------------------------------------------------
  def getMPC_ID_ByAnyName(name: String, verbose: Boolean = true): Option[Long] = {
    val r = getByAnyName(name,verbose)
    if (r.isDefined) Some(r.get._3) else None
  }
  //---------------------------------------------------------------------------
  def getByMpcId(id: Int) =
    collection.find(equal("_id", id))
      .results()
  //---------------------------------------------------------------------------
  def getBySpkId(id: Long) =
    collection.find(equal("spk_id", id))
      .results()
  //---------------------------------------------------------------------------
  def existByMpcId(id: Long) =
    collection.find(equal("_id", id))
      .resultCount > 0
  //---------------------------------------------------------------------------
  def existBySpkId(id: Long) =
    collection.find(equal("spk_id", id))
      .resultCount > 0
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file HorizonsDB.scala
//=============================================================================
