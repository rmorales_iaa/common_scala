/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  17/Feb/2022
  * Time:  20h:56m
  * Description: None
  */
//=============================================================================
package com.common.jpl.horizons.db.smallBody
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.spiceSPK.SpiceSPK_DB
import com.common.jpl.horizons.Horizons
import com.common.logger.MyLogger
import com.common.util.mail.MailAgent
import com.common.util.path.Path
import ujson.Arr

import scala.collection.mutable.ArrayBuffer
//=============================================================================
import scalaj.http.{Http, HttpResponse}
import java.time.LocalDate
import scala.collection.mutable.ListBuffer
import scala.util.{Failure, Success, Try}
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object SmallBodyDB extends MyLogger {
  //---------------------------------------------------------------------------
  final val DEFAULT_DOWNLOAD_DIRECTORY = "output/spk"
  //---------------------------------------------------------------------------
  private final val conf                             = MyConf(MyConf.c.getString("Database.smallBodyDB"))
  private final val startDate                        = LocalDate.parse(conf.getString("SmallBodyDB.httpRequest.startDate"))
  private final val endDate                          = LocalDate.parse(conf.getString("SmallBodyDB.httpRequest.endDate"))
  private final val httpRequestRetryTimeMillis       = conf.getInt("SmallBodyDB.httpRequest.waitingSecondsTimeBetweenConsecutiveRequest") * 1000
  private final val httpRequestWaitingSecondsBetween = conf.getInt("SmallBodyDB.httpRequest.retryTimes")
  //---------------------------------------------------------------------------
  private final val URL = "https://ssd-api.jpl.nasa.gov/sbdb_query.api?full-prec=true" +
                          "&sb-class=CEN,TNO" + //Centaurus plus TNO
                          "&fields=full_name,spkid,pdes,orbit_id" //fields to return
  //---------------------------------------------------------------------------
  private final var mailAgent: MailAgent = null
  private final val mailFrom             = "rmorales@iaa.es"
  private final val mailTo               = "nicolas@iaa.es"
  private final val mailSubjectUpdated   = "[henosis]: local SPK repository updated:"
  private final val mailSubjectError     = "[henosis]: local SPK repository error when updating"
  private final val mailCC               = "rmorales@iaa.es"
  //---------------------------------------------------------------------------
  private val horizons = Horizons()
  //---------------------------------------------------------------------------
  case class Response(signature: Signature, fields: Seq[String], data: Seq[Arr], count: Int)
  case class Signature(source: String, version: String)
  case class MPO(name: String
                 , spkId: Long
                 , orbitID: String) {
    //-------------------------------------------------------------------------
    override def toString() = s"name:'$name' spkid:$spkId orbitID:'$orbitID'"
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private final val connectionTimeoutMs = 10 * 1000
  private final val readTimeoutMs       = 30 * 1000
  //---------------------------------------------------------------------------
}
//=============================================================================
import SmallBodyDB._
case class SmallBodyDB() extends MyLogger {
  //---------------------------------------------------------------------------
  private val updatedSPK_List = ListBuffer[String]()
  //---------------------------------------------------------------------------
  private def httpRequest(url: String) : Option[String] = {
    info(s"JPL Small-Body Database query:'$url'")
    val result: HttpResponse[String] = Http(url)
      .charset("UTF-8")
      .timeout(connectionTimeoutMs,readTimeoutMs)
      .asString
    if (result.code != 200) return {
      error(s"Error requesting url from JPL Horizons API. Error code: ${result.body}")
      error(s"url:$url")
      error(s"Error message:${result.headers}")
      None
    }
    if (result.headers.isEmpty) None
    else Some(result.body)
  }
  //-------------------------------------------------------------------------
  private def downloadSPK(mpo: MPO, storagePath: String) : Boolean = {
    info(s"Retrieving mpo:${mpo.toString} from JPL Horizons request API")
    for(tryCount <-0 until httpRequestWaitingSecondsBetween) {
      if (Horizons.downloadSPK(mpo.name
                               , mpo.orbitID
                               , mpo.spkId
                               , startDate
                               , endDate
                               , storagePath)) {
        updatedSPK_List += mpo.toString()
        return true
      }
      else {
        error(s"MPO:'${mpo.toString}' Try:$tryCount. Remain trys: ${httpRequestWaitingSecondsBetween-tryCount}. Error getting the SPK from JPL Horizons request API")
        if (httpRequestRetryTimeMillis > 0) Thread.sleep(httpRequestRetryTimeMillis)
      }
      error(s"MPO:'${mpo.toString}' getting the SPK from JPL Horizons request API. Stopping the sync")    }
    false
  }
  //-------------------------------------------------------------------------
  private def getName(s: String) = {
    var name = s.split("\\(")(0)
      .trim
      .replaceAll(" +", " ")
      .replaceAll("/", "_")
      .split(" ")
      .drop(1)
      .mkString("_")
    if (name.isEmpty) {
      name = s.split("\\(")(1)
      name = name.dropRight(1)
    }
    name
  }
  //-------------------------------------------------------------------------
  def builRemoteMpoSeq(response: Response) = {
    info(s"Parsing:${response.count} mpos's from remote database")
    val validSpkID_Map = SpiceSPK_DB.getLastestSpkMap()
    response.data.flatMap { ast=>
      val s = ast.value(0).value.toString.trim
      val spkId = ast.value(1).value.toString.trim.toDouble.toLong
      val mpo = MPO(name = getName(s)
                    , spkId
                    , orbitID = ast.value(3).value.toString.trim.replace("JPL ","").trim)

      if (mpo.spkId == 20134340) {
        warning("Ignoring:'Pluto' because JPL generates an error")
        None
      }
      else {
        if (validSpkID_Map.contains(mpo.spkId)) Some(mpo)
        else None
      }
    }
  }
  //----------------------------------------------------------------------------
  private def senMailError(message:String) = {
    val mailContent = "Please contact with rmorales@iaa to trace the source of the error in the synchronization." +
      s"\nError message:\n'$message'"+
      s"\nError message ends"
    getMailAgent().send(
        mailTo
      , mailFrom
      , mailSubjectError
      , mailContent
      , mailCC)
  }
  //----------------------------------------------------------------------------
  private def getMailAgent() = {
    if (mailAgent == null)  mailAgent = MailAgent()
    mailAgent
  }
  //----------------------------------------------------------------------------
  private def sendMailUpdated() = {
    val mailContent = "--- List of SPK updated start \n" + updatedSPK_List.mkString("\n") + "\n--- List of SPK updated end"
    getMailAgent().send(
        mailTo
      , mailFrom
      , mailSubjectUpdated + updatedSPK_List.size
      , mailContent
      , mailCC)
  }
  //----------------------------------------------------------------------------
  private def saveUpdatedList() = {
    val bw = new BufferedWriter(new FileWriter(new File("output/spk_updated.txt")))
    bw.write(updatedSPK_List.mkString("\n"))
    bw.close()
  }
  //----------------------------------------------------------------------------
  //It syncs the SPK and versions on the local repository against the remote database (JPL's SmallBodyDB)
  def syncWithRemoteDB(debug: Boolean = false): Boolean = {
    info(s"Synchronizing local SPICE SPK database with JPL remote database. Downloading NEW versions and NEW SPK")

    //download remote repository and parseInput it
    Try {
      val r = httpRequest(URL).getOrElse(return false)
      //val source = scala.io.Source.fromFile("/home/rafa/proyecto/m3/output/remote_sync_jpl_db.json")
      //val r = try source.mkString finally source.close()

      if (debug) {
        val bw = new BufferedWriter(new FileWriter(new File("output/jpl_spice_response.json")))
        bw.write(r)
        bw.close()
      }
      val validatedResponse = ujson.read(r)
      val sig = validatedResponse("signature")
      val signature = Signature(sig("version").value.toString, sig("source").value.toString)
      val fields =  validatedResponse("fields").value.asInstanceOf[ArrayBuffer[String]].toSeq
      val data = validatedResponse("data").value.asInstanceOf[ArrayBuffer[Arr]].toSeq
      val count = validatedResponse("count").value.asInstanceOf[Double].toInt
      val response = Response(signature, fields, data, count)

      //parse Json response
      Path.resetDirectory(DEFAULT_DOWNLOAD_DIRECTORY)
      syncRemoteMpoSeq(builRemoteMpoSeq(response))
      SpiceSPK_DB.update(DEFAULT_DOWNLOAD_DIRECTORY)
    }
    match {
      case Success(_) => info(s"Success processing JSON stream from JPL")
      case Failure(ex) =>
        error(s"Error processing JSON stream from JPL " + ex.toString)
        senMailError(ex.toString)
        return false
    }

    if (updatedSPK_List.length == 0) info("None SPK updated")
    else {
      info(s"Updated SPK count:${updatedSPK_List.length}")
      saveUpdatedList
      sendMailUpdated
    }
    true
  }
  //---------------------------------------------------------------------------
  private def syncRemoteMpoSeq(remoteMpoSeq: Seq[MPO]) =
    remoteMpoSeq.foreach { remoteMpo =>
      val remoteMpoVersion = remoteMpo.orbitID
      if (!isVersionStored(remoteMpo.spkId, remoteMpoVersion))
         downloadSPK(remoteMpo, DEFAULT_DOWNLOAD_DIRECTORY)
    }
  //---------------------------------------------------------------------------
  def getVersion(spkID: Long) = {
    val db = SpiceSPK_DB(collectionName = spkID.toString)
    val r = db.getVersionSeq()
    db.close()
    if (r.isEmpty) "NOT_FOUND"
    else r.head
  }
  //---------------------------------------------------------------------------
  def isVersionStored(spkID: Long, v: String) = {
    val db = SpiceSPK_DB(collectionName = spkID.toString)
    val r = db.getVersionSeq()
    db.close()
    if(r.isEmpty) false
    else r.contains(v)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file SmallBodyDB.scala
//=============================================================================
