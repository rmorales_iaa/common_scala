/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/Jun/2021
 * Time:  17h:20m
 * Description: None
 */
//=============================================================================
package com.common.jpl.horizons
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.hardware.cpu.CPU
import com.common.logger.MyLogger
import com.common.util.path.Path
import com.common.jpl.horizons.entry.HorizonsEntry
import com.common.jpl.horizons.db.HorizonsDB
import com.common.util.file.MyFile
import com.common.util.parallelTask.ParallelTask
import com.common.util.util.Util
//=============================================================================
import scala.collection.JavaConverters._
import java.io.{BufferedReader, File, FileReader}
import java.io.FileOutputStream
import java.time.LocalDate
import java.util.Base64
import scalaj.http.{Http, HttpResponse}
import org.mongodb.scala.model.Filters.equal
import org.apache.commons.csv.{CSVFormat, CSVParser}
import java.net.URLEncoder
import java.nio.charset.StandardCharsets
import scala.language.postfixOps
import java.util.concurrent.ConcurrentLinkedQueue
//=============================================================================
//=============================================================================
object Horizons extends MyLogger {
  //---------------------------------------------------------------------------
  //JPL Horizons http request
  private val JPL_HORIZONS_HTTP_REQUEST         = "https://ssd.jpl.nasa.gov/api/horizons.api?format=text&OBJ_DATA=NO&EPHEM_TYPE=SPK&COMMAND="
  private val JPL_HORIZONS_HTTP_START_TIME      = "START_TIME="
  private val JPL_HORIZONS_HTTP_STOP_TIME       = "STOP_TIME="
  private val JPL_HORIZONS_HTTP_SIMPLE_QUOTATION="27"
  private val JPL_HORIZONS_HTTP_SEMICOLON       ="3B"
  private val JPL_HORIZONS_HTTP_SPACE           ="20"
  //---------------------------------------------------------------------------
  private val connectionTimeoutMs = 10 * 1000
  private val readTimeoutMs = 30 * 1000
  //---------------------------------------------------------------------------
  private val base64Decoder = Base64.getDecoder
  //---------------------------------------------------------------------------
  def getNormalizedSpkFileName(spkId: Long, name: String) =
    spkId + "_" +
      name
      .replaceAll(" ","_")
      .replaceAll("//", "_") +
      ".bsp"
  //---------------------------------------------------------------------------
  private def httpRequest(url: String) : Option[String] = {
    val result: HttpResponse[String] = Http(url)
      .charset("UTF-8")
      .timeout(connectionTimeoutMs,readTimeoutMs)
      .asString
    if (result.code != 200) return {
      error(s"Error requesting url from JPL Horizons API. Error code: ${result.body}")
      error(s"url:$url")
      error(s"Error message:${result.headers}")
      None
    }
    if (result.headers.isEmpty) None
    else {
      if (!result.body.take(128).contains("SPK Binary Data Follows")) None
      else Some(result.body)
    }
  }
  //---------------------------------------------------------------------------
  def downloadSPK(name: String
                  , version: String
                  , spkid: Long
                  , startTime: LocalDate
                  , stopTime: LocalDate
                  , outputDir: String): Boolean = {
    val url = s"$JPL_HORIZONS_HTTP_REQUEST'${URLEncoder.encode(s"DES=$spkid",StandardCharsets.UTF_8.toString)};'&" +
      s"$JPL_HORIZONS_HTTP_START_TIME'$startTime'&" +
      s"$JPL_HORIZONS_HTTP_STOP_TIME'$stopTime'"
    info(s"Retreiving url:'$url'")
    val r = httpRequest(url
      .replaceAll("'",s"%$JPL_HORIZONS_HTTP_SIMPLE_QUOTATION")
      .replaceAll(";",s"%$JPL_HORIZONS_HTTP_SEMICOLON")
      .replaceAll(" ",s"%$JPL_HORIZONS_HTTP_SPACE")
    )
    if (r.isEmpty) false
    else {
      val dir = outputDir + "/"
      val spkID= r.get.drop(56).take(128).split("\n").head.replaceAll("'","").trim
      val base64String= r.get.drop(112).replaceAll("\n","")
      val pos = base64String.indexOf("REFG")
      if(pos == -1) return false
      val byteSeq = base64Decoder.decode(base64String.drop(pos))
      val rawName = name.
        trim
        .replaceAll(" +", " ")
        .replaceAll(" ", "_")
        .replaceAll("/", "_")
        .toLowerCase
      val fileName = s"$dir${spkID}_${version}_$rawName.bsp"
      val bw = new FileOutputStream(fileName)
      bw.write(byteSeq)
      bw.close()
      true
    }
  }
  //---------------------------------------------------------------------------
  //(spkid,version,name)
  def splitInComponentSeq(s: String): (String, String, String) = {

    if(!s.contains("_JPL")){
      val seq =
        s
        .trim
        .replace(".bsp", "")
        .split("_")

      val pp = seq.drop(2).mkString("_")
      (seq(0),seq(1),seq.drop(2).filter( _ != ".bsp" ).mkString("_"))
    }
    else {
      val seq = s
        .trim
        .replace(".bsp", "")
        .split("JPL")
      var spkid = seq(0).trim
      info(s"Processing kernel:'$s'")
      spkid = if (spkid.endsWith("_")) spkid.dropRight(1) else spkid
      val versionSeq = seq(1).split("_")
      (spkid, versionSeq(0), versionSeq(1))
    }
  }
  //---------------------------------------------------------------------------
  class NormalizeSingleSpkName(imageSeq: Array[File]
                                , oDir: String
                                , queue: ConcurrentLinkedQueue[(String,String,String,String)]
                                , horizons: Horizons) extends ParallelTask[File](
    imageSeq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(f: File) = {
      val kernalName = MyFile.getFileNameNoPathNoExtension(f.getAbsolutePath
        .trim
        .replaceAll(" ", "_"))
      val (spkid, version, name) = splitInComponentSeq(kernalName)
      val horizonsEntry = {
        val r = horizons.getBySpk_ID(spkid.toLong)
        if (r.isDefined) r
        else { //sometimes one zero is missing
          val fixedSpkID = spkid.take(1) + "0" + spkid.drop(1)
          warning(s"Retrying with spkid: $fixedSpkID")
          horizons.getBySpk_ID(fixedSpkID.toLong)
        }
      }

      val newKernelName =
        if (horizonsEntry.isDefined)
          s"$oDir/${spkid}_JPL${version}_${getNormalizedSpkFileName(spkid.toLong,name)}"
        else
          s"$oDir/${spkid}_JPL${version}_no_name.bsp"
       queue.add((spkid,version, f.getAbsolutePath, newKernelName))
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def normalizeSpkName(inputDir: String, outputDir: String) = {

    import sys.process._
    val oDir = Path.resetDirectory(outputDir)
    val fileSeq = Path.getSortedFileList(inputDir, ".bsp")
    val queue = new ConcurrentLinkedQueue[(String,String,String,String)]()
    val horizons = Horizons()
    new NormalizeSingleSpkName(fileSeq.toArray,oDir,queue,horizons)

    //get unique names
    val uniqueKernelMap = scala.collection.mutable.Map[String, (String,String,String)]()
    queue.forEach { case (spkid,version,oldPath,newPath) =>
      if (!uniqueKernelMap.contains(spkid)
        || (version > uniqueKernelMap(spkid)._1)) {
        uniqueKernelMap += ((spkid, (version, oldPath, newPath)))
      }
    }

    for ((spkID, (version, oldPath, newPath)) <- uniqueKernelMap) {
      val command = s"cp $oldPath $newPath"
      command !
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import Horizons._
case class Horizons(configurationName : String = "input/database/horizons/horizons.conf") extends MyLogger {
  //---------------------------------------------------------------------------
  private val conf = MyConf(configurationName,verbose=false)
  private val databaseName = conf.getString("Horizons.mongo.databaseName")
  //---------------------------------------------------------------------------
  val db = HorizonsDB(databaseName, conf.getString("Horizons.mongo.collectionIndex"))
  //---------------------------------------------------------------------------
  def getByMPC_ID(mpcID: Long, verbose:Boolean = true): Option[HorizonsEntry] =
    if (!db.existByMpcId(mpcID)) {
      if(verbose) error(s"The kernel with MPC ID: '$mpcID' does not exist in the Horizons database")
      None
    }
    else Some(HorizonsEntry(db.collection
      .find(equal("_id", mpcID))
      .collation(HorizonsDB.collationCaseInsensitive)
      .headResult()))
  //---------------------------------------------------------------------------
   def getBySpk_ID(spkID: Long, verbose:Boolean = true): Option[HorizonsEntry] =
    if (!db.existBySpkId(spkID)) {
      if (verbose) error(s"The kernel with SPK ID: '$spkID' does not exist in the Horizons database")
      None
    }
    else Some(HorizonsEntry(db.collection
      .find(equal("spk_id", spkID))
      .collation(HorizonsDB.collationCaseInsensitive)
      .headResult()))
  //---------------------------------------------------------------------------
  def getSpkFileNameByMPC_ID(mpcID: Long) = {
    val index = getByMPC_ID(mpcID)
    if (index.isDefined) getNormalizedSpkFileName(index.get.spkId, index.get.name.toLowerCase)
    else "NONE"
  }
  //---------------------------------------------------------------------------
  def getMpcIdByAnyName(name: String, verbose: Boolean = true) = {
    val mpcID = db.getMPC_ID_ByAnyName(name,verbose)
    if (mpcID.isDefined) mpcID.get
    else -1L
  }
  //---------------------------------------------------------------------------
  def getByName(name: String, verbose: Boolean = true) =
    if (!db.existByName(name)) {
      if (verbose) error(s"The kernel with name: '$name' does not exist in the Horizons database")
      None
    }
    else Some(HorizonsEntry(db.collection
      .find(equal("name", name))
      .collation(HorizonsDB.collationCaseInsensitive)
      .headResult()))

  //---------------------------------------------------------------------------
  def getByAlternateName(name: String) = {
    val r = db.getByAlternateName(name)
    if (r.isEmpty) None
    else Some(HorizonsEntry(r.head))
  }
  //---------------------------------------------------------------------------
  def getSPK_ID_ByName(name: String) = {
    val index = getByName(name)
    if (index.isDefined) index.get.spkId
    else -1
  }
  //---------------------------------------------------------------------------
  def getSPK_ID_ByMpcID(mpcId: Long) = {
    val index = getByMPC_ID(mpcId)
    if (index.isDefined) index.get.spkId
    else -1
  }
  //---------------------------------------------------------------------------
  def getMpc_ID_BySpkID(spkId: Long) = {
    val index = getBySpk_ID(spkId)
    if (index.isDefined) index.get.mpc_id
    else -1
  }
  //---------------------------------------------------------------------------
  def importData() = {
    //take care to compiling options
    val updateScript = conf.getString("Horizons.dastcom5.updateScript")
    Util.runShellCommandAndGetBinaryOutput_2(updateScript)
    val rootPath = Path.ensureEndWithFileSeparator(conf.getString("Horizons.jplSourceDatabase.rootPath"))
    importDatabaseIndex(rootPath + conf.getString("Horizons.jplSourceDatabase.index"))
  }
  //---------------------------------------------------------------------------
  private def importDatabaseIndex(fileName: String) = {
    info(s"Rebuilding the JPL Horizons index database from file: '$fileName'")
    db.dropCollection()
    val br = new BufferedReader(new FileReader(new File(fileName)))
    val inputFormat = CSVFormat.DEFAULT
    //get the doc sequence
    val docSeq = new CSVParser(br, inputFormat).getRecords.asScala map { r =>
      //mpc_id,name,spkId,alternate
      val seq = r.get(0).trim.split(" ")
      val alternativeNameSeq = for(i<-2 to r.size - 2) yield r.get(i)
      HorizonsEntry(
          seq(0).toLong              //mpc_id
        , seq.drop(1).mkString(" ")  //name (primary designation)
        , r.get(1).toLong            //spkId
        , alternativeNameSeq
      ).getDocument()
    }
    br.close
    info(s"Rebuilding the Mongo Horizons index database")
    //insert all doc sequence
    db.getCollection.insertMany(docSeq.toArray).headResult

    //create the indexes on name
    db.createIndex("name",_unique = false)
    db.createIndex("alternateName",_unique = false)
  }
  //---------------------------------------------------------------------------
  def close() = db.close()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Horizons.scala
//=============================================================================
