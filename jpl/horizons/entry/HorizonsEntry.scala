/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/Jun/2021
 * Time:  19h:27m
 * Description: None
 */
//=============================================================================
package com.common.jpl.horizons.entry
//=============================================================================
import org.mongodb.scala.bson.collection.immutable.Document
import scala.collection.JavaConverters.asScalaBufferConverter
//=============================================================================
object HorizonsEntry {
  //---------------------------------------------------------------------------
  def apply(doc: Document): HorizonsEntry =
    HorizonsEntry(
      doc("_id").asInt64().getValue
    , doc("name").asString.getValue
    , doc("spk_id").asInt64.getValue
    , doc("alternateName").asArray.getValues.asScala.toArray.map( t=> t.asString().getValue)
    )
  //---------------------------------------------------------------------------
}
//=============================================================================
case class HorizonsEntry(mpc_id: Long, name:String, spkId:Long, alternateNameSeq: Seq[String]) {
  //---------------------------------------------------------------------------
  def getDocument () =
     Document (
       "_id"             -> mpc_id
       , "name"          -> name
       , "spk_id"        -> spkId
       , "alternateName" -> alternateNameSeq
     )
  //---------------------------------------------------------------------------
  }
//=============================================================================
//End of file HorizonsEntry.scala
//=============================================================================
