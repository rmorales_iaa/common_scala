/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Aug/2024
 * Time:  12h:33m
 * Description: https://ssd.jpl.nasa.gov/planets/eph_export.html
 */
package com.common.jpl.ephemeris
//=============================================================================
import com.common.configuration.MyConf
import com.common.logger.MyLogger
import com.common.util.path.Path
import com.common.jpl.ephemeris.ChebyshevInterpolation._
import com.common.jpl.ephemeris.JplEphemerisItem._
import com.common.jpl.ephemeris.JplEphemerisRecord.LoadEphemerisRecordSeq
//=============================================================================
import java.io.BufferedReader
import scala.util.{Failure, Try}
import java.util.concurrent.ConcurrentHashMap
import scala.collection.JavaConverters.mapAsScalaMapConverter
import scala.collection.mutable.ArrayBuffer
import java.io.{File, FileReader}
import scala.language.existentials
//=============================================================================
//=============================================================================
object JplEphemeris extends MyLogger {
  //---------------------------------------------------------------------------
  def loadFromAsciiDir(): Option[JplEphemeris] = {
    val dirName = MyConf.c.getString("JplEphemeris.dir")
    val pathSeq = Path.getSortedFileList(dirName, extension = "").map(_.getAbsolutePath)

    //load record seq
    val recordSeqStorage = new ConcurrentHashMap[Int, Array[JplEphemerisRecord]]
    new LoadEphemerisRecordSeq(pathSeq.dropRight(1), recordSeqStorage)

    //sort the records according to filename to avoid mixtures from different files
    val recordSeq = recordSeqStorage
      .asScala
      .toArray
      .sortWith(_._1 < _._1)
      .map { t => t._2 }.flatten

    //load header
    val jplEphemeris = loadHeader(pathSeq.last
                                  , recordSeq).getOrElse(return None)
    Some(jplEphemeris)
  }
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[JplEphemeris].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }

  //---------------------------------------------------------------------------
  def getColNameValueSeq(prefix: String = "") =
    classOf[JplEphemeris].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = (prefix + f.getName, f.getType)
      f.setAccessible(false)
      res
    }
  //-------------------------------------------------------------------------
  private def getVersion(file: BufferedReader): Option[String] = {
    if (!skipLine(file, 2)) return None
    val tokenSeq = getFirstNotEmptyLine(file).getOrElse(return None)
      .trim
      .replaceAll(" +", " ")
      .split(" ")
    Some(tokenSeq(3))
  }

  //-------------------------------------------------------------------------
  private def getJulianDateInfo(file: BufferedReader): Option[(Double, Double, Int)] = {
    if (!skipLine(file, 3)) return None
    val line = getFirstNotEmptyLine(file).getOrElse(return None)
    val tokenSeq = line
      .trim
      .replaceAll(" +", " ")
      .split(" ")
    Some((tokenSeq(0).toDouble
      , tokenSeq(1).toDouble
      , tokenSeq(2).toDouble.toInt))
  }

  //-------------------------------------------------------------------------
  private def getConstantCount(file: BufferedReader): Option[Int] = {
    if (!skipLine(file, 1)) return None
    val tokenSeq = getFirstNotEmptyLine(file).getOrElse(return None)
      .trim
      .replaceAll(" +", " ")
      .split(" ")
    Some(tokenSeq.head.toInt)
  }

  //-------------------------------------------------------------------------
  private def getIndexTriplet(file: BufferedReader): Option[Array[Int]] = {
    val tokenSeq = getFirstNotEmptyLine(file).getOrElse(return None)
      .trim
      .replaceAll(" +", " ")
      .split(" ")
    Some(tokenSeq map (_.toInt))
  }

  //-------------------------------------------------------------------------
  private def getIndexTripletSeq(file: BufferedReader): Option[Array[Array[Int]]] = {
    if (!skipLine(file, 1)) return None
    val tSeq0 = getIndexTriplet(file).getOrElse(return None)
    val tSeq1 = getIndexTriplet(file).getOrElse(return None)
    val tSeq2 = getIndexTriplet(file).getOrElse(return None)
    Some((for (i <- 0 until tSeq0.size) yield
      Array(tSeq0(i), tSeq1(i), tSeq2(i))).toArray)
  }
  //-------------------------------------------------------------------------
  def getFirstNotEmptyLine(file: BufferedReader): Option[String] = {
    var line = file.readLine()
    while (line != null) {
      if (!line.trim.isEmpty) return Some(line)
      line = file.readLine()
    }
    None
  }
  //-------------------------------------------------------------------------
  def skipLine(file: BufferedReader
               , lineCount: Int): Boolean = {
    for (_ <- 0 until lineCount) {
      val line = getFirstNotEmptyLine(file)
      if (line.isEmpty) return false
    }
    true
  }
  //-------------------------------------------------------------------------
  def getDoubleSeq(file: BufferedReader
                   , count: Int): Option[Array[Double]] = {
    val tokenSeq = getFirstNotEmptyLine(file).getOrElse(return None)
      .trim
      .replaceAll(" +", " ")
      .split(" ")
      .take(count)
      .map(s => s.replace('D', 'E').toDouble)
    Some(tokenSeq)
  }

  //-------------------------------------------------------------------------
  def getTripletSeq(file: BufferedReader
                    , tripletCount: Int
                    , skipLineCount: Int = 0): Option[Array[Array[Double]]] = {
    if (skipLineCount > 0) if (!skipLine(file, skipLineCount)) return None
    Some((for (_ <- 0 until tripletCount) yield {
      val t = getDoubleSeq(file, 3)
      if (t.isEmpty) return None
      else t.get
    }).toArray)
  }
  //---------------------------------------------------------------------------
  private def loadHeader(headerFileName: String
                        , recordSeq: Array[JplEphemerisRecord]): Option[JplEphemeris] = {
    Try {
      val file = new BufferedReader(new FileReader(new File(headerFileName)))

      val version = getVersion(file).getOrElse {file.close(); return None}
      val (ephemStartJD, ephemEndjD, ephemStep) = getJulianDateInfo(file).getOrElse {file.close(); return None}
      val constantCount = getConstantCount(file).getOrElse {file.close(); return None}
      val constantSeq = getTripletSeq(file, constantCount / 3, 67).getOrElse {file.close(); return None}
      val coeffIndexSeq = getIndexTripletSeq(file).getOrElse {file.close(); return None}

      file.close()

      val jplEphemerisItem = JplEphemerisItem(
        mercury = JplEphemerisItemDataPlanet(coeffIndexSeq(0)(0), coeffIndexSeq(0)(1), coeffIndexSeq(0)(2))
        , venus = JplEphemerisItemDataPlanet(coeffIndexSeq(1)(0), coeffIndexSeq(1)(1), coeffIndexSeq(1)(2))
        , earthMoonBarycenter = JplEphemerisItemDataPlanet(coeffIndexSeq(2)(0), coeffIndexSeq(2)(1), coeffIndexSeq(2)(2))
        , mars = JplEphemerisItemDataPlanet(coeffIndexSeq(3)(0), coeffIndexSeq(3)(1), coeffIndexSeq(3)(2))
        , jupiter = JplEphemerisItemDataPlanet(coeffIndexSeq(4)(0), coeffIndexSeq(4)(1), coeffIndexSeq(4)(2))
        , saturn = JplEphemerisItemDataPlanet(coeffIndexSeq(5)(0), coeffIndexSeq(5)(1), coeffIndexSeq(5)(2))
        , uranus = JplEphemerisItemDataPlanet(coeffIndexSeq(6)(0), coeffIndexSeq(6)(1), coeffIndexSeq(6)(2))
        , neptune = JplEphemerisItemDataPlanet(coeffIndexSeq(7)(0), coeffIndexSeq(7)(1), coeffIndexSeq(7)(2))
        , pluto = JplEphemerisItemDataPlanet(coeffIndexSeq(8)(0), coeffIndexSeq(8)(1), coeffIndexSeq(8)(2))
        , moon = JplEphemerisItemDataPlanet(coeffIndexSeq(9)(0), coeffIndexSeq(9)(1), coeffIndexSeq(9)(2))
        , sun = JplEphemerisItemDataPlanet(coeffIndexSeq(10)(0), coeffIndexSeq(10)(1), coeffIndexSeq(10)(2))
        , nutation = JplEphemerisItemDataNutation(coeffIndexSeq(11)(0), coeffIndexSeq(11)(1), coeffIndexSeq(11)(2))
        , lunarLibration = JplEphemerisItemDataLibration(coeffIndexSeq(12)(0), coeffIndexSeq(12)(1), coeffIndexSeq(12)(2))
        , lunarMantle = JplEphemerisItemDataMantle(coeffIndexSeq(13)(0), coeffIndexSeq(13)(1), coeffIndexSeq(12)(2))
        , ttTDT = JplEphemerisItemDataTime(coeffIndexSeq(14)(0), coeffIndexSeq(14)(1), coeffIndexSeq(14)(2))
      )
      val kernelByteSize = 4 + jplEphemerisItem.getKernelByteSize
      val jplEphemeris = JplEphemeris(
        version
        , ephemStartJD
        , ephemEndjD
        , ephemStep
        , constantCount
        , constantSeq
        , coeffIndexSeq
        , recordSeq
      )

      jplEphemeris.jplEphemerisItem  = jplEphemerisItem
      jplEphemeris.au = constantSeq(3)(0)
      jplEphemeris.earthMoonMassRatio = constantSeq(3)(1)
      jplEphemeris.kernelByteSize = kernelByteSize
      jplEphemeris.recordByteSize = kernelByteSize * 4
      jplEphemeris.coeffCount = kernelByteSize /2

      return Some(jplEphemeris)
    }
    match {
      case Failure(_: Exception) =>
        error(s"Error reading the JPL ephemeris header:'$headerFileName'")
        None
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class JplEphemeris(version: String
                        , ephemStartJD: Double                     //ephemeris start date
                        , ephemEndjD: Double                       //ephemeris end date
                        , ephemStep: Double                        //ephemeris step size
                        , constantCount: Int                       //constant count
                        , constantSeq: Array[Array[Double]]        //sequence of constants
                        , coeffIndexSeq: Array[Array[Int]]         //Index to Chebyshev polynomial coefficients pointers. 3 integers for e
                        , recordSeq: Array[JplEphemerisRecord]     //Chebyshev polynomial coefficients
                       ) extends MyLogger {
  //---------------------------------------------------------------------------
  //derived from original data or used in calculations
  var jplEphemerisItem: JplEphemerisItem = null //same as 'JplEphemeris.coeffIndexSeq' but with format
  var au: Double = Double.NaN
  var earthMoonMassRatio: Double = Double.NaN
  var kernelByteSize: Int = -1
  var recordByteSize: Int = -1
  var coeffCount: Int = -1
  //---------------------------------------------------------------------------
  //interpolationRequest[i]=0, no interpolation for body i
  //                       =1, position only
  //                       =2, position and velocity
  // return (state, nutation)
  def jplState(et: Double
               , interpolationRequestTypeSeq: Array[Int]
               , barycenter: Int): (ArrayBuffer[ArrayBuffer[Double]], Array[Double]) = {

    var pvSun: Array[Double] = Array()
    var pvSunJD: Double = Double.NaN
    val pv = ArrayBuffer.fill[Double](EPHEMERIS_PLANET_ITEM_COUNT, CHEBYSHEV_INTEGRATION_RESULT_COUNT)(Double.NaN)

    val nut = ArrayBuffer[Double]()
    val blockLoc = (et - ephemStartJD) / ephemStep
    val stepInfo = Array(blockLoc - blockLoc.toInt.toDouble, ephemStep)

    val aufac = 1.0 / au
    if (et < ephemStartJD || et > ephemEndjD) {
      error(s"jplState. Required time:'$et' is out of range: < ${ephemStartJD} or > ${ephemEndjD}")
      return (ArrayBuffer(), Array())
    }
    val record = findRecord(blockLoc.toInt + 3,et)
    val recomputePvSun =
      if (pvSunJD != et) {
        pvSunJD = et
        true
      }
      else false

    for (nIntervals <- Array(1, 2, 4, 8)) {
      for (i <- 0 until EPHEMERIS_TOTAL_ITEM_COUNT) {

        val (interpolationRequestType, _coeffIndexSeq) =
          if (i == 14)
            (if (recomputePvSun != 0) 3 else 0, coeffIndexSeq(SUN_EPHEMERIS_HEADER_ITEM_INDEX))
          else
            (interpolationRequestTypeSeq(i), coeffIndexSeq(if (i <= SUN_EPHEMERIS_HEADER_ITEM_INDEX) i else i + 1))

        if (nIntervals == _coeffIndexSeq(2) && interpolationRequestType > 0) {
          val interpolatedData = interpolate(record.chebyCoeffSeq.drop(_coeffIndexSeq(0) - 3)
            , stepInfo
            , _coeffIndexSeq(1)
            , jplEphemerisItem.getPhysicalUnitCount(i)
            , nIntervals
            , interpolationRequestType)
          //copy result
          if (i <= SUN_EPHEMERIS_HEADER_ITEM_INDEX) {
            //convert km to AU
            for (j <- 0 until interpolationRequestType * 3)
              pv(i)(j) = interpolatedData(j) * aufac
          }
          else {
            if (i == NUTATION_EPHEMERIS_HEADER_ITEM_INDEX)
              nut ++ interpolatedData.clone()
          }
        }
      }
    }
    pvSun = pv(SUN_EPHEMERIS_HEADER_ITEM_INDEX).toArray
    if (barycenter == 0) //gotta correct everybody for the solar system barycenter
      for (i <- 0 until CHEBYSHEV_INTEGRATION_RESULT_COUNT; j <- 0 until interpolationRequestTypeSeq(i) * 3)
        pv(i)(j) -= pvSun(j)

    (pv, nut.toArray)
  }
  //---------------------------------------------------------------------------
  private def findRecord(estimatedPos: Int
                        , jd: Double): JplEphemerisRecord = {
    if (recordSeq(estimatedPos).isIn(jd)) return recordSeq(estimatedPos)
    if (recordSeq(estimatedPos-1).isIn(jd)) return recordSeq(estimatedPos-1)
    if (recordSeq(estimatedPos+1).isIn(jd)) return recordSeq(estimatedPos+1)
    recordSeq.foreach{record=> if (record.isIn(jd)) return record}
    recordSeq(estimatedPos)
  }
  //---------------------------------------------------------------------------
  //position ion the result array:
  //location: 0,1,2
  //velocity: 3,4,5
  //acceleration: 6,7,8
  private def interpolate(coeffSeq: Array[Double]
                          , stepInfo: Array[Double]
                          , coeffCount: Int
                          , physicalUnitCount: Int
                          , na: Int
                          , interpolationRequestType: Int): Array[Double] = {
    val posAndVel = ArrayBuffer.fill[Double](CHEBYSHEV_INTEGRATION_RESULT_COUNT)(Double.NaN)
    val dna = na.toDouble
    val temp = dna * stepInfo(0)
    var l = temp.toInt
    var tc = 2.0 * (temp - l) - 1.0

    val iInfo = ChebyshevInterpolation()
    iInfo.posnCoeff(0) = 1d
    iInfo.posnCoeff(1) = -2
    iInfo.posnCoeff(0) = 1d
    iInfo.posnCoeff(1) = -2

    if (l == na) {
      l -= 1
      tc = 1.0
    }

    if (tc != iInfo.posnCoeff(1)) {
      iInfo.posnAvail = 2
      iInfo.velAvail = 2
      iInfo.posnCoeff(1) = tc
      iInfo.twot = tc + tc
    }

    //position is wanted
    //position calculations initialization
    if (iInfo.posnAvail < coeffCount) {
      for (i <- iInfo.posnAvail until coeffCount)
        iInfo.posnCoeff(i) =
          iInfo.twot * iInfo.posnCoeff(i - 1) - iInfo.posnCoeff(i - 2)
      iInfo.posnAvail = coeffCount
    }

    //interpolate to get position for each component
    var posAndVelIndex = 0
    for (i <- 0 until physicalUnitCount) {
      val coeffIndex = coeffCount * (i + l * physicalUnitCount + 1)
      val posnCoeffIndex = coeffCount
      var acc = 0d
      for (i <- 0 until coeffCount)
        acc += iInfo.posnCoeff(posnCoeffIndex - i - 1) * coeffSeq(coeffIndex - i - 1)
      posAndVel(posAndVelIndex) = acc
      posAndVelIndex += 1
    }

    if (interpolationRequestType <= 1) return posAndVel.toArray //no more calculations are wanted

    //if velocity interpolation is wanted, be sure enough
    //derivative polynomials have been generated and stored.

    //velocity calculations initialization
    if (iInfo.velAvail < coeffCount) {
      for (i <- iInfo.velAvail until coeffCount)
        iInfo.velCoeff(i) =
          iInfo.twot *
            iInfo.velCoeff(i - 1) + iInfo.posnCoeff(i - 1) +
            iInfo.posnCoeff(i - 1) - iInfo.velCoeff(i - 2)
      iInfo.velAvail = coeffCount
    }

    val vfac = (dna + dna) / stepInfo(1)
    for (i <- 0 until physicalUnitCount) {

      val coeffIndex = coeffCount * (i + l * physicalUnitCount + 1)
      val velCoeffIndex = coeffCount
      var acc = 0d
      for (i <- 0 until coeffCount)
        acc += iInfo.velCoeff(velCoeffIndex - i - 1) * coeffSeq(coeffIndex - i - 1)
      posAndVel(posAndVelIndex) = acc * vfac
      posAndVelIndex += 1
    }

    //acceleration is wanted: the second derivative of the Chebyshev polynomials
    if (interpolationRequestType == 3) {
      val accelCoeffs = ArrayBuffer.fill[Double](ChebyshevInterpolation.MAX_CHEBYSHEV_COEFFICIENT_COUNT)(0d)
      for (i <- 2 until coeffCount)
        accelCoeffs(i) = 4.0 * iInfo.velCoeff(i - 1) + iInfo.twot * accelCoeffs(i - 1) - accelCoeffs(i - 2)

      for (i <- 0 until physicalUnitCount) {
        val coeffIndex = coeffCount * (i + l * physicalUnitCount + 1)
        val accCoeffIndex = coeffCount
        var acc = 0d
        for (i <- 0 until coeffCount)
          acc += accelCoeffs(accCoeffIndex - i - 1) * coeffSeq(coeffIndex - i - 1)
        posAndVel(posAndVelIndex) = acc * vfac * vfac
        posAndVelIndex += 1
      }
    }
    posAndVel.toArray
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file JplEphemeris.scala
//=============================================================================