/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Aug/2024
 * Time:  23h:29m
 * Description: None
 */
package com.common.jpl.ephemeris
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object ChebyshevInterpolation {
  //---------------------------------------------------------------------------
  final val MAX_CHEBYSHEV_COEFFICIENT_COUNT = 18
  //---------------------------------------------------------------------------
  final val CHEBYSHEV_INTEGRATION_RESULT_COUNT = 9 // 3 for location, 3 for velocity and 3 for acceleration
  //---------------------------------------------------------------------------
  def build(): ChebyshevInterpolation = {
    val info = ChebyshevInterpolation()
    info.posnCoeff(0) = 1
    info.posnCoeff(1) = -2
    info.velCoeff(0) = 0
    info.velCoeff(1) = 1
    info
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class ChebyshevInterpolation(posnCoeff: ArrayBuffer[Double] = ArrayBuffer.fill[Double](ChebyshevInterpolation.MAX_CHEBYSHEV_COEFFICIENT_COUNT)(0d)
                                  , velCoeff: ArrayBuffer[Double] = ArrayBuffer.fill[Double](ChebyshevInterpolation.MAX_CHEBYSHEV_COEFFICIENT_COUNT)(0d)
                                  , var twot: Double = Double.NaN
                                  , var posnAvail: Int = -1
                                  , var velAvail: Int = -1
                                  )
//=============================================================================
//End of file ChebyshevInterpolation.scala
//=============================================================================