/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  05/Aug/2024 
 * Time:  23h:24m
 * Description: None
 */
package com.common.jpl.ephemeris
//=============================================================================
//=============================================================================
trait  JplEphemerisItemData {
  //---------------------------------------------------------------------------
  val startLocation: Int     //starting location in each data record of the chebychev coefficients belonging to the ith item
  val coeffCount: Int        //number of chebychev coefficients per component of the ith item
  val setCount: Int          //is the number of complete sets of coefficients in each data record for the ith item
  val physicalUnitCount: Int //number of physical units stored
  //---------------------------------------------------------------------------
  def getKernelByteSize = 2 * coeffCount * setCount * physicalUnitCount
  //---------------------------------------------------------------------------
}
//=============================================================================
case class JplEphemerisItemDataPlanet(startLocation: Int
                                         , coeffCount: Int
                                         , setCount: Int) extends  JplEphemerisItemData {val physicalUnitCount = 3} //cartesian components (x, y, z) in kilometers
//=============================================================================
case class  JplEphemerisItemDataNutation(startLocation: Int
                                         , coeffCount: Int
                                         , setCount: Int) extends  JplEphemerisItemData {val physicalUnitCount = 2} //d(psi) and d(epsilon) in radians
//=============================================================================
case class  JplEphemerisItemDataLibration(startLocation: Int
                                          , coeffCount: Int
                                          , setCount: Int) extends  JplEphemerisItemData {val physicalUnitCount = 3} //phi, theta, psi
//=============================================================================
case class  JplEphemerisItemDataMantle(startLocation: Int
                                       , coeffCount: Int
                                       , setCount: Int) extends  JplEphemerisItemData {val physicalUnitCount = 3} //omega_x,omega_y,omega_z radians/day
//=============================================================================
case class  JplEphemerisItemDataTime(startLocation: Int
                                     , coeffCount: Int
                                     , setCount: Int) extends  JplEphemerisItemData {val physicalUnitCount = 1} //in seconds
//=============================================================================
object JplEphemerisItem {
  //-----------------------------------------------------------------------
  final val EPHEMERIS_PLANET_ITEM_COUNT = 11
  final val EPHEMERIS_TOTAL_ITEM_COUNT = EPHEMERIS_PLANET_ITEM_COUNT + 4 //nutation,lunar libration, lunar mantle,ttTDT
  //-----------------------------------------------------------------------
  //Sun, moon and planets
  final val MERCURY_EPHEMERIS_HEADER_ITEM_INDEX              = 0
  final val VENUS_EPHEMERIS_HEADER_ITEM_INDEX                = 1
  final val EARTH_MOO_BARYCENTER_EPHEMERIS_HEADER_ITEM_INDEX = 2
  final val MARS_EPHEMERIS_HEADER_ITEM_INDEX                 = 3
  final val JUPITER_EPHEMERIS_HEADER_ITEM_INDEX              = 4
  final val SATURN_EPHEMERIS_HEADER_ITEM_INDEX               = 5
  final val URANUS_EPHEMERIS_HEADER_ITEM_INDEX               = 6
  final val NEPTUNE_EPHEMERIS_HEADER_ITEM_INDEX              = 7
  final val PLUTO_EPHEMERIS_HEADER_ITEM_INDEX                = 8
  final val MOON_EPHEMERIS_HEADER_ITEM_INDEX                 = 9
  final val SUN_EPHEMERIS_HEADER_ITEM_INDEX                  = 10

  //other
  final val NUTATION_EPHEMERIS_HEADER_ITEM_INDEX             = 11
  final val LUNAR_LIBRATION_EPHEMERIS_HEADER_ITEM_INDEX      = 12
  final val LUNAR_MANTLE_EPHEMERIS_HEADER_ITEM_INDEX         = 13
  final val TT_TDT_EPHEMERIS_HEADER_ITEM_INDEX               = 14
}
//-------------------------------------------------------------------------
import JplEphemerisItem._
case class JplEphemerisItem(mercury:                   JplEphemerisItemDataPlanet
                               , venus:                JplEphemerisItemDataPlanet
                               , earthMoonBarycenter:  JplEphemerisItemDataPlanet
                               , mars:                 JplEphemerisItemDataPlanet
                               , jupiter:              JplEphemerisItemDataPlanet
                               , saturn:               JplEphemerisItemDataPlanet
                               , uranus:               JplEphemerisItemDataPlanet
                               , neptune:              JplEphemerisItemDataPlanet
                               , pluto:                JplEphemerisItemDataPlanet
                               , moon:                 JplEphemerisItemDataPlanet
                               , sun:                  JplEphemerisItemDataPlanet
                               , nutation:             JplEphemerisItemDataNutation
                               , lunarLibration:       JplEphemerisItemDataLibration
                               , lunarMantle:          JplEphemerisItemDataMantle
                               , ttTDT:                JplEphemerisItemDataTime
                              ) {
  //-----------------------------------------------------------------------
  def getKernelByteSize =
    mercury.getKernelByteSize +
      venus.getKernelByteSize +
      earthMoonBarycenter.getKernelByteSize +
      mars.getKernelByteSize +
      jupiter.getKernelByteSize +
      saturn.getKernelByteSize +
      uranus.getKernelByteSize +
      neptune.getKernelByteSize +
      pluto.getKernelByteSize +
      moon.getKernelByteSize +
      sun.getKernelByteSize +
      nutation.getKernelByteSize +
      lunarLibration.getKernelByteSize +
      lunarMantle.getKernelByteSize +
      ttTDT.getKernelByteSize

  //-----------------------------------------------------------------------
  def getPhysicalUnitCount(pos: Int) =
    pos match {
      ///Sun, moon and planets
      case MERCURY_EPHEMERIS_HEADER_ITEM_INDEX              => mercury.physicalUnitCount
      case VENUS_EPHEMERIS_HEADER_ITEM_INDEX                => venus.physicalUnitCount
      case EARTH_MOO_BARYCENTER_EPHEMERIS_HEADER_ITEM_INDEX => earthMoonBarycenter.physicalUnitCount
      case MARS_EPHEMERIS_HEADER_ITEM_INDEX                 => mars.physicalUnitCount
      case JUPITER_EPHEMERIS_HEADER_ITEM_INDEX              => jupiter.physicalUnitCount
      case SATURN_EPHEMERIS_HEADER_ITEM_INDEX               => saturn.physicalUnitCount
      case URANUS_EPHEMERIS_HEADER_ITEM_INDEX               => uranus.physicalUnitCount
      case NEPTUNE_EPHEMERIS_HEADER_ITEM_INDEX              => neptune.physicalUnitCount
      case PLUTO_EPHEMERIS_HEADER_ITEM_INDEX                => pluto.physicalUnitCount
      case MOON_EPHEMERIS_HEADER_ITEM_INDEX                 => moon.physicalUnitCount
      case SUN_EPHEMERIS_HEADER_ITEM_INDEX                  => sun.physicalUnitCount

      //other
      case NUTATION_EPHEMERIS_HEADER_ITEM_INDEX             => nutation.physicalUnitCount
      case LUNAR_LIBRATION_EPHEMERIS_HEADER_ITEM_INDEX      => lunarLibration.physicalUnitCount
      case LUNAR_MANTLE_EPHEMERIS_HEADER_ITEM_INDEX         => lunarMantle.physicalUnitCount
      case TT_TDT_EPHEMERIS_HEADER_ITEM_INDEX               => ttTDT.physicalUnitCount
    }
  //-----------------------------------------------------------------------
}
//=============================================================================
//End of file JplEphemerisItem.scala
//=============================================================================