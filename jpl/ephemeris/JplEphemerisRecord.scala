/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Aug/2024
 * Time:  11h:32m
 * Description: None
 */
package com.common.jpl.ephemeris
//=============================================================================
import com.common.hardware.cpu.CPU
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
import com.common.jpl.ephemeris.JplEphemeris.{getDoubleSeq, getFirstNotEmptyLine, getTripletSeq}
import com.common.util.path.Path
//=============================================================================
import java.io.{BufferedReader, File, FileReader}
import scala.language.existentials
import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Try, Success}
import java.util.concurrent.ConcurrentHashMap
//=============================================================================
//=============================================================================
object JplEphemerisRecord extends MyLogger {
  //---------------------------------------------------------------------------
  def apply(dataSeq:Array[Double]): JplEphemerisRecord =
    JplEphemerisRecord(dataSeq.head
                      , dataSeq(1)
                      , dataSeq.drop(2))
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[JplEphemeris].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }

  //---------------------------------------------------------------------------
  def getColNameValueSeq(prefix: String = "") =
    classOf[JplEphemeris].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = (prefix + f.getName, f.getType)
      f.setAccessible(false)
      res
    }
  //-------------------------------------------------------------------------
  private def getDataCount(file: BufferedReader): Option[Int] = {
    val tokenSeq = getFirstNotEmptyLine(file).getOrElse(return None)
      .trim
      .replaceAll(" +", " ")
      .split(" ")

    if (tokenSeq.size != 2) None
    else Some(tokenSeq(1).toInt)
  }
  //---------------------------------------------------------------------------
  def load(fileName: String): Option[Array[JplEphemerisRecord]] = {
    Try {
      val file = new BufferedReader(new FileReader(new File(fileName)))
      var continue = true
      val recordSeq = ArrayBuffer[JplEphemerisRecord]()
      while (continue) {
        val dataCount = getDataCount(file)
        if (dataCount.isEmpty) continue = false
        else {
          //get the complete triplets
          val tripletCount = dataCount.get / 3
          val tripletSeq = getTripletSeq(file, tripletCount).getOrElse {
            file.close(); return None
          }

          //get the remain data
          val remainDataCount = dataCount.get % 3
          val remainDataSeq =
            if (remainDataCount > 0)
              getDoubleSeq(file, remainDataCount).getOrElse {
                file.close(); return None
              }
            else Array[Double]()

          //add to storage
          recordSeq += JplEphemerisRecord(tripletSeq.flatten ++ remainDataSeq)
        }
      }
      file.close()
      return Some(recordSeq.toArray)
    }
    match {
      case Failure(_: Exception) =>
        error(s"Error reading the JPL ephemeris data:'$fileName'")
        None
    }
  }

  //-------------------------------------------------------------------------
  class LoadEphemerisRecordSeq(fileNameSeq: List[String]
                               , storage: ConcurrentHashMap[Int, Array[JplEphemerisRecord]])
    extends ParallelTask[String](
      fileNameSeq
      , CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 100
      , message = Some("loading JPL ephemeris")) {
    //-----------------------------------------------------------------------
    def userProcessSingleItem(path: String): Unit = {
      Try {
        val r = load(path)
        if (r.isDefined) {
          val filenameIndex = Path.getOnlyFilenameNoExtension(path).drop(4).toInt
          storage.put(filenameIndex, r.get)
        }
      }
      match {
        case Success(_) =>
        case Failure(ex) => error(s"Error processing file:'${Path.getOnlyFilename(path)}'" + ex.toString)
      }
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class JplEphemerisRecord(jdStart: Double // Julian date of earliest data in record.
                              , jdEnd: Double //Julian date of latest data in record.
                              , chebyCoeffSeq: Array[Double] //Chebyshev polynomial coefficients
                            ) {
  //---------------------------------------------------------------------------
  def isIn(jd: Double) =
    jd >= jdStart &&
    jd < jdEnd
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file JplEphemerisRecord.scala
//=============================================================================