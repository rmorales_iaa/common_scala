/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/May/2021
 * Time:  12h:41m
 * Description: JNI of SPICE library
 * Based on: https://naif.jpl.nasa.gov/pub/naif/misc/JNISpice/
 */
//=============================================================================
package com.common.jpl
//=============================================================================
import com.common.configuration.MyConf
import com.common.constant.astronomy.AstronomicalUnit.ASTRONOMICAL_UNIT_KM
import com.common.coordinate.spherical.ObservedApparentPosition
import com.common.jpl.web.JplWebQuery
import com.common.util.file.MyFile
import com.common.util.native.MyNativeLibrary
import com.common.util.path.Path
import com.common.util.time.Time
import spice.basic.CSPICE._
//=============================================================================
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import scala.util.{Failure, Success, Try}
import java.util.concurrent.ConcurrentHashMap
import scala.collection.JavaConverters.mapAsScalaConcurrentMapConverter
//=============================================================================
//=============================================================================
object Spice extends MyNativeLibrary {
  //-------------------------------------------------------------------------
  private final val COMMON_META_KERNEL  = MyConf.c.getString("JplSpice.commonMetaKernel")
  //-------------------------------------------------------------------------
  private final val USE_JPL_WEB_QUERY  = MyConf.c.getIntWithDefaultValue("Astrometry.fitWcs.useJPL_WebQuery",0) == 1
  //-------------------------------------------------------------------------
  //https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/C/cspice/spkezr_c.html
  final val ABERRATION_NONE            = "NONE"
  final val ABERRATION_CORRECTION_LT   = "LT"
  final val ABERRATION_CORRECTION_LT_S = "LT+S"
  final val ABERRATION_CORRECTION_CN   = "CN"
  final val ABERRATION_CORRECTION_CN_S = "CN+S"

  final val ABERRATION_CORRECTION_XLT   = "XLT"
  final val ABERRATION_CORRECTION_XLT_S = "XLT+S"
  final val ABERRATION_CORRECTION_XCN   = "XCN"
  final val ABERRATION_CORRECTION_XCN_S = "XCN+S"
  //-------------------------------------------------------------------------
  final val REFERENCE_FRAME_J2000 = "J2000"
  //-------------------------------------------------------------------------
  private final val ILLUMINATION_SUN = "sun"
  //-------------------------------------------------------------------------
  private final val SUN_CENTER_OBSERVER_SPK_ID_CODE       = "10"
  private final val EARTH_CENTER_OBSERVER_SPK_ID_CODE     = "500"
  //-------------------------------------------------------------------------
  //https://www.cosmos.esa.int/web/spice/
  //http://spiftp.esac.esa.int/data/SPICE/Gaia/kernels/spk/
  final val GAIA_CENTER_OBSERVER_SPK_ID_CODE      = "-139479"  //500@gaia
  final val GAIA_CENTER_OBSERVER_ESA_SPK_ID_CODE  = "-123"     //use this identifier when using spk geberated by ESA
  //-------------------------------------------------------------------------
  private final val MAX_KERNEL_LOAD_LIMIT  = 4990   //https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/C/req/kernel.html#Load%20Limits
  //-------------------------------------------------------------------------
  val defaultEphemeridesTimeFormatterSize = 19
  val ephemerisTimeFormatter           = DateTimeFormatter.ofPattern("yyyy MMM dd HH:mm:ss")
  val ephemerisTimeFormatterWithMillis = DateTimeFormatter.ofPattern("yyyy MMM dd HH:mm:ss.SSS")
  //-------------------------------------------------------------------------
  private var initialized = false
  //-------------------------------------------------------------------------
  private val loadedKernelMap = new ConcurrentHashMap[String, Boolean]().asScala
  //-------------------------------------------------------------------------
  case class SpiceSourceInfo(timeStampMidPoint: LocalDateTime
                             , ra: Double
                             , dec: Double
                             , lightTime: Double
                             , phaseAngle: Double
                             , julianDateMidPointCorrected: Double
                             , heliocentricRange: Double
                             , observerRange: Double
                             , observedPos: ObservedApparentPosition)
  //-------------------------------------------------------------------------
  loadLibrary
  //-------------------------------------------------------------------------
  private def loadLibrary =
    load( "AbsolutePath.nativeLibrary.path"
      , "AbsolutePath.jplSpice.library")
  //-------------------------------------------------------------------------
  def loadKernelDir(dir: String, callback: () => Unit) = {
    //https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/C/req/kernel.html#Load%20Limits
    val kernelNameSeq = Path.getSortedFileList(dir).map {_.getAbsolutePath}
    kernelNameSeq.grouped(MAX_KERNEL_LOAD_LIMIT).foreach { seq=>
      if (loadKernelSeq(seq)) {
        callback()
        unloadKernelSeq(seq)
      }
    }
  }
  //-------------------------------------------------------------------------
  def loadKernelSeq(kernelNameSeq: List[String], isSingleKernel: Boolean = true, verbose: Boolean = false) =
    kernelNameSeq.forall { loadKernel(_, isSingleKernel,verbose) }
  //-------------------------------------------------------------------------
  def unloadKernelSeq(kernelNameSeq: List[String], isSingleKernel: Boolean = true, verbose: Boolean = false) =
    kernelNameSeq.foreach { unloadKernel(_, isSingleKernel,verbose)}
  //-------------------------------------------------------------------------
  def loadKernel(name: String
                 , isSingleKernel: Boolean = true
                 , verbose: Boolean = false): Boolean = {

    synchronized {
      if (loadedKernelMap.contains(name)) return true //already loaded
      if (!MyFile.fileExist(name)) {
        error(s"Kernel file:'$name' does not exist")
        return false
      }
      val beforeLoad = ktotal("ALL")
      if (verbose) info(s"Loading Spice kernel '$name'")
      furnsh(name)
      val afterLoad = ktotal("ALL")
      if (isSingleKernel) {
        if ((beforeLoad + 1) != afterLoad) {
          error(s"Error loading Spice kernel '$name'")
          return false
        }
        else
          if (verbose) info(s"JPL Spice '$name' loaded successfully")
      }
      else { //multiple kernel
        if (beforeLoad >= afterLoad) {
          error(s"Error loading Spice kernel '$name'")
          return false
        }
        else
          if (verbose) info(s"Spice kernel '$name' loaded successfully")
      }
      loadedKernelMap(name) = true
    }
    true
  }

  //-------------------------------------------------------------------------
  def unloadKernel(name: String, isSingleKernel: Boolean = true, verbose: Boolean = false): Unit = {
    synchronized {
      if (!loadedKernelMap.contains(name)) return //not loaded or already uloaded
      val beforeUnLoad = ktotal("ALL")
      unload(name)
      val afterUnLoad = ktotal("ALL")
      if (isSingleKernel)
        if ((beforeUnLoad - 1) != afterUnLoad) error(s"Error unloading Spice kernel '$name'")
        else if (verbose) info(s"JPL Spice '$name' unloaded successfully")
        else {
          //multiple kernel
          if (beforeUnLoad <= afterUnLoad) error(s"Error unloading Spice kernel '$name'")
          else if (verbose) info(s"Spice kernel '$name' unloaded successfully")
        }
      loadedKernelMap.remove(name)
    }
  }
  //-------------------------------------------------------------------------
  def printVersion = info( s"Loading JPL toolkit version: '${tkvrsn("TOOLKIT")}'")
  //-------------------------------------------------------------------------
  def init(verbose: Boolean = true) = {
    if (initialized && verbose) warning("Spice library is already initialized")
    else {
      if (verbose) info( s"Loading JPL toolkit version: '${tkvrsn("TOOLKIT")}'")
      if (loadKernel(COMMON_META_KERNEL, isSingleKernel = false, verbose))
        initialized = true
    }
  }
  //-------------------------------------------------------------------------
  def close(verbose: Boolean = true) = {
    if(initialized) {
      loadedKernelMap.keys.foreach{name=> unloadKernel(name, name != COMMON_META_KERNEL, verbose)}
      if (verbose) info( s"Unloading JPL toolkit version: '${tkvrsn("TOOLKIT")}'")
      initialized = false
    }
  }
  //---------------------------------------------------------------------------
  def getSpkId(spkPath: String) =
    Path.getOnlyFilename(spkPath).split("_").head.toInt
  //-------------------------------------------------------------------------
  def getEphemerisAndPhaseAngle(targetSpkID: String
                                , observerSpkID: String
                                , timeStamp: String
                                , referenceFrame: String = REFERENCE_FRAME_J2000
                                , aberrationCorrection: String = ABERRATION_CORRECTION_LT
                                , calculatePhaseAngle: Boolean = true
                                , useEarthAsObserverWhenError: Boolean = true
                                , verbose: Boolean = true): (Double,Double,Double,Double) = {
    if(!Spice.initialized) {
      error("Spice in not initialized")
      (0d,0d,0d,0d)
    }
    else {
      val et = str2et(timeStamp)
      var r = (-1d, -1d, -1d, -1d)
      val state = Array.fill[Double](6)(0)
      val lightTime = Array.fill[Double](1)(0)

      Try {
        spkezr(targetSpkID
               , et
               , referenceFrame
               , aberrationCorrection
               , observerSpkID
               , state
               , lightTime)
      }
      match {
        case Success(_) =>
          val raDec = recrad(state)
          r = (Math.toDegrees(raDec(1))
            , Math.toDegrees(raDec(2))
            , lightTime.head / 60
            , if (calculatePhaseAngle) getApparentPhaseAngle(targetSpkID, observerSpkID, et) else -1d)

        case Failure(ex) =>
          if(verbose) {
            error(s"Error querying spice for a ephemeris " +
              s"\nObserverSpkID       : '$observerSpkID'" +
              s"\nTargetSpkID         : '$targetSpkID'" +
              s"\nTimestamp           : '$timeStamp'  " +
              s"\nReferenceFrame      : '$referenceFrame'" +
              s"\nAberrationCorrection: '$aberrationCorrection'"
            )
            error(ex.toString)
          }
      }
      r
    }
  }
  //---------------------------------------------------------------------------
  //S-T-O (Sun - Target - Object)
  private def getApparentPhaseAngle(targetSpkID: String, observer: String, timeStamp: Double) =
    Math.toDegrees(phaseq(timeStamp
      , targetSpkID
      , ILLUMINATION_SUN
      , observer
      , ABERRATION_CORRECTION_LT))
  //---------------------------------------------------------------------------
  def getTimeAtTarget(targetSpkID: String
                      , observerSpkID: String
                      , timeStampAtObserver: String
                      , referenceFrame: String =  REFERENCE_FRAME_J2000
                      , aberrationCorrection: String =  ABERRATION_CORRECTION_LT) = {
    val et = str2et(timeStampAtObserver)
    val state = Array.fill[Double](6)(0)
    val lightTime = Array.fill[Double](1)(0)
    spkezr(targetSpkID     //target
      , et
      , referenceFrame
      , aberrationCorrection
      , observerSpkID  //observer
      , state
      , lightTime)

    val timeStampMonthFixed = timeStampAtObserver.zipWithIndex.map { case (c,i)=>
      i match {
        case 6 | 7 => c.toLower
        case _ => c
      }
    }.mkString
    val t = LocalDateTime.parse(timeStampMonthFixed, ephemerisTimeFormatterWithMillis)
    val newTime = t.minusNanos(Time.secondsToNanos(lightTime.head))
    newTime.format(ephemerisTimeFormatterWithMillis)
  }
  //-------------------------------------------------------------------------
  def getHeliocentricRange(targetSpkID: String
                           , observerSpkID: String
                           , timeStampMidPoint: LocalDateTime
                           , referenceFrame: String =  REFERENCE_FRAME_J2000
                           , aberrationCorrection: String =  ABERRATION_CORRECTION_LT) = {

    val et = str2et(timeStampMidPoint.format(Spice.ephemerisTimeFormatterWithMillis))

    val sunApparentPositionFromObserver = Array.fill[Double](3)(0)
    val sunLightTimeFromObserver = Array.fill[Double](1)(0)
    spkpos(SUN_CENTER_OBSERVER_SPK_ID_CODE
      , et
      , referenceFrame
      , aberrationCorrection
      , observerSpkID
      , sunApparentPositionFromObserver
      , sunLightTimeFromObserver)

    val targetApparentPositionFromObserver = Array.fill[Double](3)(0)
    val targetLightTimeFromObserver = Array.fill[Double](1)(0)
    spkpos(targetSpkID
      , et
      , referenceFrame
      , aberrationCorrection
      , observerSpkID
      , targetApparentPositionFromObserver
      , targetLightTimeFromObserver)

    //calculate the distance between targetApparentPositionFromObserver and  sunApparentPositionFromObserver
    val x = targetApparentPositionFromObserver(0) - sunApparentPositionFromObserver(0)
    val y = targetApparentPositionFromObserver(1) - sunApparentPositionFromObserver(1)
    val z = targetApparentPositionFromObserver(2) - sunApparentPositionFromObserver(2)

    val max = Math.max(Math.max(Math.abs(x),Math.abs(y)), Math.abs(z))
    val dx = x/max
    val dy = y/max
    val dz = z/max
    val dist = max *  Math.sqrt( (dx * dx) + (dy * dy) + (dz * dz) )

    dist/ASTRONOMICAL_UNIT_KM
  }
  //---------------------------------------------------------------------------
   def getEphemerisTime(t: LocalDateTime) = {
     str2et(t.format(Spice.ephemerisTimeFormatterWithMillis))
   }

  //---------------------------------------------------------------------------
  def getJulianDate(t: LocalDateTime) =
    unitim(str2et(t.format(Spice.ephemerisTimeFormatterWithMillis)), "ET", "JED")
  //---------------------------------------------------------------------------
  def getPosByJplWebQuery(objectName: String
                          , normalizedName: String
                          , orbit: String
                          , observatoryCode: String
                          , timeSeq: String)  =
    JplWebQuery.getPos(
      objectName
      , normalizedName
      , orbit
      , observatoryCode
      , Array(timeSeq)
    ).head
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file JPL_Spice.scala
//=============================================================================
