//=============================================================================
//File: VisualXML.scala
//=============================================================================
/** It implements XML management
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.xml

//=============================================================================
// System import sectionName
//=============================================================================
import javax.xml.transform.stream.StreamSource
import javax.xml.validation.SchemaFactory
import org.xml.sax.SAXException
import scala.xml.XML
//=============================================================================
// User import sectionName
//=============================================================================
//=============================================================================
// Class/Object implementation
//=============================================================================

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
//http://www.utilities-online.info/xsdvalidation/
//http://sean8223.blogspot.com.es/2009/09/xsd-validation-in-scala.html
//http://alvinalexander.com/scala/xml-parsing-xpath-extract-xml-tag-attributes
//http://alvinalexander.com/scala/scala-xml-searching-xmlns-namespaces-xpath-parsing
object VisualXML {
   val factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema") 
}

//=============================================================================
//=============================================================================

case class VisualXML(xmlFileName: String, xsdFileName: String = "") extends MyLogger {

  //-------------------------------------------------------------------------
  // Variable declaration
  //-------------------------------------------------------------------------
  var valid = false
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
  if (!fileExist(xmlFileName)) error(s"File does not exist. XML file '$xmlFileName'")
  else{       
    if (validate){
      logger.info(s"Loaded and validated XML file '$xmlFileName'")
      valid = true
    }   
  }
  private val rootNode = XML.loadFile(xmlFileName)
  //-------------------------------------------------------------------------
  def validate : Boolean = {
    try {
      val schema = factory.newSchema(new StreamSource(xsdFileName))
      val validator = schema.newValidator()
      validator.validate(new StreamSource(xmlFileName))      
      true
    } 
    catch {
      case ex: SAXException => exception(ex, s"Error validating XML file '$xmlFileName'")
      case ex: Exception => exception(ex, s"Error validating XML file '$xmlFileName'")
      false
    }
  }
  //-------------------------------------------------------------------------    
  def isValid = valid
  //-------------------------------------------------------------------------
  def root = rootNode
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //End of case class 'VisualXML'

//=============================================================================
// End of 'VisualXML.scala' file
//=============================================================================
