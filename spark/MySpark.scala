/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  08/Aug/2018
  * Time:  17h:53m
  * Description: None
  */
//=============================================================================
package com.common.spark
//=============================================================================
import com.common.hadoop.MyHadoop
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.{SQLContext, SparkSession}
import org.apache.spark.{SparkConf, SparkContext}
//=============================================================================
import com.common.configuration.MyConf
import com.common.logger.MyLogger
import com.common.util.path.Path._
//=============================================================================
//=============================================================================
case class MySpark(sparkApplicationName: String
                   , conf: MyConf) extends MyLogger {
  //---------------------------------------------------------------------------
  //use Spark and hadoop
  private var useSpark = false
  private var hadoopUse = false

  //Spark configuration
  private val sparkConf: SparkConf = new SparkConf
  private var master : String = ""

  //hadoop
  private var hadoopConf : Configuration = _
  private var core_xml : String = _
  private var hdfs_xml : String = _

  /** True if initialization has finished */
  private var initializatedDone = false

  /** Temporal directory */
  private var tmpDirectory = ""
  //---------------------------------------------------------------------------
  val (sparkSession,sparkContext,sqlContext,myHadoop) = initialize()
  //---------------------------------------------------------------------------
  def getUseSpark = useSpark
  //---------------------------------------------------------------------------
  def getUseHadoop = hadoopUse
  //---------------------------------------------------------------------------
  def getSession = sparkSession
  //---------------------------------------------------------------------------
  def getContext = sparkContext
  //---------------------------------------------------------------------------
  def getSqlContext = sqlContext
  //---------------------------------------------------------------------------
  def getMyHadoop = myHadoop.get
  //---------------------------------------------------------------------------
  def isHadoopInitialized = myHadoop.isDefined
  //---------------------------------------------------------------------------
  private def initialize(): (SparkSession,SparkContext,SQLContext,Option[MyHadoop]) = {

    useSpark = conf.getBoolean("Spark.use")
    hadoopUse= conf.getBoolean("Hadoop.use")

    if (!useSpark) {info("Not using Spark");return (null,null,null,None)}

    if (initializatedDone) {info("Spark context already created");return (sparkSession,null,null,None)}

    //fresh tmp directory
    tmpDirectory =  conf.getString("Spark.temporalDirectory")
    deleteDirectoryIfExist( tmpDirectory )
    ensureDirectoryExist( tmpDirectory )

    //configure computation cluster
    val (_sparkSession,_sparkContext,_sqlContext) = initSpark(sparkApplicationName).getOrElse(return (null,null,null,None))

    //final tasks in spark
    initializatedDone = true

    //hadoop initialization
    val _myHadoop = initHadoop(_sqlContext)

    (_sparkSession,_sparkContext,_sqlContext, _myHadoop)
  }
  //---------------------------------------------------------------------------
  def isInitialized: Boolean = initializatedDone
  //---------------------------------------------------------------------------
  def close(printLog: Boolean = false) {
    if (initializatedDone) {
      sparkSession.stop()
      if(printLog) info("Spark context closed")
      deleteDirectoryIfExist( tmpDirectory )
      if (myHadoop.isDefined) myHadoop.get.close()
    }
  }
  //-------------------------------------------------------------------------
  private def initHadoop(_sqlContext: SQLContext) : Option[MyHadoop] = {
    info("Initializing hadoop: ")

    //load hadoop libraries
    if (conf.getBoolean("Hadoop.use")) {

      //hadoop
      hadoopConf = new Configuration(_sqlContext.sparkContext.hadoopConfiguration)
      core_xml = conf.getString("Hadoop.core")
      hdfs_xml = conf.getString("Hadoop.hdfs")

      info(s"  hadoop core xml: $core_xml")
      info(s"  hadoop hdfs xml: $hdfs_xml")

      hadoopConf.addResource(new Path(core_xml))
      hadoopConf.addResource(new Path(hdfs_xml))

      val _myHadoop = MyHadoop(FileSystem.get(hadoopConf), hadoopConf)

      info(s"  hadoop min block byte size: ${_myHadoop.minBlockByteSize}")
      info("Hadoop initialized")
      Some(_myHadoop)
    }
    else {
      info("Not using hadoop")
      None
    }
  }
  //-------------------------------------------------------------------------
  /** Configures the remote computation cluster according with configuration file  */
  private def loadRemoteConfiguration : Boolean = {

    info("REMOTE SPARK run:")

    val corePerExecutor = conf.getString("Spark.computationCluster.remote.executor.corePerExecutor")
    val memPerExecutor  = conf.getString("Spark.computationCluster.remote.executor.memoryPerExecutor")

    val master          = conf.getString("Spark.computationCluster.remote.master")
    val masterIP        = conf.getString("Spark.computationCluster.remote.masterIP")
    val deployMode      = conf.getString("Spark.computationCluster.remote.deployMode")
    val driverMemory    = conf.getString("Spark.computationCluster.remote.driverMemory")
    val connectionAckWaitTimeoutInSeconds = conf.getString("Spark.computationCluster.remote.connectionAckWaitTimeoutInSeconds")

    sparkConf
      .set("spark.driver.memory",      driverMemory)

      .set("spark.executor.cores",        corePerExecutor)
      .set("spark.executor.memory",       memPerExecutor)

      .set("spark.local.path",         tmpDirectory)
      .set("spark.submit.deployMode",  deployMode)
      .set("spark.driver.bindAddress", masterIP)
      .set("spark.driver.host",        masterIP)
      .set("spark.core.connection.ack.wait.timeout", connectionAckWaitTimeoutInSeconds)

      .setMaster(master)

    info(s"Running in REMOTE CLUSTER master:'$master' corePerExecutor:$corePerExecutor memPerExecutor:$memPerExecutor deploy mode:'$deployMode' temporal directory:'$tmpDirectory'")
  }

  //-------------------------------------------------------------------------
  /** Configures the local computation cluster according with configuration file  */
  private def localLocalConfiguration : Boolean = {
    info("LOCAL SPARK run:")

    master = "local[" + conf.getString("Spark.computationCluster.local.totalCore") + "]"

    val driverMemory   = conf.getString("Spark.computationCluster.local.driverMemory")

    val corePerExecutor = conf.getString("Spark.computationCluster.local.executor.corePerExecutor")
    val memPerExecutor  = conf.getString("Spark.computationCluster.local.executor.memoryPerExecutor")

    sparkConf
      .set("spark.driver.memory",         driverMemory)

      .set("spark.executor.cores" ,       corePerExecutor)
      .set("spark.executor.memory",       memPerExecutor)

      .set("spark.local.path",            tmpDirectory)
      .setMaster(master)

    info("Running in LOCAL CLUSTER: " + master)
  }

  //-------------------------------------------------------------------------
  /** Configures the computation cluster (local or remote) according with configuration file
  @return true if cluster has been properly configured
            or false if cluster has not been properly configured
    //https://github.com/jaceklaskowski/mastering-apache-spark-book/blob/master/spark-submit.adoc
    */
  private def initSpark(sparkNameApplication: String)
      : Option[(SparkSession,SparkContext,SQLContext)] = {

    try {
      if (!loadConfiguration) return None //configure the parameters according to the configuration file

      //common configuration
      sparkConf.set("spark.app.name",                     sparkNameApplication)

      //https://sparkbyexamples.com/spark/spark-kryoserializer-buffer-max/
      sparkConf.set("spark.serializer",                   "org.apache.spark.serializer.KryoSerializer")
      sparkConf.set("spark.kryoserializer.buffer",        conf.getString("Spark.kryoserializerBuffer"))
      sparkConf.set("spark.kryoserializer.buffer.max",    conf.getString("Spark.kryoserializerBufferMax"))

      sparkConf.set("spark.driver.maxResultSize",         conf.getString("Spark.driver.resultSize"))
      sparkConf.set("spark.local.dir",                    tmpDirectory)

      //create the Spark session
      val _sparkSession = SparkSession
        .builder()
        .appName(sparkNameApplication)
        .config(sparkConf)
        .getOrCreate()

      //set the Spark context
      val _sparkContext = _sparkSession.sparkContext


      //set the sql context
      val _sqlContext = _sparkSession.sqlContext

      info("Spark context created")
      info(s"Spark version: ${_sparkContext.version}")
      info("Spark configuration: ")
      sparkConf.getAll.foreach( s=> info( "  " + s._1 + ":" + s._2 ))

      Some((_sparkSession,_sparkContext,_sqlContext))
    }
    catch {
      case e: Exception =>
      exception(e, "Error initializing Spark context")
      None
    }
  }
  //-------------------------------------------------------------------------
  private def loadConfiguration : Boolean = {
    try {

      //local remote com.common.hadoop
      val runInRemote = conf.getBoolean("Spark.computationCluster.runInRemote")
      if (runInRemote) loadRemoteConfiguration
      else localLocalConfiguration
    }
    catch {
      case e: Exception => exception(e, "Error initializaiting Spark context")
    }
  }
  //-------------------------------------------------------------------------
  def getDatasetFromDir(inputDir: String
                        , fileExtension: List[String]
                        , limitDatasetToCount: Option[Int] = None
                       ) = {
    import sparkSession.implicits._ // Import implicits for Spark SQL
    val fileNameSeq = myHadoop.get.getDirContentNoRecursive(inputDir, fileExtension)
    val defaultServiceName = myHadoop.get.defaultServiceName
    val hdfsFleNameSeq = fileNameSeq map { fileName=>
      s"$defaultServiceName$fileName"
    }
    if (limitDatasetToCount.isDefined) sparkSession.createDataset(hdfsFleNameSeq.take(limitDatasetToCount.get))
    else sparkSession.createDataset(hdfsFleNameSeq)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file MySpark.scala
//=============================================================================