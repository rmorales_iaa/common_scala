//=============================================================================
//File: DataFrame.scala
//=============================================================================
/** It implements some utilities methods related with DataFrame in catSpark. See object declaration
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.spark.dataFrame
//=============================================================================
// System import sectionName
//=============================================================================
import java.io.{File, FileOutputStream, PrintWriter}
import org.apache.commons.csv.CSVFormat
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
//=============================================================================
import com.common.logger.MyLogger
import com.common.util.path.Path
import com.common.util.path.Path._
//=============================================================================
// User import sectionName
//=============================================================================
//=============================================================================
// Class/Object implementation
//=============================================================================
/**
 * Methods to manipulate columns of a dataframe, CSV files and Parquet files
 */
object MyDataframe extends MyLogger {
  //-------------------------------------------------------------------------
  var sqlContext: SQLContext = null
  //-------------------------------------------------------------------------
  /** CSV format string used to manage CSV files */
  private var CVS_SavingFormat = CSVFormat.DEFAULT
    .withRecordSeparator(LineSeparator)
    .withDelimiter(',')
    .withQuote('"')
    .withEscape(null)
    .withQuoteMode(null)
    .withSkipHeaderRecord(false)
    .withNullString(null)
  //-------------------------------------------------------------------------
  /** Returns the n-column of a dataframe
   * @param data Dataframe to calculate
   * @param colPos Position of the column to calculate
   * @return N-column of a dataframe
   */
  def getCol(data: DataFrame, colPos: Integer = 0): Column =
    data.col(data.columns(colPos))

  //-------------------------------------------------------------------------
  /** Returns an array of rows of a dataframe
   * @param data Dataframe to calculate
   * @param colPos Position of the column to calculate
   * @return N-column of a dataframe
   */
  def getColAsArray(data: DataFrame, colPos: Integer = 0): Array[Any] =
    data.select(data.columns(colPos)).rdd.map(r => r(colPos)).collect()

  //-------------------------------------------------------------------------
  /** Returns the column names list of a dataframe
   * @param data Dataframe to calculate
   * @return List of the column names 
   */
  def getColNameList(data: DataFrame): Array[String] =
    data.schema.fields.map { f => f.name }

  //-------------------------------------------------------------------------
  /** Returns name on N-column of a dataframe
   * @param data Dataframe to calculate
   * @param colPos Position of the column to calculate
   * @return Name of the  N-column
   */
  def getColName(data: DataFrame, colPos: Int): String =  data.schema.fieldNames(colPos)

    //-------------------------------------------------------------------------
  def isColNamePresent(df: DataFrame, colName: String): Boolean = getColNameList( df ).contains( colName )
  
  //-------------------------------------------------------------------------
  def getCol(df: DataFrame, colPos: Int): DataFrame = df.select(getColName(df,colPos))

  //-------------------------------------------------------------------------
  /** Returns a dataframe where the columns names are present in a column name list
   * @param data Dataframe to calculate
   * @param colNameList Column name list to be present in the final dataframe
   * @param columnNameOrderBy Name of the column used to order the result. Empty if not order required
   * @return New dataframe with all columns present in the column name list
   */  
  def filterByColName(data: DataFrame
    , colNameList: Seq[String]
    , columnNameOrderBy: String=""): (Boolean, DataFrame) = {
    
    //check that all col name are present in the dataset
    val intersc = getColNameList(data).toSet.intersect(colNameList.toSet)

    if (intersc.size != colNameList.size) {
      error("Invalid columList. Not all column name are in the table. List of valid column name: " + intersc)
      return (false, null)
    }

    if (columnNameOrderBy.isEmpty)
      (true
        , data.select(colNameList.map(col): _*))
    else
      (true
        , data.select(colNameList.map(col): _*).orderBy(columnNameOrderBy))
  }

  //-------------------------------------------------------------------------
  /** Returns, for each column of the dataframe, if they are float or double
   * @param data Dataframe to calculate
   * @return New array where pos-n is true if type on the associated 
   * column in the dataframe is float or double   
   */ 
  def getType(data: DataFrame): Array[Boolean] = {
    val schema = data.schema
    schema.fieldNames.map { colName =>
      schema(colName).dataType match {
        case FloatType => true
        case DoubleType => true
        case _ => false
      }
    }
  }

  //-------------------------------------------------------------------------
  def isFloatType(data: DataFrame, colName: String): Boolean = {
    data.schema(colName).dataType match {
      case FloatType => true
      case _ => false
    }
  }
  //-------------------------------------------------------------------------
  def isDoubleType(data: DataFrame, colName: String): Boolean = {
    data.schema(colName).dataType match {
      case DoubleType => true
      case _ => false
    }
  }
 //-------------------------------------------------------------------------
 /** Creates a NEW iterator that applies the CSV format string  to an input row
  * @param iterator Input row iterator
  * @param newColFormat Format to apply to the row
  * @return New iterator that applies a format string to an input row
  */
  private def applyCSV_FormatToRow(iterator: Iterator[Row]
    , newColFormat: Array[Any => AnyRef]): Iterator[String] = {

    new Iterator[String] {
      override def hasNext: Boolean = iterator.hasNext
      override def next: String = {

        if (iterator.nonEmpty) {
          val valueList: Seq[AnyRef] = iterator.next().toSeq.zipWithIndex.map {
            case (value, i) => newColFormat(i)(value)
          }
          CVS_SavingFormat.getRecordSeparator + CVS_SavingFormat.format(valueList:_*)
        } //if
        else ""
      } //def
    } //iterator
  } //method


  //-------------------------------------------------------------------------
  /** Returns a NEW dataframe that append an extra column with an increasing index starting from 0)
    *  to all rows of an input dataframe
    *  Modified from: [[http://stackoverflow.com/questions/30304810/dataframe-ified-zipwithindex]]
    * @param data Dataframe to calculate
    * @param offset Starting row index
    * @param inFront True if append index as first column of the result dataframe.
    *        False to be the last one
    * @return NEW dataframe that append an extra column with an increasing index (starting from 0)
    *  to all rows of an input dataframe
    */
  //-------------------------------------------------------------------------
  def dataFrameZipWithIndex(data: DataFrame,
                            offset: Int = 1,
                            colName: String = "_id",
                            inFront: Boolean = true) : DataFrame = {

    data.sqlContext.createDataFrame(
      data.rdd.zipWithIndex.map{ln =>
        Row.fromSeq(
          (if (inFront) Seq(ln._2 + offset) else Seq())
            ++ ln._1.toSeq ++
            (if (inFront) Seq() else Seq(ln._2 + offset)))},
      StructType(
        (if (inFront) Array(StructField(colName,LongType,nullable = false))
        else Array[StructField]())
          ++ data.schema.fields ++
          (if (inFront) Array[StructField]()
          else Array(StructField(colName,LongType,nullable = false)))
      )
    )
  }
  //-------------------------------------------------------------------------
  /** Saves a dataframe to a CSV file. Modified from: [[https://github.com/databricks/hadoop-csv]]
   * @param data Dataframe to calculate
   * @param fileName Name of the CSV file to save to
   * @param generateHeader True if the name of the dataframe columns will be added 
   *        to the output file as header. False to do not append any header
   * @param itemDivider Divider to append between columns
   * @param commentHeaderPrefix string to be added before header  
   */
  //-------------------------------------------------------------------------
  //modified from: [[https://github.com/databricks/spark-csv]]
  //file: package.scala
  def saveAsCSV(data: DataFrame
    , fileName: String
    , generateHeader: Boolean = true
    , itemDivider: Char = ','
    , commentHeaderPrefix: String = ""
    , append : Boolean = false
    , printLogInfo: Boolean = true) {
    
    if ( printLogInfo ) info(s"Saving $fileName")
    
    //ensure directory exists
    ensureDirectoryExist(getParentPath(fileName))
    
    CVS_SavingFormat = CVS_SavingFormat.withDelimiter(itemDivider)

    //get the data types that will be used late to fixedPointFormatting (doubles)
    val schema = data.schema
    val formatByColPos = data.schema.fieldNames.map(colName => schema(colName).dataType match {
      case _ => (fieldValue: Any) => fieldValue.asInstanceOf[AnyRef]
    })

    val stringList = data.rdd.mapPartitions {
      case iterator => applyCSV_FormatToRow(iterator, formatByColPos) }.collect //execute lazy transformations

    val writer = new PrintWriter(new FileOutputStream(new File(fileName), append))
    if ( generateHeader )
      writer.write(commentHeaderPrefix + CVS_SavingFormat.format(data.columns.map(_.asInstanceOf[AnyRef]): _*))
    
    if ( append ) stringList.map { row => writer.append(row) }
    else stringList.map          { row => writer.write(row) }      
    writer.close()
    
    if ( printLogInfo ) info(s"Saved $fileName")
  }

  //-------------------------------------------------------------------------
  def loadCSV(inputFileName: String
    , itemDivider: String = ","
    , isHeaderPresent: Boolean = true
    , schema: StructType = null) : Option[DataFrame] = {
     
    try {
      if (schema != null)
        Some(sqlContext.read
          .format("csv")
          .option("header", isHeaderPresent.toString)         // use first line of all files as header
          .option("delimiter", itemDivider) // CSV or TSV
          .option("comment", "#")           // comments star with #
          .schema(schema)
          .load(inputFileName))
      else
        Some(sqlContext.read
          .format("csv")
          .option("header", isHeaderPresent.toString)         // use first line of all files as header
          .option("inferSchema", "true")    // automatically infer data types
          .option("delimiter", itemDivider) // CSV or TSV
          .option("comment", "#")           // comments star with #
          .load(inputFileName))
      }
      catch {        
        case e: Exception => exception(e, s"Error parsing CSV file: '$inputFileName'")
        None
      }
  }
  //-------------------------------------------------------------------------
  def joinCombination(lDF: DataFrame, rDF: DataFrame, joinColName: Seq[String]): (DataFrame, DataFrame, DataFrame) = {

    val dfCommon =    lDF.join(rDF, joinColName)
    val dfOnlyLeft =  lDF.join(rDF, usingColumns = joinColName, joinType="left_anti")
    val dfOnlyRight = rDF.join(lDF, usingColumns = joinColName, joinType="left_anti")
    (dfCommon, dfOnlyLeft, dfOnlyRight)
  }
  //-------------------------------------------------------------------------
  def compareTwoDF(rootOutputDirectory : String

                   , leftDF: DataFrame
                   , leftDfName: String

                   , rightDF: DataFrame
                   , rightDfName: String

                   , joinColName: Seq[String]) : Unit = {

    val r = joinCombination(leftDF, rightDF, joinColName)

    val dfCommon = r._1
    val dfOnlyLeft = r._2
    val dfOnlyRight = r._3

    info(s"'$leftDfName' row count:      "   + leftDF.count)
    info(s"'$rightDfName' row count:       " + rightDF.count)
    info(s"Common row count:     "           + dfCommon.count)
    info(s"Only '$leftDfName' row count: "   + dfOnlyLeft.count)
    info(s"Only '$rightDfName' row count:  " + dfOnlyRight.count)

    ensureDirectoryExist(rootOutputDirectory)
    val d = ensureEndWithFileSeparator(rootOutputDirectory)

    saveAsCSV(leftDF,   d + leftDfName  + ".csv")
    saveAsCSV(rightDF,  d + rightDfName + ".csv")

    saveAsCSV(dfOnlyLeft,  d + "only_"  + leftDfName + ".csv")
    saveAsCSV(dfOnlyRight, d + "only_"  + rightDfName+ ".csv")
    saveAsCSV(dfCommon,    d + "common_"+ leftDfName + "_"+ rightDfName + ".csv")
  }
  //-----------------------------------------------------------------------------
  def compareTwoDF(outputDir: String
              , csv_1: String
              , csv_1_prefix: String

              , csv_2: String
              , csv_2_prefix: String
              , joinColName: Seq[String]) {

    Path.deleteDirectory( outputDir )
    Path.ensureDirectoryExist( outputDir )
    val dir = Path.ensureEndWithFileSeparator( outputDir )

    val dfGaia = MyDataframe.loadCSV(csv_1).get
    val dfMatchGaia = MyDataframe.loadCSV(csv_2).get

    MyDataframe.compareTwoDF(dir, dfGaia, csv_1_prefix, dfMatchGaia, csv_2_prefix, joinColName)
  }

  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of object 'MyDataframe'
//=============================================================================
// End of 'MyDataFrame.scala' file
//=============================================================================
