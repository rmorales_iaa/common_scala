/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  20/Sep/2024
 * Time:  11h:05m
 * Description: None
 */
package com.common.logger
//=============================================================================
import java.io.{FileWriter, IOException}
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
//=============================================================================
//=============================================================================
case class ThreadSafeLogger(filePath: String) {
  //-----------------------------------------------------------------------------
  // Formatter for timestamping the log entries
  private val dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
  //-----------------------------------------------------------------------------
  // Synchronized method for logging messages
  def add(message: String
         , itemDivider:String="\t"): Unit = synchronized {
    val timestamp = LocalDateTime.now().format(dateTimeFormatter)
    val logMessage = s"[$timestamp]$itemDivider$message\n"

    try {
      val fileWriter = new FileWriter(filePath, true)  // Open file in append mode
      try {
        fileWriter.write(logMessage)
      }
      finally {
        fileWriter.close()  // Ensure the file is closed even in case of an error
      }
    }
    catch {
      case e: IOException => println(s"Error writing to log file: ${e.getMessage}")
    }
  }
  //-----------------------------------------------------------------------------
}
//=============================================================================
//End of file ThreadSafeLogger.scala
//=============================================================================