/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Nov/2018
 * Time:  14h:38m
 * Description: None
 */
//=============================================================================
package com.common.logger
//=============================================================================
import org.apache.logging.log4j.scala.Logging
//=============================================================================
//=============================================================================
trait MyLogger extends Logging {
  //---------------------------------------------------------------------------
  def fatal(s: String): Boolean =   { logger.fatal(s); false }
  def error(s: String): Boolean =   { logger.error(s); false }
  def exception(e: Exception, s: String): Boolean = { logger.error(" " + e.toString + s); false }
  def warning(s: String): Boolean = { logger.warn(s); true }
  def info(s: String): Boolean =    { logger.info(s); true }
  def debug(s: String): Boolean =   { logger.debug(s); true}
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file MyLogger.scala
//=============================================================================