/*
 * Rafael Morales (rmorales@iaa.es)
 * Instituto de Astrofísisca de Andalucía - CSIC
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  05/abr/2018
  * Time:  14h:12m
  * Description: It manages local files or HDFS files
  */
//=============================================================================
package com.common.opencv
//=============================================================================
import com.common.util.native.MyNativeLibrary
import org.opencv.calib3d.Calib3d
import org.opencv.core.DMatch
import org.opencv.features2d.DescriptorMatcher
import org.opencv.utils.Converters
import org.opencv.xfeatures2d.SURF

import scala.collection.JavaConverters.seqAsJavaListConverter
//=============================================================================
import org.opencv.core.Mat
import org.opencv.core.Core
import com.common.logger.MyLogger

import org.opencv.core.{CvType, Mat, MatOfDMatch, MatOfKeyPoint, MatOfPoint2f, Point, Scalar}
import org.opencv.features2d.{Features2d}
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.core.MatOfFloat
import org.opencv.xfeatures2d.SURF

//=============================================================================
 object Opencv extends MyNativeLibrary {
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
}
//=============================================================================
import Opencv._
case class Opencv() extends MyLogger {
  //---------------------------------------------------------------------------
  loadLibrary
  val version = Core.getVersionString()
  info(s"Using opencv version:'$version'")
  //---------------------------------------------------------------------------
  private def loadLibrary = {
    load("AbsolutePath.nativeLibrary.path"
      , "AbsolutePath.opencv.library")
  }
  //---------------------------------------------------------------------------
  def fits() = {
    val detector = SURF.create()
    val keyPointsA = new MatOfKeyPoint()
    val keyPointsB = new MatOfKeyPoint()
    val descriptorsA = new Mat()
    val descriptorsB = new Mat()

    val matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE)
    val matches = new MatOfDMatch()
    matcher.`match`(descriptorsA, descriptorsB, matches)
/*
    // Filtrar correspondencias usando RANSAC para obtener una transformación más robusta
    val goodMatches = filtrarCorrespondenciasRANSAC(keyPointsA.toList, keyPointsB.toList, matches.toList)

    // Extraer coordenadas de puntos correspondientes
    val ptsA = goodMatches.map( match
    => keypointsA.toList( match
    .queryIdx
    ).pt
    )
    val ptsB = goodMatches.map( match
    => keypointsB.toList( match
    .trainIdx
    ).pt
    )

    // Convertir listas de puntos a matrices OpenCV
    val ptsAMat = Converters.vector_Point2d_to_Mat(ptsA.asJava)
    val ptsBMat = Converters.vector_Point2d_to_Mat(ptsB.asJava)

    // Calcular la matriz de transformación utilizando findHomography
    val transformacion = Calib3d.findHomography(ptsBMat, ptsAMat, Calib3d.RANSAC, 5.0)

    // Aplicar la transformación a los puntos del conjunto B
    val puntosTransformados = new Mat()
    Core.perspectiveTransform(ptsBMat, puntosTransformados, transformacion)

    // Imprimir la matriz de transformación y los puntos transformados
    println("Matriz de transformación:")
    println(transformacion)

    println("Puntos transformados:")
    Converters.Mat_to_vector_Point2d(puntosTransformados).forEach(println)
*/
  }
  //---------------------------------------------------------------------------
  def filtrarCorrespondenciasRANSAC(keypointsA: List[Point], keypointsB: List[Point], matches: List[DMatch]): List[DMatch] = {
    /*
    val ptsA = keypointsA
    val ptsB = keypointsB

    val ptsAMat = Converters.vector_Point2d_to_Mat(ptsA.asJava)
    val ptsBMat = Converters.vector_Point2d_to_Mat(ptsB.asJava)

    val goodMatchesMat = new MatOfDMatch()
    Calib3d.findHomography(ptsBMat, ptsAMat, Calib3d.RANSAC, 5.0, goodMatchesMat)

    val goodMatchesList = goodMatchesMat.toList
    matches.filter( match
    => goodMatchesList.contains( match
    ) )*/
    List()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Opencv
// ============================================================================
