/**
  * Created by: Rafael Morales (rmorales@iaa.es) 
  * Date:  28/Jan/2021 
  * Time:  21h:29m
  * Description: None
  */
//=============================================================================
//File: ssh.scala
//=============================================================================
/** It implements a ssh management
  *  @author  Rafael Morales Muñoz
  *  @mail    rmorales.iaa.es
  *  @version 1.0
  *  @date    9 Nov 2016
  *  @history None
  */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.ssh
//=============================================================================
// System import sectionName
//=============================================================================
import com.common.control.Control
import com.common.logger.MyLogger
import com.common.ssh.MySSH.CHANNEL_TIMEOUT
import com.common.util.path.Path
import com.common.util.util.Util
import com.jcraft.jsch.{ChannelExec, ChannelSftp, JSch, Session}
//=============================================================================
// User import sectionName
//=============================================================================
//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================
object MySSH{
  //---------------------------------------------------------------------------
  final def CHANNEL_TIMEOUT = 60000
  //---------------------------------------------------------------------------
  def appy(hostName: String, hostPort: Int, userName: String, password: String ) =
    MySSH(hostName, hostPort, userName, privateKeyFilePath = "", password)
  //---------------------------------------------------------------------------
}
//=============================================================================
//https://tqft.net/svn/fusionatlas/package/strategy/src/net/tqft/strategy/ssh/SSHCluster.scala
//https://github.com/fommil/bootstrapping-deployer/blob/master/src/main/scala/com/github/fommil/deployer/ssh.scala
//Remember to copy the public key in the remote host
case class MySSH (hostName: String
                  , hostPort: Int
                  , userName: String
                  , privateKeyFilePath: String
                  , password: String
                  , enableCompression: Boolean = true) extends  MyLogger {
  //-------------------------------------------------------------------------
  // Variable declaration
  //-------------------------------------------------------------------------
  private val jsch = new JSch()
  private val session = if (password.isEmpty) sessionByKeyFile else sessionByPassword
  private var sessionOpen = false
  //-------------------------------------------------------------------------
  try{
    if(session!=null){
      session.connect()
      if (enableCompression) activateCompression()
      sessionOpen = true
      info(s"Connected by ssh to:'$userName@$hostName:$hostPort")
    }
  }
  catch {
    case e: Exception => error(s"Error connecting with ssh server: 'userName@$hostName:hostPort " + e.toString())
  }
  //-------------------------------------------------------------------------
  def this(hostName: String, hostPort: Int, userName: String, privateKeyFilePath: String) =
    this(hostName, hostPort, userName, privateKeyFilePath, password ="")
  //-------------------------------------------------------------------------
  private def disableHostKeyChecking(session: Session) =
    session.setConfig("StrictHostKeyChecking", "no");
  //-------------------------------------------------------------------------
  private def activateCompression() {
    session.setConfig("compression.s2c", "zlib@openssh.com,zlib,none")
    session.setConfig("compression.c2s", "zlib@openssh.com,zlib,none")
    session.setConfig("compression_level", "9")
  }
  //-------------------------------------------------------------------------
  private def sessionByPassword: Session = {
    try{
      val session = jsch.getSession(userName, hostName, hostPort)
      disableHostKeyChecking(session)
      session.setPassword(password)
      session
    }
    catch {
      case e: Exception => fatal(s"Error connecting with ssh server: 'userName@$hostName:hostPort " + e.toString())
      null
    }
  }
  //-------------------------------------------------------------------------
  private def sessionByKeyFile : Session = {
    try{
      jsch.addIdentity(privateKeyFilePath)
      val session = jsch.getSession(userName, hostName, hostPort)
      disableHostKeyChecking(session)
      session
    }
    catch {
      case e: Exception => fatal(s"Error connecting with ssh server: 'userName@$hostName:hostPort " + e.toString())
      null
    }
  }
  //-------------------------------------------------------------------------
  def run(command: String, ignoreResult: Boolean = true, logCommand : Boolean= true): String = {
    try {
      if(logCommand) info(s"Executing command in remote node: '$command'")
      Control.withDisconnect(session.openChannel("exec").asInstanceOf[ChannelExec]){ channel =>
        channel.setCommand(command)
        channel.setInputStream(null)
        channel.connect()
        val result = scala.io.Source.fromInputStream(channel.getInputStream()).getLines.foldLeft("")(_  + _ + "\n")
        if (!ignoreResult)
          if (channel.getExitStatus != 0) error(s"Error executing ssh command: '$command' ")
        result
      }.get
    }
    catch {
      case e: Exception =>
        fatal(s"Error in 'MySSH.run'" + e.toString())
        "ERROR!"
      case _ : Throwable => ""
    }
  }
  //-------------------------------------------------------------------------
  def copyToRemote(localFileSeq: Array[String], remoteDir: String, size: Long) {
    try {
      val channel = session.openChannel("sftp")
      val sftpChannel = channel.asInstanceOf[ChannelSftp]
      sftpChannel.connect(CHANNEL_TIMEOUT)
      localFileSeq foreach { localFile=>
        val remoteFile = remoteDir + localFile
        info(s"Copying from: '$localFile' to '$remoteFile'")
        try { sftpChannel.put(localFile, remoteFile) }
        catch {
          case e: Exception => fatal(s"Error copying to local the remote the file '$remoteFile'. " + e.toString())
          case _ : Throwable => ""
        }
      }
      sftpChannel.exit
      channel.disconnect
    }
    catch {
      case e: Exception => fatal(s"Error in 'MySSH.copyRemoteFileToLocal'" + e.toString())
      case _ : Throwable =>
    }
  }
  //-------------------------------------------------------------------------
  def copyFromRemote(localDir: String, remoteFileSeq: Array[String], keepRemotePath: Boolean = true) {
    try {
      val channel = session.openChannel("sftp")
      val sftpChannel = channel.asInstanceOf[ChannelSftp]
      sftpChannel.connect(CHANNEL_TIMEOUT)
      remoteFileSeq.foreach{ r =>
        val remoteFile = r.replaceAll("//","/")
        val filePrefix =
          if(keepRemotePath) {
            val parentPath = Path.ensureEndWithFileSeparator(Path.getParentPath(remoteFile))
            val seq = parentPath.split("/")
            seq(seq.length-2) + "/"
          }
          else ""
        val localFile = {
          if (Util.isOsWindows)
            (localDir + filePrefix + Path.getOnlyFilename(remoteFile)).replaceAll(":","_")
          else {
            val finalLocalPath = localDir + filePrefix
            Path.ensureDirectoryExist(finalLocalPath)
            Path.getCompatiblePath(finalLocalPath).replaceAll("//","/")
          }
        }

        info(s"Copying from: '$remoteFile' to '$localFile'")
        try { sftpChannel.get(remoteFile, localFile) }
        catch {
          case e: Exception => fatal(s"Error copying from remote the file '$remoteFile'. " + e.toString())
          case _ : Throwable => ""
        }
      }
      sftpChannel.exit
      channel.disconnect
    }
    catch {
      case e: Exception => fatal(s"Error in 'MySSH.copyRemoteDirToLocal'" + e.toString())
      case _ : Throwable =>
    }
  }
  //-------------------------------------------------------------------------
  def close() = {
    if (sessionOpen) {
      session.disconnect
      info(s"Closed ssh session in: '$userName@$hostName:$hostPort")
    }
  }
  //-------------------------------------------------------------------------
  def isSessionOpen() = sessionOpen
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //End of case class 'MySSH'
//=============================================================================
// End of 'ssh.scala' file
//=============================================================================
