//=============================================================================
//File: Tap.scala
//=============================================================================
/** It implements the table access protocol (TAP) of
 *  international virtual observatory alliance 
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    21 Jun 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.protocol.tap
//=============================================================================
// System import sectionName
//=============================================================================
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import scalaj.http.{HttpRequest, HttpResponse}
//=============================================================================
// User import sectionName
//=============================================================================
import com.common.util.path.Path._
import com.common.util.time.Time._
//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================
case class TapQueryResult(queryName: String, query: String, data: String)
//=============================================================================
trait ProtocolTap extends MyLogger {
  //-------------------------------------------------------------------------
  // enumerations
  //-------------------------------------------------------------------------
  sealed abstract class OutputFormat(val s: String)
  case object VotableOutputFormat extends OutputFormat("votable")
  case object CsvOutputFormat extends OutputFormat("csv")

  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  // class variable
  //-------------------------------------------------------------------------
  protected val outputRootPath : String
  protected val urlBase : String
  protected val tapServer: String
  protected val requestTableList: HttpRequest
  protected val requestSync: HttpRequest
  protected val requestAsync: HttpRequest
  //-------------------------------------------------------------------------
  def getTableList(_logResponse : Boolean = false) : Boolean = {

    val start = System.currentTimeMillis
    info("Starting query to retrieve database scheme")

    val r: HttpResponse[String] = requestTableList
      .charset("UTF-8")
      .postMulti()
      .asString

    info("End of query. Elapsed time: "+  getFormattedElapseTimeFromStart(start))
    info("Saving result of query")

    if (r.code != 200){ //check result   
      error(s"Error getting database scheme")
      return logResponse(r)
    }

    if (_logResponse) logResponse(r)
/*
    //save result
    val f =  MyFile(outputRootPath, immediatelyWrite = true)
    f.write(r.body + LineSeparator)
    f.close()

    info("Saved query result with database scheme @ " + f.getName)*/

    true
  }

  //-------------------------------------------------------------------------
  def syncQuery(queryName: String
                , query: String
                , connectionTimeoutMs: Int = 5 * 1000
                , readTimeoutMs : Int = 30 * 1000
                , format: OutputFormat = CsvOutputFormat
                , _logResponse : Boolean = false
                , isQueryCount : Boolean = false): Option[TapQueryResult] = {

    val start = System.currentTimeMillis
    info(s"Starting query to database: $tapServer")

    val result: HttpResponse[String] = requestSync
      .charset("UTF-8")
      .timeout(connectionTimeoutMs,readTimeoutMs)
      .param("REQUEST","doQuery")
      .param("LANG","ADQL")
      .param("FORMAT",format.s)
      .param("QUERY",query)
      .asString

    info(s"End of query to $tapServer. Elapsed time: "+  getFormattedElapseTimeFromStart(start))

    if (result.code != 200){ //check result   
      error(s"Error executing the query '$query'")
      return None
    }
    if (_logResponse) logResponse(result)

    //create dataframe
    if (result.body.isEmpty)   {
      info("AstrometryQuery has reported no results")
      None
    }
    else
      Some (TapQueryResult(queryName,query,result.body))
  }
  //-------------------------------------------------------------------------
  private def logResponse(r:HttpResponse[String]) : Boolean = {

    val body = r.body
    val code = r.code
    val header = r.headers
    val cokkie = r.cookies

    info(s"Body:   '$body'")
    info(s"Code:   '$code'")
    info(s"Header: '$header'")
    info(s"Cokkie: '$cokkie'")
  }
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of case class 'Tap'

//=============================================================================
// End of 'Tap.scala' file
//=============================================================================
