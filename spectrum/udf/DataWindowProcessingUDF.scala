//=============================================================================
//File: columnToRow.scala
//=============================================================================
/** It implements aggregates all wavelength and list the central wavelength
//c
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    28 Sept 2017
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.spectrum.udf
//=============================================================================
// System import sectionName
//=============================================================================
import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types._
import scala.collection.mutable.ListBuffer
//=============================================================================
// User import sectionName
//=============================================================================
import com.common.spectrum.Spectrum
import com.common.spectrum.udf.DataWindowProcessingUDF.PairType
//=============================================================================
// Class/Object implementation
//=============================================================================
object DataWindowProcessingUDF {
  //---------------------------------------------------------------------------
  type PairType = (Double,Double)
  //---------------------------------------------------------------------------
  object WindowDataResult{
    //-------------------------------------------------------------------------
    final val WAVE_LENGTH_START_COL_NAME  = "wavelengthStart"
    final val EMISSION_LINE_COL_NAME      = "emissionLine"
    final val WAVE_LENGTH_END_COL_NAME    = "wavelengthEnd"
    final val STD_CONTRAST_COL_NAME       = "stdContrast"
    final val FEATURE_COL_NAME            = "feature"
    //-------------------------------------------------------------------------
  }
  //===========================================================================
  case class WindowDataResult(wavelengthStart: Double
                              , emissionLine: Double
                              , wavelengthEnd: Double
                              , stdContrast: Double
                              , feature: String)
  //---------------------------------------------------------------------------
  case class FluxWindowDataStat(stdLeft: Double
                                , medianLeft: Double
                                , medianRight: Double
                                , stdRight: Double)
  //---------------------------------------------------------------------------
  case class SimpleStat(min: Double
                       , max: Double
                       , average : Double
                       , median : Double
                       , variance : Double
                       , stdDev : Double)

  //---------------------------------------------------------------------------
  //(left side, SimpleStat), (right side, SimpleStat))
  def splitAndGetWindowStat(v: Seq[Double], exclusionRadiusAroundCenterInPoints: Int = 0): ((Seq[Double],SimpleStat), (Seq[Double],SimpleStat)) = {
    //-------------------------------------------------------------------------
    def stat(data: Seq[Double], isPoblation : Boolean = false) : SimpleStat = {
      val n  = data.size.toDouble
      val average = data.sum / n.toDouble
      val sorted = data.sorted
      val median = if (data.size % 2 == 1) sorted(sorted.size / 2).toDouble
      else {
        val posRight = data.size / 2
        val posLeft = posRight - 1
        (data(posRight) + data(posLeft)) / 2.0d
      }
      val variance = data.map( v=> Math.pow(average - v, 2) ).sum / (if (isPoblation) n else n-1)
      SimpleStat(sorted.head, sorted.last, average, median, variance, Math.sqrt(variance))
    }
    //-------------------------------------------------------------------------
    val halfSize = v.length / 2
    val leftSide  = v.take(halfSize).dropRight(exclusionRadiusAroundCenterInPoints)
    val rightSide  = v.takeRight(halfSize).drop(exclusionRadiusAroundCenterInPoints)

    ((leftSide,stat(leftSide)) , (rightSide,stat(rightSide)))
    //-------------------------------------------------------------------------
  }
}
//=============================================================================
//https://docs.databricks.com/spark/latest/spark-sql/udaf-scala.html
//https://www.placeiq.com/2017/07/pointsofinterest_udafsinsparkdataframes/

import DataWindowProcessingUDF.WindowDataResult._
trait DataWindowProcessingUDF extends UserDefinedAggregateFunction {
  //---------------------------------------------------------------------------
  // Input data type
  def inputSchema: StructType = Spectrum.SCHEMA
  //---------------------------------------------------------------------------
  // Output data Type
  def dataType: DataType =
    StructType(
      Array(
          StructField(WAVE_LENGTH_START_COL_NAME, DoubleType)
        , StructField(EMISSION_LINE_COL_NAME,     DoubleType)
        , StructField(WAVE_LENGTH_END_COL_NAME,   DoubleType)
        , StructField(STD_CONTRAST_COL_NAME,      DoubleType)
        , StructField(FEATURE_COL_NAME,           StringType)
      )
    )
  //---------------------------------------------------------------------------
  // Intermediate data type to compute the aggregation
  def bufferSchema =
    StructType( Array( StructField("wavelength_flux_pair", ArrayType( inputSchema ))))
  //---------------------------------------------------------------------------
  // Self-explaining
  def deterministic = true
  //---------------------------------------------------------------------------
  // This distribution.function is called whenever key changes
  def initialize(buffer: MutableAggregationBuffer ): Unit = {
    buffer.update(0, Array.empty[PairType])
  }
  //---------------------------------------------------------------------------
  // Iterate over each entry of a group
  def update(buffer: MutableAggregationBuffer, row: Row): Unit = {
    val newStorage = new ListBuffer[PairType]()
    newStorage ++= buffer.getAs[List[PairType]](0)   //append previous values
    val newPair = ( row.getAs[Double](0), row.getAs[Double](1) )
    newStorage += newPair      //append NEW value
    buffer.update(0, newStorage)
  }
  //---------------------------------------------------------------------------
  // Merge two partial aggregates
  def merge(buffer: MutableAggregationBuffer, row: Row): Unit = {
    val newStorage = new ListBuffer[PairType]()
    newStorage ++= buffer.getAs[List[PairType]](0)
    newStorage ++= row.getAs[List[PairType]](0)
    buffer.update(0,newStorage)
  }
  //---------------------------------------------------------------------------
  // Called after all the entries are exhausted.
  def evaluate(buffer: Row) : Any
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
}
//=============================================================================
// End of 'DataWindowProcessing.scala' file
//=============================================================================
