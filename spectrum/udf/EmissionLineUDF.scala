/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  11/nov./2019
 * Time:  19h:32m
 * Description: None
 */
//=============================================================================
package com.common.spectrum.udf
//=============================================================================
import com.common.math.regression.Regression
import org.apache.spark.sql.{DataFrame, Dataset, Row}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.col

import scala.collection.mutable
//=============================================================================
import com.common.logger.MyLogger
import com.common.math.MyMath
import com.common.spectrum.Spectrum
import com.common.spectrum.udf.DataWindowProcessingUDF.{SimpleStat, WindowDataResult}
import DataWindowProcessingUDF.WindowDataResult._
import com.common.spectrum.udf.DataWindowProcessingUDF._
//=============================================================================
//=============================================================================
object EmissionLineUDF extends MyLogger {
 //---------------------------------------------------------------------------
 final val AGGREGATED_COL_NAME = "aggregated"
 //----------------------------------------------------------------------------
 final val FEATURE_NOT_SET  = "NOT_SET"
 final val FEATURE_INVALID  = "INVALID"
 final val FEATURE_VALID    = "VALID"
 //----------------------------------------------------------------------------
 private val projectionColSeq = Seq (
     AGGREGATED_COL_NAME + "." + WAVE_LENGTH_START_COL_NAME
   , AGGREGATED_COL_NAME + "." + EMISSION_LINE_COL_NAME
   , AGGREGATED_COL_NAME + "." + WAVE_LENGTH_END_COL_NAME
   , AGGREGATED_COL_NAME + "." + STD_CONTRAST_COL_NAME)
 //----------------------------------------------------------------------------
  //The wavelengths are split in 'wavelengthPartitionCount' equal size groups ('wavelengthPartitionCount' must be odd).
  // A valid emission line must fall inside the central split
 def getFeature(wavelengthFluxPairSeq: Seq[(Double, Double)]
                , sigmaContrastClipping: Byte
                , psfRadius: Byte
                , wavelengthPartitionCount: Int
                , calculateRegression: Boolean = true) : WindowDataResult = {
   //--------------------------------------------------------------------------
   if (wavelengthFluxPairSeq.isEmpty)  return WindowDataResult(0, 0, 0, 0, FEATURE_NOT_SET)
   val startWaveLength = wavelengthFluxPairSeq.head._1
   val endWaveLength = wavelengthFluxPairSeq.last._1
   val fluxSeq = wavelengthFluxPairSeq.map( _._2)
   //--------------------------------------------------------------------------
   def findWavelength(flux: Double): Int = {
     wavelengthFluxPairSeq.zipWithIndex.foreach{ case ((_,f),i) => if(f == flux) return i}
     -1
   }
   //--------------------------------------------------------------------------
   def assignFeature(statLeftSide: SimpleStat, statRightSide: SimpleStat) = {
     val averageMedian = (statLeftSide.median + statRightSide.median) / 2
     val averageStd = (statLeftSide.stdDev + statRightSide.stdDev) / 2
     val fluxMax = fluxSeq.max
     val wavelengthSplitPos = findWavelength(fluxMax) / (wavelengthFluxPairSeq.length / wavelengthPartitionCount)
     if (wavelengthSplitPos != (wavelengthPartitionCount/2)) (0d, FEATURE_INVALID) //max flux is not in the central split
     else {
       val range = fluxMax - averageMedian
       if (range < (averageStd * sigmaContrastClipping)) (0d, FEATURE_INVALID)
       else (range / averageStd, FEATURE_VALID)
     }
   }
   //--------------------------------------------------------------------------
   val fluxStat = splitAndGetWindowStat(fluxSeq, psfRadius)
   val centralPair = wavelengthFluxPairSeq(wavelengthFluxPairSeq.length/2)
   if (calculateRegression){
     val wavelengthSeq = wavelengthFluxPairSeq.map( _._1)

     //take the left pairs (wavelength, flux) from left and right sides avoiding the central length and its psf radius
     val leftPair = (wavelengthSeq zip fluxStat._1._1).map { t=> Seq(t._1, t._2)}.toIndexedSeq
     val rightPair = (wavelengthSeq.takeRight(fluxStat._2._1.length) zip fluxStat._2._1).map { t=> Seq(t._1, t._2)}.toIndexedSeq

     //calculate the regression and apply to the original input spectrum to obtain a NEW one
     val regression = Regression.leastSquares(leftPair ++ rightPair)
     val slope = regression._1
     val intercept = regression._2
     val newSpectrum = wavelengthFluxPairSeq.map{ t=> (t._1, t._2 - ((t._1 * slope) + intercept)) }

     //calculate the feature on the NEW spectrum
     getFeature(newSpectrum, sigmaContrastClipping, psfRadius, wavelengthPartitionCount, false)
   }
   else{
     val feature = assignFeature(fluxStat._1._2, fluxStat._2._2)
     WindowDataResult(startWaveLength, centralPair._1, endWaveLength, feature._1, feature._2 )
   }
  }
  //---------------------------------------------------------------------------
  def mining( ds: Dataset[Spectrum]
             , windowRadius: Byte
             , sigmaContrastClipping: Byte
             , psfRadius: Byte
             , wavelengthPartitionCount: Int
             , calculateRegression: Boolean = true) : Option[DataFrame] = {

    if (MyMath.isEven(windowRadius)) {
      error(s"The window radius: $windowRadius must be odd")
      None
    }
    //https://stackoverflow.com/questions/46310359/spark-dataframe-aggregation-based-on-two-or-more-columns
    //window with no partition column, so use all values
    val windowSpec = Window.orderBy(Spectrum.WAVELENGTH_COL_NAME)
      .rowsBetween(-windowRadius, windowRadius) //row frame in Spark semantics

    val udf = EmissionLineUDF(sigmaContrastClipping, psfRadius, wavelengthPartitionCount)
    val df = ds.withColumn(AGGREGATED_COL_NAME, udf(Spectrum.COL_NAME_SEQ.map(ds(_)): _*).over(windowSpec))

    Some(df.filter(df(AGGREGATED_COL_NAME + "." + FEATURE_COL_NAME) === EmissionLineUDF.FEATURE_VALID)
         .select(projectionColSeq.map(col): _*))
  }
  //----------------------------------------------------------------------------
}
//=============================================================================
import EmissionLineUDF._
case class EmissionLineUDF(sigmaContrastClipping: Byte
                         , psfRadius: Byte
                         , wavelengthPartitionCount: Int) extends DataWindowProcessingUDF {
  //---------------------------------------------------------------------------
   def evaluate(buffer: Row) : WindowDataResult = {

    val pairRowSeq = buffer.getAs[mutable.WrappedArray[Row]](0)
    val pairSeq = pairRowSeq.map{ case Row(waveLength:Double, flux:Double) => (waveLength, flux)}
    getFeature(pairSeq, sigmaContrastClipping, psfRadius, wavelengthPartitionCount) //the 'WindowDataResult' must match  'DataWindowProcessingUDF' output data type
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file EmissionLineCalculation.scala
//=============================================================================