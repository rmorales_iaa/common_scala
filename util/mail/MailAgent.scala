/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  29/Jul/2021 
 * Time:  09h:33m
 * Description: Based on https://alvinalexander.com/source-code/scala/scala-send-email-class-uses-javamail-api/
 */
//=============================================================================
package com.common.util.mail
//=============================================================================
import com.common.configuration.MyConf
import com.common.logger.MyLogger
import com.common.util.time.Time.TimeFormatShort
//=============================================================================
import com.sun.mail.imap.{IMAPFolder, SortTerm}

import java.io.File
import java.util.{Date, Properties}
import javax.activation.{CommandMap, MailcapCommandMap}
import javax.mail.internet.{InternetAddress, MimeBodyPart, MimeMessage, MimeMultipart}
import javax.mail.search.{AndTerm, ComparisonTerm, ReceivedDateTerm}
import javax.mail._
import scala.util.{Failure, Success, Try}
//===========================================================================
object MailAgent {
  //-------------------------------------------------------------------------
  val mailConf = MyConf(MyConf.c.getString("Mail.configurationFile"))
  val mailElapsedTimeMillis = mailConf.getInt("Mail.delayBetweenMailInSeconds") * 1000
  //-------------------------------------------------------------------------
}
//===========================================================================
import com.common.util.mail.MailAgent._
case class MailAgent() extends MyLogger {
  //---------------------------------------------------------------------------
  private val user     = mailConf.getString("Mail.user")
  private val password = mailConf.getString("Mail.password")
  //---------------------------------------------------------------------------
  private val smtpHost         = mailConf.getString("Mail.smtp.host")
  private val smtpHostPort     = mailConf.getString("Mail.smtp.port")
  //---------------------------------------------------------------------------
  private val imapHost         = mailConf.getString("Mail.imap.host")
  private val imapPort         = mailConf.getString("Mail.imap.port")
  //---------------------------------------------------------------------------
  configureMime
  //---------------------------------------------------------------------------
  val auth = new Authenticator() {
    override protected def getPasswordAuthentication = new PasswordAuthentication(user, password)
  }
  //---------------------------------------------------------------------------
  private def configureMime(): Unit ={
    val mc = CommandMap.getDefaultCommandMap.asInstanceOf[MailcapCommandMap]
    mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html")
    mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml")
    mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain")
    mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed")
    mc.addMailcap("message/rfc822;; x-java-content- handler=com.sun.mail.handlers.message_rfc822")
    CommandMap.setDefaultCommandMap(mc)
  }
  //---------------------------------------------------------------------------
  def send(to: String
           , from: String
           , subject: String
           , content: String
           , cc: String  = ""
           , bcc: String  = ""
           , filename: Option[String] = None) =  {
    Try {
      val message = configureForSending

      message.setFrom(new InternetAddress(from))
      setToCcBccRecipients(message, to, cc, bcc)
      message.setSentDate(new Date())
      message.setSubject(subject)
      message.setText(content)

      if(filename.isDefined) {
        //https://www.baeldung.com/java-send-emails-attachments
        val messageBodyPart = new MimeBodyPart()
        messageBodyPart.setText(content)

        val attachmentPart = new MimeBodyPart()
        attachmentPart.attachFile(new File(filename.get))

        val multipart = new MimeMultipart()

        multipart.addBodyPart(messageBodyPart)
        multipart.addBodyPart(attachmentPart)

        message.setContent(multipart)
      }
      Transport.send(message)
    }
    match
    {
      case Success(_)   => info(s"Mail message sent properly from: '$from' to: '$to' with subject:'$subject'")
      case Failure( e ) => error(s"Error sending mail message from: '$from' to: '$to' with subject:'$subject'" + e.toString )
    }
  }
  //---------------------------------------------------------------------------
  def receive(beginDateString:String, endDateString:String, from: String) =  {
    Try{
      //create properties field//create properties field

      val session = configureForReceiving
      val store = session.getStore("imaps")
      store.connect(imapHost, user, password)

      //get all mails in inboxFolder folder
      val inboxFolder = store.getFolder("INBOX").asInstanceOf[IMAPFolder]
      inboxFolder.open(Folder.READ_ONLY)

      val beginDate = TimeFormatShort.parse(beginDateString)
      val endDate = TimeFormatShort.parse(endDateString)
      val dateRangeTerm = new AndTerm( new ReceivedDateTerm( ComparisonTerm.GE, beginDate)
                                     , new ReceivedDateTerm( ComparisonTerm.LE, endDate))
      val sortTerm = Array[SortTerm](SortTerm.REVERSE, SortTerm.DATE)
      val messageSeq = inboxFolder.getSortedMessages(sortTerm, dateRangeTerm)
      messageSeq.foreach{ message=>
        val mailFound = message.getFrom.find( s=> s.toString.contains(from) )
        if (mailFound.isDefined){
          val content = message.getContent.toString
          println(content)
        }
      }
    }
    match
    {
      case Success(_)   => info(s"Messages read properly")
      case Failure( e ) => error(s"Error reading message" + e.toString )
    }
  }
  //---------------------------------------------------------------------------
  private def configureForSending() = {
    val properties = new Properties()
    properties.put("mail.smtp.host", smtpHost)
    properties.put("mail.smtp.port", smtpHostPort)
    properties.put("mail.smtp.starttls.enable", "true")
    properties.put("mail.smtp.starttls.required","true")
    properties.put("mail.smtp.auth", "true")
    val session = Session.getDefaultInstance(properties, auth)
    //enable for verbose debugging
   // session.setDebug(true)
    new MimeMessage(session)
  }
  //---------------------------------------------------------------------------
  private def configureForReceiving() = {
    val properties = new Properties()
    properties.put("mail.smtp.starttls.enable", "true")
    properties.put("mail.smtp.auth", "true")
    properties.put("mail.smtp.socketFactory.port", imapPort)
    properties.put("mail.transport.protocol", "imaps")
    properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory")
    properties.put("mail.smtp.socketFactory.fallback", "false")
    Session.getDefaultInstance(properties, auth)
  }
  //---------------------------------------------------------------------------
  private def setToCcBccRecipients(message: Message, to:String, cc:String, bcc: String) {
    setMessageRecipients(message, to, Message.RecipientType.TO)
    if (cc != null && !cc.isEmpty) setMessageRecipients(message, cc, Message.RecipientType.CC)
    if (bcc != null && !bcc.isEmpty) setMessageRecipients(message, bcc, Message.RecipientType.BCC)
  }
  //---------------------------------------------------------------------------
  private def setMessageRecipients(message: Message, recipient: String, recipientType: Message.RecipientType) {
    val addressArray = buildInternetAddressArray(recipient).asInstanceOf[Array[Address]]
    if ((addressArray != null) && (addressArray.length > 0))
      message.setRecipients(recipientType, addressArray)
  }
  //---------------------------------------------------------------------------
  private def buildInternetAddressArray(address: String): Array[InternetAddress] =
    InternetAddress.parse(address)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MailAgent.scala
//=============================================================================
