/**
  * Created by: Rafael Morales (rmorales@iaa.es) 
  * Date:  15/Nov/2018 
  * Time:  09h:44m
  * Description: None
  */
//=============================================================================
package com.common.util.time
//=============================================================================
import java.text.SimpleDateFormat
import java.time.{Duration, Instant, LocalDate, LocalDateTime, LocalTime, ZoneId, ZonedDateTime}
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatter._
import java.util.Calendar
import scala.util.{Failure, Success, Try}
//=============================================================================
object Time {
  //-------------------------------------------------------------------------
  final val HOURS_IN_A_DAY             = 24
  final val MINUTES_IN_ONE_HOUR        = 60
  final val SECONDS_IN_ONE_MINUTE      = 60
  final val MILISECONDS_IN_ONE_SECOND  = 1000
  final val NANO_SECONDS_IN_ONE_SECOND = 1000000000L
  //-------------------------------------------------------------------------
  final val SECONDS_IN_ONE_HOUR = MINUTES_IN_ONE_HOUR * SECONDS_IN_ONE_MINUTE
  final val SECONDS_IN_ONE_DAY  = HOURS_IN_A_DAY * SECONDS_IN_ONE_HOUR
  //---------------------------------------------------------------------------
  final val MILLISECONDS_IN_ONE_MINUTE = SECONDS_IN_ONE_MINUTE * MILISECONDS_IN_ONE_SECOND
  final val MILLISECONDS_IN_ONE_HOUR   = SECONDS_IN_ONE_HOUR * MILISECONDS_IN_ONE_SECOND
  final val MILLISECONDS_IN_ONE_DAY    = SECONDS_IN_ONE_DAY * MILISECONDS_IN_ONE_SECOND
  //---------------------------------------------------------------------------
  private final val OBSERVING_NIGHT_TIME_START = LocalTime.parse("12:00:00")
  //---------------------------------------------------------------------------
  final val MINUTES_IN_A_ONE_DAY = HOURS_IN_A_DAY * MINUTES_IN_ONE_HOUR
  final val SECONDS_IN_A_ONE_DAY = MINUTES_IN_A_ONE_DAY * SECONDS_IN_ONE_MINUTE
  //---------------------------------------------------------------------------
  private final val FRACTION_DAY_HOUR =   1d / HOURS_IN_A_DAY
  private final val FRACTION_DAY_MINUTE = 1d / MINUTES_IN_A_ONE_DAY
  private final val FRACTION_DAY_SECOND = 1d / SECONDS_IN_A_ONE_DAY
  //---------------------------------------------------------------------------
  private final val NANO_SECONDS_IN_ONE_MINUTE = SECONDS_IN_ONE_MINUTE * NANO_SECONDS_IN_ONE_SECOND
  //---------------------------------------------------------------------------
  final val DAYS_PER_YEAR = 365
  private final val DAYS_PER_LEAP_YEAR = 366
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Julian_year_(astronomy)
  private final val JULIAN_DATE_EPOCH = LocalDateTime.parse("2000-01-01T12:00:00",DateTimeFormatter.ISO_LOCAL_DATE_TIME)
  final val DAYS_IN_ONE_JULIAN_YEAR            = 365.25d
  private final val SECONDS_IN_ONE_JULIAN_DAY  = 86400
  private final val SECONDS_IN_ONE_JULIAN_YEAR = DAYS_IN_ONE_JULIAN_YEAR * SECONDS_IN_ONE_JULIAN_DAY
  //---------------------------------------------------------------------------
  private final val JD_TO_MJD_CONSTANT =   2400000.5 //Modified Julian Date origin, which is November 17, 1858, at 00:00 UTC."
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/Sidereal_year
  final val SIDEREAL_YEAR_PER_YEAR = 365.256363004d
  //---------------------------------------------------------------------------
  //mpc packet date
  //https://minorplanetcenter.net/iau/info/PackedDates.html
  private final val MPC_PACKET_DATE_LETTER_TO_YEAR_MAP = Map ( "I"->18
                                                             , "J"->19
                                                             , "K"->20)
  private final val MPC_PACKET_DATE_YEAR_TO_LETTER = MPC_PACKET_DATE_LETTER_TO_YEAR_MAP.map(_.swap)

  private final val MPC_PACKET_DATE_MONTH_TO_LETTER_MAP = Map ( 1->"1"
                                                              , 2->"2"
                                                              , 3->"3"
                                                              , 4->"4"
                                                              , 5->"5"
                                                              , 6->"6"
                                                              , 7->"7"
                                                              , 8->"8"
                                                              , 9->"9"
                                                              , 10->"A"
                                                              , 11->"B"
                                                              , 12->"C")

  private final val MPC_PACKET_DATE_LETTER_TO_MONT_MAP= MPC_PACKET_DATE_MONTH_TO_LETTER_MAP.map(_.swap)


  private final val MPC_PACKET_DATE_DAY_TO_LETTER_MAP = Map ( 1->"1"
                                                            , 2->"2"
                                                            , 3->"3"
                                                            , 4->"4"
                                                            , 5->"5"
                                                            , 6->"6"
                                                            , 7->"7"
                                                            , 8->"8"
                                                            , 9->"9"
                                                            , 10->"A"
                                                            , 11->"B"
                                                            , 12->"C"
                                                            , 13->"D"
                                                            , 14->"E"
                                                            , 15->"F"
                                                            , 16->"G"
                                                            , 17->"H"
                                                            , 18->"I"
                                                            , 19->"J"
                                                            , 20->"K"
                                                            , 21->"L"
                                                            , 22->"M"
                                                            , 23->"N"
                                                            , 24->"O"
                                                            , 25->"P"
                                                            , 26->"Q"
                                                            , 27->"R"
                                                            , 28->"S"
                                                            , 29->"T"
                                                            , 30->"U"
                                                            , 31->"V"
                                                          )
  private final val MPC_PACKET_DATE_LETTER_TO_DAY_MAP= MPC_PACKET_DATE_DAY_TO_LETTER_MAP.map(_.swap)
  //---------------------------------------------------------------------------
  private final val JULIAN_DATE_JGREG: Int = 15 + 31 * (10 + 12 * 1582)
  private final val HALF_SECOND = 0.5

  //---------------------------------------------------------------------------
  // create time zone object // create time zone object
  val zoneID_UTC: ZoneId =   ZoneId.of("UTC")
  private val zoneID_Spain: ZoneId = ZoneId.of("Europe/Madrid")
  private val userUTC_Formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(zoneID_UTC)
  private val sqlUTC_Formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(zoneID_UTC)
  val localDateTimeFormatterWithMillis = DateTimeFormatter.ofPattern("yyyy_MM_dd'T'HH_mm_ss.SSS")

  //Equivalent to "DateTimeFormatter.ISO_OFFSET_DATE_TIME" format string but with ms padding
  val logTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss:SSSX").withZone(zoneID_Spain)

  /** Short time format string */
  val TimeFormatShort = new SimpleDateFormat("yyyy-MM-dd")

  /** Current time clock format string */
  private val TimeFormatClock = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss")
  //---------------------------------------------------------------------------
  private final val ALPHABET = "ABCDEFGHJKLMNOPQRSTUVWXY"
  //---------------------------------------------------------------------------
  //java.time in the standard NEW package for managing time. Superseed joda
  //-------------------------------------------------------------------------
  def getFormattedElapseTimeFromStart(startTime: Long): String =
    getFormattedElapseTimeFromStartInMs(System.currentTimeMillis - startTime)
  //-------------------------------------------------------------------------
  def getFormattedElapseTimeFromStartInMs(ms:Long) : String = {

    var remainSeconds= ms / 1000

    val dayCount = remainSeconds / SECONDS_IN_ONE_DAY
    remainSeconds %= SECONDS_IN_ONE_DAY

    val hourCount = remainSeconds / SECONDS_IN_ONE_HOUR
    remainSeconds %= SECONDS_IN_ONE_HOUR

    val minuteCount = remainSeconds / SECONDS_IN_ONE_MINUTE
    val secondsCount = remainSeconds % SECONDS_IN_ONE_MINUTE

    "%d days %02dh %02dm %02ds".format(dayCount, hourCount, minuteCount, secondsCount)
  }
  //-------------------------------------------------------------------------
  def getZonedDateTimeTimeStamp(ms: Long): ZonedDateTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(ms), zoneID_UTC)
  //-------------------------------------------------------------------------
  def getUTC_ZonedUserDateTimeToMs( s: String ): Option[Long] = {
    var t = 0L
    Try {
      t = ZonedDateTime.parse(s,userUTC_Formatter ).toInstant.toEpochMilli
    }
    match {
      case Success(_) => Some(t)
      case Failure(e) =>
        println(s"Error parsing the user date: '$s'");
        println(e.toString)
        None
    }
  }
  //-------------------------------------------------------------------------
  def getTimeStampWindowsCompatible (date: String): String = {
    date.replace('-', '_')
      .replace(':', '_')
      .replace('.', '_')
  }
  //-------------------------------------------------------------------------
  def getZonedDateTimeTimeStamp: ZonedDateTime = ZonedDateTime.ofInstant(Instant.now, zoneID_UTC )
  //-------------------------------------------------------------------------
  def getZonedDateTimeStamp( ms: Long ): String = {
    val zonedDateTime = getZonedDateTimeTimeStamp( ms )
    DateTimeFormatter.ISO_OFFSET_DATE_TIME.format( zonedDateTime )
  }
  //-------------------------------------------------------------------------
  def getISO_DateLocalTimeStamp ( zoneID: ZoneId = zoneID_Spain ): String =
    logTimeFormatter.format( ZonedDateTime.ofInstant(Instant.now, zoneID ) )
  //-------------------------------------------------------------------------
  def getISO_DateLocalTimeStampWindowsCompatible ( zoneID: ZoneId = zoneID_Spain ): String =
    getTimeStampWindowsCompatible( logTimeFormatter.format( ZonedDateTime.ofInstant(Instant.now, zoneID )))
  //-------------------------------------------------------------------------
  def getISO_DateTimeStamp: String = getISO_DateLocalTimeStamp( zoneID_UTC )
  //-------------------------------------------------------------------------
  def getTimeStamp (s: String, formatter: DateTimeFormatter) : Long =
    ZonedDateTime.parse(s, formatter).toInstant.toEpochMilli
  //-------------------------------------------------------------------------
  def getTimeStampNoZone() =
    LocalDateTime.now().format(ISO_LOCAL_DATE_TIME)
  //-------------------------------------------------------------------------
  def getTimeStamp(f: SimpleDateFormat = TimeFormatShort) : String =
    f.format(Calendar.getInstance().getTime)
  //-------------------------------------------------------------------------
  def getSQL_TimeStamp = sqlUTC_Formatter.format( ZonedDateTime.ofInstant(Instant.now, zoneID_UTC ) )
  //-------------------------------------------------------------------------
  private def getHex(v:Long,  byteSize : Int): Array[Byte] = {
    val hex = new Array[Byte](byteSize)
    var mask=(byteSize -1) *8
    for(i<- 0 until byteSize){
      hex(i) = ((v>>mask) & 0xFF).byteValue()
      mask -=8
    }
    hex
  }
  //-------------------------------------------------------------------------
  def getMilliseconds : Array[Byte] = getHex(System.currentTimeMillis,8)
  //-------------------------------------------------------------------------
  def getUTC_Instant(s: String) = ZonedDateTime.ofInstant(Instant.parse(s), zoneID_UTC)
  //-------------------------------------------------------------------------
  def parseLocalDateTime(date: String, timeFormat: DateTimeFormatter = ISO_LOCAL_DATE_TIME) = {
    var localDateTime: LocalDateTime = null
    Try { localDateTime = LocalDateTime.parse(date,timeFormat) }
    match {
      case Success(_) => Some(localDateTime)
      case Failure(_) => None
    }
  }
  //-------------------------------------------------------------------------
  def parseLocalDate(date: String, timeFormat: DateTimeFormatter = ISO_LOCAL_DATE) = {
    var localDate: LocalDate = null
    Try { localDate = LocalDate.parse(date,timeFormat) }
    match {
      case Success(_) => Some(localDate)
      case Success(_) => Some(localDate)
      case Failure(_) => None
    }
  }

  //-------------------------------------------------------------------------
  def getFractionDay(t: LocalDateTime): Double =
    getFractionDay(t.getHour, t.getMinute, t.getSecond)
  //-------------------------------------------------------------------------
  def getFractionDay(h: Int, m: Int, s:Double): Double =
    (h * FRACTION_DAY_HOUR) + (m * FRACTION_DAY_MINUTE) + (s * FRACTION_DAY_SECOND)
  //-------------------------------------------------------------------------
  def getElapsedSecondsSinceBeginningOfYear(t: LocalDateTime) : Double = {
    val timeAtYearStart = LocalDateTime.parse(t.getYear + "-01-01T00:00:00", ISO_LOCAL_DATE_TIME)
    Duration.between(timeAtYearStart, t).toSeconds
  }
  //-------------------------------------------------------------------------
  def getFractionYear(t: LocalDateTime) = {
    val daysPerYear = if (t.toLocalDate.isLeapYear) DAYS_PER_YEAR else DAYS_PER_LEAP_YEAR
    val fractionYear = getElapsedSecondsSinceBeginningOfYear(t) / (daysPerYear.toDouble * SECONDS_IN_ONE_DAY)
    t.getYear.toDouble + fractionYear
  }
  //-------------------------------------------------------------------------
  def toJulianYear(t: LocalDateTime) = {
    val fractionYear =  getElapsedSecondsSinceBeginningOfYear(t) / SECONDS_IN_ONE_JULIAN_YEAR
    t.getYear.toDouble + fractionYear
  }
  //-------------------------------------------------------------------------
  def jDToMJD(jd: Double) =
    jd - JD_TO_MJD_CONSTANT
  //-------------------------------------------------------------------------
  def getHMS_FromFractionDay(t: Double) = {
    val h = (t * HOURS_IN_A_DAY).toInt
    var remain = t - (h * FRACTION_DAY_HOUR)
    val m = (remain * MINUTES_IN_A_ONE_DAY).toInt
    remain -= m * FRACTION_DAY_MINUTE
    val s = remain * SECONDS_IN_A_ONE_DAY
    val finalS = s % 60d
    val m2 = m + (s / 60d)
    val finalM = (m2 % 60d).toInt
    val finalH = (h + (finalM / 60d)).toInt
    (finalH,finalM,finalS)
  }
  //-------------------------------------------------------------------------
  def toJulian(utTimeStamp : LocalDate): Double =
    toJulian(utTimeStamp.atStartOfDay)
  //-------------------------------------------------------------------------
  def toJulian(utTimeStamp : String): Double =
    toJulian(LocalDateTime.parse(utTimeStamp,ISO_LOCAL_DATE_TIME))
  //-------------------------------------------------------------------------
  //Based on: https://www.rgagnon.com/javadetails/java-0506.html
  //Based on: Numerical Recipes in C, 2nd ed., Cambridge University Press 1992
  //https://www.aavso.org/computing-jd
  def toJulian(utTimeStamp : LocalDateTime): Double = {
    val gmtTimestamp = utTimeStamp.minusHours(12) //from UNIVERSAL TIME (UT) to  Greenwich Mean Time (GMT)
    val year = gmtTimestamp.getYear
    val month = gmtTimestamp.getMonthValue
    val day = gmtTimestamp.getDayOfMonth
    val hour = gmtTimestamp.getHour
    val min = gmtTimestamp.getMinute
    val sec = gmtTimestamp.getSecond + (gmtTimestamp.getNano / 1000000000d)
    val fractionTime = getFractionDay(hour,min,sec)

    var julianYear = year
    if (year < 0) julianYear += 1
    var julianMonth = month
    if (month > 2) julianMonth += 1
    else {
      julianYear -= 1
      julianMonth += 13
    }
    var julian = Math.floor(365.25 * julianYear) + Math.floor(30.6001 * julianMonth) + day + 1720995.0
    if (day + 31 * (month + 12 * year) >= JULIAN_DATE_JGREG) { // change over to Gregorian calendar
      val ja = (0.01 * julianYear).toInt
      julian += 2 - ja + (0.25 * ja)
    }
    Math.floor(julian) + fractionTime
  }
  //-------------------------------------------------------------------------
  //https://www.rgagnon.com/javadetails/java-0506.html
  //Numerical Recipes in C, 2nd ed., Cambridge University Press 1992
  //https://www.aavso.org/computing-jd
  def fromJulian(injulian: Double) = {
    var jalpha = 0
    var ja = 0
    var jb = 0
    var jc = 0
    var jd = 0
    var je = 0
    var year = 0
    var month = 0
    var day = 0
    val julian = injulian + HALF_SECOND / 86400.0
    val fractionDay = injulian - injulian.toInt
    ja = julian.toInt
    if (ja >= JULIAN_DATE_JGREG) {
      jalpha = (((ja - 1867216) - 0.25) / 36524.25).toInt
      ja = ja + 1 + jalpha - jalpha / 4
    }
    jb = ja + 1524
    jc = (6680.0 + ((jb - 2439870) - 122.1) / 365.25).toInt
    jd = 365 * jc + jc / 4
    je = ((jb - jd) / 30.6001).toInt
    day = jb - jd - (30.6001 * je).toInt
    month = je - 1
    if (month > 12) month = month - 12
    year = jc - 4715
    if (month > 2) year -= 1
    if (year <= 0) year -= 1
    val (hour,minute,second) = getHMS_FromFractionDay(fractionDay)

    var secondString = f"$second%.3f"
    secondString = if (secondString == "60.000") "59.999" else secondString
    val finalSecondString = if (secondString.length != 6) "0" + secondString else secondString //take into accont the milliseconds

    val dateString = "%04d".format(year) + "-" + "%02d".format(month) + "-" + "%02d".format(day) +
      "T" + "%02d".format(hour) + ":" + "%02d".format(minute) + ":" + finalSecondString

    val gmtTimestamp = LocalDateTime.parse(dateString, ISO_LOCAL_DATE_TIME)
    gmtTimestamp.plusHours(12) //GMAT to UT
  }
  //-------------------------------------------------------------------------
  //https://stackoverflow.com/questions/10350850/java-get-half-month-number
  def getHalfMonthLetter(ld: LocalDate) = {
    val adjustment = if ( ld.getDayOfMonth() < 16 )  1 else  0   // If first half of month, back off the half-month-number by 1.
    val halfMonthNumber = ( ld.getMonthValue * 2 ) - adjustment  // 1-24.
    val index = halfMonthNumber - 1
    ALPHABET.substring( index , index + 1 )
  }
  //-------------------------------------------------------------------------
  def nanosToSeconds(s: Double) = s * (1d / NANO_SECONDS_IN_ONE_SECOND)
  //-------------------------------------------------------------------------
  def secondsToNanos(s: Double) = Math.round(s * NANO_SECONDS_IN_ONE_SECOND)
  //-------------------------------------------------------------------------
  def minuteToNanos(m: Double)  = Math.round(m * NANO_SECONDS_IN_ONE_MINUTE)
  //-------------------------------------------------------------------------
  def daysToSeconds(d: Double)  = Math.round(d * SECONDS_IN_ONE_DAY)
  //-------------------------------------------------------------------------
  //https://minorplanetcenter.net/iau/info/PackedDates.html
  def mpcPacketFormToLocalDate(s:String)  =  {
    val year  = MPC_PACKET_DATE_LETTER_TO_YEAR_MAP(s.take(1)) + s.drop(1).take(2)
    val month = MPC_PACKET_DATE_LETTER_TO_MONT_MAP(s.drop(3).take(1)).toString
    val day   = MPC_PACKET_DATE_LETTER_TO_DAY_MAP(s.drop(4).take(1)).toString
    val m = if (month.length == 1) "0" + month else month
    val d = if (day.length == 1) "0" + day else day
    LocalDate.parse(s"$year-$m-$d")
  }
  //-------------------------------------------------------------------------
  //https://minorplanetcenter.net/iau/info/PackedDates.html
  def localDateToMpcPacketForm(date: LocalDate) = {
    val year = date.getYear.toString
    val col_1 = MPC_PACKET_DATE_YEAR_TO_LETTER(year.take(2).toInt)
    val col_2_3 = year.takeRight(2)
    val col_4 = MPC_PACKET_DATE_MONTH_TO_LETTER_MAP(date.getMonthValue)
    val col_5 = MPC_PACKET_DATE_DAY_TO_LETTER_MAP(date.getDayOfMonth)
    col_1 + col_2_3 + col_4 + col_5
  }
  //-------------------------------------------------------------------------
  //https://www.stjarnhimlen.se/comp/ppcomp.html#3
  //http://www.stjarnhimlen.se/comp/tutorial.html
  //The time scale in these formulae are counted in days. Hours, minutes, seconds are expressed as fractions of a day.
  // Day 0.0 occurs at 2000 Jan 0.0 UT (or 1999 Dec 31, 0:00 UT)
  def getTimeScale(date: LocalDateTime) = {
    val y = date.getYear
    val m = date.getMonthValue
    val D = date.getDayOfMonth
    val d = 367 * y - 7 * ( y + (m+9)/12 ) / 4 - 3 * ( ( y + (m-9)/7 ) / 100 + 1 ) / 4 + 275*m/9 + D - 730515
    val fractionalDay = getFractionDay(date.getHour,date.getMinute,date.getSecond)
    d + fractionalDay
  }
  //-------------------------------------------------------------------------
  def isValidLocalDate(s: String) = {
    Try { LocalDate.parse(s) }
    match {
      case Success(_) => true
      case Failure(_) => false
    }
  }
  //-------------------------------------------------------------------------
  def isValidLocalDateTime(s: String) = {
    Try { LocalDateTime.parse(s) }
    match {
      case Success(_) => true
      case Failure(_) => false
    }
  }

  //---------------------------------------------------------------------------
  def getObservingNightDate(localDateTime: LocalDateTime): LocalDate = {
    val currentNightDate = localDateTime.toLocalDate
    val nightTimeStampDivider = LocalDateTime.parse(localDateTime.toLocalDate + "T" + OBSERVING_NIGHT_TIME_START)
    if (localDateTime.isBefore(nightTimeStampDivider)) currentNightDate.minusDays(1)
    else currentNightDate
  }
  //---------------------------------------------------------------------------
  def getObservingNightDate(jd: Double): LocalDate = getObservingNightDate(Time.fromJulian(jd))
  //-------------------------------------------------------------------------
  def getObservingNightDate(s:String): LocalDate = getObservingNightDate(LocalDateTime.parse(s))
  //-------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Time.scala
//=============================================================================
