/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/nov/2018
 * Time:  02h:57m
 * Description: None
 */
//=============================================================================
//=============================================================================
//File: myFile.scala
//=============================================================================
/** It implements basic operation on a file
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.util.file
//=============================================================================
// System import sectionName
//=============================================================================
import java.io._
import java.text.SimpleDateFormat
import java.util.concurrent.atomic.AtomicBoolean
import java.util.Date
import java.io.{BufferedWriter, FileWriter}
import java.nio.file.{Files, Path, Paths}
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import scala.util.Random
//=============================================================================
import com.common.logger.MyLogger
import com.common.util.path.Path._
import com.common.util.time.Time._
//=============================================================================
//=============================================================================
object MyFile {
  //---------------------------------------------------------------------------
  val PathYear=new SimpleDateFormat("yyyy")
  val PathYearMonth=new SimpleDateFormat("yyyy'_'MM")
  val PathYearMonthDay=new SimpleDateFormat("yyyy'_'MM'_'dd")
  //---------------------------------------------------------------------------
  private val ExtensionPattern = """\.[A-Za-z0-9]+$""".r
  //---------------------------------------------------------------------------
  def copyFile(src: String, dest: String): Path = {
    import java.nio.file.StandardCopyOption.REPLACE_EXISTING
    import java.nio.file.Files.copy
    import java.nio.file.Paths.get
    import scala.language.implicitConversions
    implicit def toPath (filename: String): Path = get(filename)
    copy (src, dest, REPLACE_EXISTING)
  }
  //-------------------------------------------------------------------------
  def getFileListAndSize(dir: String, suffix: String=""): StringBuilder = {

    val s = new StringBuilder
    val d = new File(dir)

    if (!d.exists){
      println (s"Error, directory '$dir' does not exist")
      return s
    }
    if (!d.isDirectory){
      println (s"Error, directory '$dir' is not a directory")
      return s
    }
    d.listFiles.filter(_.isFile).foreach { f => s.append(f.getName+"\t"+f.length+suffix) }
    s
  }
  //-------------------------------------------------------------------------
  /** Returns if the input path is a file
   * @param s Path to check
   * @return True if input is a file, false in other case
   */
  def fileExist(s:String) : Boolean =
  {
    val f = new File(s)
    f.exists && !f.isDirectory
  }
  //-------------------------------------------------------------------------
  def deleteFile(path: String): Boolean =  new File(path).delete()
  //-------------------------------------------------------------------------
  def deleteFileIfExist(path: String): Boolean =
    if (fileExist(path)) new File(path).delete()
    else true
  //-------------------------------------------------------------------------
  def getFileNameNoPathNoExtension (s: String) : String = removeFileExtension(getOnlyFilename(s))
  //-------------------------------------------------------------------------
  def fileByteSize(s:String): Long = new File(s).length()
  //-------------------------------------------------------------------------
  /** Returns a NEW string that ends with the provided suffix
   * @param s MyString to calculate
   * @param suffix to append if it necessary
   * @return Returns a NEW string that ends with the provided suffix
   */
  def ensureFileExtension(s:String,suffix: String) : String = if (s.endsWith(suffix)) s else s+suffix
  //-------------------------------------------------------------------------
  def hasFileExtension(fileName: String, extensions: List[String]): Boolean =
    extensions.map { _.toLowerCase }.exists { fileName.toLowerCase endsWith "." + _ }
  //-------------------------------------------------------------------------
  def getFileExtension(s: String): String = ExtensionPattern findFirstIn s getOrElse ""
  //-------------------------------------------------------------------------
  def removeFileExtension(s: String): String = {
    if (s.indexOf(".") == -1)  s
    else s.slice(s.lastIndexOf("/")+1,s.lastIndexOf("."))
  }
  //-------------------------------------------------------------------------
  def generateUniqueDirectoryName(basePath: String): String = {
    val timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"))
    val randomSuffix = Random.alphanumeric.take(6).mkString
    val directoryName = s"$timestamp-$randomSuffix"
    val directoryPath = Paths.get(basePath, directoryName)

    if (!Files.exists(directoryPath)) Files.createDirectory(directoryPath)
    else generateUniqueDirectoryName(basePath) // Recursively generate until a unique directory name is found

    directoryName
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
// End of 'MyFile.scala' file
//=============================================================================
