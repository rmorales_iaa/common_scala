package com.common.util.thread
//=============================================================================

//=============================================================================
import java.util.concurrent.atomic.AtomicReference
import scala.concurrent.{CancellationException, ExecutionContext, Future, Promise}
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
object MyThread extends MyLogger {
  //-------------------------------------------------------------------------
  def interruptableFuture[T](fun: () => T)(implicit ex: ExecutionContext): (Future[T], () => Boolean) = {
    val p = Promise[T]()
    val f = p.future
    val aref = new AtomicReference[Thread](null)
    p tryCompleteWith Future {
      val thread = Thread.currentThread
      aref.synchronized { aref.set(thread) }
      try fun() finally {
       (aref.synchronized { aref getAndSet null }) ne thread
        //Deal with interrupted flag of this thread in desired
      }
    }

    (f, () => {
      aref.synchronized { Option(aref getAndSet null) foreach { _.interrupt() } }
      p.tryFailure(new CancellationException)
    })
  }
  //-------------------------------------------------------------------------
  def mySleep(name: String
              ,  timeMs: Long = 0) : Boolean = {

    try {Thread.sleep(timeMs)}
    catch {
      case e: InterruptedException => warning(s"Sleep of: $timeMs ms interrupted in : '" +name+"'")
      case _: Throwable => error(s"Error while sleeping $timeMs ms in: '" +name+"'")
        return false
    }
    true
  }

  //-------------------------------------------------------------------------
  def myThread(task: => Unit, name :String = "No name") : Thread ={

    val t= new Thread(new Runnable { def run() : Unit = { task } } )
    t.setName(name)
    t.start()
    t
  }

  //-------------------------------------------------------------------------
  def myNotifyAll(t:Thread) = {

    if (t!=null) t.synchronized{t.notifyAll()}
  }

  //-------------------------------------------------------------------------
  def myNotify(t:Thread) = {

    if (t!=null) t.synchronized{t.notify()}
  }

  //-------------------------------------------------------------------------
  def myWait(t: Thread,
              timeoutMs: Long = 0) : Boolean = {

    if (t!=null){
      t.synchronized{
        try {t.wait(timeoutMs)}
        catch {
          case e: InterruptedException => warning("Timeout waiting on thread: '" +t.getName+"'")
          case _: Throwable => error("Error waiting on thread: '" +t.getName+"'")
            return false
        }
      }
    }
    true
  }
  //-------------------------------------------------------------------------
  def myJoin(t: Thread, timeoutMs: Long = 0) : Boolean = {

    if (t!=null){
      t.synchronized{
        try {t.join(timeoutMs)}
        catch {
          case e: InterruptedException => warning("Timeout join on thread: '" +t.getName+"'")
          case _: Throwable => error("Error join on thread: '" +t.getName+"'")
            return false
        }
      }
    }
    true
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Util.scala
//=============================================================================
