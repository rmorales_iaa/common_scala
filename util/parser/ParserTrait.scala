/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  09/Aug/2021 
 * Time:  12h:21m
 * Description: None
 */
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Feb/2021
 * Time:  10h:36m
 * Description: None
 */
//=============================================================================
package com.common.util.parser
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
import fastparse._
import NoWhitespace._
//=============================================================================
//=============================================================================
object  ParserTrait{

  def digit[_: P] = CharIn("0-9")

  def letterUpper[_: P] = CharIn("A-Z")
  def stringUpper[_: P] = P( letterUpper.rep )

  def letterLower[_: P] = CharIn("u-z")
  def stringLower[_: P] = P( letterLower.rep )

  def space[_: P] = CharIn(" ")

}
//=============================================================================
trait ParserTrait extends MyLogger {
  //---------------------------------------------------------------------------
  def grammar[_: P]: P[Any] = P("")
  //---------------------------------------------------------------------------
  def parse(s: String, parentPos: Int = 0) = {
    //-------------------------------------------------------------------------
    def reporError(pos: Int, additionalMessage: String = "") = {
      val sucessParsing = s.take(pos)
      val remainParsing = s.drop(pos)
      error(s"Error parsing '$s' at position: ${parentPos + pos}. Success parsing: '$sucessParsing'. Remain parsing: '$remainParsing' $additionalMessage")
    }
    //-------------------------------------------------------------------------
    val r = fastparse.parse(s, grammar(_))
    r match {
      case Parsed.Success(_, index) =>
        if (index != s.length) reporError(index) else true
      case failure: Parsed.Failure => reporError(failure.index, failure.trace().longAggregateMsg)
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file ParserTrait.scala
//=============================================================================
