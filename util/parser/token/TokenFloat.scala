/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  09/Aug/2021
 * Time:  12h:50m
 * Description: None
 */
//=============================================================================
package com.common.util.parser.token
//=============================================================================
import com.common.util.parser.token.TokenCommon.{digit, sign}
//=============================================================================
import fastparse._
import NoWhitespace._
//=============================================================================
object TokenFloat {
  //---------------------------------------------------------------------------
  def floating_value[_: P]   = P(decimal_number ~ exponent.?).!.map{ s =>
    val  f = BigDecimal(s)
    if (f.isExactFloat) f
    else {
      throw new IllegalArgumentException(s"Error: '$s' is not a valid float")
      //There is not an easy way to report an error in fastParse https://github.com/lihaoyi/fastparse/pull/244
      BigDecimal(Float.NaN)
    }
  }
  //---------------------------------------------------------------------------
  def exponent[_: P]         = P(exponent_letter ~ sign.? ~ digit ~ digit.rep)
  def decimal_number[_: P]   = P(sign.? ~ integer_part.? ~ ("."  ~ fraction_part).?)
  def integer_part[_: P]     = P(digit | digit.rep)
  def fraction_part[_: P]    = P(digit | digit.rep)
  def exponent_letter [_: P] = P("E" | "D")
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file TokenFloat.scala
//=============================================================================
