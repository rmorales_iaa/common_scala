/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  09/Aug/2021
 * Time:  12h:50m
 * Description: None
 */
//=============================================================================
package com.common.util.parser.token
//=============================================================================
import com.common.util.parser.token.TokenCommon.{digit, sign}
//=============================================================================
import fastparse._
import NoWhitespace._
//=============================================================================
object TokenInteger {
  //---------------------------------------------------------------------------
  def integer_value[_: P]  = P(sign.? ~ digit ~ digit.rep).!.map { s =>
    val  i = BigInt(s)
    if (i.isValidInt) s.toInt
    else {
      //There is not an easy way to report an error in fastParse https://github.com/lihaoyi/fastparse/pull/244
      throw new IllegalArgumentException(s"Error: '$s' is not a valid integer")
      Int.MinValue
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file TokenInteger.scala
//=============================================================================
