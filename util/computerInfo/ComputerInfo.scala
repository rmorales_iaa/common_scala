/**
  * Created by: Rafael Morales (rmorales@iaa.es) 
  * Date:  07/Mar/2019 
  * Time:  10h:04m
  * Description: None
  */
//https://github.com/oshi/oshi/blob/master/oshi-core/src/test/java/oshi/SystemInfoTest.java
//=============================================================================
package com.common.util.computerInfo
//=============================================================================
//=============================================================================
import com.common.logger.MyLogger
import oshi.SystemInfo
import oshi.util.FormatUtil

import java.lang.management.ManagementFactory
import scala.jdk.CollectionConverters.CollectionHasAsScala
import scala.util.{Failure, Success, Try}

//=============================================================================

//=============================================================================
//=============================================================================
object ComputerInfo extends MyLogger{
  //---------------------------------------------------------------------------
  final val DEFAULT_PREFIX = ""
  //---------------------------------------------------------------------------
  val si = new SystemInfo
  val hal = si.getHardware
  val os = si.getOperatingSystem
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/12807797/java-get-available-memory
  def printComputerInternet (prefix: String = DEFAULT_PREFIX) : Unit = {
    import java.net.InetAddress
    Try {
      val ip = InetAddress.getLocalHost
      info(prefix + "host name : " + ip.getHostName())
      info(prefix + "ip address : " + ip.getHostAddress)
    }
    match {
      case Success(_) =>
      case Failure(ex) => error("Error getting computer info" + ex.toString)
    }
  }

  //---------------------------------------------------------------------------
  def printComputerSystem(prefix: String = DEFAULT_PREFIX): Unit = {

    val computerSystem = hal.getComputerSystem()

    info(prefix + "Computer main board")
    info(prefix + "  manufacturer: " + computerSystem.getManufacturer)
    info(prefix + "  model: " + computerSystem.getModel)
    info(prefix + "  serialnumber: " + computerSystem.getSerialNumber)
    val firmware = computerSystem.getFirmware
    info(prefix + "  firmware:")
    info(prefix + "    manufacturer: " + firmware.getManufacturer)
    info(prefix + "    name: " + firmware.getName)
    info(prefix + "    description: " + firmware.getDescription)
    info(prefix + "    version: " + firmware.getVersion)
    info(prefix + "    release date: " + (if (firmware.getReleaseDate == null) "unknown"
    else if (firmware.getReleaseDate == null) "unknown"
    else firmware.getReleaseDate))
    val baseboard = computerSystem.getBaseboard
    info(prefix + "  baseboard:")
    info(prefix + "    manufacturer: " + baseboard.getManufacturer)
    info(prefix + "    model: " + baseboard.getModel)
    info(prefix + "    version: " + baseboard.getVersion)
    info(prefix + "    serialnumber: " + baseboard.getSerialNumber)
  }
  //---------------------------------------------------------------------------
  def printProcessor(prefix: String = DEFAULT_PREFIX): Unit = {
    val processor = hal.getProcessor
    info(prefix + "Processor: " + processor)
    info(prefix + "  physical CPU package(s): " + processor.getPhysicalPackageCount )
    info(prefix + "  physical CPU core(s): " + processor.getPhysicalProcessorCount)
    info(prefix + "  logical CPU(s): " + processor.getLogicalProcessorCount)
    info(prefix + "  identifier: " + processor.getProcessorIdentifier)
  }
  //---------------------------------------------------------------------------
  def printMemory(prefix: String = DEFAULT_PREFIX): Unit = {
    val memory = hal.getMemory
    info(prefix + "Memory available / total: " + FormatUtil.formatBytes(memory.getAvailable) + "/" + FormatUtil.formatBytes(memory.getTotal))
  }
  //---------------------------------------------------------------------------

  private def getGarbageCollectionTime = {
    var collectionTime : Long = 0
    var collectionCount : Long = 0
    for (garbageCollectorMXBean <- ManagementFactory.getGarbageCollectorMXBeans.asScala) {
      collectionTime += garbageCollectorMXBean.getCollectionTime
      collectionCount += garbageCollectorMXBean.getCollectionCount
    }

    (collectionTime, collectionCount)
  }
  //---------------------------------------------------------------------------

  def printJvmMemory(prefix: String = DEFAULT_PREFIX): Unit = {
    val mib = 1024 * 2014

    val maxMemory = Runtime.getRuntime.maxMemory

    info(prefix + "Java Virtual Machine (JVM)")
    info(prefix + "  free memory (MiB) : " + Runtime.getRuntime.freeMemory / mib )
    info(prefix + "  used memory (MiB) : " + Runtime.getRuntime.totalMemory / mib )
    info(prefix + "  maximum memory available (MiB): " + (if (maxMemory == Long.MaxValue) "no limit"else maxMemory / mib))

    val r = getGarbageCollectionTime
    info(prefix + "  garbage collection time (ms)  : " + r._1 )
    info(prefix + "  garbage collection count      : " + r._2 )
  }
  //---------------------------------------------------------------------------

  def printProcesses(prefix: String = DEFAULT_PREFIX): Unit = {
    info(prefix + "Processes: " + os.getProcessCount + ", Threads: " + os.getThreadCount)
  }
  //---------------------------------------------------------------------------
  def printNetworkParameters(prefix: String = DEFAULT_PREFIX): Unit = {
    val networkParams = os.getNetworkParams()
    info(prefix + "Network parameters:")
    info(prefix + "  host name: " + networkParams.getHostName)
    info(prefix + "  domain name: " + networkParams.getDomainName)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file ComputerInfo.scala
//=============================================================================