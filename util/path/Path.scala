/**
  * Created by: Rafael Morales (rmorales@iaa.es) 
  * Date:  15/Nov/2018 
  * Time:  09h:57m
  * Description: None
  */
//=============================================================================
package com.common.util.path
//=============================================================================
import java.io.File
import java.nio.file.Paths
import java.text.SimpleDateFormat
import java.util.Date
import org.apache.commons.io.FileUtils
import scala.collection.mutable.ArrayBuffer
//=============================================================================
import com.common.util.util.Util
import com.common.util.pattern.Pattern
import com.common.util.file.MyFile
//=============================================================================
//=============================================================================
object Path {
  //---------------------------------------------------------------------------
  //Constants regarding to path
  /** Operating system file separator */
  val FileSeparator: String = java.io.File.separator // '/' in gnu-linux
  val FileSeparatorGnuLinux = "/"

  /** Operating system line separator */
  val LineSeparator: String = System.getProperty("line.separator","\n")
  val LineSeparatorGnuLinux = "\n"
  val LineSeparatorWindows = "\r\n"

  /** Operating system path separator */
  val PathSeparator: String = java.io.File.pathSeparator // ':' in gnu-linux

  /** Local file system prefix*/
  val localFileSystemPrefix = "file://"
  //-------------------------------------------------------------------------
  val PathYear=new SimpleDateFormat("yyyy")
  val PathYearMonth=new SimpleDateFormat("yyyy'_'MM")
  val PathYearMonthDay=new SimpleDateFormat("yyyy'_'MM'_'dd")
  //---------------------------------------------------------------------------
  def resetDirectory(s: String)  = {
    deleteDirectoryIfExist(s)
    createDirectoryIfNotExist(s)
    ensureEndWithFileSeparator(s)
  }
  //---------------------------------------------------------------------------
  def createDirectoryIfNotExist(s: String) = {
    val dir = new File(s)
    if (!dir.exists) dir.mkdirs
    s
  }
  //---------------------------------------------------------------------------
  def isAbsolute(s: String) = s.startsWith(FileSeparator)
  //---------------------------------------------------------------------------
  def deleteDirectory(path: String): Unit = FileUtils.deleteDirectory(new File(path))
  //-------------------------------------------------------------------------
  def deleteDirectoryIfExist(path: String): Unit =
    if (directoryExist (path)) FileUtils.deleteDirectory(new File(path))
  //-------------------------------------------------------------------------
  /** Returns if the input path is a directory
    * @param s Path to check
    * @return True if input is a directory, false in other case
    */
  def directoryExist(s:String) : Boolean = {
    var newPath = s
    if (s.startsWith(localFileSystemPrefix))
      newPath = s.drop(localFileSystemPrefix.length)
    val f = new File(newPath)
    f.exists && f.isDirectory
  }
  //-------------------------------------------------------------------------
  /** Returns a NEW string that ends with [com.common.util.FileSeparator]
    * @param s MyString to calculate
    * @return Returns a NEW string that ends with [com.common.util.FileSeparator]
    */
  def ensureEndWithFileSeparator(s:String) : String =
    if (s.endsWith(FileSeparator)) s else s+FileSeparator

  //-------------------------------------------------------------------------
  /** Creates a list of hierarchical directories (if it is necessary) using the specified path
    * @param s Path to calculate
    * @return True if the list of hierarchical directories can be created, false in other case
    */
  def ensureDirectoryExist(s:String, printLog: Boolean = false) : Boolean = {
    val d=new java.io.File(s)
    if (!d.exists()) {
      if (Util.isOsWindows) {
        val fixedDir = new java.io.File(Path.getCompatiblePath(d.getPath))
        if (!fixedDir.mkdirs()){
          println(s"Error creating output directory:'$s'")
          return false
        }
      }
      else {
        if (!d.mkdirs()){
          println(s"Error creating output directory:'$s'")
          return false
        }
      }
      if ( printLog ) println(s"Created output directory:'$d'")
    }
    if ( printLog ) println(s"Using previous output directory:'$d'")
    true
  }
  //-------------------------------------------------------------------------
  def getFileList(dir: String):List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) d.listFiles.filter(_.isFile).toList
    else {
      println(s"\nMyUtil.getFileList. The path '$dir' is not a directory")
      List[File]()
    }
  }
  //-------------------------------------------------------------------------
  def getSubDirectoryList(dir: String): List[File] = {
    if (!directoryExist(dir)) return List[File]()
    new File(dir).listFiles.filter(_.isDirectory).toList
  }
  //-------------------------------------------------------------------------
  def getSortedSubDirectoryList(dir: String): List[File] =
    getSubDirectoryList(dir).sortWith(sortFileByAbsolutePath)
  //-------------------------------------------------------------------------
  private def sortFileByAbsolutePath (f1: File, f2: File) : Boolean =
    f1.getAbsolutePath < f2.getAbsolutePath
  //-------------------------------------------------------------------------
  def getSortedFileList(dir: String, extension: String = ""): List[File] =
    getSortedFileList( dir, List(extension) )
  //-------------------------------------------------------------------------
  def getSortedFileList(dir: String, extensionList: List[String]): List[File] =
    getSortedFileListWithExtension(dir,extensionList).sortWith(sortFileByAbsolutePath)
  //-------------------------------------------------------------------------
  def getFileListWithExtension(dir: String, extension: String): List[File] = getSortedFileListWithExtension(dir,List(extension))
  //-------------------------------------------------------------------------
  def getSortedFileListWithExtension(dir: String, extensionList: List[String]): List[File] = {
    if (!directoryExist(dir)) return List[File]()
    val l = new File(dir).listFiles.filter(_.isFile).sortWith(sortFileByAbsolutePath).toList
    if (extensionList.isEmpty || extensionList.head.isEmpty) l.sortWith(sortFileByAbsolutePath)
    else (l filter { file => extensionList.contains(MyFile.getFileExtension(file.getName))}).sortWith(sortFileByAbsolutePath)
  }
  //-------------------------------------------------------------------------
  def getSortedFileListStartsWithNamePrefix(dir: String, prefix: String): List[File] =
    getFileListStartsWithNamePrefix(dir, prefix).sortWith(sortFileByAbsolutePath)
  //-------------------------------------------------------------------------
  def getFileListStartsWithNamePrefix(dir: String, prefix: String): List[File] = {
    if (!directoryExist(dir)) return List[File]()
    val l = new File(dir).listFiles.filter(_.isFile).sortWith(sortFileByAbsolutePath).toList
    l filter { file => MyFile.getFileNameNoPathNoExtension(file.getName).startsWith(prefix)}
  }
  //-------------------------------------------------------------------------
  def getFileListStartsWithNamePrefixAndSuffix(dir: String, prefix: String, suffix: String): List[File] = {
    if (!directoryExist(dir)) return List[File]()
    val l = new File(dir).listFiles.filter(_.isFile).sortWith(sortFileByAbsolutePath).toList
    l filter { file =>
      MyFile.getFileNameNoPathNoExtension(file.getName).startsWith(prefix) &&
      file.getAbsolutePath.endsWith(suffix) }
  }
  //-------------------------------------------------------------------------
  def getSortedFileListRecursive(dir: String
                                 , fileExtension: Seq[String] = Seq()
                                 , pattern: Option[String] = None
                                 , sorted: Boolean = true
                                 , verbose: Boolean = false
                                 , userAction: Option[File=>Unit] = None) //used for massive directory processing. No result is returned. The function is called on each file
  = {
    //-----------------------------------------------------------------------
    val dirToProcess = ArrayBuffer[String]()
    val result = ArrayBuffer[File]()
    val userPattern = pattern.getOrElse("")
    //-----------------------------------------------------------------------
    def processDir(d: String): Unit = {
      if (verbose) println(s"Parsing directory: '$d'")
      dirToProcess.remove(0)
      dirToProcess ++= (getSubDirectoryList(d) map (s=>s.getAbsolutePath))
      val fileSeq = new File(d).listFiles.filter(_.isFile)
      if (!fileSeq.isEmpty)
        fileSeq map { f =>
          if (pattern.isDefined) {
            val n = Path.getOnlyFilename(f.getName)
            if (userAction.isDefined) userAction.get(f)
            else if (Pattern.parsePattern(n, List(userPattern.r), reportErrorMessage = false)) result += f
          }
          else {
            val extension = MyFile.getFileExtension(f.getName)
            if (fileExtension.contains(extension))
              if (userAction.isDefined) userAction.get(f)
              else result += f
          }
        }
    }
    //-----------------------------------------------------------------------
    dirToProcess += dir
    while (!dirToProcess.isEmpty) processDir(dirToProcess.head)
    if (sorted) result.sortWith( _.getAbsolutePath < _.getAbsolutePath) else result
  }
  //-------------------------------------------------------------------------
  def getSortedSubdirectoryListRecursive(
    dir: String
    , sorted: Boolean = true
    , verbose: Boolean = false
    , userAction: Option[String=>Unit] = None) //used for massive directory processing. No result is returned. The function is called on each sub-dir
  = {
    //-----------------------------------------------------------------------
    val dirToProcess = ArrayBuffer[String]()
    val result = ArrayBuffer[String]()
    //-----------------------------------------------------------------------
    def processDir(d: String): Unit = {
      if (verbose) println(s"Parsing directory: '$d'")
      if (userAction.isDefined) userAction.get(d)
      else {
        dirToProcess.remove(0)
        dirToProcess ++= (getSubDirectoryList(d) map (s=>s.getAbsolutePath))
        result += d
      }
    }
    //-----------------------------------------------------------------------
    dirToProcess += dir
    while (!dirToProcess.isEmpty) processDir(dirToProcess.head)
    if (sorted) result.sorted else result
  }
  //-------------------------------------------------------------------------
  def getCurrentDirectory(): String = ensureEndWithFileSeparator(new java.io.File(".").getCanonicalPath)
  //-------------------------------------------------------------------------
  def getCurrentPath() : String = ensureEndWithFileSeparator(new File("").getAbsolutePath)
  //-------------------------------------------------------------------------
  def getPath(s: String) = Paths.get(new File(s).getAbsolutePath)
  //-------------------------------------------------------------------------
  def getPath(f: File) = Paths.get(f.getAbsolutePath)
  //-------------------------------------------------------------------------
  def splitFilePath(f:File): List[File] = Iterator.iterate(f)(_.getParentFile).takeWhile(_ != null).toList.reverse
  //-------------------------------------------------------------------------
  def getParentPath(s: String): String =  ensureEndWithFileSeparator(Paths.get(new File(s).getAbsolutePath).getParent.toString)
  //-------------------------------------------------------------------------
  def getOnlyFilename(s: String) : String = Paths.get(s).getFileName.toString
  //-------------------------------------------------------------------------
  def getOnlyFilenameNoExtension(s: String) : String =
    MyFile.removeFileExtension(getOnlyFilename(s))
  //-------------------------------------------------------------------------
  def createDirectoryByDate(path: String) : String ={
    //get time stamp
    val now = new Date()

    //create path of directories  based on date
    ensureDirectoryExist(path)

    //directory tree
    val finalDirectory = ensureEndWithFileSeparator(path)+
      PathYear.format(now) + FileSeparator +
      PathYearMonth.format(now) + FileSeparator +
      PathYearMonthDay.format(now)+ FileSeparator

    //process directory list
    ensureDirectoryExist(finalDirectory)
    ensureEndWithFileSeparator(finalDirectory)
  }
  //-------------------------------------------------------------------------
  def getCompatiblePath(path: String) : String = {
    if (!Util.isOsWindows) path.replaceAll(":","_")
    else
      path.replaceAll("//", FileSeparator).replaceAll(":","_")
  }
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Path.scala
//=============================================================================
