/**
  * Created by: Rafael Morales (rmorales@iaa.es) 
  * Date:  27/abr/2019 
  * Time:  07h:33m
  * Description: Creates a queue of items to calculate and processing them in parallel
  */
//=============================================================================
package com.common.util.parallelTask
//=============================================================================
import java.util.concurrent.{ConcurrentLinkedQueue, Executors}
import java.util.concurrent.atomic.AtomicLong
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration
import scala.language.reflectiveCalls
import scala.util.{Failure, Success, Try}
//=============================================================================
import com.common.logger.MyLogger
import com.common.util.time.Time._
//=============================================================================
//=============================================================================
//set 'isItemProcessingThreadSafe' to true when 'userProcessSingleItem' distribution.function is not thread safe (i.e. modify global/shared data)
abstract class ParallelTask [T] (itemSeq: Seq[T]
                                 , threadCount: Int
                                 , isItemProcessingThreadSafe : Boolean
                                 , randomStartMaxMsWait: Int = 0
                                 , forceThreadCount: Option[Int] = None
                                 , verbose: Boolean = true
                                 , message: Option[String] = None) extends MyLogger {
  //---------------------------------------------------------------------------
  def userProcessSingleItem (t :T) : Unit
  //---------------------------------------------------------------------------
  private val queue = new ConcurrentLinkedQueue[T]()
  private val finalThreadCount = if (forceThreadCount.isDefined) forceThreadCount.get
                                 else Math.min(threadCount,if (itemSeq.isEmpty) 1 else itemSeq.length)
  private val totalItemToProcess = itemSeq.length.toDouble
  private val itemProcessed : AtomicLong = new AtomicLong(0)
  private val random = scala.util.Random
  private val lastItemPercentage: AtomicLong = new AtomicLong(-1)
  //---------------------------------------------------------------------------
  implicit val executionContext = new ExecutionContext {
    val threadPool = Executors.newFixedThreadPool(finalThreadCount)
    override def reportFailure(cause: Throwable): Unit = {}
    override def execute(runnable: Runnable): Unit = threadPool.submit(runnable)
    def shutdown() = threadPool.shutdown()
  }
  //---------------------------------------------------------------------------
  run
  //---------------------------------------------------------------------------
  private def singleItemProcessing() {

    val userMessage = if (message.isEmpty) ""
                      else "->" + message.get .trim

    while(!queue.isEmpty) {
      val item = queue.poll
      if(item != null) { //item processing
        Try { userProcessSingleItem( item ) }
        match {
          case Success(_) =>
          case Failure(e) =>
            error(e.toString)
            error(e.getStackTrace.mkString("\n"))
        }

        if (verbose) {
          val currentItemPercentage = Math.round((itemProcessed.addAndGet(1) / totalItemToProcess) * 100)
          if (currentItemPercentage > lastItemPercentage.get()) {
            info(s"Processed: $currentItemPercentage% items" + userMessage)
            lastItemPercentage.set(currentItemPercentage)
          }
        }
      }
    }
  }
  //---------------------------------------------------------------------------
  private def parallelProcessing(): Unit = {
    val taskList: Seq[Future[Unit]] =
     for (_ <- 1 to finalThreadCount) yield {
        if (randomStartMaxMsWait > 0) Thread.sleep(random.nextInt(randomStartMaxMsWait))
         Future {
           if (isItemProcessingThreadSafe) singleItemProcessing
           else synchronized(singleItemProcessing)
         }
     }
    val aggregated = Future.sequence(taskList)
    Await.result(aggregated, Duration.Inf)
  }
  //---------------------------------------------------------------------------
  private def run(): Unit ={
    //init queue
    itemSeq.foreach{ queue.add( _ ) }
    val queueSize = queue.size
    if (verbose) info(s"Processing $queueSize items using $finalThreadCount threads")
    val startTime = System.currentTimeMillis
    parallelProcessing
    if (verbose)
      info("Queue processing time:" + getFormattedElapseTimeFromStart(startTime) +
          ". Items processed:" + itemProcessed.get() +
          s" Remain:${Math.abs(totalItemToProcess -  itemProcessed.get()).toLong} items")

    //shutting down the parallel context
    Try { executionContext.shutdown() }
    match {
      case Success(_) =>
      case Failure(e) =>
        error("Error shutting down the execution context")
        error(e.toString)
        error(e.getStackTrace.mkString("\n"))
    }
  }
  //---------------------------------------------------------------------------
  def addToQueue(item: T) = queue.add(item)
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file ParallelTask.scala
//=============================================================================