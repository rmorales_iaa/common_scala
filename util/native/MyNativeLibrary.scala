/**
  * Created by: Rafael Morales (rmorales@iaa.es) 
  * Date:  09/abr/2019 
  * Time:  13h:35m
  * Description: None
  */
//=============================================================================
package com.common.util.native
//=============================================================================
import com.common.configuration.MyConf
import com.common.logger.MyLogger
//=============================================================================
//=============================================================================
trait MyNativeLibrary extends MyLogger {
  //---------------------------------------------------------------------------
  private var isLoaded = false
  //---------------------------------------------------------------------------
  def load(nativePathConfigurationEntry: String
           , configurationLibraryName: String // The prefix "lib" and suffix ".so" mut be remove from this library name
           , subDirectory: String = ""
          ) : Boolean = {

    if (isLoaded) return true
    val newPath = MyConf.c.getString(nativePathConfigurationEntry)
    val dir = if (newPath.endsWith(java.io.File.separator)) newPath + subDirectory + java.io.File.separator
    else newPath + java.io.File.separator + subDirectory + java.io.File.separator
    val libPath = dir + "lib" + MyConf.c.getString(configurationLibraryName) + ".so"
    System.load(libPath)
    isLoaded = true
    true
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//=============================================================================
//End of file MyNativeLibrary.scala
//=============================================================================
