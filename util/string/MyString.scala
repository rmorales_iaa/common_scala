/**
  * Created by: Rafael Morales (rmorales@iaa.es) 
  * Date:  15/Nov/2018 
  * Time:  09h:52m
  * Description: None
  */
//=============================================================================
package com.common.util.string
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
//=============================================================================
object MyString {
  //---------------------------------------------------------------------------
  def byteSeqToString(a: Seq[Byte], seq: Option[String] = Some(",")): String =
    seq match {
      case None ⇒ a.map("%02x".format(_)).mkString
      case _ ⇒ a.map("%02x".format(_)).mkString(seq.get)
    }
  //---------------------------------------------------------------------------
  //https://codereview.stackexchange.com/questions/20452/a-scala-implementation-of-the-java-trim-method
  //---------------------------------------------------------------------------
  def leftTrim(s: String): String = s dropWhile (_ == ' ')
  //---------------------------------------------------------------------------
  def rightTrim(s: String): String = s.replaceAll("\\s+$", "")
  //---------------------------------------------------------------------------
  def rightPadding(s: String, totalSize: Int, paddingChar: String = " "): String = s + (paddingChar * (totalSize - s.length))
  //---------------------------------------------------------------------------
  def leftPadding(s: String, totalSize: Int, paddingChar: String = " ") = (paddingChar * (totalSize - s.length)) + s
  //---------------------------------------------------------------------------
  def trim: String => String = leftTrim _ compose rightTrim
  //---------------------------------------------------------------------------
  def getHexString(seq: Seq[Byte]) = seq.map("%02X" format _).mkString(" ")
  //---------------------------------------------------------------------------
  def stringToHex(str: String): String = str.toList.map(_.toInt.toHexString).mkString
  //---------------------------------------------------------------------------
  def hexToString(hex: String): String = hex.sliding(2, 2).toArray.map(Integer.parseInt(_, 16).toChar).mkString
  //---------------------------------------------------------------------------
  def isInvalidRestrictedAsciiCharset(s: Seq[Byte]): Boolean =
    s.exists(v ⇒ (v < 0x20) || (v > 0x7e))
  //---------------------------------------------------------------------------
  def isInvalidRestrictedAsciiCharset(s: String): Boolean =
    isInvalidRestrictedAsciiCharset(s.getBytes)
  //---------------------------------------------------------------------------
  def isValidRestrictedAsciiCharset(s: Seq[Byte]): Boolean =
    !isInvalidRestrictedAsciiCharset(s)
  //---------------------------------------------------------------------------
  def isValidRestrictedAsciiCharset(s: String): Boolean =
    isValidRestrictedAsciiCharset(s.getBytes)
  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/44672145/functional-way-to-find-the-longest-common-substring-between-two-strings-in-scala
  def longestCommonSubstring(left: String, right: String): Option[String] =
    if (left.nonEmpty && right.nonEmpty) {
      def substrings(string: String): Stream[String] = {
        def recursive(size: Int = string.length): Stream[String] = {
          if (size > 0) {
            def ofSameLength: Stream[String] =
              (0 to (string.length - size))
                .toStream
                .map(offset => string.substring(offset, offset + size))
            ofSameLength #::: recursive(size - 1)
          }
          else Stream.empty
        }
        recursive()
      }
      val (shorter, longer) =
        if (left.length <= right.length) (left, right)
        else (right, left)
        substrings(shorter).find(longer.contains)
    }
    else None
  //---------------------------------------------------------------------------
  def groupByCommonSubstring(seq: List[String], commonSubstringSize: Int) = {
    val groupMap = scala.collection.mutable.Map[String, ArrayBuffer[String]]()
    seq.foreach { s=>
      val common = s.take(commonSubstringSize)
      if (!groupMap.contains(common)) groupMap(common) = ArrayBuffer[String]()
      groupMap(common) += s
    }
    groupMap
  }
  //---------------------------------------------------------------------------
  def findCommentCharNotInString(s: String): Int = {
    var pos = 0
    var quoteOpen = false
    var lastQuotePos = -1
    var lastCommentPos = -1
    s.foreach { c=>
      pos +=1
      c match {
        case '\'' =>
          if (quoteOpen) quoteOpen = false else quoteOpen = true
          lastQuotePos = pos

        case '/' =>
          lastCommentPos = pos
          if (!quoteOpen) return pos
      }
    }
    if (quoteOpen) lastCommentPos
    else -1
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file MyString.scala
//=============================================================================
