/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  26/May/2023 
 * Time:  11h:44m
 * Description: None
 */
//Based on: https://bitbucket.hdfgroup.org/projects/HDFFV/repos/hdf5-examples/browse/1_14/JAVA
//doc: https://support.hdfgroup.org/ftp/HDF5/releases/HDF-JAVA/hdfview-3.0/docs/javadocs/index.html
//=============================================================================
//=============================================================================
package com.common.hdf5.file
//=============================================================================
import com.common.hardware.cpu.CPU
import com.common.hdf5.HDF5_Object
import com.common.hdf5.HDF5_Object._
import com.common.hdf5.group.HDF5_Group
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
//=============================================================================
import hdf.hdf5lib.H5
import hdf.hdf5lib.HDF5Constants
import scala.util.{Failure, Success, Try}
//=============================================================================
object Hdf5_File extends MyLogger {
  //---------------------------------------------------------------------------
  private def openFile(fileName: String
                       , flags: Int = HDF5Constants.H5F_ACC_RDONLY
                       , accessLink: LINK_DATA_TYPE = HDF5Constants.H5P_DEFAULT): Option[LINK_DATA_TYPE] = {
    var linkD = -1L
    Try {
      linkD = H5.H5Fopen(
          fileName
        , flags
        , accessLink)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Fopen. File: '$fileName' with flags:'$flags' and access link:'$accessLink'")
                         if (linkD < 0) {
                           error(s"HDF5:H5.H5Fopen. Error opening:'$fileName' with flags:'$flags' and access link:'$accessLink'")
                           None
                         }
                         else Some(linkD)

      case Failure(e: Exception) => error(s"HDF5:H5.H5Fopen. Error opening:'$fileName' with flags:'$flags' and acces link:'$accessLink'")
                                    error(e.toString)
                                    None
    }
  }
  //---------------------------------------------------------------------------
  private def close(link: LINK_DATA_TYPE,name: String): Boolean = {
    Try {
      H5.H5Fclose(link)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Fclose. Closed file:'$name' link:'$link'")
        true

      case Failure(e: Exception) => error(s"HDF5:H5.H5Fclose. Error closing file:'$name' link:'$link'")
                                    error(e.toString)
    }
  }
  //---------------------------------------------------------------------------
  def exportAsCsv(inputDir:String
                 , outputDir: String) = {
    //-------------------------------------------------------------------------
    class ExportAsCsv(pathSeq: Array[String]
                      , outputDir: String)
      extends ParallelTask[String](
        pathSeq
        , CPU.getCoreCount()
        , isItemProcessingThreadSafe = true
        , randomStartMaxMsWait = 100) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(fileName: String) = {
        val hdfs5 = Hdf5_File(fileName)
        hdfs5.saveAsCsv(s"$outputDir")
        hdfs5.close()
      }
      //-----------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    val fileNameSeq = Path.getSortedFileList(inputDir, ".h5")
      .map (_.getAbsolutePath)
      .toArray
    new ExportAsCsv(fileNameSeq, outputDir)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import Hdf5_File._
case class Hdf5_File(name: String) extends HDF5_Object {
  //---------------------------------------------------------------------------
  val (link,rootGroup) = loadStructure()
  val simpleName = Path.getOnlyFilenameNoExtension(name)
  //---------------------------------------------------------------------------
  def close():Boolean = {
    if(!isValid) return error(s"HDF5: file:'$name' is invalid can not close")
    rootGroup.get.close()
    Hdf5_File.close(link,simpleName)
    true
  }
  //---------------------------------------------------------------------------
  def isValid  = (link >= 0) &&
                 rootGroup.isDefined
  //---------------------------------------------------------------------------
  private def loadStructure(): (LINK_DATA_TYPE,Option[HDF5_Group]) = {
    info(s"Loading HDF5 file:'$name'")
    val link = Hdf5_File.openFile(name).getOrElse(return (-1,None))
    val rootGroup = HDF5_Group.build(link)
    (link,rootGroup)
  }
  //---------------------------------------------------------------------------
  def saveAsCsv(rootDir:String): Boolean = {
    if(!isValid) return error(s"HDF5: file:'$name' is invalid, can not save as CSV")
    info(s"Exporting HDF5 file to csv:'$simpleName'")
    rootGroup.get.saveAsCsv(Path.createDirectoryIfNotExist(s"$rootDir/$simpleName"))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Hdf5_File.scala
//=============================================================================