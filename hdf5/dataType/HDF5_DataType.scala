/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  30/May/2023 
 * Time:  21h:11m
 * Description: None
 */
//=============================================================================
//=============================================================================
package com.common.hdf5.dataType
//=============================================================================
import com.common.hdf5.HDF5_Object
import com.common.hdf5.HDF5_Object._
import com.common.logger.MyLogger
//=============================================================================
import hdf.hdf5lib.{H5, HDF5Constants}
import scala.util.{Success, Failure, Try}
import java.nio.{ByteBuffer, ByteOrder}
//=============================================================================
object HDF5_DataType extends MyLogger {
  //---------------------------------------------------------------------------
  private final val DATA_TYPE_INT    = "Int"
  private final val DATA_TYPE_FLOAT  = "Float"
  private final val DATA_TYPE_STRING = "String"
  //---------------------------------------------------------------------------
  def build(link: LINK_DATA_TYPE): Option[HDF5_DataType] = {

    val (dataTypeLink,dataByteSize,dataTypeName) = getDataType(link).getOrElse(return None)
    val dataTypeByteOrder = getByteOrder(dataTypeLink).getOrElse(return None)

    Some(HDF5_DataType(
        dataTypeLink
      , dataTypeName
      , dataByteSize
      , dataTypeByteOrder))
  }
  //---------------------------------------------------------------------------
  private def byteSeqToShortSeq(byteSeq: Array[Byte]
                                , byteOrder: ByteOrder): Seq[Short] = {
    val buffer = ByteBuffer.wrap(byteSeq)
      .order(byteOrder)
      .asShortBuffer()
    val s = new Array[Short](buffer.remaining())
    buffer.get(s)
    s.toSeq
  }
  //---------------------------------------------------------------------------
  private def byteSeqToIntSeq(byteSeq: Array[Byte]
                              , byteOrder: ByteOrder): Seq[Int] = {
    val buffer = ByteBuffer.wrap(byteSeq)
      .order(byteOrder)
      .asIntBuffer()
    val s = new Array[Int](buffer.remaining())
    buffer.get(s)
    s.toSeq
  }
  //---------------------------------------------------------------------------
  private def byteSeqToLongSeq(byteSeq: Array[Byte]
                               , byteOrder: ByteOrder): Seq[Long] = {
    val buffer = ByteBuffer.wrap(byteSeq)
      .order(byteOrder)
      .asLongBuffer()
    val s = new Array[Long](buffer.remaining())
    buffer.get(s)
    s.toSeq
  }
  //---------------------------------------------------------------------------
  private def byteSeqToFloatSeq(byteSeq: Array[Byte]
                                , byteOrder: ByteOrder): Seq[Float] = {
    val buffer = ByteBuffer.wrap(byteSeq)
      .order(byteOrder)
      .asFloatBuffer()
    val s = new Array[Float](buffer.remaining())
    buffer.get(s)
    s.toSeq
  }

  //---------------------------------------------------------------------------
  private def byteSeqToDoubleSeq(byteSeq: Array[Byte]
                                   , byteOrder: ByteOrder): Seq[Double] = {
    val buffer = ByteBuffer.wrap(byteSeq)
      .order(byteOrder)
      .asDoubleBuffer()
    val s = new Array[Double](buffer.remaining())
    buffer.get(s)
    s.toSeq
  }
  //---------------------------------------------------------------------------
  private def close(link: LINK_DATA_TYPE, name:String): Boolean = {
    Try {
      H5.H5Tclose(link)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Tclose. Closed data type:'$name' link:'$link'")
        true

      case Failure(e: Exception) => error(s"HDF5:H5.H5Tclose. Error closing data type:'$name' link:'$link'")
        error(e.toString)
    }
  }
  //---------------------------------------------------------------------------
  def getDataType(link: LINK_DATA_TYPE): Option[(Long,Byte,String)] = {
    var dataType: Long = -1
    var dataTypeByteSize: Byte = -1
    var dataTypeName = ""

    Try {
      dataType = H5.H5Dget_type(link)
      if (dataType < 0) return None

      dataTypeByteSize = H5.H5Tget_size(dataType).toByte

      // Print dataset type information
      dataTypeName =  H5.H5Tget_class(dataType) match {
        case HDF5Constants.H5T_INTEGER => DATA_TYPE_INT
        case HDF5Constants.H5T_FLOAT   => DATA_TYPE_FLOAT
        case HDF5Constants.H5T_STRING  =>
          DATA_TYPE_STRING

        case _ =>
          error(s"HDF5:H5.H5Dget_type. Error data type:'$dataType' of link:'$link' not implemented yet")
          return None
      }
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Dget_type. Got data type of link:'$link'")
        Some((dataType,dataTypeByteSize,dataTypeName))
      case Failure(e: Exception) => error(s"HDF5:H5.H5Dget_type. Error getting data type of link:'$link'")
        error(e.toString)
        None
      case e: Throwable => error(s"HDF5:H5.H5Dget_type. Error getting data type of link:'$link'")
        error(e.toString)
        None
    }
  }
  //---------------------------------------------------------------------------
  private def getByteOrder(link: LINK_DATA_TYPE): Option[ByteOrder] = {
    var byteOrder: Int = -1
    Try {
      byteOrder = H5.H5Tget_order(link)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Tget_order. Got the byte order of link:'$link'")

        Some(byteOrder match {
          case HDF5Constants.H5T_ORDER_LE => ByteOrder.LITTLE_ENDIAN
          case _                          => ByteOrder.BIG_ENDIAN
        })

      case Failure(e: Exception) => error(s"HDF5:H5.H5Tget_order. Error getting the byte order of link:'$link'")
        error(e.toString)
        None
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import HDF5_DataType._
case class HDF5_DataType(link: LINK_DATA_TYPE
                         , name: String
                         , byteSize: Int
                         , byteOrder: ByteOrder) extends HDF5_Object {
  //---------------------------------------------------------------------------
  def close(): Boolean = HDF5_DataType.close(link,name)
  //---------------------------------------------------------------------------
  def saveAsCsv(rootDir: String): Boolean = false
  //---------------------------------------------------------------------------
  def formatData(byteSeq: Array[Byte]): Array[AnyVal] = {
    name match {
      case DATA_TYPE_INT   =>
        byteSize match {
          case 1 => byteSeq.toArray
          case 2 => byteSeqToShortSeq(byteSeq, byteOrder).toArray
          case 4 => byteSeqToIntSeq(byteSeq, byteOrder).toArray
          case 8 => byteSeqToLongSeq(byteSeq, byteOrder).toArray
        }
      case DATA_TYPE_FLOAT =>
        byteSize match {
          case 4 => byteSeqToFloatSeq(byteSeq, byteOrder).toArray
          case 8 => byteSeqToDoubleSeq(byteSeq, byteOrder).toArray
        }
      case DATA_TYPE_STRING =>
        Array("").asInstanceOf[Array[AnyVal]]

      case _ =>
        Array("").asInstanceOf[Array[AnyVal]]
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file HDF5_DataType.scala
//=============================================================================