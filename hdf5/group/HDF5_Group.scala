/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  30/May/2023 
 * Time:  21h:11m
 * Description: None
 */
//=============================================================================
//=============================================================================
package com.common.hdf5.group
//=============================================================================
import com.common.hdf5.HDF5_Object
import com.common.hdf5.HDF5_Object._
import com.common.hdf5.dataSet.HDF5_DataSet
import com.common.logger.MyLogger
import com.common.util.path.Path
//=============================================================================
import hdf.hdf5lib.H5
import hdf.hdf5lib.HDF5Constants
import scala.util.{Failure, Success, Try}
import hdf.hdf5lib.structs.H5G_info_t
import scala.collection.mutable.ArrayBuffer
//=============================================================================
object HDF5_Group extends MyLogger {
  //---------------------------------------------------------------------------
  def build(linkID: LINK_DATA_TYPE): Option[HDF5_Group] = {
    val link = open(linkID).getOrElse(return None)
    val groupInfo = info(link).getOrElse(return None)
    val root = ArrayBuffer[HDF5_Group]()
    val r = getChildren(link
                         , groupInfo.nlinks.toInt
                         , name = HDF5_DATASET_ROOT_PATH
                         , root)
    if (r) Some(root.head)
    else None
  }
  //---------------------------------------------------------------------------
  private def open(link: LINK_DATA_TYPE
                   , name: String = HDF5_DATASET_ROOT_PATH
                   , accessLink: LINK_DATA_TYPE = HDF5Constants.H5P_DEFAULT): Option[LINK_DATA_TYPE] = {

    var newlink = -1L
    Try {
      newlink = H5.H5Gopen(
        link
        , name
        , accessLink)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Gopen. Link:'$link' with name:'$name' and access link:'$accessLink'")
        if (newlink < 0) {
          error(s"HDF5:H5.H5Gopen. Error opening link:'$link' with path:'$name' and access link:'$accessLink'")
          None
        }
        else Some(newlink)

      case Failure(e: Exception) => error(s"HDF5:H5.H5Gopen. Error opening link:'$link' with path:'$name' and access link:'$accessLink'")
                                    error(e.toString)
                                    None
    }
  }

  //---------------------------------------------------------------------------
  private def close(link: LINK_DATA_TYPE, name: String): Boolean = {
    Try {
      H5.H5Gclose(link)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Gclose. Closed group:'$name' link:'$link'")
        true

      case Failure(e: Exception) => error(s"HDF5:H5.H5Gclose. Error closing group:'$name' link:'$link'")
                                    error(e.toString)
    }
  }

  //---------------------------------------------------------------------------
  private def info(link: LINK_DATA_TYPE): Option[H5G_info_t] = {
    var r: H5G_info_t = null
    Try {
      r = H5.H5Gget_info(link)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5G_info_t. Get info link:'$link' success")
        Some(r)

      case Failure(e: Exception) => error(s"HDF5:H5G_info_t. Error getting inf from link:'$link'")
                                    error(e.toString)
                                    None
    }
  }

  //---------------------------------------------------------------------------
  private def getChildren(parentLink: LINK_DATA_TYPE
                          , parentChildCount: Int
                          , name: String //Name of group for which information is to be retrieved
                          , root: ArrayBuffer[HDF5_Group]
                          , parent: Option[HDF5_Group] = None
                          , indexTye: Int = HDF5Constants.H5_INDEX_NAME
                          ): Boolean = {

    val childNameSeq     = Array.fill[String](parentChildCount)("") //Names of all objects under the group, name.
    val childTypeSeq     = Array.fill[Int](parentChildCount)(0) //Types of all objects under the group, name.
    val childLinkTypeSeq = Array.fill[Int](parentChildCount)(0)
    val childLinkSeq     = Array.fill[LINK_DATA_TYPE](parentChildCount)(0) //Reference number of all objects under the group, name.
    var childCount       = -1

    Try {
      childCount = H5.H5Gget_obj_info_all(
        parentLink
        , name
        , childNameSeq
        , childTypeSeq
        , childLinkTypeSeq
        , childLinkSeq
        , indexTye)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Gget_obj_info_all. Name: '$name' and link:'$parentLink' success")
      case Failure(e: Exception) =>
        error(s"HDF5:H5.H5Gget_obj_info_all. Error getting info from name: '$name' and link:'$parentLink'")
        error(e.toString)
        return false
    }

    //build group
    val child = HDF5_Group(
          parentLink
        , name
        , childNameSeq
        , childTypeSeq
        , childLinkTypeSeq
        , childLinkSeq
        , parent)

    if (parent.isDefined) parent.get.addObject(child)
    else root += child

    //recursive call for all child groups
    for (i <- 0 until childCount) {
      val childName = childNameSeq(i)
      val childType = childTypeSeq(i)

      childType match {
        case HDF5Constants.H5O_TYPE_GROUP =>
          val newGroupLinkID = open(parentLink, childName).getOrElse(return false)
          val newGroupInfo = info(newGroupLinkID).getOrElse(return false)

          //recursive call
          val r = getChildren(
              newGroupLinkID
            , newGroupInfo.nlinks.toInt
            , name + childName
            , root
            , parent = Some(child))

          if (!r) return false

        case HDF5Constants.H5O_TYPE_UNKNOWN =>
          return error(s"HDF5:H5.H5Gget_obj_info_all. Unknown object type from name: '$name' and link:'$parentLink'")

        case HDF5Constants.H5O_TYPE_DATASET =>
          val dataSet = HDF5_DataSet.build(child.link, childName).getOrElse(return false)
          child.addObject(dataSet)

        case HDF5Constants.H5O_TYPE_NAMED_DATATYPE =>
          return error(s"HDF5:H5.H5Gget_obj_info_all. Object type: 'H5O_TYPE_NAMED_DATATYPE' to be implemented. Name: '$name' and link:'$parentLink'")
          
        case HDF5Constants.H5O_TYPE_NTYPES =>
          return error(s"HDF5:H5.H5Gget_obj_info_all. Object type: 'H5O_TYPE_NTYPES' to be implemented. Name: '$name' and link:'$parentLink'")

        //H5O_TYPE_MAP  is not the the java API

        case _ =>
          return error(s"HDF5:H5.H5Gget_obj_info_all. Unsupported type: '$childType'. Name: '$name' and link:'$parentLink'")
      }
    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class HDF5_Group(link: LINK_DATA_TYPE
                      , name: String
                      , objectNameSeq: Array[String]
                      , objectTypeSeq: Array[Int]
                      , objectLinkTypeSeq: Array[Int]
                      , linkSeq:Array[LINK_DATA_TYPE]
                      , parent: Option[HDF5_Group] = None) extends HDF5_Object {
  //---------------------------------------------------------------------------
  private val childSeq = ArrayBuffer[HDF5_Object]()
  //---------------------------------------------------------------------------
  def close(): Boolean = {
    childSeq.foreach(_.close())
    HDF5_Group.close(link,name)
  }
  //---------------------------------------------------------------------------
  def getChildCount = objectNameSeq.size
  //---------------------------------------------------------------------------
  def addObject(o: HDF5_Object) = childSeq += o
  //---------------------------------------------------------------------------
  def saveAsCsv(rootDir: String): Boolean = {
    val childCount = getChildCount
    for (i <- 0 until childCount) {
      val childName = objectNameSeq(i)
      val childType = objectTypeSeq(i)
      val childLink = linkSeq(i)
      val child = childSeq(i)

      childType match {
        case HDF5Constants.H5O_TYPE_GROUP =>
          val r = child.saveAsCsv(Path.createDirectoryIfNotExist(s"$rootDir/$name"))
          if (!r) return false

        case HDF5Constants.H5O_TYPE_DATASET =>
          val r = child.saveAsCsv(Path.createDirectoryIfNotExist(s"$rootDir/$name"))
          if (!r) return false

        case HDF5Constants.H5O_TYPE_NAMED_DATATYPE =>
          return error(s"HDF5:saveAsCsv. Object type: 'H5O_TYPE_NAMED_DATATYPE' to be implemented. Name: '$childName' and link:'$childLink'")

        case HDF5Constants.H5O_TYPE_NTYPES =>
          return error(s"HDF5:saveAsCsv. Object type: 'H5O_TYPE_NTYPES' to be implemented. Name: '$childName' and link:'$childLink'")

        //H5O_TYPE_MAP  is not the the java API

        case HDF5Constants.H5O_TYPE_UNKNOWN =>
          return error(s"HDF5:saveAsCsv. Object type: 'H5O_TYPE_UNKNOWN' to be implemented. Name: '$childName' and link:'$childLink'")

        case _ =>
          return error(s"HDF5:H5.saveAsCsv. Unsupported type: '$childType'. Name: '$name' and link:'$childLink'")
      }
    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file HDF5_Group.scala
//=============================================================================