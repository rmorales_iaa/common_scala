/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  31/May/2023 
 * Time:  19h:33m
 * Description: None
 */
//=============================================================================
package com.common.hdf5.dataSet
//=============================================================================
import com.common.hdf5.HDF5_Object
import com.common.hdf5.HDF5_Object._
import com.common.logger.MyLogger
//=============================================================================
import hdf.hdf5lib.H5
import scala.util.{Failure, Success, Try}
//=============================================================================
object HDF5_DataSetSpace extends MyLogger {
  //---------------------------------------------------------------------------
  def build(link: LINK_DATA_TYPE): Option[HDF5_DataSetSpace] = {
    var space: Long = -1
    Try {
      space = H5.H5Dget_space(link)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Dget_space. Got space of link:'$link'")
        Some(HDF5_DataSetSpace(space,""))

      case Failure(e: Exception) => error(s"HDF5:H5.H5Dget_space. Error getting space of link:'$link'")
        error(e.toString)
        None
    }
  }
  //---------------------------------------------------------------------------
  private def close(link: LINK_DATA_TYPE): Boolean = {
    Try {
      H5.H5Sclose(link)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Sclose. Closed data set space link:'$link'")
        true

      case Failure(e: Exception) => error(s"HDF5:H5.H5Sclose. Error closing data set space link:'$link'")
        error(e.toString)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class HDF5_DataSetSpace(link: LINK_DATA_TYPE
                             , name: String) extends HDF5_Object {
  //---------------------------------------------------------------------------
  def saveAsCsv(rootDir: String): Boolean = false
  //---------------------------------------------------------------------------
  def close(): Boolean = HDF5_DataSetSpace.close(link)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file HDF5_DataSetSpace.scala
//=============================================================================