/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  31/May/2023 
 * Time:  19h:33m
 * Description: None
 */
//=============================================================================
//=============================================================================
package com.common.hdf5.dataSet
//=============================================================================
import com.common.hdf5.HDF5_Object._
import com.common.logger.MyLogger
import hdf.hdf5lib.HDF5Constants
//=============================================================================
import hdf.hdf5lib.H5
import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}
//=============================================================================
object HDF5_DataSetFilter extends MyLogger {
  //---------------------------------------------------------------------------
  private final val FILTER_MAX_NAME_SIZE = 256
  //---------------------------------------------------------------------------
  def build(link: LINK_DATA_TYPE): Option[Array[HDF5_DataSetFilter]] = {
    val propertyListLink = getPropertyList(link).getOrElse(return None)
    val filterCount = getFilterCount(propertyListLink).getOrElse(return None)
    val filterSeq = ArrayBuffer[HDF5_DataSetFilter]()

    for (i <- 0 until filterCount) {
      val filter = getFilter(propertyListLink, i)
      if (filter.isEmpty) return None
    }
    Some(filterSeq.toArray)
  }
  //---------------------------------------------------------------------------
  private def close(link: LINK_DATA_TYPE): Boolean = {
    Try {
      H5.H5Pclose(link)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Pclose. Closed data set filter link:'$link'")
        true

      case Failure(e: Exception) => error(s"HDF5:H5.H5Pclose. Error closing data set filter link:'$link'")
        error(e.toString)
    }
  }
  //---------------------------------------------------------------------------
  private def getPropertyList(link: LINK_DATA_TYPE): Option[Long] = {
    var propertyList = -1L
    Try {
      propertyList = H5.H5Dget_create_plist(link)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Dget_create_plist. Got property list of link:'$link'")
        Some(propertyList)

      case Failure(e: Exception) => error(s"HDF5:H5.H5Dget_create_plist. Error getting property list of link:'$link'")
        error(e.toString)
        None
    }
  }

  //---------------------------------------------------------------------------
  private def getFilterCount(link: LINK_DATA_TYPE): Option[Int] = {
    var filterSeq = -1
    Try {
      filterSeq = H5.H5Pget_nfilters(link)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Pget_nfilters. Got number of filters of link:'$link'")
        Some(filterSeq)

      case Failure(e: Exception) => error(s"HDF5:H5.H5Pget_nfilters. Error getting number of filters of link:'$link'")
        error(e.toString)
        None
    }
  }

  //---------------------------------------------------------------------------
  private def getFilter(link: LINK_DATA_TYPE
                        , index: Int): Option[HDF5_DataSetFilter] = {
    var filter: HDF5_DataSetFilter = null
    Try {
      val flags           = Array(0)
      val elementCount    = Array(1L)
      val value           = Array(0)
      val name            = Array("")
      val configuration   = Array(0)
      val r = H5.H5Pget_filter(
          link
        , index
        , flags
        , elementCount
        , value
        , FILTER_MAX_NAME_SIZE
        , name
        , configuration)

      if  (r == HDF5Constants.H5Z_FILTER_ERROR) {
        error(s"HDF5:H5.H5Pget_filter. Error getting filter of link:'$link' index:'$index'")
        return None
      }
      val compressionInfo = getCompressionInfo(r,value(0))
      if (compressionInfo.isEmpty) {
        error(s"HDF5:H5.H5Pget_filter. Error getting compression info of filter of link:'$link' index:'$index'")
        return None
      }
      filter = HDF5_DataSetFilter(flags(0)
                                  , value(0)
                                  , name(0)
                                  , configuration(0)
                                  , compressionInfo)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Pget_filter. Got filter link:'$link' index:'$index'")
        Some(filter)

      case Failure(e: Exception) => error(s"HDF5:H5.H5Pget_filter. Error getting filter of link:'$link' index:'$index'")
        error(e.toString)
        None
    }
  }
  //---------------------------------------------------------------------------
  private def getCompressionInfo(filter: Int, value: Int) =
    filter match {
      case HDF5Constants.H5Z_FILTER_DEFLATE    => s"GZIP: level = $value"
      case HDF5Constants.H5Z_FILTER_FLETCHER32 => s""
      case HDF5Constants.H5Z_FILTER_SHUFFLE    => s"SHUFFLE: Nbytes = $value"
      case HDF5Constants.H5Z_FILTER_SZIP       => s"" //see https://github.com/saddle/jhdf5/blob/master/src/main/java/ncsa/hdf/object/h5/H5CompoundDS.java
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class HDF5_DataSetFilter(flags: Int
                             , value: Int
                             , name: String
                             , configuration: Int
                             , compressionInfo: String)
//=============================================================================
//End of file HDF5_DataSetFilter.scala
//=============================================================================