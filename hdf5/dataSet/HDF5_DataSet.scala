/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  30/May/2023 
 * Time:  21h:11m
 * Description: None
 */
//=============================================================================
//=============================================================================
package com.common.hdf5.dataSet
//=============================================================================
import com.common.csv.{CsvItem, CsvRow, CsvWrite}
import com.common.hdf5.HDF5_Object
import com.common.hdf5.HDF5_Object._
import com.common.hdf5.dataType.HDF5_DataType
import com.common.logger.MyLogger
//=============================================================================
import hdf.hdf5lib.{H5, HDF5Constants}
import scala.util.{Success, Failure, Try}
//=============================================================================
object HDF5_DataSet extends MyLogger {
  //---------------------------------------------------------------------------
  private final val sep   = "\t"
  //---------------------------------------------------------------------------
  def build(parentLink: LINK_DATA_TYPE
            , name: String
            , accessLink: LINK_DATA_TYPE = HDF5Constants.H5P_DEFAULT): Option[HDF5_DataSet] = {

    val link           = open(parentLink,name,accessLink).getOrElse(return None)
    val dt             = HDF5_DataType.build(link).getOrElse(return None)
    val space          = HDF5_DataSetSpace.build(link).getOrElse(return None)
    val dimensionCount = getDimensionCount(space.link).getOrElse(return None)
    val dimensionSeq   = getDimensionSeq(space.link,dimensionCount).getOrElse(return None)
    val filterSeq      = HDF5_DataSetFilter.build(link).getOrElse(return None)

    Some(HDF5_DataSet(link
                      , name
                      , dt
                      , space
                      , dimensionSeq
                      , filterSeq))
  }
  //---------------------------------------------------------------------------
  private def open(link: LINK_DATA_TYPE
                   , name: String
                   , accessLink: LINK_DATA_TYPE = HDF5Constants.H5P_DEFAULT): Option[LINK_DATA_TYPE] = {

    var newLink = -1L
    Try {
      newLink = H5.H5Dopen(
        link
        , name
        , accessLink)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Dopen. Link:'$link' with name:'$name' and access link:'$accessLink'")
        if (newLink < 0) {
          error(s"HDF5:H5.HH5Dopen5Gopen. Error opening link:'$link' with name:'$name' and access link:'$accessLink'")
          None
        }
        else {
          HDF5_DataType.getDataType(newLink).getOrElse(return None)
          Some(newLink)
        }

       case Failure(e: Exception) => error(s"HDF5:H5.H5Gopen. Error opening link:'$link' with name:'$name' and access link:'$accessLink'")
        error(e.toString)
        None
    }
  }
  //---------------------------------------------------------------------------
  private def close(link: LINK_DATA_TYPE, name:String): Boolean = {
    Try {
      H5.H5Dclose(link)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Dclose. Closed data set:'$name' link:'$link'")
        true

       case Failure(e: Exception)=> error(s"HDF5:H5.H5Dclose. Error closing data set:'$name' link:'$link'")
        error(e.toString)
    }
  }
  //---------------------------------------------------------------------------
  private def getDimensionCount(link: LINK_DATA_TYPE): Option[Int] = {
    var r: Int = -1
    Try {
      r = H5.H5Sget_simple_extent_ndims(link)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Sget_simple_extent_ndims. Got number of dimensions of link:'$link'")
                         Some(r)

       case Failure(e: Exception) => error(s"HDF5:H5.H5Sget_simple_extent_ndims. Error getting number of dimensions of link:'$link'")
                           error(e.toString)
                           None
    }
  }
  //---------------------------------------------------------------------------
  private def getDimensionSeq(link: LINK_DATA_TYPE
                              , dimensionCount: Int): Option[Array[Long]] = {
    val dimensionSeq  = Array.fill[Long](dimensionCount)(0L)
    Try {
      H5.H5Sget_simple_extent_dims(link,dimensionSeq, null)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Sget_simple_extent_dims. Got number of dimensions of link:'$link'")
        Some(dimensionSeq)

       case Failure(e: Exception) => error(s"HDF5:H5.H5Sget_simple_extent_dims. Error getting number of dimensions of link:'$link'")
        error(e.toString)
        None
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import HDF5_DataSet._
case class HDF5_DataSet(link: LINK_DATA_TYPE
                        , name: String
                        , dataType: HDF5_DataType
                        , space: HDF5_DataSetSpace
                        , dimensionSeq: Array[Long]
                        , filterSeq: Array[HDF5_DataSetFilter]) extends HDF5_Object {
  //---------------------------------------------------------------------------
  val itemCount = dimensionSeq.product.toInt
  val itemByteSize = itemCount * dataType.byteSize
  //---------------------------------------------------------------------------
  def close(): Boolean = {
    HDF5_DataSet.close(link,name)
    space.close()
    dataType.close()
  }
  //---------------------------------------------------------------------------
  def saveAsCsv(rootDir: String): Boolean = {
    info(s" Exporting HDF5 dataset:'$name' to csv")
    val csv = CsvWrite(s"$rootDir/$name.csv")
    val data = dataType.formatData(readData().getOrElse(return false))
    val csvRow = CsvRow()

    dimensionSeq.size match {
      case 1 =>
        csv.append(CsvRow(CsvItem("",data.mkString(sep))))
      case 2 =>
        val rowItemSize = dimensionSeq(1).toInt
        data.grouped(rowItemSize).foreach { seq =>
          csv.append(CsvRow(CsvItem("",seq.mkString(sep))))
        }
      case n => error(s"HDF5:saveAsCsv. Dimension:'$n' not supported yet to save as CSV")
    }
    csv.close()
    true
  }
  //---------------------------------------------------------------------------
  private def readData(): Option[Array[Byte]] = {
    val data =  Array.fill[Byte](itemByteSize)(0)
    Try {
      H5.H5Dread(
        link
        , dataType.link
        , HDF5Constants.H5S_ALL //mem_space_id
        , HDF5Constants.H5S_ALL //file_space_id
        , HDF5Constants.H5P_DEFAULT //xfer_plist_id
        , data)
    }
    match {
      case Success(_) => if (COMMAND_VERBOSE_ON_SUCCESS) info(s"HDF5:H5.H5Dread(readInt). Link:'$link' name:'$name'")
                         Some(data)

      case Failure(e: Exception) => error(s"HDF5:H5.H5Dread(readInt). Error reading Link:'$link' name:'$name'")
                                    error(e.toString)
                                    None

      case e: Throwable => error(s"HDF5:H5.H5Dget_type. Error getting data type of link:'$link'")
                           error(e.toString)
                           None
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file HDF5_DataSet.scala
//=============================================================================