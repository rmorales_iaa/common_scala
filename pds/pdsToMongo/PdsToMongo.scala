/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Jul/2021
 * Time:  13h:16m
 * Description: Import to mongo the field description and data of the pds
 * databases exported using the script "input/planetary_data_system/dump"
 */
//=============================================================================
package com.common.pds.pdsToMongo
//=============================================================================
import com.common.logger.MyLogger
import com.common.database.mongoDB.MongoDB
import com.henosis.database.MPO_DB
import org.mongodb.scala.bson.BsonValue
//=============================================================================
import org.apache.commons.csv.{CSVFormat, CSVParser}
import org.mongodb.scala.Document
import java.io.{BufferedReader, File, FileReader}
import scala.collection.JavaConverters.asScalaBufferConverter
import org.mongodb.scala.bson.{BsonDouble, BsonInt32, BsonString}
import org.mongodb.scala.MongoCollection
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object PdsToMongo extends MyLogger {
  //---------------------------------------------------------------------------
  private final val PDS_REAL    = "ASCII_Real"
  private final val PDS_STRING  = "ASCII_String"
  private final val PDS_INTEGER = "ASCII_Integer"
  //---------------------------------------------------------------------------
  def getFieldType(fileName: String) = {
    info(s"Processing pds file: '$fileName'")
    val br = new BufferedReader(new FileReader(new File(fileName)))
    val inputFormat = CSVFormat.DEFAULT

    //get the doc sequence
    val typeSeq = new CSVParser(br, inputFormat).getRecords.asScala flatMap { r =>
      if (r.getRecordNumber > 2){
        val s = r.toString.split(" ").last
        val t = s match {
          case _  if s.contains(PDS_REAL)    => PDS_REAL
          case _  if s.contains(PDS_STRING)  => PDS_STRING
          case _  if s.contains(PDS_INTEGER) => PDS_INTEGER
        }
        Some(t)
      }
      else None
    }
    typeSeq
  }
  //---------------------------------------------------------------------------
  private def processFile(fieldTypeFileName: String, dataFileName: String, db: MongoDB, coll: MongoCollection[Document]) = {
    db.dropCollection(coll)
    info(s"Processing pds file: '$dataFileName'")
    val typeSeq = getFieldType(fieldTypeFileName)
    val br = new BufferedReader(new FileReader(new File(dataFileName)))
    val inputFormat = CSVFormat.DEFAULT
    val headerSeq = ArrayBuffer[String]()
    val docSeq = new CSVParser(br, inputFormat).getRecords.asScala flatMap { r =>
      val i = r.getRecordNumber.toInt
      if (i == 1) {
        r.forEach{ s=> headerSeq += s }
        None
      }
      else {
        var k = 0
        val row = ArrayBuffer[(String,BsonValue)]()
        r.forEach { s =>
          val colName = headerSeq(k)
          val colType = typeSeq(k)

          colType match {
            case PDS_REAL    => row += (colName -> new BsonDouble(s.toDouble))
            case PDS_STRING  => row += (colName -> new BsonString(s))
            case PDS_INTEGER => row += (colName -> new BsonInt32(s.toInt))
          }
          k+=1
        }
       Some(Document(row))
      }
    }
    //insert all doc sequence
    db.insertBulk(docSeq.toArray, coll.namespace.getCollectionName)
  }
  //---------------------------------------------------------------------------
  def importData2Mass(fieldTypeFileName: String, dataFileName: String, db: MPO_DB) = {
    processFile(fieldTypeFileName
      , dataFileName
      , db
      , db.coll2MassMoc)

    info("Creating indexes")
    db.createIndex("AST_NUMBER", db.coll2MassMoc, _unique = false) //due to unnumbered mpo
    db.createIndex("AST_NAME", db.coll2MassMoc, _unique = false)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file PdsToMongo.scala
//=============================================================================
