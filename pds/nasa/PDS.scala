/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  29/May/2021
 * Time:  04h:25m
 * Description: Planetry Data System (PDS) scala wrapper for java API
 * https://pds.nasa.gov/
 * https://github.com/NASA-PDS/pds4-jparser/tree/main
 */
//=============================================================================
package com.common.pds.nasa
//=============================================================================
//=============================================================================
import com.common.logger.MyLogger
import com.common.util.path.Path
//=============================================================================
import gov.nasa.arc.pds.xml.generated.{ProductObservational}
import gov.nasa.pds.objectAccess.{ExporterFactory, ObjectAccess, TableReader}
import gov.nasa.pds.objectAccess.example.ExtractTable
import gov.nasa.pds.objectAccess.utility.Utility
import java.io.File
import java.net.URL
import scala.util.{Failure, Success, Try}
import scala.collection.mutable.ArrayBuffer
//=============================================================================
case class PDS(xmlURL: String) extends MyLogger {
  //---------------------------------------------------------------------------
  private val labelUrl = getURL()
  private val (objectAccess,product,parentURL) = getMainComponentSeq()
  //---------------------------------------------------------------------------
  //outputCsvFile must be in absolute path
  def dumpTable(outputCsvFile: String) = {
    val args = Array("-c", "-o", outputCsvFile, xmlURL)
    ExtractTable.main(args)
  }
  //---------------------------------------------------------------------------
  def parse() = {
     /* try {
        Utility.openConnection(xmlURL.openConnection).close()
      } catch {
        case io: IOException =>
          System.err.println("Cannot read label file " + xmlURL.toString)
          System.exit(1)
      }*/

  }
  //---------------------------------------------------------------------------
  private def getURL(): Option[URL] = {
    var url: URL = null
    Try {
      url = new URL(xmlURL).toURI.toURL
    }
    match {
      case Success(_) => openConnection(url)
      case Failure( _ ) =>
        Try {
          url = new File(xmlURL).toURI().toURL()
        }
        match {
          case Success(_) =>
            openConnection(url)
          case Failure(e) =>
            error(e.getMessage + s"Error parsing '$xmlURL'")
            error(e.getStackTrace.mkString("\n"))
            None
        }
    }
  }
  //---------------------------------------------------------------------------
  private def openConnection(url: URL) = {
    Try {
      Utility.openConnection(url.openConnection()).close();
    }
    match {
      case Success(_) => Some(url)
      case Failure(e) =>
        error(e.getMessage + s"Error opening the connection to '$xmlURL'")
        error(e.getStackTrace.mkString("\n"))
        None
    }
  }
  //---------------------------------------------------------------------------
  private def getMainComponentSeq(): (ObjectAccess, ProductObservational,URL) = {
    var objectAccess: ObjectAccess = null
    var product: ProductObservational = null
    var parentURL: URL = null

    if (!labelUrl.isEmpty) {
      Try {
        objectAccess = new ObjectAccess()
        product = objectAccess.getProduct(labelUrl.get, classOf[ProductObservational])
        parentURL = if (labelUrl.get.toURI.getPath.endsWith("/")) labelUrl.get.toURI.resolve("..").toURL
                    else labelUrl.get.toURI.resolve(".").toURL
        (product, parentURL)
      }
      match {
        case Success(_) =>
        case Failure(e) =>
          error(e.getMessage + s"Error getting product and parent URL from '$xmlURL'")
          error(e.getStackTrace.mkString("\n"))
      }
    }
    (objectAccess, product, parentURL)
  }
  //---------------------------------------------------------------------------
  private def buildDataFileURL (fileName: String) = {
    var dataFileURL: URL = null
    Try {
      dataFileURL = new URL(parentURL, fileName)
    }
    match {
      case Success(_) => Some(dataFileURL)
      case Failure(e) =>
        error(e.getMessage + s"Error building url from '$fileName'")
        error(e.getStackTrace.mkString("\n"))
        None
    }
  }
  //---------------------------------------------------------------------------
  private def getTableReader(obj: Object, dataFileURL: URL) = {
    var reader: TableReader = null
    Try {
      reader = ExporterFactory.getTableReader(obj, dataFileURL)
    }
    match {
      case Success(_) => Some(reader)
      case Failure(e) =>
        error(e.getMessage + s"Cannot create a table reader for the table: '$dataFileURL'")
        None
    }
  }
  //---------------------------------------------------------------------------
  private def getRowSeq(reader: TableReader, colCount: Int) = {
    var record = reader.readNext
    val rowSeq = ArrayBuffer[Array[String]]()
    Try {
      while (record != null) {
        val row = ArrayBuffer[String]()
        for(colIndex <-1 to colCount)
          row += record.getString(colIndex).trim
        rowSeq += row.toArray
        record = reader.readNext
      }
    }
    match {
      case Success(_) => Some(rowSeq.toArray)
      case Failure(e) =>
        error("Cannot read the next table record: " + e.getMessage)
        None
    }
  }
  //---------------------------------------------------------------------------
  private def getTable(tableReader:TableReader) = {
    val name = Path.getOnlyFilenameNoExtension(xmlURL)
    val fieldSeq = tableReader.getFields
    val colTypeSeq = ColumnType.build(fieldSeq)
    val rowSeq = getRowSeq(tableReader, fieldSeq.length)
    if (rowSeq.isEmpty) None
    else Some(Table(name,colTypeSeq,rowSeq.get))
  }
  //---------------------------------------------------------------------------
  def getTableSeq(): Array[Table] = {
    val tableSeq = ArrayBuffer[Table]()
    product.getFileAreaObservationals.forEach { fileArea =>
      val dataFileURL = buildDataFileURL(fileArea.getFile.getFileName).getOrElse(return Array())
      objectAccess.getTableObjects(fileArea).forEach { obj=>
        val tableReader = getTableReader(obj, dataFileURL).getOrElse(return Array())
        val table = getTable(tableReader)
        if (table.isDefined) tableSeq += table.get
        tableReader.close();
      }
    }
    tableSeq.toArray
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file PDS.scala
//=============================================================================
