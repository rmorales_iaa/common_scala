/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/Mar/2024
 * Time:  08h:56m
 * Description: None
 */
package com.common.pds.nasa
//=============================================================================
//=============================================================================
 case class Table(name: String
                   , header: Array[ColumnType]
                   , rowSeq: Array[Array[String]]) {
  //---------------------------------------------------------------------------
  def caseClassTableDefinition() = {
    var lineCount = 0
    val className = s"case class ${name}Source("
    val blankSeq = " " * className.length
    header.foreach { col =>
      val colName = col.pdsType.getName
      val typeName = col.getScalaTypeName()
      val formattedColName = f"$colName%-20s"

      val prefix = if (lineCount != 0) s"$blankSeq, " else s"$className\n$blankSeq"
      val line = s"$prefix$formattedColName:$typeName\n"
      lineCount += 1
      print(line)
    }
    println("                          )")
  }

  //---------------------------------------------------------------------------
  def caseClassImporterDefinition() = {
    var lineCount = 0
    val className = s"${name}Source("
    val blankSeq = " " * className.length
    header.foreach { col =>
      val colName = col.pdsType.getName
      val prefix = if (lineCount != 0) s"$blankSeq, " else s"$className\n$blankSeq"
      val suffix = col.getImportScalaTypeName()
      val line = s"${prefix}row($lineCount)$suffix //$colName\n"
      lineCount += 1
      print(line)
    }
    println("                          )")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Table.scala
//=============================================================================