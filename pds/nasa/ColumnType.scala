/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/Mar/2024
 * Time:  07h:35m
 * Description: None
 */
package com.common.pds.nasa
//=============================================================================
import com.common.logger.MyLogger
import gov.nasa.pds.label.`object`.FieldDescription
import org.mongodb.scala.bson.{BsonDouble, BsonInt32, BsonNumber, BsonString, BsonValue}
//=============================================================================
//=============================================================================
object ColumnType extends MyLogger {
  //---------------------------------------------------------------------------
  def apply(pdsType:FieldDescription) = {

    val bsonType = pdsType.getType.getXMLType.toUpperCase match {
      case "ASCII_REAL"     => BsonDouble(0)
      case "UTF8_STRING"    => BsonString("")
      case "ASCII_INTEGER"  => BsonInt32(0)
      case "ASCII_DATE_YMD" => BsonString("")
      case t =>
        error(s"Unknown PDS type: '$t'")
        BsonString("")
    }
    new ColumnType(pdsType,bsonType)
  }
  //---------------------------------------------------------------------------
  def build(pdsTypeSeq:Array[FieldDescription]) =
    pdsTypeSeq map (ColumnType( _ ))
  //---------------------------------------------------------------------------
}
//=============================================================================
case class ColumnType(pdsType:FieldDescription
                      , bsonType:BsonValue) {
  //---------------------------------------------------------------------------
  def getScalaTypeName() = {
    bsonType match {
      case _: BsonDouble => "Double"
      case _: BsonString => "String"
      case _: BsonInt32  => "Int"
    }
  }
  //---------------------------------------------------------------------------
  def getImportScalaTypeName() = {
    bsonType match {
      case _: BsonDouble => ".toDouble"
      case _: BsonString => ""
      case _: BsonInt32  => ".toInt"
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ColumnType.scala
//=============================================================================