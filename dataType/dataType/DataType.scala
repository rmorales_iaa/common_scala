package com.common.dataType.dataType
//=============================================================================
//=============================================================================
import scala.reflect.runtime.universe.{TypeTag, typeOf}
//=============================================================================
//=============================================================================
object DataType {
  //---------------------------------------------------------------------------
  type BooleanSeq = Seq[Boolean]
  type ByteSeq    = Seq[Byte]
  type ShortSeq   = Seq[Short]
  type IntSeq     = Seq[Int]
  type LongSeq    = Seq[Long]
  type FloatSeq   = Seq[Float]
  type DoubleSeq  = Seq[Double]
  //-------------------------------------------------------------------------
  def getDataTypeBitSize[T](v: T): Int = {
    v match {
      case _: Byte     => 8
      case _: Short    => 16
      case _: Int      => 32
      case _: Integer  => 32
      case _: Long     => 64
      case _: Float    => 32
      case _: Double   => 64
      case _ => printf("Error. Unknown type.getDataTypeBitSize::getDataTypeBitSize"); -1
    }
  }
  //---------------------------------------------------------------------------
  def valueSeqToDoubleSeq[T: TypeTag](l: Seq[T]): Seq[Double] =
    l match {
      case t if typeOf[Seq[T]] <:< typeOf[ByteSeq] => t.asInstanceOf[ByteSeq] map (_.toDouble)
      case t if typeOf[Seq[T]] <:< typeOf[ShortSeq] => t.asInstanceOf[ShortSeq] map (_.toDouble)
      case t if typeOf[Seq[T]] <:< typeOf[IntSeq] => t.asInstanceOf[IntSeq] map (_.toDouble)
      case t if typeOf[Seq[T]] <:< typeOf[LongSeq] => t.asInstanceOf[LongSeq] map (_.toDouble)
      case t if typeOf[Seq[T]] <:< typeOf[FloatSeq] => t.asInstanceOf[FloatSeq] map (_.toDouble)
      case t if typeOf[Seq[T]] <:< typeOf[DoubleSeq] => t.asInstanceOf[DoubleSeq]
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file DataType.scala
//=============================================================================