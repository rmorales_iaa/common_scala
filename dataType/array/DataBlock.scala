/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/May/2024
 * Time:  10h:21m
 * Description: None
 */
package com.common.dataType.array
//=============================================================================
import com.common.hardware.cpu.CPU
import com.common.util.parallelTask.ParallelTask
//=============================================================================
import java.nio.ByteOrder
import scala.reflect.ClassTag
import scala.collection.JavaConverters.collectionAsScalaIterableConverter
import java.util.concurrent.ConcurrentLinkedQueue
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object DataBlock {
  //---------------------------------------------------------------------------
  def apply[ITEM_DATA_TYPE](index: Long
                           , startPos: Long
                           , endPos: Long)
                          (implicit ct: ClassTag[ITEM_DATA_TYPE]
                          , num: Numeric[ITEM_DATA_TYPE]) : DataBlock[ITEM_DATA_TYPE] =
    DataBlock(Array(),index, startPos, endPos)
  //---------------------------------------------------------------------------
  def toByteSeq[T](b: DataBlock[T], byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN)
                  (implicit conv: DataTypeConversion[T], ct: ClassTag[T]): Array[Byte] =
    conv.toByteSeq(b.itemSeq, byteOrder)
  //---------------------------------------------------------------------------
  def toItemSeq[T: ClassTag](data: Array[Byte], byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN)
                             (implicit conv: DataTypeConversion[T]): Array[T] =
    conv.toItemSeq(data, byteOrder)
  //---------------------------------------------------------------------------
  def parallelProcessing[BT, QT: ClassTag]
                         (dataBlockSeq: Array[DataBlock[BT]]
                         , transf: DataBlock[BT] => QT
                         , verbose: Boolean = false
                         , message: Option[String] = None)
                         : Array[QT] = {
    //---------------------------------------------------------------------------
    val queue = new ConcurrentLinkedQueue[QT]()
    new ProcessParallelDataBlock(dataBlockSeq)
    //---------------------------------------------------------------------------
    class ProcessParallelDataBlock
    (dataBlockSeq: Array[DataBlock[BT]])
      extends ParallelTask[DataBlock[BT]](
        dataBlockSeq
        , CPU.getCoreCount()
        , isItemProcessingThreadSafe = true
        , randomStartMaxMsWait = 100
        , verbose = verbose
        , message = message) {
      //-------------------------------------------------------------------------
      def userProcessSingleItem(dataBLock: DataBlock[BT]): Unit =
        queue.add(transf(dataBLock))
      //-------------------------------------------------------------------------
    }
    queue.asScala.toArray
    //---------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def redistribute[ITEM_DATA_TYPE] (dataBlockSeq:  Array[DataBlock[ITEM_DATA_TYPE]]
                                    , newItemCountPerBlock: Int
                                    , itemByteSize: Int)
                                   (implicit ct: ClassTag[ITEM_DATA_TYPE]
                                           , num: Numeric[ITEM_DATA_TYPE])
                                   : Array[DataBlock[ITEM_DATA_TYPE]] = {
    //-------------------------------------------------------------------------
    val partialResultItemSeq = ArrayBuffer[ITEM_DATA_TYPE]()
    val resultBlockSeq = ArrayBuffer[DataBlock[ITEM_DATA_TYPE]]()
    var dataBlockIndex = 0L
    //-------------------------------------------------------------------------
    def addToBlockStorage(itemSeqToStore: Array[ITEM_DATA_TYPE]) = {
      resultBlockSeq += DataBlock(itemSeqToStore
                                  , dataBlockIndex
                                  , dataBlockIndex * itemByteSize * newItemCountPerBlock
                                  , (dataBlockIndex + 1) * itemByteSize * newItemCountPerBlock - 1)
      dataBlockIndex += 1
    }
    //-------------------------------------------------------------------------
    def addToStorage(itemSeqToStore: Array[ITEM_DATA_TYPE]) = {
      //group items in blocks
      val itemCountToStore = partialResultItemSeq.length + itemSeqToStore.length
      if (itemCountToStore == newItemCountPerBlock) {
        addToBlockStorage((partialResultItemSeq ++ itemSeqToStore).toArray)
        partialResultItemSeq.clear()
      }
      else {
        if (itemCountToStore > newItemCountPerBlock) {
          //get he number of complete blocks
          val completeBlockCount = itemCountToStore / newItemCountPerBlock

          //build a complete block using the partial result and new seq
          val itemCountToTake = newItemCountPerBlock - partialResultItemSeq.length
          partialResultItemSeq ++= itemSeqToStore.take(itemCountToTake)

          //add the block build with partial results and item to store
          addToBlockStorage(partialResultItemSeq.toArray)
          partialResultItemSeq.clear()

          //add complete block
          var dropItemCount = itemCountToTake
          for (_ <- 1 until completeBlockCount) {
            addToBlockStorage(itemSeqToStore
              .drop(dropItemCount)
              .take(newItemCountPerBlock))
            dropItemCount += newItemCountPerBlock
          }

          //store the new partial item
          partialResultItemSeq ++= itemSeqToStore.drop(dropItemCount)
        }
        else partialResultItemSeq ++= itemSeqToStore
      }
    }
    //-------------------------------------------------------------------------
    //add the items to the storage block by block
    dataBlockSeq.foreach { dataBlock => addToStorage(dataBlock.itemSeq)  }
    if (partialResultItemSeq.length > 0)
      addToBlockStorage(partialResultItemSeq.toArray)
    resultBlockSeq.toArray
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class DataBlock[ITEM_DATA_TYPE: ClassTag: Numeric]
   (itemSeq:Array[ITEM_DATA_TYPE]
    , index: Long = 0
    , startPos: Long = 0
    , endPos: Long = 0) {
  //---------------------------------------------------------------------------
  def transform(f: ITEM_DATA_TYPE => ITEM_DATA_TYPE): DataBlock[ITEM_DATA_TYPE] =
    DataBlock(itemSeq.map(f))
  //---------------------------------------------------------------------------
  def getItemCount: Long = itemSeq.length
  //---------------------------------------------------------------------------
  def getMinMax(implicit ord: Ordering[ITEM_DATA_TYPE])
                : (ITEM_DATA_TYPE, ITEM_DATA_TYPE) = {
    if (itemSeq.isEmpty) throw new NoSuchElementException("itemSeq is empty")
    var min = itemSeq(0)
    var max = itemSeq(0)
    for (item <- itemSeq) {
      min = ord.min(item,min)
      max = ord.max(item,min)
    }
    (min, max)
  }
  //---------------------------------------------------------------------------
  def applyLinearTransformation(bScale: Double, bZero: Double)
                               (implicit num: Numeric[ITEM_DATA_TYPE])
                               : DataBlock[Double] = {
    //optimize the operations to apply
    val scaledDataBlock =
      (bScale, bZero) match {
        case (1, 0) => itemSeq map ( item=> num.toDouble(item) )
        case (_, 0) => itemSeq map ( item=> num.toDouble(item) * bScale)
        case (1, _) => itemSeq map ( item=> num.toDouble(item) + bZero)
        case _      => itemSeq map ( item=> num.toDouble(item) * bScale + bZero)
    }
    DataBlock(scaledDataBlock,index, startPos, endPos)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file DataBlock.scala
//=============================================================================