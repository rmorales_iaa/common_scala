/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/May/2024
 * Time:  09h:50m
 * Description: None
 */
package com.common.dataType.array
//=============================================================================
import com.common.dataType.array.DataTypeConversion.longConversion
//=============================================================================
import java.nio.ByteOrder
import scala.language.implicitConversions
//=============================================================================
//=============================================================================
object NSA_Long {
  //---------------------------------------------------------------------------
  type ITEM_DATA_TYPE                = Long
  final val ITEM_DATA_TYPE_BYTE_SIZE = 4
  //---------------------------------------------------------------------------
  implicit val numericImpl: Numeric[ITEM_DATA_TYPE] = Numeric.LongIsIntegral
  implicit val orderingImpl: Ordering[ITEM_DATA_TYPE] = Ordering[ITEM_DATA_TYPE]
  //---------------------------------------------------------------------------
  def apply(nsaFitsScaled: NSA_Double, blockByteSize: Int): NSA_Long = {
    val dataBlockSeq = NSA_Long(blockByteSize = blockByteSize).fromDouble(nsaFitsScaled.dataBlockSeq)
    val redistributedDataBlockSeq = DataBlock.redistribute(dataBlockSeq
                                                           , blockByteSize / ITEM_DATA_TYPE_BYTE_SIZE
                                                           , ITEM_DATA_TYPE_BYTE_SIZE)
    NSA_Long(redistributedDataBlockSeq, blockByteSize)
  }
  //---------------------------------------------------------------------------
  def apply(data: Array[Array[Byte]], blockByteSize: Int): NSA_Long =
    NSA_Long(data map (seq => longConversion.toItemSeq(seq)), blockByteSize)
  //---------------------------------------------------------------------------
  def apply(data: Array[Array[ITEM_DATA_TYPE]], blockByteSize: Int): NSA_Long =
    NSA_Long(data map (DataBlock(_)), blockByteSize)
  //---------------------------------------------------------------------------
  def apply(data: Array[ITEM_DATA_TYPE], blockByteSize: Int): NSA_Long = {
    val blockSeq = (data.grouped(blockByteSize / ITEM_DATA_TYPE_BYTE_SIZE).zipWithIndex map {
      case (itemSeq, i) => DataBlock(
        itemSeq
        , i
        , i * blockByteSize
        , (i + 1) * blockByteSize)
    }).toArray
    NSA_Long(blockSeq,blockByteSize)
  }
  //---------------------------------------------------------------------------
  def build(blockByteSize: Int): NSA_Long =
    NSA_Long(Array(DataBlock(0 ,0 ,0)), blockByteSize)
  //---------------------------------------------------------------------------
}
//=============================================================================
import NSA_Long._
case class NSA_Long(dataBlockSeq: Array[DataBlock[ITEM_DATA_TYPE]] = Array()
                    , blockByteSize: Int) extends NonSequentialArray[ITEM_DATA_TYPE] {
  //---------------------------------------------------------------------------
  implicit val numericImpl = NSA_Long.numericImpl
  implicit val orderingImpl= NSA_Long.orderingImpl
  //---------------------------------------------------------------------------
  implicit def fromDoubleImpl(v:Double): ITEM_DATA_TYPE = Math.round(v)
  implicit def toDoubleImpl(v:ITEM_DATA_TYPE) = v.toDouble
  //---------------------------------------------------------------------------
  val itemByteSize = ITEM_DATA_TYPE_BYTE_SIZE
  assert((blockByteSize % itemByteSize) == 0, s"The 'blockByteSize': '$blockByteSize' must be a multiple of 'itemByteSize': '$itemByteSize'")
  //---------------------------------------------------------------------------
  def toByteSeq(byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Array[Array[Byte]] =
    dataBlockSeq.map { block =>
      longConversion.toByteSeq(block.itemSeq, byteOrder)
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file NSA_Long.scala
//=============================================================================