/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/May/2024
 * Time:  09h:46m
 * Description: Non sequential array. Use to store large amount of data in
 * a non sequential storage.
 * The 'blockByteSize' must be a multiple of 'itemByteSize'
 * The max allowed byte size is about 4.6TiB: +2147483647 Arrays of +2147483647 bytes
 * If more than the max  is required, change the definition of 'blockStorage' as
 * 'collection.immutable.ListMap' that  preserves insertion order and propagate
 * the changes
 */
package com.common.dataType.array
//=============================================================================
import com.common.dataType.array.DataBlock.parallelProcessing
//=============================================================================
import java.nio.ByteOrder
import scala.collection.mutable.ArrayBuffer
import scala.reflect.ClassTag
import scala.language.implicitConversions
//=============================================================================
//=============================================================================
trait NonSequentialArray[ITEM_DATA_TYPE] {
  //---------------------------------------------------------------------------
  val blockByteSize: Int
  val itemByteSize: Int
  //---------------------------------------------------------------------------
  val dataBlockSeq: Array[DataBlock[ITEM_DATA_TYPE]]
  //---------------------------------------------------------------------------
  implicit val numericImpl: Numeric[_]
  implicit val orderingImpl: Ordering[ITEM_DATA_TYPE]
  //---------------------------------------------------------------------------
  implicit def fromDoubleImpl(v:Double): ITEM_DATA_TYPE
  implicit def toDoubleImpl(v:ITEM_DATA_TYPE): Double
  //---------------------------------------------------------------------------
  def toByteSeq(byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Array[Array[Byte]]
  //---------------------------------------------------------------------------
  def toItemSeq()
        (implicit  ct: ClassTag[ITEM_DATA_TYPE])
        : Array[Array[ITEM_DATA_TYPE]] =
    dataBlockSeq.map { block=> block.itemSeq}

  //---------------------------------------------------------------------------
  def collect()
      (implicit  ct: ClassTag[ITEM_DATA_TYPE])
      = (dataBlockSeq map (_.itemSeq)).flatten
  //---------------------------------------------------------------------------
  def collectAsDouble(): Array[Double] =
    dataBlockSeq.flatMap(_.itemSeq.map(toDoubleImpl(_)))
  //---------------------------------------------------------------------------
  def getItemCount:Long =  (dataBlockSeq map (_.getItemCount)).sum
  //---------------------------------------------------------------------------
  private def getItemPerBlock = blockByteSize / itemByteSize
  //---------------------------------------------------------------------------
  def getBlockIndex(pos: Long) =
    ( (pos / getItemPerBlock).toInt, (pos % getItemPerBlock).toInt)
  //---------------------------------------------------------------------------
  def transform(f: ITEM_DATA_TYPE => ITEM_DATA_TYPE) =
    dataBlockSeq map ( _.transform(f) )
  //---------------------------------------------------------------------------
  private def getBlockSeqWithItemIndexRange(minItemIndex: Long, maxItemIndex: Long): Array[DataBlock[ITEM_DATA_TYPE]]= {
    val resultBlockSeq = ArrayBuffer[DataBlock[ITEM_DATA_TYPE]]()
    for(block <- dataBlockSeq) {
      if ((block.startPos >= minItemIndex) && (block.endPos <= maxItemIndex)) resultBlockSeq += block
      if (block.endPos > minItemIndex) return resultBlockSeq.toArray
    }
    resultBlockSeq.toArray
  }
  //---------------------------------------------------------------------------
  private def getBlock(pos: Long): Option[DataBlock[ITEM_DATA_TYPE]] =
    dataBlockSeq.find(block => block.startPos <= pos && block.endPos >= pos)
  //---------------------------------------------------------------------------
  //it is equivalent to add all items of a row
  private def addItemSeqOfSingleRow(blockIndex: Int
                                    , _skipItemCount: Int
                                    , rowItemCount: Int
                                    , partialResultItemSeq: ArrayBuffer[ITEM_DATA_TYPE]
                                    , resultBlockSeq: ArrayBuffer[DataBlock[ITEM_DATA_TYPE]])
                                   (implicit ct: ClassTag[ITEM_DATA_TYPE]
                                          , num: Numeric[ITEM_DATA_TYPE]): Unit = {
    //-------------------------------------------------------------------------
    val itemPerBlock = getItemPerBlock
    var remainItemCount = rowItemCount
    var skipItemCount = _skipItemCount
    //-------------------------------------------------------------------------
    //group items in blocks
    def addToStorage(itemSeqToStore: Array[ITEM_DATA_TYPE]): Unit = {
      val itemCountToStore = partialResultItemSeq.length + itemSeqToStore.length
      if(itemCountToStore < itemPerBlock) partialResultItemSeq ++= itemSeqToStore
      else {
        val itemCountToTake = itemPerBlock - partialResultItemSeq.length
        val blockIndex = resultBlockSeq.length
        val startPos = blockIndex * itemByteSize * itemPerBlock
        val endPos =  ((blockIndex + 1) * itemByteSize * itemPerBlock) - 1
        resultBlockSeq += DataBlock((partialResultItemSeq ++ itemSeqToStore.take(itemCountToTake)).toArray
                                    , blockIndex
                                    , startPos
                                    , endPos)
        partialResultItemSeq.clear()
        partialResultItemSeq ++= itemSeqToStore.drop(itemCountToTake)
      }
    }
    //-------------------------------------------------------------------------
    //add the items to the storage block by block
    for (i<-blockIndex to dataBlockSeq.length-1)  {

      //calculate the number of items to take in the block
      val itemCountToTakeInThisBlock = Math.min(itemPerBlock - skipItemCount, remainItemCount)
      val itemSeqToStore = dataBlockSeq(i)
                           .itemSeq
                           .drop(skipItemCount)
                           .take(itemCountToTakeInThisBlock)

      //add to Storage
      addToStorage(itemSeqToStore)

      //check if all items has been stored
      remainItemCount -= itemCountToTakeInThisBlock
      if (remainItemCount  == 0) return

      skipItemCount = 0 //use all items in next block
    }
  }
  //---------------------------------------------------------------------------
  def getRectangle(xAxisMaxItem: Long
                   , yAxisMaxItem: Long
                   , xMin: Long
                   , yMin: Long
                   , xMax: Long
                   , yMax: Long)
                   (implicit ct: ClassTag[ITEM_DATA_TYPE]
                    , num: Numeric[ITEM_DATA_TYPE]):
                   Option[Array[DataBlock[ITEM_DATA_TYPE]]] = {
    //---------------------------------------------------------------------------
    if (xMin < 0 || yMin < 0 || xMax < xMin || yMax < yMin) return None
    val xAxisRange = (0, xAxisMaxItem-1)
    val yAxisRange = (0, yAxisMaxItem-1)

    if (xMin < xAxisRange._1 || yMin < yAxisRange._1 ||
        xMax > xAxisRange._2 || yMax > yAxisRange._2) return None

    //process all rows
    val rowItemCount = (xMax - xMin + 1).toInt
    val partialResultItemSeq = ArrayBuffer[ITEM_DATA_TYPE]()
    val resultBlockSeq = ArrayBuffer[DataBlock[ITEM_DATA_TYPE]]()
    for(y<-yMin to yMax) {
      val (blockIndex,itemOffset) = getBlockIndex((y * xAxisMaxItem) + xMin)
      addItemSeqOfSingleRow(blockIndex
                            , itemOffset
                            , rowItemCount
                            , partialResultItemSeq
                            , resultBlockSeq)
    }

    //add to storage the remain items
    if (partialResultItemSeq.length > 0) {
      val itemPerBlock = getItemPerBlock
      val blockIndex = resultBlockSeq.length
      val startPos = blockIndex * itemByteSize * itemPerBlock
      val endPos = ((blockIndex + 1) * itemByteSize * itemPerBlock) - 1
      resultBlockSeq += DataBlock(partialResultItemSeq.toArray
                                  , blockIndex
                                  , startPos
                                  , endPos)
    }
    Some(resultBlockSeq.toArray)
  }
  //---------------------------------------------------------------------------
  def getMinMax(): (ITEM_DATA_TYPE, ITEM_DATA_TYPE) = {
    //-------------------------------------------------------------------------
    val ord = orderingImpl.asInstanceOf[Ordering[ITEM_DATA_TYPE]]
    //-------------------------------------------------------------------------
    def processSingleBlock(dataBlock: DataBlock[ITEM_DATA_TYPE]) =
      dataBlock.getMinMax(ord)
    //-------------------------------------------------------------------------
    val partialMinMax = parallelProcessing(dataBlockSeq
                                          , processSingleBlock
                                          , message = Some("calculating min and max pixels"))
    val (min, max) = partialMinMax.reduceLeft { (acc, elem) =>
      (ord.min(acc._1, elem._1), ord.max(acc._2, elem._2))
    }
    (min, max)
  }
  //---------------------------------------------------------------------------
  def applyLinearTransformation[T: ClassTag: Numeric]
                                (bScale: Double
                                , bZero: Double
                                , targetType: NonSequentialArray[T])
                                : Array[DataBlock[T]] = {
    //-------------------------------------------------------------------------
    def processSingleBlock(dataBlock: DataBlock[ITEM_DATA_TYPE]): DataBlock[T] = {
      val r = dataBlock.applyLinearTransformation(bScale, bZero)(numericImpl.asInstanceOf[Numeric[ITEM_DATA_TYPE]])
      DataBlock(r.itemSeq.map(targetType.fromDoubleImpl(_))
        , dataBlock.index
        , dataBlock.startPos
        , dataBlock.endPos)
    }
    //-------------------------------------------------------------------------
    parallelProcessing(dataBlockSeq
                      , processSingleBlock
                      , message = Some(s"applying linear trasformation and converting to '${targetType.getClass.getName}'"))
  }
  //---------------------------------------------------------------------------
  def fromDouble(inputBlockSeq: Array[DataBlock[Double]])
                (implicit ct: ClassTag[ITEM_DATA_TYPE]
                , num: Numeric[ITEM_DATA_TYPE])
  : Array[DataBlock[ITEM_DATA_TYPE]] = {
    //-------------------------------------------------------------------------
    def processSingleBlock(dataBlock: DataBlock[Double]): DataBlock[ITEM_DATA_TYPE] = {
      DataBlock(dataBlock.itemSeq.map(fromDoubleImpl(_))
                , dataBlock.index
                , dataBlock.startPos
                , dataBlock.endPos)
    }
    //-------------------------------------------------------------------------
    parallelProcessing(inputBlockSeq
                       , processSingleBlock
                       , message = Some("converting to double"))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file NonSequentialArray.scala
//=============================================================================