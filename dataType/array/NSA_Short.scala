/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/May/2024
 * Time:  09h:50m
 * Description: None
 */
package com.common.dataType.array
//=============================================================================
import com.common.dataType.array.DataTypeConversion.shortConversion
//=============================================================================
import scala.language.implicitConversions
import java.nio.ByteOrder
//=============================================================================
//=============================================================================
object NSA_Short {
  //---------------------------------------------------------------------------
  type ITEM_DATA_TYPE                = Short
  final val ITEM_DATA_TYPE_BYTE_SIZE = 2
  //---------------------------------------------------------------------------
  implicit val numericImpl: Numeric[ITEM_DATA_TYPE] = Numeric.ShortIsIntegral
  implicit val orderingImpl: Ordering[ITEM_DATA_TYPE] = Ordering[ITEM_DATA_TYPE]
  //---------------------------------------------------------------------------
  def apply(nsaFitsScaled: NSA_Double, blockByteSize: Int): NSA_Short = {
    val dataBlockSeq = NSA_Short(blockByteSize = blockByteSize).fromDouble(nsaFitsScaled.dataBlockSeq)
    val redistributedDataBlockSeq = DataBlock.redistribute(dataBlockSeq
                                                           , blockByteSize / ITEM_DATA_TYPE_BYTE_SIZE
                                                           , ITEM_DATA_TYPE_BYTE_SIZE)
    NSA_Short(redistributedDataBlockSeq, blockByteSize)
  }
  //---------------------------------------------------------------------------
  def apply(data: Array[Array[Byte]], blockByteSize: Int): NSA_Short =
    NSA_Short(data map (seq => shortConversion.toItemSeq(seq)), blockByteSize)
  //---------------------------------------------------------------------------
  def apply(data: Array[Array[ITEM_DATA_TYPE]], blockByteSize: Int): NSA_Short =
    NSA_Short(data map (DataBlock(_)), blockByteSize)
  //---------------------------------------------------------------------------
  def apply(data: Array[ITEM_DATA_TYPE], blockByteSize: Int): NSA_Short = {
    val blockSeq = (data.grouped(blockByteSize / ITEM_DATA_TYPE_BYTE_SIZE).zipWithIndex map {
      case (itemSeq, i) => DataBlock(
        itemSeq
        , i
        , i * blockByteSize
        , (i + 1) * blockByteSize)
    }).toArray
    NSA_Short(blockSeq, blockByteSize)
  }
  //---------------------------------------------------------------------------
  def build(blockByteSize: Int): NSA_Short =
    NSA_Short(Array(DataBlock(0 ,0 ,0)), blockByteSize)
  //---------------------------------------------------------------------------
}
//=============================================================================
import NSA_Short._
case class NSA_Short(dataBlockSeq: Array[DataBlock[ITEM_DATA_TYPE]] = Array()
                     , blockByteSize: Int) extends NonSequentialArray[ITEM_DATA_TYPE] {
  //---------------------------------------------------------------------------
  implicit val numericImpl = NSA_Short.numericImpl
  implicit val orderingImpl= NSA_Short.orderingImpl
  //---------------------------------------------------------------------------
  implicit def fromDoubleImpl(v:Double): ITEM_DATA_TYPE = Math.round(v).toShort
  implicit def toDoubleImpl(v:ITEM_DATA_TYPE) = v.toDouble
  //---------------------------------------------------------------------------
  val itemByteSize = ITEM_DATA_TYPE_BYTE_SIZE
  assert((blockByteSize % itemByteSize) == 0, s"The 'blockByteSize': '$blockByteSize' must be a multiple of 'itemByteSize': '$itemByteSize'")
  //---------------------------------------------------------------------------
  def toByteSeq(byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Array[Array[Byte]] =
    dataBlockSeq.map { block =>
      shortConversion.toByteSeq(block.itemSeq, byteOrder)
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file NSA_Short.scala
//=============================================================================