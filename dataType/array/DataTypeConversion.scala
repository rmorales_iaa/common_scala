/*
* Created by: Rafael Morales (rmorales@iaa.es)
* Date:  22/May/2024
* Time:  11h:23m
  * Description: None
*/
package com.common.dataType.array
//=============================================================================
import java.nio.{ByteBuffer, ByteOrder}
//=============================================================================
//=============================================================================
trait DataTypeConversion[T] {
  //---------------------------------------------------------------------------
  implicit def toItemSeq(data: Array[Byte], byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Array[T]
  //---------------------------------------------------------------------------
  implicit def toByteSeq(data: Array[T], byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Array[Byte]
  //---------------------------------------------------------------------------
}
//=============================================================================
object DataTypeConversion {
  //---------------------------------------------------------------------------
  implicit val byteConversion: DataTypeConversion[Byte] = new DataTypeConversion[Byte] {
    //-------------------------------------------------------------------------
    implicit def toItemSeq(data: Array[Byte], byteOrder: ByteOrder): Array[Byte] = data
    //-------------------------------------------------------------------------
    implicit def toByteSeq(data: Array[Byte], byteOrder: ByteOrder): Array[Byte] = data
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  implicit val shortConversion: DataTypeConversion[Short] = new DataTypeConversion[Short] {
    //-------------------------------------------------------------------------
    implicit def toItemSeq(data: Array[Byte], byteOrder: ByteOrder): Array[Short] = {
      require(data.length % 2 == 0, "Byte array length must be a multiple of 2 for Short conversion.")
      val bb = ByteBuffer.wrap(data).order(byteOrder)
      val result = new Array[Short](data.length / 2)
      bb.asShortBuffer().get(result)
      result
    }
    //-------------------------------------------------------------------------
    implicit def toByteSeq(data: Array[Short], byteOrder: ByteOrder): Array[Byte] = {
      val bb = ByteBuffer.allocate(data.length * 2).order(byteOrder)
      bb.asShortBuffer().put(data)
      bb.array()
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  implicit val intConversion: DataTypeConversion[Int] = new DataTypeConversion[Int] {
    //-------------------------------------------------------------------------
    implicit def toItemSeq(data: Array[Byte], byteOrder: ByteOrder): Array[Int] = {
      require(data.length % 4 == 0, "Byte array length must be a multiple of 4 for Int conversion.")
      val bb = ByteBuffer.wrap(data).order(byteOrder)
      val result = new Array[Int](data.length / 4)
      bb.asIntBuffer().get(result)
      result
    }
    //-------------------------------------------------------------------------
    implicit def toByteSeq(data: Array[Int], byteOrder: ByteOrder): Array[Byte] = {
      val bb = ByteBuffer.allocate(data.length * 4).order(byteOrder)
      bb.asIntBuffer().put(data)
      bb.array()
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  implicit val longConversion: DataTypeConversion[Long] = new DataTypeConversion[Long] {
    //-------------------------------------------------------------------------
    implicit def toItemSeq(data: Array[Byte], byteOrder: ByteOrder): Array[Long] = {
      require(data.length % 8 == 0, "Byte array length must be a multiple of 8 for Long conversion.")
      val bb = ByteBuffer.wrap(data).order(byteOrder)
      val result = new Array[Long](data.length / 8)
      bb.asLongBuffer().get(result)
      result
    }
    //-------------------------------------------------------------------------
    implicit def toByteSeq(data: Array[Long], byteOrder: ByteOrder): Array[Byte] = {
      val bb = ByteBuffer.allocate(data.length * 8).order(byteOrder)
      bb.asLongBuffer().put(data)
      bb.array()
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  implicit val floatConversion: DataTypeConversion[Float] = new DataTypeConversion[Float] {
    //-------------------------------------------------------------------------
    implicit def toItemSeq(data: Array[Byte], byteOrder: ByteOrder): Array[Float] = {
      require(data.length % 4 == 0, "Byte array length must be a multiple of 4 for Float conversion.")
      val bb = ByteBuffer.wrap(data).order(byteOrder)
      val result = new Array[Float](data.length / 4)
      bb.asFloatBuffer().get(result)
      result
    }
    //-------------------------------------------------------------------------
    implicit def toByteSeq(data: Array[Float], byteOrder: ByteOrder): Array[Byte] = {
      val bb = ByteBuffer.allocate(data.length * 4).order(byteOrder)
      bb.asFloatBuffer().put(data)
      bb.array()
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  implicit val doubleConversion: DataTypeConversion[Double] = new DataTypeConversion[Double] {
    //-------------------------------------------------------------------------
    implicit def toItemSeq(data: Array[Byte], byteOrder: ByteOrder): Array[Double] = {
      require(data.length % 8 == 0, "Byte array length must be a multiple of 8 for Double conversion.")
      val bb = ByteBuffer.wrap(data).order(byteOrder)
      val result = new Array[Double](data.length / 8)
      bb.asDoubleBuffer().get(result)
      result
    }
    //-------------------------------------------------------------------------
    implicit def toByteSeq(data: Array[Double], byteOrder: ByteOrder): Array[Byte] = {
      val bb = ByteBuffer.allocate(data.length * 8).order(byteOrder)
      bb.asDoubleBuffer().put(data)
      bb.array()
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file DataTypeConversion.scala
//=============================================================================