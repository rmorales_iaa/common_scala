/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/May/2024
 * Time:  09h:50m
 * Description: None
 */
package com.common.dataType.array
//=============================================================================
import com.common.dataType.array.DataTypeConversion.doubleConversion
//=============================================================================
import java.nio.ByteOrder
import scala.language.implicitConversions
//=============================================================================
//=============================================================================
object NSA_Double {
  //---------------------------------------------------------------------------
  type ITEM_DATA_TYPE                = Double
  final val ITEM_DATA_TYPE_BYTE_SIZE = 8
  //---------------------------------------------------------------------------
  implicit val numericImpl: Numeric[ITEM_DATA_TYPE] = Numeric.DoubleIsFractional
  implicit val orderingImpl: Ordering[ITEM_DATA_TYPE] = Ordering[ITEM_DATA_TYPE]
  implicit def toArrayImpl(v: Array[ITEM_DATA_TYPE]) = v
  //---------------------------------------------------------------------------
  def apply(data: Array[Array[Byte]], blockByteSize: Int): NSA_Double =
    NSA_Double(data map (seq => doubleConversion.toItemSeq(seq)), blockByteSize)
  //---------------------------------------------------------------------------
  def apply(data: Array[Array[ITEM_DATA_TYPE]], blockByteSize: Int): NSA_Double =
    NSA_Double(data map (DataBlock(_)), blockByteSize)
  //---------------------------------------------------------------------------
  def apply(data: Array[ITEM_DATA_TYPE], blockByteSize: Int): NSA_Double = {
    val blockSeq = (data.grouped(blockByteSize / ITEM_DATA_TYPE_BYTE_SIZE).zipWithIndex map {
      case (itemSeq, i) => DataBlock(
        itemSeq
        , i
        , i * blockByteSize
        , (i + 1) * blockByteSize)
    }).toArray
    NSA_Double(blockSeq, blockByteSize)
  }
  //---------------------------------------------------------------------------
  def build(blockByteSize: Int): NSA_Double =
    NSA_Double(Array(DataBlock(0 ,0 ,0)), blockByteSize)
  //---------------------------------------------------------------------------
}
//=============================================================================
import NSA_Double._
case class NSA_Double(dataBlockSeq: Array[DataBlock[ITEM_DATA_TYPE]] = Array()
                      , blockByteSize: Int) extends NonSequentialArray[ITEM_DATA_TYPE] {
  //---------------------------------------------------------------------------
  implicit val numericImpl = NSA_Double.numericImpl
  implicit val orderingImpl= NSA_Double.orderingImpl
  //---------------------------------------------------------------------------
  implicit def fromDoubleImpl(v:Double): ITEM_DATA_TYPE = v
  implicit def toDoubleImpl(v:ITEM_DATA_TYPE) = v
  //---------------------------------------------------------------------------
  val itemByteSize = ITEM_DATA_TYPE_BYTE_SIZE
  assert((blockByteSize % itemByteSize) == 0, s"The 'blockByteSize': '$blockByteSize' must be a multiple of 'itemByteSize': '$itemByteSize'")
  //---------------------------------------------------------------------------
  def toByteSeq(byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Array[Array[Byte]] =
    dataBlockSeq.map { block =>
      doubleConversion.toByteSeq(block.itemSeq, byteOrder)
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file NSA_Double.scala
//=============================================================================