/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/May/2024
 * Time:  09h:50m
 * Description: None
 */
package com.common.dataType.array
//=============================================================================
import com.common.dataType.array.DataTypeConversion.floatConversion
//=============================================================================
import java.nio.ByteOrder
import scala.language.implicitConversions
//=============================================================================
//=============================================================================
object NSA_Float {
  //---------------------------------------------------------------------------
  type ITEM_DATA_TYPE                = Float
  final val ITEM_DATA_TYPE_BYTE_SIZE = 4
  //---------------------------------------------------------------------------
  implicit val numericImpl: Numeric[ITEM_DATA_TYPE] = Numeric.FloatIsFractional
  implicit val orderingImpl: Ordering[ITEM_DATA_TYPE] = Ordering[ITEM_DATA_TYPE]
  //---------------------------------------------------------------------------
  def apply(nsaFitsScaled: NSA_Double, blockByteSize: Int): NSA_Float = {
    val dataBlockSeq = NSA_Float(blockByteSize = blockByteSize).fromDouble(nsaFitsScaled.dataBlockSeq)
    val redistributedDataBlockSeq = DataBlock.redistribute(dataBlockSeq
                                                           , blockByteSize / ITEM_DATA_TYPE_BYTE_SIZE
                                                           , ITEM_DATA_TYPE_BYTE_SIZE)
    NSA_Float(redistributedDataBlockSeq, blockByteSize)
  }
  //---------------------------------------------------------------------------
  def apply(data: Array[Array[Byte]], blockByteSize: Int): NSA_Float =
    NSA_Float(data map (seq => floatConversion.toItemSeq(seq)), blockByteSize)
  //---------------------------------------------------------------------------
  def apply(data: Array[Array[ITEM_DATA_TYPE]], blockByteSize: Int): NSA_Float =
    NSA_Float(data map (DataBlock(_)), blockByteSize)
  //---------------------------------------------------------------------------
  def apply(data: Array[ITEM_DATA_TYPE], blockByteSize: Int): NSA_Float = {
    val blockSeq = (data.grouped(blockByteSize / ITEM_DATA_TYPE_BYTE_SIZE).zipWithIndex map {
      case (itemSeq, i) => DataBlock(
        itemSeq
        , i
        , i * blockByteSize
        , (i + 1) * blockByteSize)
    }).toArray
    NSA_Float(blockSeq,blockByteSize)
  }
  //---------------------------------------------------------------------------
  def build(blockByteSize: Int): NSA_Float =
    NSA_Float(Array(DataBlock(0 ,0 ,0)), blockByteSize)
  //---------------------------------------------------------------------------
}
//=============================================================================
import NSA_Float._
case class NSA_Float(dataBlockSeq: Array[DataBlock[ITEM_DATA_TYPE]] = Array()
                     , blockByteSize: Int) extends NonSequentialArray[ITEM_DATA_TYPE] {
  //---------------------------------------------------------------------------
  implicit val numericImpl = NSA_Float.numericImpl
  implicit val orderingImpl= NSA_Float.orderingImpl
  //---------------------------------------------------------------------------
  implicit def fromDoubleImpl(v:Double): ITEM_DATA_TYPE = v.toFloat
  implicit def toDoubleImpl(v:ITEM_DATA_TYPE) = v.toDouble
  //---------------------------------------------------------------------------
  val itemByteSize = ITEM_DATA_TYPE_BYTE_SIZE
  assert((blockByteSize % itemByteSize) == 0, s"The 'blockByteSize': '$blockByteSize' must be a multiple of 'itemByteSize': '$itemByteSize'")
  //---------------------------------------------------------------------------
  def toByteSeq(byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Array[Array[Byte]] =
    dataBlockSeq.map { block =>
      floatConversion.toByteSeq(block.itemSeq, byteOrder)
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file NSA_Float.scala
//=============================================================================