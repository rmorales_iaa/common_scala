/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/May/2024
 * Time:  09h:50m
 * Description: None
 */
package com.common.dataType.array
//=============================================================================
import com.common.dataType.array.DataTypeConversion.byteConversion
import com.common.dataType.array.NSA_Byte.ITEM_DATA_TYPE
//=============================================================================
import java.nio.ByteOrder
import scala.language.implicitConversions
//=============================================================================
//=============================================================================
object NSA_Byte {
  //---------------------------------------------------------------------------
  type ITEM_DATA_TYPE                = Byte
  final val ITEM_DATA_TYPE_BYTE_SIZE = 1
  //---------------------------------------------------------------------------
  implicit val numericImpl: Numeric[ITEM_DATA_TYPE] = Numeric.ByteIsIntegral
  implicit val orderingImpl: Ordering[ITEM_DATA_TYPE] = Ordering[ITEM_DATA_TYPE]
  //---------------------------------------------------------------------------
  def apply(nsaFitsScaled: NSA_Double, blockByteSize: Int): NSA_Byte = {
    val dataBlockSeq = NSA_Byte(blockByteSize = blockByteSize).fromDouble(nsaFitsScaled.dataBlockSeq)
    val redistributedDataBlockSeq = DataBlock.redistribute(dataBlockSeq
                                                           , blockByteSize / ITEM_DATA_TYPE_BYTE_SIZE
                                                           , ITEM_DATA_TYPE_BYTE_SIZE)
    NSA_Byte(redistributedDataBlockSeq, blockByteSize)
  }
  //---------------------------------------------------------------------------
  def apply(data: Array[Array[Byte]]
            , blockByteSize: Int): NSA_Byte =
    NSA_Byte(data, blockByteSize)
  //---------------------------------------------------------------------------
  def apply(data: Array[ITEM_DATA_TYPE]
            , blockByteSize: Int): NSA_Byte = {
    val blockSeq = (data.grouped(blockByteSize / ITEM_DATA_TYPE_BYTE_SIZE).zipWithIndex map {
      case (itemSeq, i) => DataBlock(
        itemSeq
        , i
        , i * blockByteSize
        , (i + 1) * blockByteSize)
    }).toArray
    NSA_Byte(blockSeq, blockByteSize)
  }
  //---------------------------------------------------------------------------
  def build(blockByteSize: Int): NSA_Byte =
    NSA_Byte(Array(DataBlock(0 ,0 ,0)), blockByteSize)
  //---------------------------------------------------------------------------
}
//=============================================================================
import NSA_Byte._
case class NSA_Byte(dataBlockSeq: Array[DataBlock[ITEM_DATA_TYPE]] = Array()
                   , blockByteSize: Int) extends NonSequentialArray[ITEM_DATA_TYPE] {
  //---------------------------------------------------------------------------
  implicit val numericImpl = NSA_Byte.numericImpl
  implicit val orderingImpl= NSA_Byte.orderingImpl
  //---------------------------------------------------------------------------
  implicit def fromDoubleImpl(v:Double): ITEM_DATA_TYPE = Math.round(v).toByte
  implicit def toDoubleImpl(v:ITEM_DATA_TYPE) = v.toDouble
  //---------------------------------------------------------------------------
  val itemByteSize = ITEM_DATA_TYPE_BYTE_SIZE
  assert((blockByteSize % itemByteSize) == 0, s"The 'blockByteSize': '$blockByteSize' must be a multiple of 'itemByteSize': '$itemByteSize'")
  //---------------------------------------------------------------------------
  def toByteSeq(byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Array[Array[Byte]] =
    dataBlockSeq.map { block =>
      byteConversion.toByteSeq(block.itemSeq, byteOrder)
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file NSA_Byte.scala
//=============================================================================