//=============================================================================
package com.common.dataType.pixelDataType
//=============================================================================
import com.common.dataType.conversion.DataTypeConversion._
import com.common.dataType.dataType.DataType.{getDataTypeBitSize}
//=============================================================================
object PixelDataType {
  //---------------------------------------------------------------------------
  //BITPIX = -64 (double)
  //Use the largest type to support the remain data types operations. It generates an overhead: storing and processing
  //Use generic types (as Scala Numeric or Spite numeric librart) generate a lot of overhead : storing and processing
  type PIXEL_DATA_TYPE = Double //it must be the data type after applying the BSCALE  and BZERO transformation. No native data type in JVM can support a BSCALE and BZERO transformation: precdision problems are expected.
  final val PIXEL_ZERO_VALUE: PIXEL_DATA_TYPE = 0d
  final val PIXEL_INVALID_VALUE: PIXEL_DATA_TYPE = -1  //value that can not be held by any pixel
  //----------------------------------------------------------------------------
  def PIXEL_DATA_TYPE_TO_INT(d: PIXEL_DATA_TYPE) = Math.round(d).toInt
  //---------------------------------------------------------------------------
  def getPixelBitSize: Int = getDataTypeBitSize[PIXEL_DATA_TYPE](0d)
  //---------------------------------------------------------------------------
  def getPixelByteSize: Int = getPixelBitSize / 8
  //---------------------------------------------------------------------------
  //before any scale
  def getMinMaxPixelValue(bitPix: Int): (Double,Double) =
    bitPix match {
      case 8   =>  (0, 0x255)                        //in FITS standard the BITPIX  = 8 is native unsigned
      case 16  =>  (Short.MinValue, Short.MaxValue)  //in FITS standard the BITPIX  = 16 is native signed
      case 32  =>  (Int.MinValue, Int.MaxValue)      //in FITS standard the BITPIX  = 32 is native signed
      case 64  =>  (Long.MinValue, Long.MaxValue)    //in FITS standard the BITPIX  = 64 is native signed
      case -32 =>  (Float.MinValue, Float.MaxValue)  //in FITS standard the BITPIX  = -32 is native signed
      case -64 =>  (Double.MinValue, Double.MaxValue)//in FITS standard the BITPIX  = -64 is native signed
    }

  //---------------------------------------------------------------------------
  def getPixelSeq(data: Array[Byte], bitPix: Int): Array[PIXEL_DATA_TYPE] =
    bitPix match {
      case 8   => data map (b=>(b.toInt & 0xFF).toDouble)
      case 16  => byteSeqToShortSeq(data) map (_.toDouble)
      case 32  => byteSeqToIntSeq(data) map (_.toDouble)
      case 64  => byteSeqToLongSeq(data) map (_.toDouble)
      case -32 => byteSeqToFloatSeq(data) map (_.toDouble)
      case -64 => byteSeqToDoubleSeq(data)
    }

  //---------------------------------------------------------------------------
  def getPixelByteSeq(data: Array[PIXEL_DATA_TYPE], bitPix: Int): Array[Byte] =
    bitPix match {
      case 8   => data.map(v=> Math.round(v).toByte)
      case 16  => shortSeqToByteSeq(data.map(v=> Math.round(v).toShort))
      case 32  => intSeqToByteSeq(data.map(v=> Math.round(v).toInt))
      case 64  => longSeqToByteSeq(data.map(v=> Math.round(v)))
      case -32 => floatSeqToByteSeq(data.map(v=> v.toFloat))
      case -64 => doubleSeqToByteSeq(data)
    }

  //---------------------------------------------------------------------------
  def scalePixelSeq(data: Array[PIXEL_DATA_TYPE]
                    , bScale: Double
                    , bZero: Double
                    , isForward: Boolean = true): Array[PIXEL_DATA_TYPE] = {
    val isScaled = bScale != 1
    val isTranslated = bZero != 0

    (isScaled, isTranslated) match {
      case (false, false) => data

      case (true, false)  =>
        if (isForward) data map (_ * bScale)
        else data map (_ / bScale)

      case (false, true)  =>
        if (isForward) data map (_ + bZero)
        else data map (_ - bZero)

      case (true, true)   =>
        if (isForward) data map (v=> (v * bScale) + bZero)
        else data map (v=> (v - bZero) / bScale)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file PixelDataType.scala
//=============================================================================