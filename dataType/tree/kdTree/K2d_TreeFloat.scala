/** It implements tree of 2-dimensions for floats (https://en.wikipedia.org/wiki/K-d_tree) for space partitioning of floats
 * It is a specialized class of K2d_Tree created to avoid the template and increase the speed of processing
 * One alternative to kd-Tree is: https://en.wikipedia.org/wiki/Hilbert_R-tree#Packed_Hilbert_R-trees
 *                                  Implementation: https://github.com/mourner/flatbush
 * Another alternative to kd-Tree is : https://en.wikipedia.org/wiki/R-tree
 * A comparative can be found at: https://blog.mapbox.com/a-dive-into-spatial-search-algorithms-ebd0c5e39d2a
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history https://www.geeksforgeeks.org/k-dimensional-tree/?ref=lbp
 *          https://web.eecs.umich.edu/~sugih/courses/eecs281/f11/lectures/07-kdtrees.pdf
 *          https://www.datasciencecentral.com/profiles/blogs/implementing-kd-tree-for-fast-range-search-nearest-neighbor
 *          https://web.eecs.umich.edu/~sugih/courses/eecs281/f11/lectures/07-kdtrees.pdf
 */
//=============================================================================
package com.common.dataType.tree.kdTree
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import java.util.concurrent.atomic.AtomicInteger
import scala.collection.mutable.ArrayBuffer
//=============================================================================
import com.common.dataType.tree.binary.printer.TreePrinter
import com.common.dataType.tree.binary.printer.TreePrinter.PrintableNode
import com.common.geometry.point.Point2D_Float
import com.common.geometry.rectangle.RectangleFloat
import com.common.geometry.circle.CircleFloat
import com.common.util.path.Path
//=============================================================================
object K2d_TreeFloat{
  //---------------------------------------------------------------------------
  //Data type used to store the coordinates of the nodes.
  //Please, update properly the values of variables: xMin,yMin,xPixMax,yPixMax
  type CoordinateType = Float
  //---------------------------------------------------------------------------
  //Type used for store the value of the node
  type ValueStorageType = Int
  //---------------------------------------------------------------------------
  abstract class FloatMetric() {
    //-------------------------------------------------------------------------
    def complies(p: Point2D_Float) : Boolean
    //-------------------------------------------------------------------------
    def getMinPos() : Point2D_Float
    //-------------------------------------------------------------------------
    def getMaxPos() : Point2D_Float
    //-------------------------------------------------------------------------
    def intersects(r:RectangleFloat) = r intersects RectangleFloat(getMinPos, getMaxPos)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  case class RectangleMetric(r: RectangleFloat) extends FloatMetric {
    //-------------------------------------------------------------------------
    def complies(p: Point2D_Float) : Boolean = r isIn p
    //-------------------------------------------------------------------------
    def getMinPos() = r.min
    //-------------------------------------------------------------------------
    def getMaxPos() = r.max
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  case class CircleMetric(c: CircleFloat) extends FloatMetric {
    //-------------------------------------------------------------------------
    def complies(p: Point2D_Float) : Boolean = c isIn p
    //-------------------------------------------------------------------------
    def getMinPos() = c.getMin
    //-------------------------------------------------------------------------
    def getMaxPos() = c.getMax
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import K2d_TreeFloat._
case class K2d_TreeFloat(name: String = "No Name", k: Int = 2){
  //---------------------------------------------------------------------------
  assert(k == 2 ,"The dimension of the k-tree must be 2" )
  //---------------------------------------------------------------------------
  private var root : Node = null
  //---------------------------------------------------------------------------
  private var xMin = Float.MaxValue
  private var yMin = Float.MaxValue
  private var xMax = Float.MinValue
  private var yMax = Float.MinValue
  //---------------------------------------------------------------------------
  def isEmpty = root == null
  //---------------------------------------------------------------------------
  def createNode(v: ValueStorageType): Node =
    Node(Seq(Float.MaxValue,Float.MaxValue),v)
  //---------------------------------------------------------------------------
  def createNode(seq:Seq[CoordinateType], v: ValueStorageType = -1): Node = {
    assert(seq.length == k, s"The size of input sequence: ${seq.length} must match with the dimension of the tree $k")
    Node(seq,v)
  }
  //---------------------------------------------------------------------------
  def addSeq(seq: Seq[(Seq[CoordinateType],ValueStorageType)], depth: Int = 0) : Unit   = {
    val s = seq.length
    s match {
      case 0 =>
      case 1 => += (seq(0)._1,seq(0)._2)
      case _ =>
        val cutDimension = depth % k
        val sortedSeq = seq.sortWith(_._1(cutDimension) < _._1(cutDimension))  //sort by first dimension
        val midPos = sortedSeq.length / 2
        val mid = sortedSeq(midPos)
        += (mid._1, mid._2)
        addSeq(sortedSeq.slice(0,midPos),depth + 1)
        addSeq(sortedSeq.slice(midPos+1,sortedSeq.length),depth + 1)
    }
  }
  //---------------------------------------------------------------------------
  def +=(seq: Seq[CoordinateType], v: ValueStorageType): Unit = {
    root = add(root, createNode(seq,v),0)
    xMin = Math.min(xMin,seq(0))
    yMin = Math.min(yMin,seq(1))
    xMax = Math.max(xMax,seq(0))
    yMax = Math.max(yMax,seq(1))
  }
  //---------------------------------------------------------------------------
  private def add(currentRoot : Node, n: Node, depth: Int): Node = {
    if (currentRoot == null) n
    else {
      val cutDimension = depth % k
      if (n.pointSeq(cutDimension) < currentRoot.pointSeq(cutDimension))
        currentRoot.left = add(currentRoot.left, n, depth + 1)
      else
        currentRoot.right = add(currentRoot.right, n, depth + 1)
      currentRoot
    }
  }
  //---------------------------------------------------------------------------
  def toNodeSeq(): Array[Node] = {
    //-------------------------------------------------------------------------
    val result = ArrayBuffer[Node]()
    //-------------------------------------------------------------------------
    def iterate(n: Node): Unit = {
      if (n == null) return
      result += n
      iterate(n.left)
      iterate(n.right)
    }
    //-------------------------------------------------------------------------
    iterate(root)
    result.toArray
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def findFirst(seq: Seq[CoordinateType]) : Node = findFirst(createNode(seq), Node.EQUAL_BY_COORDINATE)
  //---------------------------------------------------------------------------
  def exists(seq: Seq[CoordinateType]) : Boolean = findFirst(createNode(seq), Node.EQUAL_BY_COORDINATE) != null
  //---------------------------------------------------------------------------
  def findFirst(v:ValueStorageType) : Node = findFirst(createNode(v), Node.EQUAL_BY_VALUE)
  //---------------------------------------------------------------------------
  def exists(v:ValueStorageType) : Boolean = findFirst(createNode(v), Node.EQUAL_BY_VALUE) != null
  //---------------------------------------------------------------------------
  private def findFirst(requestedNode: Node, equalityType:Node.NodeEqualityType) : Node = {
    //-------------------------------------------------------------------------
    var findNode : Node = null
    var nodeFound = false
    //-------------------------------------------------------------------------
    def find(currentRoot : Node, depth: Int): Unit ={
      if (currentRoot == null) return
      if (currentRoot.equal(requestedNode, equalityType)) {
        findNode = currentRoot
        nodeFound = true
        return
      }
      val cutDimension = depth % k
      if (requestedNode.pointSeq(cutDimension) < currentRoot.pointSeq(cutDimension))
        return find(currentRoot.left, depth + 1)
      if (!nodeFound) find(currentRoot.right, depth + 1)
    }
    //-------------------------------------------------------------------------
    find(root, 0)
    findNode
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def findMin(userDim: Int) : Node = findMin(root,userDim,0)
  //---------------------------------------------------------------------------
  //find the minimum of the d'th dimension of the tree
  private def findMin(currentRoot : Node, dim: Int, depth: Int) : Node = {
    if (currentRoot == null) return null
    val cutDimension = depth % k
    if (cutDimension == dim){
      if (currentRoot.left == null) return currentRoot
      return findMin( currentRoot.left, dim, depth + 1)
    }
    minThreeNodes( currentRoot
      , findMin(currentRoot.left,  dim, depth + 1)
      , findMin(currentRoot.right, dim, depth + 1)
      , dim)
  }
  //---------------------------------------------------------------------------
  def findMax(userDim: Int) : Node = findMin(root,userDim,0)
  //---------------------------------------------------------------------------
  //find the maximum of the d'th dimension of the tree
  private def findMax(currentRoot : Node, userDim: Int, depth: Int) : Node = {
    if (currentRoot == null) return null
    val cutDimension = depth % k
    if (cutDimension == userDim){
      if (currentRoot.right == null) return currentRoot
      return findMax( currentRoot.right, userDim, depth + 1 )
    }
    maxThreeNodes( currentRoot
      , findMax(currentRoot.left,  userDim, depth + 1)
      , findMax(currentRoot.right, userDim, depth + 1)
      , userDim)
  }
  //---------------------------------------------------------------------------
  private def minThreeNodes(x: Node, y: Node, z: Node, dim: Int)  = {
    var res = x
    if ((y != null) && y.data(dim) < res.data(dim)) res = y
    if ((z != null) && z.data(dim) < res.data(dim)) res = z
    res
  }
  //---------------------------------------------------------------------------
  private def maxThreeNodes(x: Node, y: Node, z: Node, dim: Int)  = {
    var res = x
    if ((y != null) && y.data(dim) >= res.data(dim)) res = y
    if ((z != null) && z.data(dim) >= res.data(dim)) res = z
    res
  }
  //---------------------------------------------------------------------------
  def deleteNode(seq:Seq[CoordinateType]) : Unit  = root = deleteNode(root, createNode(seq,Int.MaxValue), 0, Node.EQUAL_BY_COORDINATE)
  //---------------------------------------------------------------------------
  //find the minimum of the d'th dimension of the tree
  //https://www.geeksforgeeks.org/k-dimensional-tree-set-3-delete/
  private def deleteNode(currentRoot : Node, n: Node, depth: Int, equalityType:Node.NodeEqualityType) : Node = {
    if (currentRoot == null) return null
    val cutDimension = depth % k
    if (currentRoot.equal(n, equalityType)) {
      if (currentRoot.right != null){
        val minNode = findMin(currentRoot.right, cutDimension, depth+1)
        currentRoot.copy(minNode)
        currentRoot.right = deleteNode(currentRoot.right, minNode, depth+1, equalityType)
      }
      else {
        if (currentRoot.left != null) {
          val maxNode = findMax(currentRoot.left, cutDimension, depth+1)
          currentRoot.copy(maxNode)
          currentRoot.left = deleteNode(currentRoot.left, maxNode, depth+1, equalityType)
        }
        else return null
      }
      return currentRoot
    }
    if (n.pointSeq(cutDimension) < currentRoot.pointSeq(cutDimension))
      currentRoot.left = deleteNode(currentRoot.left, n, depth + 1, equalityType)
    else
      currentRoot.right = deleteNode(currentRoot.right, n, depth + 1, equalityType)
    currentRoot
  }
  //---------------------------------------------------------------------------
  def prettyPrint(tabSpaceCount : Int = 4) : Unit = {
    println(s"------------------K-$k tree:'$name' starts ------------------")
    if (root == null) println("----Empty tree----")
    else TreePrinter.print(root,tabSpaceCount)
    println(s"------------------ K-$k tree:'$name' ends ------------------")
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def prettyPrintVertical() : Unit = {
    println(s"------------------K-$k tree:'$name' starts ------------------")
    if (root == null) println("----Empty tree----")
    else TreePrinter.printVertical("",root,false)
    println(s"------------------ K-$k tree:'$name' ends ------------------")
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private def getBoundRectangle(parent: Node, n: Node, parentDepth: Int, parentRectangle: RectangleFloat, isLeft: Boolean): RectangleFloat = {
    if (n.id == root.id) return parentRectangle
    val cutDimension = parentDepth % k
    val cutDimensionValue = parent.pointSeq(cutDimension)

    if (cutDimension == 0) {  //vertical split
      if (isLeft) RectangleFloat(parentRectangle.min,Point2D_Float(cutDimensionValue,parentRectangle.yMax))
      else RectangleFloat(Point2D_Float(cutDimensionValue,parentRectangle.yMin),parentRectangle.max)
    }
    else{ //horizontal split
      if (isLeft) RectangleFloat(parentRectangle.min,Point2D_Float(parentRectangle.xMax,cutDimensionValue))
      else RectangleFloat(Point2D_Float(parentRectangle.xMin,cutDimensionValue),parentRectangle.max)
    }
  }
  //---------------------------------------------------------------------------
  private def getEuclideanDistance(w: Node, z: Node) ={
    val a = w.pointSeq(0) - z.pointSeq(0)
    val b = w.pointSeq(1) - z.pointSeq(1)
    Math.sqrt( (a * a) + (b * b) ).toFloat
  }
  //---------------------------------------------------------------------------
  //https://gopalcdas.com/2017/05/24/construction-of-k-d-tree-and-using-it-for-nearest-neighbour-search/
  //K nearest neighbor search
  def getKNN(seq: Seq[CoordinateType], requestedK: Int = 1, keepAllWithMinDistanceIgnoringK: Boolean = false): Array[Node]  = {
    //---------------------------------------------------------------------------
    var sortedQueue = ArrayBuffer[(Node,Float)]() //sorted by distance. RegistrationByClosestSource first
    //---------------------------------------------------------------------------
    def updateStorage(n: Node, d: Float) : Unit = {
      sortedQueue.append((n,d))
      sortedQueue = sortedQueue.sortWith(_._2 < _._2) //add and keep sorted
      if (keepAllWithMinDistanceIgnoringK) {
        val nodeCountSeqWithMinDistance = sortedQueue.filter( p=> p._2 == sortedQueue.head._2 ).length
        val remainNodeCount = sortedQueue.length - nodeCountSeqWithMinDistance
        val nodeCountToAdd = nodeCountSeqWithMinDistance + (if (remainNodeCount == 0) 0 else Math.min(requestedK - 1,remainNodeCount))
        if (sortedQueue.length > requestedK) sortedQueue = sortedQueue.slice(0,nodeCountToAdd) //keep the k closest, ignoring requestedK
      }
      else
          if (sortedQueue.length > requestedK) sortedQueue.remove(sortedQueue.length-1) //keep only the k closests
    }
    //---------------------------------------------------------------------------
    val requestedNode = createNode(seq)
    val requestedPoint = Point2D_Float(seq(0),seq(1))
    //-------------------------------------------------------------------------
    def search(parent: Node, n : Node, depth: Int, r: RectangleFloat, isLeft: Boolean): Unit ={
      if (n == null) return
      val boundingRectangle = getBoundRectangle(parent, n, depth-1, r, isLeft)
      val distanceToBoundBox = if (boundingRectangle.isIn(requestedPoint)) 0 else boundingRectangle.getMinDistanceToPoint(requestedPoint)
      updateStorage(n,getEuclideanDistance(n,requestedNode))
      if (distanceToBoundBox > sortedQueue.last._2) return
      if (n.left != null) search(n,n.left,depth+1,boundingRectangle,true)
      if (n.right != null) search(n,n.right,depth+1,boundingRectangle,false)
    }
    //-------------------------------------------------------------------------
    if (!RectangleFloat.isValid(Point2D_Float(xMin,yMin),Point2D_Float(xMax,yMax))) Array()
    else {
      search(null,root,0, RectangleFloat(Point2D_Float(xMin,yMin),Point2D_Float(xMax,yMax)),true)
      (sortedQueue map (_._1)).toArray
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private def metricSearch(metric : FloatMetric): Array[Node]  = {
    //-------------------------------------------------------------------------
    val storage = ArrayBuffer[Node]() //sorted by distance. RegistrationByClosestSource first
    //-------------------------------------------------------------------------
    def search(parent: Node, n : Node, depth: Int, parentRectangle: RectangleFloat, isLeft: Boolean): Unit ={
      if (n == null) return
      val boundingRectangle = getBoundRectangle(parent, n, depth-1, parentRectangle, isLeft)
      if (!metric.intersects(boundingRectangle)) return
      if (metric.complies(Point2D_Float(n.data))) storage += n
      if (n.left != null) search(n,n.left,depth+1,boundingRectangle,true)
      if (n.right != null) search(n,n.right,depth+1,boundingRectangle,false)
    }
    //-------------------------------------------------------------------------
    search(null,root,0, RectangleFloat(Point2D_Float(xMin,yMin),Point2D_Float(xMax,yMax)),true)
    storage.toArray
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //return all the points contained in a input rectangle
  //also known as orthogonal range search or just range search
  def rectangleSearch(r: RectangleFloat) = metricSearch(RectangleMetric(r))
  //---------------------------------------------------------------------------
  def circleSearch(c: CircleFloat) = metricSearch(CircleMetric(c))
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
  //typically used before print tree
  def calculateAndStoreBoundingRectangle() = {
    //-------------------------------------------------------------------------
    def walk(parent: Node, n : Node, depth: Int, parentRectangle: RectangleFloat, isLeft: Boolean): Unit ={
      if (n == null) return
      n.boundingRectangle =  getBoundRectangle(parent, n, depth-1, parentRectangle, isLeft)
      if (n.left != null)  walk(n, n.left, depth+1, n.boundingRectangle,true)
      if (n.right != null) walk(n, n.right, depth+1, n.boundingRectangle,false)
    }
    //---------------------------------------------------------------------------
    if(root != null) walk(null, root, 0, RectangleFloat(Point2D_Float(xMin, yMin), Point2D_Float(xMax, yMax)), true)
    //---------------------------------------------------------------------------
  }
  ///----------------------------------------------------------------------------
  //order =0 => inorder walk: (left, root, right)
  //order =1 => preorder walk: (root, left, right)
  //order =2 => postorder walk: (left, right, root)
  def getNodeSeq(order: Int = 1) = {
    //---------------------------------------------------------------------------
    val storage = ArrayBuffer[(Node,Int,Boolean)]()
    //---------------------------------------------------------------------------
    def walk(n : Node,depth: Int,isLeft: Boolean): Unit ={
      if (n == null) return

      order match {
        case 0 =>
          if (n.left != null) walk(n.left,depth+1, true)
          storage.append((n,depth,isLeft))
          if (n.right != null) walk(n.right,depth+1, false)

        case 1 =>
          storage.append((n,depth,isLeft))
          if (n.left != null) walk(n.left,depth+1, true)
          if (n.right != null) walk(n.right,depth+1, false)

        case 2 =>
          if (n.left != null) walk(n.left,depth+1, true)
          if (n.right != null) walk(n.right,depth+1, false)
          storage.append((n,depth,isLeft))
      }
    }
    //-------------------------------------------------------------------------
    if(root != null) walk(root,0,true)
    storage
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def generatePlot(outputDir : String = "output/k2D_tree_plot"): Unit = {

    val oDir = Path.resetDirectory(Path.resetDirectory(outputDir) + name)
    calculateAndStoreBoundingRectangle

    val gnuPlotFileName =oDir + "plot.gnuplot"
    val axisFileName =   oDir + "axis.data"
    val xPointFileName = oDir + "xPoint.data"
    val yPointFileName = oDir + "yPoint.data"
    val xAxisFileName =  oDir + "xAxisPartition.data"
    val yAxisFileName =  oDir + "yAxisPartition.data"

    val nodeSeq = getNodeSeq()

    //axis file
    val xAxis = Point2D_Float(xMin,xMax)
    val yAxis = Point2D_Float(yMin,yMax)
    val axisFile = new BufferedWriter(new FileWriter(new File(axisFileName)))
    axisFile.write(xAxis.x + " " + xAxis.y + "\n")
    axisFile.write(yAxis.x + " " + yAxis.y + "\n")
    axisFile.close()

    //aXis partition (generates a vertical line)
    val xPartitionFile = new BufferedWriter(new FileWriter(new File(xAxisFileName)))
    val xNodeSeq = nodeSeq.filter( _._2 % 2 == 0) map  { n=>
      val rec = n._1.boundingRectangle
      val x = n._1.pointSeq(0)
      val y = rec.min.y
      val size = rec.max.y - rec.min.y
      xPartitionFile.write( x + " " + y + " " + 0 +  " "  + size +"\n" )
      n._1
    }
    xPartitionFile.close

    //yXis partition (generates a horizontal line)
    val yPartitionFile = new BufferedWriter(new FileWriter(new File(yAxisFileName)))
    val yNodeSeq = nodeSeq.filter( _._2 % 2 != 0) map  { n=>
      val rec = n._1.boundingRectangle
      val x = rec.min.x
      val y =  n._1.pointSeq(1)
      val size = rec.max.x - rec.min.x
      yPartitionFile.write( x + " " + y + " " + size + " " + 0 + "\n" )
      n._1
    }
    yPartitionFile.close

    //point file
    val xPointFile = new BufferedWriter(new FileWriter(new File(xPointFileName)))
    xNodeSeq foreach {p=> xPointFile.write(p.pointSeq(0) + " " + p.pointSeq(1) + "\n") }
    xPointFile.close

    val yPointFile = new BufferedWriter(new FileWriter(new File(yPointFileName)))
    yNodeSeq foreach {p=> yPointFile.write(p.pointSeq(0) + " " + p.pointSeq(1) + "\n") }
    yPointFile.close

    val gnuPlot = new BufferedWriter(new FileWriter(new File(gnuPlotFileName)))
    gnuPlot.write(
    """
      |#-----------------------------------------
      |#star commands
      |reset
      |set multiplot
      |#-----------------------------------------
      |#variables
      |user_title = "K2D tress axis partition"
      |x_min = system("head -1  axis.data  | awk '{print $1}'")
      |x_max = system("head -1  axis.data  | awk '{print $2}'")
      |
      |y_min = system("tail -1  axis.data  | awk '{print $1}'")
      |y_max = system("tail -1  axis.data  | awk '{print $2}'")
      |
      |#-----------------------------------------
      |#colors
      |x_axis_line_color = "#0000ff"
      |x_axis_label_color=x_axis_line_color
      |x_axis_tics_color=x_axis_label_color
      |
      |y_axis_line_color ="#880000"
      |y_axis_label_color=y_axis_line_color
      |y_axis_tics_color=y_axis_label_color
      |##-----------------------------------------
      |#border
      |#set border 2
      |#-----------------------------------------
      |#axis
      |set xrange[x_min-1:x_max+1]
      |set yrange[y_min-1:y_max+1]
      |
      |set xlabel "x coordinate" tc rgb x_axis_label_color
      |set ylabel "y coordinate" tc rgb y_axis_label_color
      |
      |set xtics 1
      |set ytics 1
      |#----------------------------------------
      |#styles
      |set style line 12 lc rgb x_axis_line_color lt 1 lw 1
      |set style line 13 lc rgb y_axis_line_color lt 1 lw 1
      |set style line 14 lc rgb 'grey' lt 1 lw 1
      |#-----------------------------------------
      |#grid
      |#set grid xtics ytics lc rgb 'grey' ls 14, ls 14
      |
      |set arrow 1 from x_min,y_min to x_max,y_min lc rgb 'black' dt 3 lw 1 nohead
      |set arrow 2 from x_min,y_min to x_min,y_max lc rgb 'black' dt 3 lw 1 nohead
      |set arrow 3 from x_max,y_max to x_min,y_max lc rgb 'black' dt 3 lw 1 nohead
      |set arrow 4 from x_max,y_max to x_max,y_min lc rgb 'black' dt 3 lw 1 nohead
      |#-----------------------------------------
      |#points
      |set style line 1 pointtype 7 pointsize 2 lc rgb x_axis_line_color
      |set style line 2 pointtype 7 pointsize 2 lc rgb y_axis_line_color
      |#-----------------------------------------
      |#plot
      |set key off
      |set title user_title
      |set offset 1,1,1,1
      |
      |plot 'xPoint.data' using 1:2:(sprintf("(%d, %d)", $1, $2)) with labels point ls 1 offset char 1,1 \
      |   , 'yPoint.data' using 1:2:(sprintf("(%d, %d)", $1, $2)) with labels point ls 2 offset char 1,1 \
      |   , 'xAxisPartition.data' using 1:2:3:4 with vectors lc rgb x_axis_line_color lt 1 lw 1 nohead \
      |   , 'yAxisPartition.data' using 1:2:3:4 with vectors lc rgb y_axis_line_color lt 1 lw 1 nohead
      |
      |pause -1
      |#-----------------------------------------
      |#end of file
      |#-----------------------------------------
      |
    """.stripMargin
    )
    gnuPlot.close()
  }
  //===========================================================================
  private object Node {
    //---------------------------------------------------------------------------
    private val id = new AtomicInteger(Int.MinValue)
    //---------------------------------------------------------------------------
    def getNewID: Int = id.addAndGet(1)
    //---------------------------------------------------------------------------
    sealed trait NodeEqualityType
    case object EQUAL_BY_COORDINATE extends NodeEqualityType
    case object EQUAL_BY_VALUE extends NodeEqualityType
    case object EQUAL_BY_ID extends NodeEqualityType
    //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
  }
  //===========================================================================
  import Node._
  case class Node (data: Seq[CoordinateType], v: ValueStorageType) extends PrintableNode {
    //-------------------------------------------------------------------------
    var left: Node = null
    var right: Node = null
    var pointSeq = ArrayBuffer[CoordinateType]()
    val id = getNewID
    var boundingRectangle: RectangleFloat = null   //only used in plots
    //-------------------------------------------------------------------------
    pointSeq ++= data
    //-------------------------------------------------------------------------
    def equal(n: Node, equalityType: NodeEqualityType) =
      equalityType match {
        case EQUAL_BY_COORDINATE => (pointSeq zip n.pointSeq) forall (t=> t._1 == t._2)
        case EQUAL_BY_VALUE => v == n.v
        case EQUAL_BY_ID => id == id
      }
    //-------------------------------------------------------------------------
    def copy(n: Node) = pointSeq = n.pointSeq
    //-------------------------------------------------------------------------
    override def toString = s"${pointSeq.mkString("{",",","}")}" + "->" + v
    //-------------------------------------------------------------------------
    def getLeft: PrintableNode = left
    //-------------------------------------------------------------------------
    def getRight: PrintableNode = right
    //-------------------------------------------------------------------------
    def getText: String = toString
    //-------------------------------------------------------------------------
    def isLeaf = (left == null) && (right == null)
    //-------------------------------------------------------------------------
  }
  //===========================================================================
}
//=============================================================================
//End of file K2d_TreeFloat.scala
//=============================================================================