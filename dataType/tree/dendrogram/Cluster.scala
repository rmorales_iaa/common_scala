/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Jan/2022
 * Time:  12h:18m
 *  Based on: https://www.javatips.net/api/yooreeka-master/src/iweb2/ch4/model/Cluster.java
 */
//=============================================================================
package com.common.dataType.tree.dendrogram
//=============================================================================
//=============================================================================
object Cluster {
  //---------------------------------------------------------------------------
  def apply[T](name: String,dataPoint: DataPoint[T]): Cluster[T] = Cluster(name,dataPoint)
  //---------------------------------------------------------------------------
  def apply[T](name: String,dataPoint: Seq[DataPoint[T]]): Cluster[T] = Cluster(name,dataPoint)
  //---------------------------------------------------------------------------
  def apply[T](name: String,cluster: Cluster[T]): Cluster[T] = Cluster(name, cluster.storage)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Cluster[T](name: String, storage:Array[DataPoint[T]]) {
  //---------------------------------------------------------------------------
  def size = storage.size
  //---------------------------------------------------------------------------
  def contains(dataPoint:DataPoint[T]): Boolean = storage.find(dataPoint == _).isDefined
  //---------------------------------------------------------------------------
  def contains (cluster: Cluster[T]): Boolean = cluster.storage.forall( contains(_) )
  //---------------------------------------------------------------------------
  def == (obj: Cluster[T]): Boolean = {
    if (this == obj) return true
    if (obj == null) return false
    if (getClass ne obj.getClass) return false

    if ((name == null) && (obj.name != null)) return false
    if ((obj.name == null) && (name != null)) return false
    if (name != obj.name) return false

    if (storage.size != obj.storage.size) return false
    (storage zip obj.storage) forall (t=> t._1 == t._2)
  }
  //---------------------------------------------------------------------------
  override def toString: String = s"{name='$name' ${(storage map (_.toString)).mkString("\n")}"
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Cluster.scala
//=============================================================================
