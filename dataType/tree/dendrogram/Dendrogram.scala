/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Jan/2022
 * Time:  11h:59m
 * Description:
 *  based on: https://www.javatips.net/api/yooreeka-master/src/iweb2/ch4/clustering/hierarchical/Dendrogram.java
 */
//=============================================================================
package com.common.dataType.tree.dendrogram
//=============================================================================

//=============================================================================
//=============================================================================
object Dendrogram {
}
//=============================================================================
case class Dendrogram[T](name: String) {
  //---------------------------------------------------------------------------
  private val clusterMap = scala.collection.mutable.Map[Int, Seq[Cluster[T]]]()
  private val levelNameMap = scala.collection.mutable.Map[Int, String]()
  private var nextLevel = 1
  //---------------------------------------------------------------------------
  def updateLevel(levelName: String, cluster: Cluster[T]): Unit = updateLevel(levelName,Seq(cluster))
  //---------------------------------------------------------------------------
  def updateLevel(levelName: String, clusterSeq: Seq[Cluster[T]]): Unit = {
    clusterMap(nextLevel) = clusterSeq
    levelNameMap(nextLevel) = levelName
    nextLevel += 1
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Dendrogram.scala
//=============================================================================
