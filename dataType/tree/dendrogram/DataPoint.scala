/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Jan/2022
 * Time:  12h:28m
 * Description:
 *  Based on: https://www.javatips.net/api/yooreeka-master/src/iweb2/ch4/model/DataPoint.java
 */
//=============================================================================
package com.common.dataType.tree.dendrogram
//=============================================================================
//=============================================================================
object DataPoint {
  //---------------------------------------------------------------------------
  def apply[T](name: String,seq: Seq[Attribute[T]]): DataPoint[T] = DataPoint(name, seq)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class DataPoint[T](name: String, attributeSeq: Array[Attribute[T]]) {
  //---------------------------------------------------------------------------
  def size = attributeSeq.size
  //---------------------------------------------------------------------------
  def getAttributeNameSeq = (attributeSeq map  (_.name))
  //---------------------------------------------------------------------------
  def getAttributeValueSeq = (attributeSeq map  (_.getValueString))
  //---------------------------------------------------------------------------
  def == (obj: DataPoint[T]): Boolean = {
    if (this == obj) return true
    if (obj == null) return false
    if (getClass ne obj.getClass) return false

    if ((name == null) && (obj.name != null)) return false
    if ((obj.name == null) && (name != null)) return false
    if (name != obj.name) return false

    if (attributeSeq.size != obj.attributeSeq.size) return false
    (attributeSeq zip obj.attributeSeq) forall (t=>  t._1 == t._2)
  }
  //---------------------------------------------------------------------------
  override def toString: String = {
    val s = ((getAttributeNameSeq zip getAttributeValueSeq) map { t=> s"('${t._1}'=${t._2})" }).mkString(",")
    s"[name='$name' $s ]"
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file DataPoint.scala
//=============================================================================
