/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Jan/2022
 * Time:  12h:30m
 * Description:
 *  Based on: https://www.javatips.net/api/yooreeka-master/src/iweb2/ch4/model/Attribute.java
 */
//=============================================================================
package com.common.dataType.tree.dendrogram
//=============================================================================
//=============================================================================
case class Attribute[T](name: String, value: T) {
  //---------------------------------------------------------------------------
  def isNumeric() =
    value match {
      case _: Byte     => true
      case _: Short    => true
      case _: Int      => true
      case _: Integer  => true
      case _: Float    => true
      case _: Double   => true
      case _ => false
    }
  //---------------------------------------------------------------------------
  def isText() = value.isInstanceOf[String]
  //---------------------------------------------------------------------------
  def getValueString = value.toString
  //---------------------------------------------------------------------------
  def == (obj: Attribute[T]): Boolean = {
    if (this == obj) return true
    if (obj == null) return false
    if (getClass ne obj.getClass) return false

    if ((name == null) && (obj.name != null)) return false
    if ((obj.name == null) && (name != null)) return false
    if (name != obj.name) return false

    if ((value == null) && (obj.value != null)) return false
    if ((obj.value == null) && (value != null)) return false

    if (value.toString != obj.toString) return false

    true
  }
  //---------------------------------------------------------------------------
  override def toString: String = "[name=" + name + ", value=" + value.toString + ", isText=" + isText + ", isNumeric=" + isNumeric + "]"
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Attribute.scala
//=============================================================================
