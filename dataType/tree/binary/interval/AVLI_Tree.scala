/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  20/Feb/2020
 * Time:  20h:28m
 * Description: Mutable Augmented AVL (Adelson-Velskii y Landis) Interval Tree (self-balancing binary search tree)
 * Based on: https://www.geeksforgeeks.org/avl-tree-set-1-insertion/
 *           https://www.geeksforgeeks.org/avl-tree-set-2-deletion/
 *           http://www.davismol.net/2016/02/07/data-structures-augmented-interval-tree-to-search-for-interval-overlapping/
 * Duplicated values are allowed but not recommended when find . In that case please use "deepFind"
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.dataType.tree.binary.interval
//=============================================================================
import scala.annotation.tailrec
import scala.collection.mutable.ArrayBuffer
//=============================================================================
import com.common.geometry.interval.Interval.IntervalDataType
import com.common.geometry.interval.Interval
import com.common.dataType.tree.binary.printer.TreePrinter
import com.common.dataType.tree.binary.printer.TreePrinter.PrintableNode
import com.common.dataType.tree.binary.interval.AVLI_Tree.STORED_VALUE_TYPE
//=============================================================================
object AVLI_Tree{
  //---------------------------------------------------------------------------
  type STORED_VALUE_TYPE = Int
  //---------------------------------------------------------------------------
  def apply(itv: Interval): AVLI_Tree = {
    val tree = AVLI_Tree()
    tree += itv
    tree
  }
  //---------------------------------------------------------------------------
  def apply(seq: Seq[Interval]): AVLI_Tree = {
    val tree = AVLI_Tree()
    tree += seq
    tree
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//it a typical binary seach tree using only min value of the nodes
case class AVLI_Tree(name: String = "No Name") {
  //---------------------------------------------------------------------------
  protected var root: TreeNode = null
  //---------------------------------------------------------------------------
  def getRoot = root
  //---------------------------------------------------------------------------
  private def toNode(itv: Interval): TreeNode = BasicTreeNode(itv.s, itv.e, itv.id)
  //---------------------------------------------------------------------------
  def +=(seq: Seq[Interval]): Unit = seq foreach (this += _)
  //---------------------------------------------------------------------------
  def +=(itv: Interval) = root = insert(root, itv)
  //---------------------------------------------------------------------------
  def isBalanced(n: TreeNode) = {
    val b = getBalanceFactor(n)
    (b < -1) || (b > 1)
  }
  //---------------------------------------------------------------------------
  private def getBalanceFactor(n: TreeNode): Int =
    if (n == null) 0 else height(n.left) - height(n.right)
  //---------------------------------------------------------------------------
  def height(n: TreeNode): Int = if (n == null) 0 else n.height
  //---------------------------------------------------------------------------
  protected def getMin(n: TreeNode): Option[Int] = if (n == null) None else Some(n.min)
  //---------------------------------------------------------------------------
  protected def getMax(n: TreeNode): Option[Int] = if (n == null) None else Some(n.max)
  //--------------------------------------------------------------------------
  def toSeqValue(): ArrayBuffer[Int] = toSeqValue(root)
  //--------------------------------------------------------------------------
  def toSeqNode(): ArrayBuffer[TreeNode] = toSeqNode(root)
  //--------------------------------------------------------------------------
  private def toSeqValue(n: TreeNode): ArrayBuffer[Int] =
    for (node <- toSeqNode(n)) yield node.v
  //--------------------------------------------------------------------------
  private def toSeqNode(n: TreeNode) = {
    //------------------------------------------------------------------------
    val r = ArrayBuffer[TreeNode]()
    //------------------------------------------------------------------------
    def walk(node: TreeNode): Unit = {
      if (node != null) {
        r += node
        walk(node.left)
        walk(node.right)
      }
    }
    //------------------------------------------------------------------------
    walk(n)
    r
    //------------------------------------------------------------------------
  }
  //--------------------------------------------------------------------------
  protected def copy(a: TreeNode, b: TreeNode): Unit = {
    a.v = b.v
    a.s = b.s
    a.e = b.e
    a.left = b.left
    a.right = b.right
    a.max = b.max
    a.min = b.min
    a.height = b.height
  }
  //--------------------------------------------------------------------------
  def verifyMinMax(): Boolean = verifyMinMax(root)
  //--------------------------------------------------------------------------
  private def verifyMinMax(n: TreeNode): Boolean = {
    //------------------------------------------------------------------------
    def verify(node: TreeNode): Boolean = {
      val min = getMinNode(node).min
      val max = getMaxValue(node)
      if (min != node.min) {
        println(s"Error verifying node: $node. Expected min: $min but stored: ${node.min}"); return false
      }
      if (max != node.max) {
        println(s"Error verifying node: $node. Expected max: $max but stored: ${node.max}"); return false
      }
      true
    }
    //------------------------------------------------------------------------
    val seq = toSeqNode(n)
    seq forall { t =>
      verify(t)
    }
    //------------------------------------------------------------------------
  }
  //--------------------------------------------------------------------------
  private def getMinNode(n: TreeNode): TreeNode = {
    var current = n
    while (current.left != null)
      current = current.left
    current
  }
  //--------------------------------------------------------------------------
  def getMinValue(n: TreeNode) = getMinNode(n).s
  //--------------------------------------------------------------------------
  def getMaxValue(n: TreeNode): Int = { //max values are not constricted as min values, so check both branches
    var max = n.max
    var current = n
    while (current.right != null) {
      max = Math.max(max, n.e)
      current = current.right
    }
    current = n
    while (current.left != null) {
      max = Math.max(max, n.e)
      current = current.left
    }
    max
  }
  //---------------------------------------------------------------------------
  protected def updateMinMax(n: TreeNode): Unit = {
    val minSeq = Seq(Some(n.s), getMin(n.left)).flatten
    n.min = if (minSeq.isEmpty) n.s else minSeq.min

    //the tree is sorted by 'star' value, but the 'end' value is not constricted, so both branches must be considered
    val maxSeq = Seq(Some(n.e), getMax(n.left), getMax(n.right)).flatten
    n.max = if (maxSeq.isEmpty) n.e else maxSeq.max
  }
  //---------------------------------------------------------------------------
  protected def updateHeight(n: TreeNode) =
    n.height = 1 + Math.max(height(n.left), height(n.right))
  //---------------------------------------------------------------------------
  protected def updateHeightDeeply(n: TreeNode) : Unit = {
    if (n.left != null) updateHeightDeeply(n.left)
    if (n.right != null) updateHeightDeeply(n.right)
    updateHeight(n)
  }
  //---------------------------------------------------------------------------
  protected def setHeight(n: TreeNode, h: Int) = n.height = h
  //---------------------------------------------------------------------------
  protected def rightRotation(currentRoot: TreeNode): TreeNode = {
    val newRoot = currentRoot.left
    val adoptedNode = newRoot.right

    newRoot.right = currentRoot
    currentRoot.left = adoptedNode

    // update heights, min and max
    updateHeight(currentRoot)
    updateHeight(newRoot)

    updateMinMax(currentRoot)
    updateMinMax(newRoot)

    newRoot
  }
  //---------------------------------------------------------------------------
  protected def leftRotation(currentRoot: TreeNode): TreeNode = {
    val newRoot = currentRoot.right
    val adoptedNode = newRoot.left

    newRoot.left = currentRoot
    currentRoot.right = adoptedNode

    //update heights, min and max
    updateHeight(currentRoot)
    updateHeight(newRoot)

    updateMinMax(currentRoot)
    updateMinMax(newRoot)

    newRoot
  }
  //--------------------------------------------------------------------------
  protected def insert(n: TreeNode, itv: Interval): TreeNode = insert(n, toNode(itv))
  //--------------------------------------------------------------------------
  protected def insert(n: TreeNode, inNode: TreeNode): TreeNode = {
    //------------------------------------------------------------------------
    /* 1.  Perform the normal BST (binary search tree) insertion */
    if (n == null) return inNode
    inNode.compareTo(n) match {
      case 0 =>  return n //duplicated key. Final location depends on 'compareTo'
      case c if c < 0 => n.left  = insert(n.left, inNode)
      case c if c > 0 => n.right = insert(n.right, inNode);
    }
    //Update height, max, min of this ancestor AVLINode
    updateHeight(n)
    updateMinMax(n)

    // 3. Get the balance factor of this ancestor AVLINode to check whether this node became unbalanced
    val balance = getBalanceFactor(n)

    //left left case
    if (balance > 1 && inNode.compareTo(n.left) < 0) return rightRotation(n)

    //left right case
    if (balance > 1 && inNode.compareTo(n.left) > 0) {
      n.left = leftRotation(n.left)
      return rightRotation(n)
    }

    //right right case
    if (balance < -1 && inNode.compareTo(n.right) > 0) return leftRotation(n)

    //right left case
    if (balance < -1 && inNode.compareTo(n.right) < 0) {
      n.right = rightRotation(n.right)
      return leftRotation(n)
    }
    n
  }
  //--------------------------------------------------------------------------
  def delete(itv: Interval): Unit = root = delete(root, toNode(itv))
  //--------------------------------------------------------------------------
  private def delete(n: TreeNode, inNode: TreeNode): TreeNode = {
    // Step 1: Perform standard BST delete
    if (n == null) return n
    inNode.compareTo(n) match {
      case c if c < 0 => n.left = delete(n.left, inNode)
      case c if c > 0 => n.right = delete(n.right, inNode)
      case 0 => //node found

        // node with only one child or no child
        if ((n.left == null) || (n.right == null)) {
          var temp = if (n.left != null) n.left else n.right
          // No child case
          if (temp == null) {
            temp = n
            return null //original code 'n = NULL' and later 'return n'
          }
          else copy(n, temp) // One child case. Copy the contents of the non-empty child
        }
        else { // node with two children: Get the inorder successor (smallest in the right subtree)
          val temp = getMinNode(n.right)

          // Copy the inorder successor's data to this node
          n.v = temp.v
          n.s = temp.s
          n.e = temp.e

          // Delete the inorder successor
          n.right = delete(n.right, temp)
        }
    }

    // If the tree had only one node then return
    if (n == null) return n

    // Step 2: Update height, min, max of the current node
    updateHeight(n)
    updateMinMax(n)

    // Step 3: Get the balance factor of thi node (to check whether this node became unbalanced)
    val balance = getBalanceFactor(n)

    //if this node becomes unbalanced, then there are 4 cases
    //left left Case
    if (balance > 1 && getBalanceFactor(n.left) >= 0)
      return rightRotation(n)

    //left right Case
    if (balance > 1 && getBalanceFactor(n.left) < 0) {
      n.left = leftRotation(n.left)
      return rightRotation(n)
    }

    //right right Case
    if (balance < -1 && getBalanceFactor(n.right) <= 0)
      return leftRotation(n)

    //right left Case
    if (balance < -1 && getBalanceFactor(n.right) > 0) {
      n.right = rightRotation(n.right)
      return leftRotation(n)
    }
    n
  }
  //---------------------------------------------------------------------------
  def getIntersection(itv: Interval) = iterate(root, toNode(itv), intersectFilter)
  //---------------------------------------------------------------------------
  def getIntersectionOrContiguous(itv: Interval) = iterate(root, toNode(itv), intersectOrContiguousFilter)
  //---------------------------------------------------------------------------
  private def intersectFilter(a: TreeNode, b: TreeNode) = a intersects b
  //---------------------------------------------------------------------------
  private def intersectOrContiguousFilter(a: TreeNode, b: TreeNode) = a.intersects(b) || a.isContiguous(b) || b.isContiguous(a)
  //---------------------------------------------------------------------------
  def iterate(n: TreeNode, filter: (TreeNode, TreeNode) => Boolean): Unit = iterate(root, n, filter)
  //---------------------------------------------------------------------------
  private def iterate(newRoot: TreeNode
                      , n: TreeNode
                      , filter: (TreeNode, TreeNode) => Boolean): Array[TreeNode] = {
    //-------------------------------------------------------------------------
    val result = ArrayBuffer[TreeNode]()
    //-------------------------------------------------------------------------
    def iterate(newRoot: TreeNode, n: TreeNode): Unit = {
      if (newRoot == null) return
      if (filter(newRoot, n)) result += newRoot
      if ((newRoot.left != null) && (newRoot.left intersectsMinMax n)) iterate(newRoot.left, n)
      if ((newRoot.right != null) && (newRoot.right intersectsMinMax n)) iterate(newRoot.right, n)
    }
    //-------------------------------------------------------------------------
    iterate(newRoot, n)
    result.toArray
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //(nodeFoundOnLeftChildOfRoot,grandParent,parent,foundNode)
  def find(nodeToFind:TreeNode): Option[(Boolean,TreeNode,TreeNode,TreeNode)] = {
    //-------------------------------------------------------------------------
    var nodeFoundOnLeftChildOfRoot = false
    var nodeFound = false
    //-------------------------------------------------------------------------
    @tailrec
    def iterate(grandParent: TreeNode, parent: TreeNode, newNode: TreeNode): Option[(TreeNode,TreeNode,TreeNode)] = {
      if (newNode == null) return None
      if (nodeToFind.v == newNode.v) {
        nodeFound = true
        return Some((grandParent,parent,newNode))
      }
      if (!(newNode intersectsMinMax nodeToFind)) return None
      if ((newNode.left != null) && (nodeToFind.min < newNode.min)) { //it a typical binary seach tree using only min value
        if (newNode == root) nodeFoundOnLeftChildOfRoot = true
        iterate(parent, newNode, newNode.left)
      }
      else {
        if ((newNode.right != null) && (newNode == root)) nodeFoundOnLeftChildOfRoot = false
        iterate(parent, newNode, newNode.right)
      }
    }
    //-------------------------------------------------------------------------
    val r = iterate(null, null, root)
    if (!nodeFound) None
    else Some((nodeFoundOnLeftChildOfRoot, r.get._1, r.get._2, r.get._3))
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //(nodeFoundOnLeftChildOfRoot,grandParent,parent,foundNode)
  def deepFind(nodeToFind:TreeNode): Option[(Boolean,TreeNode,TreeNode,TreeNode)] = {
    //-------------------------------------------------------------------------
    var nodeFoundOnLeftChildOfRoot = false
    var nodeFound = false
    //-------------------------------------------------------------------------
    def iterate(grandParent: TreeNode, parent: TreeNode, newNode: TreeNode): Option[(TreeNode,TreeNode,TreeNode)] = {
      if (newNode == null) return None
      if (nodeToFind.v == newNode.v) {
        nodeFound = true
        return Some((grandParent,parent,newNode))
      }
      if (newNode.left != null && !nodeFound) {
        if (newNode == root) nodeFoundOnLeftChildOfRoot = true
        val r = iterate(parent, newNode, newNode.left)
        if (nodeFound) nodeFoundOnLeftChildOfRoot = true
        return r
      }
      if (newNode.right != null && !nodeFound) {
        if (newNode == root) nodeFoundOnLeftChildOfRoot = false
        val r = iterate(parent, newNode, newNode.right)
        if (nodeFound) nodeFoundOnLeftChildOfRoot = false
        return r
      }
      None
    }
    //-------------------------------------------------------------------------
    val r = iterate(null, null, root)
    if (!nodeFound) None
    else Some((nodeFoundOnLeftChildOfRoot, r.get._1, r.get._2, r.get._3))
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def prettyPrint(n: TreeNode = root, tabSpaceCount: Int = 1): Unit = {
    println(s"------------------ Binary search tree:'$name' starts ------------------")
    if (n == null) println("----Empty tree----")
    else TreePrinter.print(n, tabSpaceCount)
    println(s"------------------ Binary search tree:'$name' ends ------------------")
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//===========================================================================
sealed case class BasicTreeNode(var s: IntervalDataType, var e: IntervalDataType, var v: Int) extends TreeNode //also known as parabola
//===========================================================================
trait TreeNode extends PrintableNode {
  var s : IntervalDataType  //star
  var e : IntervalDataType  //end
  var v : STORED_VALUE_TYPE               //stored value of the node
  //-----------------------------------------------------------------------
  var left: TreeNode = null
  var right: TreeNode = null
  var max: IntervalDataType = e
  var min: IntervalDataType = s
  var height: Int = 1  // NEW node is initially added at leaf
  //-----------------------------------------------------------------------
  override def toString = s"[s:$s,e:$e](m:$min,M:$max)hFilterR:$height=>v:$v"
  //-----------------------------------------------------------------------
  def compareTo(n: TreeNode): IntervalDataType =
    if (s < n.s) -1
    else {
      if (s == n.s) {
        if ((e == n.e) && (v == n.v)) 0
        else {
          if (e < n.e) -1 else 1
        }
      }
      else 1
    }
  //-------------------------------------------------------------------------
  def isContiguous(itv: TreeNode) : Boolean =
    ((itv.e + 1) == s) ||    //itv is just on the right
    ((e + 1)    == itv.s)   //itv is just on the left
  //-------------------------------------------------------------------------
  def equalAsInterval(n: TreeNode) = (n.s >= s) && (n.e <= e)
  //-------------------------------------------------------------------------
  def equalAsInterval(itv: Interval) = (itv.s >= s) && (itv.e <= e)
  //-------------------------------------------------------------------------
  def hasIn(p: Int) = (p >= s) && (p <= e)
  //  //-------------------------------------------------------------------------
  def intersects(n: TreeNode) = hasIn(n.s) || hasIn(n.e) ||
                                n.hasIn(s) || n.hasIn(e)
  //-------------------------------------------------------------------------
  def intersectsMinMax (n: TreeNode) =
    (n.s >= min) && (n.s <= max) ||
    (n.e >= min) && (n.e <= max) ||
    (s >= n.min) && (s <= n.max) ||
    (e >= n.min) && (e <= n.max)

  //-------------------------------------------------------------------------
  def getLeft: PrintableNode = left
  //-------------------------------------------------------------------------
  def getRight: PrintableNode = right
  //-------------------------------------------------------------------------
  def getText: String = toString
  //-------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file AVLITree.scala
//=============================================================================