//=============================================================================
package com.common.dataType.tree.binary.printer
//=============================================================================
//adapted from : https://stackoverflow.com/questions/4965335/how-to-print-binary-tree-diagram/27153988#27153988
import scala.collection.mutable.ListBuffer
//=============================================================================
//=============================================================================
object TreePrinter {
  //===========================================================================
   trait PrintableNode {
    //-------------------------------------------------------------------------
    /** Get left child */
    def getLeft: PrintableNode
    //-------------------------------------------------------------------------
    /** Get right child */
    def getRight: PrintableNode
    //-------------------------------------------------------------------------
    /** Get text to be printed */
    def getText: String
    //-------------------------------------------------------------------------
    def isEmptyNode(n: PrintableNode) = n == null
    //-------------------------------------------------------------------------
    def isNotAnEmptyNode(n: PrintableNode) = !isEmptyNode(n)
    //-------------------------------------------------------------------------
  }
  //===========================================================================
  import com.common.dataType.tree.binary.search.EmptyNode.isNotAnEmptyNode
  //===========================================================================
  final val CHAR_DIVIDER_INVERTED_T =  '┴'
  final val CHAR_DIVIDER_L          =  '└'
  final val CHAR_DIVIDER_INVERTED_L =  '┘'
  final val STRING_DIVIDER_RIGHT    =  "┌"
  final val STRING_DIVIDER_LEFT     =  "┐"
  final val STRING_DIVIDER_MINUS    =  "─"
  //===========================================================================
  def printVertical(prefix: String, n: PrintableNode, isLeft: Boolean): Unit = {
    if (isNotAnEmptyNode(n)) {
      println(prefix + (if (isLeft) "L--" else "R--") + n.getText)
      printVertical(prefix + (if (isLeft) "|   " else "    "), n.getLeft, true)
      printVertical(prefix + (if (isLeft) "|   " else "    "), n.getRight, false)
    }
  }
  //===========================================================================
  def print(root: TreePrinter.PrintableNode,tabSpaceCount : Int = 4): Unit = {
    val lineSeq = ListBuffer[ListBuffer[String]]()
    var levelSeq = ListBuffer[TreePrinter.PrintableNode]()
    var nextSeq = ListBuffer[TreePrinter.PrintableNode]()
    
    levelSeq += root
    var nn = 1
    var widest = 0
    while ( nn != 0) {
      val line = ListBuffer[String]()
      nn = 0
      for (n <- levelSeq) {
        if (n == null) {
          line += null
          nextSeq += null
          nextSeq += null
        }
        else {
          val aa = n.getText
          line += aa
          if (aa.length > widest) widest = aa.length
          nextSeq += n.getLeft
          nextSeq += n.getRight
          if (isNotAnEmptyNode(n.getLeft)) nn += 1
          if (isNotAnEmptyNode(n.getRight)) nn += 1
        }
      }
      if (widest % 2 == 1) widest += 1
      
      lineSeq += line
      
      val tmp = levelSeq
      levelSeq = nextSeq
      nextSeq = tmp
      nextSeq.clear
    }
    var perpiece = lineSeq(lineSeq.size - 1).size * (widest + tabSpaceCount)
    var i = 0
    while ( i < lineSeq.size) {
      val line = lineSeq(i)
      val hpw = Math.floor(perpiece.toDouble / 2f).toInt - 1
      if (i > 0) {
        var j = 0
        while ( j < line.size) { // split node
          var c = ' '
          if (j % 2 == 1)
            if (line(j - 1) != null)
              c = if (line(j) != null) CHAR_DIVIDER_INVERTED_T else CHAR_DIVIDER_INVERTED_L
          else if (j < line.size && line(j) != null) c = CHAR_DIVIDER_L
          System.out.print(c)
          // lineSeq and spaces
          if (line(j) == null) for (k <- 0 until perpiece - 1) {
            System.out.print(" ")
          }
          else {
            for (k <- 0 until hpw) {
              System.out.print(if (j % 2 == 0) " "
              else STRING_DIVIDER_MINUS)
            }
            System.out.print(if (j % 2 == 0) STRING_DIVIDER_RIGHT
            else STRING_DIVIDER_LEFT)
            for (k <- 0 until hpw) {
              System.out.print(if (j % 2 == 0) STRING_DIVIDER_MINUS
              else " ")
            }
          }

          j += 1
        }
        println()
      }
      // print line of numbers
      var j = 0
      while ( {
        j < line.size
      }) {
        var f = line(j)
        if (f == null) f = ""
        val gap1 = Math.ceil(perpiece / 2f - f.length / 2f).toInt
        val gap2 = Math.floor(perpiece / 2f - f.length / 2f).toInt
        // a number
        for (_ <- 0 until gap1) {
          System.out.print(" ")
        }
        System.out.print(f)
        for (_ <- 0 until gap2) {
          System.out.print(" ")
        }

        j += 1
      }
      System.out.println()
      perpiece /= 2

      i += 1
    }
  }
  //===========================================================================
}
//=============================================================================