//=============================================================================
package com.common.dataType.tree.binary.search
//=============================================================================
//adapted from : https://gist.github.com/dholbrook/2967371
import com.common.dataType.tree.binary.printer.TreePrinter.PrintableNode
//=============================================================================
import scala.language.postfixOps
//=============================================================================
//mutable nodes!!!
//=============================================================================
trait NodeTrait[A] extends BinaryTree[A]{
  //---------------------------------------------------------------------------
  var v: A
  var l: BinaryTree[A]
  var r: BinaryTree[A]
  //---------------------------------------------------------------------------
  def isNodeALeaf() = (l == EmptyNode) && (r == EmptyNode)
  //---------------------------------------------------------------------------
}
//=============================================================================
trait LeafNodeTrait[A] extends BinaryTree[A] with PrintableNode {
  //---------------------------------------------------------------------------
  var v: A
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Node[A](var v: A, var l: BinaryTree[A], var r: BinaryTree[A]) extends NodeTrait[A]
//=============================================================================
case class LeafNode[A](var v: A) extends LeafNodeTrait[A]
//=============================================================================
case object EmptyNode extends BinaryTree[Nothing]
//=============================================================================
trait BinaryTree[+A] extends PrintableNode{
  //---------------------------------------------------------------------------
  import scala.annotation.tailrec
  //-------------------------------------------------------------------------
  def getLeft: PrintableNode = left.get
  //-------------------------------------------------------------------------
  def getRight: PrintableNode = right.get
  //-------------------------------------------------------------------------
  def getText: String = toString
  //---------------------------------------------------------------------------
  def value: Option[A] = this match {
    case n: Node[A]     => Some(n.v)
    case l: LeafNode[A] => Some(l.v)
    case EmptyNode      => None
  }
  //---------------------------------------------------------------------------
  def setValue[T](nv: T): Unit = this match {
    case n: Node[T @unchecked]     => n.v = nv
    case l: LeafNode[T @unchecked] => l.v = nv
    case EmptyNode      => None
  }
  //---------------------------------------------------------------------------
  def left: Option[BinaryTree[A]] = this match {
    case n: Node[A]     => Some(n.l)
    case _: LeafNode[A] => None
    case EmptyNode      => None
  }
  //---------------------------------------------------------------------------
  def right: Option[BinaryTree[A]] = this match {
    case n: Node[A]     => Some(n.r)
    case _: LeafNode[A] => None
    case EmptyNode      => None
  }
  //---------------------------------------------------------------------------
  /**
   * (*) Count the number of nodes in a binary tree.
   */
  def size: Int = walk(0) { (sum, v) => sum + 1 }

  //---------------------------------------------------------------------------
  /**
   * (*) Determine the height of a binary tree.
   * Definition:  The height of a tree is the length of the path from the root to the deepest node in the tree. A (rooted) tree with only one node (the root) has a height of zero.
   */
  def getHeight: Int = {
    //-------------------------------------------------------------------------
    def walk(t: BinaryTree[A]): Int = t match {
      case _: LeafNode[A] => 1
      case n: Node[A] => Seq(walk(n.left.get), walk(n.right.get)).max + 1
      case _          => 0
    }
    //-------------------------------------------------------------------------
    walk(this)-1
  }
  //---------------------------------------------------------------------------
  /**
   * (*) Count the number of leaves in a binary tree.
   */
  def leafCount: Int = {
    @tailrec
    def walk(t: List[BinaryTree[A]], z: Int): Int = t match {
      case (l: LeafNode[A]) :: remain => walk(remain, z + 1)
      case (n: Node[A]) :: remain     => walk(n.left.get :: n.right.get :: remain, z)
      case _ :: remain                => walk(remain, z)
      case _                          => z
    }
    walk(List(this), 0)
  }
  //---------------------------------------------------------------------------
  /**
   * Represents a deferred evaluation of a node value
   */
  //https://stackoverflow.com/questions/43180310/covariant-type-a-occurs-in-contravariant-position-in-type-a-of-value-a
  //https://www.geeksforgeeks.org/convert-normal-bst-balanced-bst/
  //covariant: return type
  //contravariant: parameter type
  private case class Eval[T](v: T) extends BinaryTree[T]
  //---------------------------------------------------------------------------
  @tailrec
  private def fold[T, B](a: List[BinaryTree[T]], z: B)
                        (f: (B, T) => B)
                        (o: (Node[T], List[BinaryTree[T]]) => List[BinaryTree[T]]): B =
    a match {
      case (n: Node[T]) :: remain     => fold(o(n, remain), z)(f)(o)   // never directly evaluate nodes, distribution.function o will create NEW accumulator
      case (l: LeafNode[T]) :: remain => fold(remain, f(z, l.v))(f)(o) // always evaluate Leaf
      case (e: Eval[T]) :: remain     => fold(remain, f(z, e.v))(f)(o) // always evaluate Eval
      case EmptyNode :: tl            => fold(tl, z)(f)(o)             // ignore Empty
      case _                          => z                             // will be Nil (empty list)
  }

  //---------------------------------------------------------------------------
  /**
   * fold with preorder traversal (root, left, right)
   * */
  def foldPreorder[B](z: B)(f: (B, A) => B): B = {
    fold(List(this), z)(f) { (n, tl) => Eval(n.v) :: n.l :: n.r :: tl }
  }
  //---------------------------------------------------------------------------
  /**
   * fold with inorder traversal (left, root, right)
   * */
  def foldInorder[B](z: B)(f: (B, A) => B): B = {
    fold(List(this), z)(f) { (n, tl) => n.l :: Eval(n.v) :: n.r :: tl }
  }
  //---------------------------------------------------------------------------
  /**
   * fold with postorder traversal (left, right, root)
   **/
  def foldPostorder[B](z: B)(f: (B, A) => B): B = {
    fold(List(this), z)(f) { (n, tl) => n.l :: n.r :: Eval(n.v) :: tl }
  }

  //---------------------------------------------------------------------------
  /**
   * fold with levelorder traversal
   **/
  def foldLevelOrder[B](z: B)(f: (B, A) => B): B = {
    fold(List(this), z)(f) { (n, tl) => (Eval(n.v) :: tl) ::: List(n.l, n.r) }
  }
  //---------------------------------------------------------------------------
  def walk[B](z: B)(f: (B, A) => B): B = foldInorder(z)(f)
  //---------------------------------------------------------------------------
  def toSeq: Seq[A] = walk(List[A]()) { (l, v) => v :: l } reverse
  //---------------------------------------------------------------------------
  def toSeqPreorder: Seq[A] = foldPreorder(List[A]()) { (l, v) => v :: l } reverse
  //---------------------------------------------------------------------------
  def toSeqInorder: Seq[A] = foldInorder(List[A]()) { (l, v) => v :: l } reverse
  //---------------------------------------------------------------------------
  def toSeqPostOrder: Seq[A] = foldPostorder(List[A]()) { (l, v) => v :: l } reverse
  //---------------------------------------------------------------------------
  def toSeqLevelOrder: Seq[A] = foldLevelOrder(List[A]()) { (l, v) => v :: l } reverse
  //---------------------------------------------------------------------------
  //double tail recursive call, so it not possible to use @tail rec
  //Mitigation: increase the JVM stack accordingly with parameter (to set stack up to 8MiB): -Xss8M
  def applyPredicate[T](node: BinaryTree[T], predicate: T => Boolean) : Unit  = {
    //-------------------------------------------------------------------------
    def applyPredicate(node: Option[BinaryTree[T]]) : Unit  = {
      node.get match {
        case EmptyNode =>
        case _ => {
          val nv = node.get.value.get
          predicate(nv)
          applyPredicate(node.get.left)
          applyPredicate(node.get.right)
        }
      }
    }
    //-------------------------------------------------------------------------
    applyPredicate(Some(node))
  }

  //---------------------------------------------------------------------------
  //double tail recursive call, so it not possible to use @tail rec
  //Mitigation: increase the JVM stack accordingly with parameter (to set stack up to 8MiB): -Xss8M
  def applyFilter[T](node: BinaryTree[T], predicate: T => Boolean = null): List[T]  = {
    //---------------------------------------------------------------------------
    //double tail recursive call, so it not possible to use @tail rec
    //Mitigation: increase the JVM stack accordingly with parameter (to set stack up to 8MiB): -Xss8M
    def applyFilter(node: Option[BinaryTree[T]]) : List[T]  = {
      if (node.isEmpty) List[T]()
      else {
        node.get match {
          case EmptyNode => List[T]()
          case _ => {
            val nv = node.get.value.get
            val pr = if (predicate(nv)) List(nv) else List[T]()
            val left =  applyFilter(node.get.left)
            val right = applyFilter(node.get.right)
            pr ++ left ++ right
          }
        }
      }
    }
    //---------------------------------------------------------------------------
    applyFilter(Some(node))
    //---------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //https://leetcode.com/problems/print-binary-tree/solution/
  def prettyPrint[T](node: BinaryTree[T], divider: String="\t") : Unit = {
    //-------------------------------------------------------------------------
    processTree(node)
    //-----------------------------------------------------------------------
    def fill(result: Array[Array[String]], node:Option[BinaryTree[T]], i: Int, l: Int, r: Int): Unit = {
      if (node.isEmpty)  {
        result(i)((l + r) / 2) = "x"
        return
      }
      node.get match {
        case _: LeafNode[T] =>
          result(i)((l + r) / 2) = node.get.value.get.toString
          fill(result, None, i + 1, l, (l + r) / 2)
          fill(result, None, i + 1, (l + r + 1) / 2, r)
        case n: Node[T] =>
          result(i)((l + r) / 2) = node.get.value.get.toString
          fill(result, n.left,  i + 1, l, (l + r) / 2)
          fill(result, n.right, i + 1, (l + r + 1) / 2, r)
        case _ => result(i)((l + r) / 2) = "x"
      }
    }
    //-----------------------------------------------------------------------
    def processTree(node: BinaryTree[T]) = {
      val height = (node getHeight) + 2
      val width = (1 << height) - 1
      val resultByLevel = if (height == 0)  Array(Array[String]("<--- Tree is empty -->"))
      else Array.ofDim[String](height, width)
      if (height != 0){
        resultByLevel.foreach(a => for (i <- 0 until a.length) a(i) = "" * a.length)
        fill(resultByLevel, Some(node), 0, 0, resultByLevel(0).length)
      }
      resultByLevel.foreach(a => println(a.mkString(divider)))
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def getBalanceFactor() : Int = {
    this match {
      case EmptyNode       => 0
      case _: LeafNode[A]  => 0
      case n: Node[A]      =>  n.r.getHeight - n.l.getHeight
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file BeachLine.scala
//=============================================================================