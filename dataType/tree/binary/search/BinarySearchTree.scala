//=============================================================================
package com.common.dataType.tree.binary.search
//=============================================================================
//adapted from "Introduction to the Art of Programming Using Scala" Mark C. Lewis
//=============================================================================
object BinarySearchTree {
  //-------------------------------------------------------------------------
  def apply[A](seq: Seq[A])(comparator:(A,A) => Int) : BinarySearchTree[A] = {
    //-------------------------------------------------------------------------
    val tree = BinarySearchTree[A](comparator)
    seq foreach ( tree += _ )
    tree
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//duplicated values are allowed but only a single instance is stored
case class BinarySearchTree[T](comparator : (T,T) => Int, name: String = "No name")  extends BinarySearchTreeTrait[T]
//=============================================================================

//=============================================================================
//End of file BinarySearchTree.scala
//=============================================================================
