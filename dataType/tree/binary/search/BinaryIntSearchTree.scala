//=============================================================================
package com.common.dataType.tree.binary.search
//=============================================================================
//=============================================================================
//duplicated values are allowed but only a single instance is stored
class BinaryIntSearchTree(name: String  = "No name") extends
  BinarySearchTree[Int]( { (a, b) => Ordering[Int].compare (a, b)}, name )
//=============================================================================

//=============================================================================
//End of file BinaryIntSearchTree.scala
//=============================================================================