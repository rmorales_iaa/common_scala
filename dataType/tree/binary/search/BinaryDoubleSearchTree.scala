//=============================================================================
package com.common.dataType.tree.binary.search
//=============================================================================
//=============================================================================
//duplicated values are allowed but only a single instance is stored
class BinaryDoubleSearchTree(name: String  = "No name") extends
  BinarySearchTree[Double]( { (a, b) => Ordering[Double].compare (a, b)}, name )
//=============================================================================

//=============================================================================
//End of file BinaryDoubleSearchTree.scala
//=============================================================================