//=============================================================================
package com.common.dataType.tree.binary.search
//=============================================================================
//adapted from "Introduction to the Art of Programming Using Scala" Mark C. Lewis
//=============================================================================
import scala.annotation.tailrec
//=============================================================================
//=============================================================================
//duplicated values are allowed but only a single instance is stored
trait BinarySearchTreeTrait[T] {
  //---------------------------------------------------------------------------
  val name: String
  protected val comparator : (T,T) => Int
  protected var root : BinaryTree[T] = EmptyNode
  //---------------------------------------------------------------------------
  def getRoot = root
  //---------------------------------------------------------------------------
  def prettyPrint(divider: String="\t") : Unit = {
    println(s"------------------ Binary search tree:'$name' starts ------------------")
    root.prettyPrint(root, divider)
    println(s"------------------ Binary search tree:'$name' ends ------------------")
  }
  //---------------------------------------------------------------------------
  def +=(newValue:T)  = {
    //-------------------------------------------------------------------------
    add(newValue, root, EmptyNode)
    //-------------------------------------------------------------------------
    @tailrec
    def add(newValue:T, node: BinaryTree[T], parent: BinaryTree[T]): Unit = {
      node match {
        case EmptyNode =>
          parent match {
            case EmptyNode => root = LeafNode(newValue) //empty tree
            case pNode: Node[T] => {  //update the proper child
              if (pNode.l == node) pNode.l = LeafNode(newValue)
              else pNode.r = LeafNode(newValue)
            }
            case  _ =>  //it cannot be a leaf, so do nothing
          }

        case leaf: LeafNode[T] =>
          val newLeaf = LeafNode[T](newValue)
          val c = comparator(newValue, leaf.v)
          val newNode = c match {
            case v if v < 0 => Some(Node(node.value.get, newLeaf, EmptyNode))
            case v if v > 0 => Some(Node(node.value.get, EmptyNode, newLeaf))
            case _ => None   // duplicated value, just keep the current one
          }
          if (newNode.isDefined)
            parent match {
              case EmptyNode => root = newNode.get
              case pNode: Node[T] =>
                if(pNode.l == node) pNode.l = newNode.get
                else pNode.r = newNode.get
              case  _ =>  //it cannot be a leaf, so do nothing
            }

        case n: Node[T] => comparator(newValue, n.v) match {
          case v if v < 0 => add(newValue, n.l, n)
          case v if v > 0 => add(newValue, n.r, n)
          case _ => // duplicated value, just keep the current one
        }
      }
    }
    //-------------------------------------------------------------------------
  }

  //---------------------------------------------------------------------------
  //sort and add to balance the tree
  def += (seq: Seq[T]): Unit  = {
    //-------------------------------------------------------------------------
    val d = seq.sortWith((a, b) => comparator(a, b) < 0).toIndexedSeq
    //-------------------------------------------------------------------------
    def insertOrderedSeq(seq: IndexedSeq[T]): Unit ={
      val mid =  seq.length / 2
      this += seq(mid)
      for(i <- 1 until mid+1) {
        this += seq(mid - i)
        this += seq(mid + i)
      }
    }
    //-------------------------------------------------------------------------
    if ((seq.length % 2) == 0) { //if even size, make it odd and insert the first one
      insertOrderedSeq(d.drop(1))
      this += d.head
    }
    else insertOrderedSeq(d)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def filterByRange(min: T, max: T, predicate: T => Boolean = null): List[T]  = {
    //-------------------------------------------------------------------------
    //double tail recursive call, so it not possible to use @tail rec
    //Mitigation: increase the JVM stack accordingly with parameter (to set stack up to 8MiB): -Xss8M
    def getRange(node: Option[BinaryTree[T]]) : List[T]  = {
      if (node.isEmpty) List[T]()
      else {
        node.get match {
          case EmptyNode => List[T]()
          case _ => {
            val nv = node.get.value.get
            val cMin = comparator(nv, min)
            val cMax = comparator(nv, max)
            val pr =
              if ((cMin >= 0) && (cMax <= 0)) {
                if (predicate == null) List(nv)
                else {
                  if (predicate(nv)) List(nv)
                  else List[T]()
                }
              }
              else List[T]()

            val left =  if (cMin > 0) getRange(node.get.left)  else List[T]()
            val right = if (cMax < 0) getRange(node.get.right) else List[T]()
            pr ++ left ++ right
          }
        }
      }
    }
    //-------------------------------------------------------------------------
    getRange(Some(root))
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def exist(v: T): Boolean  = {
    //-------------------------------------------------------------------------
    var found = false
    //-------------------------------------------------------------------------
    def exist(node: Option[BinaryTree[T]]): Unit = {
      if (found) return
      if (node.isEmpty) return
      node.get match {
        case EmptyNode =>
        case _ => {
          val c = comparator(v, node.get.value.get)
          if (c == 0) found = true
          else {
            if (c < 0) exist(node.get.left)
            if (!found && c > 0) exist(node.get.right)
          }
        }
      }
    }
    //-------------------------------------------------------------------------
    exist(Some(root))
    found
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //return: (node,parent)
  def findNode(v: T): (Option[BinaryTree[T]],BinaryTree[T]) = {
    //-------------------------------------------------------------------------
    var foundNode : Option[BinaryTree[T]] = None
    var foundParent: BinaryTree[T] = EmptyNode
    //-------------------------------------------------------------------------
    def findNode(node: Option[BinaryTree[T]], parent: BinaryTree[T]): Unit = {
      if (foundNode.isDefined) return
      if (node.isEmpty) return
      node.get match {
        case EmptyNode =>
        case _ => {
          val c = comparator(v, node.get.value.get)
          if (c == 0) {
            foundNode = node
            foundParent = parent
          }
          else {
            if (c < 0) findNode(node.get.left, node.get)
            if (foundNode.isEmpty && c > 0) findNode(node.get.right, node.get)
          }
        }
      }
    }
    //-------------------------------------------------------------------------
    findNode(Some(root), EmptyNode)
    (foundNode,foundParent)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private def findParent(nodeToFind: BinaryTree[T]): Option[BinaryTree[T]]  = {
    //-------------------------------------------------------------------------
    var foundParent : Option[BinaryTree[T]] = None
    //-------------------------------------------------------------------------
    def findNode(node: Option[BinaryTree[T]], parent: BinaryTree[T]): Unit = {
      if (foundParent.isDefined) return
      if (node.isEmpty) return
      node.get match {
        case EmptyNode =>
        case _ => {
          if (node.get == nodeToFind) foundParent = Some(parent)
          else {
            findNode(node.get.left, node.get)
            if (foundParent.isEmpty) findNode(node.get.right, node.get)
          }
        }
      }
    }
    //-------------------------------------------------------------------------
    findNode(Some(root), EmptyNode)
    foundParent
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def findMaxNode(node: BinaryTree[T] = root, parent: BinaryTree[T] = EmptyNode): (BinaryTree[T],BinaryTree[T]) = {
    //-------------------------------------------------------------------------
    @tailrec
    def findMaxNode(node: Option[BinaryTree[T]], parent:BinaryTree[T]): (BinaryTree[T],BinaryTree[T])  = {
      if (node.isEmpty) (EmptyNode,EmptyNode)
      else{
        node.get match {
          case EmptyNode          => (EmptyNode,EmptyNode)
          case leaf: LeafNode[T]  => (leaf, parent)
          case n: Node[T]         => if (n.right.get == EmptyNode) (n,parent) else findMaxNode(n.right,n)
        }
      }
    }
    //-------------------------------------------------------------------------
    findMaxNode(Some(node), parent)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def findMaxValue(node: BinaryTree[T]): Option[T] = findMaxNode(node,findParent(node).get)._1.value
  //---------------------------------------------------------------------------
  def findMaxValue(): Option[T] = findMaxNode()._1.value
  //---------------------------------------------------------------------------
  def findMinNode(node: BinaryTree[T] = root, parent:BinaryTree[T] = EmptyNode): (BinaryTree[T],BinaryTree[T])  = {
    //-------------------------------------------------------------------------
    @tailrec
    def findMinNode(node: Option[BinaryTree[T]], parent:BinaryTree[T]): (BinaryTree[T],BinaryTree[T]) = {
      if (node.isEmpty) (EmptyNode,EmptyNode)
      else{
        node.get match {
          case EmptyNode          => (EmptyNode,EmptyNode)
          case leaf: LeafNode[T]  => (leaf, parent)
          case n: Node[T]         => if (n.left.get == EmptyNode) (n,parent) else findMinNode(n.left,n)
        }
      }
    }
    //-------------------------------------------------------------------------
    findMinNode(Some(node), parent)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def findMinValue(node: BinaryTree[T]): Option[T] = findMinNode(node, findParent(node).get)._1.value
  //---------------------------------------------------------------------------
  def findMinValue(): Option[T] = findMaxNode()._1.value
  //---------------------------------------------------------------------------
  //https://appliedgo.net/bintree/
  private def deleteNode(node: BinaryTree[T], parent: BinaryTree[T]) : Unit = {
    node match {
      case leaf: LeafNode[T] =>
        parent match {
          case EmptyNode => root = EmptyNode //empty tree
          case pNode: Node[T] => {
            if (pNode.l == leaf) pNode.l = EmptyNode
            else pNode.r = EmptyNode
            if (pNode.isNodeALeaf()) { //parent become a leaf node
              val grandParent = findParent(pNode)
              if (grandParent.isEmpty) root = LeafNode(root.value.get)
              else
                grandParent.get match {
                  case EmptyNode => root = LeafNode(root.value.get)
                  case gpNode: Node[T] =>
                    if (gpNode.l == pNode) gpNode.l = LeafNode(pNode.value.get)
                    else gpNode.r = LeafNode(pNode.value.get)
                  case _ => //it cannot be a leaf, so do nothing
              }
            }
          }
          case _ => //it cannot be a leaf, so do nothing
        }

      case n: Node[T] =>
        parent match {
          case EmptyNode => {   //deleting root, just use the left branch
            val (maxNode, parentMaxNode) = findMaxNode(n.left.get, n)
            root.setValue(maxNode.value.get)
            deleteNode(maxNode, parentMaxNode) //delete duplicated
          }
          case pNode: Node[T] => {
            if(pNode.l == n){
              val (substituteNode, parentSubstituteNode) = if(n.l == EmptyNode) findMinNode(n.r,n) else findMaxNode(n.l,n)
              pNode.l.setValue(substituteNode.value.get)
              deleteNode(substituteNode, parentSubstituteNode) //delete duplicated
            }
            else{
              val (substituteNode, parentSubstituteNode) = if(n.r == EmptyNode) findMaxNode(n.l,n) else findMinNode(n.r,n)
              pNode.r.setValue(substituteNode.value.get)
              deleteNode(substituteNode, parentSubstituteNode) //delete duplicated
            }
          }
        }
      case _ => //can not be empty, so do nothing
    }
  }
  //---------------------------------------------------------------------------
  def delete(node: BinaryTree[T]): Boolean = {
    if (node == EmptyNode) {
      println("Error. Can not delete an Empty node")
      return false
    }
    val parent = findParent(node)
    if (parent.isEmpty) false
    else {
      deleteNode(node, parent.get)
      true
    }
  }
  //---------------------------------------------------------------------------
  def delete(v: T): Boolean = {
    val (node, parent) = findNode(v)
    if (node.isEmpty) false
    else {
      if (node == EmptyNode) {
        println("Error. Can not delete an Empty node")
        false
      }
      else {
        deleteNode(node.get, parent)
        true
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================

//=============================================================================
//End of file BinarySearchTree.scala
//=============================================================================
