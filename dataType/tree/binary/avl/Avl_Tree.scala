/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  20/Feb/2020
 * Time:  20h:28m
 * Description: Mutable Augmented AVL (Adelson-Velskii y Landis) Interval Tree (self-balancing binary search tree)
 * Based on: https://www.geeksforgeeks.org/avl-tree-set-1-insertion/
 *           https://www.geeksforgeeks.org/avl-tree-set-2-deletion/
 *           http://www.davismol.net/2016/02/07/data-structures-augmented-interval-tree-to-search-for-interval-overlapping/
 * Duplicated values will be ignored
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.dataType.tree.binary.avl
//=============================================================================
import scala.annotation.tailrec
//=============================================================================
import com.common.dataType.tree.binary.printer.TreePrinter
import com.common.dataType.tree.binary.printer.TreePrinter.PrintableNode
import com.common.dataType.tree.binary.avl.AVL_Tree.KEY_VALUE_TYPE
//=============================================================================
object AVL_Tree{
  //---------------------------------------------------------------------------
  type KEY_VALUE_TYPE = Double
  final val INVALID_VALUE = Int.MinValue
  //---------------------------------------------------------------------------
}
//=============================================================================
//it a typical binary seach tree using only min value of the nodes
case class AVL_Tree(name: String = "No Name") {
  //---------------------------------------------------------------------------
  protected var root: TreeNode = null
  //---------------------------------------------------------------------------
  def getRoot = root
  //---------------------------------------------------------------------------
  def +=(w: KEY_VALUE_TYPE) = root = insert(root, BasicTreeNode(w))
  //---------------------------------------------------------------------------
  def isBalanced(n: TreeNode) = {
    val b = getBalanceFactor(n)
    (b < -1) || (b > 1)
  }
  //---------------------------------------------------------------------------
  private def getBalanceFactor(n: TreeNode): Int =
    if (n == null) 0 else height(n.left) - height(n.right)
  //---------------------------------------------------------------------------
  def height(n: TreeNode): Int = if (n == null) 0 else n.height
  //--------------------------------------------------------------------------
  protected def copy(a: TreeNode, b: TreeNode): Unit = {
    a.v = b.v
    a.left = b.left
    a.right = b.right
    a.height = b.height
  }
  //--------------------------------------------------------------------------
  private def getMinNode(n: TreeNode): TreeNode = {
    var current = n
    while (current.left != null)
      current = current.left
    current
  }
  //---------------------------------------------------------------------------
  protected def updateHeight(n: TreeNode) =
    n.height = 1 + Math.max(height(n.left), height(n.right))
  //---------------------------------------------------------------------------
  protected def updateHeightDeeply(n: TreeNode) : Unit = {
    if (n.left != null) updateHeightDeeply(n.left)
    if (n.right != null) updateHeightDeeply(n.right)
    updateHeight(n)
  }
  //---------------------------------------------------------------------------
  protected def setHeight(n: TreeNode, h: Int) = n.height = h
  //---------------------------------------------------------------------------
  protected def rightRotation(currentRoot: TreeNode): TreeNode = {
    val newRoot = currentRoot.left
    val adoptedNode = newRoot.right

    newRoot.right = currentRoot
    currentRoot.left = adoptedNode

    // update heights, min and max
    updateHeight(currentRoot)
    updateHeight(newRoot)

    newRoot
  }
  //---------------------------------------------------------------------------
  protected def leftRotation(currentRoot: TreeNode): TreeNode = {
    val newRoot = currentRoot.right
    val adoptedNode = newRoot.left

    newRoot.left = currentRoot
    currentRoot.right = adoptedNode

    //update heights, min and max
    updateHeight(currentRoot)
    updateHeight(newRoot)

    newRoot
  }
  //--------------------------------------------------------------------------
  protected def insert(visitedNode: TreeNode, nodeToInsert: TreeNode): TreeNode = {
    //------------------------------------------------------------------------
    /* 1.  Perform the normal BST (binary search tree) insertion */
    if (visitedNode == null) return nodeToInsert
    nodeToInsert.compareTo(visitedNode) match {
      case 0 =>  return visitedNode //duplicated key. Final location depends on 'compareTo'
      case c if c <= 0 => visitedNode.left  = insert(visitedNode.left, nodeToInsert)
      case c if c > 0 => visitedNode.right = insert(visitedNode.right, nodeToInsert)
    }
    //Update height, max, min of this ancestor AVLINode
    updateHeight(visitedNode)

    // 3. Get the balance factor of this ancestor AVLINode to check whether this node became unbalanced
    val balance = getBalanceFactor(visitedNode)

    //left left case
    if (balance > 1 && nodeToInsert.compareTo(visitedNode.left) < 0) return rightRotation(visitedNode)

    //left right case
    if (balance > 1 && nodeToInsert.compareTo(visitedNode.left) > 0) {
      visitedNode.left = leftRotation(visitedNode.left)
      return rightRotation(visitedNode)
    }

    //right right case
    if (balance < -1 && nodeToInsert.compareTo(visitedNode.right) > 0) return leftRotation(visitedNode)

    //right left case
    if (balance < -1 && nodeToInsert.compareTo(visitedNode.right) < 0) {
      visitedNode.right = rightRotation(visitedNode.right)
      return leftRotation(visitedNode)
    }
    visitedNode
  }
  //--------------------------------------------------------------------------
  def delete(w: KEY_VALUE_TYPE): Unit = root = delete(root, BasicTreeNode(w))
  //--------------------------------------------------------------------------
  private def delete(n: TreeNode, inNode: TreeNode): TreeNode = {
    // Step 1: Perform standard BST delete
    if (n == null) return n
    inNode.compareTo(n) match {
      case c if c < 0 => n.left = delete(n.left, inNode)
      case c if c > 0 => n.right = delete(n.right, inNode)
      case 0 => //node found

        // node with only one child or no child
        if ((n.left == null) || (n.right == null)) {
          var temp = if (n.left != null) n.left else n.right
          // No child case
          if (temp == null) {
            temp = n
            return null //original code 'n = NULL' and later 'return n'
          }
          else copy(n, temp) // One child case. Copy the contents of the non-empty child
        }
        else { // node with two children: Get the inorder successor (smallest in the right subtree)
          val temp = getMinNode(n.right)

          // Copy the inorder successor's data to this node
          n.v = temp.v

          // Delete the inorder successor
          n.right = delete(n.right, temp)
        }
    }

    // If the tree had only one node then return
    if (n == null) return n

    // Step 2: Update height, min, max of the current node
    updateHeight(n)

    // Step 3: Get the balance factor of thi node (to check whether this node became unbalanced)
    val balance = getBalanceFactor(n)

    //if this node becomes unbalanced, then there are 4 cases
    //left left Case
    if (balance > 1 && getBalanceFactor(n.left) >= 0)
      return rightRotation(n)

    //left right Case
    if (balance > 1 && getBalanceFactor(n.left) < 0) {
      n.left = leftRotation(n.left)
      return rightRotation(n)
    }

    //right right Case
    if (balance < -1 && getBalanceFactor(n.right) <= 0)
      return leftRotation(n)

    //right left Case
    if (balance < -1 && getBalanceFactor(n.right) > 0) {
      n.right = rightRotation(n.right)
      return leftRotation(n)
    }
    n
  }
  //---------------------------------------------------------------------------
  def find(k: KEY_VALUE_TYPE): Option[KEY_VALUE_TYPE] = find(BasicTreeNode(k))
  //---------------------------------------------------------------------------
  private def find(nodeToFind:TreeNode): Option[KEY_VALUE_TYPE] = {
    //-------------------------------------------------------------------------
    @tailrec
    def iterate(n: TreeNode): Option[KEY_VALUE_TYPE] = {
      if (n == null)  None
      else {
        nodeToFind.compareTo(n) match {
          case c if c < 0 => iterate(n.left)
          case c if c > 0 => iterate(n.right)
          case 0 => Some(n.v)
        }
      }
    }
    //-------------------------------------------------------------------------
    val r = iterate(root)
    if (r.isDefined) Some(r.get)
    else None
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //postorder traversal : (left, right, root)
  def postOrder(n: TreeNode = root): Seq[KEY_VALUE_TYPE] = {
    if (n == null) Seq()
    else postOrder(n.left) ++ postOrder(n.right) ++ Seq(n.v)
  }
  //---------------------------------------------------------------------------
  def getFirstLeftLeaf(): Option[KEY_VALUE_TYPE] = {
    //-------------------------------------------------------------------------
    var firstLeaf : TreeNode = null
    //-------------------------------------------------------------------------
    def iterate(n: TreeNode) : Unit = {
      if ((n != null) && (firstLeaf == null)) {
        if (firstLeaf == null) iterate(n.left)
        if (firstLeaf == null) iterate(n.right)
        if (firstLeaf == null) firstLeaf = n
      }
    }
    //-------------------------------------------------------------------------
    if (root == null) None
    else {
      iterate(root)
      Some(firstLeaf.v)
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def prettyPrint(n: TreeNode = root, tabSpaceCount: Int = 1): Unit = {
    println(s"------------------ Binary search tree:'$name' starts ------------------")
    if (n == null) println("----Empty tree----")
    else TreePrinter.print(n, tabSpaceCount)
    println(s"------------------ Binary search tree:'$name' ends ------------------")
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def printVertical(n: TreeNode = root): Unit = {
    println(s"------------------ Binary search tree:'$name' starts ------------------")
    if (n == null) println("----Empty tree----")
    else TreePrinter.printVertical("",n,false)
    println(s"------------------ Binary search tree:'$name' ends ------------------")
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//===========================================================================
//===========================================================================
sealed case class BasicTreeNode(var v: KEY_VALUE_TYPE) extends TreeNode //also known as parabola
//===========================================================================
trait TreeNode extends PrintableNode {
  var v : KEY_VALUE_TYPE               //stored value of the node
  var left: TreeNode = null
  var right: TreeNode = null
  //-----------------------------------------------------------------------
  var height: Int = 1  // NEW node is initially added at leaf
  //-----------------------------------------------------------------------
  override def toString = s"[v:$v]:hFilterR:$height"
  //-----------------------------------------------------------------------
  def compareTo(n: TreeNode): Int = v compareTo  n.v
  //-------------------------------------------------------------------------
  def getLeft: PrintableNode = left
  //-------------------------------------------------------------------------
  def getRight: PrintableNode = right
  //-------------------------------------------------------------------------
  def getText: String = toString
  //-------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file AVL_Tree.scala
//=============================================================================