/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  20/Feb/2020
 * Time:  20h:28m
 * Description: Mutable Augmented AVL (Adelson-Velskii y Landis) Interval Tree (self-balancing binary search tree)
 *              that allows duplicates. A node 'n' with key 'k', implies that nodes with values less than 'k'
 *              will be found on left child, values great than 'k' on right child and equal to 'k' in the same node 'n'
 *              The bottom left child has the minor key.
 *              This class can be used as PriorityQueue with deletion capabilities
 * Based on: https://www.geeksforgeeks.org/avl-tree-set-1-insertion/
 *           https://www.geeksforgeeks.org/avl-tree-set-2-deletion/
 *           http://www.davismol.net/2016/02/07/data-structures-augmented-interval-tree-to-search-for-interval-overlapping/
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.dataType.tree.binary.avl
//=============================================================================
import scala.annotation.tailrec
import scala.collection.mutable.ArrayBuffer
import scala.reflect.ClassTag
//=============================================================================
import com.common.dataType.tree.binary.printer.TreePrinter
import com.common.dataType.tree.binary.printer.TreePrinter.PrintableNode
import com.common.dataType.tree.binary.avl.AVL_Tree.KEY_VALUE_TYPE
//=============================================================================
object AVL_TreeWithDuplicates{
  //---------------------------------------------------------------------------
  type KEY_VALUE_TYPE = Double
  final val INVALID_VALUE = Int.MinValue
  //---------------------------------------------------------------------------
}

//=============================================================================
//it a typical binary seach tree using only min value of the nodes
case class AVL_TreeWithDuplicates[V : ClassTag](name: String = "No Name") {
  //===========================================================================
  protected var root: TreeNode[V] = null
  protected var nodeCount = 0
  //---------------------------------------------------------------------------
  def getRoot = root
  //---------------------------------------------------------------------------
  def size = nodeCount
  //---------------------------------------------------------------------------
  def +=(k: KEY_VALUE_TYPE, v: V) = {
    root = insert(root, new TreeNode[V](k, v))
    nodeCount += 1
  }
  //---------------------------------------------------------------------------
  def isBalanced(n: TreeNode[V]) = {
    val b = getBalanceFactor(n)
    (b < -1) || (b > 1)
  }
  //---------------------------------------------------------------------------
  private def getBalanceFactor(n: TreeNode[V]): Int =
    if (n == null) 0 else height(n.left) - height(n.right)
  //---------------------------------------------------------------------------
  def height(n: TreeNode[V]): Int = if (n == null) 0 else n.height
  //--------------------------------------------------------------------------
  protected def copy(a: TreeNode[V], b: TreeNode[V]): Unit = {
    a.k = b.k
    a.storage.clear()
    a.storage ++= b.storage
    a.left = b.left
    a.right = b.right
    a.height = b.height
  }
  //--------------------------------------------------------------------------
  private def getMinNode(n: TreeNode[V]): TreeNode[V] = {
    var current = n
    while (current.left != null)
      current = current.left
    current
  }
  //---------------------------------------------------------------------------
  protected def updateHeight(n: TreeNode[V]) =
    n.height = 1 + Math.max(height(n.left), height(n.right))
  //---------------------------------------------------------------------------
  protected def updateHeightDeeply(n: TreeNode[V]) : Unit = {
    if (n.left != null) updateHeightDeeply(n.left)
    if (n.right != null) updateHeightDeeply(n.right)
    updateHeight(n)
  }
  //---------------------------------------------------------------------------
  protected def setHeight(n: TreeNode[V], h: Int) = n.height = h
  //---------------------------------------------------------------------------
  protected def rightRotation(currentRoot: TreeNode[V]): TreeNode[V] = {
    val newRoot = currentRoot.left
    val adoptedNode = newRoot.right

    newRoot.right = currentRoot
    currentRoot.left = adoptedNode

    // update heights, min and max
    updateHeight(currentRoot)
    updateHeight(newRoot)

    newRoot
  }
  //---------------------------------------------------------------------------
  protected def leftRotation(currentRoot: TreeNode[V]): TreeNode[V] = {
    val newRoot = currentRoot.right
    val adoptedNode = newRoot.left

    newRoot.left = currentRoot
    currentRoot.right = adoptedNode

    //update heights, min and max
    updateHeight(currentRoot)
    updateHeight(newRoot)

    newRoot
  }
  //--------------------------------------------------------------------------
  protected def insert(visitedNode: TreeNode[V], nodeToInsert: TreeNode[V]): TreeNode[V] = {
    //------------------------------------------------------------------------
    /* 1.  Perform the normal BST (binary search tree) insertion */
    if (visitedNode == null) return nodeToInsert
    nodeToInsert.compare(visitedNode) match {
      case 0 =>  visitedNode.addWithEqualKey(nodeToInsert.storage.head)   //duplicated key
                 return visitedNode
      case c if c < 0 => visitedNode.left  = insert(visitedNode.left, nodeToInsert)
      case c if c > 0 => visitedNode.right = insert(visitedNode.right, nodeToInsert)
    }
    //Update height, max, min of this ancestor AVLINode
    updateHeight(visitedNode)

    // 3. Get the balance factor of this ancestor AVLINode to check whether this node became unbalanced
    val balance = getBalanceFactor(visitedNode)

    //left left case
    if (balance > 1 && nodeToInsert.compare(visitedNode.left) < 0)
      return rightRotation(visitedNode)


    //left right case
    if (balance > 1 && nodeToInsert.compare(visitedNode.left) > 0) {
      visitedNode.left = leftRotation(visitedNode.left)
      return rightRotation(visitedNode)
    }

    //right right case
    if (balance < -1 && nodeToInsert.compare(visitedNode.right) > 0) return leftRotation(visitedNode)

    //right left case
    if (balance < -1 && nodeToInsert.compare(visitedNode.right) < 0) {
      visitedNode.right = rightRotation(visitedNode.right)
      return leftRotation(visitedNode)
    }
    visitedNode
  }
  //--------------------------------------------------------------------------
  //it deletes the fits ocuurence of 'k' (in case of duplicated values, can exsit more than one node with the same key 'k'
  //When there is onlu one 'k' ocurrence, the node will be deleted also, in othe case it remains
  def -=(k: KEY_VALUE_TYPE): Unit = {
    root = delete(root, TreeNode(k, null))
    nodeCount -= 1
  }
  //--------------------------------------------------------------------------
  private def delete(n: TreeNode[V], inNode: TreeNode[V]): TreeNode[V] = {
    // Step 1: Perform standard BST delete
    if (n == null) return n
    inNode.compare(n) match {
      case c if c < 0 => n.left = delete(n.left, inNode)
      case c if c > 0 => n.right = delete(n.right, inNode)
      case 0 =>
        if (n.getCountEqualKey > 1) n.removeFirstWithEqualKey()
        else {
          // node with only one child or no child
          if ((n.left == null) || (n.right == null)) {
            var temp = if (n.left != null) n.left else n.right
            // No child case
            if (temp == null) {
              temp = n
              return null //original code 'n = NULL' and later 'return n'
            }
            else copy(n, temp) // One child case. Copy the contents of the non-empty child
          }
          else { // node with two children: Get the inorder successor (smallest in the right subtree)
            val temp = getMinNode(n.right)

            // Copy the inorder successor's data to this node
            n.k = temp.k

            // Delete the inorder successor
            n.right = delete(n.right, temp)
          }
        }
    }

    // If the tree had only one node then return
    if (n == null) return n

    // Step 2: Update height, min, max of the current node
    updateHeight(n)

    // Step 3: Get the balance factor of thi node (to check whether this node became unbalanced)
    val balance = getBalanceFactor(n)

    //if this node becomes unbalanced, then there are 4 cases
    //left left Case
    if (balance > 1 && getBalanceFactor(n.left) >= 0)
      return rightRotation(n)

    //left right Case
    if (balance > 1 && getBalanceFactor(n.left) < 0) {
      n.left = leftRotation(n.left)
      return rightRotation(n)
    }

    //right right Case
    if (balance < -1 && getBalanceFactor(n.right) <= 0)
      return leftRotation(n)

    //right left Case
    if (balance < -1 && getBalanceFactor(n.right) > 0) {
      n.right = rightRotation(n.right)
      return leftRotation(n)
    }
    n
  }
  //---------------------------------------------------------------------------
  def find(k: KEY_VALUE_TYPE): Option[Array[V]] = find(TreeNode[V](k, null))
  //---------------------------------------------------------------------------
  private def find(nodeToFind:TreeNode[V]): Option[Array[V]] = {
    //-------------------------------------------------------------------------
    @tailrec
    def iterate(n: TreeNode[V]): Option[TreeNode[V]] = {
      if (n == null)  None
      else {
        nodeToFind.compare(n) match {
          case c if c < 0 => iterate(n.left)
          case c if c > 0 => iterate(n.right)
          case 0 => Some(n)
        }
      }
    }
    //-------------------------------------------------------------------------
    val r = iterate(root)
    if (r.isDefined) Some(r.get.storage.toArray)
    else None
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //preOrder traversal : (root, left, right)
  def preOrder(n: TreeNode[V] = root): Seq[Array[V]] = {
    if (n == null) Seq()
    else Seq(n.storage.toArray) ++ preOrder(n.left) ++ preOrder(n.right)
  }
  //---------------------------------------------------------------------------
  //inorOrder traversal : (left, root, right)
  def inorOrder(n: TreeNode[V] = root): Seq[Array[V]] = {
    if (n == null) Seq()
    else inorOrder(n.left) ++ Seq(n.storage.toArray) ++ inorOrder(n.right)
  }
  //---------------------------------------------------------------------------
  //postorder traversal : (left, right, root)
  def postOrder(n: TreeNode[V] = root): Seq[Array[V]] = {
    if (n == null) Seq()
    else postOrder(n.left) ++ postOrder(n.right) ++ Seq(n.storage.toArray)
  }
  //---------------------------------------------------------------------------
  def getFirstLeftLeaf(): Option[Array[V]] = {
    //-------------------------------------------------------------------------
    var firstLeaf : TreeNode[V] = null
    //-------------------------------------------------------------------------
    def iterate(n: TreeNode[V]) : Unit = {
      if ((n != null) && (firstLeaf == null)) {
        if (firstLeaf == null) iterate(n.left)
        if (firstLeaf == null) iterate(n.right)
        if (firstLeaf == null) firstLeaf = n
      }
    }
    //-------------------------------------------------------------------------
    if (root == null) None
    else {
      iterate(root)
      Some(firstLeaf.storage.toArray)
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def getFirstRightLeaf(): Option[Array[V]] = {
    //-------------------------------------------------------------------------
    var firstLeaf : TreeNode[V] = null
    //-------------------------------------------------------------------------
    def iterate(n: TreeNode[V]) : Unit = {
      if ((n != null) && (firstLeaf == null)) {
        if (firstLeaf == null) iterate(n.right)
        if (firstLeaf == null) firstLeaf = n
        if (firstLeaf == null) iterate(n.left)
      }
    }
    //-------------------------------------------------------------------------
    if (root == null) None
    else {
      iterate(root)
      Some(firstLeaf.storage.toArray)
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def prettyPrint(n: TreeNode[V]= root, tabSpaceCount: Int = 1): Unit = {
    println(s"------------------ Binary search tree:'$name' starts ------------------")
    if (n == null) println("----Empty tree----")
    else TreePrinter.print(n, tabSpaceCount)
    println(s"------------------ Binary search tree:'$name' ends ------------------")
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def printVertical(n: TreeNode[V] = root): Unit = {
    println(s"------------------ Binary search tree:'$name' starts ------------------")
    if (n == null) println("----Empty tree----")
    else TreePrinter.printVertical("",n,false)
    println(s"------------------ Binary search tree:'$name' ends ------------------")
    //-------------------------------------------------------------------------
  }
  //===========================================================================
  case class TreeNode[W](var k: KEY_VALUE_TYPE, storage: ArrayBuffer[W]) extends PrintableNode {
    var left: TreeNode[W] = null
    var right: TreeNode[W] = null
    //-------------------------------------------------------------------------
    def this(_k: KEY_VALUE_TYPE, _v: W) = this(_k, ArrayBuffer[W](_v))
    //-------------------------------------------------------------------------
    var height: Int = 1  // NEW node is initially added at leaf
    //-------------------------------------------------------------------------
    override def toString = s"[k:$k,v:(${storage.mkString(",")})]:hFilterR:$height"
    //-------------------------------------------------------------------------
    def addWithEqualKey(v2 : W) = storage += v2
    //-------------------------------------------------------------------------
    def removeFirstWithEqualKey() = storage.remove(0)
    //-------------------------------------------------------------------------
    def getCountEqualKey() = storage.size
    //-------------------------------------------------------------------------
    def compare(n: TreeNode[W]): Int = k compare n.k
    //-------------------------------------------------------------------------
    def getLeft: PrintableNode = left
    //-------------------------------------------------------------------------
    def getRight: PrintableNode = right
    //-------------------------------------------------------------------------
    def getText: String = toString
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //===========================================================================
}
//=============================================================================
//=============================================================================
//=============================================================================
//End of file AVL_TreeWithDuplicates.scala
//=============================================================================