/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/Feb/2021
 * Time:  20h:51m
 * Description: None
 */
//=============================================================================
package com.common.dataType.conversion
//=============================================================================
import java.nio.{ByteBuffer, ByteOrder}
//=============================================================================
import com.common.dataType.dataType.DataType._
//=============================================================================
import org.apache.commons.io.FileUtils
//=============================================================================
object DataTypeConversion {
  //---------------------------------------------------------------------------
  def byteToBooleanList(b: Array[Byte]): BooleanSeq = b.flatMap(byteToBooleanList)
  //---------------------------------------------------------------------------
  def byteToBooleanList(b: Byte): BooleanSeq = 0 to 7 map isBitSet(b)
  //---------------------------------------------------------------------------
  def isBitSet(b: Byte)(bit: Int): Boolean = ((b >> bit) & 1) == 1
  //---------------------------------------------------------------------------
  def getBitArrayAsString(array: BooleanSeq): String =
    (for (i <- array.length - 1 to 0 by -1)
      yield if (array(i)) '1' else '0').mkString
  //---------------------------------------------------------------------------
  def shortToByteSeq(v: Short, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Array[Byte] =
    ByteBuffer
      .allocate(2)
      .order(byteOrder)
      .putShort(v).array
  def shortSeqToByteSeq(in: Array[Short], byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Array[Byte] = {
    val buffer = ByteBuffer.allocate(in.length * 2) // 2 bytes per int
    buffer.order(byteOrder)
    in.foreach(buffer.putShort)
    buffer.array()
  }
  //---------------------------------------------------------------------------
  def intToByteSeq(v: Int, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Array[Byte] =
    ByteBuffer
      .allocate(4)
      .order(byteOrder)
      .putInt(v).array

  def intSeqToByteSeq(in: Array[Int], byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN) = {
    val buffer = ByteBuffer.allocate(in.toArray.length * 4) // 4 bytes per int
    buffer.order(byteOrder)
    in.foreach(buffer.putInt)
    buffer.array()
  }
  //---------------------------------------------------------------------------
  def longToByteSeq(v: Long, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Array[Byte] =
    ByteBuffer
      .allocate(8)
      .order(byteOrder)
      .putLong(v).array

  def longSeqToByteSeq(in: Array[Long], byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN) = {
    val buffer = ByteBuffer.allocate(in.length * 8) // 8 bytes per long
    buffer.order(byteOrder)
    in.foreach(buffer.putLong)
    buffer.array()
  }
  //---------------------------------------------------------------------------
  def floatToByteSeq(v: Float, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Array[Byte] =
    ByteBuffer
      .allocate(4)
      .order(byteOrder)
      .putFloat(v).array

  def floatSeqToByteSeq(in: Array[Float], byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN) = {
    val buffer = ByteBuffer.allocate(in.length * 4) // 4 bytes per float
    buffer.order(byteOrder)
    in.foreach(buffer.putFloat)
    buffer.array()
  }
  //---------------------------------------------------------------------------
  def doubleToByteSeq(v: Double, byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN): Array[Byte] =
    ByteBuffer.allocate(8)
      .order(byteOrder)
      .putDouble(v).array

  def doubleSeqToByteSeq(in: Array[Double]
                        , byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN) = {
    val buffer = ByteBuffer.allocate(in.length * 8) // 8 bytes per double
    buffer.order(byteOrder)
    in.foreach(buffer.putDouble)
    buffer.array()
  }
  //--------------------------------------------------------------------------
  def byteSeqToShortSeq(byteSeq: Array[Byte]
                        , byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN) = {
    require((byteSeq.size % 2) == 0, s"'byteSeqToShortSeq'. Array must have even size. Current size: ${byteSeq.size}")
    val buffer = ByteBuffer
      .wrap(byteSeq)
      .order(byteOrder)
      .asShortBuffer()
    val s = new Array[Short](buffer.remaining())
    buffer.get(s)
    s
  }
  //---------------------------------------------------------------------------
  def byteSeqToIntSeq(byteSeq: Array[Byte]
                      , byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN) = {

    require((byteSeq.size % 4) == 0, s"'byteSeqToIntSeq'. Array size must be multiple of 4. Current size: ${byteSeq.size}")
    val buffer = ByteBuffer
      .wrap(byteSeq)
      .order(byteOrder)
      .asIntBuffer()
    val s = new Array[Int](buffer.remaining())
    buffer.get(s)
    s
  }
  //---------------------------------------------------------------------------
  def byteSeqToLongSeq(byteSeq: Array[Byte]
                       , byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN) = {
    require((byteSeq.size % 8) == 0, s"'byteSeqToLongSeq'. Array size must be multiple of 8. Current size: ${byteSeq.size}")
    val buffer = ByteBuffer
      .wrap(byteSeq)
      .order(byteOrder)
      .asLongBuffer()
    val s = new Array[Long](buffer.remaining())
    buffer.get(s)
    s
  }
  //---------------------------------------------------------------------------
  def byteSeqToFloatSeq(byteSeq: Array[Byte]
                        , byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN) = {
    require((byteSeq.size % 4) == 0, s"'byteSeqToFloatSeq'. Array size must be multiple of 4. Current size: ${byteSeq.size}")
    val buffer = ByteBuffer
      .wrap(byteSeq)
      .order(byteOrder)
      .asFloatBuffer()
    val s = new Array[Float](buffer.remaining())
    buffer.get(s)
    s
  }
  //---------------------------------------------------------------------------
  def byteSeqToDoubleSeq(byteSeq: Array[Byte]
                         , byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN) = {
    require((byteSeq.size % 8) == 0, s"'byteSeqToDoubleSeq'. Array size must be multiple of 8. Current size: ${byteSeq.size}")
    val buffer = ByteBuffer
      .wrap(byteSeq)
      .order(byteOrder)
      .asDoubleBuffer()
    val s = new Array[Double](buffer.remaining())
    buffer.get(s)
    s
  }
  //---------------------------------------------------------------------------
  def getDataTypeByteSize[T](v: T): Int = getDataTypeBitSize(v) / 8
  //---------------------------------------------------------------------------
  def getByteLengthFromBitCount(b: Int): Int = (b / 8) + (if ((b % 8) > 0) 1 else 0)
  //---------------------------------------------------------------------------
  def getInt(v: Byte): Int = v.toInt & 0xFF
  //---------------------------------------------------------------------------
  def formatSize(v: Long) = FileUtils.byteCountToDisplaySize(v)
  //---------------------------------------------------------------------------
  def baseToInt(s: String, base: String): Int = s.toList.map(base.indexOf(_)).reduceLeft(_ * base.length + _)
  //---------------------------------------------------------------------------
  def hexToInt(s: String): Int = s.toList.map("0123456789abcdef".indexOf(_)).reduceLeft(_ * 16 + _)
  //---------------------------------------------------------------------------
  def isPartialByte(bitSize: Long): Boolean = if ((bitSize % 8) > 0) true else false
  //---------------------------------------------------------------------------
  def getRemainBitInByte(bitSize: Long): Long = if ((bitSize % 8) > 0) 1 else 0
  //---------------------------------------------------------------------------
  def getNumberOfByte(bitSize: Long): Long = (bitSize / 8) + getRemainBitInByte(bitSize)
  //---------------------------------------------------------------------------
  def byteToInt(b: Byte): Int = b & 0xFF
  //---------------------------------------------------------------------------
  def getHex(v: Long, byteSize: Int): Array[Byte] = {

    val hex = new Array[Byte](byteSize)
    var mask = (byteSize - 1) * 8

    for (i <- 0 to byteSize - 1) {
      hex(i) = ((v >> mask) & 0xFF).byteValue()
      mask -= 8
    }
    hex
  }
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
