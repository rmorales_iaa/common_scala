/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  13/Jul/2022 
 * Time:  08h:48m
 * Description: None
 */
//=============================================================================
package com.common.hardware.cpu
//=============================================================================
import com.common.configuration.MyConf
import com.common.util.util.Util
//=============================================================================
//=============================================================================
object CPU {
  //---------------------------------------------------------------------------
  private val USE_PHYSICAL_CORE  = 0
  private val USE_THREAD_CORE    = -1
  //---------------------------------------------------------------------------
  private val DEFAULT_CORE_COUNT = 4
  //---------------------------------------------------------------------------
  private def _getCoreCount(physicalCore: Boolean, confKey : String = "Common.coreCount"): Int = {
    val userCoreCount = MyConf.c.getInt(confKey)
    if (userCoreCount > 0) userCoreCount
    else {
      val r =  if (physicalCore) Util.getCoreCount()._1 else Util.getCoreCount()._2
      if (r == 0) DEFAULT_CORE_COUNT else r
    }
  }
  //---------------------------------------------------------------------------
  def getPhysicalCoreCount(): Int = _getCoreCount(true)
  //---------------------------------------------------------------------------
  def getThreadCoreCount(): Int = _getCoreCount(false)
  //---------------------------------------------------------------------------
  def getCoreCount(confKey : String = "Common.coreCount") = {
    val userCoreCount = MyConf.c.getInt(confKey)
    userCoreCount match {
      case USE_PHYSICAL_CORE => getPhysicalCoreCount
      case USE_THREAD_CORE   => getThreadCoreCount
      case _                 => userCoreCount
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CPU.scala
//=============================================================================
