//=============================================================================
//File: MyConf.scala
//=============================================================================
/** It manages the user configuration file in commonHenosis. See object declaration
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.configuration
//=============================================================================
/** Parser of a user's configuration file based on 'config' ([[https://github.com/typesafehub/config]])
 *  The configuration files are based on key-value pairs
 */
object MyConf {
  //---------------------------------------------------------------------------
  var c :  MyConf = null
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
case class MyConf(confFileName: String = "input/configuration/main.conf"
                  , verbose: Boolean = false) extends MyConfTrait
//=============================================================================
// End of 'MyConf.scala' file
//=============================================================================
