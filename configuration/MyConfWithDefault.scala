//=============================================================================
//File: MyConfWithDefault.scala
//=============================================================================
/** It manages the user configuration file in commonHenosis. See object declaration
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.configuration
//=============================================================================
case class MyConfWithDefault(confFileName: String = "input/configuration/main.conf"
                             , defaultConf: MyConf
                             , verbose: Boolean = false) extends MyConfTrait {
  //---------------------------------------------------------------------------
  override def getBoolean(key: String) : Boolean =
    if (super.existKey(key)) super.getBoolean(key)
    else defaultConf.getBoolean(key)
  //---------------------------------------------------------------------------
  override def getString(key: String) : String =
    if(super.existKey(key)) super.getString(key)
    else defaultConf.getString(key)
  //---------------------------------------------------------------------------
  override def getStringSeq(key: String): List[String] =
    if (super.existKey(key)) super.getStringSeq(key)
    else defaultConf.getStringSeq(key)
  //---------------------------------------------------------------------------
  override def getInt(key: String) : Int =
    if (super.existKey(key)) super.getInt(key)
    else defaultConf.getInt(key)
  //---------------------------------------------------------------------------
  override def getIntSeq(key: String): List[Int] =
    if (super.existKey(key)) super.getIntSeq(key)
    else defaultConf.getIntSeq(key)
  //---------------------------------------------------------------------------
  override def getFloat(key: String) : Float =
    if (super.existKey(key)) super.getFloat(key)
     else defaultConf.getFloat(key)
  //---------------------------------------------------------------------------
  override def getDouble(key: String) : Double =
    if (super.existKey(key)) super.getDouble(key)
    else defaultConf.getDouble(key)
  //---------------------------------------------------------------------------
  override def getIfExistKey(key: String): Option[String] =
    if (existKey(key)) Some(getString(key))
    else defaultConf.getIfExistKey(key)
  //---------------------------------------------------------------------------
  override def getDoubleWithDefaultValue(key: String, defaultValue: Double): Double = {
    if (super.existKey(key)) super.getDoubleWithDefaultValue(key, defaultValue)
    else defaultConf.getDoubleWithDefaultValue(key, defaultValue)
  }
  //---------------------------------------------------------------------------
  override def getStringWithDefaultValue(key: String, defaultValue: String): String = {
    if (super.existKey(key)) super.getStringWithDefaultValue(key, defaultValue)
    else defaultConf.getStringWithDefaultValue(key, defaultValue)
  }
  //---------------------------------------------------------------------------
  override def getIntWithDefaultValue(key: String, defaultValue: Int): Int = {
    if (super.existKey(key)) super.getIntWithDefaultValue(key, defaultValue)
    else defaultConf.getIntWithDefaultValue(key, defaultValue)
  }
  //---------------------------------------------------------------------------
  override def existKey(key: String) : Boolean = {
    if (super.existKey(key)) true
    else defaultConf.existKey(key)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
// End of 'MyConfWithDefault.scala' file
//=============================================================================
